# Annales de l'Institut Galilée

Ce gitlab est une collection de documents compilé par les étudiants de
l'Institut Galilée afin de s'entraider dans les cours. Pour l'instant, les
archives contiennent surtout celle d'étudiants en Double-Licence
Mathématiques et Informatique.

## Avertissement légal et sur le plagiat

Ce dépôt n'est disponible sous les termes d'**aucune** Licence, et ce
volontairement : **je n'autorise pas la réutilisation d'aucune des données de ce
GitLab**. Ces données ne sont disponible qu'à titre éducatif et uniquement
pour leur consultation. J'interdis leur réutilisation (mes propres contributions
incluse).

**SI, POUR UN PROJET, L'ENVIE VOUS TENTE DE RÉUTILISER DES FICHIERS DE CE GITLAB :**
1. **LE PLAGIAT EST UN IMMENSE MANQUE DE RESPECT POUR MOI ET POUR TOUS CEUX QUI ONT
COLLABORÉ À CES ANNALES ET IL NE FAIT QUE MONTRER VOTRE DÉSINTÉRÊT POUR VOS ÉTUDES.
SI VOUS EN ÊTES ARRIVÉ À RECOURIR AU PLAGIAT, IL VAUDRAIT MIEUX POUR VOUS DE
RECONSIDÉRER VOTRE PLACE EN UNIVERSITÉ.**
2. **VOUS AUREZ 0. LES PROFESSEURS GARDENT LES ANCIENS PROJETS DANS L'OUTIL DE
DÉTECTION DU PLAGIAT. ET NON, RENOMMER DES VARIABLES, DÉFAIRE DES MACROS, ETC...
NE SUFFIT PAS À VOUS ÉVITER LA DÉTECTION DU PLAGIAT. EN 2024, PLUSIEURS ÉTUDIANTS
SE SONT VU METTRE 0 POUR AVOIR TENTÉ DE RECOPIER UN PROJET. CERTAINS AVAIENT
TENTÉ D'EFFECTUER DES CHANGEMENTS POUR DUPER LE LOGICIEL, IL SE SONT QUAND MÊME
FAIT PRENDRE. JE RAPPELLE QU'UN PLAGIAIRE AURA 0 ET RISQUE L'EXCLUSION DÉFINITIVE
DE TOUT ÉTABLISSEMENT D'ENSEIGNEMENT SUPÉRIEUR JUSQU'À 5 ANS, ET LES PROCÉDURES
SONT FRÉQUENTES. SI VOUS VOULEZ RISQUER DE PERDRE 5 ANS DE VOTRE VIE, FAITES DONC.**

Légalement parlant, les documents de cours (hors notes personnelles)
appartiennent aux professeurs de la matière. Si j'ai consulté plusieurs de mes
professeurs, malheureusement je n'ai pas obtenu l'accord de chacun pour
redistribuer leurs cours. Je compte sur les valeurs académiques de libre partage
de la science dans ma démarche, surtout que je ne suis pas le seul à compiler
des annales dans l'Institut Galilée (et au moins, les miennes ne sont pas
payante, contrairement à d'autres), et je pense sincèrement que ces
annales peuvent aider les étudiants dans leurs enseignements.
Néanmoins, je comprendrais que certains professeurs ne veulent pas voir leurs
cours apparaître en public. Si cela est votre cas, contactez-moi avec l'un
des moyens de contact listé dans la section "Contribuer", je supprimerai
vos travaux, ainsi que l'historique git.

## Contribuer

Nous accueillons toutes les contributions tant que ce sont celle d'étudiants
à l'Institut Galilée, surtout à fin de maintenir ce GitLab dans le futur.

Pour contribuer vous-même à ces archives, vous pouvez :
- Ouvrir un ticket sur GitLab
- Ouvrir une requête de fusion sur GitLab (si vous vous y connaissez en git)
- M'envoyer un e-mail à mon adresse personnelle : `mail+git-ig` \[at\] `lyes` \[dot\] `eu`
- M'envoyer un message sur Discord à \[at\] `ntlyes`

## Contributeurs

- Lyes Saadi
- Youssef El Otmani
- Kylian Maouchi
- Tidjani Stefel
- Malek Benhamed
