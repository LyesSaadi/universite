(** *Nombres réels : la distance et les suites*)
Require Import Unicode.Utf8.
Require Import Reals.
Require Import Lra.
Require Import Lia.
Require Export RealField.
Open Scope R_scope.

(** **La valeur absolue

    Maintenant, on fait de l'analyse !
    L'analyse mathématique est la partie des mathématiques qui s'intéressent
    aux ordres de grandeur : est-ce que cette chose est "petite" ? "grande" ? à
    quelle point ? Ces deux choses sont-elles "proches" ou "éloignées", à quel
    point et comment le dire et le prouver rigoureusement ?

    C'est Karl Weierstrass (1815 -- 1897) qui nous a donné les définitions
    précises qui nous permettent de faire de l'analyse rigoureusement
    aujourd'hui.

    Sur l'ensemble des réels, c'est la valeur absolue (qui elle-même provient de
    l'ordre) qui nous permet de parler de distance entre deux réels.
*)
(** Elle est notée Rabs dans la bibliothèque standard de coq. Sa définition
    n'est pas immédiatement compréhensible. *)
Print Rabs.
(** En fait le théorème [Rcase_abs] (conséquence de la totalité de l'ordre)
    affirme que pour tout réel, on a soit une preuve qu'il est strictement
    négatif, soit une preuve qu'il est positif ou nul. *)
Check Rcase_abs.
(** Le [if ... then ... else] bizarre dans [Rabs] dit donc que dans le cas où
    [r < 0], (Rabs r) retourne (-r) et sinon r.

    Pour prouver des théorèmes sur la valeur absolue, on utilise donc une
    disjonction de cas ([destruct]) sur [Rcase_abs]. Voici un exemple. *)
Theorem Rabs_Ropp: ∀ x : R, Rabs (- x) = Rabs x.
Proof.
  intros x. unfold Rabs.
  (* On sépare les cas (il y en a 4), dont deux contradictoires, suivant le
   * signe de x et le signe de -x *)
  destruct (Rcase_abs x) as [Hx | Hx];
      destruct (Rcase_abs (-x)) as [Hmx | Hmx].
      - lra. (* lra sait prouver tout seul que x < 0 et -x < 0 est
      contradictoire *)
      - lra.
      - apply Ropp_involutive.
      - assert (E: x = 0). {
          (* preuve que x = 0 *)
          lra. (* la flemme... *)
        }
      rewrite E. rewrite Ropp_0. reflexivity.
Qed.

(** Quelques commentaires importants sur cette preuve.
    - le premier [destruct] est suivi par un point-virgule : le second
    [destruct] s'applique alors à chaque sous-cas du premier.
    - On a utilisé une tactique automatique puissante qui s'appelle [lra] et
    sait prouver des inégalités entre réels qui sont "évidentes" dans le
    contexte. On aurait pu tout faire à la main comme dans << CorpsOrdonné.v >>
    mais pour écrire des preuves non triviales d'analyse, ça devient trop lourd.
    - On a utilisé la tactique [assert] sous la forme suivante :
      [[
      assert (nom: formule). {
        (* preuve de la formule ... *)
      }
      ]]
    Il s'agit d'ajouter une nouvelle formule (appelée nom) au contexte. Bien
    sûr, coq commence par vous en demander une preuve.

    En fait, de nombreuses choses dans la preuve précédente sont à but
    uniquement pédagogique. On aurait pu aller encore beaucoup plus vite : *)
Theorem Rabs_Ropp': ∀ x : R, Rabs (- x) = Rabs x.
Proof.
  intros x. unfold Rabs.
  destruct (Rcase_abs x); destruct (Rcase_abs (-x)); lra.
Qed.

(** À vous ! On commence par prouver quelques propriétés de Rabs.
    N'hésitez pas à utiliser lra. *)

Theorem Rabs_pos: ∀ x : R, 0 <= Rabs x.
Proof.
  intros.
  unfold Rabs.
  destruct (Rcase_abs x); lra.
Qed. (* Remplacer cette ligne par Qed. *)


Theorem Rabs_0_0: forall x, (Rabs x) = 0 -> x = 0.
Proof.
  intros.
  unfold Rabs in H.
  destruct (Rcase_abs x) in H; lra.
Qed. (* Remplacer cette ligne par Qed. *)


Theorem Rabs_triang: ∀ a b : R, Rabs (a + b) <= Rabs a + Rabs b.
Proof.
  intros.
  unfold Rabs.
  destruct (Rcase_abs a); destruct (Rcase_abs b); destruct (Rcase_abs (a + b)); lra.
Qed. (* Remplacer cette ligne par Qed. *)


Lemma Rabs_neq_0 : ∀ x, x <> 0 → Rabs x > 0.
Proof.
  intros.
  unfold Rabs.
  destruct (Rcase_abs x); lra.
Qed. (* Remplacer cette ligne par Qed. *)

(** Nous n'avons pas épuisé la liste des théorèmes utiles sur [Rabs]. Pour les
    chercher (et en général pour chercher un théorème dans Coq) : *)
Search Rabs. (* Les théorèmes qui font apparaître Rabs dans leur énoncé. *)
Search Rabs Rmult. (* Les théorèmes qui font apparaître Rabs et Rmult dans leur énoncé. *)
Search "Rabs". (* Les théorèmes qui contiennent Rabs dans leur nom. *)

(** **Distance sur R et égalité en analyse

    En analyse, prouver que deux choses sont égales s'obtient souvent par
    des moyens détournés qui peuvent surprendre le débutant.
*)

(** Voici le résultat sans doute le plus fréquemment utilisé en
    analyse (mais souvent implicitement) : pour prouver qu'un réel est nul, il
    suffit de montrer que sa valeur absolue (c'est-à-dire sa distance à 0) est
    inférieure à tout réel strictement positif.
*)
Lemma small_zero: forall x, (forall eps, eps > 0 -> (Rabs x) < eps) -> x = 0.
Proof.
  intros.
  unfold Rabs in H.
  destruct (Rcase_abs x) in H.
  -
    assert (E: - x > 0) by lra.
    apply (H (- x)) in E.
    lra.
  -
    destruct r.
    +
      apply H in H0.
      lra.
    +
    assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** La distance sur R est notée R_dist dans cette bibliothèque. *)
Print R_dist.

(** Les propriétés usuelles des distances sont déjà prouvées dans la
    bibliothèque. À ce stade, avec [lra] et des [destruct (Rcase_abs)] ça ne
    vous apporterait pas grand chose de les prouver, donc nous les admettons.
*)
Search "R_dist".
(** Remarque culturelle au passage : généralement, en mathématiques, une
    distance sur un ensemble E est une fonction d de E * E dans l'ensemble des
    réels positifs ou nuls qui satisfait :
    - symétrie : d(x,y) = d(y,x)
    - séparation : d(x,y) = 0 <-> x = y
    - inégalité triangulaire : d(x, z) <= d(x, y) + d(y, z).

    Ici, ces propriétés sont exprimées dans : *)
Check R_dist_pos. (* à valeurs positives ou nulles *)
Check R_dist_sym. (* symétrie *)
Check R_dist_refl. (* séparation *)
Check R_dist_tri. (* inégalité triangulaire *)

(** Avec le travail fait précédemment, il est facile de prouver que deux réels
    sont égaux lorsque leur distance peut être rendue aussi petite qu'on veut.
    Pensez à [unfold R_dist]. *)
Lemma small_dist_equal: forall x y,
  (forall eps, eps > 0 -> (R_dist x y) < eps) -> x = y.
Proof.
  intros.
  unfold R_dist in H.
  apply small_zero in H.
  lra.
Qed. (* Remplacer cette ligne par Qed. *)


(** ** Les suites convergentes
    
    Une suite réelle n'est rien d'autre qu'une fonction de nat vers R.
    La définition de la convergence d'une suite vers une limite l (finie)
    par Weierstrass a fait entrer l'analyse dans son ère moderne.
    Dans cette bibliothèque, cette propriété se note Un_cv.
*)
Print Un_cv.

(** Ne vous laissez pas impressionner par les notations un
    peu cryptiques de coq, il s'agit bien de la définition usuelle : une suite
    Un converge vers une limite l si la distance entre Un et l devient aussi
    petite que l'on veut à partir d'un certain rang. *)

(** En guise d'exemple, voici la preuve de l'unicité d'une telle limite,
    lorsqu'elle existe. *)

Theorem UL_sequence:
  ∀ (Un : nat → R) (l1 l2 : R), Un_cv Un l1 → Un_cv Un l2 → l1 = l2.
Proof.
  unfold Un_cv.
  intros Un l1 l2 Hl1 Hl2.
  (* On va montrer que la distance entre l1 et l2 est aussi petite qu'on veut.*)
  apply small_dist_equal.
  (* Soit eps > 0. *)
  intros eps Heps.
  assert (Heps2 : eps / 2 > 0) by lra. (* eps / 2 > 0 *)
  (* Soit n1 tel que pour tout n >= n1, |Un - l1| < eps / 2. *)
  destruct (Hl1 (eps / 2) Heps2) as [n1 Hn1].
  (* Soit n2 tel que pour tout n >= n2, |Un - l2| < eps / 2. *)
  destruct (Hl2 (eps / 2) Heps2) as [n2 Hn2].
  (* Soit n3 = max(n1, n2). *)
  pose (n3 := (max n1 n2)).
  (* Alors, |Un3 - l1| < eps / 2 *)
  assert (Hdistl1: R_dist (Un n3) l1 < eps / 2). {
    apply Hn1.
    apply Nat.le_max_l. (* max(n1, n2) >= n1 *)
  }
  (* et |Un3 - l2| < eps / 2 *)
  assert (Hdistl2: R_dist (Un n3) l2 < eps / 2). {
    apply Hn2.
    apply Nat.le_max_r. (* max(n1, n2) >= n2 *)
  }
  (* Il suffit de montrer (par transitivité de <) que
    |l1 - l2| < |l1 - Un3| + |Un3 - l2| et
    |l1 - Un3| + |Un3 - l2| < eps *)
  apply Rle_lt_trans with
    (r2 := R_dist (Un n3) l1 + R_dist (Un n3) l2).
    - (* La première inégalité est l'inégalité triangulaire. *)
    rewrite (R_dist_sym (Un n3)).
    apply R_dist_tri.
    - (* La seconde en sommant les deux inégalités précédentes. *)
    replace eps with (eps/2 + eps/2) by lra.
    now apply Rplus_lt_compat.
Qed.

(** Remarques sur l'importante preuve précédentes
    - Ça se complique et s'allonge... ce sont des résultats non triviaux.
    - On essaie de déléguer au maximum les calculs évidents annexes à [lra].
    Avec assert, si lra peut conclure tout seul, on peut utiliser la syntaxe
    [[
      assert (nom: formule) by lra.
    ]]
    (ou au besoin, remplacer lra par toute autre tactique qui permet de conclure
    immédiatement.)
    - La tactique [replace terme1 with terme2] remplace un terme par un autre
    dans le but. Coq va nous demander ensuite une preuve de l'égalité
    [terme1 = terme2].
    Ici, on la donne directement après le mot-clé [by] car [lra] sait le faire.
    Il y a bien sûr une variante [replace terme1 with terme2 in nom_de_formule]
    qui permet de remplacer un terme par un autre dans une formule du contexte.
    - La tactique [pose] permet d'introduire dans le contexte un nouvel objet
    (ou même un théorème, une hypothèse). Ici on aurait pu s'en passer et
    simplement écrire [exists (max n1 n2)] mais ça correspondait mieux à la
    démonstration mathématique.
    - Le coeur de l'activité de l'analyste est de manipuler des inégalités. Ici
    on a utilisé (et vous utiliserez encore beaucoup) :
*)
Check Rle_lt_trans.
Check Rlt_trans.
Check Rplus_lt_compat.
(** On ne peut plus tout passer en revue, donc : *)
Search "Rlt". (* théorèmes dont le nom contient Rlt *)
Search Rlt Rplus. (* théorèmes faisant intervenir < et + *)
Search Rlt Rmult. (* théorèmes faisant intervenir < et * *)

(** Pour couronner le tout, comme on étudie des suites, on a aussi besoin de
    théorèmes sur les inégalités entre entiers naturels. *)
Search "<="%nat. (* Le %nat sert à définir la portée du symbole <= car ici, nous
  sommes dans celles des réels. *)
Check le_trans. (* le pour "lesser than or equal to", transitivité *)
Check le_refl. (* réflexivité *)
Search "_ <= _"%nat.
Check le_antisym. (* antisymétrie *)
(** Et pour des inégalités "évidentes" dans nat, il y a la tactique automatique
    [lia]. Un exemple (utile) : *)
Lemma exemple_lia (n m : nat) : (n <= (S m) -> n <= m \/ n = (S m))%nat.
Proof.
  intro H. lia.
Qed.

(** Enfin, on a souvent à utiliser les lemmes sur le maximum de deux entiers. *)
Check Nat.max_l.
Check Nat.max_r.
Check Nat.le_max_l.
Check Nat.le_max_r.
Search max%nat.

(** Assez de blah-blah. En vous inspirant de la preuve de UL_sequence, prouvez
    que la somme de deux suites convergentes converge vers la somme des limites.
 *)

Theorem CV_plus (An Bn : nat -> R) (l1 l2 : R) :
  let Cn := (fun n => An n + Bn n) in
  Un_cv An l1 -> Un_cv Bn l2 -> Un_cv Cn (l1 + l2).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** Maintenant, à vous de faire la preuve qu'en multipliant une suite
    convergente vers l par une constante lambda, on obtient une suite qui
    converge vers lambda * l.
    Attention, il y a deux cas lambda = 0 ou non.
    Faire un [destruct Req_dec lambda 0] pour séparer ces deux cas.
    Quelques lemmes, non rencontrés jusqu'à maintenant que nous avons utilisé :
*)
Check Req_dec.
Check R_dist_eq.
Check Rabs_pos_lt.
Check Rdiv_lt_0_compat.
Check Rmult_lt_compat_l.
Check R_dist_mult_l.
(** Ça ne veut pas dire que ce soit indispensable de les utiliser (il y a
    de nombreuses façons de faire). Assurez-vous, avant de vous lancer dans la
    preuve formelle de savoir le faire en maths. *)
Lemma CV_mult_const (Un : nat → R) (lambda : R) (l : R) :
  let Vn := fun n => lambda * Un n in
  Un_cv Un l → Un_cv Vn (lambda * l).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** On va maintenant vers un résultat très important : les suites convergences
    sont bornées. Pour ce faire on a besoin de l'opération suivante : *)
Fixpoint running_max (Un : nat → R) (n : nat) :=
  match n with
    0%nat => (Un 0%nat)
  | S(n) => Rmax (running_max Un n) (Un (S n))
  end.
(** En français, running_max Un n est le maximum des n + 1 premiers termes de la
    suite Un. *)

Lemma Un_le_running_max (Un : nat → R) (n : nat) :
  forall i, (i <= n)%nat → Un i <= (running_max Un n).
Proof.
(* N'oubliez pas que sur les entiers nat, l'induction existe! *)
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** On a maintenant ce qu'il faut pour le montrer. Assurez-vous encore de savoir
    écrire la preuve mathématique avant de vous lancer. *)

Theorem CV_impl_bounded (Un : nat -> R) (l : R) :
  Un_cv Un l -> exists m : R, 0 < m /\ (forall n : nat, Rabs (Un n) <= m).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** **Un exemple de suite qui diverge vers l'infini
    
    Il n'y a pas que les suites convergentes dans la vie. Il y a aussi
    - des suites qui divergent vers + l'infini;
    - des suites qui divergent vers - l'infini;
    - des suites qui divergent tout court (n'admettent ni limite finie ni limite
      infinie).

    On va ici prouver la suite définir par Un = n pour tout n diverge vers
    l'infini.
    D'abord la définition, dans cette bibliothèque, cela s'écrit [cv_infty].
*)
Print cv_infty.

(** En mathématiques, on fait souvent (et pour de bonnes raisons) l'abus que
    l'ensemble des naturels est inclus dans l'ensemble des réels. En coq, ce
    n'est pas possible : nat et R sont deux types différents.
    La fonction INR (pour injection de N dans R) permet de passer du naturel n
    au réel 1 + 1 + ... + 1 (n fois). *)
Print INR.

(** Nous allons prouver que la suite INR diverge vers l'infini. Pour ceci, nous
    aurons besoin de la propriété suivante des réels : ils forment un corps
    archimédien : *)
Axiom archimed' :
  forall eps A : R, eps > 0 -> A > 0 -> exists n : nat, (INR n) * eps > A.
(** Cet axiome dit en substance que
    - pour un eps > 0 aussi petit qu'on veut;
    - pour un A > 0 aussi grand qu'on veut;
    On peut toujours trouver un entier naturel n tel que n * eps est plus grand
    que A.
    
    Remarque : l'axiome [archimed] de cette bibliothèque est différent, car plus
    puissant, mais plus difficile, dans un premier temps, à utiliser.
*)

(** À vous maintenant, vous aurez à
    [destruct (archimed' preuve1 preuve2 val_eps val_A)]
    avec :
    - val_eps : ce que vous avez choisi comme eps (n'allez pas chercher trop
    loin...)
    - val_A : ce que vous avez choisi comme A
    - preuve1 : une preuve que votre eps > 0
    - preuve2 : une preuve que votre A > 0

    Petit point embêtant : comme il n'y a pas d'hypothèse sur M dans
    cv_infty, il faut gérer les cas M < 0 et M = 0 (qui pourtant ne servent à
    rien dans la preuve), [lra] est votre ami.

    Enfin, vous aurez sans doute besoin de résultats sur INR.
*)
Search "INR".
Check le_INR.

Theorem n_to_infty :
  let Un n := INR n in cv_infty Un.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** archimed' n'est pas définit dans la stdlib. Vous pouvez utiliser archimed de 
    la stdlib, mais c'est plus difficile (passages de N à Z à R). Pas exemple,
    si vous souhaitez vous entrainer un peu plus (optionnel), vous pouvez
    démontrer le lemme suivant (avec archimed ou archimed'): *)

Lemma cv_speed_1_Sn :
  Un_cv (fun n:nat => / INR (S n)) 0.
Proof.
(* On utilise le fait que si une suite Un tend vers +infini alors 
la suite 1/Un tend vers 0 *)
(* On peut utiliser le lemme cv_infty_cv_R0 défini dans la stdlib. *)
(* ... ici ... *)
(* Dans notre cas, il faudra par la ensuite montrer que (S n) tend vers
+infini. *)
(* Une suite Un tend vers +infini si:
∀ M : R, ∃ N : nat, ∀ n : nat, N ≤ n → M < Un n *)
(* Pour cela, il faut considérer 3 cas selon que M est négatif, égal à 0 
ou positif *)
(* On peut utiliser le lemme total_order_T défini dans la stdlib. *)
(* ... ici ... *)
(* Cas M < 0: N = 0 et vous pourrez utiliser Rlt_trans, lt_INR_0 et lt_O_Sn. *)
(* Cas M = 0: N = 0 également. *)
(* Cas M > 0: N = l'entier supérieur à M. 
Vous pourrez utiliser up, archimed (ou archimed'), et les lemmes d'injection 
de N->R, Z->R, etc.*)
(* ... ici ... *)
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)



(** **Majorant, minorant, borne supérieure et borne inférieure *)

(** Rappel : en coq, une partie de R est une fonction de R vers Prop.
    Un majorant (en anglais upper bound) est une valeur qui est supérieure ou
    égale à tous les éléments de l'ensemble. *)
Print is_upper_bound. (* en français : est majorant de *)
(** On rappelle qu'il faut ici comprendre (E x) comme "x appartient à E", donc
    [is_upper_bound E m] dit que pour tout x appartenant à E, x <= m. *)

(** Un minorant d'une partie est défini de la même manière. *)
Definition is_lower_bound (E : R -> Prop) (l : R) :=
  ∀ x : R, E x → l <= x.

(** On va montrer que l'ensembles des inverses des entiers naturels est minoré
    par 0. *)
Definition ens_inverses (x:R) := exists n : nat, x = / ((INR n) + 1).

Check pos_INR.
Theorem inverses_min : is_lower_bound ens_inverses 0.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** La borne supérieure d'un ensemble, lorsqu'elle existe est le plus petit
    majorant de cet ensemble (en anglais least upper bound), ici, [is_lub]. *)
Print is_lub.

(** Même chose pour la borne inférieure (en anglais greatest lower bound). *)
Definition is_glb (E : R -> Prop) (l : R) :=
  is_lower_bound E l ∧ (∀ b : R, is_lower_bound E b → b <= l).

(** À vous, maintenant, prouvez que la borne inférieure des inverses des entiers
    naturels est 0. *)
Theorem inverses_glb : is_glb ens_inverses 0.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** **Produit de deux suites convergentes. *)

(** On commence par le cas où l'une des suites converge vers 0.
    N'hésitez pas, si vous en éprouver le besoin à écrire des lemmes pour
    faciliter la preuve. *)

Theorem CV_mult_0 (An Bn : nat → R) (a : R) :
  let Cn (n:nat) := (An n * Bn n) in
  Un_cv An a → Un_cv Bn 0 → Un_cv Cn 0.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** Ensuite, le cas général, attention, ça peut être long et pénible... *)
Theorem CV_mult (An Bn : nat → R) (a b : R) :
  let Cn (n:nat) := (An n * Bn n) in
  Un_cv An a → Un_cv Bn b → Un_cv Cn (a * b).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


