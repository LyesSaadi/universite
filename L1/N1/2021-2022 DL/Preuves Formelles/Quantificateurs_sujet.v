(** *Quantificateur universel et existentiel *)

Require Import Unicode.Utf8.
Require Import Nat.
Require Import PeanoNat.

(** Ce sujet contient deux parties :
    - la première reprend et approfondit le travail entamé sur le quantificateur
      universel [forall] ;
    - la seconde introduit le quantificateur existentiel [exists], ainsi que les
      tactiques nécessaires pour _utiliser_ une hypothèse ainsi quantifiée
      ([destruct] comme toujours...) et pour _prouver_ ou introduire
      une formule du type "il existe ...".
*)

Module Forall.
(** ** Le connecteur forall *)

(** Le connecteur ∀ s'écrit (forall) dans des mathématiques en Coq, et
(\forall + RET) dans du texte en Coq. Il se lit "pour tout" : "∀ x,
P(x)" est la proposition qui dit que pour tout x, la proposition P x
est vraie. Il a déjà été vu en pratique, et il s'agit ici
principalement de révisions*)

(** *** Premiers exemples
    Pour les exemples à venir, on se donne :
    - un type quelconque X
    - deux _prédicats_ P et Q sur X, ce qui revient en coq à des fonctions de X
      vers le type Prop des propositions.
*)

Parameters (X : Type) (P Q : X → Prop).

(** L'énoncé [forall x : X, (P x)], en français "tous les éléments x du type X
    satisfont P" est encore une proposition. *)
Check (∀ x : X, (P x)).

(** La proposition True en coq est une proposition toujours vraie, ce qui
    signifie en pratique :
    - qu'on n'a jamais besoin de rien pour la prouver ;
    - qu'à l'inverse elle ne sert jamais à rien en hypothèse.
*)

Print True.
(** Pour ceux qui veulent tout comprendre : cette définition inductive de True
    signifie que pour construire une preuve de True, on utilise le constructeur
    I qui n'a aucun argument, donc peut toujours être appliqué. *)

Lemma true_idiot : True.
Proof.
  apply I.
Qed.

(** La tactique [trivial] essaie de terminer une preuve de façon un peu
    automatique (par exemple, essaie [assumption] et [reflexivity]). Entre
    autres choses, elle sait terminer une preuve de True, donc peut être
    utilisée à la place de [apply I] *)

Lemma true_idiot2 : ∀ A : Prop, A ∨ True.
Proof.
  intros A.
  right.
  trivial.
Qed.

(** **** Exercice : Prouver le théorème suivant. *)

Lemma p_and_true : ∀ x : X, (P x) ↔ ((P x) ∧ True).
Proof.
  intros x.
  split.
  -
    intros Px.
    split.
    *
      trivial.
    *
      trivial.
  -
    intros PxaT.
    destruct PxaT as [Px T].
    apply Px.
Qed. (* Remplacer cette ligne par Qed. *)



(** Rappel : pour utiliser une proposition commençant par un ∀, on peut utiliser
    les tactiques [exact] ou [apply].

    Le lemme suivant (un peu idiot) s'énoncerait de la façon suivante :
    "Soit x0 un élément du type X. Si P est satisfait par tout élément x du
    type X, alors P est satisfait par x0." *)

Lemma apply_univ1 (x0 : X) : (∀ x : X, P x) → (P x0).
Proof.
  (* On suppose (hypothèse H), (P x) pour tout x de type X.*)
  intros H.
  (* On doit montrer (P x0), mais c'est exactement notre hypothèse H appliquée à
     x0. *)
  exact (H x0).
Qed.

(** Même chose avec [apply]. *)

Lemma apply_univ2 (x0 : X) : (forall x : X, P x) -> (P x0).
Proof.
  intros H.
  apply (H x0).
Qed.

(** En fait, avec [apply], il n'y a pas besoin de préciser qu'on remplace x par
    x0, l'algorithme d'unification de coq s'en charge pour nous. *)

Lemma apply_univ3 (x0 : X) : (∀ x : X, P x) → (P x0).
Proof.
  intros H.
  apply H.
Qed.

(** Remarque: Il n'y a aucune différence fondamentale entre *)

Lemma test (x : X) : P x.
Admitted.
 (** et *)
Lemma test' : ∀ (x : X), P x.
Admitted.

(** Les deux ont le même type, ce qui dans le cas des lemmes (ou théorèmes)
    veut dire que les deux prouvent la même chose *)

Check test.
Check test'.

(** *** Quelques tautologies du calcul des prédicats
    Les formules qu'on vous demande de prouver (ou au contraire de ne pas
    prouver) ci-dessous sont assez importantes.
*)

(** **** Exercice : Prouver le théorème suivant. *)

Lemma forall_and_forall :
  (∀ x : X, P x) ∧ (∀ y : X, Q y) ↔ (∀ x, (P x ∧ Q x)).
Proof.
  (* indice: penser à "destruct (H x)" *)
  split.
  -
    intros H.
    destruct H as [HP HQ].
    split.
    apply HP.
    apply HQ.
  -
    intros H.
    split.
    apply H.
    apply H.
Qed. (* Remplacer cette ligne par Qed. *)


Lemma forall_or_forall : (∀ x : X, P x) ∨ (∀ y : X, Q y) → (∀ x, (P x ∨ Q x)).
Proof.
  intros.
  destruct H.
  -
    left.
    apply H.
  -
    right.
    apply H.
Qed. (* Remplacer cette ligne par Qed. *)


(** Est-ce que vous savez expliquer pourquoi l'implication inverse
    n'est pas vrai ?  Vous pouvez éventuellement essayer de la prouver
    ci-dessous et de comprendre pourquoi vous êtes bloqués. *)

Lemma forall_p_or_q_false : ∀ x, (P x ∨ Q x) → (∀ x : X, P x) ∨ (∀ y : X, Q y).
Proof.
Abort.

End Forall.


(** ** Le connecteur exists *)

Module Exists.

(** Le connecteur ∃ s'écrit (exists) dans des mathématiques en Coq, et
(\exists + RET) dans du texte en Coq. Il se lit "il existe" : "∃ x,
P(x)" est la proposition qui dit qu'il existe un x tel que la proposition P x
est vraie.*)

(** *** Exemples *)

Parameters (X : Type) (P Q : X → Prop).

Check (exists x, P x).

(** En logique intuitionniste, pour prouver une proposition du type "il
    existe un élément ayant telle propriété", la seule possibilité est
    _d'exhiber un tel élément_ (appelé témoin). La tactique [exists], suivie de
    ce témoin pressenti permet d'éliminer le quantificateur [exists]. *)

Lemma exists_n_p_3_eq_8 : ∃ n : nat, n + 3 = 8.
Proof.
  (* Pour montrer qu'il existe n tel que n + 3 = 8, _il suffit_ de montrer que
     5 + 3 = 8 (c'est une intuition géniale qui m'a fait penser à 5). *)
  exists 5.
  (* Par définition de +, 5 + 3 = 8. *)
  simpl.
  (* Les deux membres de l'égalité sont les mêmes, la preuve est terminée. *)
  reflexivity.
Qed.

(** Pour un type habité (c'est-à-dire ayant au moins un élément), on a le
    résultat très intuitif suivant.  *)

Lemma forall_imp_exists (x0 : X) : (∀ x, P x) → (∃ x, P x).
Proof.
  intros H.
  (* Il s'agit de montrer que il existe un x tel que P x est
  vrai. Dans notre cas c'est trivial. On sait que il existe au moins
  un x de type X, c'est x0. Et grace à H, on sait que (P x0) est
  vrai *)
  exists x0.
  apply (H x0).
Qed.

(** Attention, une erreur très courante en mathématiques (ou même dans la
    logique de tous les jours) consiste à penser qu'avec un ou quelques exemples
    (donc des preuves de "il existe"), on montre "pour tout" :
    - Le polynôme X^2 - 4 a des racines réelles donc tous les polynômes en ont.
    - Les entiers impairs 3, 5 et 7 sont premiers donc tous les nombres impairs
    le sont.
    - Cet étudiant est nul en maths, la preuve, il a eu 3 au dernier contrôle.
*)


(** Pour utiliser une proposition avec exists, on utilise de nouveau
    la tactique destruct. Noter la syntaxe [as [y H]], où y est la
    variable pour laquelle la propritétée va être vrai, et H la preuve que
    cette propriété est vraie pour y. *)

Lemma exP_imp_exQ (Q : X → Prop) :
  (∀ x, P x → Q x) → (∃ x, P x) → (∃ x, Q x).
Proof.
  (* On suppose :
     (H1) : ∀ x, P x → Q x
     (H2) : ∃ x, P x. *)
  intros H1 H2.
  (* D'après (H2), il y a un certain y de type X qui vérifie
     (H') : P y. *)
  destruct H2 as [y H'].
  (* Pour prouver qu'il existe x tel que Q x, il suffit de prouver Q y. *)
  exists y.
  (* D'après (H1), pour prouver Q y, il suffit de prouver P y. *)
  apply H1.
  (* Or, d'après H', on a bien P y. *)
  assumption.
Qed.

(** **** Exercice : Prouver le théorème suivant. *)

Lemma nat_0_ou_S : ∀ n : nat, n = 0 ∨ (∃ k : nat, n = S k).
Proof.
  (* Indice : destruct n *)
  intros n.
  induction n as [|n' HI].
  -
    left.
    reflexivity.
  -
    right.
    exists n'.
    reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)


(** **** Exercice : Prouver le théorème suivant. *)

Lemma exists_and : (∃ x, P x ∧ Q x) → (∃ x : X, P x) ∧ (∃ y : X, Q y).
Proof.
  intros H.
  destruct H as [z H].
  destruct H as [HP HQ].
  split.
  -
    exists z.
    assumption.
  -
    exists z.
    assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** Comprenez vous pourquoi l'implication inverse n'est pas vraie ?  Vous pouvez
    éventuellement essayer de prouver l'exercice suivant et voir où vous êtes
    bloquées. *)

Lemma exists_and_faux : (∃ x : X, P x) ∧ (∃ y : X, Q y) → (∃ x, P x ∧ Q x).
Proof.
Abort.

(** **** Exercice : Prouver le théorème suivant. *)

Lemma exists_or : (∃ x, P x ∨ Q x) ↔ (∃ x : X, P x) ∨ (∃ y : X, Q y).
Proof.
  split.
  -
    intros HPQ.
    destruct HPQ as [z HPQ].
    destruct HPQ as [HP | HQ].
    *
      left.
      exists z.
      assumption.
    *
      right.
      exists z.
      assumption.
  -
    intros HPQ.
    destruct HPQ.
    *
      destruct H as [y HP].
      exists y.
      left.
      assumption.
    *
      destruct H as [y HQ].
      exists y.
      right.
      assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** **** Exercice : Un raisonnement valide et un autre qui ne l'est pas. *)

Theorem exists_forall {X Y} (R : X → Y → Prop) :
  (∃ x, ∀ y, R x y) → (∀ y, ∃ x, R x y).
Proof.
  intros H.
  destruct H.
  intros y.
  exists x.
  trivial.
Qed. (* Remplacer cette ligne par Qed. *)


(** L'implication inverse est fausse :
    - si une personne est amie avec tout le monde, alors tout le monde a au
      moins un ami ;
    - mais même si tout le monde a au moins un ami, ça ne signifie pas qu'une
      personne est amie avec tout le monde.
*)

End Exists.
