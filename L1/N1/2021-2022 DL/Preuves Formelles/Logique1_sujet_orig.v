(** * Rudiments de logique dans le système coq *)
Require Import Unicode.Utf8.
(** ** La flèche ou implication *)
(** *** Type [Prop] et [→] *)
(** L'objet fondamental du système coq est le _type_ (on dit qu'il est basé sur
    la _théorie des types_).
    Tous les objets que l'on manipule ont un certain type.
    Le type [Prop] est celui des propositions, c'est-à-dire des énoncés
    mathématiques que l'on peut chercher à prouver ou à réfuter.
*)

(** La commande [Check] permet d'imprimer le type d'un terme.  *)
Check 2 + 2 = 4.
Check 2 + 2 = 5.

(** Les énoncés [2 + 2 = 4] et [2 + 2 = 5] sont tous deux de type [Prop], 
    peu importe le fait qu'ils soient prouvables, réfutables (ou même
    indécidables, c'est-à-dire ni prouvables ni réfutables). *)

(** Étant données deux propositions [P] et [Q], on peut en former une nouvelle,
    [P → Q] (lire : "P flèche Q" ou "P implique Q").

   Remarque : la flèche peut être entrée des façons suivantes :
    - avec << -> >> (le caractère '-' puis le caractère '>')
    - directement en unicode : sur beaucoup d'applications sur Linux taper
      Ctrl+Shift+u puis l'unicode, ici 2192 puis espace.
    - sur coqide : taper << \-> >> puis Shift+Espace.
*)

Module PropPlayground1.
(** On ne parlera pas des modules dans ce cours, on les utilise simplement pour
    isoler certaines parties d'un fichier coq
*)
Parameters P Q : Prop.
Check P.
Check Q.
Check P → Q.
Check (P → Q) → Q.
Check P → (Q → P).
Check P → ((Q → P) → Q).
End PropPlayground1.

(** Intuituivement, dans la logique (intuitionniste) de coq,
    la proposition [P → Q] signifie "si [P], alors [Q]" ou, plus précisément,
    "je sais contruire une preuve de Q à partir d'une preuve de P".

    Deux remarques _très importantes_ :
    - La notation usuelle des mathématiques pour "P implique Q" est
    [P ⇒ Q]. Continuez à l'utilisez lors de vos cours plus
    classiques de mathématiques.
    - En logique _classique_ (voir plus loin) et en particulier dans vos cours
    de mathématiques, l'implication est définie par sa table de vérité et a un
    sens un peu différent. Ceci dit, toutes les formules et techniques de
    preuves que nous voyons dans ce cours sont encore valides dans un cours
    "classique" de mathématiques.
*)
(** *** Première preuve *)

(** Nous allons maintenant énoncer et prouver un premier théorème. Exécuter pas
    à pas cette preuve pour bien voir les modifications dans le contexte et le
    but. En coq, les commentaires sont écrits entre (* ... *) *)

Theorem imp_refl : ∀ P : Prop, P → P.
Proof.
  (* Soit P une proposition quelconque. *)
  intros P.
  (* Pour montrer une implication on suppose que ce qui est à gauche est prouvé.
     On doit prouver ce qui est à droite avec cette hypothèse supplémentaire. *)
  (* On suppose (hypothèse (HP)) que P est prouvée. *)
  intros HP.
  (* On doit prouver P. Mais cela fait partie des hypothèses ! *)
  assumption.
Qed. (* Quod erat demonstrandum. Ce qu'il fallait démontrer. *)

(** Sans entrer dans les détails, quelques commentaires s'imposent :
    - [imp_refl] est le nom que nous avons choisi pour ce théorème.
    - La preuve se trouve entre les mots-clés [Proof.] et [Qed.]
    - Les éléments de la preuve [intros] et [assumption] sont appelés des
    _tactiques_ de preuve.
    - Lorsque l'évaluation par coq atteint [Qed.], coq vérifie que la preuve est
       correcte et dans le cas contraire affiche un message d'erreur.
*)

(** *** Plusieurs flèches à la suite *)
(** L'implication n'est pas associative, ce qui veut dire que
    [(P → Q) → R] n'est pas équivalente à [P → (Q → R)], ces deux
    propositions ont des sens très différents.
    En coq, lorsqu'on ne met pas de parenthèses dans une expression avec des
    [→], c'est comme s'il y en avait à
    droite : [P → Q → R] est la même proposition que [P → (Q → R)].
*)

(** **** Exercice : Prouver le théorème suivant. *)

Theorem imp_ex1 : ∀ P Q : Prop, P → (Q → P).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

Theorem imp_ex2 : ∀ P Q : Prop, Q → (P → P).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** Remarque importante :
    Après avoir introduit toutes les variables et toutes les hypothèses dans ces
    deux derniers exercices, les contextes sont exactement les mêmes. En fait,
    plutôt que de penser à [P → Q → R] comme (P implique que (Q implique R)),
    il vaut mieux y voir "si P et Q, alors R", ou encore, "si j'ai une preuve de
    P et une preuve de Q, alors je sais contruire une preuve de R".
*)

(** coq ne va pas vous laisser prouver quelque chose de faux : essayez ! *)

(** **** Exercice : Où êtes-vous bloqué dans cette preuve ? *)
Theorem faux_thm : ∀ P Q : Prop, (P → Q) → P.
Proof.
Abort.

(** Avec de l'argent je peux acheter des gâteaux (ou de la drogue), ça ne veut 
    pas dire que j'ai de l'argent... *)

(** *** Appliquer une implication *)
(** Maintenant qu'on a vu comment _prouver_ (introduire) une implication, on va
  voir comment on _utilise_ (élimine, applique) une implication. *)

Theorem modus_ponens : ∀ P Q : Prop, P → (P → Q) → Q.
Proof. (* Démonstration. *)
  (* Soient P et Q deux propositions quelconques.*)
  intros P Q.
  (* Supposons que P est prouvée ... *)
  intro HP.
  (* ... et que (P → Q) est prouvée. *)
  intro HPimpQ.
  (* Comme (P → Q), pour montrer Q _il suffit_ de montrer P. *)
  apply HPimpQ.
  (* Or, P est vraie par hypothèse. *)
  assumption.
Qed. (* CQFD. *)

(** Revenons sur la tactique [apply].
    Si la formule à prouver est [Q] et qu'une hypothèse [H] a la forme
    [P → Q], alors l'utilisation de [apply H.] entraîne le changement du but en
    [P].
    Ceci est conforme à la règle appelée syllogisme ou _modus ponens_ qui dit
    que de [P → Q] et [P], on peut déduire [Q].
    - Socrate est un homme. ([P])
    - Tous les hommes sont mortels. ([P → Q])
    - Donc Socrate est mortel. ([Q])
*)

(** **** Exercice : Prouver le théorème suivant. *)

Theorem imp_trans : ∀ P Q R : Prop,
  (P → Q) → (Q → R) → (P → R).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant (plus difficile) *)

Theorem weak_peirce : ∀ P Q : Prop,
  ((((P → Q) → P) → P) → Q) → Q.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** *** Les sous-buts *)

(** Il arrive très souvent qu'une tactique crée des _sous-buts_ (en anglais
    _subgoals_), comme dans cette preuve du théorème suivant. *)

Theorem imp3 : ∀ P Q R : Prop,
  (P → Q → R) → P → Q → R.
Proof.
  (* Soient P, Q et R trois propositions quelconques. *)
  intros P Q R.
  (* Supposons :
    (HPQR) : P → Q → R.
    (HP) : P
    (HR) : Q *)
  intros HPQR HP HQ.
  (* D'après (HPQR), pour prouver R il suffit d'avoir une preuve de P et une preuve de Q *)
  apply HPQR.
  (* Remarquer les deux sous-buts ici *)
  (* On peut (mais on n'est pas obligé) utiliser les caractères - + et * pour mettre en évidence 
     le fait qu'on travaille sur un sous-but *)
  - (* Preuve de P *)
    assumption.
  - (* Preuve de Q *)
    assumption.
Qed.

 
(** **** Exercice : Prouver le théorème suivant. *)

Theorem prémisses_non_ordonnées1 : ∀ P Q R : Prop, (P → Q → R) → (Q → P → R).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

Theorem diamond : ∀ P Q R T : Prop,
  (P → Q) → (P → R) → (Q → R → T) → P → T.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	
 
(** *** Appliquer une implication dans une autre hypothèse. *)

(** Jusqu'à présent, nous avons toujours prouvé nos théorèmes en travaillant sur
    le _but_. C'est ce qu'on appelle un raisonnement "vers l'arrière"
    (_backward reasoning_). Ce style de raisonnement est favorisé en coq : par
    défaut, la plupart des tactiques transforment le but avec des raisonnements
    du type "d'après telle hypothèse, si je sais prouver truc (nouveau but),
    alors j'aurai bidule (ancien but)", autrement dit,
    "d'après telle hypothèse, pour
    prouver bidule (ancien but) _il suffit_ de prouver truc (nouveau but)". *)

Theorem imp_trans2 : ∀ P Q R S : Prop,
  (P → Q) → (Q → R) → (R → S) → P → S.
Proof.
  (* Soient P, Q, R et S des propositions quelconques *)
  intros P Q R S.
  (* On suppose prouvées
     (HPQ) : P → Q
     (HQR) : Q → R
     (HRS) : R → S
     (HP)  : P
     et on doit montrer S.*)
  intros HPQ HQR HRS HP.
  (* D'après HPS, pour prouver S il suffit de prouver R. *)
  apply HRS.
  (* D'après HQR, pour prouver R il suffit de prouver Q. *)
  apply HQR.
  (* D'après HPQ, pour prouver Q il suffit de prouver P. *)
  apply HPQ.
  (* Or, P est prouvée par hypothèse. *)
  assumption.
Qed.

(** En mathématiques, on a plutôt l'habitude du raisonnement "vers l'avant" :
    construire pas à pas le but en énonçant de nouveaux faits. Ceci est
    aussi possible en coq. *)

Theorem imp_trans2' : ∀ P Q R S : Prop,
  (P → Q) → (Q → R) → (R → S) → P → S.
Proof.
  (* La preuve commence de la même manière. *)
  intros P Q R S HPQ HQR HRS HP. 
  (* Remarquez qu'on peut introduire en même temps les variables et les
     hypothèses. *)
  (* D'après (HP) et (HPQ), Q est prouvée. *)
  apply HPQ in HP as HQ.
  (* Donc, par HQR, on a aussi une preuve de R. *)
  apply HQR in HQ as HR.
  (* Enfin, par HRS, on obtient une preuve de S. *)
  apply HRS in HR as HS.
  (* S est donc prouvée par hypothèse. *)
  assumption.
Qed.

(** *** Exercice :
    Prouver avec un raisonnement "vers l'avant" les théorèmes [modus_ponens] et
    [imp_trans]. *)

Theorem modus_ponens' : ∀ P Q : Prop, P → (P → Q) → Q.
Proof. (* Démonstration "vers l'avant". *)
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

Theorem imp_trans' : ∀ P Q R : Prop,
  (P → Q) → (Q → R) → (P → R).
Proof. (* Démonstration "vers l'avant". *)
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** ** Négation [¬] *)

(** La négation en coq (en fait en logique intuitionniste) est aussi un peu
    particulière par rapport à la négation
    d'un cours de mathématique usuel. En effet, on ne définit pas la négation 
    d'une proposition par sa table de vérité mais en ajoutant une nouvelle
    proposition appelée [False].
*)

(** *** La proposition [False] *)
Check False.
(** La proposition [False] 
    désigne le faux, l'absurde, souvent notée en mathématiques ⊥
    (symbole _bottom_). Elle est définie par la règle appelée _ex falso quod
    libet_ (du faux, on peut déduire n'importe quoi), aussi appelée _principe
    d'explosion_.
*)

(** Si [False] est dans les hypothèses, alors la tactique [destruct] permet
    simplement de terminer la preuve.
*)
Theorem ex_falso_quod_libet : ∀ P : Prop, False → P.
Proof.
  (* Soit P une proposition quelconque. *)
  intros P.
  (* Supposons (hypothèse (contra)) que False est prouvée. *)
  intros contra.
  (* Explosion : le faux fait partie des hypothèses. *)
  destruct contra.
Qed.

(** **** Exercice : prouver le théorème suivant : *)

Theorem exo_false : ∀ P Q : Prop,
  (P → False) → P → Q.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	
  
(** La tactique [exfalso] remplace le but courant par False. En effet, si l'on prouve False on prouve
    tout de toute façon. *)

Theorem stupide : ∀ P Q : Prop, (P → False) → P → (Q → P → Q).
Proof.
  (* Soient P et Q deux propositions quelconques. *)
  intros P Q.
  (* On suppose
    (HnP) : P → False
    (HP) : P *)
  intros HnP HP.
  (* Comme le False implique n'importe quoi, il suffit de prouver False. *)
  exfalso.
  (* Par HnP il suffit de prouver P. *)
  apply HnP.
  (* Or P est vrai par hypothèse. *)
  assumption.
Qed.
  
(** *** Réfuter une proposition *)
(** En logique intuitionniste, plutôt que de dire qu'une proposition est
    _fausse_, on dit qu'elle est _contradictoire_, ce qui signifie qu'ajouter
    cette proposition dans les hypothèse mène à une explosion : une preuve de
    [False] (et donc, par _ex falso quod libet_ de _toutes_ les propositions).

    La _négation_ d'une proposition P, notée en coq [~P], [¬P] ou encore [not
    P],  
    est en fait simplement une _notation_ pour [P → False].

    Pour entrer le symbole ¬ :
    - Ctrl+Shift+u puis 00ac et Espace
    - ou sur coqide \not puis Shift+Espace.
*)
Check not.
Print not.
Locate "~".
Locate "¬".

(** On peut _déplier_ une notation, c'est-à-dire la remplacer par ce qu'elle
    représente avec la tactique [unfold] (littéralement "déplier").  *)

(* Le prochain théorème dit la chose suivante :
   - si C fait tout exploser
   - et que A → C
   - alors A aussi fait tout exploser. *)

Theorem contraposée : ∀ A C : Prop, (A → C) → (¬C → ¬A).
Proof.
  (* Soient A et C deux propositions quelconques. *)
  intros A C.
  (* On suppose :
    - (HAC) : [A → C]
    - (HnC) : [¬C] *)
  intros HAC HnC.
  (* Dans le but, on remplace ¬A par sa définition, à savoir [A → False]. *)
  unfold not.
  (* On suppose (HA) : A  et on cherche à prouver False. *)
  intro HA.
  (* Par modus ponent appliqué à (HAC) et (HA), on a (HC) : C *)
  apply HAC in HA as HC.
  (* Dans l'hypothèse (HnC), on remplace ¬C par sa définition. *)
  unfold not in HnC.
  (* Par modus ponent appliqué à (HnC) et (HC) on prouve
     (explosion) : False *)
  apply HnC in HC as explosion.
  (* On a donc prouvé le but. *)
  assumption.
  (* [destruct explosion.] aurait marché aussi par ex falso quod libet. *)
Qed.

(** En mathématique, la _contraposée_ de l'implication [A → C] est
    [¬C → ¬A]. On vient de voir que [A → C] implique [¬C → ¬A]. En
    _logique classique (voir plus loin) la réciproque est vraie : une
    implication et sa contraposée sont équivalentes. *)

(** **** Exercice : Prouver le théorème suivant. *)

Theorem absurd : ∀ A C : Prop, A → ¬A → C.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	


(** **** Exercice : Prouver le théorème suivant. *)

Theorem exo_neg : ∀ P Q : Prop,
  (P → Q) → (P → ¬Q) → ¬P.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** *** La double négation *)
(** En logique classique, pour toute proposition A, ¬¬A est équivalente à A.
    En logique intuitionniste, ce n'est pas toujours le cas.
    ¬A signifiant "A est contradictoire", c'est-à-dire "A fait tout exploser",
    ¬¬A signifie "Il est contradictoire d'affirmer que A est contradictoire",
    autrement dit "A est non contradictoire", ou encore "A ne fait pas tout
    exploser".
    ¬¬A est une notation pour (A → False) → False.
*)

(** Une proposition prouvée est toujours non contradictoire. *)

(** **** Exercice : prouver le théorème suivant. *)
Theorem A_imp_NNA : ∀ A : Prop, A → ¬¬A.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice bonus (assez difficile) *)
(* On peut prouver (pour deux propositions données) que le raisonnement par
   contraposition est non contradictoire. *)

Theorem contraposee2 : ∀ A C : Prop,
  ¬¬((¬C → ¬A) → (A → C)).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice bonus (assez difficile) *)

(** Dans la même veine, on a le théorème suivant. *)

Theorem contraposee3 : ∀ A C : Prop,
  (¬C → ¬A) → ¬¬(A → C).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** ** Conjonction [∧] *)

(** Le "et logique" aussi appelé opérateur de conjonction est noté en Coq [∧] ou
    [/\].
    Si [P] et [Q] sont deux propositions, alors [P ∧ Q] est une proposition dont
    une preuve est à la fois une preuve de [P] et une preuve de [Q].

    Pour entrer ∧ :
    - Ctrl + Shift + u puis 2227 et Espace.
    - sur coqide : \and puis Shift + Espace.
*)

(** *** Utiliser une conjonction : [destruct] *)

(** Pour décomposer une preuve de [P ∧ Q] en une preuve de [P] et une preuve de
    [Q], on utilise la tactiques
    [destruct]. *)
Theorem proj1 : ∀ P Q : Prop, P ∧ Q → P.
Proof.
  (* Soient P et Q deux propositions quelconques. *)
  intros P Q.
  (* Supposons
     (HPQ) : P ∧ Q *)
  intros HPQ.
  (* De HPQ on déduit :
      (HP) : P
      (HQ) : Q *)
  destruct HPQ as [HP HQ].
  (* Alors [P] est prouvée par hypothèse. *)
  assumption.
Qed.

(** Noter la syntaxe avec crochets pour [destruct] une proposition conjonctive. *)

(** **** Exercice : Prouver le théorème suivant. *)

Theorem proj2 : ∀ P Q : Prop, P ∧ Q → Q.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** *** Prouver une conjonction : [split] *)

(** Prouver [P ∧ Q] se fait en deux étapes : il faut une preuve de P et une
    preuve de Q. Ceci se fait avec la tactique [split]. *)

Theorem conj : ∀ P Q : Prop, P → Q → P ∧ Q.
Proof.
  (* Soient P et Q deux propositions quelconques. *)
  intros P Q.
  (* Supposons :
      (HP) : P
      (HQ) : Q. *)
  intros HP HQ.
  (* Pour prouver P ∧ Q on doit prouver P puis Q. *)
  split.
  - (* Preuve de P : *)
    (* P est prouvée par hypothèse. *)
    assumption.
  - (* Preuve de Q : *)
    (* Q est prouvée par hypothèse. *)
    assumption.
Qed.

(** **** Exercice : Prouver le théorème suivant. *)

Theorem and_comm1 : ∀ P Q : Prop, P ∧ Q → Q ∧ P.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** Remarque : comme pour  [→] l'opérateur [∧] est "associatif à droite" en coq,
    ce qui signifie que P ∧ Q ∧ R dénote en fait P ∧ (Q ∧ R). Comme on va le
    voir ci-dessous, ceci n'a pas tellement d'importance ici. *)

(** **** Exercice : Prouver le théorème suivant. *)

Theorem and_assoc1 : ∀ P Q R : Prop, (P ∧ Q) ∧ R → P ∧ Q ∧ R.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

Theorem and_assoc2 : ∀ P Q R : Prop, P ∧ Q ∧ R → (P ∧ Q) ∧ R.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** ** Équivalence [↔] *)

(** L'équivalence logique est notée en coq [↔] ou [<->].
    Une preuve de [P ↔ Q] est à la fois une preuve de [P → Q] et une preuve de
    [Q → R].  Les tactiques [destruct] (pour utiliser une équivalence) et
    [split] (pour prouver une équivalence) s'appliquent de la même manière que
    pour la conjonction.

    Pour entrer ↔ :
    - Ctrl + Shift + u puis 2194 et Espace.
    - sur coqide : \harr puis Shift + Espace.
*)

Theorem iff_sym : ∀ P Q : Prop, P ↔ Q → Q ↔ P.
Proof.
  (* Soient P et Q deux propositions quelconques. *)
  intros P Q.
  (* On suppose (H) : P ↔ Q. *)
  intros H.
  (* De (H) on déduit :
     (HPQ) : P → Q
     (HQP) : Q → P *)
  destruct H as [HPQ HQP].
  (* Pour prouver P ↔ Q, il suffit de prouver P → Q et Q → P. *)
  split.
  - (* Preuve de P → Q : *)
    assumption.
  - (* Preuve de Q → P : *)
    assumption.
Qed.

(** **** Exercice : Prouver le théorème suivant. *)

Theorem iff_refl : ∀ P : Prop, P ↔ P.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

Theorem iff_trans : ∀ P Q R : Prop, (P ↔ Q) → (Q ↔ R) → (P ↔ R).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** ** Appliquer un théorème. *)

(** En coq, on peut fournir des _arguments_ à un théorème pour produire des
    formules déjà prouvées. *)
Module ThmPlayground.
(* Module pour ne pas polluer le reste du fichier avec les paramètres A, B et C. *)
Parameters A B C : Prop.
Check and_comm1.
(** On peut _appliquer_ le théorème and_comm1 à deux propositions. *)
Check and_comm1 A B.
(** Ou bien à une seule, l'autre restant universellement quantifiée. *)
Check and_comm1 A.
(** On peut même donner en argument des propositions plus compliquées. *)
Check and_comm1 (A → B) (B → C).
End ThmPlayground.

(** On peut alors utiliser dans des preuves des théorèmes déjà prouvés. *)
Theorem and_comm : ∀ P Q : Prop, (P ∧ Q) ↔ (Q ∧ P).
Proof.
  intros P Q.
  split.
  - apply (and_comm1 P Q).
  - apply (and_comm1 Q P).
Qed.

(** **** Exercice : *)

(** Prouver le théorème suivant en utilisant [prémisses_non_ordonnées1] *)

Theorem prémisses_non_ordonnées : ∀ P Q R : Prop, (P → Q → R) ↔ (Q → P → R).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	
  
(** **** Exercice : *)

(** Prouver le théorème suivant en utilisant [and_assoc1] et {and_assoc2] *)

Theorem and_assoc : ∀ P Q R, (P ∧ Q) ∧ R ↔ P ∧ (Q ∧ R).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** ** Disjonction [∨] *)

(** En coq, le "ou logique", aussi appelée opérateur de disjonction est noté [∨]
    ou [\/].
    Une preuve de [P ∨ Q] est une preuve de P ou une preuve de Q.

    Pour entrer ∨ :
    - Ctrl + Shift + u puis 2228 et Espace.
    - sur coqide : \or puis Shift + Espace.
*)

(** *** Prouver une disjonction : [left] et [right] *)

(** Pour pouvoir prouver une disjonction (par exemple [P ∨ Q]) en logique
    intuitionniste, il faut dire explicitement
    si l'on donne une preuve de l'opérande de gauche (ici [P]) ou de celui de
    droite (ici [Q]).  Ceci se fait avec les tactiques [left] et [right].  *)

Theorem or_introl : ∀ P Q : Prop, P → P ∨ Q.
Proof.
  (* Soient P et Q deux propositions quelconques. *)
  intros P Q.
  (* Supposons
     (HP) : P *)
  intros HP.
  (* Pour prouver P ∨ Q il suffit de prouver P. *)
  left.
  (* Or P est vrai par hypothèse. *)
  assumption.
Qed.

(** **** Exercice : *)

(* Prouver le théorème suivant : *)

Theorem or_intror : ∀ P Q : Prop, Q → P ∨ Q.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : *)

(* Prouver le théorème suivant : *)

Theorem and_imp_or : ∀ P Q : Prop, P ∧ Q → P ∨ Q.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** *** Utiliser une disjonction : [destruct] *)

(** Utiliser une disjonction (par exemple [P ∨ Q] est un peu plus délicat.
    On va alors faire une preuve _par cas_ :
    - Dans un premier temps, on prouve le but sous l'hypothèse que l'opérande de
      gauche (ici [P]) est prouvée.
    - Dans un second temps, on prouve le but sous l'hypothèse que  l'opérande de
      droite (ici [Q]) est prouvée. *)

Theorem or_comm1 : ∀ P Q : Prop, P ∨ Q → Q ∨ P.
Proof.
  (* Soient P et Q deux propositions quelconques. *)
  intros P Q.
  (* Supposons
     (H) : P ∨ Q. *)
  intros H.
  (* De (H) on déduit 
     (HP) : P
     ou
     (HQ) : Q *)
  destruct H as [HP | HQ].
  (* Noter beaucoup de choses ici :
     - la syntaxe avec la barre verticale dans les crochets pour le "ou"
     - il y a maintenant deux sous-buts, un par cas. En fait ce ne sont pas les
       sous-buts qui diffèrent, mais les contextes (hypothèses). *)
  - (* cas 1 : hypothèse (HP) *)
    right.
    assumption.
  - (* cas 2 : hypothèse (HQ) *)
    left.
    assumption.
Qed.

(** **** Exercice : Prouver le théorème suivant. *)

Theorem or_ind : ∀ P Q R: Prop, (P → R) → (Q → R) → P ∨ Q → R.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

Theorem or_assoc : ∀ P Q R: Prop, (P ∨ Q) ∨ R ↔ P ∨ Q ∨ R.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** ** Lois de de Morgan *)
(** Les "lois" de de Morgan sont un résultat très connu (et très utile !) de
    logique classique. Que se passe-t-il en logique intuitionniste ? *)

(** **** Exercice : Prouver le théorème suivant. *)

Theorem de_morgan_not_or : ∀ P Q : Prop, ¬(P ∨ Q) ↔ (¬ P) ∧ (¬ Q).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

Theorem de_morgan_not_or_not : ∀ P Q : Prop, (¬ P) ∨ (¬ Q) → ¬ (P ∧ Q).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** Remarquez qu'on ne vous a demandé qu'une seule implication dans l'exercice
    précédent...
    C'est parce que l'autre n'est tout simplement pas prouvable en logique
    intuitionniste. *)

(** **** Exercice : *)

(** En essayant de prouver le théorème suivant, se convaincre que ce n'est pas possible. *)
Theorem de_morgan_not_and : ∀ P Q : Prop, ¬ (P ∧ Q) → (¬ P) ∨ (¬ Q).
Proof.
  (* Essayez d'aller le plus loin possible dans la preuve. *)
Abort.

(** *** Logique intuitionniste et logique classique *)

(** La différence avec votre cours de mathématiques classiques est que coq n'est
    pas équipé par défaut du
    tiers-exclus. En pratique, cela veut dire pour les inconvénients :
    - pas de preuve par l'absurde
    - pas de raisonnement par contraposition
    - il manque une loi de de Morgan sur les 4
    mais il y a un gros avantage à cela : les preuves sont constructives. *)

(** Peut-on ajouter le tiers-exclus à coq pour prouver les mêmes théorèmes qu'en
    mathématiques ?
    Un théorème difficile montre que si coq est correct (c'est-à-dire ne permet
    pas de prouver quelque chose de faux), alors (coq + tiers-exclus) est aussi
    correct.  On peut donc, si on le souhaite ajouter l'axiome du tiers-exclus.
*)

Axiom excluded_middle : ∀ P : Prop, P ∨ ¬ P.

(** Attention ! Un axiome n'est pas un théorème ! En ajoutant un axiome on prend
    le risque (énorme) que tout s'écroule ! *)

(** Avec cet axiome, on peut prouver, par exemple, que le raisonnement par
    l'absurde est correct. *)

Theorem NNPP : ∀ P : Prop, ¬ ¬ P → P.
Proof.
  (* Soit P une proposition quelconque. *)
  intros P. unfold not.
  (* On suppose (hyp. (H)) que P est non contradictoire. *)
  intros H.
  (* Par le principe du tiers-exclus appliqué à P, on a
     (HP) : P
     ou
     (HnP) : ¬ P *)
  destruct (excluded_middle P) as [HP | HnP].
  - (* cas 1 : (HP) *)
  assumption.
  - (* cas 2 : (HnP) *)
  (* On va ici utiliser le principe d'explosion : ce cas est contradictoire. *)
  exfalso.
  unfold not in HnP.
  apply H.
  assumption.
Qed.

(** **** Exercice : *)

(** En utilisant le tiers-exclus ou l'une de ses conséquences, prouver le
    théorème suivant : *)

Theorem de_morgan_not_and : ∀ P Q : Prop, ¬ (P ∧ Q) → (¬ P) ∨ (¬ Q).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	
  
(** **** Exercice : *)

(** En utilisant le tiers-exclus ou l'une de ses conséquences, prouver le
    théorème suivant : *)

Theorem classical_imp : ∀ P Q : Prop, (P → Q) ↔ (Q ∨ ¬ P).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** Pour terminer cette partie sur la logique classique.
    Il y a bien des théorème qu'on peut démontrer en logique classique et pas en
   logique intuitionniste.  Ceci dit, une preuve dite "constructive",
   c'est-à-dire en logique intuitionniste, lorsqu'elle est possible, apporte
   toujours plus d'informations qu'une preuve non constructive. Les preuves par
   l'absurde peuvent très souvent (même dans les preuves de mathématiciens
   professionnels) être remplacées par des preuves plus simples et plus
   parlantes.  Dans vos études de mathématiques, vous devriez éviter de vous
   jeter dessus. *)

(** ** Conclusion *)
(** Il est assez difficile de se lancer en preuves formelles. Il y a beaucoup,
   beaucoup de choses à assimiler d'un seul coup.
    Le plus dur est sans doute fait. N'hésitez pas à refaire les exercices
   ci-dessus assez souvent. *)
