(** *Ensembles et fonctions *)

Require Import Unicode.Utf8.
Require Import Nat.
Require Import PeanoNat.

(** ** Ensembles et propriétés *)

Module Ensembles.
(** Dans votre cours, une application est définie via un graphe
    d'application, qui lui-même est un ensemble. Ici nous prenons le
    point de vue inverse. L'objet fondamental est la fonction, et un
    ensemble d'élements de type A (i.e., un sous-ensemble de A), est lui
    même une fonction au type bien particulier. *)

(** Un ensemble est défini directement par la propriété qui le
  caractérise. Si A est un type, un ensemble est un objet P de type
  (P : A -> Prop).

  Voici quelques exemples sur les entiers naturels.
*)

(* Ci-dessous une définition de parties finies de l'ensemble des entiers
   naturels notés usuellement {0, 1, 2} et {1, 3, 5}. *)

Definition ens1 (n : nat) :=
  match n with 0 => True | 1 => True | 2 => True | _ => False end.

Definition ens2 (n : nat) :=
  match n with 1 => True | 3 => True | 5 => True | _ => False end.

(** L'appartenance à un ensemble est alors très simple. Nul besoin de
    définit la notation ∈, on se contente de l'application d'une
    fonction. Un élément (a : A) appartient à l'ensemble (E : A -> Prop)
    si (E a) est vraie. *)

(** Le théorème suivant dit que 2 appartient à ens1 mais 42 non. *)

Theorem ens1_1 : (ens1 2) /\ ~(ens1 42).
Proof.
  split.
  - simpl. apply I. (* ou [trivial] *)
  - unfold not. intros H. simpl in H. assumption. (* ou [destruct H] *)
Qed.

(* On peut définir par récurrence des ensembles d'entiers plus compliqués,
   ci-dessous, l'ensemble des entiers pairs. *)

Fixpoint pair n : Prop :=
  match n with
    | 0 => True
    | 1 => False
    | S (S n') => pair n'
  end.
Check pair.

(** La propriété [pair] est une propriété sur les entiers naturels.
    C'est aussi une définition possible de l'ensemble des entiers pairs.

    Comme coq est basée sur la théorie des types,
    au lieu de : "partie d'un ensemble"
    on parle de : "propriété des éléments d'un ensemble"
    Autrement dit, au lieu de manipuler l'ensemble
    { x : nat | x est pair }, on manipule le prédicat [pair] directement. *)

(** **** Exercice : Prouver les théorèmes suivants. *)

Lemma quatre_pair : (pair 4).
Proof.
  simpl.
  trivial.
Qed. (* Remplacer cette ligne par Qed. *)


Lemma cinq_non_pair : ~(pair (3 + 2)).
Proof.
  unfold not.
  simpl.
  trivial.
Qed. (* Remplacer cette ligne par Qed. *)


Lemma n_fois_2_pair : ∀ n,  (pair (n*2)).
Proof.
  intros n.
  induction n as [|n' HI].
  simpl.
  trivial.
  simpl.
  trivial.
Qed. (* Remplacer cette ligne par Qed. *)


(** Nous allons voir comment adapter les
    opérations usuelles de la théorie des parties d'un ensemble dans notre
    cadre. *)

(** L'ensemble A tout entier est le prédicat sur A qui est toujours vrai. *)
Definition ensemble_total {A : Type} (a : A):= True.
Check ensemble_total.

(** La partie vide de A est le prédicat sur A qui est toujours faux. *)
Definition ensemble_vide {A : Type} (a : A) := False.
Check ensemble_vide.

(** Si A est un type (A : Type), on remplace (a ∈ A) par (a : A) *)

(** Tactique : maintenant que l'on pose des *définitions*, et pas
seulements des notations comme pour ¬, la tactique [unfold] est
indispensable pour permettre à coq de déplier la définition. *)

(** **** Exercice : Prouver les théorèmes suivants. *)
Lemma total (A : Type) (a : A) : ensemble_total a.
Proof.
  unfold ensemble_total.
  trivial.
Qed. (* Remplacer cette ligne par Qed. *)


Lemma vide (A : Type) (a : A) : ~ (ensemble_vide a).
Proof.
  unfold ensemble_vide.
  unfold not.
  trivial.
Qed. (* Remplacer cette ligne par Qed. *)


(** Les opérations usuelles sur les ensembles sont donc des opérations
    logiques. Par exemple, l'union de deux ensembles correspond à la
    disjonction des propriétés qui les définissent *)

Definition union {A : Type} (E F : A -> Prop) := fun (a : A) => (E a \/ F a).
Check union.

Theorem exple_union : ((union ens1 ens2) 5) /\ ((union ens1 ens2) 1) /\
  ~ ((union ens1 ens2) 42).
Proof.
  unfold union.
  split.
  - right. simpl. apply I.
  - split.
    + left. (* on a le choix ici *) simpl. apply I.
    + intros [H | H].
      * simpl in H. assumption.
      * simpl in H. assumption.
Qed.

(** L'inclusion est l'implication entre les propriétés *)

Definition incl {A : Type} (E F : A -> Prop) := forall (a : A), (E a -> F a).
Check incl.

Lemma E_incl_EuF {A : Type} (E F : A -> Prop): incl E (union E F).
Proof.
  unfold incl.
  intros Ea.
  intros EEa.
  unfold union.
  left.
  apply EEa.
Qed. (* Remplacer cette ligne par Qed. *)


(** **** Exercice : Les multiples de 4 sont multiples de 2. *)

(** On peut définir l'ensemble des multiples d'un entier k de cette façon : *)
Definition multiple_de (k : nat) (n : nat) := ∃ m, n = k * m.

(** Pour prouver le théorème suivant, on a besoin de l'associativité de la
    multiplication, qui s'appelle Nat.mul_assoc dans la bibliothèque standard.
*)

Search Nat.mul.
Check Nat.mul_assoc.
Theorem mult_2_4 : incl (multiple_de 4) (multiple_de 2).
Proof.
  (* Pour cette preuve, vous aurez besoin de la tactique
     [replace (...) with (...).] Après avoir remplacé un terme du but par un
     autre, coq vous demande une preuve que ces termes sont égaux. Ici, il
     s'agit de remplacer (2 * 2) par 4. *)
  unfold incl.
  intros a.
  unfold multiple_de.
  intros H.
  destruct H.
  exists (2 * x).
  rewrite Nat.mul_assoc.
  replace (2 * 2) with 4.
  apply H.
  reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)


(** **** Exercice : Tous les multiples de 2 ne sont pas multiples de 4. *)

Check Nat.add_comm.
Theorem non_mul_4_2 : ¬ (incl (multiple_de 2) (multiple_de 4)).
Proof.
  (* Vous aurez peut-être besoin de la tactique [assert] (en français affirmer),
     qui ajoute une hypothèse dans le contexte... à condition que vous puissiez
     la prouver. Ici : [assert (E:multiple_de 2 2).] vous demande de prouver que
     2 est bien multiple de 2 et ajoute ce fait au contexte (avec E comme nom).
  *)
  unfold incl.
  assert (E:multiple_de 2 2).
  unfold multiple_de.
  exists 1.
  reflexivity.
  intros H1.
  destruct (H1 2 E).
  destruct x as [|x'].
  simpl in H.
  discriminate.
  simpl in H.
  rewrite Nat.add_comm in H.
  rewrite (Nat.add_comm x' (S (x' + S (x' + 0)))) in H.
  discriminate.
Qed. (* Remplacer cette ligne par Qed. *)


(** L'égalité de deux ensemble correspond à l'équivalence des
propriétés qui les définissent *)

Notation "E <=> F" := (forall a, E a <-> F a)   (at level 80).

Lemma vide_neutre_union {A : Type} (E : A -> Prop) : E <=> (union E (ensemble_vide)).
Proof.
  split.
  -
    intros Ea.
    unfold union.
    unfold ensemble_vide.
    left.
    assumption.
  -
    unfold union.
    unfold ensemble_vide.
    intros H.
    destruct H.
    *
      assumption.
    *
      exfalso.
      assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** A vous de définir l'intersection entre deux ensemble, remplacez
True ci-dessous *)

Definition inter {A : Type} (E F : A -> Prop) := fun (a : A) => E a ∧ F a .

Lemma inter_incl {A : Type} (E F : A -> Prop) : incl (inter E F) F.
Proof.
  unfold incl.
  unfold inter.
  intros a.
  intros EF.
  destruct EF.
  apply H0.
Qed. (* Remplacer cette ligne par Qed. *)

Lemma total_neutre_inter {A : Type} (E : A -> Prop) : E <=> (inter E (ensemble_total)).
Proof.
  split.
  -
    intros Ea.
    unfold inter,ensemble_total.
    split.
    *
      assumption.
    *
      trivial.
  -
    unfold inter,ensemble_total.
    intros EaT.
    destruct EaT as [Ea T].
    assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** A vous de définire le complémentaire d'un ensemble, remplacez True
ci-dessous *)

Definition compl {A : Type} (E : A -> Prop) := fun (a : A) => ¬ E a.
Check compl.

Lemma inter_compl {A : Type} (E F : A -> Prop) : (inter E (compl E)) <=> ensemble_vide.
Proof.
  split.
  -
    unfold inter,compl.
    intros H.
    unfold ensemble_vide.
    destruct H as [Ea HnA].
    apply HnA.
    assumption.
  -
    unfold inter,compl,ensemble_vide.
    intros contra.
    destruct contra.
Qed. (* Remplacer cette ligne par Qed. *)

(** À l'aide de l'axiome excluded-middle, prouver la proposition ci-dessous. *)

Axiom excluded_middle : ∀ P : Prop, P ∨ ¬ P.

Lemma union_compl {A : Type} (E : A -> Prop) : (union E (compl E)) <=> ensemble_total.
Proof.
  split.
  -
    unfold union,compl,ensemble_total.
    intros H.
    trivial.
  -
    unfold union,compl,ensemble_total.
    intros T.
    apply (excluded_middle (E a)).
Qed. (* Remplacer cette ligne par Qed. *)

End Ensembles.

(** ** Composition des fonctions *)

Section Composition.

Definition croissante (f : nat -> nat) := forall n m, (n <= m) -> (f n <=  f m).

Definition decroissante (f : nat -> nat) := forall n m, (n <= m) -> (f n >=  f m).

(** On introduit une notation pour la composition des fonctions *)
Notation "g \o f" := (fun x => g (f x)) (at level 70).

Lemma croi_rond_croi (f g : nat -> nat) : (croissante f) /\ (croissante g) -> croissante (g \o f).
Proof.
  intros fog.
  destruct fog as [cf cg].
  unfold croissante.
  intros n m.
  intros nmm.
  apply cg.
  apply cf.
  assumption.
Qed. (* Remplacer cette ligne par Qed. *)


Lemma dec_rond_dec (f g : nat -> nat) : (decroissante f) /\ (decroissante g) -> croissante (g \o f).
Proof.
  intros fog.
  destruct fog as [cf cg].
  unfold croissante.
  intros n m.
  intros nmm.
  apply cg.
  apply cf.
  assumption.
Qed. (* Remplacer cette ligne par Qed. *)



Lemma dec_rond_croi (f g : nat -> nat) : (decroissante f) /\ (croissante g) -> decroissante (g \o f).
Proof.
  intros fog.
  destruct fog as [cf cg].
  unfold decroissante.
  intros n m.
  intros nmm.
  apply cg.
  apply cf.
  assumption.
Qed. (* Remplacer cette ligne par Qed. *)


End Composition.

(** ** Fonctions et inverses *)

Section Fonctions.
Definition surjective {X Y} (f : X -> Y) := forall (y : Y), exists (x : X), (f x = y).

Definition injective {X Y} (f : X -> Y) := forall x1, forall  x2, (f x1 = f x2 -> x1=x2).

Definition bijective {X Y} (f : X → Y) := (injective f) /\ (surjective f).

(** On commence par quelques exemples simples qui vous permettent de
maitriser les définitions *)

Fixpoint double (n : nat) :=
  match n with
    0 => 0
  | S n' => S (S (double n'))
  end.


Theorem double_inj : injective double.
Proof.
  unfold injective.
  intros x1 x2.
  intros hdd.
  destruct x2 as [|x'].
  -
    simpl in hdd.
Admitted. (* Remplacer cette ligne par Qed. *)



Theorem double_non_surj : ¬ (surjective double).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)



Fixpoint div_par_deux (n : nat) :=
  match n with
    0 => 0
  | 1 => 0
  | S (S n') => S (div_par_deux n')
  end.

Theorem div_par_deux_surj : surjective div_par_deux.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)



Theorem div_par_deux_non_inj : ¬ (injective div_par_deux).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** On vous propose de montrer le fait suivant: f est bijective ssi
tout (y : Y) admet un unique antécédent. Attention, la preuve est plus
longue que ce dont vous avez l'habitude. Nous vous conseillons de bien
structurer le code. *)

(** Le connecteur [exists!] signifie "il existe un unique". C'est une
notation qui se déplie automatiquement quand vous faites la preuve
d'un énoncé, comme ci-dessous.  ? *)

Lemma bij_exuniq {X Y} (f : X → Y) : (bijective f) ↔ (∀ (y : Y), (exists! x, f x = y)) .
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


Definition inverse_gauche {X Y} (g : Y → X) (f : X → Y) := ∀ (x : X), g (f x) = x.

Definition inverse_droite {X Y} (g : Y → X) (f : X → Y) := ∀ x, f (g x) = x.

(** Pour parler facilement de fonctions, on va avoir besoin d'un axiome,
qui transforme les relations en fonctions. On l'appelle l'axiome du
choix, il permet de construire une fonction décrivant la propriété
que doivent vérifier ses valeurs *)

(* from Coq.Logic.ClassicalChoice *)
Axiom choice : ∀ (A B : Type) (R : A→B→Prop),
  (∀ x : A, exists y : B, R x y) →
  exists f : A→B, (∀ x : A, R x (f x)).

Definition graphe {X Y} (f : X → Y) y x := f x = y.

(* Prouvez le lemme ci-dessous *)
Lemma fun_graphe {X Y} f : surjective f -> (∀ y : Y, ∃ x : X, graphe f y x).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** Par exemple, si f est une fonction, (choice Y X (graphe f)) nous
dit que si " (∀ y : Y, ∃ x : X, graphe f y x)" est vérifié, alors on
peut construire une fonction g tel que pour tout y, on ai "graphe f y
g(y)", c'est à dire " f (g y) = y)".  Ici, la relation "R" en
paramètre de l'axiome choice est instanciée par "graphe f", c'est à
dire que y est x sont en relation ssi "f x = y" est vérifié *)


(* La preuve de ce lemme vous demande d'utiliser l'axiome du choix
appliqué au graphe de la bonne fonction. Faites usage de Check pour
vérifier que vous l'appliquez bien, avec de l'utiliser avec un
destruct *)

Lemma surj_inverse_d {X Y} (f : X -> Y) : surjective f -> exists g, inverse_droite g f.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)



Lemma bij_inversible {X Y} (f : X -> Y) : bijective f -> exists g, inverse_droite g f /\ inverse_gauche g f.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


Lemma inversible_bijective {X Y} (f : X -> Y) : (exists g, inverse_droite g f /\ inverse_gauche g f) -> bijective f.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


End Fonctions.
