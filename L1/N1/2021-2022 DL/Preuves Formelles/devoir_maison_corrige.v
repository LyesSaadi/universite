Require Import Unicode.Utf8.
Require Import Nat.
Require Import PeanoNat.
Require Import Unicode.Utf8.
Require Import Reals.
Require Import Lra.
Require Import Lia.
Require Export RealField.

(** * DEVOIR MAISON *)

(** Devoir à rendre avant le 12 novembre 23h59 *)

(** IMPORTANT !! Commencez par remplir les champs ci-dessous pour
    signer la déclaration suivante :

    "Je déclare qu'il s'agit de mon propre travail."

    NOM PRENOM :

    NUMERO ÉTUDIANT : 
*)


(** ** Partie 0 : mettre les choses au clair sur ce devoir. *)

(** **** Exercice : Prouver le théorème suivant. 12 points sur 20.
    Indices : utiliser la tactique [intros] puis la tactique [assumption],
    séparées par le caractère '.' comme ceci :
    [intros. assumption.]
*)

Theorem imp_refl: ∀ P : Prop, P → P.
Proof.
  intros. assumption.
Qed.

(** Maintenant on va pouvoir travailler entre adultes :

    - Le but de ce devoir (et en général de ce module) est d'apprendre
    beaucoup de mathématiques et d'informatique, pas d'avoir une note.
    - En particuler, ça ne sert à rien de copier-coller des solutions
    qui ne seraient pas de vous (en plus ce serait malhonnête).
    - Par contre, demander à un camarade de vous débloquer si vraiment
    vous l'êtes ou de vous expliquer quelque chose est permis, et même
    encouragé, mais dans ce cas, citez-le (ça ne vous enlèvera pas de
    points).
    - Vous avez des niveaux très divers à ce jour et ce devoir est
    vraiment très long. Faîtes ce que vous pouvez (vous pouvez bien
    sûr choisir). Tout ce que vous faîtes là sera très formateur et
    utile pour la suite de vos études.  
    - Amusez-vous !  
*)

(** Il y a 4 partie indépendantes : - Première partie : Composition
des fonctions, - Deuxième partie : La représentation binaire des
entiers, - Troisième partie : Fonctions et inverses, - Quatrième
partie : Suites.

    Du bon usage de votre temps :

    - Avant de faire quoi que ce soit, il vaut mieux être sûr d'être
      au clair sur Logique1.v et Naturels.v
    - La première partie est sans doute la plus simple.
    - La deuxième partie traite des entiers strictements positifs en
      binaire.  Outre le fait de vous faire travailler sur l'écriture
      en binaire des entiers, elle vous fait travailler sur un type
      inductif à trois constructeurs, donc les preuves par induction
      ont 3 étapes.
    - Avant de faire la suite, il vaut mieux être au clair sur les
      quantificateurs (fichier Quantificateurs.v)
    - La troisième partie fait travailler sur les concepts
      extrêmements importants de surjection, injection et
      bijection. Il y a des choses un peu dures et profondes à la fin
      (axiome du choix et relation fonctionnelle).
    - Ensuite, il vaut mieux être au clair sur les réels :
      CorpsOrdonné.v et début de Suites.v, UL_sequence devrait être
      bien compris.
    - Alors, seulement vous pouvez vous attaquer à la quatrième
      partie, qui est sans doute la plus formatrice.

    Au niveau de la notation, encore une fois ça ne devrait pas être ce qui vous
    motive, mais voilà :
    - Pour chaque partie, si vous avez fait une partie des exercices, vous avez
      un point (on décidera au cas par cas si ce que vous avez fait le
      justifie).
    - Si vous avez réussi des choses difficiles dans une partie ça rapporte un
      point supplémentaire.
    - Les choses très bien faites peuvent rapporter des points supplémentaires.
    - La mise en forme soignée des preuves aussi :
      - les lignes devraient être assez courtes (toujours moins de 80
        caractères) ;
      - pas de ligne vide, pas de Check ou Search dans la preuve ;
      - pas de preuve qui fait 20 lignes si elle peut en faire 4 ;
      - des commentaires de temps en temps quand ça aide à comprendre votre
        preuve ;
      - utiliser les "bullets" [-] [+] [*] [--] etc pour séparer les sous-cas et
        les indenter (par exemple avec 2 espaces) ;
      - bref rendre quelque chose qui est joli.
*)

(** Dans tout le devoir, vous pouvez admettre un exercice avec
    [Admitted] et l'utiliser dans le suivant. Les différentes parties
    sont indépendantes. Vous n'avez pas besoin de la fonctions
    [Search] pour la première, deuxième et troisième partie, sauf
    quand c'est demandé explicitement à la fin de la deuxième partie.
*)

(** ** Première partie : Composition des fonctions *)

Section Composition.

Definition croissante (f : nat -> nat) := forall n m, (n <= m) -> (f n <=  f m).

Definition decroissante (f : nat -> nat) := forall n m, (n <= m) -> (f n >=  f m).

(** On introduit une notation pour la composition des fonctions. C'est
comme une définition mais en moins fort, pas besoin de faire [unfold],
coq le fera tout seul.*)

Notation "g \o f" := (fun x => g (f x)) (at level 70).

Lemma croi_rond_croi (f g : nat -> nat) :
  (croissante f) /\ (croissante g) -> croissante (g \o f).
Proof.
unfold croissante; intros [Cf Cg] n m lenm.
apply Cg.
apply Cf.
assumption.
Qed.

Lemma dec_rond_dec (f g : nat -> nat) :
  (decroissante f) /\ (decroissante g) -> croissante (g \o f).
Proof.
unfold decroissante; intros [Df Dg] n m lenm.
apply Dg.
apply Df.
assumption.
Qed.


Lemma dec_rond_croi (f g : nat -> nat) :
  (decroissante f) /\ (croissante g) -> decroissante (g \o f).
Proof.
unfold decroissante; intros [Df Cg] n m lenm.
apply Cg.
apply Df.
assumption.
Qed.

(** Changer la définition de [constante] ci-dessous en vous inspirant
    des définitions précédentes. *)
Definition constante (f : nat -> nat) :=
  forall n m : nat, f(n) = f(m).

(** Pour prouver le lemme suivant, utiliser l'antisymétrie de la relation <= 
    et le fait qu'on a toujours n <= m ou m <= n. *)
Check Nat.le_antisymm.
Check Nat.le_ge_cases.
Lemma croi_et_dec_const (f : nat -> nat) :
  (croissante f) /\ (decroissante f) -> constante f.
Proof.
  unfold croissante, decroissante, constante.
  intros [Hc Hd] n m.
  apply Nat.le_antisymm; destruct (Nat.le_ge_cases n m).
  - apply Hc; assumption.
  - apply Hd; assumption.
  - apply Hd; assumption.
  - apply Hc; assumption.
Qed.

End Composition.

(** ** Deuxième partie : La représentation binaire des entiers *)
Module Positive.
(** 
    Vous êtes (trop) habitué(e)s à la représentation décimale des
    nombres.  Cette représentation utilise la base 10 et les 10
    chiffres décimaux que sont
    0, 1, 2, 3, 4, 5, 6, 7, 8 et 9.
    Un entier qui s'écrit en décimal 345 vaut 3 centaines, 4 dizaines
    et 5 unités, soit

    3 * 10^2 + 4 * 10^1 + 5 * 1.

    En binaire, ou base 2, il n'y a que deux chiffres : 0 et 1, qu'on
    appelle aussi _bits_ pour _binary digits_ c'est-à-dire chiffres
    binaires.

    Le nombre écrit en binaire 1101 vaut 1 * 2^3 + 1 * 2^2 + 0 * 2^1 +
    1, c'est-à-dire 13 en décimal.

    Pour plus d'informations, voir
    https://fr.wikipedia.org/wiki/Syst%C3%A8me_binaire

    Dans ce sujet, nous donnons une définition alternative des entiers
    strictement positifs en binaire. Un des intérêts est que
    l'écriture des nombres est beaucoup plus courte. Comparer 1101
    avec S(S(S(S(S(S(S(S(S(S(S(S(S O))))))))))))). Les évaluations
    faites par coq seront aussi beaucoup plus rapides.  *)

(** Un élément de type positive est soit :
    - le nombre 1
    - le résultat d'ajouter un 0 à la fin d'un positive
    - le résultat d'ajouter un 1 à la fin d'un positive
*)

Inductive positive : Set :=
  | xI : positive -> positive (* mettre un 1 à la fin *)
  | xO : positive -> positive (* mettre un 0 à la fin *)
  | xH : positive. (* l'entier 1 *)

(* Notations pour que ce soit beaucoup plus pratique. *)
Declare Scope positive.
Notation "1" := xH
  (at level 6) : positive.
Notation "p ~ 1" := (xI p)
 (at level 7, left associativity, format "p '~' '1'") : positive.
Notation "p ~ 0" := (xO p)
 (at level 7, left associativity, format "p '~' '0'") : positive.
Local Open Scope positive.

Check 1~1~0. (* en décimal, vaut 6 *)
Check 1~1~0~1. (* en décimal, vaut 13 *)

(** **** Exercice : convertir un entier binaire positif en nat

    Rendre la définition suivante correcte (changer les deux derniers cas).
    Les tests suivants (sous la forme de théorèmes) devraient être
    automatiquement prouvés. *)

(* Parce que le niveau 6 de priorité affectée à la notation "1"
   ci-dessus est très forte, coq ne veut plus entendre parler du
   naturel 1, c'est celui de positive qui prend le dessus, donc on
   utilise S. *)

Fixpoint nat_of_positive (p : positive) :=
  match p with
    1 => S 0
  | p' ~ 0 => 2 * (nat_of_positive p')
  | p' ~ 1 => S(2 * (nat_of_positive p'))
  end.

Theorem test_nop1: (nat_of_positive 1~1~0) = 6.
Proof.
  simpl. reflexivity.
Qed.
Theorem test_nop2: (nat_of_positive 1~1~1~1) = 15.
Proof.
  simpl. reflexivity.
Qed.
Theorem test_nop3: (nat_of_positive 1) = S 0.
Proof.
  simpl. reflexivity.
Qed.

(** **** Exercice : Prouver le théorème suivant. *)

(** Pour vous aider : *)
Check Nat.eq_add_0.

Theorem positive_not_0 : forall p : positive, ~ (nat_of_positive p = 0).
Proof.
  (* Il y a 3 constructeurs, donc les preuves par induction se font en trois
     étapes. *)
  induction p as [p' IHp'|p' IHp'|].
  - (* Cas p = p'~0 *)
      intros H. discriminate.
  - (* Cas p = p'~1 *)
    simpl. intros H. 
    apply Nat.eq_add_0 in H as H'.
    destruct H' as [H'1 H'2].
    apply IHp'.
    assumption.
  - (* Cas p = 1 *)
    simpl. intros H. discriminate.
Qed.

(** **** Exercice : définition du successeur.

    Rendre la définition du successeur correct, changer tous les cas. Les tests
    suivants (sous la forme de théorèmes) devraient passer. *)

Fixpoint succ x :=
  match x with
    | p~1 => (succ p)~0
    | p~0 => p~1
    | 1 => 1~0
  end.

Theorem test_succ1 : nat_of_positive (succ (succ (succ (succ (succ 1))))) = 6.
Proof.
simpl. reflexivity.
Qed.
Theorem test_succ2 : nat_of_positive (succ 1) = 2.
Proof.
simpl. reflexivity.
Qed.
Theorem test_succ3 : nat_of_positive (succ (succ 1)) = 3.
Proof.
simpl. reflexivity.
Qed.

(** **** Exercice : prouver le théorème suivant.

    Vous ferez une preuve par cas avec destruct.
    Rappelez-vous que [discriminate] cherche dans les hypothèses des égalités
    contradictoires entre constructeurs.
*)

Lemma succ_discr : forall p : positive, p <> succ p.
Proof.
  intros p H.
  destruct p as [p'|p'|].
  - simpl in H. discriminate.
  - simpl in H. discriminate.
  - simpl in H. discriminate.
Qed.


(** Pour la preuve du théorème suivant, vous aurez sans doute envie d'utiliser la
    tactique [f_equal]. Cette tactique très simple, dit juste que pour prouver une
    égalité du type f(a) = f(b), où f est une fonction quelconque, _il suffit_
    de prouver que a = b.

    Exemple trivial : *)

Lemma exple_f_equal : forall p q : positive, p = q → succ(p) = succ(q).
Proof.
  (* Soient p et q deux positives. *)
  intros p q.
  (* On suppose (hyp. H) que p = q. *)
  intros H.
  (* Pour montrer que succ(p) = succ(q), il suffit de prouver que p = q. *)
  f_equal.
  (* Or, ceci est vrai par hypothèse. *)
  assumption.
Qed.

(** Autre rappel, pour une égalité entre termes construits avec le
    même constructeur dans une hypothèse H, utiliser [injection H] ou
    [injection H as H'] permet de simplifier par ce constructeur.

    Exemple trivial : *)
Lemma exple_injection : forall p q : positive, p~1 = q~1 → p = q.
Proof.
  intros p q.
  intros H.
  (* Par injectivité de l'application qui à p associe p ~ 1, on a, d'après
     l'hypothèse H, que p = q. *)
  injection H as H'.  
  assumption.
Qed.

(** **** Exercice : Prouver le théorème suivant.

    C'est assez long et un peu plus dur qu'on pourrait le penser.
*)

Lemma succ_inj : forall p q : positive, succ p = succ q -> p = q.
Proof.
  induction p as [p'|p'|].
  - intros q. simpl.
    destruct q as [q'|q'|].
    + simpl. intros H.
      injection H as H'.
      f_equal.
      apply IHp'. assumption.
    + simpl. discriminate.
    + simpl. intros H. injection H as H'.
      exfalso.
      destruct p'; simpl in H'; discriminate.
  - intros q. destruct q as [q'|q'|]; simpl; intros H; try discriminate.
    injection H as H'. f_equal.
    apply IHp'. f_equal. assumption.
  - intros q. destruct q as [q'|q'|]; simpl.
    + intros H. destruct q'; simpl; discriminate.
    + intros H. discriminate.
    + intros H. reflexivity.
Qed.

(** *** Partie bonus : les tacticielles.

    La preuve précédente comporte de nombreuses redites. Le système coq comporte
    un système de _tacticielles_ (en anglais tacticals) permettant justement
    d'éviter de dire la même chose plein de fois.

    Vous pouvez voir les plus fréquentes sur cette _Cheatsheet_ (à la toute
    fin) : en particulier, regarder le point-virgule [;] et [try]
    https://www.cs.cornell.edu/courses/cs3110/2018sp/a5/coq-tactics-cheatsheet.html
*)

(** **** Exercice bonus : essayer de rendre la preuve précédente
    aussi courte que possible en utilisant des tacticielles. *)

Lemma succ_inj' : forall p q : positive, succ p = succ q -> p = q.
Proof.
  (* On raisonne par induction sur p et par cas sur q.
     Il y a donc 9 cas, 4 d'entre eux étant immédiatement contradictoires et l'un d'entre eux étant évident. *)
  induction p as [p' IHp'|p' IHp'|]; intros q; destruct q as [q'|q'|];
      simpl; intros H; try discriminate; try reflexivity.
  (* Dans tous les cas restants, on peut simplifier H. *)
  all: injection H as H.
  (* Ce qui révèle deux nouveaux cas contradictoires. *)
  2,4: destruct p'|| destruct q'; simpl in H; discriminate.
  - f_equal. now apply IHp'.
  - now f_equal.
Qed.

(** **** Exercice : Prouver le théorème suivant.

    Vous pouvez faire votre petit marché dans les théorème déjà prouvés sur les
    nat de la bibliothèque standard : *)

Search Nat.add.

Theorem succ_S : forall p : positive,
  nat_of_positive (succ p) = S (nat_of_positive p).
Proof.
  induction p; simpl; try reflexivity.
  rewrite IHp. simpl. f_equal.
  rewrite Nat.add_0_r.
  rewrite Nat.add_succ_r.
  reflexivity.
Qed.
End Positive.
  
(** ** Troisième partie : surjections, injections et bijections *)

Section Fonctions.
Definition surjective {X Y} (f : X -> Y) := forall (y : Y), exists (x : X), (f x = y).

Definition injective {X Y} (f : X -> Y) := forall x1, forall  x2, (f x1 = f x2 -> x1=x2).

Definition bijective {X Y} (f : X → Y) := (injective f) /\ (surjective f).

(** On commence par quelques exemples simples qui vous permettent de
maitriser les définitions *)

Fixpoint double (n : nat) :=
  match n with
    0 => 0
  | S n' => S (S (double n'))
  end.


Theorem double_inj : injective double.
Proof.
  unfold injective.
  induction x1 as [|x1' IH]; simpl; intros x2 H; destruct x2 as [|x2'].
  - reflexivity.
  - simpl in H. discriminate.
  - simpl in H. discriminate.
  - f_equal.
    apply IH.
    simpl in H. injection H as H. assumption.
Qed.


Theorem double_non_surj : ¬ (surjective double).
Proof.
  unfold surjective, not.
  intros H.
  destruct (H 1) as [x_faux H_faux].
  destruct x_faux as [|x_faux_pred]; simpl in H_faux; discriminate.
Qed.


Fixpoint div_par_deux (n : nat) :=
  match n with
    0 => 0
  | 1 => 0
  | S (S n') => S (div_par_deux n')
  end.

Theorem div_par_deux_surj : surjective div_par_deux.
Proof.
  unfold surjective.
  induction y.
  - exists 0. simpl. reflexivity.
  - destruct (IHy) as [x0 H0].
    exists (S(S x0)).
    simpl. f_equal. assumption.
Qed.


Theorem div_par_deux_non_inj : ¬ (injective div_par_deux).
Proof.
  unfold not, injective.
  intros H.
  assert (E:div_par_deux 0 = div_par_deux 1). {
    simpl. reflexivity.
  }
  apply H in E.
  discriminate.
Qed.

(** On vous propose de montrer le fait suivant: f est bijective ssi
tout (y : Y) admet un unique antécédent.
Attention, la preuve est un peu longue. Nous vous conseillons de bien
structurer le code. *)

(** Le connecteur [exists!] signifie "il existe un unique". C'est une
notation qui se déplie automatiquement quand vous faites la preuve
d'un énoncé, comme ci-dessous.  *)

Lemma bij_exuniq {X Y} (f : X → Y) : (bijective f) ↔ (∀ (y : Y), (exists! x, f x = y)) .
Proof.
split.
- unfold bijective; unfold injective; unfold surjective.
  intros [H1 H2] y; destruct (H2 y).
  exists x.
  unfold unique; split; trivial.
  intros x' H'; apply H1.
  rewrite H'; trivial.
- unfold unique; intros H; unfold bijective.
  unfold injective; unfold surjective; split.
  + intros x1 x2.
    destruct (H (f x2)); destruct H0; intro H2.
    pose (H3 := (H1 x1 H2)).
    rewrite <- H3.
    apply H1; reflexivity.
  + intro y;  destruct (H y) as [x [H1 H2]].
    exists x; trivial.
Qed.


Definition inverse_gauche {X Y} (g : Y → X) (f : X → Y) := ∀ (x : X), g (f x) = x.

Definition inverse_droite {X Y} (g : Y → X) (f : X → Y) := ∀ x, f (g x) = x.

(** Pour parler facilement de fonctions, on va avoir besoin d'un axiome,
qui transforme les relations en fonctions. On l'appelle l'axiome du
choix, il permet de construire une fonction décrivant la propriété
que doivent vérifier ses valeurs *)

(* from Coq.Logic.ClassicalChoice *)
Axiom choice : ∀ (A B : Type) (R : A→B→Prop),
  (∀ x : A, exists y : B, R x y) →
  exists f : A→B, (∀ x : A, R x (f x)).

Definition graphe {X Y} (f : X → Y) y x := f x = y.

(* Prouvez le lemme ci-dessous *)
Lemma fun_graphe {X Y} f : surjective f -> (∀ y : Y, ∃ x : X, graphe f y x).
Proof.
unfold surjective; unfold graphe.
intros H y.
destruct (H y) as [x Hx].
exists x; assumption.
Qed.

(** Par exemple, si f est une fonction, (choice Y X (graphe f)) nous
dit que si " (∀ y : Y, ∃ x : X, graphe f y x)" est vérifié, alors on
peut construire une fonction g tel que pour tout y, on ai "graphe f y
g(y)", c'est à dire " f (g y) = y)".  Ici, la relation "R" en
paramètre de l'axiome choice est instanciée par "graphe f", c'est à
dire que y est x sont en relation ssi "f x = y" est vérifié *)


(** La preuve de ce lemme vous demande d'utiliser l'axiome du choix
appliqué au graphe de la bonne fonction. Faites usage de Check pour
vérifier que vous l'appliquez bien, avec de l'utiliser avec un
destruct. C'est un peu plus difficile, n'hésitez pas à faire les
autres et à revenir dessus plus tard. *)

Lemma surj_inverse_d {X Y} (f : X -> Y) :
  surjective f -> exists g, inverse_droite g f.
Proof.
unfold surjective; unfold inverse_droite.
intro H.
destruct ((choice Y X (graphe f) (fun_graphe f H))) as [g H'].
unfold graphe in H'.
exists g; trivial.
Qed.


Lemma bij_inversible {X Y} (f : X -> Y) :
  bijective f -> exists g, inverse_droite g f /\ inverse_gauche g f.
Proof.
unfold bijective; unfold injective; unfold inverse_gauche.
intros [H1 H2].
destruct (surj_inverse_d f H2) as [g H'].
exists g; split; trivial.
intro x.
unfold inverse_droite in H'.
apply (H1 (g (f x)) x).
rewrite (H' ( f x)).
trivial.
Qed.

Lemma inversible_bijective {X Y} (f : X -> Y) :
  (exists g, inverse_droite g f /\ inverse_gauche g f) -> bijective f.
Proof.
unfold bijective; unfold injective; unfold surjective.
unfold inverse_droite; unfold inverse_gauche.
intros [g [H1 H2]].
split.
- intros x1 x2 H.
  rewrite <- H2; rewrite <- (H2 x1); rewrite H; trivial.
- intro y; exists (g y); trivial.
Qed.

End Fonctions.

Module Suites.

Open Scope R_scope.
(** En vous inspirant de la preuve de UL_sequence, prouvez
    que la somme de deux suites convergentes converge vers la somme des limites.
 *)

Theorem CV_plus (An Bn : nat -> R) (l1 l2 : R) :
  let Cn := (fun n => An n + Bn n) in
  Un_cv An l1 -> Un_cv Bn l2 -> Un_cv Cn (l1 + l2).
Proof.
  unfold Un_cv.
  intros HA HB eps Heps.
  assert (Heps2 : eps / 2 > 0) by lra.
  destruct (HA (eps / 2) Heps2) as [n1 Hn1].
  destruct (HB (eps / 2) Heps2) as [n2 Hn2].
  pose (n3 := (max n1 n2)).
  exists n3.
  intros n Hn.
  assert (Hdistl1: R_dist (An n) l1 < eps / 2). {
    apply Hn1.
    apply le_trans with (m:=n3).
    - apply Nat.le_max_l.
    - assumption.
  }
  assert (Hdistl2: R_dist (Bn n) l2 < eps / 2). {
    apply Hn2.
    apply le_trans with (m:=n3).
    - apply Nat.le_max_r.
    - assumption.
  }
  unfold R_dist.
  replace (An n + Bn n - (l1 + l2)) with ((An n - l1) + (Bn n - l2)) by lra.
  apply Rle_lt_trans with
    (r2 := Rabs (An n - l1) + Rabs (Bn n - l2)).
  - apply Rabs_triang.
  - replace eps with (eps/2 + eps/2) by lra.
    now apply Rplus_lt_compat.
Qed.

(** Maintenant, à vous de faire la preuve qu'en multipliant une suite
    convergente vers l par une constante lambda, on obtient une suite qui
    converge vers lambda * l.
    Attention, il y a deux cas lambda = 0 ou non.
    Faire un [destruct Req_dec lambda 0] pour séparer ces deux cas.
    Quelques lemmes, non rencontrés jusqu'à maintenant que nous avons utilisé :
*)
Check Req_dec.
Check R_dist_eq.
Check Rabs_pos_lt.
Check Rdiv_lt_0_compat.
Check Rmult_lt_compat_l.
Check R_dist_mult_l.
(** Ça ne veut pas dire que ce soit indispensable de les utiliser (il y a
    de nombreuses façons de faire). Assurez-vous, avant de vous lancer dans la
    preuve formelle de savoir le faire en maths. *)
Lemma CV_mult_const (Un : nat → R) (lambda : R) (l : R) :
  let Vn := fun n => lambda * Un n in
  Un_cv Un l → Un_cv Vn (lambda * l).
Proof.
  
  unfold Un_cv. intros HUn eps Heps.
  destruct (Req_dec lambda 0) as [H0 | Hn0].
  - (* lambda = 0 *)
    exists 0%nat. intros n _. rewrite H0. repeat rewrite Rmult_0_l.
    rewrite R_dist_eq. assumption.
  - (* lambda <> 0 *)
    assert (E : Rabs lambda > 0). {
      apply Rabs_pos_lt; assumption.
    }
    assert (Epos : eps / (Rabs lambda) > 0). {
      apply Rdiv_lt_0_compat; assumption.
    }
    destruct (HUn (eps / (Rabs lambda)) Epos) as [N HN].
    exists N.
    intros n Hn.
    rewrite R_dist_mult_l.
    replace eps with ((Rabs lambda) * (eps / (Rabs lambda))).
    apply Rmult_lt_compat_l.
    + assumption.
    + apply HN. assumption.
    + field. lra.
Qed.

(** Pour se reposer un peu, on peut prouver ce petit lemme, qui peut servir ! *)
Theorem CV_An_minus_a_0 (An : nat -> R) (a : R) :
  let Cn (n : nat) := (An n - a) in
  Un_cv An a <-> Un_cv Cn 0.
Proof.
  unfold Un_cv, R_dist.
  split; intros H eps Heps; destruct (H eps Heps) as [N HN]; exists N; intros n Hn.
  - replace (An n - a - 0) with (An n - a) by lra. now apply HN.
  - replace (An n - a) with (An n - a - 0) by lra. now apply HN.
Qed.

(** On va maintenant vers un résultat très important : les suites convergences
    sont bornées. Pour ce faire on a besoin de l'opération suivante : *)
Fixpoint running_max (Un : nat → R) (n : nat) :=
  match n with
    0%nat => (Un 0%nat)
  | S(n) => Rmax (running_max Un n) (Un (S n))
  end.
(** En français, running_max Un n est le maximum des n + 1 premiers termes de la
    suite Un. *)

Lemma Un_le_running_max (Un : nat → R) (n : nat) :
  forall i, (i <= n)%nat → Un i <= (running_max Un n).
Proof.
(* N'oubliez pas que sur les entiers nat, l'induction existe! *)
  induction n as [|n' IHn'].
  - intros i Hi.
    simpl.
    assert (E: i = 0%nat) by lia.
    rewrite E. lra.
  - intros i Hi.
    assert (E: i = (S n') \/ (i <= n')%nat) by lia.
    destruct E as [Heq | Hle].
    + simpl. rewrite Heq. apply Rmax_r.
    + simpl. apply Rle_trans with (r2 := running_max Un n').
      * now apply IHn'.
      * apply Rmax_l.
Qed.

(** On a maintenant ce qu'il faut pour le montrer. Assurez-vous encore de savoir
    écrire la preuve mathématique avant de vous lancer. *)

Theorem CV_impl_bounded (Un : nat -> R) (l : R) :
  Un_cv Un l -> exists m : R, 0 < m /\ (forall n : nat, Rabs (Un n) <= m).
Proof.
  unfold Un_cv.
  intros H.
  destruct (H 1) as [n0 Hn0].
  - lra.
  - pose (AUn := fun n => Rabs (Un n)).
    exists (Rmax (Rabs l + 1) (running_max AUn n0)).
    split.
    apply Rlt_le_trans with (r2 := 1).
    + lra.
    + apply Rle_trans with (r2 := Rabs l + 1).
      * pose proof (Rabs_pos l); lra.
      * apply Rmax_l.
    + intros n.
      destruct (le_lt_dec n n0) as [H1 | H2].
      * apply Rle_trans with (r2 := running_max AUn n0).
        -- now apply (Un_le_running_max AUn n0 n).
        -- apply Rmax_r.
      * apply Rle_trans with (r2 := Rabs l + 1).
        -- assert (E:(n >= n0)%nat) by lia.
           apply Hn0 in E.
           replace (Un n) with ((Un n) - l + l) by lra.
           apply Rle_trans with (r2 := Rabs (Un n - l) + Rabs l).
           ++ apply Rabs_triang.
           ++ unfold R_dist in E. lra.
        -- apply Rmax_l.
Qed.

(** **Un exemple de suite qui diverge vers l'infini
    
    Il n'y a pas que les suites convergentes dans la vie. Il y a aussi
    - des suites qui divergent vers + l'infini;
    - des suites qui divergent vers - l'infini;
    - des suites qui divergent tout court (n'admettent ni limite finie ni limite
      infinie).

    On va ici prouver la suite définir par Un = n pour tout n diverge vers
    l'infini.
    D'abord la définition, dans cette bibliothèque, cela s'écrit [cv_infty].
*)
Print cv_infty.

(** En mathématiques, on fait souvent (et pour de bonnes raisons) l'abus que
    l'ensemble des naturels est inclus dans l'ensemble des réels. En coq, ce
    n'est pas possible : nat et R sont deux types différents.
    La fonction INR (pour injection de N dans R) permet de passer du naturel n
    au réel 1 + 1 + ... + 1 (n fois). *)
Print INR.

(** Nous allons prouver que la suite INR diverge vers l'infini. Pour ceci, nous
    aurons besoin de la propriété suivante des réels : ils forment un corps
    archimédien : *)
Axiom archimed' :
  forall eps A : R, eps > 0 -> A > 0 -> exists n : nat, (INR n) * eps > A.
(** Cet axiome dit en substance que
    - pour un eps > 0 aussi petit qu'on veut;
    - pour un A > 0 aussi grand qu'on veut;
    On peut toujours trouver un entier naturel n tel que n * eps est plus grand
    que A.
    
    Remarque : l'axiome [archimed] de cette bibliothèque est différent, car plus
    puissant, mais plus difficile, dans un premier temps, à utiliser.
*)

(** À vous maintenant, vous aurez à
    [destruct (archimed' preuve1 preuve2 val_eps val_A)]
    avec :
    - val_eps : ce que vous avez choisi comme eps (n'allez pas chercher trop
    loin...)
    - val_A : ce que vous avez choisi comme A
    - preuve1 : une preuve que votre eps > 0
    - preuve2 : une preuve que votre A > 0

    Petit point embêtant : comme il n'y a pas d'hypothèse sur M dans
    cv_infty, il faut gérer les cas M < 0 et M = 0 (qui pourtant ne servent à
    rien dans la preuve), [lra] est votre ami.

    Enfin, vous aurez sans doute besoin de résultats sur INR.
*)
Search "INR".
Check le_INR.

Theorem n_to_infty :
  let Un n := INR n in cv_infty Un.
Proof.
  unfold cv_infty. intros M.
  destruct (total_order_T M 0) as [H | H].
  - (* cas qui ne servent à rien : M = 0 ou M < 0 *)
    exists 1%nat.
    + intros n Hn.
      apply Rlt_le_trans with (r2 := 1).
      * destruct H; lra.
      * replace 1 with (INR 1%nat) by reflexivity.
        now apply le_INR.
  - destruct (archimed' 1 M Rlt_0_1 H) as [N HN].
    exists N.
    intros n Hn.
    apply Rlt_le_trans with (r2 := INR N).
    + lra.
    + now apply le_INR.
Qed.

(** archimed' n'est pas définit dans la stdlib. Vous pouvez utiliser archimed de 
    la stdlib, mais c'est plus difficile (passages de N à Z à R). Pas exemple,
    si vous souhaitez vous entrainer un peu plus (optionnel), vous pouvez
    démontrer le lemme suivant (avec archimed ou archimed'): *)

Lemma cv_speed_1_Sn :
  Un_cv (fun n:nat => / INR (S n)) 0.
Proof.
(* On utilise le fait que si une suite Un tend vers +infini alors 
la suite 1/Un tend vers 0 *)
(* On peut utiliser le lemme cv_infty_cv_R0 défini dans la stdlib. *)
(* ... ici ... *)
(* Dans notre cas, il faudra par la ensuite montrer que (S n) tend vers
+infini. *)
(* Une suite Un tend vers +infini si:
∀ M : R, ∃ N : nat, ∀ n : nat, N ≤ n → M < Un n *)
(* Pour cela, il faut considérer 3 cas selon que M est négatif, égal à 0 
ou positif *)
(* On peut utiliser le lemme total_order_T défini dans la stdlib. *)
(* ... ici ... *)
(* Cas M < 0: N = 0 et vous pourrez utiliser Rlt_trans, lt_INR_0 et lt_O_Sn. *)
(* Cas M = 0: N = 0 également. *)
(* Cas M > 0: N = l'entier supérieur à M. 
Vous pourrez utiliser up, archimed (ou archimed'), et les lemmes d'injection 
de N->R, Z->R, etc.*)
(* ... ici ... *)
apply (cv_infty_cv_R0 (fun n:nat => INR (S n))).
  intro n; apply not_O_INR; discriminate.
  unfold cv_infty. intro M.
    destruct (total_order_T M 0) as [[Hlt|Heq]|Hgt].
  exists 0%nat; intros n H.
  apply Rlt_trans with 0; [ assumption | apply lt_INR_0; apply lt_O_Sn ].
  exists 0%nat; intros n H; rewrite Heq; apply lt_INR_0; apply lt_O_Sn.
  set (M_z := up M).
  assert (H10 := archimed M).
  cut (0 <= M_z)%Z.
  intro H11; elim (IZN _ H11); intros M_nat H12.
  exists M_nat; intros.
  apply Rlt_le_trans with (IZR M_z).
  elim H10; intros H0 H1; assumption.
  rewrite H12; rewrite <- INR_IZR_INZ; apply le_INR.
  apply le_trans with n; [ assumption | apply le_n_Sn ].
  apply le_IZR; left; simpl; unfold M_z;
    apply Rlt_trans with M; [ assumption | elim H10; intros; assumption ].
Qed.


(** **Majorant, minorant, borne supérieure et borne inférieure *)

(** Rappel : en coq, une partie de R est une fonction de R vers Prop.
    Un majorant (en anglais upper bound) est une valeur qui est supérieure ou
    égale à tous les éléments de l'ensemble. *)
Print is_upper_bound. (* en français : est majorant de *)
(** On rappelle qu'il faut ici comprendre (E x) comme "x appartient à E", donc
    [is_upper_bound E m] dit que pour tout x appartenant à E, x <= m. *)

(** Un minorant d'une partie est défini de la même manière. *)
Definition is_lower_bound (E : R -> Prop) (l : R) :=
  ∀ x : R, E x → l <= x.

(** On va montrer que l'ensembles des inverses des entiers naturels est minoré
    par 0. *)
Definition ens_inverses (x:R) := exists n : nat, x = / ((INR n) + 1).

Check pos_INR.
Theorem inverses_min : is_lower_bound ens_inverses 0.
Proof.
  unfold is_lower_bound, ens_inverses.
  intros x [n H]. rewrite H.
  pose (E:=pos_INR n).
  unfold Rle. left.
  apply Rinv_0_lt_compat. lra.
Qed.

(** La borne supérieure d'un ensemble, lorsqu'elle existe est le plus petit
    majorant de cet ensemble (en anglais least upper bound), ici, [is_lub]. *)
Print is_lub.

(** Même chose pour la borne inférieure (en anglais greatest lower bound). *)
Definition is_glb (E : R -> Prop) (l : R) :=
  is_lower_bound E l ∧ (∀ b : R, is_lower_bound E b → b <= l).

(** À vous, maintenant, prouvez que la borne inférieure des inverses des entiers
    naturels est 0. *)
Theorem inverses_glb : is_glb ens_inverses 0.
Proof.
  unfold is_glb, ens_inverses.
  split.
  - apply inverses_min.
  - unfold is_lower_bound; intros b H.
    destruct (Rle_lt_dec b 0).
    + assumption.
    + exfalso.
      assert (fact : / b > 0). {
        apply Rinv_0_lt_compat. assumption.
      }
      destruct (archimed' 1 (/b) Rlt_0_1 fact) as [N HN].
      assert (contra : b <= / (INR N + 1)). {
        apply H. exists N. reflexivity.
      }
      apply Rle_not_lt in contra. apply contra.
      replace b with (/ / b) by (field; lra).
      apply Rinv_lt_contravar.
      * apply Rmult_lt_0_compat; lra.
      * lra.
Qed.

(** **Produit de deux suites convergentes. *)


(** On commence par le cas où l'une des suites converge vers 0.
    N'hésitez pas, si vous en éprouver le besoin à écrire des lemmes pour
    faciliter la preuve. *)

Theorem CV_mult_0 (An Bn : nat → R) (a : R) :
  let Cn (n:nat) := (An n * Bn n) in
  Un_cv An a → Un_cv Bn 0 → Un_cv Cn 0.
Proof.
  unfold Un_cv. unfold R_dist.
  intros HA HB eps Heps.
  destruct (CV_impl_bounded An a HA) as [m [Hm0 HM]].
  destruct (HB (eps /  m)) as [nB HnB].
  * (* lra ne sait pas faire ? *)
    apply (Rmult_lt_reg_r m); try lra.
    rewrite Rmult_0_l.
    unfold Rdiv; rewrite Rmult_assoc; rewrite Rinv_l; lra.
  * exists nB.
    intros n Hn. rewrite Rminus_0_r.
    rewrite Rabs_mult.
    apply Rle_lt_trans with (r2 := m * Rabs (Bn n)).
    - apply Rmult_le_compat_r.
      + apply Rabs_pos.
      + apply HM.
    - apply HnB in Hn.
      rewrite Rminus_0_r in Hn.
      replace eps with (m * (eps / m)).
      + apply Rmult_lt_compat_l; try easy.
      + unfold Rdiv. rewrite <- Rmult_assoc.
        rewrite Rmult_comm. rewrite <- Rmult_assoc.
        field. lra.
Qed.

(** Ensuite, le cas général *)

Theorem CV_mult (An Bn : nat → R) (a b : R) :
  let Cn (n:nat) := (An n * Bn n) in
  Un_cv An a → Un_cv Bn b → Un_cv Cn (a * b).
Proof.
  unfold Un_cv, R_dist.
  intros HA HB eps Heps.
  assert (E:Un_cv (fun n => (An n) * ((Bn n)- b)) 0). {
    apply CV_mult_0 with (a:=a).
    - now unfold Un_cv.
    - apply CV_An_minus_a_0.
      (* Ces 4 lignes sont laides ! On ne devrait avoir à écrire que le replace... *)
      unfold Un_cv, R_dist. intros **.
      destruct (HB eps0 H) as [N HN]. exists N. intros n Hn.
      replace (Bn n - b - 0 - 0) with (Bn n - b) by lra.
      now apply HN.
  }
  assert (F:Un_cv (fun n => (b * An n)) (b*a)). {
    apply CV_mult_const. now unfold Un_cv.
  }
  unfold Un_cv, R_dist in E, F.
  assert (Heps2: eps / 2 > 0) by lra.
  destruct (E (eps / 2) Heps2) as [n1 Hn1].
  destruct (F (eps / 2) Heps2) as [n2 Hn2].
  exists (max n1 n2).
  intros n Hn.
  replace (An n * Bn n - a * b) with ((An n * (Bn n - b)) + (b * An n - b * a)) by lra.
    apply Rle_lt_trans with
    (r2 := Rabs (An n * (Bn n - b)) + Rabs (b * An n - b * a)).
  - apply Rabs_triang.
  - replace eps with (eps/2 + eps/2) by lra.
    apply Rplus_lt_compat.
    + rewrite <- (Rminus_0_r (An n * (Bn n - b))).
      apply Hn1.
      apply le_trans with (m := max n1 n2). apply Nat.le_max_l. lia.
    + apply Hn2.
      apply le_trans with (m := max n1 n2). apply Nat.le_max_r. lia.
Qed.
End Suites.
