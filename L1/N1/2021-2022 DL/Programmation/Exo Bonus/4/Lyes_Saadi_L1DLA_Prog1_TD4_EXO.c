/* Algorithme :
 *
 * On demande à l'utilisateur la taille de la suite d'entiers on va faire la
 * somme.
 *
 * Ensuite, on demande à l'utilisateur d'entrer tous les nombres de la suite, du
 * premier terme 1 jusqu'au dernier terme qui équivaut à la taille de la suite
 * demandé par l'utilisateur. Et, au fur et à mesure que l'utilisateur entre les
 * termes de la suite, on ajoute la valeur de chaque terme à une variable somme.
 *
 * Lorsque l'utilisateur a entré tous les termes de la suite, on affiche la
 * somme finale, et on renvoie EXIT_SUCCESS.
 */

// On importe les librairies standards.
#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h> // printf() et scanf()

/*
 * Demande la taille et les éléments d'une suite et en fait somme de ses
 * éléments.
 */
int main() {
	// On déclare les variables. (et on initialise somme à 0.)
	int taille, i, n, somme = 0;

	// On demande à l'utilisateur la taille de la somme et on la stocke dans la
	// variable taille.
	printf("Taille de la suite dont vous voulez faire la somme : ");
	scanf("%d", &taille);

	// On demande à l'utilisateur chaque élément de la suite en parcourant la
	// suite du premier élément 1, jusqu'à l'élément taille.
	for (i = 1; i <= taille; i++) {
		// On demande à l'utilisateur l'élément n°i et on le stocke dans n.
		printf("Donnez l'élément n°%d de la suite : ", i);
		scanf("%d", &n);

		// On rajoute l'élément n°i dans le reste de la somme.
		somme += n;
	}

	// On affiche la somme de la suite.
	printf("La somme de la suite est %d.\n", somme);

	// Tout s'est bien passé, on retourne EXIT_SUCCESS.
	return EXIT_SUCCESS;
}
