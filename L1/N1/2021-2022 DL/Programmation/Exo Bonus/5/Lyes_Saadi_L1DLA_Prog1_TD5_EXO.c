/* Algorithme
 *
 * On déclare une variable A, contenant le résultat, qu'on initialise à 1 pour
 * éviter qu'elle ne nous empêche de réaliser le produit issu de la puissance n
 * de (1 + r) dans la boucle for.
 *
 * Ensuite, on déclare les variables x, r, n et i, et on demande à l'utilisateur
 * de nous donner les valeurs de l'argent x, du taux r et du nombre d'années
 * placé.
 *
 * Puis, on calcule le capital A en multipliant n fois (1 + r) à l'aide d'une
 * boucle for, puis, en multipliant le résultat par x.
 *
 * On affiche ensuite le résultat du calcul du capital A.
 */
#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h> // printf() & scanf()

/*
 * Calcule le capital A produit par x euros, placés au taux r au bout de n
 * années.
 */
int main() {
	// On déclare les variables.
	// On initialise A à 1 pour éviter que A = 0 nous empêche de faire le
	// produit dans la boucle for.
	int A = 1;
	int x, r, n, i;

	// On demande à l'utilisateur l'argent x.
	printf("Donnez le l'argent placé x = ");
	scanf("%d", &x);

	// On demande à l'utilisateur le taux r.
	printf("Donnez le taux r = ");
	scanf("%d", &r);

	// On demande à l'utilisateur le nombre d'années placé n.
	printf("Donnez le nombre d'années n = ");
	scanf("%d", &n);

	// On multiplie (1 + r) n fois. On calcule donc (a + r) à la puissance n.
	for (i = 1; i <= n; i++) {
		A *= 1 + r;
	}

	// On multiplie (A+r)^n par x pour avoir la valeur finale de A.
	A *= x;

	// On affiche la valeur du capital A.
	printf("A = %d\n", A);

	// Tout s'est bien passé, on retourne EXIT_SUCCESS.
	return EXIT_SUCCESS;
}
