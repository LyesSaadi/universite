#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h> // printf & scanf
#include <math.h> // abs & modf
#include <time.h> // time, localtime & tm

// Structure pour représenter une date.
struct date {
	int jour;  // Le jour.
	int mois;  // Le mois.
	int annee; // L'année.
};

/*
 * Convertir une date grégorienne en une date julienne.
 */
int gregorien_to_julien(struct date date);

/*
 * Convertir une date julienne en une date grégorienne.
 */
struct date julien_to_gregorien(int jj);

/*
 * Afficher une date de format :
 * Jour_de_la_semaine Jour_du_mois Mois Année
 * Exemple : Jeudi 18 Avril 2002
 */
void affichage_date(struct date date);

/*
 * Demande à l'utilisateur sa date de naissance, affiche la date, calcule et
 * affiche le nombre de jours écoulé depuis sa naissance, calcule et affiche
 * la différence de jours entre la date de naissance et le 1er janvier 2000,
 * calcule et affiche le 10 000ième jour avec votre naissance.
 */
int main() {
	// Crée une instance de la structure date.
	struct date date;

	// Récupère la date de naissance de l'utilisateur.
	do {
		printf("Bonjour, entrez votre date de naissance (aaaa-mm-jj) : ");
		// Utilise la convention AAAA-MM-JJ (Exemple : 2000-12-31).
		scanf("%d-%d-%d", &date.annee, &date.mois, &date.jour);
	} while (date.mois < 1 || date.mois > 12 || date.jour < 1 || date.jour > 31);

	// On affiche la date entrée.
	printf("Vous avez entré : ");
	affichage_date(date);

	// Tout le crédit pour ces fonctions revient à Adam Rosenfield:
	// https://stackoverflow.com/a/1442131/9118726
	time_t t = time(NULL);
	struct tm today = *localtime(&t);

	// La date à laquelle a été exécuté le programme.
	struct date aujourdhui = {
		// tm_mday renvoit le jour du mois (1-31).
		.jour = today.tm_mday,
		// tm_mon  renvoit le mois de l'année (0-11, on rajoute 1).
		.mois = today.tm_mon + 1,
		// tm_year renvoit l'année depuis 1900 (donc, on rajoute 1900).
		.annee = today.tm_year + 1900,
	};

	// La date au 1er Janvier 2000
	struct date janvier2000 = {
		.jour = 1,
		.mois = 1,
		.annee = 2000,
	};

	// Calcule le nombre de jours écoulé depuis la naissance de l'utilisateur.
	int anniv_aujourdhui = gregorien_to_julien(aujourdhui)
						   - gregorien_to_julien(date);
	// Calcule le nombre de jours entre la date de naissance de l'utilisateur
	// et le 1er janvier 2000. On utilise une valeur absolue dans le cas où
	// l'utilsateur serait né avec le 1er janvier 2000.
	int anniv_janvier2000 = abs(gregorien_to_julien(date)
								- gregorien_to_julien(janvier2000));

	// Affiche le nombre de jours écoulé la naissance de l'utilisateur.
	printf("Il s'est écoulé %d jours depuis votre anniversaire\n",
		   anniv_aujourdhui);
	// Calcule le nombre de jours entre la date de naissance de l'utilisateur
	// et le 1er janvier 2000.
	printf("Il s'est écoulé %d jours depuis le premier janvier 2000\n",
		   anniv_janvier2000);

	// On convertit la date en Julien, avant de rajouter 10000 jours, puis on la
	// reconvertit en date grégorienne.
	date = julien_to_gregorien(gregorien_to_julien(date) + 10000);

	// Affiche le 10 000ième jour apès la naissance de l'utilisateur.
	printf("Le 10 000ième jour après votre naissance est le : ");
	affichage_date(date);

	// Tout s'est bien passé, on retourne donc EXIT_SUCCESS.
	return EXIT_SUCCESS;
}

/*
 * Convertir une date grégorienne en une date julienne.
 */
int gregorien_to_julien(struct date date) {
	// Je ne saurais décrire cet algorithme, j'ai juste appliqué les
	// instructions :P !
	int jj, x1, x2, x3, x4 = 0;

	if (date.mois > 2) {
		x1 = date.annee;
		x2 = date.mois;
	} else {
		x1 = date.annee - 1;
		x2 = date.mois + 12;
	}

	if (!(date.annee < 1982 || (date.annee == 1982 && (date.mois < 10 || (date.mois == 10 && date.jour < 15))))) {
		x3 = x1 / 100.;
		x4 = 2 - x3 + (x3 / 4.);
	}
	jj = x4 + date.jour + 30.6001 * (x2 + 1) + 365.25 * x1 + 1720994.5;

	return jj;
}

/*
 * Convertir une date julienne en une date grégorienne.
 */
struct date julien_to_gregorien(int jj) {
	// Alors, j'ai pas réussi à commenter gregorien_to_julien(), aucune chance
	// pour cette fonction du coup xD !
	struct date date;

	int z, a, b, c, d, e;
	double zd, f;

	f = modf(jj + .5, &zd);
	z = zd;

	if (z < 2299161) {
		a = z;
	} else {
		int alpha = (z - 1867216.5) / 36524.25;
		a = z + 1 + alpha - (int)(alpha / 4);
	}

	b = a + 1524;
	c = (b - 122.1) / 365.25;
	d = 365.25 * c;
	e = (b - d) / 30.6001;

	date.jour = b - d - (int)(30.6001 * e) + f;

	if (e < 13.5) {
		date.mois = e - 1;
	} else {
		date.mois = e - 13;
	}

	if (date.mois > 2.5) {
		date.annee = c - 4716;
	} else {
		date.annee = c - 4715;
	}

	return date;
}

/*
 * Afficher une date de format :
 * Jour_de_la_semaine Jour_du_mois Mois Année
 * Exemple : Jeudi 18 Avril 2002
 */
void affichage_date(struct date date) {
	// Tableau des jours de la semaine.
	char semaine[7][9] = {
		"Dimanche",
		"Lundi",
		"Mardi",
		"Mercredi",
		"Jeudi",
		"Vendredi",
		"Samedi",
	};

	// Tableau des mois d'une année.
	char mois[12][10] = {
		"Janvier",
		"Février",
		"Mars",
		"Avril",
		"Mai",
		"Juin",
		"Juillet",
		"Août",
		"Septembre",
		"Octobre",
		"Novembre",
		"Decembre",
	};

	// Convertit le jour Grégorien en jour Julien.
	int jj = gregorien_to_julien(date);

	// On affiche la date.
	printf("%s %d %s %d\n",
			// On affiche le jour de la semaine en rajoutant 1.5 au jour Julien,
			// en calculant le reste de la division par 7 et en récupérant le
			// jour correspondant dans le tableau des jours de la semaine.
			semaine[(int)(jj + 1.5) % 7],
			date.jour,
			// On affiche le mois en décrémentant le mois pour correspondre à
			// l'intervalle 0-11.
			mois[date.mois - 1],
			date.annee);
}
