/* Algorithme :
 * 
 * On défini un codage pour chacun des états possible du temps en assignant à
 * chaque état une valeur entière. On peut, par exemple, prendre :
 * 1 => Temps couvert
 * 2 => Temps ensoleillé
 * 3 => Temps pluvieux
 *
 * Ensuite, on déclare une variable temps, et on l'initialise avec l'une des
 * valeurs du codage, par exemple PLUVIEUX.
 *
 * Puis, si temps est COUVERT,    on affiche « Le temps est couvert. »,
 *       si temps est ENSOLEILLE, on affiche « Le temps est ensoleillé. »,
 *       si temps est PLUVIEUX,   on affiche « Le temps est pluvieux. »,
 *       sinon,                   on affiche une erreur et on retourne EXIT_FAILURE.
 * Dans notre exemple, on affiche « Le temps est pluvieux. ».
 *
 * Enfin, on retourne EXIT_SUCCESS.
 */

// On importe les librairies standards dont on a besoin.
#include <stdlib.h> // `EXIT_SUCCESS` et `EXIT_FAILURE`
#include <stdio.h> // `printf()`

// On défini des variables de préprocesseur pour coder les valeurs :
// Temps couvert    => 1
// Temps ensoleillé => 2
// Temps pluvieux   => 3
#define COUVERT 1
#define ENSOLEILLE 2
#define PLUVIEUX 3

int main() {
	int temps = PLUVIEUX; // On déclare et initialise temps avec une valeur.

	// On teste temps pour chacune des valeurs possibles.
	if (temps == COUVERT) { // Le cas où temps est COUVERT.
		printf("Prenez une veste ! Le temps est couvert.\n");
	} else if (temps == ENSOLEILLE) { // Le cas où temps est ENSOLEILLE.
		printf("C'est le temps parfait pour se ballader ! Le temps est ensoleillé.\n");
	} else if (temps == PLUVIEUX) { // Le cas où temps est PLUVIEUX.
		printf("Un parapluie ou un manteau bien chaud est indispensable ! Le temps est pluvieux.\n");
	} else { // Dans le cas où temps ne correspond à aucune valeur du codage. C'est une erreur.
		printf("Oups ! Vous auriez pas mis une valeur invalide par hasard ?\n");

		// Vu que ce cas est une erreur, on retourne EXIT_FAILURE.
		return EXIT_FAILURE;
	}

	// Tout s'est bien passé, on retourne EXIT_SUCCESS!
	return EXIT_SUCCESS;
}
