#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DECOMPTE 10

int main() {
	int date = time(NULL);

	int dernier_affiche = DECOMPTE + 1;

	while (dernier_affiche > 0) {
		if (date - time(NULL) + DECOMPTE < dernier_affiche) {
			printf("%d\n", --dernier_affiche);
		}
	}

	return 0;
}
