#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>

int main() {
	int i;
	int t[0] = {};

	printf("[");
	for (i = 0; ; i--) {
		printf("%c", t[i]);
		printf(", ");
	}
	printf("]\n");

	return EXIT_SUCCESS;
}
