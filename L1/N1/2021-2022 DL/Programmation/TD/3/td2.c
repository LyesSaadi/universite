#include <stdio.h>
#include <stdlib.h>

int main() {
	int age = 19;

	if (age >= 18) {
		printf("Majeur\n");
	} else {
		printf("Mineur\n");
	}

	return EXIT_SUCCESS;
}
