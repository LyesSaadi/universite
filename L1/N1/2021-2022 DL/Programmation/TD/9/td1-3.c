#include <stdlib.h>
#include <stdio.h>

int main() {
	int mots = 1;
	int len = 0;
	char phrase[500] = "Bonjour, mon petit, Grrrr.";

	while (phrase[len] != '\0') {
		if (phrase[len] == ' ' && phrase[len - 1] != ' ')
			mots++;
		len++;
	}

	printf("La phrase a %d mots\n", mots);

	return EXIT_SUCCESS;
}
