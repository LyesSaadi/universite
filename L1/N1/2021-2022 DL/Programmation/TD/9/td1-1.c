#include <stdlib.h>
#include <stdio.h>

int main() {
	int len = 0;
	char phrase[500];

	printf("Entrez une phrase : ");
	scanf("%s", phrase);

	while (phrase[len] != '\0') {
		len++;
	}

	printf("La phrase a %d charactères\n", len);

	return EXIT_SUCCESS;
}
