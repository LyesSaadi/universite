#include <stdlib.h>
#include <stdio.h>

#define TRUE 1
#define FALSE 0

int cube(int x);
int est_majeur(int x);
int somme(int n);
void afficher_rectangle(int largeur, int longueur);
int saisie_utilisateur();

int main() {
	printf("cube(3) = %d\n", cube(3));
	printf("est_majeur(66) = %d\n", est_majeur(66));
	printf("somme(13) = %d\n", somme(13));
	printf("afficher_rectangle(5, 10)\n"); afficher_rectangle(5, 10);
	printf("saisie_utilisateur()\n");
	printf("saisie_utilisateur() = %d\n", saisie_utilisateur());

	return EXIT_SUCCESS;
}

int cube(int x) {
	return x * x * x;
}

int est_majeur(int x) {
	if (x >= 18)
		return TRUE;
	return FALSE;
}

int somme(int n) {
	if (n > 0)
		return n + somme(n - 1);
	return 0;
}

void afficher_rectangle(int largeur, int longueur) {
	int i, j;

	for (i = 1; i <= largeur; i++) {
		printf("*");
		for (j = 2; j < longueur; j++) {
			if (i == 1 || i == largeur)
				printf("*");
			else
				printf(" ");
		}
		printf("*\n");
	}
}

int saisie_utilisateur() {
	int x;

	printf("Veuillez entrer un entier x = ");
	scanf("%d", &x);

	return x;
}
