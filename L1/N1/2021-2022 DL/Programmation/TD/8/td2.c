#include <stdlib.h>
#include <stdio.h>

int valeur_absolue(int x);
int factorielle(int x);
int minimum(int x, int y);

int main() {
	int x = -3;
	int y = 5;
	int z;

	x = valeur_absolue(x);
	z = minimum(x, y);
	z = factorielle(z);
	z = minimum(y, z);

	printf("x = %d, y = %d, z = %d\n", x, y, z);
	printf("3! = %d\n", factorielle(10));

	return EXIT_SUCCESS;
}

int valeur_absolue(int x) {
	if (x < 0)
		return -x;
	return x;
}

int factorielle(int x) {
	if (x > 1)
		return x * factorielle(x - 1);
	return x;
}

int minimum(int x, int y) {
	if (x < y)
		return x;
	return y;
}
