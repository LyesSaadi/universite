/* Algorithme :
 * 
 * On défini un codage pour chacun des états possible du temps en assignant à
 * chaque état une valeur entière. On peut, par exemple, prendre :
 * 1 => Temps couvert
 * 2 => Temps ensoleillé
 * 3 => Temps pluvieux
 *
 * Ensuite, on déclare une variable temps, et on l'initialise avec l'une des
 * valeurs du codage, par exemple PLUVIEUX.
 *
 * Puis, si temps est COUVERT,    on affiche « Le temps est couvert. »,
 *       si temps est ENSOLEILLE, on affiche « Le temps est ensoleillé. »,
 *       si temps est PLUVIEUX,   on affiche « Le temps est pluvieux. »,
 *       sinon,                   on affiche une erreur et on retourne EXIT_FAILURE.
 * Dans notre exemple, on affiche « Le temps est pluvieux. ».
 *
 * Enfin, on retourne EXIT_SUCCESS.
 */

// On importe les librairies standards dont on a besoin.
#include <stdlib.h> // `EXIT_SUCCESS` et `EXIT_FAILURE`
#include <stdio.h> // `printf()`

// On défini des variables de préprocesseur pour coder les valeurs :
// Temps couvert    => 1
// Temps ensoleillé => 2
// Temps pluvieux   => 3
enum temps {
	COUVERT = 0,
	ENSOLEILLE = 1,
	PLUVIEUX = 2,
	NEIGEUX = 3
};

enum pression {
	BAISSE,
	STABLE,
	AUGMENTE
};

enum vent {
	EST,
	OUEST,
	NORD,
	SUD
};

struct meteo {
	enum temps temps;
	enum pression pression;
	enum vent vent;
};

int main() {
	enum temps temps; // On déclare et initialise temps avec une valeur.

	printf("COUVERT    = %d\n", COUVERT);
	printf("ENSOLEILLE = %d\n", ENSOLEILLE);
	printf("PLUVIEUX   = %d\n", PLUVIEUX);
	printf("NEIGEUX    = %d\n", NEIGEUX);
	printf("Quel temps fait-il : ");
	scanf("%d", &temps);

	// On teste temps pour chacune des valeurs possibles.
	switch (temps) {
		case COUVERT:
			printf("Prenez une veste ! Le temps est couvert.\n");
			break;
		case ENSOLEILLE:
			printf("C'est le temps parfait pour se ballader ! Le temps est ensoleillé.\n");
			break;
		case PLUVIEUX:
			printf("Un parapluie ou un manteau bien chaud est indispensable ! Le temps est pluvieux.\n");
			break;
		case NEIGEUX:
			printf("Brrrrr, il fait glacé, et il neige. Prenez une doudoune bien chaude !\n");
			break;
		default:
			printf("Oups ! Vous auriez pas mis une valeur invalide par hasard ?\n");

			// Vu que ce cas est une erreur, on retourne EXIT_FAILURE.
			return EXIT_FAILURE;
	}

	// Tout s'est bien passé, on retourne EXIT_SUCCESS!
	return EXIT_SUCCESS;
}
