// On importe les librairies standards dont on a besoin.
#include <stdlib.h> // `EXIT_SUCCESS` et `EXIT_FAILURE`
#include <stdio.h> // `printf()`

#include "bulletin.h"


int main() {
	struct meteo meteo = saisie_meteo(); // On déclare et initialise temps avec une valeur.

	printf("\nLa météo actuelle :\n");
	affichage_temps(meteo);
	printf("\nLa météo prévision :\n");
	affichage_temps(prevision(meteo));

	// Tout s'est bien passé, on retourne EXIT_SUCCESS!
	return EXIT_SUCCESS;
}

