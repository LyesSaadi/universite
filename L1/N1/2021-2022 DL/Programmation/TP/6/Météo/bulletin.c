#include <stdio.h>

#include "bulletin.h"

struct meteo saisie_meteo() {
	struct meteo meteo;

	meteo = saisie_temperature(meteo);
	meteo = saisie_temps(meteo);
	meteo = saisie_pression(meteo);
	meteo = saisie_vent(meteo);

	return meteo;
}

struct meteo saisie_temps(struct meteo meteo) {
	printf("\n");
	printf("COUVERT    = %d\n", COUVERT);
	printf("ENSOLEILLE = %d\n", ENSOLEILLE);
	printf("PLUVIEUX   = %d\n", PLUVIEUX);
	printf("NEIGEUX    = %d\n", NEIGEUX);

	int temps;
	do {
		printf("Quel temps fait-il : ");
		scanf("%d", &temps);
	} while (!(temps == COUVERT || temps == ENSOLEILLE || temps == PLUVIEUX || temps == NEIGEUX));
	meteo.temps = temps;

	return meteo;
}

struct meteo saisie_pression(struct meteo meteo) {
	printf("\n");
	printf("BAISSE     = %d\n", BAISSE);
	printf("STABLE     = %d\n", STABLE);
	printf("AUGMENTE   = %d\n", AUGMENTE);

	int pression;
	do {
		printf("Comment la pression evolue-t-elle : ");
		scanf("%d", &pression);
	} while (!(pression == BAISSE || pression == STABLE || pression == AUGMENTE));
	meteo.pression = pression;

	return meteo;
}

struct meteo saisie_vent(struct meteo meteo) {
	printf("\n");
	printf("EST        = %d\n", EST);
	printf("OUEST      = %d\n", OUEST);
	printf("NORD       = %d\n", NORD);
	printf("SUD        = %d\n", SUD);

	int vent;
	do {
		printf("Quel est la direction du vent : ");
		scanf("%d", &vent);
	} while (!(vent == EST || vent == OUEST || vent == NORD || vent == SUD));
	meteo.vent = vent;

	return meteo;
}

struct meteo saisie_temperature(struct meteo meteo) {
	printf("Quel température fait-il : ");
	scanf("%d", &meteo.temperature);

	return meteo;
}

struct meteo prevision(struct meteo actuel) {
	struct meteo prev = actuel;

	if (prev.temps == ENSOLEILLE && prev.pression == STABLE && prev.vent == OUEST) {
		prev.temps = ENSOLEILLE;
		return prev;
	}

	if (prev.pression == AUGMENTE && prev.temperature > 0 && prev.vent == NORD) {
		prev.temps = COUVERT;
		return prev;
	}

	if (prev.pression == BAISSE && prev.temps == COUVERT) {
		prev.temps = PLUVIEUX;
		if (prev.temperature < 0) {
			prev.temps = NEIGEUX;
		}

		return prev;
	}

	return prev;
}

void affichage_temps(struct meteo meteo) {
	printf("Température : %d\n", meteo.temperature);

	printf("Temps : ");
	switch (meteo.temps) {
		case ENSOLEILLE:
			printf("ensoilleillé");
			break;
		case COUVERT:
			printf("couvert");
			break;
		case PLUVIEUX:
			printf("pluvieux");
			break;
		case NEIGEUX:
			printf("neigeux");
			break;
	}
	printf("\n");

	printf("Pression : ");
	switch (meteo.pression) {
		case BAISSE:
			printf("baisse");
			break;
		case STABLE:
			printf("stable");
			break;
		case AUGMENTE:
			printf("augmente");
			break;
	}
	printf("\n");

	printf("Vent : ");
	switch (meteo.vent) {
		case EST:
			printf("est");
			break;
		case OUEST:
			printf("ouest");
			break;
		case NORD:
			printf("nord");
			break;
		case SUD:
			printf("sud");
			break;
	}
	printf("\n");
}
