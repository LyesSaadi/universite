#include <stdlib.h>

int main()
{
    int x;
    int y;
    int z;

    x = 10;
    y = 30;

    z = x + y;
    y = y / 3;
    x = x * x;

    return EXIT_SUCCESS;
}