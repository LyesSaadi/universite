#include <stdio.h>
#include <stdlib.h>

int main() {
	char langue;

beginning:
	printf("Quel langue parlez-vous ?\n");
	printf("F => Français\n");
	printf("A => Anglais\n");
	printf("E => Espagnol\n");
	printf("Entrez votre langue : ");

	scanf(" %s", &langue);

	switch (langue) {
		case 'F':
			printf("Bonjour !\n");
			break;
		case 'A':
			printf("Hello !\n");
			break;
		case 'E':
			printf("Hola !\n");
			break;
		default:
			goto beginning;
	}

	return EXIT_SUCCESS;
}
