#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 100

int main() {
	char verbe[MAX];

entree:
	printf("Entrez un verbe du premier groupe : ");
	scanf("%s", verbe);

	int len = strlen(verbe);

	if (verbe[len - 2] == 'e' && verbe[len - 1] == 'r')
		goto conjug;
	else
		goto entree;

conjug:
	char radical[MAX];
	strncpy(radical, verbe, len - 2);
	radical[len - 1] = '\0';
	char pronoms[6][11] = {
		"Je",
		"Tu",
		"Il/Elle/On",
		"Nous",
		"Vous",
		"Ils/Elles"
	};
	char terminaisons[6][4] = {
		"e",
		"es",
		"e",
		"ons",
		"ez",
		"ent"
	};

	int i = 0;

boucle:
	printf("%s %s%s\n", pronoms[i], radical, terminaisons[i]);
	i++;
	if (i < 6)
		goto boucle;

	return EXIT_SUCCESS;
}
