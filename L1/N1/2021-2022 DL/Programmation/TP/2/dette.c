#include <stdlib.h>
#include <stdio.h>

// Valeurs de marches
#define PANIQUE 10
#define INQUIETUDE 20
#define STABILITE 30

// Valeurs de hausse
#define NON 0
#define OUI 1

int main() {
	int marches, hausse;
	float dette;

	marches = PANIQUE;
	hausse = OUI;
	dette = 0.7;

	if (marches == STABILITE) {
		return 0;
	} else if (marches == INQUIETUDE) {
		if (hausse == NON) {
			return 0;
		} else if (hausse == OUI) {
			return 1;
		}
	} else if (marches == PANIQUE) {
		if (dette > 0.5) {
			return 1;
		} else {
			return 0;
		}
	}
}
