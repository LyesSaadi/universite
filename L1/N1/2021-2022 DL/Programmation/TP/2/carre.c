#include <stdio.h>
#include <stdlib.h>

int main() {
	int longueur, largeur;

	printf("La longueur du triangle est ");
	scanf("%d", &longueur);
	printf("La largeur du triangle est ");
	scanf("%d", &largeur);

	int i, j;

	for (i = 0; i < longueur; i++) {
		for (j = 0; j < largeur; j++) {
			printf("*");
		}
		printf("\n");
	}

	return EXIT_SUCCESS;
}
