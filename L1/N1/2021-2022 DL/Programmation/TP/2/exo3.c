#include <stdlib.h>
#include <stdio.h>

int main() {
	int x, carre, cube;

	printf("x = ");
	scanf("%d", &x);

	carre = x * x;
	cube = x * x * x;

	printf("Le carré de %d est %d.\n", x, carre);
	printf("Le cube de %d est %d.\n", x, cube);

	return EXIT_SUCCESS;
}

