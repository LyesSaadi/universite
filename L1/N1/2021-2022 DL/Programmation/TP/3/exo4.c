#include <stdlib.h>
#include <stdio.h>

int main() {
	double x, a;

	printf("x = ");
	scanf("%lf", &x);

	a = 8118 * x * x * x * x - 11482 * x * x * x + x * x + 5741 * x - 2030;

	printf("%lf\n", a);

	return EXIT_SUCCESS;
}
