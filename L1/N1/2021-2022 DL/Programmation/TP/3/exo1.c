#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define NB_MAX 100
#define TRUE 1
#define FALSE 0

int main() {
	int nb;

	srand(time(NULL));
	
	int secret = rand() % (NB_MAX + 1);

	int trouve = FALSE;

	while (!trouve) {
		printf("Devinez un nombre entre 0 et %d : ", NB_MAX);
		scanf("%d", &nb);

		if (nb < 0 || nb > NB_MAX) {
			printf("Le nombre est entre 0 et %d.\n", NB_MAX);
			continue;
		}

		if (nb == secret) {
			trouve = TRUE;
		} else if (nb > secret) {
			printf("Le nombre est plus petit !\n");
		} else if (nb < secret) {
			printf("Le nombre est plus grand !\n");
		}
	}

	printf("Bravo ! Vous avez trouvé %d !\n", secret);

	return EXIT_SUCCESS;
}
