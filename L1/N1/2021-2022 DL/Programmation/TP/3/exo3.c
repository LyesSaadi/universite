#include <stdlib.h>
#include <stdio.h>

int main() {
	int n = 5, p = 9;
	int q; float x;

	q = n < p;
	printf("q = %d\n", q);
	q = n == p;
	printf("q = %d\n", q);
	q = p % n + p > n;
	printf("q = %d\n", q);
	x = p / n;
	printf("x = %f\n", x);
	x = (p + 0.5) / n;
	printf("x = %f\n", x);

	char c = 'l';
	printf("c = %c\n", c * 3);

	return EXIT_SUCCESS;
}
