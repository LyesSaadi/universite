#include <stdlib.h>
#include <stdio.h>

int main() {
	int a, b;

	printf("Entrez deux valeurs booléennes (1 pour True, 0 pour False) : ");
	scanf("%d %d", &a, &b);

	printf("a\t\tb\t\ta ET b\ta OU B\tNON a\tNON b\tNON a ET b\n");
	printf("%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\n",
			a,
			b,
			a && b,
			a || b,
			!a,
			!b,
			!a && b
	);

	return EXIT_SUCCESS;
}
