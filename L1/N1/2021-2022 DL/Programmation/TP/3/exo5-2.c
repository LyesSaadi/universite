#include <stdlib.h>
#include <stdio.h>

int main() {
	int un = 1;
	int un_1 = 1;
	int n = 2;
	int m, tmp;

	printf("m = ");
	scanf("%d", &m);

	while (un < m) {
		tmp = un;
		un += un_1;
		un_1 = tmp;
		n++;
	}

	printf("u_%d = %d\n", n - 1, un_1);

	return EXIT_SUCCESS;
}
