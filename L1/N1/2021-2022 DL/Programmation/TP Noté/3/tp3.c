#include <stdlib.h> // EXIT_SUCCESS & EXIT_FAILURE
#include <stdio.h> // printf & scanf

// La taille maximale du tableau
#define MAX 256

/*
 * Compte le nombre de multiples de 5 dans un tableau T de taille n.
 */
int compter_multiple5(int T[], int n);

/*
 * Demande à l'utilisateur un tableau, affiche le nombres d'entiers multiples et
 * non multiples de 5, puis trie le tableau en fonction de si le nombre est
 * multiple ou non de 5, et enfin affiche le tableau.
 */
int main() {
	int t; // La taille du tableau.
	int tab[MAX]; // Le tableau d'entier (de taille maximale théorique MAX).

	int i, j; // Des itérateurs.
	int temp; // Une variable pour stocker des valeurs temporaires.

	// On demande à l'utilisateur la taille du tableau d'entiers.
	do {
		printf("Combien d'entiers dans le tableau ? ");
		scanf("%d", &t);
		// On répète tant que la taille ne se trouve pas dans l'intervalle
		// [t; MAX].
	} while (t < 0 || t > MAX);

	// On récupère chaque élément du tableau.
	for (i = 0; i < t; i++) {
		scanf("%d", &tab[i]);
	}

	// On compte le nombre de multiples de 5 dans le tableau. Et on calcule le
	// nombre de non multiples de 5 en faisant t - mult5. Et on l'affiche à
	// l'utilisateur.
	int mult5 = compter_multiple5(tab, t);
	printf("%d nombres multiples de 5 et %d nombres non multiples de 5\n",
			mult5, t - mult5);

	// On va trier le tableau en fonction des multiples de 5.
	//
	// Algorithme :
	// On connait le nombre total de multiples de 5 dans le tableau.
	// Et on veut que tous les multiples de 5 soient au début du tableau.
	// Donc, on va parcourir les n premiers éléments du tableau, n étant le
	// nombres de multiples de 5 dans le tableau, et si on trouve dans ces
	// éléments là, un nombre qui n'est pas multiple de 5, on parcoure le
	// tableau dans le sens inverse jusqu'à trouver un élément qui est multiple
	// de 5. Quand on en trouve un, on intervertit les deux éléments.
	//
	// On se retrouve donc à la fin de la boucle avec les n premiers éléments
	// (n nombre de multiple de 5) étant uniquement des multiples de 5, et le
	// reste du tableau étant les éléments n'étant pas des multiples de 5.

	// On parcoure les n premiers éléments, avec n = mult5, le nombre total de
	// multiples de 5 dans le tableau.
	for (i = 0; i < mult5; i++) {
		// Si l'élément n'est pas un multiple de 5 :
		if (tab[i] % 5 != 0) {
			// On parcours le tableau à l'envers.
			for (j = t - 1; j >= 0; j--) {
				// Quand on trouve un élément multiple de 5, on arrête la boucle
				// et on garde j qui devient l'index de l'élément qu'on doir
				// intervertir
				if (tab[j] % 5 == 0) {
					break;
				}
			}

			// Ceci est théoriquement impossible.
			if (j == -1) {
				// Si on arrive là, c'est qu'il y a eu une erreur quelque part.
				printf("J'ai merdé dans mon algorithme.\n");
				return EXIT_FAILURE;
			}

			// On intervertit les éléments à l'aide d'une variable temporaire.
			temp = tab[i];
			tab[i] = tab[j];
			tab[j] = temp;

			// On affiche les éléments après que nous ayons modifié l'ordre du
			// tableau.
			for (j = 0; j < t; j++) {
				printf("%d ", tab[j]);
			}

			printf("\n");
		}
	}

	// On affiche encore une fois le tableau dans son état final pour être
	// conforme à l'affichage demandé.
	for (j = 0; j < t; j++) {
		printf("%d ", tab[j]);
	}

	printf("\n");

	// On affiche le tableau trié avec les nombres multiples de 5 et non
	// multiples de 5 séparés par une barre verticale.
	printf("Tableau trie : multiples de 5 | non multiples de 5\n");

	// On affiche les n premiers éléments du tableau, n étant le nombre
	// de multiples de 5.
	for (i = 0; i < mult5; i++) {
		printf("%d ", tab[i]);
	}

	printf("| ");

	// On reprend le tableau où on s'est arrêté, et on affiche le reste.
	for (i = mult5; i < t; i++) {
		printf("%d ", tab[i]);
	}

	printf("\n");

	// Tout s'est bien passé, on renvoie EXIT_SUCCESS.
	return EXIT_SUCCESS;
}

/*
 * Compte le nombre de multiples de 5 dans un tableau T de taille n.
 */
int compter_multiple5(int T[], int n) {
	int i; // L'itérateur
	int nb = 0; // Une variable pour garder en mémoire le nombre de multiples.

	// On traverse le tableau.
	for (i = 0; i < n; i++) {
		// Si on trouve un multiple de 5, on incrémente nb.
		if (T[i] % 5 == 0) {
			nb++;
		}
	}

	return nb; // On retourne le nombre de multiples de 5.
}
