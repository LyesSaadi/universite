#!/bin/sh

for fichier in *; do
	case $fichier in
		*.jpg | *.jpeg)
			mv $fichier ~/Images/
			;;
		a.out | *.o)
			rm $fichier
			;;
		*.txt)
			head -n 10 $fichier
	esac
done
