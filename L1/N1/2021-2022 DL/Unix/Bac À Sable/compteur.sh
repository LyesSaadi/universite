#!/usr/bin/sh

i=0

while true; do
	i=$(( $i + 1 ))
	echo $i
done &

sleep 3

kill $!
