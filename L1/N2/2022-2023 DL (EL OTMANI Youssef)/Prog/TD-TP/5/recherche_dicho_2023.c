#include <stdio.h> /* pour printf, scanf */
#include <stdlib.h> /* pour EXIT_SUCCESS */
                    /* rand, srand, RAND_MAX */
#include <time.h> /* pour time */

#define N 100

typedef int item;

void echanger_item (item *, item *);
void trier_par_insertion (item *, int);
int rechercher_dicho_tab (item, item *, int);
void initialiser_alea_tab (item *, int);
void afficher_tab_vingt (item *, int);
int saisir_entier (int, int);

int main () {
  item x, tab[N];
  int res, taille = saisir_entier(0, N);

  /* germe pour le générateur pseudo-aléatoire */
  srand (time (NULL));
  /* initialisation des valeurs du tableau à des entiers dans l'intervalle [0,99], */
  initialiser_alea_tab (tab, taille);
  /* tri et affichage du tableau */
  trier_par_insertion (tab, taille);
  printf("Tableau (seulement 20 premières valeurs si taille > 20): \n");
  afficher_tab_vingt (tab, taille);
  /* appel de la recherche */
  printf("Saisie de la valeur recherchée. \n");
  x = saisir_entier (0, N-1);
  res = rechercher_dicho_tab (x, tab, taille);
  /* affichage du résultat de la recherche */
  if (res < 0)
    printf("La valeur %d est absente du tableau: \n", x);
  else
    printf("La valeur %d occupe la position %d dans le tableau: \n", x, res);

  return EXIT_SUCCESS;
}

/** Fonction récursive non terminale qui recherche un item x dans un tableau t d'items */
/* ordonné : t[0] ≤ t[1] ≤ ... ≤ t[taille-1] */
/* Si x est dans t, la fonction renvoie un indice pour cet item, */
/* sinon elle renvoie -1 */
int rechercher_dicho_tab (item x, item *t, int taille) {
  int m = taille/2;
  if (taille <= 0) return -1;
  if (t[m] == x) return m;
  if (t[m] > x)
    return rechercher_dicho_tab(x, t, m);
  else {
    int res = rechercher_dicho_tab(x, t + m + 1, taille - (m + 1));
    /* si le rappel récursif ci-dessus renvoie -1, c'est que x est absent de t */
    /* s'il renvoie un indice positif, il faut lui ajouter le décalage m + 1 */
    return res == -1 ? -1 : m + 1 + res;
  }
}

/** Fonction qui trie selon l'ordre croissant un tableau t d'items, */
void trier_par_insertion (item *t, int taille) {
  int i, j;
  for (i = 1; i < taille; ++i) {
    j = i;
    while (j > 0 && t[j] < t[j-1]) {
      echanger_item (t + j, t + j - 1);
      --j;
    }
  }
}

/** Fonction qui échange les valeurs de deux items */
/* dont les adresses sont reçues comme premier et deuxième paramètres d'entrée */
void echanger_item (item *x, item *y) {
  item tmp = *x;
  *x = *y;
  *y = tmp;
}

/** Fonction qui initialise de façon pseudo-aléatoire un tableau t */
/* dont la taille est reçue comme 2e paramètre d'entrée */
void initialiser_alea_tab (item *t, int taille) {
  int i;
  for (i = 0; i < taille; ++i) { t[i] = rand()%100; }
}

/** Fonction qui affiche les vingt premières valeurs d'un tableau t */
/* dont la taille est reçue comme 2e paramètre d'entrée */
void afficher_tab_vingt (item *t, int taille) {
  int i;
  taille = taille > 20 ? 20 : taille;
  for (i = 0; i < taille; ++i) { printf("%5d", t[i]); }
  printf("\n");
}

/** Fonction qui saisit et renvoie un entier compris entre vmin et vmax */
int saisir_entier (int vmin, int vmax) {
  int res, n;
  char c;
  do {
    printf("Tapez une valeur entière comprise entre %d et %d : ", vmin, vmax);
    n = scanf("%d", &res);
    if (1 == n && res >= 0 && res <= vmax)
      return res;
    if (0 == n) {
			do { scanf("%c", &c); }
			while (c != ' ' && c != '\t' && c != '\n');
		}
    printf("Erreur ! Recommencez !");
  }
  while (1);
}
