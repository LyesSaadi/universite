#include <stdlib.h>

#include "matrice.h"
#include "operation.h"
#include "pivot.h"

int main() {
	matrice m1, m2, ma, mm;

	m1 = creer_matrice_user();
	/*
	m2 = creer_matrice_user();

	matrice_affichage(m1);
	matrice_affichage(m2);

	m1 = operation_dilatation(2, m1);
	matrice_affichage(m1);

	ma = operation_addition(m1, m2);
	matrice_affichage(ma);

	mm = operation_produit(m1, m2);
	matrice_affichage(mm);
	*/

	pivot_solution(&m1);
	matrice_affichage(m1);

	matrice_free(m1);

	return EXIT_SUCCESS;
}
