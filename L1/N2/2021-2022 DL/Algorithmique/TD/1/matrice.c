#include "matrice.h"

matrice creer_matrice(int p, int q) {
	float** r = malloc(p * sizeof(float*));

	for (int i = 0; i < p; i++) {
		r[i] = malloc(q * sizeof(float));
	}

	matrice m = { p, q, r };

	return m;
}

matrice creer_matrice_user() {
	int i, j, p, q;

	printf("Donnez le nombre de lignes de votre matrice : ");
	scanf("%d", &p);
	printf("Donnez le nombre de colonnes de votre matrice : ");
	scanf("%d", &q);

	matrice m = creer_matrice(p, q);

	for (i = 0; i < p; i++) {
		for (j = 0; j < q; j++) {
			printf("m[%d][%d] = ", i + 1, j + 1);
			scanf("%f", &m.m[i][j]);
		}
	}

	return m;
}

matrice creer_matrice_rand(int p, int q) {
	int i, j;
	matrice m = creer_matrice(p, q);

	srand(time(NULL));

	for (i = 0; i < p; i++) {
		for (j = 0; j < q; j++) {
			m.m[i][j] = rand();
		}
	}

	return m;
}

matrice creer_matrice_hilbert(int p, int q) {
	int i, j;
	matrice m = creer_matrice(p, q);

	for (i = 0; i < p; i++) {
		for (j = 0; j < q; j++) {
			m.m[i][j] = 1. / (3 + i + j);
		}
	}

	return m;
}

matrice creer_matrice_stochastique(int p, int q) {
	int i, j, sum, r[q];
	matrice m = creer_matrice(p, q);

	srand(time(NULL));

	for (i = 0; i < p; i++) {
		sum = 0;

		for (j = 0; j < q; j++) {
			r[j] = rand() / q;
			sum += r[j];
		}

		for (j = 0; j < q; j++) {
			m.m[i][j] = ((float) r[j]) / sum;
		}
	}

	return m;
}

matrice creer_matrice_circulante(int p, int q) {
	int i, j;
	matrice m = creer_matrice(p, q);
	matrice o = creer_matrice_rand(1, q);

	for (i = 0; i < p; i++) {
		for (j = 0; j < q - i; j++) {
			m.m[i][j] = o.m[0][j + i];
		}

		for (; j < q; j++) {
			m.m[i][j] = 0;
		}
	}

	return m;
}

matrice matrice_copie(matrice o) {
	int i, j;
	matrice m = creer_matrice(o.p, o.q);

	for (i = 0; i < o.p; i++) {
		for (j = 0; j < o.q; j++) {
			m.m[i][j] = o.m[i][j];
		}
	}

	return m;
}

void matrice_affichage(matrice m) {
	int i, j;

	for (i = 0; i < m.p; i++) {
		for (j = 0; j < m.q; j++) {
			printf("%2.2f ", m.m[i][j]);
		}

		printf("\n");
	}

	/*
	for (i = 0; i < m.p; i++) {
		for (j = 0; j < m.q; j++) {
			printf("m[%d][%d] = %f\n", i + 1, j + 1, m.m[i][j]);
		}
	}
	*/

	printf("\n");
}

void matrice_free(matrice m) {
	free(m.m);
}
