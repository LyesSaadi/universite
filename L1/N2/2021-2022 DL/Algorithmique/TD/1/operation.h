#ifndef OPERATION_H
#define OPERATION_H

#include "matrice.h"

matrice operation_dilatation(float x, matrice m);
matrice operation_addition(matrice m1, matrice m2);
matrice operation_evaluation(matrice m, matrice u);
matrice operation_produit(matrice m1, matrice m2);

#endif
