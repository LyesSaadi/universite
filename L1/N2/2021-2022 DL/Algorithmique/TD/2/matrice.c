#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct mat {
	int p, q;
	float** m;
} matrice;

static matrice matrice_nulle = {
	0,
	0,
	NULL
};

matrice creer_matrice(int p, int q);
matrice creer_matrice_user();
matrice creer_matrice_rand(int p, int q);
matrice creer_matrice_hilbert(int p, int q);
matrice creer_matrice_stochastique(int p, int q);
matrice creer_matrice_circulante(int p, int q);
matrice matrice_copie(matrice m);
matrice matrice_dilatation(float x, matrice m);
matrice matrice_addition(matrice m1, matrice m2);
matrice matrice_evaluation(matrice m, matrice u);
matrice matrice_produit(matrice m1, matrice m2);
void matrice_affichage(matrice m);

int main() {
	return EXIT_SUCCESS;
}

matrice creer_matrice(int p, int q) {
	float** r = malloc(p * sizeof(float*));

	for (int i = 0; i < p; i++) {
		r[i] = malloc(q * sizeof(float));
	}

	matrice m = { p, q, r };

	return m;
}

matrice creer_matrice_user() {
	int i, j, p, q;

	printf("Donnez le nombre de lignes de votre matrice : ");
	scanf("%d", &p);
	printf("Donnez le nombre de colonnes de votre matrice : ");
	scanf("%d", &q);

	matrice m = creer_matrice(p, q);

	for (i = 0; i < p; i++) {
		for (j = 0; j < q; j++) {
			printf("m[%d][%d] = ", i + 1, j + 1);
			scanf("%f", &m.m[i][j]);
		}
	}

	return m;
}

matrice creer_matrice_rand(int p, int q) {
	int i, j;
	matrice m = creer_matrice(p, q);

	srand(time(NULL));

	for (i = 0; i < p; i++) {
		for (j = 0; j < q; j++) {
			m.m[i][j] = rand();
		}
	}

	return m;
}

matrice creer_matrice_hilbert(int p, int q) {
	int i, j;
	matrice m = creer_matrice(p, q);

	for (i = 0; i < p; i++) {
		for (j = 0; j < q; j++) {
			m.m[i][j] = 1. / (3 + i + j);
		}
	}

	return m;
}

/*
matrice creer_matrice_stochastique(int p, int q) {
	return NULL;
}

matrice creer_matrice_circulante(int p, int q) {
	return NULL;
}
*/

matrice matrice_copie(matrice o) {
	int i, j;
	matrice m = creer_matrice(o.p, o.q);

	for (i = 0; i < o.p; i++) {
		for (j = 0; j < o.q; j++) {
			m.m[i][j] = o.m[i][j];
		}
	}

	return m;
}

matrice matrice_dilatation(float x, matrice m) {
	int i, j;
	matrice r = matrice_copie(m);

	for (i = 0; i < m.p; i++) {
		for (j = 0; j < m.q; j++) {
			r.m[i][j] *= x;
		}
	}

	return r;
}

matrice matrice_addition(matrice m1, matrice m2) {
	int i, j;

	if (m1.p != m2.p || m1.q != m2.q) {
		return matrice_nulle;
	}

	matrice m = matrice_copie(m1);

	for (i = 0; i < m.p; i++) {
		for (j = 0; j < m.q; j++) {
			m.m[i][j] += m2.m[i][j];
		}
	}

	return m;
}

matrice matrice_evaluation(matrice m, matrice u) {
	int i, j;

	if (m.q != u.p || u.q != 0) {
		return matrice_nulle;
	}

	matrice r = matrice_copie(m);

	for (i = 0; i < m.p; i++) {
		for (j = 0; j < m.q; j++) {
			r.m[i][j] *= u.m[j][0];
		}
	}

	return r;
}

matrice matrice_produit(matrice m1, matrice m2) {
	int i, j, k;

	if (m1.q != m2.p) {
		return matrice_nulle;
	}

	matrice m = creer_matrice(m1.p, m2.q);

	for (i = 0; i < m.p; i++) {
		for (j = 0; j < m.q; j++) {
			m.m[i][j] = 0;

			for (k = 0; k < m1.q; k++) {
				m.m[i][j] += m1.m[i][k]*m2.m[k][j];
			}
		}
	}

	return m;
}

void matrice_affichage(matrice m) {
	int i, j;

	for (i = 0; i < m.p; i++) {
		for (j = 0; j < m.q; j++) {
			printf("m[%d][%d] = %f\n", i + 1, j + 1, m.m[i][j]);
		}
	}
}
