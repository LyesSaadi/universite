#include "pivot.h"

#include "matrice.h"
#include <stdio.h>

int pivot_recherche_pivot(matrice* m, int lp, int cp) {
	if (cp >= m->q)
		return -1;

	if (lp >= m->p)
		return -1;

	if (m->m[lp][cp] != 0)
		return lp;
	else
		return pivot_recherche_pivot(m, lp + 1, cp);

	/* Non récursive
	int i;

	for (i = lp; i < m.q; i++) {
		if (m.m[i][cp] != 0)
			return i;
	}

	return -1;
	*/
}

void pivot_addition(matrice* m, int l1, float x, int l2) {
	int i;

	for (i = 0; i < m->q; i++) {
		m->m[l1][i] += x * m->m[l2][i];
	}
}

void pivot_multiplication(matrice* m, int l1, float x) {
	int i;

	for (i = 0; i < m->q; i++) {
		m->m[l1][i] *= x;
	}
}

void pivot_echange(matrice* m, int l1, int l2) {
	if (l1 >= m->p || l2 >= m->p)
		return;

	float* temp = m->m[l1];
	m->m[l1] = m->m[l2];
	m->m[l2] = temp;
}

void pivot_annule(matrice* m, int c) {
	int i;

	for (i = 0; i < m->p; i++) {
		if (i == c)
			continue;
		pivot_addition(m, i, -m->m[i][c], c);
	}
}

int min(int x, int y) {
	return x < y ? x : y;
}

void pivot_gauss_simple(matrice* m) {
	int i, p;

	for (i = 0; i < min(m->p, m->q); i++) {
		p = pivot_recherche_pivot(m, i, i);

		if (p == -1)
			continue;

		pivot_echange(m, i, p);

		pivot_multiplication(m, i, 1 / m->m[i][i]);

		pivot_annule(m, i);
	}
}

void pivot_gauss_complet(matrice* m) {
	pivot_gauss_simple(m);

	int i, j, vide;
	int l = m->p;

	for (i = 0; i < l; i++) {
		vide = 1;
		for (j = 0; j < (m->q - 1); j++) {
			if (m->m[i][j] != 0) {
				vide = 0;
				break;
			}
		}

		if (vide) {
			l -= 1;

			for (j = i; j < (m->p - 1); j++) {
				pivot_echange(m, j, j + 1);
			}
		}
	}
}

void pivot_solution(matrice *m) {
	pivot_gauss_complet(m);

	int c, l, cl, cr;
	float coeff;
	char signe;

	int vide = 1;
	for (int j = 0; j < (m->q - 1); j++) {
		if (m->m[m->p - 1][j] != 0) {
			vide = 0;
			break;
		}
	}

	if (vide && m->m[m->p - 1][m-> q - 1] != 0) {
		printf("La matrice est incompatible : %f != 0.\n", m->m[m->p - 1][m-> q - 1]);
	}

	for (c = 0, l = 0; c < (m->q - 1); c = cl, l++) {
		printf("x%d = %2.2f", c + 1, m->m[l][c]);
		for (cl = c + 1; m->m[l][cl] != 0 && cl < (m->q - 1); cl++) {
			coeff = -m->m[l][cl];

			signe = '+';
			if (coeff < 0) {
				signe = '-';
				coeff = -coeff;
			}

			printf(" %c %2.2f×x%d", signe, coeff, cl + 1);
		}

		for (cr = cl + 1; cr < (m->q - 1); cr++) {
			coeff = -m->m[l][cr];

			if (!coeff)
				continue;

			signe = '+';
			if (coeff < 0) {
				signe = '-';
				coeff = -coeff;
			}

			printf(" %c %2.2f×x%d", signe, coeff, cr + 1);
		}

		printf("\n");
	}
}
