#ifndef LISTE_OPERA_H
#define LISTE_OPERA_H

#include "opera.h"

struct maillon_s{
	opera * valeur;
	struct maillon_s * suivant;
};
typedef struct maillon_s maillon;

struct liste_s{
	struct maillon_s * debut;
	int taille;
};
typedef struct liste_s liste;

/** Fonction qui crée un maillon sans successeur et dont tous les champs sont à NULL */
/* et renvoie un pointeur vers ce maillon */
maillon * creer_maillon (void);
/** Fonction qui initialise la valeur d'un maillon avec son argument */
void initialiser_valeur_maillon (maillon * m, const opera * op);
/** Fonction qui libère tout l'espace mémoire occupé par le maillon pointé par m */
void detruire_maillon (maillon * m);
/** Fonction qui crée une liste, l'initialise à une liste vide */
/* et renvoie un pointeur vers la liste ainsi créée et initialisée */
liste * initialiser_liste_vide ();
/** Fonction qui renvoie 1 si la liste pointée par l est vide */
/* et renvoie 0 sinon */
int test_liste_vide (const liste * l);
/** Fonction qui ajoute le maillon pointé par m au début de la liste pointée par l */
void ajouter_maillon_debut_liste_debut (liste * l, maillon * m);
/** Fonction qui renvoie un pointeur vers le maillon qui occupe la position pos */
/* dans la liste pointée par l */
maillon * acceder_liste(const liste * l, int pos);
/** Fonction qui extrait le premier maillon de la liste pointée par l */
/* et renvoie son adresse */
maillon * extraire_maillon_debut_liste (liste * l);
/** Fonction qui affiche la valeur de chaque maillon de la liste pointée par l */
/* en respectant l'ordre des maillons */
void afficher_liste (const liste * l);
/** Fonction qui libère tout l'espace mémoire occupé par la liste pointée par l */
void detruire_liste (liste * l);
/** Fonction qui inverse l'ordre des maillons de la liste pointée par l */
void renverser_liste (liste * l);
/** Fonction qui trie la liste pointée par l */
/* selon l'ordre lexicographique des titres des opéras */
void trier_liste_titre (liste * l);
/** Fonction qui supprime de la liste pointée par l */
/* les opéras qui n'ont pas été créés au i_ème siècle */
void filtrer_liste_siecle (liste * l, int i);

/***********************************************************/
/**    FONCTIONS DE CHARGEMENT ET DE SAUVEGARDE           **/
/***********************************************************/
/** Fonction qui charge le contenu du fichier chemin dans la liste d'opéras l **/
liste * charger_liste_opera (const char * chemin);
/** Fonction qui sauvegarde le contenu de la liste d'opéras l dans le fichier chemin **/
void sauvegarder_liste_opera (const char * chemin, liste * l);

#endif
