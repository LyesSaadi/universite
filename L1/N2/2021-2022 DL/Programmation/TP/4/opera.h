#ifndef OPERA_H
#define OPERA_H

#include "date.h"
#include "individu.h"

typedef struct opera_s {
	char * titre; /* le titre de l'opéra */
	date * date_creation; /* l'année de la création (i.e. de la première représentation) */
	char * ville_creation; /* ville de la création (i.e. de la première représentation) */
	individu * compositeur; /* nom du compositeur */
} opera;

/** Fonction qui alloue de l'espace sur le tas pour une structure opéra */
/* et renvoie l'adresse de l'opéra ainsi créé */
opera * creer_opera (void);

/** Fonction qui initialise les champs de l'opéra pointé par le premier argument aux valeurs des autres arguments */
void initialiser_opera (opera *, const char *, const date *, const char *, const individu *);

/** Fonction qui libère toute la mémoire occupée par l'opéra pointé par l'argument */
void detruire_opera (opera *);

/** Fonction qui affiche le titre, le prénom et le nom du compositeur, */
/* (la date et la ville de création) de l'opéra pointé par l'argument */
void afficher_opera (const opera *);

/** Fonction qui échange les adresses d'opéra pointées par les arguments */
void echanger_opera (opera **, opera **);

#endif
