#include "date.h" /* Cette inclusion n'est pas indispensable */
#include "individu.h"
#include <string.h> /* pour strcpy */
#include <stdlib.h> /* pour malloc, free, sizeof */
#include <stdio.h> /* pour printf */

/** Fonction qui crée et initialise un individu champ par champ */
/** et renvoie l'adresse de l'individu ainsi créé et initialisé */
individu * creer_init_individu (const char * nom, const char * prenom, const date * naissance) {
	/* Allocation de la structure individu */
	individu * res = malloc(sizeof(individu));
	if (res == NULL) {
		perror("Échec allocation individu");
		exit(2);
	}
	/* Allocation et initialisation du nom de l'individu */
	res->nom = malloc(sizeof(char)*(strlen(nom)+1));
	if (res->nom == NULL) {
		perror("Échec allocation nom");
		exit(2);
	}
	strcpy(res->nom, nom);
	/* Allocation et initialisation du prénom de l'individu */
	res->prenom = malloc(sizeof(char)*(strlen(prenom)+1));
	if (res->prenom == NULL) {
		perror("Échec allocation prénom");
		exit(2);
	}
	strcpy(res->prenom, prenom);
	/* Initialisation de la date d ebnaissance de l'individu */
	 res->naissance = creer_date(naissance->jour, naissance->mois, naissance->annee);
	return res;
}

/** Fonction qui libère toute la mémoire occupée par l'individu pointé par ind */
void detruire_individu (individu * individu) {
	if (individu->nom != NULL)
		free(individu->nom);
	if (individu->prenom != NULL)
		free(individu->prenom);
	if (individu->naissance != NULL)
		detruire_date(individu->naissance);
	free (individu);
}

/** Fonction qui calcule le nombre d'années révolues de l'individu ind à la date d */
/** Par exemple, un individu né le 7 avril 1950 a 49 années révolues le 7 avril 2000 et en a 50 le 8 avril 2000 */
/** Complexité : \Theta(1) */
int age_revolu (const individu * ind, const date * d) {
	date * d_copie = creer_date(ind->naissance->jour, ind->naissance->mois, ind->naissance->annee);
	int res = d->annee - (d_copie->annee + 1);
	d_copie->annee += res + 1;
	if (comparer_date(d_copie, d) < 0)
		res ++;
	detruire_date(d_copie);
	if (res > 0) return res;
	return 0;
}

/* Fonction qui affiche le prénom, le nom et la date de naissance de l'individu pointé par ind */
void afficher_individu (const individu * ind) {
	printf("%s %s (", ind->prenom, ind->nom);
	afficher_date(ind->naissance);
	printf(")");
}
