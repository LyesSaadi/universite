#include <stdlib.h>
#include <stdio.h>

unsigned fibo(unsigned n, unsigned x, unsigned y) {
	if (n == 0)
		return x;
	else
		return fibo(n - 1, y, x + y);
}

int main(int argc, char *argv[]) {
	printf("%d\n", fibo(atoi(argv[1]), 0, 1));

	return 0;
}
