#include <stdio.h>

void f(short int i) {
	printf("%d\n", i);
	f(++i);
}

int main() {
	f(0);

	return 0;
}

// Récursivité terminale
int pgcd(unsigned m, unsigned n) {
	if (0 == m % n)
		return n;
	else
		// Le dernier appel (qui est récursif) de la fonction est la fonction
		// elle-même. Son appel récursif est terminal.
		return pgcd(n, m % n);
}

// Récursivité non-terminale
int somme(unsigned n) {
	if (n == 0)
		return 0;
	else
		// Ici, l'appel récursif n'est pas terminal, une addition est exécuté
		// après.
		return n + somme(n - 1);
}

// Le compilateur peut optimiser les récursivités terminales (avec l'option -O2
// avec gcc). Ça permet d'éviter l'explosion de la mémoire de la pile dû aux
// appels consécutifs. Il optimisera la récursivité terminale comme une fonction
// itérative (tail-call optimization).
