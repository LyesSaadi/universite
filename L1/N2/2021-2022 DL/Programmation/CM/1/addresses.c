#include <stdio.h>

int main() {
	int x;
	int *p = &x;

	printf("*p = %d\n", *p);

	x = 17;

	printf("*p = %d\n", *p);

	p = -1298177836;

	printf("*p = %d\n", *p);

	return 0;
}
