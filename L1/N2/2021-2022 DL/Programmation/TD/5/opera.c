#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct {
	unsigned jour;
	unsigned mois;
	int annee;
} date;

typedef struct {
	char* nom;
	char* prenom;
	date* naissance;
} individu;

typedef struct {
	char* titre;
	date* date_creation;
	char* ville_creation;
	individu* compositeur;
} opera;

void afficher_opera(const opera* op);
int age_revolu(const individu* ind, const date* d);
void recherche_tab_opera_age(const opera* tab[], int n, int a);
int date_comparer(const date* d1, const date* d2);
void trier_tab_opera_creation(opera* tab[], int n);
void pairs_a_gauche(int t[], size_t s);
int reorganiser_tab_opera_ville(opera* t[], size_t s, char* v);
void tri_par_insertion(int t[], size_t s);
void tri_par_fusion(int t[], size_t s);
void recherche_dicho_tab_opera_date(opera* tab[], int n, int a);
void inserer_opera_dans_trier(opera* t[], int s, opera* a_inserer);
void trier_tab_opera_compositeur(opera* tab[], int n);

int main() {
	date lyes_nai = {
		.jour = 18,
		.mois = 4,
		.annee = 2002
	};

	individu lyes = {
		.nom = "Saadi",
		.prenom = "Lyes",
		.naissance = &lyes_nai,
	};

	date dl_crea = {
		.jour = 1,
		.mois = 9,
		.annee = 2021,
	};

	opera dl = {
		.titre = "Double License",
		.date_creation = &dl_crea,
		.ville_creation = "Villetaneuse",
		.compositeur = &lyes,
	};

	date d1 = { 18, 4, 2002 };
	date d2 = { 23, 4, 2002 };
	date d3 = { 18, 5, 202 };
	date d4 = { 28, 1, 2002 };

	opera op1 = {
		.titre = "Opera 1",
		.date_creation = &d1,
		.ville_creation = "Quelque part",
		.compositeur = &lyes,
	};

	opera op2 = {
		.titre = "Opera 2",
		.date_creation = &d2,
		.ville_creation = "Quelque part",
		.compositeur = &lyes,
	};

	opera op3 = {
		.titre = "Opera 3",
		.date_creation = &d3,
		.ville_creation = "Quelque part",
		.compositeur = &lyes,
	};

	opera op4 = {
		.titre = "Opera 4",
		.date_creation = &d4,
		.ville_creation = "Quelque part 2",
		.compositeur = &lyes,
	};

	opera* tab_opera[] = { &dl, &op1, &op2, &op3, &op4 };

	/*
	trier_tab_opera_creation(tab_opera, 5);

	for (int i = 0; i < 5; i++) {
		afficher_opera(tab_opera[i]);
	}
	*/

	printf("%d\n", reorganiser_tab_opera_ville(tab_opera, 5, "Quelque part 3"));

	for (int i = 0; i < 5; i++) {
		afficher_opera(tab_opera[i]);
	}

	int t[10] = { 1, 3, 5, 7, 9, 19, 13, 15, 17, 2 };

	for (int i = 0; i < 10; i++) {
		printf("\t%d", t[i]);
	}
	printf("\n");

	pairs_a_gauche(t, 10);

	for (int i = 0; i < 10; i++) {
		printf("\t%d", t[i]);
	}
	printf("\n");

	tri_par_fusion(t, 10);

	for (int i = 0; i < 10; i++) {
		printf("\t%d", t[i]);
	}
	printf("\n");

	recherche_dicho_tab_opera_date(tab_opera, 5, 201);

	trier_tab_opera_compositeur(tab_opera, 5);

	for (int i = 0; i < 5; i++) {
		afficher_opera(tab_opera[i]);
	}

	return EXIT_SUCCESS;
}

void afficher_opera(const opera* op) {
	printf("opera {\n");
	printf("\ttitre : \"%s\",\n", op->titre);
	printf("\tdate_creation : %02d/%02d/%04d,\n", op->date_creation->jour, op->date_creation->mois, op->date_creation->annee);
	printf("\tvile_creation : \"%s\",\n", op->ville_creation);
	printf("\tcompositeur : individu {\n");
	printf("\t\tnom : \"%s\",\n", op->compositeur->nom);
	printf("\t\tprenom : \"%s\",\n", op->compositeur->prenom);
	printf("\t\tnaissance : %02d/%02d/%04d,\n", op->compositeur->naissance->jour, op->compositeur->naissance->mois, op->compositeur->naissance->annee);
	printf("\t},\n");
	printf("}\n");
}

int age_revolu(const individu* ind, const date* d) {
	int age = d->annee - ind->naissance->annee;

	if (ind->naissance->mois > d->mois || (ind->naissance->mois == d->mois && ind->naissance->jour > d->jour))
		age--;

	return age;
}

void recherche_tab_opera_age(const opera* tab[], int n, int a) {
	for (int i = 0; i < n; i++) {
		if (age_revolu(tab[i]->compositeur, tab[i]->date_creation) < a) {
			afficher_opera(tab[i]);
			printf("\n");
		}
	}
}

int date_comparer(const date* d1, const date* d2) {
	int d1sum, d2sum, sus;	

	d1sum = d1->jour + d1->mois * 100 + d1->annee * 10000;
	d2sum = d2->jour + d2->mois * 100 + d2->annee * 10000;

	sus = d1sum - d2sum;

	return (sus > 0) - (sus < 0);
}

void trier_tab_opera_creation(opera* tab[], int n) {
	opera **p, **q, **min, *temp;

	for (p = tab; p < tab + n; p++) {
		min = p;
		for (q = p + 1; q < tab + n; q++) {
			if (date_comparer((*min)->date_creation, (*q)->date_creation) != -1)
				min = q;
		}

		temp = *p;
		*p = *min;
		*min = temp;
	}
}

// Fonction qui n'a rien à voir, mais permet de modéliser l'algo pour la
// réorganisation de la ville.
void pairs_a_gauche(int t[], size_t s) {
	int i = 0, j = s - 1, temp;

	while (i != j) {
		if (t[i] % 2 == 0)
			i++;
		else if (t[j] % 2 == 1)
			j--;
		else {
			temp = t[i];
			t[i] = t[j];
			t[j] = temp;
		}
	}
}

int reorganiser_tab_opera_ville(opera* t[], size_t s, char* v) {
	int i = 0, j = s - 1;
	opera* temp;

	if (s <= 0) {
		return 0;
	}

	while (i != j) {
		if (strcmp(t[i]->ville_creation, v) == 0)
			i++;
		else if (strcmp(t[j]->ville_creation, v) != 0)
			j--;
		else {
			temp = t[i];
			t[i] = t[j];
			t[j] = temp;
		}
	}

	return i + (strcmp(t[i]->ville_creation, v) == 0);
}

void inserer_dans_trie(int t[], size_t s, int a_inserer) {
	int i = 0;
	for (i = s; i > 0 && t[i - 1] > a_inserer; --i)
		t[i] = t[i - 1];

	t[i] = a_inserer;
}

void tri_par_insertion(int t[], size_t s) {
	for (int i = 1; i < s; i++) {
		inserer_dans_trie(t, i, t[i]);
	}
}

void fusionner(int t[], size_t s1, size_t s2) {
	int* t_int = malloc(sizeof(int) * (s1 + s2));

	for (int i = 0, j = 0, k = s1; i < s1 + s2; i++) {
		if (k == s1 + s2) {
			t_int[i] = t[j];
			j++;
		} else if (j == s1) {
			t_int[i] = t[k];
			k++;
		} else if (t[j] < t[k]) {
			t_int[i] = t[j];
			j++;
		} else if (t[j] > t[k]) {
			t_int[i] = t[k];
			k++;
		}
	}

	for (int i = 0; i < s1 + s2; i++) {
		t[i] = t_int[i];
	}

	free(t_int);
}

void tri_par_fusion(int t[], size_t s) {
	if (s <= 1) {
		return;
	}
	
	tri_par_fusion(t, s / 2);
	tri_par_fusion(t + s / 2, s - s / 2);
	fusionner(t, s / 2, s - s / 2);
}

void recherche_dicho_tab_opera_date(opera* tab[], int n, int a) {
	if (n <= 0) {
		printf("Il n'y a pas d'opéra dont la première représentation était le %d.\n", a);
		return;
	}

	if (tab[0]->date_creation->annee == a) {
		afficher_opera(tab[0]);
		return;
	}

	recherche_dicho_tab_opera_date(tab + 1, n - 1, a);
}

void inserer_opera_dans_trier(opera* t[], int s, opera* a_inserer) {
	int i = 0;
	for (i = s; i > 0 && strcmp(t[i - 1]->compositeur->nom, a_inserer->compositeur->nom) == 1; --i)
		t[i] = t[i - 1];

	t[i] = a_inserer;
}

void trier_tab_opera_compositeur(opera* tab[], int n) {
	for (int i = 1; i < n; i++) {
		inserer_opera_dans_trier(tab, i, tab[i]);
	}
}
