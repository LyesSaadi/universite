#include <stdlib.h>
#include <stdio.h>

typedef struct maillon2 {
	float valeur;
	struct maillon2* suivant;
	struct maillon2* precedent;
} maillon2;

typedef struct liste2 {
	int taille;
	struct maillon2* premier;
	struct maillon2* dernier;
} liste2;

maillon2 *creer_maillon(float val);
void detruire_maillon(maillon2* m);
liste2 *initialiser_liste_vide();
int test_liste2_vide(const liste2* l);
void ajouter_maillon_debut_liste2(liste2* l, maillon2* m);
void ajouter_maillon_fin_liste2(liste2* l, maillon2* m);
maillon2* extraire_maillon_debut_liste2(liste2* l);
maillon2* extraire_maillon_fin_liste2(liste2* l);
void detruire_liste2(liste2* l);
void reorganiser_liste2(liste2* l, float v);

maillon2 *creer_maillon(float val) {
	maillon2* m = malloc(sizeof(maillon2));

	if (m == NULL) {
		return NULL;
	}

	m->valeur = val;
	m->suivant = NULL;
	m->suivant = NULL;

	return m;
}

void detruire_maillon(maillon2* m) {
	free(m);
}

liste2 *initialiser_liste_vide() {
	liste2* l = malloc(sizeof(liste2));

	if (l == NULL) {
		return NULL;
	}

	l->taille = 0;
	l->premier = NULL;
	l->dernier = NULL;

	return l;
}

int test_liste2_vide(const liste2* l) {
	return !l->taille;
}

void ajouter_maillon_debut_liste2(liste2* l, maillon2* m) {
	if (!test_liste2_vide(l)) {
		m->precedent = NULL;
		m->suivant = l->premier;
		l->premier->precedent = m;
		l->premier = m;
		l->taille++;
	} else {
		m->precedent = NULL;
		m->suivant = NULL;
		l->premier = m;
		l->dernier = m;
		l->taille = 1;
	}
}

void ajouter_maillon_fin_liste2(liste2* l, maillon2* m) {
	if (!test_liste2_vide(l)) {
		m->suivant = NULL;
		m->precedent = l->dernier;
		l->dernier->suivant = m;
		l->dernier = m;
		l->taille++;
	} else {
		m->precedent = NULL;
		m->suivant = NULL;
		l->premier = m;
		l->dernier = m;
		l->taille = 1;
	}
}

maillon2* extraire_maillon_debut_liste2(liste2* l) {
	maillon2* ex = l->premier;
	l->premier = ex->suivant;
	l->premier->precedent = NULL;
	ex->suivant = NULL;
	ex->precedent = NULL;

	return ex;
}

maillon2* extraire_maillon_fin_liste2(liste2* l) {
	maillon2* ex = l->dernier;
	l->dernier = ex->precedent;
	l->dernier->suivant = NULL;
	ex->suivant = NULL;
	ex->precedent = NULL;

	return ex;
}

void detruire_rec(maillon2* m) {
	if (m->suivant != NULL)
		detruire_rec(m->suivant);
	detruire_maillon(m);
}

void detruire_liste2(liste2* l) {
	detruire_rec(l->premier);
	l->premier = NULL;
	l->dernier = NULL;
	l->taille = 0;
}

void ech_val(maillon2* m1, maillon2* m2) {
	float tmp = m1->valeur;
	m1->valeur = m2->valeur;
	m2->valeur = tmp;
}

void reorganiser_liste2(liste2* l, float v) {
	for (maillon2 *d = l->premier, *f = l->dernier; d != f; ) {
		if (d->valeur < v) {
			d = d->suivant;
		} else if (f->valeur >= v) {
			f = f->precedent;
		} else {
			ech_val(d, f);
			d = d->suivant;
			f = f->precedent;
		}
	}
}
