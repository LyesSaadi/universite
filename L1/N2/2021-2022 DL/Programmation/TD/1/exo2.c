#include <stdlib.h>
#include <stdio.h>

void affiche_bin_rev(unsigned n);
void affiche_bin(unsigned n);
unsigned affiche_octet(unsigned n, unsigned i);
void affiche_bin_rec(unsigned n);

int main() {
    int n;
    
    printf("Entrez un nombre : ");
    scanf("%d", &n);
    
    affiche_bin_rev(n);
    
    return EXIT_SUCCESS;
}

void affiche_bin_rev(unsigned n) {
    while (n > 1) {
        printf("%d", n % 2);
        n = n / 2;
    };
    
    printf("%d\n", n);
}
                             
void affiche_bin(unsigned n) {
    int i = 1;
    
    while (n - i > 1) {
        printf("%d", n % 2);
        n = n / 2;
    };
    
    printf("%d\n", n);
}

unsigned affiche_octet(unsigned n, unsigned i) {
    unsigned mask = n << i;
    return (mask & n) >> i;
}
                         
void affiche_bin_rec(unsigned n) {}
