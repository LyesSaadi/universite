Ex.1 :
2 -
CsteNb 2
CsteBo True
BoToNb
AddiNb
CsteNb 2
NbToBo
ConJmp 2
CsteBo False
Jump 1
CsteNb False
BoToNb
GrEqBo
Halt

3-
GetVar x
TypeOf
Case
Jump 1
NbToBo
ConJmp 3
CsteBo False
SetVar y
Jump 8
GetVar x
TypeOf
Case
Jump 1
BoToNb
CsteNb 1
AddiNb
SetVar y
Halt

4- true
5- false


6- 
GetVar y
TypeOf
Case 16
  GetVar z
  TypeOf
  Case 4
    BoToNb
    Jump 2
    Noop
    Noop #fin cas z bool
    Swap
    BoToNb
    AddiNb
    Jump 29 #fin cas z nb
    Swap
    BoToSt
    Swap
    Concat
    Jump 24 #fin cas y bool
  GetVar z
  TypeOf
  Case 2
    BoToNb
    Noop #fin cas z bool
    AddiNb
    Jump 17 #fin cas z nb
    Swap
    NbToStr
    Swap
    Concat
    Jump 12 
    Noop
    Noop
    Noop
    Noop #fin cas y nb
  GetVar z
  TypeOf
  Case 2
    BoToStr
    Jump 2 #fin cas z bool
    NbToStr
    Noop #fin cas z nb
    Concat
Halt


Exercice 2 :

1-
#entete
DecVar f #pas obligatoire
NewClot 13
DecArg x
SetVar f
#main
GetVar f
StCall
GetVar x
GetVar y
AddiNb
SetArg
Call
CsteNb 3
Equal
SetVar z
Halt
#function f
GetVar x
GetVar y
SubsNb
Return

2-
#entete
DecVar f #pas obligatoire
NewClot 7
DecArg x
SetVar f
#main
GetVar x
CsteNb 3
Equal
SetVar z
Halt
#function f
GetVar x
GetVar y
SubsNb
Return


Exercice 3

2-
f(10,f(30,8))
function f (x,y) {
  if (x > 0)
    return (f(x-y,g(y)));
  return y+x;
  function g (x) {
  return (x+1);
  }
}


Exercice 4

#entete
DecVar g
DecVar f
DecVar compose
NewClot 19
DecArg x
SetVar g
NewClot 20
DecArg g
SetVar f
NewClot 48
DecArg f
DecArg g
SetVar compose
#main
GetVar f
StCall
GetVar g
SetArg
Call
StCall
CsteNb 4
SetArg
Call
Halt
#function g
GetVar x
GetVar x
AddiNb
Return
#function f
    GetVar g
    StCall
      CstNb 5
      SetArg
    Call
    CsteNb 30
  GrStNb
ConJmp 8
  GetVar compose
  StCall
    GetVar g
    SetArg
    GetVar g
    SetArg
  Call      # ou TlCall et enlever return
  Return
GetVar compose
StCall
  GetVar f
  SetArg
  GetVar f
  SetArg
Call
GetVar compose
StCall
  GetVar g
  SetArg
  GetVar g
  SetArg
Call      # ou TlCall et enlever return
Return
#fonction compose
#entete
DecVar h
NewClot 4
DecArg x
SetVar h
#corps compose
GetVar h
Return
#fonction h
GetVar f
StCall
  GetVar g
  StCall
    GetVar x
    SetArg
  Call
  SetArg
Call      # ou TlCall et enlever return
Return
