# Exercice 1
# 1.

CsteNb 25
CsteNb 2
CsteNb 3
MultNb
AddiNb
Halt

# 5.

   CsteNb 0.02
    CsteNb 7
     GetVar x
    MultNb
   AddiNb
   GetVar y
  AddiNb
    CsteNb 11
    CsteNb 0.2
   DiviNb
     GetVar x
    NegaNb
    CsteNb 7
   MultiNb
  AddiNb
 GeEqNb
Not
SetVar z
Halt

# Exercice 2
# 1.

     GetVar x
     ConJmp else
     CsteBo False
     SetVar x
     Jump   end
else GetVar x
     SetVar y
end  Halt

# 2.

GetVar x
ConJmp 4
CsteBo False
SetVar x
Jump   2
GetVar x
SetVar y
Halt

# 5.

    GetVar x
    CstNb 3
    CstNb 1.5
    NegaNb
    AddiNb
    GeEqNb
    ConJmp et
    GetVar x
    CsteNb 200
    GrStNb
et  ConJmp fin
    CsteNb 30
    SetVar y
tq  GetVar y
    GetVar x
    CsteNb 3
    SubsNb
    ConJmp fin
    GetVar y
    GetVar x
    SubsNb
    Jump tq
fin GetVar x
    SetVar y

# 5 alternatif.

    GetVar x
    CstNb 3
    CstNb 1.5
    NegaNb
    AddiNb
    GeEqNb
    ConJmp 3
    GetVar x
    CsteNb 200
    GrStNb
    ConJmp 11
    CsteNb 30
    SetVar y
    GetVar y
    GetVar x
    CsteNb 3
    SubsNb
    ConJmp 4
    GetVar y
    GetVar x
    SubsNb
    Jump -9
    GetVar x
    SetVar y


# Exercice 3

#2    # 2 + true >= (2 && false) ;
CsteNb 2
CsteBo true
BoToNb
AddiNb
CsteNb 2
NbToBo
ConJmp 2
CsteNb false
Jump   1
CsteNb false
BoToNb
GreEqNb

#3    # if (x) y = false; else y = x+1;
GetVar x
TypeOf
Case
Jump   1
NbToBo
ConJmp 3
CsteBo false
SetVar y
Jump   5
GetVar x
TypeOf
Case
BoToNb
SetVar y
Halt





