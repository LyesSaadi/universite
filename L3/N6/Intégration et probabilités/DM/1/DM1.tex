\documentclass[a4paper,11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{textcomp}

\newcommand{\eq}[1]{\underset{#1}{\sim}}

\title{Devoir Maison 1 d'Intégration et Probabilités}
\author{Lyes Saadi}
\date{\today}

\begin{document}

\maketitle

\section*{Exercice 1}

Pour $x, y \in [-1, 1]$, on pose $x \sim y$ si $x - y \in \mathbb{Q}$.

\paragraph{1.} Montrer que $\sim$ est une relation d'équivalence sur $[-1, 1]$.

\begin{proof}\leavevmode
\begin{itemize}
    \item \emph{Réflexivité :} Soit $x \in [-1, 1]$, $x \sim x \iff x - x = 0 \in \mathbb{Q}$, donc $\sim$ est réfléxif.
    \item \emph{Transitivié :} Soit $x, y, z \in [-1, 1]$, si $x \sim y \iff x - y \in \mathbb{Q}$ et $y \sim z \iff y - z \in \mathbb{Q}$, alors, $x - z = x + (- y + y) - z = (x - y) + (y - z) \in \mathbb{Q}$ par stabilité de l'addition des corps. Donc, $\sim$ est transitif.
    \item \emph{Symétrie :} Soit $x, y \in [-1, 1]$, $x \sim y \iff x - y \in \mathbb{Q} \iff\\ -(x - y) \in \mathbb{Q} \iff y - x \in \mathbb{Q} \iff y \sim x$, par stabilité de l'opposé dans les corps. Donc, $\sim$ est symétrique.
\end{itemize}
\end{proof}

\paragraph{2.} Soit $A$ un sous-ensemble de $[-1, 1]$ contenant exactement un élément de chaque classe d'équivalence. On pose
\[
    B := \bigcup_{r \in \mathbb{Q} \cap [-2, 2]} (r + A)\text{.}
\]

\subparagraph{(a)} Montrer que les $r + A$ sont deux à deux disjoints.

\begin{proof}
Par l'absurde: supposons que les $r + A$ ne sont pas deux à deux disjoints.

C'est à dire qu'il existe $r, r' \in \mathbb{Q} \cap [-2, 2]$ avec $r \not= r'$ tel que $r + A$ et $r' + A$ ne sont pas disjoints. Et donc, $\exists a \in (r + A) \cap (r' + A)$, et $\exists x,x' \in A$ tel que $a = r + x = r' + x'$. Or, $0 = a - a = r + x - (r' + x') = r-r' + x-x'$, d'où $x-x' = r'-r \in \mathbb{Q}$, d'où $x \sim x'$ et $x = x'$ vu que $A$ ne contient qu'un élément de chaque classe d'équivalence. On a donc $r + x = r' + x$, d'où $r = r'$, ce qui est absurde.
\end{proof}

\subparagraph{(b)} Montrer que $[-1, 1] \subset B \subset [-3, 3]$.

\begin{proof}\leavevmode
\begin{itemize}
\item[$\bullet$] $[-1, 1] \subset B$ :

Soit $x \in [-1, 1]$. D'après la définition de $A$, $\exists a \in A$ tel que $x \in [a]$, c'est à dire que $x \sim a$ et que $x - a \in \mathbb{Q}$. Et vu que $-1 \leq x \leq 1$ et $-1 \leq a \leq 1$ (car $a \in A \subset [-1, 1]$), on a, $-2 \leq x - a \leq 2$. Donc, $\exists r \in \mathbb{Q}\cap[-2, 2]$ tel que $x - a = r$, et donc, $x = r + a \in r + A \subset B$. D'où $[-1, 1] \subset B$.

\item[$\bullet$] $B \subset [-3, 3]$ :

Soit $x \in B$. Alors, $\exists r \in \mathbb{Q}\cap[-2, 2]$ tel que $x \in r + A$, et, $\exists a \in A$ tel que $x = r + a$. Et, $a \in A \subset [-1, 1]$, d'où $-1 \leq a \leq 1$, et, $r \in \mathbb{Q}\cap[-2, 2] \subset [-2, 2]$, d'où $-2 \leq r \leq 2$. D'où, $-3 \leq r + a \leq 3$, donc, $x \in [-3, 3]$. D'où $B \subset [-3, 3]$.

\end{itemize}
\end{proof}

\subparagraph{(c)} Montrer en raisonnant par l'absurde que $A$ n'est pas borélien.

\begin{proof}
Supposons que $A$ est borélien.

Alors, $\lambda(A)$ existe. Et, d'après la proposition $1.3.7$, $\forall r \in \mathbb{Q} \cap [-2, 2]$, $\lambda(r + A) = \lambda(A)$.

On a donc,
\[
\begin{aligned}
    \lambda(B) &= \lambda\left(\bigcup_{r \in \mathbb{Q} \cap [-2, 2]} (r + A)\right)\\
    &= \sum_{r \in \mathbb{Q} \cap [-2, 2]} \lambda(r+A)\ \text{(par sigma-additivité et la question 2. (a))}\\
    &= \sum_{r \in \mathbb{Q} \cap [-2, 2]} \lambda(A)
\end{aligned}
\]

\begin{itemize}
\item[$\bullet$] Supposons que $\lambda(A) = 0$ :

Alors, $\lambda(B) = 0$. Pourtant, $\lambda([-1, 1]) = 2$ et $[-1, 1] \subset B$, d'où $2 = \lambda([-1, 1]) \leq \lambda(B) = 0$, ce qui est absurde.

\item[$\bullet$] Supposons que $\lambda(A) > 0$ :

Alors, $\lambda(B) = +\infty$ parce que $card(\mathbb{Q} \cap [-2, 2]) = +\infty$. Pourtant, $\lambda([-3, 3]) = 6$ et $B \subset [-3, 3]$, d'où $+\infty = \lambda(B) \leq \lambda([-3, 3]) = 6$, ce qui est absurde.

Donc, $A$ n'est pas borélien.

\end{itemize}

\end{proof}

\section*{Exercice 2}

On considère une suite de fonctions $(f_n)$ définies pour tout $n \in \mathbb{N}*$ et tout $x \in \mathbb{R}$ par
\[
    f_n(x) := \left(1 + \frac{x^2}{n}\right)^{-\frac{n+1}{2}}\text{.}
\]

On rappelle que
\[
    \int_{0}^{\infty} e^{-\frac{x^2}{2}}.dx = \sqrt{\frac{\pi}{2}}\text{.}
\]

\paragraph{1.}
\subparagraph{(a)} Soit $p \in [1, +\infty[$. Montrer que $(1+u)^p \geq 1+pu$, pour tout $u \in \mathbb{R}_+$.

\begin{proof}
Pour trouver l'inégalité, étudions $h(u) = (1+u)^p - (1 + pu)$.

On a $h'(u) = p(1+u)^{p-1} - p$. Or,
\[
\begin{aligned}
    u &\geq 0\\
    1 + u &\geq 1\\
    (1 + u)^{p-1} &\geq 1^{p-1} = 1 &\text{ (car }p - 1 \geq 0\text{)}\\
    p(1 + u)^{p-1} &\geq p &\text{ (car }p \geq 1 > 0\text{)}\\
    p(1 + u)^{p-1} - p &\geq 0\\
    h'(u) &\geq 0\\
\end{aligned}
\]

Donc, $h$ est croissante sur $\mathbb{R}_+$, et $h(0) = (1 + 0)^p - (1 + p\times 0) = 1^p - 1 = 0$. D'où, $\forall u \geq 0$, $h(u) \geq h(0) = 0$. D'où $\forall u \in \mathbb{R}_+$, $(1+u)^p \geq 1+pu$.
\end{proof}

\subparagraph{(b)} En déduire que $f_n(x) \leq \frac{1}{1+\frac{x^2}{2}}$, pour tout $x \in \mathbb{R}$, $n \in \mathbb{N}^*$.

\begin{proof} Soit $p \in [1, +\infty[$ et $u \in \mathbb{R}_+$.

D'après la question précédente, on a $(1+u)^p \geq 1+pu \geq 1$, d'où,\\$0 \leq (1+u)^{-p} \leq \frac{1}{1+pu} \leq 1$.

En posant $u = \frac{x^2}{n} \geq 0$ et $p = \frac{n + 1}{2} \geq 1$, on a donc :

\[
    \left(1+\frac{x^2}{n}\right)^{-\frac{n + 1}{2}} \leq \frac{1}{1+\frac{n + 1}{2}\frac{x^2}{n}}
\]

D'où :

\[
    f_n(x) \leq \frac{1}{1+\frac{x^2}{2}+\frac{x^2}{2n}}
\]

Or, $1+\frac{x^2}{2}+\frac{x^2}{2n} \geq 1+\frac{x^2}{2} \geq 1$, d'où, $\frac{1}{1+\frac{x^2}{2}+\frac{x^2}{2n}} \leq \frac{1}{1+\frac{x^2}{2}}$.

Donc :

\[
    f_n(x) \leq \frac{1}{1+\frac{x^2}{2}}
\]

\end{proof}

\paragraph{2.} On pose pour $n \in \mathbb{N}^*$, $a_n := \int_{\mathbb{R}} f_n(x).dx$.
\subparagraph{(a)} Justifier que cette intégrale existe et est finie.

\begin{proof}
$\forall x \in \mathbb{R}$, $f_n(x)$ est positive et est continue sur $\mathbb{R}$, donc, mesurable sur $\mathbb{R}$. Donc, l'intégrale existe. De plus, d'après la question précédente, on a $0 \leq f_n(x) \leq \frac{1}{1+\frac{x^2}{2}}$. Or, $\frac{1}{1+\frac{x^2}{2}}$ est Riemann-intégrable sur $\mathbb{R}$. Et,

\[
\begin{aligned}
    \int_{\mathbb{R}} \frac{1}{1+\frac{x^2}{2}}.dx &= 2\int_{0}^{+\infty} \frac{1}{1+\frac{x^2}{2}}.dx &\text{(car la fonction est paire)}\\
    &= 2\int_{0}^{+\infty} \frac{1}{1+\frac{x^2}{2}}.dx\\
\end{aligned}
\]

Étudions cette intégrale. Il existe un problème en $+\infty$. Or, $1+\frac{x^2}{2} \eq{+\infty} \frac{x^2}{2}$. Donc, $\frac{1}{1+\frac{x^2}{2}} \eq{+\infty} \frac{2}{x^2}$, et, d'après le critère de Riemann, l'intégrale $\int_{1}^{+\infty} \frac{2}{x^2}.dx$ converge, donc, par le critère d'équivalence, $\int_{1}^{+\infty} \frac{1}{1+\frac{x^2}{2}}.dx$ converge, et vu que $\int_{0}^{1} \frac{1}{1+\frac{x^2}{2}}.dx$ converge (l'intégrale est bien définie partout), alors, $\int_{0}^{+\infty} \frac{1}{1+\frac{x^2}{2}}.dx$ converge, et donc, par le critère de comparaison, $\int_{\mathbb{R}} f_n(x).dx$ converge.

Donc, cette intégrale $\int_{\mathbb{R}} f_n(x).dx$ existe et est finie.
\end{proof}

\subparagraph{(b)} Déterminer $\lim\limits_{n\to\infty} a_n$.

\begin{proof} Nous allons appliquer le Théorème de Convergence Dominée.

Cherchons avant tout la limite de $f_n$.

\[
    f_n(x) = \left(1 + \frac{x^2}{n}\right)^{-\frac{n+1}{2}} = e^{-\frac{n+1}{2}\ln\left(1 + \frac{x^2}{n}\right)}
\]

Or, $\lim\limits_{n\to+\infty} \frac{x^2}{n} = 0$, et $\ln(1+x) \eq{x \to 0} x$. D'où $\ln\left(1+\frac{x^2}{n}\right) \eq{n \to +\infty} \frac{x^2}{n}$, donc, $-\frac{n+1}{2}\ln\left(1+\frac{x^2}{n}\right) \eq{n \to +\infty} -\frac{n+1}{2}\frac{x^2}{n} \eq{n \to +\infty} -\frac{x^2}{2}$ (car $\lim\limits_{n\to+\infty} \frac{n+1}{n} = 1$).

D'où $f_n(x) \eq{n \to +\infty} e^{-\frac{x^2}{2}}$.

Donc, $\lim\limits_{n\to+\infty} f_n(x) = e^{-\frac{x^2}{2}}$ (car $e^{-\frac{x^2}{2}}$ ne dépend pas de $n$).

Ensuite, d'après la question \textbf{1. (b)}, nous avons $|f_n(x)| = f_n(x) \leq \frac{1}{1+\frac{x^2}{2}}$ (car $f_n(x)$ est positive), avec $\frac{1}{1+\frac{x^2}{2}}$ intégrable car convergente comme ça l'a été démontré dans la question précédente.

On peut donc appliquer le Théorème de Convergence Dominée. Et on a :
\[
    \lim\limits_{n\to +\infty} a_n = \int_{\mathbb{R}} e^{-\frac{x^2}{2}} = 2 \int_{0}^{+\infty} e^{-\frac{x^2}{2}} = 2\sqrt{\frac{\pi}{2}}
\]

\end{proof}

\paragraph{3.} Montrer que pour tout $\lambda \geq 1/2$, $\lim\limits_{n\to\infty} \int_{\mathbb{R}} e^{\lambda x^2} f_n(x).dx = +\infty$.

\begin{proof}
On a que $x \mapsto e^{\lambda x^2} f_n(x)$ est positive et continue sur $\mathbb{R}$, donc, mesurable sur $\mathbb{R}$.

Et, $\lim\limits_{n\to\infty} e^{\lambda x^2} f_n(x) = e^{\lambda x^2} e^{-\frac{x^2}{2}} = e^{(\lambda - \frac{1}{2}) x^2}$.

On a donc, en appliquant le lemme de Fatou :
\[
 \int_{\mathbb{R}} \liminf\limits_{n\to\infty} e^{\lambda x^2} f_n(x).dx = \int_{\mathbb{R}} e^{(\lambda - \frac{1}{2}) x^2}.dx \leq \liminf\limits_{n\to\infty} \int_{\mathbb{R}} e^{\lambda x^2} f_n(x).dx
\]

Or, $\forall x \in \mathbb{R}$
\[
1 = e^{(\frac{1}{2} - \frac{1}{2}) x^2} \leq e^{(\lambda - \frac{1}{2}) x^2} \text{ (car } 1/2 \leq \lambda \text{)}
\]

D'où,
\[
    +\infty = \int_{\mathbb{R}} 1.dx \leq \int_{\mathbb{R}} e^{(\lambda - \frac{1}{2}) x^2}.dx \leq \liminf\limits_{n\to\infty} \int_{\mathbb{R}} e^{\lambda x^2} f_n(x).dx
\]

Donc,
\[
\liminf\limits_{n\to\infty} \int_{\mathbb{R}} e^{\lambda x^2} f_n(x).dx = +\infty
\]

Et vu qu'on a toujours $\liminf\limits_{n\to\infty} \int_{\mathbb{R}} e^{\lambda x^2} f_n(x).dx \leq \limsup\limits_{n\to\infty} \int_{\mathbb{R}} e^{\lambda x^2} f_n(x).dx$, par propriété de la $\liminf$ et $\limsup$, on a bien :
\[
\liminf\limits_{n\to\infty} \int_{\mathbb{R}} e^{\lambda x^2} f_n(x).dx = \limsup\limits_{n\to\infty} \int_{\mathbb{R}} e^{\lambda x^2} f_n(x).dx = +\infty
\]

D'où :
\[
\lim\limits_{n\to\infty} \int_{\mathbb{R}} e^{\lambda x^2} f_n(x).dx = +\infty
\]
\end{proof}

\end{document}