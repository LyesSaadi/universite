\subsubsection{\'Epimorphismes et applications surjectives}

Dans la section qui suit, on \'etudie une autre classe de morphismes:
les \'epimorphismes. On va voir qu'ils sont obtenus de mani\`ere
duale aux monomorphismes, et permettent de g\'en\'eraliser la notion
d'application surjective dans la cat\'egorie des ensembles.

On rappelle que, si $f : A \to B$ est une fl\`eche dans une
cat\'egorie $\mathcal{C}$, alors on peut \'egalement voir $f$ comme une fl\`eche
$B \to A$ dans la cat\'egorie oppos\'ee
$\mathcal{C}^\mathrm{op}$. Pour \'eviter toute ambiguit\'e on appelle
cette deuxi\'eme fl\`eche $f^\mathrm{op}$. 

\begin{defi}[\'Epimorphisme: d\'efinition par dualit\'e]
Une flèche \(f : A \to B\) est un \emph{épimorphisme} (ou
\emph{\'epi}) dans une catégorie \(\mathcal{C}\) si la fl\'eche
 $f^\mathrm{op}$ est un monomorphisme dans la catégorie opposée
\(\mathcal{C}^\mathrm{op}\). 
\end{defi}

\begin{prop}[\'Epimorphisme: caract\'erisation directe]
    Un morphisme \( f : A \to B \) dans une catégorie est un
    épimorphisme si et seulement si, pour tout objet \( Z \) et pour tout couple de morphismes \( h, k : B \to Z \) tels que \( h \circ f = k \circ f \), alors \( h = k \).
\end{prop}
\begin{proof}
Il suffit d'\'etudier la propri\'et\'e des monomorphismes dans $\mathcal{C}^\mathrm{op}$. La
fl\`eche $f^\mathrm{op}: B \to A$ est un mono si, pour tout objet $Z$ et
pour toute paire de morphismes $h^\mathrm{op}, k^\mathrm{op} : Z \to
B$, $f^\mathrm{op} \circ h^\mathrm{op} = f^\mathrm{op} \circ
k^\mathrm{op}$ implique $h^\mathrm{op} = k^\mathrm{op}$. Mais la
composition dans $\mathcal{C}^\mathrm{op}$ est celle de $\mathcal{C}$
vue \`a l'envers, donc la propri\'et\'e correspond \`a dire que, pour
toute paire de morphismes $h, k : B \to Z$ dans $\mathcal{C}$, si $h
\circ f = k \circ f$ alors $h = k$. 
\end{proof}

On montre maintenant que, dans les ensembles, les \'epimorphismes sont
les applications surjectives, dont on rappelle la d\'efinition. 

\begin{defi}[Application surjective] Soit \(f : A \to B\) une
    application ensembliste. On dit que \(f\) est \emph{surjective} si pour
    tout \(y \in B\), il existe \(x \in A\) tel que \(f(x) = y\).
      \end{defi}

\begin{prop}
Dans la catégorie $\mathcal{Ens}$ des ensembles,  tout morphisme $f : A \to B$ vérifie :
\[ f \text{ est un épimorphisme} \iff f \text{ est surjective}. \]
\end{prop}

\begin{proof}
Démontrons l'implication dans les deux sens.

($\Rightarrow$) Supposons que $f$ soit un épimorphisme.
Supposons par l'absurde qu'il existe un élément $b \in B$ pour lequel
il n'existe pas d'élément $a \in A$ tel que $f(a) = b$.
On note $1_{\text{Im}(A)} : B \to \{ 0, 1\}$ la fonction indicative du
sous-ensemble $\mathrm{Im}(A)$, et on construit une fonction $h : B \to \{0, 1\}$ comme suit :
\[ h(y) = \begin{cases} 
      1 & \text{si } y = b~, \\
      1_{\text{Im}(A)}(y) & \text{sinon}~.
   \end{cases}
 \]
 On a donc la situation suivante:
\begin{center}
\begin{tikzpicture}
  \node (A) at (0,0) {$A$};
  \node (B) at (3,0) {$B$};
  \node (C) at (6,0) {$\{0, 1\}$~.};
  \draw[->, >=Stealth] (A) -- node[above] {$f$} (B);
  \draw[->, >=Stealth] (B) to[bend left] node[above] {$h$} (C);
  \draw[->, >=Stealth] (B) to[bend right] node[below] {$1_{\text{Im}(A)}$} (C);
\end{tikzpicture}
\end{center}
On a les \'egalit\'es suivantes:
\begin{align*}
&(h \circ f)(a) = h(f(a)) = h(b) = 1 & \text{ si } f(a) = b~, \\
 &(h \circ f)(a) = h(f(a)) = 1_{\text{Im}(A)}(f(a))&  \text{ si } f(a) \neq b~, 
\end{align*}
et donc
\[ (1_{\text{Im}(A)} \circ f)(a) = 1_{\text{Im}(A)}(f(a))~. \]
Puisqu'il n'existe pas de $a$ tel que $f(a)= b$, on a que, pour tout
$a \in A$,  $(h \circ f)(a) = (1_{\text{Im}(A)} \circ f)(a)$, et donc
$h \circ f = 1_{\text{Im}(A)} \circ f$. Puisque $f$ est un \'epi, on
doit donc avoir $h = 1_{\text{Im}(A)}$, une contradiction. Ainsi,
notre hypothèse selon laquelle il existe un élément $b \in B$ pour
lequel il n'existe pas d'élément $a \in A$ tel que $f(a) = b$ est
incorrecte, et par conséquent, $f$ est surjective.

\medskip

($\Leftarrow$) Supposons maintenant que $f$ soit surjective. Pour
montrer que $f$ est un épimorphisme, prenons deux morphismes $h, k : B
\to Z$ pour un objet $Z$. Supposons que $h \circ f = k \circ f$. Nous
devons montrer que $h = k$. Pour tout $y \in B$, puisque $f$ est
surjective, il existe $x \in A$ tel que $f(x) = y$, et par cons\'equent
\[
  h(y) = h(f(x)) = k(f(x)) = k(y).
  \] 
 Donc $h = k$ et $f$ est un \'epi. 
\end{proof}

\begin{exem}
Considérons l'application de projection $\pi_1 : \mathbb{R}^2 \to \mathbb{R}$ dans la catégorie des espaces vectoriels définie par
\[
\pi_1(x, y) = x~. 
\]
Montrons que c'est un épimorphisme. Soit $f, g : \mathbb{R} \to V$ deux applications linéaires vers un espace vectoriel $V$ telles que $f \circ \pi_1 = g \circ \pi_1$. Alors, pour tout $x \in \mathbb{R}$, nous avons :
\[
f(x) = f(\pi_1(x, 0)) = (f \circ \pi_1)(x, 0) = (g \circ \pi_1)(x, 0) = g(\pi_1(x, 0)) = g(x)~. 
\]
Ainsi, $f = g$, ce qui montre que $\pi_1$ est un épimorphisme dans la catégorie des espaces vectoriels.
\end{exem}

\begin{rema}
Cet exemple peut \^etre adapt\'e pour montrer que, pour toute
cat\'egorie <<concr\`etes>> (dans le sens o\`u les morphismes sont des
applications ensemblistes), tout morphisme correspondant \`a une
application surjective est un \'epi. La r\'eciproque n'est pas vraie
en g\'en\'eral. 
\end{rema}

\begin{defprop}[Sous-catégorie des épimorphismes]
Soit $\mathcal{C}$ une catégorie. La \emph{sous-catégorie des épimorphismes} de $\mathcal{C}$, notée $\mathcal{C}_{\text{epi}}$, est la sous-catégorie de $\mathcal{C}$ définie par les données suivantes. 

\begin{itemize}[label=\textbullet]
    \item Les objets de $\mathcal{C}_{\text{epi}}$ sont les mêmes que ceux de $\mathcal{C}$.
    
    \item Pour chaque paire d'objets $A$ et $B$ de $\mathcal{C}$, si $f : A \to B$ est une flèche (morphisme) dans $\mathcal{C}$ et que $f$ est un épimorphisme dans $\mathcal{C}$, alors $f$ est également une flèche dans $\mathcal{C}_{\text{epi}}$.
    
    \item La composition des flèches dans $\mathcal{C}_{\text{epi}}$ est la même que celle de $\mathcal{C}$.
    
    \item Les flèches <<identité>> dans $\mathcal{C}_{\text{epi}}$ sont les mêmes que celles de $\mathcal{C}$.
\end{itemize}
\end{defprop}

\begin{proof}
La démonstration est similaire est celle de la définition/proposition~\ref{defprop:SousCatMono}. Elle s'obtient formellement à partir de celle-ci en passant à la catégorie opposée. 
\end{proof}

\begin{defi}[\'Epimorphisme fort]
    Soit $\mathcal{C}$ une catégorie. Un \emph{épimorphisme fort}
    dans $\mathcal{C}$ est une flèche $f : A \to B$ telle qu'il existe un $g: B \to A$ avec $f \circ g = \mathrm{id}_B$.
\end{defi}
\begin{center}
\begin{tikzcd}
A \arrow[r, "\exists g"] \arrow[rd, "\mathrm{id}_A"'] & B \arrow[d, "f"] \\
& A
\end{tikzcd}
\end{center}

\begin{prop}\label{prop:epifort}
Tout épimorphisme fort est un épimorphisme. 
\end{prop}
\begin{proo}
On commence par observer que $f$ est un \'epimorphisme fort si et
seulement si $f^\mathrm{op}$
est un monomorphisme fort dans $\mathcal{C}^\mathrm{op}$. Par la
Proposition~\ref{prop:monofort}, $f^\mathrm{op}$ est un monomorphisme,
et donc, par d\'efinition, $f$ est un \'epimorphisme.
\end{proo}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "Categories"
%%% End:
