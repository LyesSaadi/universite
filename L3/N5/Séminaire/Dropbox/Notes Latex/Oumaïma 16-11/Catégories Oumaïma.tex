\documentclass[twoside, 11pt]{amsart}
\usepackage{amsmath}
\usepackage{tikz-cd} 
\include{package}
\begin{document}

\title{Vers la gare du Nord ...}

\maketitle

\begin{theo}[Lemme de Yoneda]
Pour tout objet $c$ de $\mathcal{C}$ et tout foncteur $F : \mathcal{C} \rightarrow \mathcal{Ens}$, on dispose d'une bijection naturelle en c et en $F$ :
\begin{align*}
\mathrm{Nat}\left(\mathcal{C}(c,-),F\right) & \overset{\sim}{\longrightarrow} F(c) \\
\psi & \longmapsto \psi_c(\mathrm{id}_c).
\end{align*}
\end{theo}

\begin{proof}
Injectivité:
Considérons $\psi$ une transformation naturelle de $\mathcal{C}(c, -)$ sur $F$. Pour tout élément $x$ dans $\mathcal{C}(c, -) = \mathrm{Hom}_{\mathcal{C}}(c, d)$, on a :
$x = \mathcal{C}(c, d)(x)(\mathrm{id}_c)$.

En appliquant à cette identité l'application ensembliste $\psi(d) : \mathcal{C}(c, d) \rightarrow F(d)$, on obtient :
\[
\psi(d)(x) = \psi(d)\left[ \mathcal{C}(c, d)(\mathrm{id}_c) \right] = F(x)\left[ \psi(c)(\mathrm{id}_c) \right],
\]
où la seconde égalité vient de la définition d'une transformation naturelle. L'élément $\psi(d)(x)$ est donc l'image de $\psi(c)(\mathrm{id}_c)$ par $F(x)$. De fait, en faisant varier $x$, on montre que $\psi$ est uniquement déterminée par $\psi(c)(\mathrm{id}_c)$. L'application énoncée est injective.

Surjectivité :
Soit un élément $y$ de $F(c)$. La preuve de l'injectivité permet de deviner un antécédent de $y$ (forcément unique). Pour tout objet $d$ de $\mathcal{C}$, définissons :
\[
\psi_y(d) : \mathcal{C}(c, d) \rightarrow F(d),
\]
\[
x \mapsto F(x)(y).
\]
Vérifions que $\psi_y$ est bien une transformation naturelle. Pour toute flèche $g : d \rightarrow c$ et pour tout élément $x$ de $\mathcal{C}(c, d)$, on est en mesure d'écrire :
\[
F(g)\left[\psi_y(d)(x)\right] = F(g)\left[F(x)(y)\right] = F(g \circ x)(y) = \psi_y(c)(g \circ x).
\]
Or, la composée $g \circ x$ peut être regardée comme l'image de $x$ par $\mathcal{C}(c, -)(g)$. Donc, l'identité obtenue se réécrit :
\[
F(g)\left[\psi_y(d)(x)\right] = \psi_y(c)\left[\mathcal{C}(c, -)(g)(x)\right].
\]
En faisant varier $x$ :
\[
F(g) \circ \psi_y(d) = \psi_y(c) \circ \mathcal{C}(c, -)(g).
\]
Cela étant vérifié pour toute flèche $g$, $\psi_y$ est bien une transformation naturelle de $\mathcal{C}(c, -)$ sur $F$ et son image est presque par définition $y$

\end{proof}

\begin{coro} [Prolengement de Yoneda]

Soit $\mathcal{C}$ une catégorie petite. Le foncteur $F : \mathcal{C}^{op} \rightarrow \text{Fon}(\mathcal{C},\mathcal{Ens})$, appelé foncteur de Yoneda, qui envoie tout objet $C$ de $\mathcal{C}$ sur le foncteur $\mathcal{C}(C, -) : \mathcal{C} \rightarrow \mathcal{Ens}$, est pleinement fidèle.
En particulier, dans une catégorie $\mathcal{C}$, un morphisme $f : c \rightarrow d$ est un isomorphisme si et seulement si la transformation naturelle induite par $F(f) : \mathcal{C}(d, -) \rightarrow \mathcal{C}(c, -)$ est 

\end{coro}

\begin{proof}
Si $c$ et $d$ sont deux objets de $\mathcal{C}$, le lemme de Yoneda établit une bijection fonctorielle entre $\text{Nat}(\mathcal{C}(c, -), \mathcal{C}(d, -))$ et $\mathcal{C}(d, c) = \mathcal{C}(d, c)$ qui envoie $1_{\mathcal{C}(c, -)}$ sur $1_c$ lorsque $d = c$. Donc le foncteur de Yoneda est pleinement fidèle.

Soit $f : c \rightarrow d$ un morphisme dans $\mathcal{C}$. Comme le foncteur $F$ est pleinement fidèle, si $f$ est inversible, alors $F(ff^{-1}) = F(f^{-1})F(f) = 1_{\mathcal{C}(d, c)}$, et $F(f^{-1}f) = F(f)F(f^{-1}) = 1_{\mathcal{C}(c, d)}$. D’où $F(f)$ est un isomorphisme dans la catégorie de foncteurs $\text{Fon}(\mathcal{C}, \mathcal{Ens})$. Réciproquement, si $F(f)$ est un isomorphisme de foncteurs et si $\psi$ est son inverse, correspondant à un morphisme $g : d \rightarrow c$ par le lemme de Yoneda (c'est-à-dire on a $F(g) = \psi$), on a alors $F(fg) = F(g)F(f) = \psi F(f) = 1_{\mathcal{C}(d, c)}$ et $F(gf) = F(f)F(g) = 1_{\mathcal{C}(c, d)}$. D’où $fg = 1_d$ et $gf = 1_c$, ce qui montre que $f$ est un isomorphisme dans $\mathcal{C}$.


 \[
\begin{array}{cccccc}
\text{Nat}(\mathcal{C}(c, -), \mathcal{C}(d, -)) & \xrightarrow{\quad \text{Yoneda} \quad} & \mathcal{C}(d, c) & & \text{Nat}(\mathcal{C}^{op}(d, -), \mathcal{C}^{op}(c, -)) & \xrightarrow{\quad \text{Yoneda} \quad} & \mathcal{C}^{op}(c, d) \\
\downarrow{\quad \cong \quad} & & \downarrow{\quad \cong \quad} & & \downarrow{\quad \cong \quad} & & \downarrow{\quad \cong \quad} \\
1_{\mathcal{C}(c, -)} & \xrightarrow{\quad \text{Yoneda} \quad} & 1_c & & 1_{\mathcal{C}^{op}(d, -)} & \xrightarrow{\quad \text{Yoneda} \quad} & 1_d
\end{array}
\]

\end{proof}

\begin{coro}
le foncteur de Yoneda établit une équivalence entre la catégorie originale et la catégorie opposée des foncteurs représentables. Plus précisément, pour toute catégorie $\mathcal{C}$, le foncteur de Yoneda $y: \mathcal{C}^{op} \to \text{Func}(\mathcal{C}, \text{Set})$ est pleinement fidèle, ce qui signifie qu'il préserve les isomorphismes. Ainsi, il établit une équivalence entre la catégorie opposée de $\mathcal{C}$ et la catégorie des foncteurs représentables sur $\mathcal{C}$
    
\[
\begin{array}{l}
\text{Image}_{\text{Objets}}(\text{Yoneda}) = \{\text{Foncteurs de représentation (représentable)}\} \\
\phantom{} \\
\hspace{-2em}\Bigg\downarrow \\
C^{op} \xleftarrow{\quad \text{Yoneda} \quad} \text{Fonceurs représentables}
\end{array}
\]
Équivalence des catégories 

\[
\text{Soient } c, c' \text{ dans } \mathcal{C} \text{ tels que } \mathcal{C}(c,-) = \mathcal{C}(c',-) \text{ pour } - = c, \quad \mathcal{C}(c,c) = \mathcal{C}(c',c)
\] 
Donc c=c'
\end{coro}
\end{document}
