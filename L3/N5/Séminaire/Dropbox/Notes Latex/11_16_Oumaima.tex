\subsubsection{Vers la gare du Nord ...}

On peut remarquer que si $\mathrm{F} \colon \mathcal{C} \to \mathcal{Ens}$ est un foncteur représentable par un objet  $C$ de $\mathcal{C}$, alors tout isomorphisme naturel $\mathrm{F} \cong \mathcal{C}(C, -)$ définit une bijection entre $\mathrm{F}(C)$ et $\mathcal{C}(C, C) = \mathrm{Hom}_{\mathcal{C}}(C, C)$. L'élément $x$ de $\mathrm{F}(C)$ correspondant canoniquement à l'identité $\mathrm{id}_C \in \mathcal{C}(C, C)$ est unique à isomorphisme près. Il s'agit d'un élément universel associé au foncteur représentable $\mathrm{F}$.
On peut alors voir que l'isomorphisme naturel $\mathcal{C}(C, -)\cong \mathrm{F}$ est donné par 
$f\in \mathcal{C}(C, D) \mapsto \mathrm{F}(f)(x)$. Le résultat fondamental suivant montre que cette propriété se généralise au cas général où la transformation naturelle est quelconque. 

\begin{theo}[Lemme de Yoneda]\label{theo:LemmeYoneda}
Pour tout objet $C$ d'une catégorie localement finie $\mathcal{C}$ et tout foncteur $\mathrm{F} : \mathcal{C} \rightarrow \mathcal{Ens}$, il existe une bijection 
\[
\mathrm{Nat}\left(\mathcal{C}(C,-), \mathrm{F}\right) \cong \mathrm{F}(C) 
\]
naturelle en $C$ et en $\mathrm{F}$, explicitement donnée par 
\[\varphi  \longmapsto \varphi_C(\mathrm{id}_C)~.\]
\end{theo}

\begin{proof}
Notons $\Phi \colon \mathrm{Nat}\left(\mathcal{C}(C,-), \mathrm{F}\right) \to \mathrm{F}(C)$ l'application 
définie par $\varphi  \longmapsto \varphi_C(\mathrm{id}_C)$~.
Dans l'autre sens, on considère l'application 
$\Psi \colon \mathrm{F}(C)\to \mathrm{Nat}\left(\mathcal{C}(C,-), \mathrm{F}\right)$ définie par 
\begin{align*}
x\mapsto \Psi(x)_D \colon   \mathcal{C}(C,D) & \to \mathrm{F}(D) \\
 f & \mapsto \mathrm{F}(f)(x)~.
\end{align*}
On commence par vérifier que cette application est bien définie, c'est-à-dire que $\Psi(x)$ est bien une transformation naturelle : pour toute flèche $g : D \rightarrow D'$, on a 
\[
\mathrm{F}(g)\circ \Psi(x)_D = \Psi(x)_{D'}\circ g_*~. 
\]
Appliqué à toute flèche $f$ de $\mathcal{C}(C, D)$, on obtient bien 
\[
\left(\mathrm{F}(g)\circ \Psi(x)_D\right)(f)= 
\mathrm{F}(g) \circ \mathrm{F}(f)(x)=
\mathrm{F}(g\circ f)(x)= \Psi(x)_{D'}(g \circ f)=
\left( \Psi(x)_{D'}\circ g_*\right)(f)~.
\]
On vérifie ensuite que la composée $\Psi\circ \Phi$ est égale à l'identité de 
$\mathrm{Nat}\left(\mathcal{C}(C,-), \mathrm{F}\right)$ : 
pour toute transformation naturelle $\varphi \colon \mathcal{C}(C,-) \Rightarrow \mathrm{F}$ pour toute flèche $f \colon C \to D$ de $\mathcal{C}$, on a bien 
\[
\big((\Psi\circ \Phi)(\varphi)\big)(f)=\Psi(\varphi_C(\mathrm{id}_C))(f)=\mathrm{F}(f)(\varphi_C(\mathrm{id}_C))
=\big(\varphi_D\circ f_*\big)(\mathrm{id}_C)=\varphi_D(f)~. 
\]
Dans l'autre sens, on vérifie que la composée $\Phi \circ \Psi$ est égale à l'identité de l'ensemble
$\mathrm{F}(C)$ : pour tout élément $x$ de $\mathrm{F}(C)$, on a bien 
\[(\Phi \circ \Psi)(x)=\Psi(x)_C(\mathrm{id}_C)=\mathrm{F}(\mathrm{id}_C)(x)
=\mathrm{id}_{\mathrm{F}(C)}(x)=x~. \]
On laisse à la lectrice ou au lecteur le soin de vérifier la naturalité en l'objet  $C$ de $\mathcal{C}$ et en le foncteur $\mathrm{F}$, chose qui est automatique avec les définitions.
% @Oumaïma : à faire 
\end{proof}

\begin{coro} [Plongement de Yoneda]
Soit $\mathcal{C}$ une catégorie localement petite. Le foncteur $\mathrm{Y} \colon \mathcal{C}^{\mathrm{op}} \rightarrow \mathrm{Fon}(\mathcal{C},\mathcal{Ens})$, appelé \emph{plongement de Yoneda}, qui envoie tout objet $C$ de $\mathcal{C}^{\mathrm{op}}$ sur son foncteur de représentation $\mathcal{C}(C, -) : \mathcal{C} \rightarrow \mathcal{Ens}$, est plein, fidèle et injectif au niveau des objets. 

De plus, un morphisme $f : C \rightarrow D$ de $\mathcal{C}$ est un isomorphisme si et seulement si la transformation naturelle induite par $\mathrm{Y}(f^{\mathrm{op}}) : \mathcal{C}(D, -) \Rightarrow \mathcal{C}(C, -)$ est un isomorphisme.
\end{coro}

\begin{proof}
Si $C$ et $D$ sont deux objets de $\mathcal{C}$, le lemme de Yoneda établit une bijection naturelle entre $\text{Nat}(\mathcal{C}(C, -), \mathcal{C}(D, -))$ et $\mathcal{C}(D,C)$, ce qui signifie que  le foncteur de Yoneda est plein et fidèle. 
Si deux foncteurs $\mathcal{C}(C, -)$ et  $\mathcal{C}(D, -)$ sont égaux, cela implique en particulier que 
$\mathcal{C}(C, C)=\mathcal{C}(D, C)$ et en appliquant l'application <<source>>, on obtient $C=D$, c'est-à-dire que 
le plongement de Yoneda $\mathrm{Y}$ est  injectif au niveau des objets. 

On sait qu'un foncteur préserve les isomorphismes (Exercice~\ref{exer:FoncIso}) et que les isomorphismes de la catégorie des foncteurs sont les isomorphismes naturels (Proposition~\ref{prop:IsoNat=IsoFonc}). 
 Réciproquement, si 
 $\mathrm{Y}(f^{\mathrm{op}}) : \mathcal{C}(D, -) \Rightarrow \mathcal{C}(C, -)$ est un isomorphisme de foncteurs d'inverse $\psi \colon \mathcal{C}(C, -) \Rightarrow \mathcal{C}(D, -)$, le lemme de Yoneda (Théorème~\ref{theo:LemmeYoneda}) garantit qu'il existe un (unique) morphisme $g \colon D \to C$ de $\mathcal{C}$ tel que $\psi=g^*$. Toujours en appliquant le lemme de Yoneda, on obtient que la composée 
 $\psi \circ \mathrm{Y}(f^{\mathrm{op}}) : \mathcal{C}(D, -) \Rightarrow \mathcal{C}(D, -)$
 de transformations naturelles, qui est égale à l'identité du foncteur $\mathcal{C}(D, -)$, est égale au tirage en arrière  $\left(\mathrm{id}_D\right)^*$ par l'identité de $D$. Par unicité, on en conclut que $f \circ g = \mathrm{id}_D$. On procède de la même manière pour montrer 
 que $f \circ g = \mathrm{id}$, ce qui conclut la démonstration. 
 \end{proof}

Dit autrement, toute catégorie localement petite $\mathcal{C}^{\mathrm{op}}$ est isomorphe à la sous-catégorie des foncteurs de représentations de $\mathrm{Fon}(\mathcal{C},\mathcal{Ens})$.

\begin{rema}   
Le lemme de Yoneda est surement le résultat le plus puissant de la théorie des catégories; il est très fréquement utilisé pour démontrer des résultats plus avancés dans ce domaine. Il est a été nommé ainsi après que Saunders MacLane, père de la théorie des catégories, ait rencontré le jeune étudiant Nobuo Yoneda, découvreur de ce résultat, à la ... gare du Nord à Paris. :) 
\end{rema}