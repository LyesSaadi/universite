\subsection{Définitions}

\begin{defi}[Transformation naturelle]
    Soient $\mathrm{F}, \mathrm{G} : \mathcal{C} \to \mathcal{D}$ deux foncteurs de la catégorie $\mathcal{C}$ à la catégorie $\mathcal{D}$. Une \emph{transformation naturelle} $\varphi$ de $\mathrm{F}$ à $\mathrm{G}$ est la donnée d'une famille  
    $\varphi_A : \mathrm{F}(A) \to \mathrm{G}(A)$ de flèches de $\mathcal{D}$, une pour chaque objet $A$ de $\mathcal{C}$, telle que pour tout morphisme $f : A \to B$ dans $\mathcal{C}$, le diagramme suivant commute :
    \[
        \begin{tikzcd}
            \mathrm{F}(A) \arrow[r, "\mathrm{F}(f)"] \arrow[d, "\varphi_A"'] & \mathrm{F}(B)~\, \arrow[d, "\varphi_B"] \\
            \mathrm{G}(A) \arrow[r, "\mathrm{G}(f)"'] & \mathrm{G}(B)~. 
        \end{tikzcd}
    \]

La notion de transformation naturelle permet de comparer les foncteurs : il s'agit en fait d'une notion de <<morphisme>> entre foncteurs. C'est pourquoi on utilise la notation $\varphi : \mathrm{F} \Rightarrow  \mathrm{G}$ pour indiquer que $\varphi$ est une transformation
    naturelle de $\mathrm{F}$ vers $\mathrm{G}$.
                        
\end{defi}

\begin{exem}
    Soit \(\alpha : R \to S\) un morphisme dans la catégorie $\mathcal{CRing}$ des anneaux commutatifs unitaires  (donc en particulier \(\alpha(1_R) = 1_S\)). Pour tout entier positif \(n\), nous obtenons un homomorphisme de groupes induit \(\varphi_\alpha : \text{GL}_n(R) \to \text{GL}_n(S)\) donné par \(\varphi_\alpha([a_{ij}]) \coloneqq [\alpha(a_{ij})]\) (c'est-à-dire, appliquer \(\alpha\) à chaque entrée de la matrice). Ici, \(\text{GL}_n(R)\) désigne le groupe de des matrices inversibles \(n \times n\) sur \(R\), qui est le même que le groupe de toutes les matrices \(n \times n\) sur \(R\) ayant un déterminant unité dans \(R\).

    Fixons un entier positif \(n\). Définissons un foncteur \(\mathrm{F} : \text{$\mathcal{CRing}$} \to \mathcal{Grp}\) par \(\mathrm{F}(R) \coloneqq \text{GL}_n(R)\) et \(\mathrm{F}(\alpha) = \varphi_\alpha\). Définissons également un foncteur \(\mathrm{G} : \text{$\mathcal{CRing}$} \to \mathcal{Grp}\) par \(\mathrm{G}(R) \coloneqq R^\times\) et \(\mathrm{G}(\alpha) \coloneqq \alpha|_{R^\times}\), où \(R^\times\) désigne le groupe des unités de \(R\) pour la multiplication. Pour un objet \(R\) de $\mathcal{CRing}$, définissons \(\eta_R = \det_R : \text{GL}_n(R) \to R^\times\) comme la fonction déterminant.
    \[
    \begin{tikzcd}
        R \arrow[r, "\alpha"]  & S \\
        \text{GL}_n(R) \arrow[r, "\varphi_\alpha"] \arrow[d, "\det_R "'] & \text{GL}_n(S) \arrow[d, "\det_S"] \\
        R^\times \arrow[r, "\alpha |_{R^\times}"] & S^\times
    \end{tikzcd}
    \]
    
    
    Pour tout \(A = [a_{ij}] \in \text{GL}_n(R)\), en utilisant le fait que \(\alpha\) est un morphisme d'anneaux, on a
    \begin{align*}
        \alpha|_{R^\times} {\det}_R(A) = \alpha \left( \sum_{\sigma \in \mathbb{S}_n} \text{sgn}(\sigma) \prod_{i=1}^n a_{i\sigma(i)} \right) 
= \sum_{\sigma \in \mathbb{S}_n} \text{sgn}(\sigma) \prod_{i=1}^n \alpha(a_{i\sigma(i)}) 
= {\det}_S \varphi_\alpha(A)~,
    \end{align*}
    où \(\mathbb{S}_n\) désigne le groupe symétrique sur \(\{1, \ldots, n\}\) et \(\text{sgn}(\sigma)\) est la signature de la permutation $\sigma$. Par conséquent, \(\alpha|_{R^\times} \det_R = \det_S \varphi_\alpha\) et le diagramme ci-dessus est commutatif.
\end{exem}

\begin{exem}
    Soit $\eta : \mathrm{Id}_\mathcal{Ens} \Rightarrow \mathrm{P}$ la transformation naturelle de l'identité vers le foncteur des parties d'un ensemble  dont les composants $\eta_A : A \rightarrow \mathrm{P}(A)$ sont les applications  qui envoient $a \in A$ sur le singleton $\{a\} \in \mathrm{P}(A)$. 

\end{exem}

%\begin{exem}[Transformation naturelle dans la vie]
%    Soit $\mathcal{C}$ la  catégorie définie par les données suivantes. 
%    \begin{itemize}[label=\textbullet]
%        \item \textsc{sur les objets :} les gens avec dollars, 
%        
%        \item \textsc{sur les morphismes :} les transactions.
%    \end{itemize}
%
%    Soit $\mathcal{D}$  la  catégorie définie par les données suivantes. 
%    \begin{itemize}[label=\textbullet]
%        \item \textsc{sur les objets :} les gens avec euros, 
%        
%        \item \textsc{sur les morphismes :} les transactions.
%    \end{itemize}
%
%    Alors on a une Transformation naturelle est \textbf{Changement de devise}
%\end{exem}

\begin{defprop}[Catégorie de foncteurs]
    Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. La \emph{catégorie des foncteurs} de $\mathcal{C}$ vers $\mathcal{D}$, notée $[\mathcal{C}, \mathcal{D}]$,  est formée des données suivantes : 
    \begin{itemize}[label=\textbullet]
        \item les objects sont les foncteurs $\mathrm{F} : \mathcal{C} \to \mathcal{D}$~, 
        \item les flèches de $\mathrm{F} : \mathcal{C} \to \mathcal{D}$ vers $\mathrm{G} : \mathcal{C} \to \mathcal{D}$  sont les transformations naturelles $\varphi  : \mathrm{F} \Rightarrow \mathrm{G}$~, 
        \item la composition de deux transformations naturelles $\varphi: \mathrm{F} \Rightarrow \mathrm{G}$ et $\psi: 
        \mathrm{G} \Rightarrow \mathrm{H}$ est la composition naturelle $\psi\circ \varphi : \mathrm{F} \Rightarrow \mathrm{H}$ définie composante par composante par :             $(\psi\circ \varphi)_A \coloneqq \psi_{A} \circ \varphi_{A}$~,
	        \item l'identité $\iota_\mathrm{F} : \mathrm{F} \Rightarrow \mathrm{F}$ d'un foncteur $\mathrm{F} : \mathcal{C} \to \mathcal{D}$ est définie composante par composante par : $\left(\iota_\mathrm{F}\right)_A\coloneqq \mathrm{id}_{\mathrm{F}(A)}$~.
    \end{itemize}
\end{defprop}

\begin{proo}
On commence par vérifier que la composée de deux transformations naturelles est bien une transformation naturelle :     \[
        \begin{tikzcd}
            \mathrm{F}(A) \arrow[r, "\mathrm{F}(f)"] \arrow[d, "\varphi_A"'] & \mathrm{F}(B) \arrow[d, "\varphi_B"] \\
            \mathrm{G}(A) \arrow[r, "\mathrm{G}(f)"'] \arrow[d, "\psi_A"'] & \arrow[d, "\psi_B"]\mathrm{G}(B) \\
            \mathrm{H}(A) \arrow[r, "\mathrm{H}(f)"'] & \mathrm{H}(B)
        \end{tikzcd}
    \]
les deux carrés intérieurs commutent donc le rectangle extérieur commute. On voit immédiatement que les identités sont des transformations naturelles. 
L'associativité et l'unitarité de la catégorie $\mathcal{D}$ assurent l'associativité et l'unitarité de la composition des transformations naturelles. 
\end{proo}

\begin{exem}[Espaces vectoriels de dimension finie et matrices]\label{ex:EvMat}
On commence par considérer le foncteur $\mathrm{F}$ de la catégorie $\mathcal{Vect}_{\text{dim finie, base ordonnée}}$ vers la catégorie  $\mathcal{Mat}$ défini de la manière suivante. 
    
    \begin{itemize}[label=\textbullet]
        \item \textsc{sur les objets :} Pour un espace vectoriel \(V\) de dimension finie avec une base ordonnée fixée, on pose $\mathrm{F}(V)\coloneqq \dim(V)$.
        
        \item \textsc{sur les morphismes :} Pour une application linéaire \(f : V \to W\), \(\mathrm{F}(f)\) est la matrice associée à \(f\) dans les bases ordonnées de \(V\) et \(W\).
    \end{itemize}
    \[
        \begin{tikzcd}
                \mathcal{Vect}_{\text{dim finie, base ordonnée}}\arrow[r, "\mathrm{F}"] & \mathcal{Mat}
        \end{tikzcd}
    \]
    \\ 
    \[
        \begin{tikzcd}
            ( {V}, \{v_1, \ldots, v_n\} ) \arrow[r, "\mathrm{F}"] & n = \dim V \\
            ( {W}, \{w_1, \ldots, w_m\} ) \arrow[u, "f"] \arrow[r, "\mathrm{F}"'] & m = \dim W \arrow[u, "\mathrm{F}(f)"']~.
        \end{tikzcd}
    \]

    
On considère maintenant le foncteur $\mathrm{G}$ de la catégorie  $\mathcal{Mat}$ vers 
la catégorie $\mathcal{Vect}_{\text{dim finie, base ordonnée}}$ défini de la manière suivante. 

    \begin{itemize}[label=\textbullet]
        \item \textsc{sur les objets :} \`A tout entier $n$, le foncteur $\mathrm{G}$ associe l'espace vectoriel 
        $\mathbb{R}^n$ muni de sa base canonique, soit 
       $\mathrm{G}(n)\coloneqq  ( \mathbb{R}^n, \{e_1, \ldots, e_n\} )$.
        
        \item \textsc{sur les morphismes :} \`A toute matrice \(A \in \mathrm{Mat}_{n,m}(\mathbb{R})\), le foncteur \(\mathrm{G}\) associe  l'application linéaire $\mathrm{G}(A) \colon X \mapsto AX$ définie par la multiplication à gauche par \(A\).
    \end{itemize}
    \[
        \begin{tikzcd}
                \mathcal{Mat}\arrow[r, "\mathrm{G}"] & \mathcal{Vect}_{\text{dim finie, base ordonnée}}
        \end{tikzcd}
    \]
    \\ 
    \[
        \begin{tikzcd}
            n \arrow[r, "\mathrm{G}"] & ( \mathbb{R}^n, \{e_1, \ldots, e_n\} ) \\
            m \arrow[u, "A"] \arrow[r, "\mathrm{G}"'] & ( \mathbb{R}^m, \{e_1, \ldots, e_m\} ) \arrow[u, "\mathrm{G}(A)"']~.
        \end{tikzcd}
    \]
On remarque que $\mathrm{F} \circ \mathrm{G} = \mathrm{Id}_\mathcal{Mat}$ mais que 
$\mathrm{G} \circ \mathrm{F} \ne \mathrm{Id}_{\mathcal{Vect}_{\text{dim finie, base ordonnée}}}$, 
en effet le foncteur $\mathrm{G} \circ \mathrm{F}$ envoie tout espace vectoriel de dimension $n$ sur 
$\mathbb{R}^n$. Donc  les foncteurs $F$ et $G$ ne sont pas des foncteurs inverses l'un de l'autre. 

On remarque néanmoins que pour tout espace vectoriel $V$ de dimension finie muni d'une base ordonnée, l'espace 
vectoriel $\mathrm{G} \circ \mathrm{F}(V)$ est canoniquement isomorphe à $V$. On a donc envie de donner du sens à la notation suivante :
\[\mathrm{G} \circ \mathrm{F} \cong \mathrm{Id}_{\mathcal{Vect}_{\text{dim finie, base ordonnée}}}~.\]
Pour cela, on considère la transformation naturelle 
$\eta \colon \mathrm{G} \circ \mathrm{F} \Rightarrow \mathrm{Id}_{\mathcal{Vect}_{\text{dim finie, base ordonnée}}}$ définie 
pour tout espace vectoriel 
$({V}, \{v_1, \ldots, v_n\})$ muni d'un base ordonnée
par l'application linéaire (isomorphisme) $\eta_V \colon \mathbb{R}^{n} \to V$ qui envoie la base canonique sur 
$\{v_1, \ldots, v_n\}$~. 

    \[
    \begin{tikzcd}[column sep=huge]
        \textbf{${\mathcal{Vect}_{\mathrm{dim\, finie,\, base\, ordonnee}}}$}
          \arrow[bend left=50]{r}[name=U,label=above:$\scriptstyle \mathrm{G} \circ \mathrm{F}$]{}
          \arrow[bend right=50]{r}[name=D,label=below:$\scriptstyle\iota_{\mathcal{Vect}_{\mathrm{dim\, finie,\, base\, ordonnee}}} $]{} &
        \textbf{${\mathcal{Vect}_{\mathrm{dim\, finie,\, base\, ordonnee}}}$}
          \arrow[shorten <=10pt,shorten >=10pt,Rightarrow,to path={(U) -- node[label=right:$\eta$] {} (D)}]{}
    \end{tikzcd}
    \]
\end{exem}

\begin{defi}[Isomorphisme naturel]
Soient $\mathrm{F}, \mathrm{G} : \mathcal{C} \to \mathcal{D}$ deux foncteurs. 
    Une transformation naturelle $\varphi : \mathrm{F} \Rightarrow \mathrm{G}$ est un \emph{isomorphisme naturel} si chaque composante $\varphi_A : \mathrm{F}(A) \xrightarrow{\cong} \mathrm{G}(A)$ est un isomorphisme de $\mathcal{D}$, pour tout objet $A$ de $\mathcal{C}$.
\end{defi}

\begin{exem}
    Soit \(G\) un groupe. Le <<groupe opposé>> de \(G\) est le groupe \(G^\text{op}\) ayant le même ensemble sous-jacent que \(G\), mais avec l'opération \(a \cdot b = ba\), pour \(a, b \in G\), où le produit à droite est celui de \(G\). Soit \(\mathrm{F} : \mathcal{Grp} \rightarrow \mathcal{Grp}\) le foncteur donné par \(\mathrm{F}(G) \coloneqq G^\text{op}\), \(\mathrm{F}(\phi) = \phi\). Pour un groupe \(G\), définissons \(\eta_G : G \rightarrow G^\text{op}\) par \(\eta_G(a) \coloneqq a^{-1}\). Alors, \(\eta\) est un isomorphisme naturel du foncteur identité sur \(\mathcal{Grp}\) au foncteur \(\mathcal{F}\).
\end{exem}

\begin{prop}\label{prop:IsoNat=IsoFonc}
Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories et soient 
$\mathrm{F}, \mathrm{G} : \mathcal{C} \to \mathcal{D}$ deux foncteurs. 
Une transformation naturelle $\varphi : \mathrm{F} \Rightarrow \mathrm{G}$ est un isomorphisme naturel si et seulement si c'est un isomorphisme dans la catégorie $[\mathcal{C}, \mathcal{D}]$ des foncteurs de 
$\mathcal{C}$ vers $\mathcal{D}$.
\end{prop}


\begin{proof}\leavevmode
\begin{description}
\item[$(\Rightarrow)$\ ]
 Soit $\varphi : \mathrm{F} \Rightarrow \mathrm{G}$ un isomorphisme naturel. 
Alors, pour tout objet $C$ de $\mathcal{C}$, l'isomorphisme $\varphi_C$ admet un isomorphisme réciproque : 
\[\varphi_C \circ {\varphi_C}^{-1} = \mathrm{id}_{\mathrm{G}(C)} \qquad \text{et} \qquad 
{\varphi_C}^{-1} \circ \varphi_C = \mathrm{id}_{\mathrm{F}(C)}~.\]
On considère donc les composantes ${\varphi_C}^{-1} \colon \mathrm{G}(C) \to \mathrm{F}(C)$, donc il est facile de vérifier qu'elles forment une transformation naturelle $\varphi^{-1} \colon \mathrm{G} \Rightarrow \mathrm{F}$. On prétend qu'il s'agit d'une transformation naturelle inverse de $\varphi$ car
\[\left(\varphi \circ \varphi^{-1}\right)_C = \mathrm{id}_{\mathrm{G}(C)} \qquad \text{et} \qquad 
\left(\varphi^{-1} \circ \varphi\right)_C = \mathrm{id}_{\mathrm{F}(C)}~.  \]
%    Donc:
%    \begin{center}
%        $\varphi \circ \varphi^{-1} =  \iota_G$
%    \end{center}
%    \begin{center}
%        $\varphi^{-1} \circ \varphi =  \iota_F$
%    \end{center}
 Ce qui montre que $\varphi$ est un isomorphisme dans la catégorie $[\mathcal{C}, \mathcal{D}]$.
    
\item[$(\Leftarrow)$\ ]    
     Soit $\varphi : \mathrm{F} \Rightarrow \mathrm{G}$  un isomorphisme dans la catégorie $[\mathcal{C}, \mathcal{D}]$. Il existe donc un isomorphisme $\varphi^{-1} : \mathrm{G} \Rightarrow \mathrm{F}$ dans la catégorie  $[\mathcal{C}, \mathcal{D}]$ réciproque de $\varphi$ : 
\[\varphi \circ \varphi^{-1} =  \iota_G\qquad \text{et} \qquad  \varphi^{-1} \circ \varphi =  \iota_F~.\]
Ceci signifie que, pour tout objet 
$C$ de $\mathcal{C}$, on a :
\[\left(\varphi \circ \varphi^{-1}\right)_C = \varphi_C \circ {\varphi_C}^{-1} = \mathrm{id}_{\mathrm{F}(C)}
\qquad 
\text{et}
\qquad 
\left(\varphi^{-1} \circ \varphi\right)_C = {\varphi_C}^{-1} \circ \varphi_C = \mathrm{id}_{\mathrm{G}(C)}~.\]
Ceci montre que $\varphi$ est un isomorphisme naturel. 
\end{description}     
\end{proof}

\subsection{\'Equivalence de catégories}

\subsubsection{Définition}

\begin{defi}[\'Equivalence de catégories]
    Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. Une \emph{équivalence de catégories} entre $\mathcal{C}$ et $\mathcal{D}$ est la donnée de deux foncteurs
    \[
        \mathrm{F} : \mathcal{C} \to \mathcal{D} \quad \text{et} \quad \mathrm{G} : \mathcal{D} \to \mathcal{C}
    \]
et de deux isomorphismes naturels
    \[
        \mathrm{F} \circ \mathrm{G} \cong \mathrm{Id}_{\mathcal{D}} \quad \text{et} \quad \mathrm{G} \circ \mathrm{F} \cong \mathrm{Id}_{\mathcal{C}}~. 
    \]
\end{defi}

\begin{exem}
 Dans l'exemple~\ref{ex:EvMat}, la paire de foncteur 
$\mathrm{F} \colon \mathcal{Vect}_{\text{dim finie, base ordonnée}}\to  \mathcal{Mat}$ et 
$\mathrm{G} \colon \mathcal{Mat} \to \mathcal{Vect}_{\text{dim finie, base ordonnée}}$ et 
 forment une équivalence de catégories. 
\end{exem}

\begin{prop}
La relation <<être équivalente>> est une relation d'équivalence entre catégories. 
\end{prop}

\begin{proof} \leavevmode
\begin{description}
\item[{\sc Réflexivité}]
Soit $\mathcal{C}$ une catégorie. Considérons le foncteur identité $\mathrm{Id}_{\mathcal{C}} : \mathcal{C} \to \mathcal{C}$ qui envoie chaque objet et chaque morphisme sur lui-même. Il est clair que $\mathrm{Id}_{\mathcal{C}}$ est un isomorphisme, car son inverse est lui-même. 

\item[{\sc Symétrie}]
Soit une catégorie $\mathcal{C}$ équivalente à une autre catégorie $\mathcal{D}$ par un foncteur $\mathrm{F} : \mathcal{C} \to \mathcal{D}$ et un foncteur $\mathrm{G} : \mathcal{D} \to \mathcal{C}$ et deux isomorphismes naturels de  $\mathrm{G} \circ \mathrm{F}$ vers l'identité de $\mathcal{C}$ et de $\mathrm{F} \circ \mathrm{G}$ vers l'identité  de $\mathcal{D}$. En inversant, $\mathcal{C}$ et $\mathcal{D}$ puis $\mathrm{F}$ et $\mathrm{G}$, on obtient que la catégorie $\mathcal{D}$ est équivalente à la catégorie $\mathcal{C}$. 

\item[{\sc Transitivité}]
Soit une catégorie $\mathcal{C}$ équivalente à une catégorie $\mathcal{D}$, qui elle-même est équivalente à la catégorie $\mathcal{E}$. Ces deux équivalences sont réalisées par des foncteurs :
    \begin{eqnarray*}
&&        \mathrm{F} : \mathcal{C} \to \mathcal{D} \qquad \text{et}    \qquad   \mathrm{K} : \mathcal{D} \to \mathcal{E}\\
 &&       \mathrm{G} : \mathcal{D} \to \mathcal{C} \qquad\qquad \quad   \mathrm{H} : \mathcal{E} \to \mathcal{D}
    \end{eqnarray*}    
tels que 
    \begin{eqnarray*}
   &&    \mathrm{F} \circ \mathrm{G} \cong \mathrm{Id}_{\mathcal{D}} \qquad \text{et}    \qquad \mathrm{K} \circ \mathrm{H} \cong \mathrm{Id}_{\mathcal{E}} \\
&&        \mathrm{G} \circ \mathrm{F} \cong \mathrm{Id}_{\mathcal{C}}  \qquad \quad    \qquad  \mathrm{H} \circ \mathrm{K} \cong \mathrm{Id}_{\mathcal{D}}~.
    \end{eqnarray*}
On prétend alors que les deux composées de foncteurs $\mathrm{K}\circ \mathrm{F}$
 et $\mathrm{G}\circ \mathrm{H}$ sont des équivalences de catégories. En effet, on a bien 
\[
     (\mathrm{K} \circ  \mathrm{F}) \circ (\mathrm{G} \circ \mathrm{H})=
     \mathrm{K} \circ  (\mathrm{F} \circ \mathrm{G}) \circ \mathrm{H} \cong 
     \mathrm{K} \circ  \mathrm{H}\cong \mathrm{Id}_{\mathcal{E}}
\]
et 
\[
     (\mathrm{G} \circ  \mathrm{H}) \circ (\mathrm{K} \circ \mathrm{F})=
     \mathrm{G} \circ  (\mathrm{H} \circ \mathrm{K}) \circ \mathrm{F} \cong 
     \mathrm{G} \circ  \mathrm{F}\cong \mathrm{Id}_{\mathcal{C}}~. 
\]
\end{description}
Par conséquent, la relation <<être équivalente>> est une relation d'équivalence entre catégories.
\end{proof}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "Categories"
%%% End:
