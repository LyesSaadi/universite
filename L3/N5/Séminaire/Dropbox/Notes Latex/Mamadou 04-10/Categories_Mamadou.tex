\documentclass[twoside, 11pt]{amsart}
\usepackage{tikz}
\usetikzlibrary{positioning, arrows.meta}
\include{package}
\usepackage{amsthm}



\begin{document}
\section{Produit dans une catégorie}

\subsection{Exemples de produits}  
 
Le concept de \emph{"produit"} est une notion fondamentale que nous rencontrons fréquemment dans différentes strutures que nous connaisons, qu'elles relèvent de l'algèbre ou de l'informatique:

\begin{itemize}
  \item Dans la structure des ensembles, nous avons le \textbf{produit cartésien} 
  \item Dans la structure des corps, le produit de deux corps $K$ et $L$, noté $\mathbf{K\times L}$, peut être défini comme un corps où les opérations de base (addition et multiplication) sont définies composante par composante. Peuvent être définies de la même façon le produit de groupes, d'anneaux, d'espaces vectoriels.
  \item En OCaml, qui est un langage de programmation fonctionnelle, le produit $\mathbf{(int*string)}$ définit un tuple (regroupement ordonné de valeurs de types différents) composé d'un entier de type $'int'$ et d'une chaine de caractère $'string'$
  \item Le produit cartésien de deux graphes $G=(S, A)$ et $G'=(S', A')$ correspond à un graphe $\mathbf{H =(S_{H}, A_{H})}$ tel que $S_{H} = \{{(s, t)\mid s\in S, t\in S'}\}$ et pour tout $e = ((s, t), (s', t'))\in S_{H}\times S_{H}$ on a $ e\in A_{H} \Leftrightarrow (s,t)\in A \vee (s', t')\in A'$
\end{itemize}


Au niveau de ces \emph{"produits"}, on retrouve souvent cette idée de \textbf{"combiner des objets"} de façon struturée, générant ainsi des entités ayant leurs propres propriétés.
Il serait dans sens intéressant de voir s'il est possible d'appliquer cette notion de produit à une échelle plus large.

\subsection{Exemple de départ : produit cartésien dans les ensembles}

Soient $A$ et $B$ deux ensembles. Le produit cartésien de $A$ et $B$,  noté \textbf{$A{\times}B$}, est défini par l'ensemble des couples ordonnés $(a, b)$ tels que $a$ appartient à $A$ et $b$ appartient à $B$. Formellement:
$$ A\times B  = \{(a,b) \mid a \in A , b \in B  \} $$

Sont définies par la même occasion les applications :
\[ P_{A} : A\times B \to A, \quad P_{A}(a, b) = a, \]
\[ P_{B} : A\times B \to B, \quad P_{B}(a, b) = b. \]

Analysons cette définition du produit cartésien et partons de l'idée selon laquelle :
$$ \forall a \in A, \quad \forall b \in B, \quad \exists! (a, b) \in A\times B $$ 

En termes catégoriques, cela donne : \[\forall x : \{*\} \to A, \forall y: \{*\} \to B, \exists! z: \{*\} \to A\times B\] tel que
$$ P_{A} \circ z = x \quad \text{et} \quad P_{B} \circ z = y $$ 

Le shéma suivant résume la caractérisation donnée ci-dessus:

\begin{center}
  \begin{tikzpicture}

    % Nodes
   \node (P) at (0, 0) {$A\times B$};
   \node (Singleton) at (0, 1.5) {$\{*\}$};
   \node (A) at (-1.5, -1.5) {$A$};
   \node (B) at (1.5, -1.5) {$B$};

   % Lines
  \draw[->, dashed, >=Stealth] (Singleton) -- node[right] {$\exists ! z$} (P);
  \draw[->, >=Stealth] (P) -- node[midway, left] {$P_{A}$} (A);
  \draw[->, >=Stealth] (P) -- node[midway, right] {$P_{B}$} (B);
  \draw[->, >=Stealth, bend left] (Singleton) to node[midway,right] {$y$} (B);
  \draw[->, >=Stealth, bend right] (Singleton) to node[midway, left] {$x$} (A);
  \end{tikzpicture}
\end{center}
 
Abstractisons à présent cette notion de produit dans un ensemble en l'élargissant dans la structure des catégories.


\subsection{Produit dans une catégorie}
  \begin{defi}

    (Produit dans une Catégorie):  Soient $\mathcal{C}$ une catégorie (arbitraire), $A, B \in \mathrm{Obj}(\mathcal{C})$.
    Un objet $P$  de $\mathcal{C}$ muni des morphismes $P_{A} : P \to A$ et $P_{B} : P \to B$ est un \emph{produit} de $A$ et $B$ dans $\mathcal{C}$ si:
    $\forall X \in \mathrm{Obj}(\mathcal{C}) $ , 
    $\forall\; X \xrightarrow{f} A $ ,
    $\forall\; X \xrightarrow{g} B $ ,
    $ \exists !\; X \xrightarrow{h} P$ tel que :
    $$P_{A} \circ h = f \quad \text{et} \quad P_{B} \circ h = g $$ .

    \begin{center}
      \begin{tikzpicture}
    
        % Nodes
       \node (P) at (0, 0) {$P$};
       \node (X) at (0, 1.5) {$X$};
       \node (A) at (-1.5, -1.5) {$A$};
       \node (B) at (1.5, -1.5) {$B$};
    
       % Lines
      \draw[->, dashed, >=Stealth] (X) -- node[right] {$\exists ! h$} (P);
      \draw[->, >=Stealth] (P) -- node[midway, left] {$P_{A}$} (A);
      \draw[->, >=Stealth] (P) -- node[midway, right] {$P_{B}$} (B);
      \draw[->, >=Stealth, bend left] (X) to node[midway,right] {$g$} (B);
      \draw[->, >=Stealth, bend right] (X) to node[midway, left] {$f$} (A);
      \end{tikzpicture}
    \end{center}

  \textbf{Notation :} Le produit $P$, s'il existe, est souvent noté $A\times B$ et on écrit $h = <f, g>$ .

  \end{defi}
  
\begin{rema}
   Les morphismes $P_{A}$ et $P_{B}$, appelés projections, sont en effet des \emph{données} et doivent être définies pour que la notion de produit puisse avoir un sens.  
\end{rema}

\begin{exem}(Exemple dans $\mathcal{Ens}$)
  Soit $A$ et $B$ deux objets de $\mathcal{Ens}$. Le produit cartésien $A{\times}B$ est un produit catégorique de $A$ et $B$.
  \begin{proof}
    \text{\\}
    \begin{itemize}[label=$\rightarrow$]
      \item Existence : Soit $X$ un ensemble. Si les fonctions $f: X\to A$ et $g: X\to B$ sont définies, alors la fonction $h: X\to A\times B$ tel que $\forall x\in X,\;h(x)=(f(x), g(x))$ est bien définie.(a)
      \item Unicité : Aussi conséquence directe de la définition du produit cartésien dans les ensembles. Supposons l'existence de $h, h':X\to A\times B$ vérifiant (a). Pour tout $x\in X$, $h(x) = (f(x), g(x)) = h'(x).$ 
    \end{itemize}
  \end{proof}
\end{exem}

\subsection{Propriétés sur le produit d'objets dans une catégorie}
\begin{prop}(Stablité à isomorphisme près)
  Les produits sont "stables par isomorphisme". En d'autres termes, un objet isomorphe à un produit donne un autre produit. 
\end{prop}

\begin{proof}
  Soit une catégorie $\mathcal{C}$ arbitraire, $A$ et $B$ $\in \mathrm{Obj}(\mathcal{C})$ et $P$ un objet de $\mathcal{C}$ muni des projections $P_{A}$ et $P_{B}$ produit de $A$ et $B$ défini dans $\mathcal{C}$. 
  Soit $Q \in \mathrm{Obj}(\mathcal{C})$ tel que $Q\cong P$ .
  Il existe alors des morphismes $\rho : P\to Q$ et $\rho^{-1} : Q\to P$ tels que $\rho^{-1} \circ \rho = Id_{P}$ et $\rho \circ \rho^{-1} = Id_{Q}$.
  \begin{enumerate}
    \item On a d'abord que $Q$ est muni de morphismes $P^\prime_{A}$ et $P^\prime_{B}$ définies par composition de morphismes:
    $$P^\prime_{A} = P_{A}\circ\rho^{-1}\text{ et }P^\prime_{B} = P_{B}\circ\rho^{-1}$$
    \text{\\}
    \text{\\}
      \begin{tikzpicture}
    
        % Nodes
       \node (P) at (0, 0) {$P$};
       \node (Q) at (2.5, 0) {$Q$};
       \node (A) at (-1.5, -1.5) {$A$};
       \node (B) at (1.5, -1.5) {$B$};
       \node (Q1) at (10, 0) {$Q$};
       \node (P1) at (8.5, -1.5) {$P$};
       \node (P2) at (11.5, -1.5) {$P$};
       \node (A1) at (8.5, -3) {$A$};
       \node (B1) at (11.5, -3) {$B$};
    
       % Lines
      \draw[->, >=Stealth, red] (P) -- node[above] {$\rho$} node[below] {\(\sim\)} (Q);
      \draw[->, >=Stealth] (P) -- node[midway, left] {$P_{A}$} (A);
      \draw[->, >=Stealth] (P) -- node[midway, right] {$P_{B}$} (B);
      \draw[->, >=Stealth, red] (Q1) -- node[midway, left] {$\rho^{-1}$} (P1);
      \draw[->, >=Stealth, red] (Q1) -- node[midway, right] {$\rho^{-1}$} (P2);
      \draw[->, >=Stealth] (P1) -- node[midway, left] {$P_{A}$} (A1);
      \draw[->, >=Stealth] (P2) -- node[midway, right] {$P_{B}$} (B1);
      \draw[double,<->] (4,-1.5) -- (6,-1.5);

      \end{tikzpicture}

  \item $P^\prime_{A}$ et $P^\prime_{B}$ définies, il reste à prouver que $Q$ vérifie les propriétés d'un produit. 
  Par définition du produit $P$, on sait que pour tout $X \in \mathrm{Obj(\mathcal{C})}$, il existe un unique morphisme
  $X\xrightarrow{k} P$ tel que $P_{A} \circ k = f \quad \text{et} \quad P_{B} \circ k = g $ ,  pour tout $f\in Hom_{\mathcal{C}}(X, A)$ et  $g\in Hom_{\mathcal{C}}(X, B)$.
  Les morphismes $k$ et $\rho$ étant composables, il existe un morphisme $X\xrightarrow{h} Q$ dans $Hom_{\mathcal{C}}$ tel que $h = \rho\circ k$. On a donc:
  $$ f = P_{A} \circ k  = P_{A} \circ (\rho^{-1}\circ h) = (P_{A} \circ \rho^{-1})\circ h =  P^\prime_{A}\circ h.$$
  Par symétrie, on trouve avec un raisonnement similaire $g =  P_{B} \circ (\rho^{-1}\circ h) =P^\prime_{B}\circ h$. 
  \item Quant à l'unicité de $h$, supposons qu'il existe un autre morphisme $X\xrightarrow{h'} Q$ qui vérifie $$f=P^\prime_{A}\circ h' \;et \; g = P^\prime_{B}\circ h'.$$ 
  On peut donc construire deux flèches $\rho^{-1}\circ h,\; \rho^{-1}\circ h':X\to P$. En les composant avec $P_{A}$ et $P_{B}$, on obtient :
  $$P_{A}\circ(\rho^{-1}\circ h) = P^\prime_{A}\circ h = P^\prime_{A}\circ h' = f $$ 
  $$P_{B}\circ(\rho^{-1}\circ h') = P^\prime_{B}\circ h' = P^\prime_{B}\circ h = g $$
  Par définition du produit $P$, la flèche $X\xrightarrow{k} P$ vérifiant $P_{A}\circ k= f\; et\; P_{B}\circ k = g$ est unique. On en conclut donc que 
  $$\rho^{-1}\circ h = \rho^{-1}\circ h' \implies h' = h. \qquad(\rho\;est\;un\;isomorphisme)$$
  Le diagramme ci-dessous commute.



  \end{enumerate}

  \begin{center}
    \begin{tikzpicture}
    
      % Nodes
     \node (Q) at (0, 0) {Q};
     \node (P) at (0, -2) {P};
     \node (X) at (0, 2) {X};
     \node (A) at (-2, -4) {A};
     \node (B) at (2, -4) {B};
    
  
     % Lines
    \draw[->, >=Stealth, red, bend right] (Q) to node[left] {$\rho^{—1}$} (P);
    \draw[->, >=Stealth, red, bend right] (P) to node[right] {$\rho$} (Q);
    \draw[->, >=Stealth] (P) -- node[midway, right] {$P_{B}$} (B);
    \draw[->, >=Stealth] (P) -- node[midway, right] {$P_{A}$} (A);
    \draw[->, >=Stealth, dashed, orange] (X) -- node[midway, right] {$h = \rho\circ k$} (Q);
    \draw[->, >=Stealth, dashed, out=00, in = 40] (X) to node[midway, right] {$k$} (P);
    \draw[->, >=Stealth, bend right, orange] (X) to node[midway, left] {$f$} (A);
    \draw[->, >=Stealth, bend left, orange] (X) to node[midway, right] {$g$} (B);
  
    \end{tikzpicture}
    
  \end{center}

\end{proof}



\end{document}
