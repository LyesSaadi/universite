\select@language {french}
\contentsline {section}{\tocsection {}{}{Introduction : l'alg\IeC {\`e}bre moderne}}{1}{section*.2}
\contentsline {paragraph}{\tocparagraph {}{}{\bf L'Alg\IeC {\`e}bre ou l'analyse des \IeC {\'e}quations}}{1}{section*.3}
\contentsline {paragraph}{\tocparagraph {}{}{\bf L'alg\IeC {\`e}bre ou l'axiomatisation des structures alg\IeC {\'e}briques}}{2}{section*.4}
\contentsline {paragraph}{\tocparagraph {}{}{\bf Int\IeC {\'e}r\IeC {\^e}ts de l'axiomatisation }}{3}{section*.5}
\contentsline {paragraph}{\tocparagraph {}{}{\bf L'Alg\IeC {\`e}bre moderne}}{4}{section*.6}
\contentsline {paragraph}{\tocparagraph {}{}{\bf Bourbaki}}{4}{section*.7}
\contentsline {section}{\tocsection {}{1}{Cat\'egories}}{4}{section.1}
\contentsline {subsection}{\tocsubsection {}{1.1}{D\'efinitions et exemples}}{5}{subsection.1.1}
\contentsline {subsubsection}{\tocsubsubsection {}{1.1.1}{D\'efinition}}{5}{subsubsection.1.1.1}
\contentsline {subsubsection}{\tocsubsubsection {}{1.1.2}{Cat\IeC {\'e}gories <<concr\IeC {\`e}tes>>}}{5}{subsubsection.1.1.2}
\contentsline {subsubsection}{\tocsubsubsection {}{1.1.3}{Cat\IeC {\'e}gories <<non-concr\IeC {\`e}tes>>}}{6}{subsubsection.1.1.3}
\contentsline {subsubsection}{\tocsubsubsection {}{1.1.4}{Contre-exemple}}{7}{subsubsection.1.1.4}
\contentsline {subsubsection}{\tocsubsubsection {}{1.1.5}{Cat\IeC {\'e}gories petites et localement petites}}{7}{subsubsection.1.1.5}
\contentsline {subsection}{\tocsubsection {}{1.2}{Constructions de cat\IeC {\'e}gories}}{8}{subsection.1.2}
\contentsline {subsubsection}{\tocsubsubsection {}{1.2.1}{Cat\IeC {\'e}gories associ\IeC {\'e}es \IeC {\`a} des ensembles partiellement ordonn\IeC {\'e}s}}{8}{subsubsection.1.2.1}
\contentsline {subsubsection}{\tocsubsubsection {}{1.2.2}{Cat\IeC {\'e}gorie associ\IeC {\'e}e \IeC {\`a} un graphe}}{9}{subsubsection.1.2.2}
\contentsline {subsubsection}{\tocsubsubsection {}{1.2.3}{Union disjointe de cat\IeC {\'e}gories}}{10}{subsubsection.1.2.3}
\contentsline {subsubsection}{\tocsubsubsection {}{1.2.4}{Joint de cat\IeC {\'e}gories}}{11}{subsubsection.1.2.4}
\contentsline {subsubsection}{\tocsubsubsection {}{1.2.5}{Produit de cat\IeC {\'e}gories}}{13}{subsubsection.1.2.5}
\contentsline {subsubsection}{\tocsubsubsection {}{1.2.6}{Cat\IeC {\'e}gorie oppos\IeC {\'e}e}}{13}{subsubsection.1.2.6}
\contentsline {subsubsection}{\tocsubsubsection {}{1.2.7}{Sous-cat\IeC {\'e}gories}}{14}{subsubsection.1.2.7}
\contentsline {subsubsection}{\tocsubsubsection {}{1.2.8}{Intersection de sous-cat\IeC {\'e}gories}}{14}{subsubsection.1.2.8}
\contentsline {subsection}{\tocsubsection {}{1.3}{Les cat\'egories en informatique}}{15}{subsection.1.3}
\contentsline {subsection}{\tocsubsection {}{1.4}{Propri\IeC {\'e}t\IeC {\'e}s des morphismes}}{16}{subsection.1.4}
\contentsline {subsubsection}{\tocsubsubsection {}{1.4.1}{Isomorphismes}}{16}{subsubsection.1.4.1}
\contentsline {subsubsection}{\tocsubsubsection {}{1.4.2}{Monomorphismes et applications injectives}}{16}{subsubsection.1.4.2}
\contentsline {subsubsection}{\tocsubsubsection {}{1.4.3}{\'Epimorphismes et applications surjectives}}{18}{subsubsection.1.4.3}
\contentsline {subsubsection}{\tocsubsubsection {}{1.4.4}{Relation monomorphismes-\IeC {\'e}pimorphismes-isomorphismes}}{20}{subsubsection.1.4.4}
\contentsline {subsubsection}{\tocsubsubsection {}{1.4.5}{Caract\IeC {\'e}risation des monomorphismes, \IeC {\'e}pimorphismes et isomorphismes dans les cat\IeC {\'e}gories concr\IeC {\`e}tes}}{21}{subsubsection.1.4.5}
\contentsline {subsection}{\tocsubsection {}{1.5}{Produits et coproduits}}{22}{subsection.1.5}
\contentsline {subsubsection}{\tocsubsubsection {}{1.5.1}{Produits classiques}}{22}{subsubsection.1.5.1}
\contentsline {subsubsection}{\tocsubsubsection {}{1.5.2}{Exemple motivant : produit cart\IeC {\'e}sien dans les ensembles}}{23}{subsubsection.1.5.2}
\contentsline {subsubsection}{\tocsubsubsection {}{1.5.3}{D\'efinition g\'en\'erale.}}{23}{subsubsection.1.5.3}
\contentsline {subsubsection}{\tocsubsubsection {}{1.5.4}{Propri\IeC {\'e}t\IeC {\'e}s des produits}}{24}{subsubsection.1.5.4}
\contentsline {subsubsection}{\tocsubsubsection {}{1.5.5}{Lien avec l'objet terminal}}{26}{subsubsection.1.5.5}
\contentsline {subsubsection}{\tocsubsubsection {}{1.5.6}{Coproduit d'objets dans une cat\'egorie}}{26}{subsubsection.1.5.6}
\contentsline {subsubsection}{\tocsubsubsection {}{1.5.7}{Propri\IeC {\'e}t\IeC {\'e}s des coproduits}}{27}{subsubsection.1.5.7}
\contentsline {subsubsection}{\tocsubsubsection {}{1.5.8}{Lien avec l'objet initial}}{28}{subsubsection.1.5.8}
\contentsline {section}{\tocsection {}{2}{Foncteurs}}{28}{section.2}
\contentsline {subsection}{\tocsubsection {}{2.1}{D\IeC {\'e}finition et composition}}{28}{subsection.2.1}
\contentsline {subsection}{\tocsubsection {}{2.2}{Cat\IeC {\'e}gories de cat\IeC {\'e}gories}}{30}{subsection.2.2}
\contentsline {subsection}{\tocsubsection {}{2.3}{Propri\IeC {\'e}t\IeC {\'e}s des foncteurs}}{30}{subsection.2.3}
\contentsline {section}{\tocsection {}{3}{Transformations naturelles}}{32}{section.3}
\contentsline {subsection}{\tocsubsection {}{3.1}{D\IeC {\'e}finitions}}{32}{subsection.3.1}
\contentsline {subsection}{\tocsubsection {}{3.2}{\'Equivalence de cat\IeC {\'e}gories}}{35}{subsection.3.2}
\contentsline {subsubsection}{\tocsubsubsection {}{3.2.1}{D\IeC {\'e}finition}}{35}{subsubsection.3.2.1}
\contentsline {subsubsection}{\tocsubsubsection {}{3.2.2}{Caract\IeC {\'e}risation}}{36}{subsubsection.3.2.2}
\contentsline {subsubsection}{\tocsubsubsection {}{3.2.3}{Cat\IeC {\'e}gories squel\IeC {\'e}tales}}{37}{subsubsection.3.2.3}
\contentsline {subsection}{\tocsubsection {}{3.3}{Foncteurs repr\IeC {\'e}sentables}}{38}{subsection.3.3}
\contentsline {subsubsection}{\tocsubsubsection {}{3.3.1}{D\IeC {\'e}finitions}}{38}{subsubsection.3.3.1}
\contentsline {subsubsection}{\tocsubsubsection {}{3.3.2}{Vers la gare du Nord ...}}{39}{subsubsection.3.3.2}
\contentsline {subsection}{\tocsubsection {}{3.4}{Monades et programmation}}{41}{subsection.3.4}
\contentsline {section}{\tocsection {Annexe}{A}{Exercices}}{41}{appendix.A}
