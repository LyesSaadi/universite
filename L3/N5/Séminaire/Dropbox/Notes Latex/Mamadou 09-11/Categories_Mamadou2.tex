\documentclass[twoside, 11pt]{amsart}
\usepackage{tikz}
\usetikzlibrary{positioning, arrows.meta}
\include{package}
\usepackage{amsthm}

\begin{document}

\subsection{Première tentative de caractérisation des équivalences de catégories}
\text{\\}
    \begin{theo}
        Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories localement petites et $F : \mathcal{C} \to \mathcal{D}$ un foncteur. On dit que $F$ fait partie d'une équivalence de catégorie si les conditions suivantes sont satisfaites :
        \begin{itemize}
            \item $F$ est essentiellement surjectif ("surjectif au niveau des classes d'isomorphismes d'objets")
            \item $F$ est un foncteur plein
            \item $F$ est un foncteur fidèle.
        \end{itemize}
    \end{theo}

    \begin{proof}
        Nous allons procéder par analyse-synthèse. Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories localement petites et soit $F : \mathcal{C} \to \mathcal{D}$ un foncteur.
        \begin{itemize}
            \item Analyse: Supposons que $F$ fasse partie d'une équivalence de catégorie. Alors il existe un foncteur $G : \mathcal{C} \to \mathcal{D}$ et des isomorphismes naturels:  $ \eta : \mathbb{1}_{\mathcal{C}} \stackrel{\cong}{\Rightarrow} G\circ F$ et $ \epsilon: \mathbb{1}_{\mathcal{D}} \stackrel{\cong}{\Rightarrow} F\circ G$.
                Pour tout $d$ appartenant à $\mathcal{D}$, nous avons $F\circ G(d) \stackrel{{\epsilon}_{d}}{\cong}d $. Il existe donc un $c = G(d)$ dans $\mathcal{C}$ tel que $F(c)\cong d$. Par conséquent, F est esssentiellement surjective. \\
                
                Avec l'isomorphisme naturel $\eta$, pour $c$ et $c'$ dans $\mathcal{C}$, le diagramme suivant commute:

                \[
                \begin{tikzcd}
                    c \arrow[r, "\eta_{c}"] \arrow[d, "f"'] & G\circ F(c)~\, \arrow[d, "G\circ F(f)"] \\
                    c' \arrow[r, "\eta_{c'}"'] & G\circ F(c')~. 
                \end{tikzcd}
                \]

                $\eta_{c}$ et $\eta_{c'}$ tous deux des isomorphismes de foncteur, on a $$G\circ F(f)= \eta_{c'} \circ (f) \circ \eta_{c} ^{-1}  \Rightarrow f=\eta_{c'} ^{-1} \circ(G\circ F(f))\circ\eta_{c}. $$ 
                Si on considére deux flèches  $f,f': c \to c' $ tel que $F(f) = F(f')$, on obtient: 
                $$ G\circ F(f) = G\circ F(f')\Rightarrow \eta_{c'} ^{-1} \circ(G\circ F(f))\circ\eta_{c} = \eta_{c'}^{-1} \circ(G\circ F(f'))\circ\eta_{c} \Rightarrow f = f' $$ 
                d'où F est fidèle. \\


            \item Synthèse: Supposons maintenant que $F$ soit un foncteur plein, fidèle et essentiellement surjectif. Notre but est de prouver que ce foncteur appartient à une classe d'équivalence. Construisons un foncteur $G : \mathcal{D} \to \mathcal{C}$ qui à tout $d$ dans $\mathcal{D}$, on associe (axiome du choix) un $c$ dans $\mathcal{C}$  tel que $F(c) = F(G(d))\stackrel{\alpha_{d}}{\cong}d$ (un tel $c$ existe vu que $F$ est essentiellement surjectif).
            Pour tout $d$, $d'$ dans $\mathcal{D}$ et $h : d\to d'$, par isomorphisme des $\alpha_{d}$ et essentielle surjectivité de F, le diagramme suivant commute.

            \[
                \begin{tikzcd}
                    F\circ G(d) \arrow[r, "\alpha_{d}", "\cong"'] \arrow[d, "f"'] & d ~\, \arrow[d, "h"] \\
                    F\circ G(d') \arrow[r, "\alpha_{d'}"', "\cong"] & d'~. 
                \end{tikzcd}
            \]

            Puisque $F$ est fidèle, il existe un unique morphisme $k: G(d)\to G(d')$ tel que $F(k) = f$. On pose $G(h) = k$.
            $G$ définit bel et bien un foncteur. En effet, nous avons pour tout $d\in \mathcal{D}$, $$F\circ G(d)\xrightarrow{F\circ \mathbb{1}_{G(d)}} F\circ G(d)$$
            Par définiton de $G$, nous avons aussi $$F\circ G(d)\xrightarrow{F\circ G (\mathbb{1}_{d})} F\circ G(d).$$
            Par unicité du morphisme $f$ et fidélité de $F$, on obtient $$ F\circ G (\mathbb{1}_{d}) = F\circ \mathbb{1}_{G(d)} \Rightarrow G (\mathbb{1}_{d}) = \mathbb{1}_{G(d)}.$$
            Avec un  raisonnement similaire, on prouve que $G$ est stable par composition.
            Par conséquent, la famille des isomorphismes $\{\alpha_{d} : F\circ G(d)\to d\}_{d\in \mathcal{D}}$ constitue un isomorphisme naturel $\alpha : F\circ G\Rightarrow \mathbb{1}_{\mathcal{D}}$ : le diagramme ci-dessus verifiant la condition de naturalité de $\alpha$.\\
            Il ne reste qu'à montrer que $G\circ F \stackrel{\cong}{\Rightarrow}\mathbb{1}_{\mathcal{C}}$    
              
        \end{itemize}
    \end{proof}


\subsection{Catégories squelétales}
    \begin{defi}(Catégorie squelétale)
        Soit $\mathcal{C}$ une catégorie aléatoire . $\mathcal{C}$ est dite \emph{squelétale} si toutes ses classes d'équivalence d'objets (à isomorphisme près) n'ont qu'un élément.
    \end{defi}
    \begin{exem}
        Les catégories $\mathcal{Mat}_{dim finie}$ et $\mathcal{Fin}$ vues précèdemment sont des catégories squelétales. 
    \end{exem}

    \begin{prop}
        Toute catégorie $\mathcal{C}$ localement petite admet une catégorie squelétale équivalente $\mathcal{sqC}$. Cette catégorie est unique à isomorphisme près. 
    \end{prop}

    Avant de démontrer cette proposition, énonçons un lemme qui nous sera utile pour la démonstration.
    \begin{lemm}
        Deux catégories squelétales équivalentes sont isomorphes.
    \end{lemm}
    \begin{proof}
        Soient $\mathcal{S}$ et $\mathcal{S'}$ deux catégories squelétales telles que $\mathcal{S} \rightleftarrows \mathcal{S'}$.
        Alors il existe des foncteurs $F : \mathcal{S} \to \mathcal{S'}$, $G : \mathcal{S'} \to \mathcal{S}$ et des isomorphismes naturels:  $ \eta : \mathbb{1}_{\mathcal{S}} \stackrel{\cong}{\Rightarrow} G\circ F$ et $ \epsilon: \mathbb{1}_{\mathcal{S'}} \stackrel{\cong}{\Rightarrow} F\circ G$.
        Pour tout $s$ appartenant à $\mathcal{S}$, on a $\eta_{s} : G\circ F(s)\to s$ est un isomorphisme dans $\mathcal{S}$. Vu que $\mathcal{S}$ est squelétale, le seul objet de $\mathcal{S}$ isomorphe à s est lui même, d'où $G\circ F(s) = s$. On obtient donc, avec la naturalité de $\eta$
        l'égalité de foncteur $G\circ F = \mathbb{1}_{\mathcal{S}}$. Par un raisonnement analogue, on obtient $ F\circ G = \mathbb{1}_{\mathcal{S'}}$ et on conclut que $\mathcal{S} \cong \mathcal{S'}$
    \end{proof}
    On peut à présent procéder à la démonstration de la proposition ci-dessus.
    \begin{proof}
        \text{\\}
        \begin{enumerate}
            \item Construction de $\mathcal{sqC}$ : Soit $\mathcal{C}$ une catégorie localement petite, donc les objets de $\mathcal{C}$ constituent un ensemble. Construisons la catégorie $\mathcal{sqC}$ comme suit:\\ \\
            \emph{Objets}: les classes d'équivalences à isomorphisme près des objets de la catégorie $\mathcal{C}$ \\ 
            \emph{Morphismes}:  On fixe pour chaque classe d'équivalence A un objet $a$ et on a pour tout $A$, $A'$ dans $\mathcal{sqC}$: \\
            
            $Hom_{\mathcal{sqC}}(A, A') =  \left\{ \begin{array}{rcl}
                A\to A' & \mbox{si}
                & a\to a' \\ \emptyset & sinon \\
                       \end{array}\right.$ \\
            \emph{Composition}: definie à partir de la composition dans $\mathcal{C}$. Pour $A$, $A'$, $A''$ dans $\mathcal{sqC}$, si on a une flèche $A\to A'$ et une flèche $A'\to A''$, il existe un morphisme $A\to A''$. \\
            \emph{Identités}: Pour tout objet $A$, il existe un morphisme $A\to A$. \\
            Les axiomes d'associativité et d'unitarité de la composition de $\mathcal{sqC}$ sont vérifiés, ils découlent de ceux de la catégorie $\mathcal{C}$.\\
            \\
            Par sa construction, $\mathcal{sqC}$ est un catégorie squelétale: en effet, supposons qu'on ait pour deux objets $A$ et $A''$ distincts dans  $\mathcal{sqC}$, $A\cong A''$ . Alors on aurait $a\cong a''$ dans $\mathcal{C}$ donc $a$ et $a''$ appartiendraient à la même classe d'équivalence, impliquant ainsi que  $A=A''$ (absurde).\\ \\
            \item Construction du Foncteur F : A présent, construisons un foncteur: $$F: \mathcal{sqC} \to \mathcal{C}$$ $$A\to a$$ où l'objet $a$ de $\mathcal{C}$ est l'objet de $\mathcal{C}$ qui fut fixé pour la construction des morphismes de $\mathcal{sqC}$ (axiome du choix).

            Etant de donné qu'un objet $A$ de $\mathcal{sqC}$ soit associé à un objet $a$ qui par définition appartient à la classe d'équivalence engendrant $A$, on a $$\forall a_{1} \;dans\; \mathcal{C}, \;il \;existe \;un \;A \;dans\; \mathcal{sqC} \;tel \;que\; F(A)\cong a_{1}.$$  F est donc essentiellement surjective.
            Par construction de $F$ et de $\mathcal{sqC}$, pour tout morphisme $g: a \to a'$, il existe un unique morphise $f:F(A)\to F(A')$ tel que $F(f)=g $. Donc $F$ est à la fois plein et fidèle.
            F fait donc partie d'une équivalence de catégorie, d'où $ \mathcal{C}\rightleftarrows \mathcal{sqC}$ par le Théorème 1.
            \\ \\ \\ 

            
            \item Unicité à isomorphisme près de $\mathcal{sqC}$ : Supposons que $\mathcal{C}$ soit équivalente à deux catégorie squelétales $\mathcal{S}$ et $\mathcal{S'}$. Etant donné que la relation "être équivalent" est une relation d'équivalence entre catégories, on obtient par transitivité : 
            $$\mathcal{S}\rightleftarrows\mathcal{C} \;et\;\mathcal{C}\rightleftarrows \mathcal{S'}\Rightarrow\mathcal{S}\rightleftarrows \mathcal{S'}\Rightarrow\mathcal{S}\cong \mathcal{S'}(Lemme\; 1)$$
            
        \end{enumerate}
    \end{proof}
    



\end{document}