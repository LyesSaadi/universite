\documentclass[twoside, 11pt]{amsart}
\include{package}
\usepackage{amsmath}
\usepackage{tikz-cd} 

\begin{document}

\maketitle
\section{Propriétés des morphismes}
\subsection{Isomorphisme dans une Catégorie}
\begin{defi}[Isomorphisme]
Dans une catégorie $\mathcal{C}$, deux objets $A$ et $B$ sont dits isomorphes si et seulement si il existe des morphismes $f : A \to B$ et $g : B \to A$ tels que les compositions $f \circ g$ et $g \circ f$ soient respectivement égales aux morphismes d'identité des objets $A$ et $B$, c'est-à-dire :
\[
f \circ g = \text{id}_A
\]
et
\[
g \circ f = \text{id}_B.
\]
\end{defi}
\textbf{ Explication :}
L'isomorphisme est une notion fondamentale en théorie des catégories. Il représente une équivalence spéciale entre deux objets de la catégorie. Pour que deux objets, $A$ et $B$, soient considérés comme isomorphes, il doit exister deux morphismes, $f$ de $A$ vers $B$ et $g$ de $B$ vers $A$, tels que leurs compositions fournissent les morphismes d'identité de leurs objets respectifs.

Concrètement, cela signifie que si vous prenez le chemin $f$ de $A$ à $B$ suivi du chemin $g$ de $B$ à $A$, vous revenez à votre point de départ, qui est $A$, et inversement si vous prenez le chemin $g$ de $B$ à $A$ suivi du chemin $f$ de $A$ à $B$, vous revenez à $B$. Cela montre que $f$ et $g$ agissent comme des "morphismes inverses" l'un de l'autre.

Le diagramme ci-dessus illustre cette notion d'isomorphisme dans une catégorie. Les flèches représentent les morphismes, et les carrés indiquent que les compositions $f \circ g$ et $g \circ f$ sont les morphismes d'identité correspondants. En conséquence, $f$ est un isomorphisme de l'objet $A$ vers l'objet $B$, et $g$ est son inverse, ce qui signifie que $A$ et $B$ sont isomorphes dans la catégorie $\mathcal{C}$.

\begin{minipage}{0.5\textwidth}
\centering
\begin{tikzcd}
A \arrow[r, "f", "\sim"'] \arrow[rd, "id_A"'] & B \arrow[d, "g"] \\
& A
\end{tikzcd}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\centering
\begin{tikzcd}
A \arrow[r, "g", "\sim"'] \arrow[rd, "id_A"'] & B \arrow[d, "f"] \\
& A
\end{tikzcd}
\end{minipage}

\subsection{Monomorphismes et Applications Injectives}

\begin{defi}[Fonction injective]
Soit A et B des ensembles, une fonction \(f : A \to B\) est \textbf{injective} si, pour tous \(x\) et \(y\) appartenant à \(A\), la propriété suivante est satisfaite : \\

\begin{center}
$\forall x, y \in A, \quad f(x) = f(y) \Rightarrow x = y$
\end{center}
\end{defi}
\begin{defi}[Monomorphisme]
Un monomorphisme (ou simplement un mono) est un morphisme $f : A \rightarrow B$ tel que, pour tout objet $X$ et $Y$ ainsi que pour tout morphisme $x : X \rightarrow A$ et $y : Y \rightarrow A$, si $f \circ x = f \circ y$, alors $x = y$.

En d'autres termes, si deux morphismes distincts $x$ et $y$ de $X$ vers $A$ produisent le même résultat lorsqu'ils sont composés avec $f$, alors $x$ et $y$ doivent être identiques.


\end{defi}

\begin{prop}
Nous allons maintenant prouver que dans la catégorie des ensembles $\mathcal{Ens}$ , les monomorphismes sont précisément les applications injectives.\\
Dans $\mathcal{Ens}$, \(f : A \to B\) \\
\begin{center}
$f$ est une application injective $\iff$ $f$ est un monomorphisme
\end{center}
\end{prop}

\begin{proof}

($\Leftarrow$)Supposons que $f : A \to B$ est un monomorphisme.
Nous voulons montrer que $f$ est injective.\\
Soit $x$ et $y$ deux éléments distincts de l'ensemble $A$. Nous devons montrer que $f(x) \neq f(y)$.\\
Considérons l'ensemble unitaire $\{1\}$ qui a un seul élément. Dans la catégorie des ensembles, il existe deux morphismes uniques de $\{1\}$ vers $A$, que nous noterons $g$ et $h$, tels que $g(1) = x$ et $h(1) = y$.\\
Maintenant, étant donné que $f$ est un monomorphisme, si $f \circ g = f \circ h$, alors $g = h$.\\
Cependant, si $f(x) = f(y)$, alors nous aurions $f \circ g(1) = f \circ h(1)$, ce qui impliquerait que $g(1) = h(1)$.\\
Mais nous savons que $g(1) = x$ et $h(1) = y$, et comme $x \neq y$, cela contredirait l'égalité $g(1) = h(1)$.\\
Par conséquent, si $f$ est un monomorphisme dans la catégorie donnée, alors $f$ est injective dans la catégorie des ensembles.

($\Rightarrow$) Réciproquement, supposons que $f: A \to B$ soit une fonction injective. Nous devons montrer que $f$ est un monomorphisme.

Soit $X$ un objet quelconque et $h, k: X \to A$ deux morphismes tels que $f \circ h = f \circ k$. En utilisant l'associativité de la composition des morphismes, nous pouvons écrire :

\[
f \circ h = f \circ k \Rightarrow f \circ h = f \circ k \circ \text{id}_X \Rightarrow f \circ (h \circ \text{id}_X) = f \circ (k \circ \text{id}_X).
\]

Maintenant, comme $f$ est une fonction injective, pour tout $x$ dans $X$, $h(x) = k(x)$ (car $f(h(x)) = f(k(x))$ implique $h(x) = k(x)$). Ainsi, nous avons $h \circ \text{id}_X = k \circ \text{id}_X$.

Et comme $h \circ \text{id}_X = h$ et $k \circ \text{id}_X = k$, nous concluons que $h = k$. Cela montre que $f$ est un monomorphisme.

Ainsi, dans la catégorie des ensembles (Ens), les monomorphismes sont précisément les applications injectives.
\end{proof}

\begin{defi}[Monomorphisme Fort]

Dans certaines catégories, il existe une notion plus forte que celle des monomorphismes, appelée monomorphisme fort. Un monomorphisme fort est un morphisme $f : A \to B$ tel que pour tout objet $B$ de la catégorie, il existe un unique morphisme $g : B \to A$ tel que $f \circ g = \text{id}_B$.

\begin{center}
\begin{tikzcd}
A \arrow[r, "f"] \arrow[rd, "id_A"'] & B \arrow[d, "g"] \\
& A
\end{tikzcd}
\end{center}
\end{defi}

\begin{prop}

Tout monomorphisme fort est également un monomorphisme. 
\end{prop}
\begin{proof}
Supposons que $f : A \to B$ est un monomorphisme fort, et considérons deux morphismes $h$ et $k : X \to A$ tels que $f \circ h = f \circ k$. Alors, nous pouvons déduire :
\[
f \circ h &= f \circ k \Rightarrow  g \circ f \circ h &= g \circ f \circ k \Rightarrow \text{id} \circ h &= \text{id} \circ k
\]
\end{proof}

\begin{defi}[Sous-catégorie des monomorphismes]
Dans une catégorie donnée $\mathcal{C}$, on peut définir une \textbf{sous-catégorie $\mathcal{C}_\text{mono}$} qui contient les mêmes objets que $\mathcal{C}$, mais seulement certains des morphismes. Plus précisément, $\mathcal{C}_\text{mono}$ contient les monomorphismes de $\mathcal{C}$ en tant que morphismes, mais pas nécessairement tous les morphismes de $\mathcal{C}$. 
\end{defi}

\begin{prop}
Si $f : A \to B$ est un monomorphisme dans $\mathcal{C}$, alors $f$ est également un morphisme dans $\mathcal{C}_\text{mono}$.
\end{prop}

\begin{coro}
$\mathcal{C}_\text{mono}$ est une sous-catégorie de $\mathcal{C}$ . 
\end{coro}

\begin{proof}
L'identité d'un objet dans n'importe quelle catégorie est toujours un monomorphisme par définition. En effet, pour tout objet $X$ et tout morphisme $f : X \to A$, si $id_A \circ f = id_A \circ f$, alors $f = f$. Par conséquent, l'identité est un monomorphisme dans la catégorie $\mathcal{C}_\text{mono}$.
    
 Pour montrer que la composition de deux monomorphismes dans $\mathcal{C}_\text{mono}$ est un monomorphisme dans $\mathcal{C}_\text{mono}$, commençons par supposer que $f : A \to B$ et $g : B \to C$ sont deux monomorphismes dans $C_{\text{mono}}$.

Nous voulons montrer que leur composition $g \circ f : A \to C$ est également un monomorphisme dans $\mathcal{C}_\text{mono}$.

Considérons deux morphismes $h : X \to A$ et $k : X \to A$ pour un objet $X$ de $\mathcal{C}_\text{mono}$ tels que $(g \circ f) \circ h = (g \circ f) \circ k$.

En utilisant la propriété associative de la composition, nous obtenons $g \circ (f \circ h) = g \circ (f \circ k)$.

Puisque $g$ est un monomorphisme, nous pouvons conclure que $f \circ h = f \circ k$.

Et puisque $f$ est également un monomorphisme, cela implique que $h = k$.


\end{proof}

\begin{defi}[Monomorphisme dans un Ensemble Partiellement Ordonné]
Dans une catégorie, il est également possible de définir les monomorphismes dans le contexte d'un ensemble partiellement ordonné (EPO). Un EPO est caractérisé par une relation d'ordre partiel entre ses éléments, où chaque paire d'éléments peut être comparée.

\textbf{Objets :} Les objets de cette catégorie sont les éléments de l'ordre partiel. Chaque élément de l'ordre partiel devient un objet de la catégorie.

\textbf{Morphismes :} Les morphismes (ou flèches) de cette catégorie sont définis par la relation d'ordre de l'ensemble partiellement ordonné. Pour chaque paire d'objets $x$ et $y$ de la catégorie, c'est-à-dire, chaque paire d'éléments de l'ordre partiel, il existe un unique morphisme de $x$ vers $y$ si et seulement si $x \leq y$ dans l'ordre partiel.

\textbf{Composition :} La composition des morphismes dans cette catégorie suit directement la composition de la relation d'ordre dans l'ensemble partiellement ordonné. Si $x \leq y$ et $y \leq z$, alors $x \leq z$, ce qui se traduit par la composition des morphismes $x \to y$ et $y \to z$ pour obtenir un morphisme $x \to z$.

\end{defi}

\begin{prop}
Dans un ensemble partiellement ordonné $(E, \leq)$, tous les morphismes sont des monomorphismes.
\end{prop}
    

\end{document}


