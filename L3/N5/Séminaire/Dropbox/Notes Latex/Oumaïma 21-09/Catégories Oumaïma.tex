\documentclass[twoside, 11pt]{amsart}
\include{package}
\usepackage{amsmath}
\usepackage{tikz-cd} 

\begin{document}

\maketitle
\section{Propriétés des morphismes}
\subsection{Isomorphisme dans une Catégorie}
\begin{defi}[Isomorphisme]
Dans une catégorie, un isomorphisme est une notion d'équivalence entre deux objets. Plus précisément, dans une catégorie $\mathcal{C}$, deux objets $A$ et $B$ sont dits isomorphes s'il existe des morphismes $f : A \to B$ et $g : B \to A$ tels que les compositions $f \circ g$ et $g \circ f$ sont les morphismes d'identité des objets $A$ et $B$, respectivement. Voici ce que cela signifie :

Existence des morphismes inverses : Il existe un morphisme $f : A \to B$ et un morphisme $g : B \to A$.

Compositions pour les identités :
\[
f \circ g = \text{id}_A,
\]
où $\text{id}_A$ est le morphisme d'identité de l'objet $A$.
\[
g \circ f = \text{id}_B,
\]
où $\text{id}_B$ est le morphisme d'identité de l'objet $B$.

En d'autres termes, avoir des morphismes $f$ et $g$ tels que leurs compositions produisent les morphismes d'identité signifie que $f$ est un isomorphisme de l'objet $A$ vers l'objet $B$, et $g$ est son inverse.

\begin{minipage}{0.5\textwidth}
\centering
\begin{tikzcd}
A \arrow[r, "f", "\sim"'] \arrow[rd, "id_A"'] & B \arrow[d, "g"] \\
& A
\end{tikzcd}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\centering
\begin{tikzcd}
A \arrow[r, "g", "\sim"'] \arrow[rd, "id_A"'] & B \arrow[d, "f"] \\
& A
\end{tikzcd}
\end{minipage}
\end{defi}

\subsection{Monomorphismes et Applications Injectives}

\begin{defi}[Fonction injective]
une fonction \(f : A \to B\) est \textbf{injective} si, pour tous \(x\) et \(y\) appartenant à \(A\), la propriété suivante est satisfaite : \\

\begin{center}
$\forall x, y \in A, \quad f(x) = f(y) \Rightarrow x = y$
\end{center}
\end{defi}
\begin{defi}[Monomorphisme]
Un monomorphisme (ou simplement un mono) est un morphisme c tel que pour tous les objets \(X\) et \(Y\) et tous les morphismes \(x\) et \(y : X \to A\), si \(f \circ x = f \circ y\), alors \(x = y\).

En d'autres termes, si deux morphismes différents \(x\) et \(y\) de \(X\) à \(A\) donnent le même résultat lorsqu'ils sont composés avec \(f\), alors \(x\) et \(y\) doivent être égaux.

\end{defi}

\begin{prop}
Nous allons maintenant prouver que dans la catégorie des ensembles $\mathcal{C}_\text{Ens}$ , les monomorphismes sont précisément les applications injectives.\\
Dans Ens, \(f : A \to B\) \\
\begin{center}
$f$ est une application injective $\iff$ $f$ est un monomorphisme
\end{center}
\end{prop}

\begin{proof}

($\Leftarrow$) Supposons que $f : A \to B$ est \textbf{un monomorphisme} dans $\mathcal{Ens}$, et que $f$ n'est pas une application injective. Cela signifie qu'il existe des éléments $x$ et $y$ dans $A$ tels que $x \neq y$, mais $f(x) = f(y)$.

Considérons deux morphismes $h$ et $k$ de $\{1\}$ (un ensemble à un élément) vers $A$, définis comme suit :
\[ h : \{1\} \to A, \quad h(1) = x, \]
\[ k : \{1\} \to A, \quad k(1) = y. \]

Puisque $f(x) = f(y)$, nous avons $f \circ h = f \circ k$. Cependant, $h \neq k$ puisque $h(1) \neq k(1)$, ce qui contredit la définition de monomorphisme. Par conséquent, $f$ doit être une application injective.

($\Rightarrow$) Réciproquement, supposons que $f : A \to B$ est \textbf{ une fonction injective}. Nous devons montrer que c'est un monomorphisme.

Soit $X$ un objet quelconque et $h$, $k : X \to A$ deux morphismes tels que $f \circ h = f \circ k$. En utilisant l'associativité de la composition des morphismes, nous pouvons écrire :
\[ f \circ h = f \circ k \Rightarrow f \circ h = f \circ k \circ \text{id}_X \Rightarrow f \circ (h \circ \text{id}_X) = f \circ (k \circ \text{id}_X). \]

Puisque $f$ est injective, nous pouvons annuler $f$ de chaque côté de l'équation, ce qui donne :
\[ h \circ \text{id}_X = k \circ \text{id}_X. \]

Comme $h \circ \text{id}_X = h$ et $k \circ \text{id}_X = k$, nous concluons que $h = k$. Cela montre que $f$ est un monomorphisme.

Ainsi, dans $\mathcal{C}_\text{Ens}$ , les monomorphismes sont précisément les applications injectives.
\end{proof}
\begin{defi}[Monomorphisme Fort]

Dans certaines catégories, il existe une notion plus forte que celle des monomorphismes, appelée \textbf{monomorphisme fort}. Un monomorphisme fort est un morphisme \(f : A \rightarrow B\) tel que pour tout objet \(X\) de la catégorie, il existe un unique morphisme \(g : X \rightarrow A\) tel que \(f \circ g = \text{id}_X\), où \(\text{id}_X\) est le morphisme identité de l'objet \(X\). 
\begin{center}
\begin{tikzcd}
X \arrow[r, "g"] \arrow[dr, swap, "\text{id}_X"] & A \arrow[d, "f"] \\
& B
\end{tikzcd}
\end{center}

La propriété essentielle d'un monomorphisme fort est que, pour tout \(X\), il existe un seul \(g\) tel que \(f \circ g = \text{id}_X\), garantissant ainsi que \(f\) préserve la structure des objets et des morphismes dans la catégorie.

\end{defi}

\begin{prop}

Tout monomorphisme fort est également un monomorphisme. 
\end{prop}
\begin{proof}
Supposons que $f : A \to B$ est un monomorphisme fort, et considérons deux morphismes $g_1$ et $g_2 : X \to A$ tels que $f \circ g_1 = f \circ g_2$. En utilisant la propriété de monomorphisme fort, nous pouvons trouver un unique morphisme $h_1 : X \to A$ tel que $f \circ h_1 = g_1$ et un unique morphisme $h_2 : X \to A$ tel que $f \circ h_2 = g_2$.

Puisque $f \circ g_1 = f \circ g_2$, nous avons également $f \circ h_1 = f \circ h_2$. Comme $f$ est un monomorphisme fort, cela implique que $h_1 = h_2$. Par conséquent, $g_1 = f \circ h_1 = f \circ h_2 = g_2$, ce qui montre que $f$ est un monomorphisme.
\end{proof}

\begin{defi}[Sous-catégorie des monomorphismes]
Dans une catégorie donnée $\mathcal{C}$, on peut définir une \textbf{sous-catégorie $\mathcal{C}_\text{mono}$} qui contient les mêmes objets que $\mathcal{C}$, mais seulement certains des morphismes. Plus précisément, $\mathcal{C}_\text{mono}$ contient les monomorphismes de $\mathcal{C}$ en tant que morphismes, mais pas nécessairement tous les morphismes de $\mathcal{C}$. 
\end{defi}

\begin{prop}
Si $f : A \to B$ est un monomorphisme dans $\mathcal{C}$, alors $f$ est également un morphisme dans $\mathcal{C}_\text{mono}$.
\end{prop}

\begin{proof}
Supposons que \(f: A \rightarrow B\) est un monomorphisme dans $\mathcal{C}$. Nous voulons montrer que \(f\) est également un morphisme dans $\mathcal{C}_\text{mono}$. Pour ce faire, nous devons montrer que \(f\) est un monomorphisme dans $\mathcal{C}$.

Soit \(X\) et \(Y\) deux objets de $\mathcal{C}$ et \(h, k: X \rightarrow A\) deux flèches de \(X\) à \(A\) telles que \(f \circ h = f \circ k\).

Puisque \(f\) est un monomorphisme dans $\mathcal{C}$, nous avons que \(h = k\). Cela découle directement de la définition de monomorphisme dans $\mathcal{C}$.

Ainsi, \(f\) satisfait la propriété d'un monomorphisme dans $\mathcal{C}_\text{mono}$.

Par conséquent, nous avons montré que si \(f\) est un monomorphisme dans $\mathcal{C}$ , alors il est également un morphisme dans $\mathcal{C}_\text{mono}$.
\end{proof}

\begin{coro}
$\mathcal{C}_\text{mono}$ est une sous-catégorie de $\mathcal{C}$ . 
\end{coro}

\begin{defi}[Monomorphisme dans un Ensemble Partiellement Ordonné]
Dans une catégorie, les monomorphismes peuvent également être définis dans le contexte d'un ensemble partiellement ordonné (EPO). Dans un EPO, chaque paire d'éléments est comparée selon une relation d'ordre partiel.

\textbf{Un monomorphisme dans un EPO} est un morphisme de flèche qui préserve l'ordre partiel entre les éléments. Formellement, si $f : A \rightarrow B$ est un monomorphisme dans un EPO, alors pour tout $a$ et $b$ dans $A$, si $a \leq b$, alors $f(a) \leq f(b)$.

Cela signifie que si $a$ est inférieur ou égal à $b$ dans l'ordre partiel, alors l'image de $a$ sous $f$ doit être inférieure ou égale à l'image de $b$ sous $f$.

La notion de monomorphismes dans un EPO est essentielle pour étudier les relations d'ordre et de comparaison dans un ensemble partiellement ordonné. Elle permet de caractériser des propriétés d'ordre importantes et des structures mathématiques dans ce contexte.
\end{defi}

\begin{exem}

Considérons l'ensemble partiellement ordonné \(P\) de tous les entiers naturels positifs (\(\mathbb{N}^+\)) avec la relation d'ordre définie par la divisibilité. C'est-à-dire, pour \(a, b \in \mathbb{N}^+\), nous disons que \(a \leq b\) si et seulement si \(a\) divise \(b\) (c'est-à-dire que \(b\) est un multiple de \(a\)).

Dans cet EPO, considérons deux éléments :

\begin{align*}
a &= 2 \\
b &= 6
\end{align*}

Nous pouvons voir que \(2\) divise \(6\), donc \(2 \leq 6\) dans notre EPO.

Maintenant, supposons que nous ayons une fonction \(f : \mathbb{N}^+ \rightarrow \mathbb{N}^+\) définie comme suit :

\[
f(x) = 2x \text{ pour tout } x \text{ dans } \mathbb{N}^+.
\]

Nous affirmons que \(f\) est un monomorphisme dans cet EPO. Pour le prouver, nous devons montrer que \(f\) préserve l'ordre partiel.

- Soit \(a_1 = 2\) et \(a_2 = 3\) deux éléments de \(\mathbb{N}^+\) tels que \(a_1 \leq a_2\) (car \(2\) divise \(3\)).

- Ensuite, nous appliquons \(f\) à ces éléments : \(f(a_1) = 2 \cdot 2 = 4\) et \(f(a_2) = 2 \cdot 3 = 6\).

- Nous voyons que \(f(a_1) \leq f(a_2)\) (car \(4\) divise \(6\)).

Ainsi, \(f\) préserve l'ordre partiel, ce qui signifie que \(f\) est un monomorphisme dans cet ensemble partiellement ordonné.
\end{exem}
\end{document}


