
Les catégories de $\mathcal{Vect}$ des espaces vectoriels, $\mathcal{Ab}$ des groupes abéliens et $\mathcal{Cor}$ des corps sont comme des <<continents>> mathématiques distincts, chacun abritant sa propre richesse et complexité. Cependant, il peut être intéressant de construire des ponts élégants qui relient ces <<continents>>, permettant ainsi un échange fluide d'idées et de concepts. Ces ponts nous permettent de voir comment les espaces vectoriels de la catégorie $\mathcal{Vect}$, les groupes abéliens de la catégorie $\mathcal{Ab}$ et les corps de la catégorie $\mathcal{Cor}$ peuvent interagir harmonieusement, révélant ainsi des perspectives inédites sur la nature profonde des mathématiques.

	\subsection{Définition et composition}
\begin{defi}[Foncteur]		
Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. Un \emph{foncteur} \(\mathrm{F}\) de $\mathcal{C}$ vers $\mathcal{D}$ est un processus qui associe 
\begin{itemize}[label=\textbullet]
	\item un objet \(\mathrm{F}(A)\) de $\mathrm{Obj}_{\mathcal{D}}$, à tout objet $A$ de $\mathrm{Obj}_{\mathcal{C}}$, 
	\item  un morphisme  \(\mathrm{F}(f)\) de $\mathrm{Hom}_\mathcal{D}(\mathrm{F}(A), \mathrm{F}(A')$, 
	à tout morphisme $f$ de $\mathrm{Hom}_\mathcal{C}(A, A')$,
\end{itemize}
\begin{center}
		\begin{tikzpicture}
			\node at (4,3) {$\operatorname{Flech}_\mathcal{D}$};
        	\node at (0,3) {$\operatorname{Flech}_\mathcal{C}$};
        	\node at (0,0) {$\operatorname{Obj}_\mathcal{C}$};
        	\node at (4,0) {$\operatorname{Obj}_\mathcal{D}$};
        	
        	\draw[->,red] (0.7,3) -- node[above]{\(\mathrm{F}\)$_{\operatorname{Flech}}$} (3.3,3);
    	\draw[->, red] (0.7,0.03) -- node[above]{\(\mathrm{F}\)$_{\operatorname{Obj}}$} (3.3,0.03);
    	
	\draw[->, blue] (0.1,2.6) -- node[left]{$\operatorname{Source}_\mathcal{C}/\operatorname{But}_\mathcal{C}$} (0.1,0.4);
    	\draw[->, blue] (3.8,2.6) -- node[right]{$\operatorname{Source}_\mathcal{D}/\operatorname{But}_\mathcal{D}$} (3.8,0.4);
    	\end{tikzpicture}
	\end{center}
de telle sorte que 
		\begin{itemize}[label=\textbullet]
			\item $\mathrm{F}(f\circ_{\mathcal C} g) = \mathrm{F}(f)\circ_{\mathcal D} \mathrm{F}(g)$, 
			pour tout morphisme $f$ de $\mathrm{Hom}_\mathcal{C}(A', A'')$ et  $g$ de $\mathrm{Hom}_\mathcal{C}(A, A')$~, 
			\item \(\mathrm{F}\)($ \mathrm{id}_A$) = $ \mathrm{id}_{\mathrm{F}(A)}$~. 
		\end{itemize}
\end{defi}

Heuristiquement, un foncteur \(\mathrm{F}\) de $\mathcal{C}$ vers $\mathcal{D}$ est une <<application>> préservant les structures catégoriques respectives. 

\begin{exems}\leavevmode	
	\begin{itemize}[label=\textbullet]
		\item Soient la catégorie $\mathcal{Ens}_{fini}$ des ensembles finis et la catégorie $					\mathcal{Ens}$ des ensembles. On définit le foncteur \emph{oubli} $\mathrm{F} : \mathcal{Ens}_{fini} \to \mathcal{Ens}$ qui envoie tout ensemble fini $A$ sur lui-même $\mathrm{F}(A) = A$ et 
		qui envoie toute application ensembliste $f : A \to B$ entre ensembles finis sur elle-même 
		$\mathrm{F}(f) = f$. il préserve les compositions et identités respectives. 
		\item Soient la catégorie $\mathcal{Grp}$ des groupes et la catégorie $							\mathcal{Ens}$ des ensembles. On définit le foncteur \emph{oubli}  $\mathrm{O} :  \mathcal{Grp} \to  \mathcal{Ens}$ qui associe à tout groupe  $(G,x,1)$ son ensemble sous-jacent $G$ (où on oubli la structure de groupe de G) et qui associe à tout morphisme de groupes $f : (G,x,1) \to (G',\cdot,0)$ son application ensembliste sous-jacente $\mathrm{O}(f) : G \to G'$. 
		\item Soient $X$ et $Y$ deux ensembles munis de deux ordres partiels  $\pi_X := (X, \leq)$ et $\pi_Y := (Y, \preceq)$. La donnée d'un foncteur $\mathrm{F} : \mathcal{C}_{\pi_X} \to \mathcal{C}_{\pi_Y}$ entre les catégories associées est équivalente à la donnée d'une application croissante $f : X \to Y$. 
	
	\item Pour toute catégorie $\mathcal C$, le foncteur \emph{identité} $\mathrm{Id} : \mathcal C \to \mathcal C$ 
	associe  à tout objet de $A$ lui-même et à tout morphisme $f$ lui-même. 
	\end{itemize}
\end{exems}	

\begin{defprop}[Composition de foncteurs]
	Soient $\mathcal{C}$, $\mathcal{D}$ et $\mathcal{E}$ trois catégories, et soient 
	$\mathrm{F} : \mathcal{C} \to  \mathcal{D}$ et $\mathrm{G} :  \mathcal{D} \to \mathcal{E}$ deux foncteurs, où $\mathrm{F}$ envoie les objets de $\mathcal{C}$ dans les objets de $\mathcal{D}$ et les flèches de $\mathcal{C}$ dans les flèches de $\mathcal{D}$, et où $\mathrm{G}$ envoie les objets de $\mathcal{D}$ dans les objets de $\mathcal{E}$ et les flèches de $\mathcal{D}$ dans les flèches de $\mathcal{E}$.
	La \emph{composition} de $\mathrm{F}$ et $\mathrm{G}$, notée $\mathrm{G}\circ\mathrm{F}$, est le foncteur de $\mathcal{C}$ dans $\mathcal{E}$ défini comme suit :
	\begin{itemize}[label=\textbullet]
		\item à chaque objet $X$ de $\mathrm{Obj}_\mathcal{C}$, il associe l'objet 
		$\mathrm{G}(\mathrm{F}(X))$ de $\mathrm{Obj}_{\mathcal{E}}$, 
		\item à chaque morphisme $f$ dans $\mathrm{Hom}_\mathcal{C}(X, Y)$, il associe le morphisme  
		$\mathrm{G}(\mathrm{F}(f))$ de 
		 $\mathrm{Hom}_\mathcal{E}(\mathrm{G}(\mathrm{F}(X)),\allowbreak \mathrm{G}(\mathrm{F}(Y)))$.
	\end{itemize}
\end{defprop}

	En d'autres termes, pour chaque objet et chaque morphisme de la catégorie $\mathcal{C}$, la composition $\mathrm{G}\circ\mathrm{F}$ applique d'abord le foncteur $\mathrm{F}$ pour obtenir l'objet ou le morphisme correspondant dans $\mathcal{D}$, puis elle applique le foncteur $\mathrm{G}$ pour obtenir l'objet ou le morphisme correspondant dans $\mathcal{E}$.

\begin{proo}		
Il reste à montrer que le composé de deux foncteurs est encore un foncteur. Soient $g : A \to A'$ et $f : A' \to A''$ deux morphismes de la catégorie $\mathcal C$. On a bien 
\[
\mathrm{G}\big(\mathrm{F}(f \circ_\mathcal{C} g)\big)=
\mathrm{G}\big(\mathrm{F}(f) \circ_\mathcal{D} \mathrm{F}(g)\big)=
\mathrm{G}(\mathrm{F}(f)) \circ_\mathcal{E} \mathrm{G}(\mathrm{F}(g))
~.\]
De la même manière, pour tout objet $A$ de la catégorie $\mathcal C$, on a
\[
\mathrm{G}\big(\mathrm{F}(\mathrm{id}_A)\big)=
\mathrm{G}\big(\mathrm{id}_{\mathrm{F}(A)}\big)=\mathrm{id}_{\mathrm{G}(\mathrm{F}(A))}
~.\]
\end{proo}

\subsection{Catégories de catégories}

Pour se faire du bien au cerveau, on peut donc voir que les catégories forment une collection d'<<objets>> et que les foncteurs sont des <<flèches>> entre eux ... On doit donc pouvoir empaqueter tout cela dans une ... catégorie ! 

\begin{defprop}[Catégorie des petites catégories]
	La \emph{catégorie des petites catégories}, notée $\mathcal{Cat}$, est la catégorie définie de la manière suivante : 
	\begin{itemize}[label=\textbullet]
		\item les objets de $\mathcal{Cat}$ sont les petites catégories, c'est-à-dire les catégories où les objets et les morphismes forment des ensembles, 
		\item les morphismes de $\mathcal{Cat}$ sont les foncteurs entre les petites catégories, 
		\item la composition des morphismes dans $\mathcal{Cat}$ est la composition des foncteurs, 
		\item les identités de $\mathcal{Cat}$ sont les foncteurs identités $\mathrm{Id}_\mathcal{C}$, pour toute petite catégorie $\mathcal{C}$. 
	\end{itemize}
\end{defprop}

\begin{proo}
Il suffit de vérifier que la composition des foncteurs est associative et unitaire, ce qui est immédiat. 
\end{proo}	
	
\begin{defprop}[Catégories des catégories localement petites]
	La \emph{catégorie des catégories localement petites}, notée $\mathcal{CAT}$, est la catégorie définie de la manière suivante : 
	\begin{itemize}[label=\textbullet]
		\item les objets de $\mathcal{CAT}$ sont les catégories localement petites, c'est-à-dire les catégories où  les morphismes de $\mathrm{Hom}_{\mathcal C}(X,Y)$ forment  des ensembles, pour chaque paire d'objets $X,Y$ de $\mathcal C$,
		\item les morphismes de $\mathcal{CAT}$ sont les foncteurs entre les catégories localement petites, 
		\item la composition des morphismes dans $\mathcal{CAT}$ est la composition des foncteurs, 
		\item les identités de $\mathcal{CAT}$ sont les foncteurs identités $\mathrm{Id}_\mathcal{C}$, pour toute petite catégorie $\mathcal{C}$. 
	\end{itemize}
\end{defprop}

\begin{proo}
Cette démonstration est similaire à la précédente : il suffit de vérifier que la composition des foncteurs est associative et unitaire.
\end{proo}	

\subsection{Propriétés des foncteurs}	

Les foncteurs sont des <<applications>>, c'est-à-dire des <<flèches>> entre catégories. Peut-on alors mettre au jour des notions telles que <<foncteur injectif>> ou  <<mono-foncteur>>,  <<foncteur surjectif>> ou <<épi-foncteur>>, 
<<foncteur bijectif>> ou <<iso-foncteur>> ? 

\begin{defi}[Foncteur fidèle]
	Soient $\mathcal{C}$, $\mathcal{D}$ deux catégories et soit $\mathrm{F} : \mathcal{C} \to \mathcal{D}$ un foncteur. On dit que le foncteur $\mathrm{F}$ est  \emph{fidèle} si, pour toute paire d'objets $A$ et $B$ de $\mathcal{C}$ et pour toute paire de  morphismes $f,g : A \to B$, l’égalité $\mathrm{F}(f) = \mathrm{F}(g) :  \mathrm{F}(A) \to  \mathrm{F}(B)$ de morphismes dans $\mathcal{D}$ entraîne l'égalité de morphismes $f = g$ dans $\mathcal{C}$. 
\end{defi}	

Lorsque les catégories $\mathcal C$ et $\mathcal D$ sont localement petites, cela signifie que les applications ensemblistes $\mathrm{F} : \mathcal{C}(A,B) \to  \mathcal{D}(\mathrm{F}(A),\mathrm{F}(B))$ sont injectives, pour toute paire d'objets $A,B$ de $\mathcal C$.

\begin{exem}
Le foncteur \emph{oubli} $\mathrm{O} : \mathcal{Grp} \to \mathcal{Ens}$ est fidèle.
\end{exem}

\begin{defi}[Foncteur plein]
	Soient $\mathcal{C}$, $\mathcal{D}$ deux catégories et soit $\mathrm{F} : \mathcal{C} \to \mathcal{D}$ un foncteur. On dit que le foncteur $\mathrm{F}$ est  \emph{plein} si, pour toute paire d'objets $A$ et $B$ de $\mathcal{C}$ et pour tout morphisme $g : \mathrm{F}(A) \to \mathrm{F}(B)$ de $\mathcal D$,  il existe un morphisme $f : A \to  B$ de $\mathcal{C}$ tel que $g = \mathrm{F}(f)$.
	\end{defi}

Lorsque les catégories $\mathcal C$ et $\mathcal D$ sont localement petites, cela signifie que les applications ensemblistes $\mathrm{F} : \mathcal{C}(A,B) \to  \mathcal{D}(\mathrm{F}(A),\mathrm{F}(B))$ sont surjectives, pour toute paire d'objets $A,B$ de $\mathcal C$.

\begin{exem}	Le foncteur identité 
$\mathrm{Id} : \mathcal{C} \to \mathcal{C}$ est toujours  plein.  
\end{exem}
	
\begin{contrex}
Le foncteur \emph{oubli} $\mathrm{O} : \mathcal{Grp} \to \mathcal{Ens}$ n'est pas plein car il existe des applications ensemblistes entre groupes qui ne sont pas des morphismes. 
\end{contrex}	
	
\begin{defi}[Monofoncteur]
	Soient $\mathcal{C}$, $\mathcal{D}$ deux catégories petites ou localement petites et soit $\mathrm{F} : \mathcal{C} \to \mathcal{D}$ un foncteur. On dit que $\mathrm{F}$ est un \emph{monofoncteur}
	si c'est un monomorphisme dans la catégorie $\mathcal{Cat}$ ou $\mathcal{CAT}$, c'est-à-dire s'il vérifie  la propriété suivante : toute paire $\mathrm{G}_1, \mathrm{G}_2 : \mathcal{E} \to \mathcal{C}$ de foncteurs tels que 
	$\mathrm{F} \circ \mathrm{G}_1 = \mathrm{F} \circ \mathrm{G}_2$ sont égaux $\mathrm{G}_1=\mathrm{G}_2$.
\end{defi}

\begin{defi}[Épifoncteur]
	Soient $\mathcal{C}$, $\mathcal{D}$ deux catégories petites ou localement petites et soit $\mathrm{F} : \mathcal{C} \to \mathcal{D}$ un foncteur. On dit que $\mathrm{F}$ est un \emph{épifoncteur}
	si c'est un épimorphisme dans la catégorie $\mathcal{Cat}$ ou $\mathcal{CAT}$, c'est-à-dire s'il vérifie  la propriété suivante : toute paire $\mathrm{G}_1, \mathrm{G}_2 : \mathcal{D} \to \mathcal{E}$ de foncteurs tels que 
	$\mathrm{G}_1 \circ \mathrm{F} =  \mathrm{G}_2\circ \mathrm{F}$ sont égaux $\mathrm{G}_1=\mathrm{G}_2$.
\end{defi}	
		
\begin{defi}[Isofoncteur ou foncteur inversible]
	Soient $\mathcal{C}$, $\mathcal{D}$ deux catégories petites ou localement petites et soit $\mathrm{F} : \mathcal{C} \to \mathcal{D}$ un foncteur. On dit que $\mathrm{F}$ est un \emph{isofoncteur} ou aussi un \emph{foncteur inversible}
	si c'est un isomorphisme dans la catégorie $\mathcal{Cat}$ ou $\mathcal{CAT}$, c'est-à-dire s'il admet un foncteur $\mathrm{G} : \mathcal{D} \to \mathcal{C}$ tel que $\mathrm{G} \circ \mathrm{F} = \mathrm{Id}_{\mathcal{C}}$ et $\mathrm{F} \circ \mathrm{G} = \mathrm{Id}_{\mathcal{D}}$.
\end{defi}

\begin{rema}
Ces notions sont bien définies pour toute catégorie, si on utilise la dernière propriété exprimée.
\end{rema}
