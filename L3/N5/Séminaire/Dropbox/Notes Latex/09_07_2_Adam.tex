Imaginez-vous dans un vaste musée, où les chefs-d'œuvre de l'art mathématique prennent vie sous la lumière douce de la théorie et l'abstraction. Au cœur de cette galerie se trouve une salle mystérieuse, un sanctuaire intellectuel où les mathématiciens et mathématiciennes se réunissent pour explorer l'un des concepts les plus puissants et englobants qui soient : les catégories. Les catégories sont bien plus que de simples ensembles d'objets et de flèches que nous allons découvrir par la suite. Elles sont un concept central en mathématiques qui permet de structurer et d'organiser les objets mathématiques ainsi que les relations entre eux afin d’avoir une vue globale de ces dernières et de les étudier uniformément. Cette partie va nous aider à pénétrer dans ce monde abstrait et à découvrir ces fameuses catégories en s’appuyant sur des exemples pertinents et en soulignant sur les remarques, questions faites et réflexions qui en découlent.

\subsection{D\'efinitions et exemples}

\subsubsection{D\'efinition}

\begin{defi}[Catégorie]
Une \emph{catégorie} $\mathcal{C}$ est composée des éléments suivants :
\begin{itemize}[label=\textbullet]
\item une collection d'\emph{objets}, notée $\mathrm{Obj}_\mathcal{C}$ : ${X}, {Y}, {Z},  \ldots$, 
\item une collection de \emph{flèches}, notée $\mathrm{Flech}_\mathcal{C}$ : 
$\nearrow, \swarrow, \uparrow, \downarrow, \rightarrow, \ldots $, 
\item deux applications $\mathrm{source} : \mathrm{Flech}_\mathcal{C} \rightarrow \mathrm{Obj}_\mathcal{C}$ et $\mathrm{but} : \mathrm{Flech}_\mathcal{C} \rightarrow \mathrm{Obj}_\mathcal{C}$, 
\item une \emph{composition} : pour toute paire de flèches
$$
f \colon X \rightarrow {Y} \quad \text{et} \quad {g} \colon {Y} \rightarrow {Z} \quad 
\text{telles que}\quad  \mathrm{source}(g)=\mathrm{but}(f),$$
il existe une flèche 
$$g \circ f : X \to Z \quad 
\text{telle que}\quad  \mathrm{source}(g\circ f)=\mathrm{source}(f) \ \text{et}\ \mathrm{but}(g\circ f)=\mathrm{but}(g),$$
\item des \emph{identités} : pour tout objet $X$ de $\mathcal{C}$,  il existe une flèche 
$$ \mathrm{id}_X : X \to X 
\quad 
\text{telle que}\quad  \mathrm{source}(\mathrm{id}_X)=\mathrm{but}(\mathrm{id}_X)=X.$$
\end{itemize}
La composition est \emph{associative} 
\[h\circ (g\circ f) = (h\circ g)\circ f\]
et \emph{unitaire} 
\[f\circ \mathrm{id}_X=f \quad \text{et} \quad \mathrm{id}_Y \circ f=f.\]
\end{defi}

\subsubsection{Catégories <<concrètes>>}

\begin{exem}[La catégorie des ensembles] 
La \emph{catégorie $\mathcal{Ens}$ des ensembles} est formée des données suivantes : 
    \begin{itemize}[label=\textbullet]
\item ses objets $\textrm{Obj}_{\mathcal{Ens}}$ sont les ensembles : ${X}, {Y}, {Z},  \ldots$, 
\item ses flèches $\text{Flech}_\mathcal{Ens}$ sont les applications ensemblistes :  $f \colon X \to Y$, 
\item l'application $\mathrm{source}$ associe à toute flèche $f \colon X \to Y$ son ensemble de départ ($X$) et l'application $\mathrm{but}$ associe à toute flèche $f \colon X \to Y$ son ensemble d'arrivée ($Y$), 
\item la composition des flèches est la composition usuelle $g \circ f$ des applications ensemblistes, 
\item pour tout ensemble $X$, sa flèche identité est l'application ensembliste identité de $X$ définie par $\mathrm{id}_X(x)\coloneq x$.
\end{itemize}
\end{exem}

\begin{rema}
Il n'existe pas d'ensemble de tous les ensembles (paradoxe de Russell), c'est pourquoi on a utilisé la notion de collection dans la définition de catégories et non celle d'ensemble. Ceci nous a permis d'inclure l'exemple de la catégorie des ensembles. 
\end{rema}

\begin{exem}[Catégorie des espaces vectoriels]
La \emph{catégorie $\mathcal{Vect}$ des espaces vectoriels} sur un corps $\mathbb{K}$ fixé est formée des données suivantes : 
    \begin{itemize}[label=\textbullet]
\item ses objets $\textrm{Obj}_{\mathcal{Vect}}$ sont les espaces vectoriels sur le corps $\mathbb{K}$, 
\item ses flèches $\text{Flech}_\mathcal{Vect}$ sont les applications linéaires, 
\item l'application $\mathrm{source}$ associe à toute flèche $f \colon X \to Y$ son espace vectoriel de départ ($X$) et l'application $\mathrm{but}$ associe à toute flèche $f \colon X \to Y$ son espace vectoriel d'arrivée ($Y$), 
\item la composition des flèches est la composition $g \circ f$ des applications linéaires, 
\item pour tout espace vectoriel $X$, sa flèche identité est l'automorphisme identité $\mathrm{id}_X$ de $X$.
\end{itemize}
\end{exem}

\begin{exem}[Catégories des corps]
La \emph{catégorie $\mathcal{Cor}$ des corps} est formée des données suivantes : 
\begin{itemize}[label=\textbullet]
\item ses objets $\textrm{Obj}_{\mathcal{Cor}}$ sont les corps, 
\item ses flèches $\text{Flech}_\mathcal{Cor}$ sont les morphismes de corps, 
\item l'application $\mathrm{source}$ associe à toute flèche $f \colon X \to Y$ son corps de départ ($X$) et l'application $\mathrm{but}$ associe à toute flèche $f \colon X \to Y$ son corps d'arrivée ($Y$), 
\item la composition des flèches est la composition $g \circ f$ des morphismes de corps, 
\item pour tout corps $\mathbb{K}$, sa flèche identité est l'automorphisme identité $\mathrm{id}_{\mathbb{K}}$ de $\mathbb{K}$.
\end{itemize}
\end{exem}    

De manière analogue, on pourrait introduire les catégories des anneaux, des groupes, des groupes abéliens, etc. On parle ici de catégories <<concrètes>> car les flèches sont toutes des applications ensemblistes (vérifiant parfois des propriétés). 

\subsubsection{Catégories <<non-concrètes>>}
Toutes les catégories ne sont pas forcément faites de flèches qui sont des applications ensemblistes, en voici quelques exemples. 

\begin{exem}[Catégories des quaternions]
La catégorie $\mathcal{Quat}$ des quaternions est formée des données suivantes : 
\begin{itemize}[label=\textbullet]
\item cette catégorie n'a qu'un seul objet $\textrm{Obj}_{\mathcal{Quat}}\coloneq \{\bullet\}$, 
\item ses flèches sont les éléments de l'algèbre des quaternions 
\[\text{Flech}_\mathcal{Quat}\coloneq \mathbb{H}=\{\alpha 1+\beta \mathrm{i} +\gamma \mathrm{j}+\delta \mathrm{k}\, |\,  \alpha, \beta, \gamma, \delta \in \mathbb{R}\},\]
\item les deux applications $\mathrm{source}$ et $\mathrm{but}$ sont identiques : elles associent à tout quaternion l'unique objet $\bullet$, 
\item la composition des flèches est définie par le produit des quaternions dont on rappelle qu'il est déterminé par 
\begin{align*}
& \mathrm{i}^2=\mathrm{j}^2=\mathrm{k}^2=\mathrm{ijk}=-1~,  \\
& \mathrm{ij}=-\mathrm{ji}=\mathrm{k}~, \ \mathrm{jk}=-\mathrm{kj}=\mathrm{i}~,\  
\mathrm{ki}=-\mathrm{ik}=\mathrm{j}~, 
\end{align*}
\item l'identité de l'unique objet $\bullet$ est l'unité $1$ de l'algèbre des quaternions. 
\end{itemize}
\end{exem}

\begin{exem}[Les catégories $\mathcal{C}_{\mathbb{Z}+}$ et $\mathcal{C}_{\mathbb{Z}*}$]
La catégorie $\mathcal{C}_{\mathbb{Z}+}$  est formée des données suivantes : 
\begin{itemize}[label=\textbullet]
\item cette catégorie n'a qu'un seul objet $\textrm{Obj}_{\mathcal{C}_{\mathbb{Z}+}}\coloneq \{\bullet\}$, 
\item ses flèches sont les entiers relatifs $\text{Flech}_{\mathcal{C}_{\mathbb{Z}+}}\coloneq \mathbb{Z}$, 
\item les deux applications $\mathrm{source}$ et $\mathrm{but}$ sont identiques : elles associent à tout entier l'unique objet $\bullet$, 
\item la composition des flèches est définie par la \emph{somme} des entiers, 
\item l'identité de l'unique objet $\bullet$ est $0$. 
\end{itemize}

La catégorie $\mathcal{C}_{\mathbb{Z}*}$  est formée des données suivantes : 
\begin{itemize}[label=\textbullet]
\item cette catégorie n'a qu'un seul objet $\textrm{Obj}_{\mathcal{C}_{\mathbb{Z}*}}\coloneq \{\bullet\}$, 
\item ses flèches sont les entiers relatifs $\text{Flech}_{\mathcal{C}_{\mathbb{Z}*}}\coloneq \mathbb{Z}$, 
\item les deux applications $\mathrm{source}$ et $\mathrm{but}$ sont identiques : elles associent à tout entier l'unique objet $\bullet$, 
\item la composition des flèches est définie par le \emph{produit} des entiers, 
\item l'identité de l'unique objet $\bullet$ est $1$. 
\end{itemize}
\[
\begin{tikzcd}
    \bullet 
    \ar[loop, ->, in=80,out=10,looseness=10, "0"']
    \ar[loop, ->, in=170,out=100,looseness=10, "1"']
    \ar[loop, ->, in=260,out=190,looseness=10, "2"']
     \ar[loop, ->, phantom, in=350,out=280,looseness=5, "\ldots"]
\end{tikzcd}
\]
\end{exem}

Ces deux derniers exemples montrent qu'il ne suffit pas de donner les objets et les flèches pour décrire une catégorie : les données des compositions et des identités sont importantes. Dans les deux cas précédents, les objets et les flèches sont les mêmes, mais les deux catégories diffèrent par leurs compositions et identités. 

\subsubsection{Contre-exemple}
Quoi de mieux pour bien assimiler la notion de catégorie que de trouver un <<contre-exemple>>.
On considère les données suivantes  : 
\begin{itemize}[label=\textbullet]
\item on se donne 4 objets : $X , Y , Z , W$, 
\item on se donne 7 flèches : 
$f \colon X \to Y$, $g \colon Y \to Z$, $h \colon Z \to W$, 
$\phi \colon X \to Z$, $\psi \colon Y \to W$,  $\alpha \colon X \to W$, $\beta \colon W \to Z$, 
\[\begin{tikzcd}
  X \arrow[r, "f"]   \arrow[rrr, bend left=50, "\beta"] \arrow[rr, bend right=35, "\phi"] \arrow[rrr, bend right=50, "\alpha"] & Y \arrow[r, "g"]  \arrow[rr, bend left=35, "\psi"]& Z \arrow[r, "h"]& W
\end{tikzcd}\]
\item la composition est définie par : $g\circ f \coloneq \phi$, 
$h\circ g \coloneq \psi$, 
$\psi \circ f \coloneq \beta$, 
$h\circ \phi  \coloneq \alpha$. 
\item chaque objet $X, Y, Z, W$ admet une identité respective $\mathrm{id}_X, \mathrm{id}_Y, \mathrm{id}_Z, \mathrm{id}_W$.
\end{itemize}

On voit que la composition \emph{n'est pas associative} : $(h\circ g) \circ f = \beta \neq \alpha = 
h\circ (g \circ f)$. Bon, on voit bien que nous avons tout fait pour construire un contre-exemple \emph{ad hoc} de catégorie ...
