\subsection{Monades et programmation} 
	\begin{defi}
          Une \emph{monade} $\mathbb{T}$ sur une cat\'egorie $\mathcal{C}$
          comprend les donn\'ees suivantes:
          \begin{itemize}[label=\textbullet]
            \item  Pour tout objet $A$, un objet $TA$
              $\in \mathcal{C}$ et un morphisme $\eta_A : A \to TA$.
              \item Pour tout morphisme $g : A \to TB$, un morphisme
                $g^\dagger : TA \to TB$.
              \end{itemize}
              Ces donn\'ees doivent satisfaire trois propriétés:
                \begin{enumerate}
			\item Pour tout $A$, 
$\eta_{A}^\dagger$ = $\mathrm{id}_{TA}$.
			\item Pour tout morphisme $f : A \to TB$, le diagramme
                          suivant commute:
                                             \[\begin{tikzcd}
	A & TA \\
	& TB
	\arrow["{\eta_A}", from=1-1, to=1-2]
	\arrow["{f^\dagger}", from=1-2, to=2-2]
	\arrow["f"', from=1-1, to=2-2]
      \end{tikzcd}\]
			\item Pour toute paire de morphismes $f : A
                          \to TB$ et $g : B \to TC$,  le diagramme
                          suivant commute:
                          \[\begin{tikzcd}
	TA & TB \\
	& TC
	\arrow["{f^\dagger}", from=1-1, to=1-2]
	\arrow["{g^\dagger}", from=1-2, to=2-2]
	\arrow["{(g^\dagger \circ f)^\dagger}"', from=1-1, to=2-2]
\end{tikzcd}\]
		\end{enumerate}
              \end{defi}

\'Etant donn\'ee une monade $\mathbb{T}$ sur une cat\'egorie
$\mathcal{C}$, on peut construire une nouvelle catégorie, appel\'ee
$\mathcal{C}_{\mathbb{T}}$, dont les objets sont ceux de
$\mathcal{C}$. Les morphismes de $\mathcal{C}_{\mathbb{T}}$ sont
d\'efinis par $\mathrm{Hom}_{\mathcal{C}_{\mathbb{T}}}(A, B) =
\mathrm{Hom}_{\mathcal{C}}(A, TB)$. L'identité sur $A$ est donnée par $\eta_A$,
et la compositon de $f : A \to TB$ et $g : B \to TC$ est le morphisme
$f ; g = g^{+}\circ{f} : A \to TC$. 

\begin{prop}
  La construction  de $\mathcal{C}_{\mathbb{T}}$ donn\'ee ci-dessus
  donne bien une cat\'egorie.
\end{prop}
\begin{proof}
  La composition <<$;$>> est associative : considérons $f : A \to TB$,
  $g : B \to TC$ et $h : C \to TD$ trois morphismes de
  $\mathcal{C}_{\mathbb{T}}$. Alors, 
		 $(f;g);h = (g^{+}\circ{f});{h} =
                 h^{+}\circ{(g^{+}\circ{f})} =
                 (h^{+}\circ{g^{+}})\circ{f}$, et de la m\^eme
                 mani\`ere on a 
		 $ f;{(g ;{h})} = f;{(h^{+}\circ{g})} =
                 (h^{+}\circ{g})^{+}\circ{f} =
                 (h^{+}\circ{g^{+}})\circ{f}$, en utilisant la
                 propri\'et\'e (3). 
		On peut \'egalement montrer que 
                 $\eta$ est un \'el\'ement neutre pour l'identit\'e. En
                effet, pour  $f : A \to TB$, on a 
		$\eta_A ; {f} = f^{+}\circ{\eta_A} = f$ par (2), et $f
                ;{\eta_B} = {\eta_B}^{+}\circ{f} = \mathrm{id}_{TB}\circ{f} = f$ par (1).
          \end{proof}
          \begin{exem} On consid\'ere la cat\'egorie
            $\mathcal{Ens}_\mathrm{print}$ (\emph{cf.}
            \'Exemple~\ref{ex:print}). Il s'agit en fait de la
            cat\'egorie associ\'ee \`a une monade : pour chaque objet
            $A$, on prend $TA = A\times\mathrm{string}$ et pour $f :
            A \to B\times \mathrm{string}$ on d\'efinit $f^\dagger : A
            \times \mathrm{string} \to B \times \mathrm{string}$
comme suit:
            \[
              A \times \mathrm{string} \xrightarrow{f} B \times
              \mathrm{string} \times \mathrm{string}
              \xrightarrow{\mathrm{id}_B \times \mathrm{concat}} B
              \times \mathrm{string}. 
            \]
            (Il reste \`a v\'erifier que la composition et les
            identit\'es sont les bonnes.)
	\end{exem}


	% \begin{exem} On peut prendre la catégorie Ens dont chaque objet A est un programme et chaque morphisme prend un programme avec la mémoire A, fait une lecture seule et renvoie B $(f : A\times{M} \to B)$. Ainsi pour chaque objet A on prend $TA = Hom_{Ens}(M,A)$ et pour tous f : $A \to Hom_{Ens}(M,B)$ et  g : $B \to Hom_{Ens}(M,C)$ des morphismes de $Ens_\mathcal{T}$ on a alors
	% 	\[f\star{g} : A \to Hom_{Ens}(M,C) ,  f\star{g}(a) = g(f(a)(m)).\]
	% \end{exem}
	
	% \begin{exem} On peut même généraliser le cas précédent de sorte que chaque morphisme prend $A\times{M}$ et retourne $B\times{M}$. Dans ce cas pour chaque objet A on aura donc $TA = Hom_{Ens}(M,(A\times{M})).$
	% \end{exem}

        \begin{exem}
D'autres exemples de monades sont utilis\'es pour mod\'eliser les
programmes qui utilisent l'\'etat (c'est-\`a-dire la m\'emoire). Par
exemple, la monade sur $\mathcal{Ens}$ d\'efinie par $TA =
\mathrm{Hom}(M, A)$, pour un ensemble $M$ fix\'e (repr\'esentant les
valeurs possible de la m\'emoire), repr\'esente les programmes qui
peuvent seulement \emph{lire} l'\'etat. Pour des programmes qui peuvent
\'egalement le modifier, on prend $TA = \mathrm{Hom}(M, A \times M)$.
        \end{exem}
        
        %%% Local Variables:
%%% mode: latex
%%% TeX-master: "Categories"
%%% End:
