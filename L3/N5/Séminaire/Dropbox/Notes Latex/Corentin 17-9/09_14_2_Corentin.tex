Maintenant que nous avons vu comment définir la notion de catégorie, nous pouvons chercher comment construire de nouvelles catégories à partir de catégories existantes.

\subsubsection{Union disjointe de catégories}

\begin{defprop}[Union disjointe de catégories]

    Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. On appelle \emph{union disjointe de $\mathcal{C}$ et $\mathcal{D}$} la catégorie $\mathcal{C} \sqcup \mathcal{D}$ formée des données suivantes :
    \begin{itemize}[label=\textbullet]
        \item $\mathrm{Obj}(C \sqcup D) :=$ La collection formée des éléments de $\mathrm{Obj}(C) \text{ et } \mathrm{Obj}(D)$ telle que les éléments de $\mathrm{Obj}(C) \text{ et } \mathrm{Obj}(D)$ sont disjoints.
        \item $\mathrm{Hom}_{C \sqcup D}(e,f) :=
            \begin{cases}
                \mathrm{Hom}_C(e,f)\text{ si } e \text{ et } f \text{ sont dans } \mathrm{Obj}(C)\\
                \mathrm{Hom}_D(e,f)\text{ si } e \text{ et } f \text{ sont dans } \mathrm{Obj}(D)\\
                \emptyset \text{ sinon}
            \end{cases}$
        \item Les compositions et les identités de $C \sqcup D$ sont celles de $C$ et de $D$.
    \end{itemize}
\end{defprop}    

\begin{exem}
\
    \begin{center}
    \begin{tikzpicture}
        \node (C) at (1,2.5) {$C$};
        \node (1) at (1,2) {$1$};
        \node (2) at (0.5,1) {$2$};
        \node (3) at (1.5,1) {$3$};
        \node (4) at (1,0) {$4$};

        \draw[->] (1) to (2);
        \draw[->] (2) to (3);
        \draw[->] (2) to (4);
        \draw[->] (1) to (3);
        \draw[->, bend right=100] (1) to (4);
        
        \node (D) at (4,2.5) {$D$};
        \node (a) at (4,2) {$a$};
        \node (b) at (3.5,1) {$b$};
        \node (c) at (4.5,1) {$c$};
        
        \draw[->] (a) to (b);
        \draw[->] (b) to (c);
        \draw[->] (c) to (a);
    \end{tikzpicture}
    \end{center}
    \begin{center}
    \begin{tikzpicture}
        \node (C) at (2,2.5) {$C \sqcup D$};
        \node (1) at (1,2) {$1$};
        \node (2) at (0.5,1) {$2$};
        \node (3) at (1.5,1) {$3$};
        \node (4) at (1,0) {$4$};

        \draw[->] (1) to (2);
        \draw[->] (2) to (3);
        \draw[->] (2) to (4);
        \draw[->] (1) to (3);
        \draw[->, bend right=100] (1) to (4);
        
        \node (a) at (3,2) {$a$};
        \node (b) at (2.5,1) {$b$};
        \node (c) at (3.5,1) {$c$};
        
        \draw[->] (a) to (b);
        \draw[->] (b) to (c);
        \draw[->] (c) to (a);
    \end{tikzpicture}
    \end{center}
\end{exem}

\begin{proof}
\
    \begin{itemize}
        \item La composition est bien associative par l'associativité des compositions de $C$ et $D$.
        \item La composition est bien unitaire par l'unitarité des identités de $C$ et $D$.
    \end{itemize}
\end{proof}

\begin{prop}
   La construction de l'union disjointe est \emph{commutative}, c'est-à-dire que l'on a une équivalence entre $C \sqcup D$ et $D \sqcup C$, c'est à dire qu'il existe un isomorphisme entre ces deux catégories.
\end{prop}

\subsubsection{Joint de catégories}

\begin{defprop}[Joint de catégories]

    Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. On appelle \emph{joint entre $\mathcal{C}$ et $\mathcal{D}$} la catégorie $\mathcal{C} \rhd \mathcal{D}$ formée des données suivantes :
    \begin{itemize}[label=\textbullet]
        \item $\mathrm{Obj}(C \rhd D) := \mathrm{Obj}(C \sqcup D)$ 
        \item $\mathrm{Hom}_{C \rhd D}(e,f) :=
            \begin{cases}
                \mathrm{Hom}_{C \rhd D}(e,f) = \{e \rightarrow e'\} \text{ si } e \text{ est dans } \mathrm{Obj}(C) \text{ et } f \text{ est dans } \mathrm{Obj}(D)\\
                \text{sinon } \mathrm{Hom}_{C \rhd D}(e,f) = \mathrm{Hom}_{C \sqcup D}(e,f)
            \end{cases}$
        \item Les identités de $C \rhd D$ sont celles de $C$ et celles de $D$
        \item Les compositions de $C \rhd D$ sont les compositions de $C$ et de $D$ et s'il existe des flèches $e \rightarrow f$ et $f \rightarrow g$ alors il existe une flèche $e \rightarrow g$.
    \end{itemize}
\end{defprop}

\begin{exem}
\
    \begin{center}
    \begin{tikzpicture}
        \node (C) at (1,2.5) {$C$};
        \node (1) at (1,2) {$1$};
        \node (2) at (0.5,1) {$2$};
        \node (3) at (1.5,1) {$3$};
        \node (4) at (1,0) {$4$};

        \draw[->] (1) to (2);
        \draw[->] (2) to (3);
        \draw[->] (2) to (4);
        
        \node (D) at (4,2.5) {$D$};
        \node (a) at (4,2) {$a$};
        \node (b) at (4,1) {$b$};
        
        \draw[->] (a) to (b);
    \end{tikzpicture}
    \end{center}
    \begin{center}
    \begin{tikzpicture}
        \node (C) at (2,2.5) {$C \rhd D$};
        \node (1) at (1,2) {$1$};
        \node (2) at (0.5,1) {$2$};
        \node (3) at (1.5,1) {$3$};
        \node (4) at (1,0) {$4$};

        \draw[->] (1) to (2);
        \draw[->] (2) to (3);
        \draw[->] (2) to (4);
        
        \node (a) at (2,0) {$a$};
        \node (b) at (3,0) {$b$};
        
        \draw[->] (a) to (b);
        \draw[->] (3) to (a);
        \draw[->] (4) to (a);
    \end{tikzpicture}
    \begin{tikzpicture}
        \node (C) at (2,2.5) {$D \rhd C$};
        \node (1) at (1,2) {$1$};
        \node (2) at (0.5,1) {$2$};
        \node (3) at (1.5,1) {$3$};
        \node (4) at (1,0) {$4$};

        \draw[->] (1) to (2);
        \draw[->] (2) to (3);
        \draw[->] (2) to (4);
        
        \node (a) at (3,2) {$a$};
        \node (b) at (2.5,1) {$b$};
        
        \draw[->] (a) to (b);
        \draw[->] (b) to (1);
    \end{tikzpicture}
    \end{center}
\end{exem}

\begin{proof}
\
    \begin{itemize}
        \item \textcolor{red}(Besoin d'aide pour les démonstrations de définitions-prop)
    \end{itemize}
\end{proof}

\begin{rema}
   La  joint entre les catégories n'est \emph{plus} une construction commutative.
\end{rema}

\begin{prop}
Soit $\mathcal{C}at_{[n]} :=$ la catégorie formée à partir de $(\textlbrackdbl n\textrbrackdbl ,\leq)_{n \in \mathbb{N}^*}$
On pose $\mathcal{C} = \mathcal{C}at_n$ et $\mathcal{D} = \mathcal{C}at_m$, $(n,m)\in {\mathbb{N}^{*}}^2$. On a :

\[
\mathcal{C}at_{[n]} \rhd \mathcal{C}at_{[m]}=\mathcal{C}at_{[n+m]}
\]

\end{prop}

\begin{proof}
\
    Soit $\mathcal{C}at_n :=$ la catégorie formée à partir de $(\textlbrackdbl n\textrbrackdbl ,\leq)_{n \in \mathbb{N}^*}$
On pose $\mathcal{C} = \mathcal{C}at_n$ et $\mathcal{D} = \mathcal{C}at_m$, $(n,m)\in {\mathbb{N}^{*}}^2$.

On écrit $C : 1 \rightarrow 2 \rightarrow \ldots \rightarrow n$, et de même $D : 1 \rightarrow 2 \rightarrow \ldots \rightarrow m$

(La transitivité pour la composition et la réflexivité pour l'identité sont assurés par le fait que $\leq$ est une relation d'ordre, les morphismes associés sont dont sous-entendus pour une meilleure visibilité)

On souhaiterait que le joint entre $C$ et $D$ vérifie $C \rhd D = \mathcal{C}at_{n+m}$.

Or, on a $C \rhd D : 1_C \rightarrow 2_C \rightarrow \ldots \rightarrow n \ \textcolor{teal}{\rightarrow}\  1_D \rightarrow 2_D \rightarrow \ldots \rightarrow m$.

La composition de $C \rhd D$ et le fait que chaque élément de $Obj(C)$ a un morphisme vers chaque élément de $Obj(D)$ nous permettent de compléter "l'ordre" entre $C$ et $D$, en notant :

$1_D = n+1$, $2_D = n+2$, \ldots, $m_D = n+m$

on retrouve bien la catégorie $\mathcal{C}at_{n+m}$.
\end{proof}

\subsubsection{Produit de catégories}

\begin{defprop}[Produit de catégories]

    Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. On appelle \emph{produit de $\mathcal{C}$ et $\mathcal{D}$} la catégorie $\mathcal{C} \times \mathcal{D}$ formée des données suivantes :
    \begin{itemize}[label=\textbullet]
        \item $\mathrm{Obj}(C \times D) :=$ La collection formée des paires $(e,f)$ telles que $e$ est dans $\mathrm{Obj}(C)$ et $f$ est dans $\mathrm{Obj}(D)$.
        \item $(e,f) \rightarrow (e',f')$ est dans $\mathrm{Hom}_{C \times D}((e,f),(e',f'))$ si et seulement si $e \rightarrow e'$ est dans $\mathrm{Hom}_C(e,e')$ et $f \rightarrow f'$ est dans $\mathrm{Hom}_D(f,f')$.
    \end{itemize}
\end{defprop}

\begin{exem}
\
    \begin{center}
    \begin{tikzpicture}
        \node (C) at (1,2.5) {$C$};
        \node (1) at (1,2) {$1$};
        \node (2) at (0.5,1) {$2$};
        \node (3) at (1.5,1) {$3$};

        \draw[->] (1) to (2);
        \draw[->] (2) to (3);
        \draw[->] (3) to (1);
        
        \node (D) at (3,2.5) {$D$};
        \node (a) at (3,2) {$a$};
        \node (b) at (3,1) {$b$};
        
        \draw[->] (a) to (b);
    \end{tikzpicture}
    \end{center}
    \medskip
    \begin{center}
    \begin{tikzpicture}
        \node (CxD) at (2,4) {$C \times D$};
        \node (1a) at (2,3) {$(1,a)$};
        \node (1b) at (2,2) {$(1,b)$};
        \node (2a) at (0,2) {$(2,a)$};
        \node (2b) at (2,1) {$(2,b)$};
        \node (3a) at (2,0) {$(3,a)$};
        \node (3b) at (4,1) {$(3,b)$};
        
        \draw[->] (1a) to (1b);
        \draw[->] (1a) to (2a);
        \draw[->] (2a) to (2b);
        \draw[->] (2a) to (3a);
        \draw[->] (3a) to (3b);
        \draw[bend right=57] (3a) to (5,1.5);
        \draw[->, bend right=57] (5,1.5) to (1a);
        \draw[->] (1b) to (2b);
        \draw[->] (2b) to (3b);
        \draw[->] (3b) to (1b);

    \end{tikzpicture}
    \end{center}
\end{exem}

\begin{proof}
\Bruno{a faire} \textcolor{red}(Besoin d'aide pour les démonstrations de définitions-prop)
\end{proof}

\subsubsection{Opposé de catégorie}

\begin{defi}
    Soit $C$ une catégorie, et $f : i \rightarrow j$ un morphisme de $C$. On définit $f^{op} : j \rightarrow i$ comme étant son morphisme opposé.
\end{defi}

\begin{rema}
    Soit $f : i \rightarrow j$ et $g : j \rightarrow k$ tels que $f \circ g$ (sa composition) est aussi un morphisme de $C$. On a la composition opposée $(f \circ g)^{op} := f^{op}\circ g^{op}$.
\end{rema}

\begin{defi}
    \textbf{Opposé}
    Soit $C$ une catégorie. On note $C^{op}$ la catégorie opposée formée telle que :
    \begin{itemize}
        \item $Obj(C^{op}) := Obj(C)$
        \item Un morphisme $e \rightarrow f$ est dans $Hom_{C^{op}}(e,f)$ si et seulement si $f \rightarrow e$ est dans $Hom_C(f,e)$.
        \item Les identités de $C^{op}$ sont les mêmes que celles de $C$.
        \item Par la remarque précédente, on définit les compositions de $C^{op}$ comme les opposés des compositions de $C$.
    \end{itemize}
\end{defi}

\begin{exem} Opposé de $\mathcal{C}at_n$

    $\mathcal{C}at_n = 1 \rightarrow 2 \rightarrow 3 \rightarrow \ldots \rightarrow n$
    
    $\mathcal{C}at_n^{op} = n \rightarrow \ldots \rightarrow 3 \rightarrow 2 \rightarrow 1$
\end{exem}

\subsection{Sous-catégories}

\subsubsection{Définition}

\begin{defi} Sous-catégorie. Soit $C$ une catégorie. $S$ est une sous-catégorie de $C$ (noté $S \subset C$) si et seulement si :
    \begin{itemize}
        \item Chaque élément de $Obj(S)$ est aussi dans $Obj(C)$.
        \item Pour chaque couple d'éléments $(e,f)$ dans $Obj(S)$, Chaque morphisme de $Hom_S(e,f)$ est également dans $Hom_C(e,f)$.
        \item Soient $i, j, k$ dans $Obj(S)$. S'il existe un morphisme $f : i \rightarrow j$ dans $Hom_S(i,j)$ et un morphisme $g : j \rightarrow k$ dans $Hom_S(j,k)$ alors la composition $f \circ g$ est la composition $f \circ g$ de $C$ produite par $Hom_C(i,j)$ et $Hom_C(j,k)$.
        \item Les identités de $S$ sont celles de $C$ pour les éléments de $Obj(S)$.
    \end{itemize}
\end{defi}

\begin{exem}
    Soit $C$ une catégorie telle que :
    $Obj(C) : a,b,c,d,e$ et $Hom_C :$
    
    \begin{center}
    \begin{tikzpicture}
        \node (a) at (1,3) {$a$};
        \node (b) at (1,2) {$b$};
        \node (c) at (0,1) {$c$};
        \node (d) at (2,1) {$d$};
        \node (e) at (1,0) {$e$};
        
        \draw[->] (a) to (b);
        \draw[->] (b) to (c);
        \draw[->] (b) to (d);
        \draw[->] (c) to (e);
    \end{tikzpicture}
    \end{center}
    
    On pose $S$ une catégorie telle que :
    $Obj(S) : a,b,d$ où $a_S = a_C$, $b_S = b_C$ et $d_S = d_C$ (contrairement à l'union disjointe de catégorie on considère ici les éléments nommés pareillement comme étant identiques) et $Hom_S :$
    \begin{center}
    \begin{tikzpicture}
        \node (a) at (0,1) {$a$};
        \node (b) at (1,1) {$b$};
        \node (d) at (2,1) {$d$};
        
        \draw[->] (a) to (b);
        \draw[->] (b) to (d);
    \end{tikzpicture}
    \end{center}
    
    Alors $S$ est une sous-catégorie de $C$, et on note $S \subset C$.
\end{exem}

\Bruno{Corentin [21/09]}

\begin{theo}
    Soient $C$ une catégorie et $S \subset C$, alors $S^{op} \subset C^{op}$.
    
    \begin{proof}
\
        \begin{itemize}
            \item Les éléments de $Obj(C^{op})$ sont les mêmes que $Obj(C)$, les éléments de $Obj(S^{op})$ sont les mêmes que $Obj(S)$. Ces derniers étant dans $Obj(C)$, les objets de $S^{op}$ sont aussi dans $Obj(C)$ et par conséquent dans $Obj(C^{op})$.
            \item Comme les morphismes de $S$ sont dans $C$, en passant à l'opposé on a que les morphismes opposés de $S$ sont aussi des morphismes opposés de $C$, donc les morphismes de $S^{op}$ sont dans les morphismes de $C^{op}$.
            \item Même raisonnement pour les compositions que pour les homomorphismes
            \item Même raisonnement pour les identités que pour les objets.
        \end{itemize}
    \end{proof}
\end{theo}

\subsubsection{Intersection de sous-catégories}

\begin{defi}
    Soit $C$ une catégorie. Soient $S,S'$ 2 sous-catégories de $C$. ($S \subset C$ et $S' \subset C$).
    On appelle intersection de $S$ et $S'$ (noté $S\cap S'$) est la sous-catégorie de $C$ telle que :
    \begin{itemize}
        \item Les objets de $S \cap S'$ sont les objets étant à la fois dans $S$ et dans $S'$
        \item Les morphismes entre $e$ vers $f$ dans $S \cap S'$ sont les morphismes de $e$ vers $f$ à la fois présents dans $S$ et dans $S'$.
        \item Les compositions et identités sont celles de $C$ pour les éléments de $Obj(S \cap S')$.
    \end{itemize}
    \begin{proof}
        $S \cap S'$ est bien une sous-catégorie de $C$, les compositions et identités de l'intersection étant induites par celles de $S$ et $S'$. L'associativité et l'unitarité de l'intersection sont vérifiées par l'associativité et l'unitarité de $C$.
    \end{proof}
\end{defi}

