% LaTeX : Séminaire sur les Catégories (DL3 2023)
% Auteur-trices : Hugo Paquet, Bruno Vallette, Adam, Lyes,
% Encodage : UTF 8

\documentclass[twoside, 11pt]{amsart}

\include{package}

%%%%%%%%%%%%%%%%%%%%   Titre et auteur %%%%%%%%%%%%%%%%%%%%

\title{Les cat\'egories}

\dedicatory{``Mal nommer les choses contribue au malheur du monde''' \\  \hfill \textit{Albert Camus}}

\author{Bruno Vallette}
\address{Laboratoire Analyse, G\'eom\'etrie et Applications, Universit\'e Paris 13, Sorbonne Paris Cit\'e, CNRS, UMR 7539, 93430 Villetaneuse, France.}
\email{vallette@math.univ-paris13.fr}

\date{\today}

\begin{document}

\maketitle

\begin{abstract} Notes du séminaire sur les catégories de la classe de Double Licence 3 Mathématiques-Informatique de l'Université Sorbonne Paris Nord 2023-2024.
 \end{abstract}

\setcounter{tocdepth}{2}

\tableofcontents

\section{Introduction : l'algèbre moderne}

\subsection{L'Algèbre ou l'analyse des équations}
Le mot français  <<algèbre>> provient du mot arabe <<al-jabr>> qui apparait
dans le titre du livre  <<Abrégé du calcul par la restauration et la
comparaison>> publié par en 825 par Al-Khwarizmi, mathématicien perse. 
Cet ouvrage est le premier à étudier systématiquement la résolution des
équations du premier et du second degré. Il est intéressant de  noter qu'il se
compose de deux parties : la première contient la théorie abstraite des
équations algébriques où l'accent est mis sur le type d'opérations utilisée, la
seconde contient les diverses applications en vue à l'époque comme les calculs
d'héritage, d'arpentage ou de commerce. Le mot arabe <<al-jabr>>, qui signifie
<<la réduction>> au sens de <<la réduction d'une fracture>>, est la terminologie
choisie par Al-Khwarizmi pour une des opérations sur les équations, celle  qui
consiste à réduire une équation en ajoutant des termes, soit deux termes de même
nature d'une même côté d'une équation comme 
$$x^2+3x^2= 5x+3 \ \Longleftrightarrow \ 4x^2= 5x+3 \ ,$$
soit deux termes identiques de part et d'autres de l'équation comme 
$$3x^2+3=-5x \ \Longleftrightarrow \ 3x^2+5x+3=0 \ .$$
(Il est amusant de noter qu'en espagnol le terme dérivé <<algebrista>> signifie
à la fois un algébriste ou rebouteux, celui qui réduit les fractures.)\\

Disons rapidement que jusqu'au XIX\ieme, les mathématiciens font des calculs,
parfois du même type sur des objets différents. Même s'ils savent utiliser des
variables à la place de nombres, s'ils se servent d'une notation pour le $0$ ou
qu'ils peuvent considérer des nombres imaginaires, peu de place est alors
accordée à  la théorie de ses calculs. 
La première moitié du XIX\ieme \ siècle voit une renaissance de l'Algèbre par
l'introduction de nouveaux concepts, méthodes et objets pour la résolution des
équations algébriques. D'ailleurs, Serret en 1866 écrit en introduction de son
\emph{Cours d'algèbre supérieure} (sic) que <<L'Algèbre [est], à proprement
parlé, l'Analyse des équations>>. \\


\subsection{L'algèbre ou l'axiomatisation des structures algébriques}
C'est donc à partir de la seconde moitié du XIX\ieme \ siècle que naît la forme
actuelle de l'Algèbre qui consiste à axiomatiser les propriétés des opérations
apparaissant dans le traitement des équations et à étudier les structures
algébriques qui en résultent plus qu'à étudier les manières de résoudre les
dites équations.  
Voici ce qu'écrit Bourbaki dans son <<\'Elements d'histoire des mathématiques>>
: <<Nous arrivons ainsi à l'époque moderne, où la méthode axiomatique et la
notion de structure (sentie d'abord, définie à date récente seulement),
permettent de séparer des concepts qui jusque-là avaient été inextricablement
mêlés, de formuler ce qui était vague ou inconscient, de démontrer avec la
généralité qui leur est propre les théorèmes qui n'étaient connus que dans des
cas particuliers.>>
Trois grandes familles d'équations y ont  alors joué un rôle crucial. 

\begin{description}
\item[\sc Les équations linéaires] Elles sont du type 
$$\left\{\begin{array}{lll}
2x+y-z&=& 1\\
3x+2y+z&=& 4 \\
x+3y+z&=& 2\ .
\end{array}\right. $$
La théorie des espaces dans lesquels en elles s'expriment à donné 
naissance à la notion d'{\it espaces vectoriels}, dont l'axiomatisation a été
donnée principalement par Peano en 1888. 

\item[\sc Les équations diophantiennes] Elles sont type 
$$2^{x}-1=y \quad \text{ou}\quad  x^{5}+y^{5}=z^5 \ ,$$ 
avec pour solutions des nombres entiers. Leur étude abstraite a donné naissance
aux notions algébriques d'{\it anneaux}, d'{\it idéaux} et de {\it corps}, via
celle de nombre algébrique gr‰ce principalement à l'école allemande des
Dirichlet, Kummer, Kronecker, Dedekind, Hilbert, après bien sur les travaux de
Gauss. 

\item[\sc Les équations polynomiales] Elles sont du type 
$$8x^3-3x^2+x+7=0 \ .$$
L'étude de leurs solutions a donné naissance à la notion de {\it groupe}. Galois
est en assurément le principal instigateur mais ses travaux fulgurants mais
succincts ne  sont  publiés et  diffusés par Liouville et Serret que bien des
années après sa mort en 1832. L'émergence conceptuelle de cette notion doit
beaucoup au <<Traité des substitutions et des équations algébriques>> de Camille
Jordan (1870). 
\end{description}

Que se passe-t-il à chaque fois ? On reconnait dans différents exemples des
opérations, méthodes et résultats similaires. Il s'agit alors d'en extraire une
substantifique moelle : on fait ressortir les  propriétés communes essentielles
que l'on érige  en axiomes pour définir une nouvelle notion conceptuelle.
Prenons l'exemple de l'algèbre linéaire, c'est-à-dire des espaces vectoriels.
Comment travaille-t-on avec des objets apparemment si différents que sont 
\begin{itemize}
\item[$\diamond$] la droite, le plan, l'espace ambiant,
\item[$\diamond$] les matrices (tableaux de nombres), 
\item[$\diamond$] $\mathbb{R}^n$,
\item[$\diamond$] les polynômes, 
\item[$\diamond$] les applications ensemblistes réelles (à valeurs dans $\mathbb{R}$), 
\item[$\diamond$] les applications continues réelles,  
\item[$\diamond$] les applications $C^{\infty}$ réelles, 
\item[$\diamond$] les applications mesurable réelles, 
\item[$\diamond$] les ensembles de solution de systèmes d'équations linéaires homogènes, 
\item[$\diamond$] les ensembles de solution de systèmes d'équations différentielles homogènes, 
\item[$\diamond$] les suites numériques satisfaisant un relation de récurrence linéaire, 
\item[$\diamond$] les extensions de corps, 
\item[$\diamond$] etc. ?
\end{itemize}

On se rend compte que tous les calculs utilisent deux opérations et que ces
dernières vérifient toujours le même type de relations. 
La première opération est une  opération binaire consistant à {\it sommer} les
éléments : 
$$ E\times E \xrightarrow{+} E\ .$$ 
Cette addition vérifie à chaque fois les mêmes propriétés : associativité,
commutativité, présence d'un neutre ($0$). 
Cette structure est enrichie par la présence d'une action du corps de base : on
sait multiplier ces éléments par des nombres : 
$$ \mathbb{R}\times E \xrightarrow{\cdot} E\ .$$ 
Dans ce cas aussi, tous les exemples susmentionnés vérifient la même liste de
relations : associatitivé, distributivité, action du neutre, et action sur le
$0$. Facile alors de donner la définition abstraite et générale d'un espace
vectoriel. 

\subsection{Intérêts de l'axiomatisation }

On l'a tous bien senti, le cerveau commence à chauffer. Il y a en effet un prix
à payer pour arriver à concevoir cette axiomatisation, c'est celui de travailler
de plus en plus abstraitement. Se pose du coup avec acuité la question de
l'intérêt d'une telle démarche ; essayons d'en dégager quels bénéfices. 

\begin{enumerate}
\item Cette conceptualisation offre une prise de hauteur remarquable. Cela
permet de mettre sur un même pied 
différents objets qui sont au fond de même nature et cela donne des moyens de
les comparer efficacement à l'aide d'une bonne notion de morphisme.

\noindent 
{\sc Exemple : } En algèbre linéaire, on dispose d'une notion d'{\it application
linéaire} entre espaces vectoriels qui permet de les comparer facilement
(injectivité-noyau, surjectivité, dimension, etc.)


\item \'Etablir une telle  théorie générale permet de démontrer d'un seul coup
un  résultat qui sera valable automatiquement dans tous les exemples de la
théorie. Cela permet une simplification conceptuelle des énoncés. 

\noindent 
{\sc Exemple : } L'existence de bases et leur cardinal qui définit la notion de
dimension. 

\item L'approche abstraite permet de s'affranchir des contraintes imposées à
l'esprit par tel ou tel domaine. 

\noindent 
{\sc Exemple : } La géométrie peut nous faire penser que la dimension finie est
une hypothèse indispensable, il n'en est souvent rien. 

\item Extraire un type de structure algébrique permet de mettre au jour ce type
de structure sur de nouveaux objets et ainsi d'y appliquer les méthodes d'autres
domaines. 

\noindent 
{\sc Exemple : } Utiliser les méthodes vectoriels puissantes de dimension dans
le domaine des extensions de corps. 


\item Cette axiomatisation, une fois bien digérée, permet de voir dans quelle
direction poursuivre les recherches. 
On peut dire que cette sédimentation des idées amènent irrémédiablement à une
renaissance quelques (dizaines ?) années plus tard. 

\noindent 
{\sc Exemple : } La notion d'espace vectoriel ouvre ensuite les portes à celles
d'algèbres (associatives, commutatives, de Lie), d'espace tangent d'une variété,
d'espace vectoriel topologique, etc. 


\item Cette démarche met au jour un language universel dont d'autres matières
peuvent d'emparer avec intérêt. 

\noindent 
{\sc Exemple : } Les méthodes et le language de l'algèbre linéaire ont été
accaparé par de nombreux champs de la connaissance comme la mécanique, les
sciences naturelles ou les sciences sociales, par exemple. En économie, la
modélisation de l'état de l'économie à plusieurs facteurs comme celle d'un pays
à l'aide de vecteurs de $\mathbb{R}^n$ a permis à Leontief d'obtenir le <<prix
Nobel>> d'économie en 1973. En effet, si on considère que l'évolution d'une
telle économie évolue suivant des règles constantes et linéaires, on est ramené
à itérer un endomorphisme, dont la réduction permettra de faire efficacement de
la prospective. 

%\item Changement d'échelle et .... 
%
%on recommence. 
\end{enumerate} 
 
\subsection{L'Algèbre moderne}
Sous l'impulsion de l'école allemande des Dedekind, Hilbert, Steinitz, Artin,
Noether une unification conceptuelle des notions susmentionnées est entreprise
entre 1900 et 1930. Son point culminant est le livre de Van der Waerden, publié
en 1930 et dont le titre est bien sur <<Algèbre moderne>>, en français. 

\subsection{Bourbaki}
C'est en 1935 que naquit le groupe Bourbaki dont l'ambition n'est rien de moins
que d'offrir une 
présentation cohérente et exhaustive des mathématiques de son époque. Pour se
faire, il faut une bonne méthode. Il commence donc par un premier volume de
fondation avec la théorie des ensembles, puis continue avec l'Algèbre, etc.  Le
style est aussi détonnant pour l'époque\!; Bourbaki écrit ainsi en exergue de
chaque de ses traités : <<
Le mode d'exposition suivi est axiomatique et procède le plus souvent du général
au particulier>>, un peu  comme chez Al-Khwarizmi ! Bourbaki choisit donc de
décrire les mathématiques à travers les diverses structures qui les composent.
Cela fera dire à Emil Artin : <<Notre époque assiste à la création d'un ouvrage
monumental : un exposé de la totalité des mathématiques d'aujourd'hui. De plus,
cet exposé est fait de telle manière que les liens entre les diverses branches
des mathématiques deviennent clairement visibles>>. Car évidement, le fait de
faire ressortir les différentes structures présentes permet de faire des liens
entre les différents domaines. 

\section{Catégories}

\Bruno{Adam [10/9]}

\subsection{Introduction}
Imaginez-vous dans un vaste musée, où les chefs-d'œuvre de l'art mathématique prennent vie sous la lumière douce de la théorie et l'abstraction. Au cœur de cette galerie se trouve une salle mystérieuse, un sanctuaire intellectuel où les mathématiciens et mathématiciennes se réunissent pour explorer l'un des concepts les plus puissants et englobants qui soient : les catégories. Les catégories sont bien plus que de simples ensembles d'objets et de flèches que nous allons découvrir par la suite. Elles sont un concept central en mathématiques qui permet de structurer et d'organiser les objets mathématiques ainsi que les relations entre eux afin d’avoir une vue globale de ces dernières et de les étudier uniformément. Cette partie va nous aider à pénétrer dans ce monde abstrait et à découvrir ces fameuses catégories en s’appuyant sur des exemples pertinents et en soulignant sur les remarques, questions faites et réflexions qui en découlent.

\subsection{Définition}

Une catégorie $\mathcal{C}$ est composée des éléments suivants :
\begin{itemize}
    \item[\textbullet] Une collection d'objets: $\operatorname{Obj}(\mathcal{C})=\{\mathrm{X}, \mathrm{Y}, \mathrm{Z} \ldots\}$.
\item[\textbullet] Une collection de flèches: $\operatorname{Flech}(\mathcal{C})=\{\nearrow, \swarrow, \uparrow, \downarrow, \rightarrow, \ldots\}$
\item[\textbullet] Deux applications : «Source » $: \operatorname{Flech}(\mathcal{C}) \rightarrow \operatorname{Obj}(\mathcal{C})$.
«But » $: \operatorname{Flech}(\mathcal{C}) \rightarrow \operatorname{Obj}(\mathcal{C})$.
-\item[\textbullet] Une composition : pour toute paire de flèches:
$$
\mathrm{f:X} \rightarrow \mathrm{Y} \quad \mathrm{g}: \mathrm{Y} \rightarrow \mathrm{Z}
$$
\item[\textbullet] L'identité : pour tout objet c de $\mathcal{C}$ il existe idc : $\mathrm{C} \rightarrow \mathrm{C}$
\end{itemize}
Notons que la composition a comme propriété :

\hspace{50mm}L'associativité : $h\circ (g\circ f) = (h\circ g)\circ f$.

\Bruno{unité !}


\subsection{Exemples}
\subsubsection{Catégories concrètes}
\begin{itemize}
    \item[\textbullet]	Catégorie des ensembles : 
    \begin{itemize}
        \item[\ding{118}] $\text{Obj}_{ens}$ $=X, Y$. des ensembles
        
\item[\ding{118}] $\text{Flech}_{ens}$ $=x \stackrel{f }\longmapsto=$ y: application /entre ensembles / injective/ surjective
\item[\ding{118}] Source $=f\longmapsto x$ surjective...
\item[\ding{118}] But $=f \longmapsto y$
\item[\ding{118}] "O" = composition normale des application
\end{itemize}
\textbf{Remarque:}

Il n'existe pas un ensemble des ensembles, c'est pourquoi on utilisera plutôt le terme collection.

    \item[\textbullet] Catégorie des espaces vectoriels:
    \begin{itemize}
        \item[\ding{118}]	$\text{Obj}_{Vect}$ : Les objets dans cette catégorie sont les espaces vectoriels sur un champ donné (par exemple, le corps des nombres réels R ou le corps des nombres complexes C).
        
\item[\ding{118}]	$\text{Flech}_{Vect}$ : Les flèches dans cette catégorie sont les transformations linéaires entre les espaces vectoriels. Chaque flèche relie deux espaces vectoriels et représente une transformation linéaire d'un espace vectoriel dans un autre.

\item[\ding{118}]	Applications "Source" et "But" : Les applications "Source" et "But" associent à chaque flèche sa source et son but dans la catégorie des espaces vectoriels. La source d'une flèche est l'espace vectoriel de départ, et le but est l'espace vectoriel d'arrivée.

Source(f) : E1 (l'espace vectoriel de départ de la flèche f)

But(f) : E2 (l'espace vectoriel d'arrivée de la flèche f)
\item[\ding{118}]	La composition est une opération définie sur les flèches de la catégorie. Pour composer deux flèches f : E1 → E2 et g : E2 → E3, il doit être vérifié que l'espace vectoriel E2 est à la fois la source de f et le but de g (But(f) = Source(g)).
    \end{itemize}
    \item[\textbullet] Catégories des corps :  
    \begin{itemize}
        \item[\ding{118}] 
        $\text{Obj}_{Cor}$ : les ensembles munis d'opérations d'addition et de multiplication qui forment un corps ($\mathbb{Z}$/n$\mathbb{Z}, \mathbb{Q}$ …)
\item[\ding{118}]	$\text{Flech}_{Cor}$  : les homomorphismes de corps, inclusion ensembliste, application ensembliste

\item[\ding{118}]	Source / But : corps de départ et corps d’arrivé.

\item[\ding{118}]	« o » : composition des flèches

    \end{itemize}
\end{itemize}

\textbf{Citons aussi les catégories des anneaux, des groupes etc.}

\subsubsection{Catégories non-concrètes}

\begin{itemize}

    \item[\textbullet] Catégories des Quaternions : 
    \begin{itemize}
        \item[\ding{118}] 	$\text{Obj}_{Quat}$ : « . » nous manipulons des quaternions sous la forme $\alpha 1$+$\beta \mathrm{i}$+$\gamma \mathrm{j}$+ $\delta \mathrm{k}$, où , $\alpha,\beta,\gamma,$ \text{et}  $\delta$
           sont des nombres réels. 
        
\item[\ding{118}] $\text{Flech}_{Quat}$ : Les flèches dans cette catégorie sont les éléments $1, \mathrm{i}, \mathrm{j}$ et $\mathrm{k}$, qui sont les unités imaginaires des quaternions. Chacun de ces éléments est considéré comme une flèche distincte. $\mathrm{H}=\{\alpha 1+\mathrm{i} \beta+\mathrm{j} \gamma+\mathrm{k} \delta, \alpha, \beta, \gamma, \delta \in \mathbb{R}\}$

\item[\ding{118}] « o » : Pour deux flèches a et $\mathrm{b}$, la composition $\mathrm{a} \circ \mathrm{b}$ est la multiplication de quaternions correspondante :
$$
\begin{aligned}
& \mathrm{i}^2=\mathrm{j}^2=\mathrm{k}^2=\mathrm{ijk}=-1 \\
& \mathrm{ij}=-\mathrm{ji}=\mathrm{k}, \mathrm{jk}=-\mathrm{kj}=\mathrm{i}, \mathrm{ki}=-\mathrm{ik}=\mathrm{j} \ldots
\end{aligned}
$$ 
    \end{itemize}
    \item[\textbullet] Catégorie  Cnc: 
    \begin{itemize}
        \item[\textbullet] $\text{Obj}_{enc}$ = \quad \textbullet
        \item[\textbullet] $\text{Flech}_{enc}$\quad=\quad\{-3,  -2,  ....  1..\}
        \item[\textbullet] 2 composition \qquad $\longrightarrow$ + \qquad($\text{id}_.$  =  0)

        \qquad \qquad \qquad \qquad 
        \hspace{0.3mm} $\longrightarrow$ $\times$ \qquad($\text{id}_.$  =  1)
    \end{itemize}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.2\textwidth]{sym1.png}
\end{figure}
\end{itemize}
\subsection{Contre-exemple}

Et quoi pour bien assimiler la notion de catégorie que de trouver un contre-exemple concret.
\vspace{3mm}

$\text{Obj}_{enc}$: \qquad \qquad 
 X , Y , Z , W
\vspace{3mm}

\begin{minipage}{0\textwidth}
 $\text{Flech}_{enc}$:   
\end{minipage}
\hspace{1mm}
\begin{minipage}{0.7\textwidth}
    
    \centering
\includegraphics[width=0.5\textwidth]{Sym.png}

\end{minipage}


\hspace{92mm}$(g\circ f): = \phi$

\hspace{92mm} $(h\mathrm{o}g):  = \psi$ 

Composition: on voit clairement que  $\alpha \ne \beta$  \hspace{21mm}$(h\mathrm{o}\phi): = \alpha$

\hspace{92mm}$(\psi\mathrm{o}f):=\beta$

\Bruno{Lyes [17/9]}
\setcounter{subsection}{1}
\subsection{Définitions}

On peut aussi donner une autre définition des catégories en regroupant chaque
flèche, sa source et son but en un objet qu'on appellera homomorphisme.

En effet, soit $f$ une flèche, et soit $A$ sa source et $B$ son but. On dira que 
$f$ appartient à la collection des homomorphismes de $A$ dans $B$, qu'on notera
$\operatorname{Hom}_\mathcal{C}(A, B) = \operatorname{Mor}_\mathcal{C}(A, B) = \mathcal{C}(A, B)$.

On donne donc cette nouvelle définition :

\begin{defi}
Une catégorie $\mathcal{C}$ est composée des éléments suivants :
\begin{itemize}
    \item Une collection d'objets : $\operatorname{Obj}(\mathcal{C}) = X, Y, Z, ...$.
    \item Une collection de flèches :
        Pour toute paire d'objets $A$ et $B$, on a :
        $\operatorname{Hom}_\mathcal{C}(A, B) = f, g, h, ...$.
    \item Une composition :
        Pour toute paire de flèches $f$ de $\operatorname{Hom}_\mathcal{C}(A, B)$
        et $g$ de $\operatorname{Hom}_\mathcal{C}(B, C)$, on a $f \circ g$
        dans $\operatorname{Hom}_\mathcal{C}(A, C)$.
    \item L'identité :
        Pour tout objet $C$ de $\operatorname{Obj}(\mathcal{C})$, il existe un
        homomorphisme de $\operatorname{Hom}_\mathcal{C}(C, C)$ noté $id_C$.
\end{itemize}
\end{defi}

\setcounter{subsection}{4}

\subsection{Petites catégories}
\subsubsection{Définitions}

Les catégories utilisent dans nos définitions des collections d'objets, et non
des ensembles. Cela nous donne plus de flexibilité, et nous évite certaines
restrictions vis à vis des ensembles. Néanmoins, il peut arriver de vouloir
travailler sur des ensembles. Nous définissons pour cela les catégories petites
et localement petites.

\textcolor{red}{(pas certain de la section qui suit en général)}

\begin{defi}
Une catégorie est dite localement petite si ses flèches forment un ensemble.
\end{defi}

\begin{defi}
Une catégorie est dite petite si ses objets forment un ensemble.
\end{defi}

\begin{theo}
Si une catégorie est petite, alors, elle est aussi localement petite. 

\textcolor{red}{(à prouver ? mais difficile sans une bonne compréhension de la différence entre
ensemble et collection)}
\end{theo}

\subsubsection{Exemples}

\begin{exem}
La catégorie des ensembles finis $\mathcal{E}ns_{fini}$ est une petite catégorie :
\begin{itemize}
    \item $\operatorname{Obj} = \{ \text{ensembles finis} \}$
    \item $\operatorname{Hom} =$ \begin{itemize}
                                     \item[\textbullet] applications ensemblistes
                                     \item[\textbullet] injections
                                     \item[\textbullet] surjections
                                 \end{itemize}
\end{itemize}
\end{exem}

En effet, l'ensemble des ensembles finis est défini. De la même façon, la
catégorie des corps finis, des espaces vectoriels de dimension finie et toute
catégorie dont les objets sont finis sont des petites catégories.

\begin{exem}
La catégorie composée d'un seul objet est une petite catégorie :
\begin{itemize}
    \item $\operatorname{Obj} = \{ \cdot \}$
    \item $\operatorname{Hom} = \{ id_\cdot : \cdot \rightarrow \cdot \}$
\end{itemize}

\textcolor{red}{(insérer ici un graphe d'un point qui revient vers lui-même)}
\end{exem}

\subsubsection{Contre-exemples}

La catégorie des ensembles $\mathcal{E}ns$ n'est pas une petite catégorie. En
effet, l'ensemble des ensembles n'existe pas (cf. Paradoxe de Russel).
\textcolor{red}{(pas sûr d'avoir eu une bonne lecture du tableau sur ce qui suit)}
Néanmoins, les $\operatorname{Hom}_{\mathcal{E}ns}(X, Y)$ forment des ensembles.
C'est donc une catégorie localement petite.

La catégorie des espaces vectoriels et des corps ne forment pas non plus de
petites catégories, parce qu'il n'existent pas d'ensembles de tous les espaces
vectoriels, ni d'ensemble de tous les corps, on doit donc se limiter aux
collections.

\subsection{Catégories associées à des ensembles partiellement ordonnés}
\subsubsection{Ensembles partiellement ordonnés}

Les ensembles partiellement ordonnés sont des ensembles sur lesquelles on
défini une relation d'ordre.

\begin{remas} Rappel sur les ensembles partiellement ordonnés.

Soit $E$ un ensemble muni d'une relation $\le$ sur $E$.
Une relation d'ordre est une relation qui vérifie les propriétées suivantes :
\begin{itemize}
    \item \textbf{La réflexivité} : $\forall x \in E,\ x \leq x$
    \item \textbf{La transitivité} : $\forall x, y, z \in E,\ x \leq y \wedge y \leq z \Leftrightarrow x \leq z$
    \item \textbf{L'antisymétrie} : $\forall x, y \in E,\ x \leq y \wedge y \leq x \Leftrightarrow x = y$
\end{itemize}
\end{remas}

\begin{exem}
Soit $X = {1, 2, 3}$ un ensemble.

On peut définir l'ensemble partiellement ordonné $(E, \leq)$, avec
$E = \mathcal{P}(E)$ et $\leq$ l'inclusion.

On peut représenter un tel ensemble avec un diagramme, le diagramme de Hasse,
qui met en évidence l'ordre établi dans l'ensemble.

\textcolor{red}{(insérer ici un diagramme de Hasse)}

\end{exem}

On peut créer une catégorie pour tout ensemble partiellement ordonné.

\begin{theo}
    Soit $\pi = (E, \leq)$ un ensemble partiellement ordonné. Il existe une catégorie
    associée à cet ensemble partiellement ordonné qu'on note $\mathcal{C}_\pi$
    avec :
    \begin{itemize}
        \item $\operatorname{Obj} = E$
        \item $
            \operatorname{Hom}(e, e') =
            \begin{cases}
            	e \rightarrow e'\ \text{si } e \leq e'\\
                \emptyset\ \text{sinon}
            \end{cases}
        $
        \item Les compositions sont vérifiées par la transitivité.
        \item L'identité est vérifié par la réflexivité.
    \end{itemize}
\end{theo}

\subsubsection{Graphes}

Les graphes sont des outils centraux en Informatique. Et l'on remarque une
proximité entre eux et les Catégories. Après tout, on représente souvent des
catégories au moyen de graphes. Et, nous pouvons, grâce aux catégories
partiellement ordonnés créer des catégories de graphes.

\begin{remas} Rappels sur les définitions des graphes.
\textcolor{red}{(insérer pour chaque définition un graphe)}
\begin{itemize}
    \item Un graphe $G$ est une paire de sommets $S$ et d'arêtes $A$.
        $G = (S,A)$.
    \item Un graphe $G$ est dit simple s'il n'y a au plus qu'une seule arête
        reliant deux sommets. 
    \item Un graphe $G$ est dit dirigé si chaque arête a un sens.
    \item Un graphe simple $G$ est dit complet s'il n'est pas possible d'ajouter
        de nouvelles arêtes.
\end{itemize}

On ne parlera que de graphes simples dirigés. 
\end{remas}

\subsubsection{Constructions}

\begin{defi}
    Un graphe simple dirigé $G$ est transitif si et seulement si
    $\forall x, y, z \in S$ tel que $\exists (x, y), (y, z) \in A$, on a
    $(x, z) \in A$.
\end{defi}

\begin{defi}
    Un graphe simple dirigé $G$ est réflexif si et seulement si
    $\forall x \in S$, on a $(x, x) \in A$.
\end{defi}

\begin{theo}
    Si un graphe est transitif et réflexif, alors on peut construire une
    catégorie représentant ce graphe :
    \begin{itemize}
        \item $\operatorname{Obj} = E$
        \item $
            \operatorname{Hom}(e, e') =
            \begin{cases}
            	e \rightarrow e'\ \text{si } e \leq e'\\
                \emptyset\ \text{sinon}
            \end{cases}
        $
        \item Les compositions sont vérifiées par la transitivité du graphe.
        \item L'identité est vérifié par la réflexivité du graphe.
    \end{itemize}
\end{theo}

\begin{rema}
    On a sait donc désormais qu'à partir d'un ensemble partiellement ordonné on
    peut construire un graphe avec le diagramme de Hasse. Et qu'à partir d'un
    graphe transitif et réflexif (ce qui est le cas du diagramme de Hasse),
    on peut construire une catégorie.

    \[
        \text{Ensembles partiellement ordonnés}
            \rightsquigarrow
        \text{Graphes transitifs réflexifs}
            \rightsquigarrow
        \text{Catégories}
    \]
\end{rema}

\Bruno{Corentin [17/9]}

\subsection{Constructions de catégories} Maintenant que nous avons vu comment définir proprement ce qu'est une catégorie, nous pouvons maintenant chercher comment construire des nouvelles catégories à partir de catégories existantes.

\subsubsection{Union disjointe de catégories}

\begin{defi}
    \textbf{Union disjointe}

    Soient $C$ et $D$ deux catégories. On appelle union disjointe de $C$ et $D$ (noté $C \sqcup D$) la catégorie formée telle que :
    \begin{itemize}
        \item $Obj(C \sqcup D) :=$ La collection formée des éléments de $Obj(C) \text{ et } Obj(D)$ telle que les éléments de $Obj(C) \text{ et } Obj(D)$ sont disjoints.
        \item $Hom_{C \sqcup D}(e,f) :=
            \begin{cases}
                Hom_C(e,f)\text{ si } e \text{ et } f \text{ sont dans } Obj(C)\\
                Hom_D(e,f)\text{ si } e \text{ et } f \text{ sont dans } Obj(D)\\
                \emptyset \text{ sinon}
            \end{cases}$
        \item Les compositions et les identités de $C \sqcup D$ sont celles de $C$ et de $D$.
    \end{itemize}
    \textcolor{red}{(insérer un exemple (forme graphe) classique)}
\end{defi}

\begin{rema}
   \textbf{Attention :} Si on appelle $e$ un élément de $Obj(C)$ et $e$ un élément de $Obj(D)$, ils seront considérés différents dans $Obj(C \sqcup D)$. On pourra les noter $e_C$ et $e_D$ dans $Obj(C \sqcup D)$.
\end{rema}
\begin{theo}
   La construction de l'union disjointe est \emph{commutative}, c'est à dire que l'on a $C \sqcup D = D \sqcup C$.
\end{theo}

\subsubsection{"Joint" de catégories (union jointe)}

\begin{defi}
    \textbf{Joint}

    Soient $C$ et $D$ deux catégories. On appelle joint entre $C$ et $D$ (noté $C \rhd D$) la catégorie formée telle que :
    \begin{itemize}
        \item $Obj(C \rhd D) := Obj(C \sqcup D)$ 
        \item $Hom_{C \rhd D}(e,f) :=
            \begin{cases}
                Hom_{C \rhd D}(e,f) = \{e \rightarrow e'\} \text{ si } e \text{ est dans } Obj(C) \text{ et } f \text{ est dans } Obj(D)\\
                \text{sinon } Hom_{C \rhd D}(e,f) = Hom_{C \sqcup D}(e,f)
            \end{cases}$
        \item Les identités de $C \rhd D$ sont celles de $C$ et celles de $D$
        \item Les compositions de $C \rhd D$ sont les compositions de $C$ et de $D$. Soient $i,j$ dans $Obj(C)$ et $k,l$ dans $Obj(D)$. Alors la composition $i \rightarrow j \circ j \rightarrow k$ produit le morphisme $i \rightarrow k$ de $Hom_{C\rhd D}(i,k)$. De même, la composition $i \rightarrow k \circ k \rightarrow l$ produit le morphisme $i \rightarrow l$ de $Hom_{C\rhd D}(i,l)$.

    \end{itemize}
\end{defi}

\begin{remas} Joints

    \begin{itemize}
        \item On considère donc $i \rightarrow j \circ j \rightarrow l = i \rightarrow k \circ k \rightarrow l$.
        \item La construction du joint entre $C$ et $D$ n'est plus \emph{commutative}.
    \end{itemize}
\end{remas}

\begin{exem}
Soit $\mathcal{C}at_n :=$ la catégorie formée à partir de $(\textlbrackdbl n\textrbrackdbl ,\leq)_{n \in \mathbb{N}^*}$
On pose $\mathcal{C} = \mathcal{C}at_n$ et $\mathcal{D} = \mathcal{C}at_m$, $(n,m)\in {\mathbb{N}^{*}}^2$.

On écrit $C : 1 \rightarrow 2 \rightarrow \ldots \rightarrow n$, et de même $D : 1 \rightarrow 2 \rightarrow \ldots \rightarrow m$

(La transitivité pour la composition et la réflexivité pour l'identité sont assurés par le fait que $\leq$ est une relation d'ordre, les morphismes associés sont dont sous-entendus pour une meilleure visibilité)

On souhaiterait que le joint entre $C$ et $D$ vérifie $C \rhd D = \mathcal{C}at_{n+m}$.

Or, on a $C \rhd D : 1_C \rightarrow 2_C \rightarrow \ldots \rightarrow n \ \textcolor{teal}{\rightarrow}\  1_D \rightarrow 2_D \rightarrow \ldots m$.
La composition de $C \rhd D$ (et le fait que chaque élément de $Obj(C)$ a un morphisme vers chaque élément de $Obj(D)$) nous permet de compléter "l'ordre" entre $C$ et $D$, en notant :

$1_D = n+1$, $2_D = n+2$, \ldots, $m = n+m$

on retrouve bien la catégorie $\mathcal{C}at_{n+m}$.

\end{exem}

\subsubsection{Produit de catégories}

\begin{defi}
    \textbf{Produit}

    Soient $C$ et $D$ deux catégories. On appelle produit de $C$ et $D$ (noté $C \times D$) la catégorie formée des paires d'objets de $C$ et $D$ telle que :
    \begin{itemize}
        \item $Obj(C \times D) :=$ La collection formée des paires $(e,f)$ telles que $e$ est dans $Obj(C)$ et $f$ est dans $Obj(D)$.
        \item $(e,f) \rightarrow (e',f')$ est dans $Hom_{C \times D}((e,f),(e',f'))$ si et seulement si $e \rightarrow e'$ est dans $Hom_C(e,e')$ et $f \rightarrow f'$ est dans $Hom_D(f,f')$.
        \item Les compositions de $C \times D$ sont les couples formés en premier élément des compositions de $C$ et en deuxième élément des compositions de $D$.
        \item Les idendités de $C \times D$ sont les couples formés des identités de $C$ et des identités de $D$.
    \end{itemize}
    \textcolor{red}{(insérer un exemple ?)}
\end{defi}

\subsubsection{Opposé de catégorie}

\begin{defi}
    Soit $C$ une catégorie, et $f : i \rightarrow j$ un morphisme de $C$. On définit $f^{op} : j \rightarrow i$ comme étant son morphisme opposé.
\end{defi}

\begin{rema}
    Soit $f : i \rightarrow j$ et $g : j \rightarrow k$ tels que $f \circ g$ (sa composition) est aussi un morphisme de $C$. On a la composition opposée $(f \circ g)^{op} := f^{op}\circ g^{op}$.
\end{rema}

\begin{defi}
    \textbf{Opposé}
    Soit $C$ une catégorie. On note $C^{op}$ la catégorie opposée formée telle que :
    \begin{itemize}
        \item $Obj(C^{op}) := Obj(C)$
        \item Un morphisme $e \rightarrow f$ est dans $Hom_{C^{op}}(e,f)$ si et seulement si $f \rightarrow e$ est dans $Hom_C(f,e)$.
        \item Les identités de $C^{op}$ sont les mêmes que celles de $C$.
        \item Par la remarque précédente, on définit les compositions de $C^{op}$ comme les opposés des compositions de $C$.
    \end{itemize}
\end{defi}

\begin{exem} Opposé de $\mathcal{C}at_n$

    $\mathcal{C}at_n = 1 \rightarrow 2 \rightarrow 3 \rightarrow \ldots \rightarrow n$
    
    $\mathcal{C}at_n^{op} = n \rightarrow \ldots \rightarrow 3 \rightarrow 2 \rightarrow 1$
\end{exem}

\subsection{Sous-catégories}

\subsubsection{Définition}

\begin{defi} Sous-catégorie. Soit $C$ une catégorie. $S$ est une sous-catégorie de $C$ (noté $S \subset C$) si et seulement si :
    \begin{itemize}
        \item Chaque élément de $Obj(S)$ est aussi dans $Obj(C)$.
        \item Pour chaque couple d'éléments $(e,f)$ dans $Obj(S)$, Chaque morphisme de $Hom_S(e,f)$ est également dans $Hom_C(e,f)$.
        \item Soient $i, j, k$ dans $Obj(S)$. S'il existe un morphisme $f : i \rightarrow j$ dans $Hom_S(i,j)$ et un morphisme $g : j \rightarrow k$ dans $Hom_S(j,k)$ alors la composition $f \circ g$ est la composition $f \circ g$ de $C$ produite par $Hom_C(i,j)$ et $Hom_C(j,k)$.
        \item Les identités de $S$ sont celles de $C$ pour les éléments de $Obj(S)$.
    \end{itemize}
\end{defi}

\begin{exem}
    Soit $C$ une catégorie telle que :
    $Obj(C) : a,b,c,d,e$ et $Hom_C :$ \textcolor{red}{inserer graphe exemple pour lisibilité}.
    On pose $S$ une catégorie telle que :
    $Obj(S) : a,b,d$ où $a_S = a_C$, $b_S = b_C$ et $d_S = d_C$ (contrairement à l'union disjointe de catégorie on considère ici les éléments nommés pareillement comme étant identiques) et $Hom_S :$ \textcolor{red}{inserer graphe exemple pour lisibilité}.
    
    Alors $S$ est une sous-catégorie de $C$, et on note $S \subset C$.
\end{exem}

\Bruno{Corentin [21/09]}

\begin{theo}
    Soient $C$ une catégorie et $S \subset C$, alors $S^{op} \subset C^{op}$.
    
    \emph{Démonstration} à venir.
\end{theo}

\subsubsection{Intersection de sous-catégories}

\begin{defi}
    Soit $C$ une catégorie. Soient $S,S'$ telles 2 sous-catégories de $C$. ($S \subset C$ et $S' \subset C$).
    On appelle intersection de $S$ et $S'$ (noté $S\cap S'$) est la sous-catégorie de 
\end{defi}

\textcolor{red}{(finir bonus de la séance du 21 sur les sous-catégories, finir exemple de sous-catégorie, et écrire la preuve de mon théorème sur les opposés)}
\end{document}
