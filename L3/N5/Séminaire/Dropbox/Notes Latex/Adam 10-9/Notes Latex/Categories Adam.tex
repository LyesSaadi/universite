% LaTeX : Séminaire sur les Catégories (DL3 2023)
% Auteur-trices : Hugo Paquet, Bruno Vallette
% Encodage : Mac OS Roman

\documentclass[twoside, 11pt]{amsart}
\usepackage[utf8]{inputenc}
\include{package}

%%%%%%%%%%%%%%%%%%%%   Titre et auteur %%%%%%%%%%%%%%%%%%%%

\title{Les cat\'egories}

\dedicatory{``Mal nommer les choses contribue au malheur du monde''' \\  \hfill \textit{Albert Camus}}

\author{Bruno Vallette}
\address{Laboratoire Analyse, G\'eom\'etrie et Applications, Universit\'e Paris 13, Sorbonne Paris Cit\'e, CNRS, UMR 7539, 93430 Villetaneuse, France.}
\email{vallette@math.univ-paris13.fr}

\date{\today}

\begin{document}

\maketitle

\begin{abstract} Notes du séminaire sur les catégories de la classe de Double Licence 3 Mathématiques-Informatique de l'Université Sorbonne Paris Nord 2023-2024.
 \end{abstract}

\setcounter{tocdepth}{2}

\tableofcontents

\section{Introduction : l'algèbre moderne}

\subsection{L'Algèbre ou l'analyse des quations}
Le mot français  <<algèbre>> provient du mot arabe <<al-jabr>> qui apparait dans le titre du livre  <<Abrégé du calcul par la restauration et la comparaison>> publié par en 825 par Al-Khwarizmi, mathématicien perse. 
Cet ouvrage est le premier à étudier systématiquement la résolution des équations du premier et du second degré. Il est intéressant de  noter qu'il se compose de deux parties : la première contient la théorie abstraite des équations algébriques où l'accent est mis sur le type d'opérations utilisées, la seconde contient les diverses applications en vue à l'époque comme les calculs d'héritage, d'arpentage ou de commerce. Le mot arabe <<al-jabr>>, qui signifie <<la réduction>> au sens de <<la réduction d'une fracture>>, est la terminologie choisie par Al-Khwarizmi pour une des opérations sur les équations, celle  qui consiste à réduire une équation en ajoutant des termes, soit deux termes de même nature d'une même côté d'une équation comme 
$$x^2+3x^2= 5x+3 \ \Longleftrightarrow \ 4x^2= 5x+3 \ ,$$
soit deux termes identiques de part et d'autres de l'équation comme 
$$3x^2+3=-5x \ \Longleftrightarrow \ 3x^2+5x+3=0 \ .$$
(Il est amusant de noter qu'en espagnol le terme dérivé <<algebrista>> signifie à la fois un algébriste ou rebouteux, celui qui réduit les fractures.)\\

Disons rapidement que jusqu'au XIX\ieme, les mathématiciens font des calculs, parfois du même type sur des objets différents. Même s'ils savent utiliser des variables à la place de nombres, s'ils se servent d'une notation pour le $0$ ou qu'ils peuvent considérer des nombres imaginaires, peu de place est alors accordée à  la théorie de ses calculs. 
La première moitié du XIX\ieme \ siècle voit une renaissance de l'Algèbre par l'introduction de nouveaux concepts, méthodes et objets pour la résolution des équations algébriques. D'ailleurs, Serret en 1866 écrit en introduction de son \emph{Cours d'algèbre supérieure} (sic) que <<L'Algèbre [est], à proprement parlé, l'Analyse des équations>>. \\


\subsection{L'algèbre ou l'axiomatisation des structures algébriques}
C'est donc à partir de la seconde moitié du XIX\ieme \ siècle que naît la forme actuelle de l'Algèbre qui consiste à axiomatiser les propriétés des opérations apparaissant dans le traitement des équations et à étudier les structures algébriques qui en résultent plus qu'à étudier les manières de résoudre les dites équations.  
Voici ce qu'écrit Bourbaki dans son <<\'Elements d'histoire des mathématiques>> : <<Nous arrivons ainsi à l'époque moderne, où la méthode axiomatique et la notion de structure (sentie d'abord, définie à date récente seulement), permettent de séparer des concepts qui jusque-là avaient été inextricablement mêlés, de formuler ce qui était vague ou inconscient, de démontrer avec la généralité qui leur est propre les théorèmes qui n'étaient connus que dans des cas particuliers.>>
Trois grandes familles d'équations y ont  alors joué un rôle crucial. 

\begin{description}
\item[\sc Les équations linéaires] Elles sont du type 
$$\left\{\begin{array}{lll}
2x+y-z&=& 1\\
3x+2y+z&=& 4 \\
x+3y+z&=& 2\ .
\end{array}\right. $$
La théorie des espaces dans lesquels en elles s'expriment à donné 
naissance à la notion d'{\it espaces vectoriels}, dont l'axiomatisation a été donnée principalement par Peano en 1888. 

\item[\sc Les équations diophantiennes] Elles sont type 
$$2^{x}-1=y \quad \text{ou}\quad  x^{5}+y^{5}=z^5 \ ,$$ 
avec pour solutions des nombres entiers. Leur étude abstraite a donné naissance aux notions algébriques d'{\it anneaux}, d'{\it idéaux} et de {\it corps}, via celle de nombre algébrique grâce principalement à l'école allemande des Dirichlet, Kummer, Kronecker, Dedekind, Hilbert, après bien sur les travaux de Gauss. 

\item[\sc Les équations polynomiales] Elles sont du type 
$$8x^3-3x^2+x+7=0 \ .$$
L'étude de leurs solutions a donné naissance à la notion de {\it groupe}. Galois est en assurément le principal instigateur mais ses travaux fulgurants mais succincts ne  sont  publiés et  diffusés par Liouville et Serret que bien des années après sa mort en 1832. L'émergence conceptuelle de cette notion doit beaucoup au <<Traité des substitutions et des équations algébriques>> de Camille Jordan (1870). 
\end{description}

Que se passe-t-il à chaque fois ? On reconnait dans différents exemples des opérations, méthodes et résultats similaires. Il s'agit alors d'en extraire une substantifique moelle : on fait ressortir les  propriétés communes essentielles que l'on érige  en axiomes pour définir une nouvelle notion conceptuelle. Prenons l'exemple de l'algèbre linéaire, c'est-à-dire des espaces vectoriels. Comment travaille-t-on avec des objets apparemment si différents que sont 
\begin{itemize}
\item[$\diamond$] la droite, le plan, l'espace ambiant,
\item[$\diamond$] les matrices (tableaux de nombres), 
\item[$\diamond$] $\mathbb{R}^n$,
\item[$\diamond$] les polynômes, 
\item[$\diamond$] les applications ensemblistes réelles (à valeurs dans $\mathbb{R}$), 
\item[$\diamond$] les applications continues réelles,  
\item[$\diamond$] les applications $C^{\infty}$ réelles, 
\item[$\diamond$] les applications mesurable réelles, 
\item[$\diamond$] les ensembles de solution de systèmes d'équations linéaires homogènes,
\item[$\diamond$] les ensembles de solution de systèmes d'équations différentielles homogènes, 
\item[$\diamond$] les suites numériques satisfaisant un relation de récurrence linéaire, 
\item[$\diamond$] les extensions de corps, 
\item[$\diamond$] etc. ?
\end{itemize}

On se rend compte que tous les calculs utilisent deux opérations et que ces dernières vérifient toujours le même type de relations. 
La première operation est une  opération binaire consistant à {\it sommer} les éléments : 
$$ E\times E \xrightarrow{+} E\ .$$ 
Cette addition vérifie à chaque fois les mêmes propriétés : associativité, commutativité, présence d'un neutre ($0$). 
Cette structure est enrichie par la présence d'une action du corps de base : on sait multiplier ces éléments par des nombres : 
$$ \mathbb{R}\times E \xrightarrow{\cdot} E\ .$$ 
Dans ce cas aussi, tous les exemples susmentionnés vérifient la même liste de relations : associatitivé, distributivité, action du neutre, et action sur le $0$. Facile alors de donner la définition abstraite et générale d'un espace vectoriel. 

\subsection{Intérêts de l'axiomatisation }

On l'a tous bien senti, le cerveau commence à chauffer. Il y a en effet un prix à payer pour arriver à concevoir cette axiomatisation, c'est celui de travailler de plus en plus abstraitement. Se pose du coup avec acuité la question de l'intérêt d'une telle démarche ; essayons d'en dégager quels bénéfices. 

\begin{enumerate}
\item Cette conceptualisation offre une prise de hauteur remarquable. Cela permet de mettre sur un même pied 
différents objets qui sont au fond de même nature et cela donne des moyens de les comparer efficacement à l'aide d'une bonne notion de morphisme.

\noindent 
{\sc Exemple : } En algèbre linéaire, on dispose d'une notion d'{\it application linéaire} entre espaces vectoriels qui permet de les comparer facilement (injectivité-noyau, surjectivité, dimension, etc.)


\item \'Etablir une telle  théorie générale permet de démontrer d'un seul coup un  résultat qui sera valable automatiquement dans tous les exemples de la théorie. Cela permet une simplification conceptuelle des énoncés. 

\noindent 
{\sc Exemple : } L'existence de bases et leur cardinal qui définit la notion de dimension. 

\item L'approche abstraite permet de s'affranchir des contraintes imposées à l'esprit par tel ou tel domaine. 

\noindent 
{\sc Exemple : } La géométrie peut nous faire penser que la dimension finie est une hypothèse indispensable, il n'en est souvent rien. 

\item Extraire un type de structure algébrique permet de mettre au jour ce type de structure sur de nouveaux objets et ainsi d'y appliquer les méthodes d'autres domaines. 

\noindent 
{\sc Exemple : } Utiliser les méthodes vectoriels puissantes de dimension dans le domaine des extensions de corps. 


\item Cette axiomatisation, une fois bien digérée, permet de voir dans quelle direction poursuivre les recherches. 
On peut dire que cette sédimentation des idées amènent irrémédiablement à une renaissance quelques (dizaines ?) années plus tard. 

\noindent 
{\sc Exemple : } La notion d'espace vectoriel ouvre ensuite les portes à celles d'algèbres (associatives, commutatives, de Lie), d'espace tangent d'une variété, d'espace vectoriel topologique, etc. 


\item Cette démarche met au jour un language universel dont d'autres matières peuvent d'emparer avec intérêt. 

\noindent 
{\sc Exemple : } Les méthodes et le language de l'algèbre linéaire ont été accaparé par de nombreux champs de la connaissance comme la mécanique, les sciences naturelles ou les sciences sociales, par exemple. En économie, la modélisation de l'état de l'économie  plusieurs facteurs comme celle d'un pays à l'aide de vecteurs de $\mathbb{R}^n$ a permis à Leontief d'obtenir le <<prix Nobel>> d'économie en 1973. En effet, si on considère que l'évolution d'une telle économie évolue suivant des règles constantes et linéaires, on est ramené à itérer un endomorphisme, dont la réduction permettra de faire efficacement de la prospective. 

%\item Changement d'échelle et .... 
%
%on recommence. 
\end{enumerate} 
 
\subsection{L'Algèbre moderne}
Sous l'impulsion de l'école allemande des Dedekind, Hilbert, Steinitz, Artin, Noether une unification conceptuelle des notions susmentionnées est entreprise entre 1900 et 1930. Son point culminant est le livre de Van der Waerden, publié en 1930 et dont le titre est bien sur <<Algèbre moderne>>, en français. 

\subsection{Bourbaki}
C'est en 1935 que naquit le groupe Bourbaki dont l'ambition n'est rien de moins que d'offrir une 
présentation cohérente et exhaustive des mathématiques de son époque. Pour se faire, il faut une bonne méthode. Il commence donc par un premier volume de fondation avec la théorie des ensembles, puis continue avec l'Algèbre, etc.  Le style est aussi détonnant pour l'époque\!; Bourbaki écrit ainsi en exergue de chaque de ses traités : <<
Le mode d'exposition suivi est axiomatique et procède le plus souvent du général au particulier>>, un peu  comme chez Al-Khwarizmi ! Bourbaki choisit donc de décrire les mathématiques à travers les diverses structures qui les composent. Cela fera dire à Emil Artin : <<Notre époque assiste à la création d'un ouvrage monumental : un exposé de la totalité des mathématiques d'aujourd'hui. De plus, cet exposé est fait de telle manière que les liens entre les diverses branches des mathématiques deviennent clairement visibles>>. Car évidement, le fait de faire ressortir les différentes structures présentes permet de faire des liens entre les différents domaines. 
\`A vous de jouer ...

\section{L’algèbre de l’algèbre : les catégories}
\subsection{Introduction}
Imaginez-vous dans un vaste musée, où les chefs-d'œuvre de l'art mathématique prennent vie sous la lumière douce de la théorie et l'abstraction. Au cœur de cette galerie se trouve une salle mystérieuse, un sanctuaire intellectuel où les mathématiciens se réunissent pour explorer l'un des concepts les plus puissants et englobants qui soient : les catégories. Les catégories sont bien plus que de simples ensembles d'objets et de flèches que nous allons découvrir par la suite. Elles sont un concept central en mathématiques qui permet de structurer et d'organiser les objets mathématiques ainsi que les relations entre eux à fin d’avoir une vue globale de ces dernières et les étudier uniformément. Cette partie va nous aider à pénétrer dans ce monde abstrait et à découvrir ces fameuses catégories en s’appuyant sur des exemples pertinents et en soulignant sur les remarques, questions faites et réflexions qui en découlent.
\subsection{Définition}

Une catégorie $\mathcal{C}$ est composée des éléments suivants :
\begin{itemize}
    \item[\textbullet] Une collection d'objets: $\operatorname{Obj}(\mathcal{C})=\{\mathrm{X}, \mathrm{Y}, \mathrm{Z} \ldots\}$.
\item[\textbullet] Une collection de flèches: $\operatorname{Flech}(\mathcal{C})=\{\nearrow, \swarrow, \uparrow, \downarrow, \rightarrow, \ldots\}$
\item[\textbullet] Deux applications : «Source » $: \operatorname{Flech}(\mathcal{C}) \rightarrow \operatorname{Obj}(\mathcal{C})$.
«But » $: \operatorname{Flech}(\mathcal{C}) \rightarrow \operatorname{Obj}(\mathcal{C})$.
-\item[\textbullet] Une composition : pour toute paire de flèches:
$$
\mathrm{f:X} \rightarrow \mathrm{Y} \quad \mathrm{g}: \mathrm{Y} \rightarrow \mathrm{Z}
$$
\item[\textbullet] L'identité : pour tout objet c de $\mathcal{C}$ il existe idc : $\mathrm{C} \rightarrow \mathrm{C}$
\end{itemize}
Notons que la composition a comme propriété :

\hspace{50mm}L'associativité : $h\mathrm{o}(g\mathrm{o}f) = (h\mathrm{o}g)\mathrm{o}f$.


\subsection{Exemples}
\subsubsection{Catégories concrètes}
\begin{itemize}
    \item[\textbullet]	Catégorie des ensembles : 
    \begin{itemize}
        \item[\ding{118}] $\text{Obj}_{ens}$ $=X, Y$. des ensembles
        
\item[\ding{118}] $\text{Flech}_{ens}$ $=x \stackrel{f }\longmapsto=$ y: application /entre ensembles / injective/ surjective
\item[\ding{118}] Source $=f\longmapsto x$ surjective...
\item[\ding{118}] But $=f \longmapsto y$
\item[\ding{118}] "O" = composition normale des application
\end{itemize}
\textbf{Remarque:}

Jl n'existe pas un ensemble des ensembles, c'est pourquoi on utilisera plutôt le terme collection.

    \item[\textbullet] Catégorie des espaces vectoriels:
    \begin{itemize}
        \item[\ding{118}]	$\text{Obj}_{Vect}$ : Les objets dans cette catégorie sont les espaces vectoriels sur un champ donné (par exemple, le corps des nombres réels R ou le corps des nombres complexes C).
        
\item[\ding{118}]	$\text{Flech}_{Vect}$ : Les flèches dans cette catégorie sont les transformations linéaires entre les espaces vectoriels. Chaque flèche relie deux espaces vectoriels et représente une transformation linéaire d'un espace vectoriel dans un autre.

\item[\ding{118}]	Applications "Source" et "But" : Les applications "Source" et "But" associent à chaque flèche sa source et son but dans la catégorie des espaces vectoriels. La source d'une flèche est l'espace vectoriel de départ, et le but est l'espace vectoriel d'arrivée.

Source(f) : E1 (l'espace vectoriel de départ de la flèche f)

But(f) : E2 (l'espace vectoriel d'arrivée de la flèche f)
\item[\ding{118}]	La composition est une opération définie sur les flèches de la catégorie. Pour composer deux flèches f : E1 → E2 et g : E2 → E3, il doit être vérifié que l'espace vectoriel E2 est à la fois la source de f et le but de g (But(f) = Source(g)).
    \end{itemize}
    \item[\textbullet] Catégories des corps :  
    \begin{itemize}
        \item[\ding{118}] 
        $\text{Obj}_{Cor}$ : les ensembles munis d'opérations d'addition et de multiplication qui forment un corps ($\mathbb{Z}$/n$\mathbb{Z}, \mathbb{Q}$ …)
\item[\ding{118}]	$\text{Flech}_{Cor}$  : les homomorphismes de corps, inclusion ensembliste, application ensembliste

\item[\ding{118}]	Source / But : corps de départ et corps d’arrivé.

\item[\ding{118}]	« o » : composition des flèches

    \end{itemize}
\end{itemize}

\textbf{Citons aussi les catégories des anneaux, des groupes etc.}

\subsubsection{Catégories non-concrètes}

\begin{itemize}

    \item[\textbullet] Catégories des Quaternions : 
    \begin{itemize}
        \item[\ding{118}] 	$\text{Obj}_{Quat}$ : « . » nous manipulons des quaternions sous la forme $\alpha 1$+$\beta \mathrm{i}$+$\gamma \mathrm{j}$+ $\delta \mathrm{k}$, où , $\alpha,\beta,\gamma,$ \text{et}  $\delta$
           sont des nombres réels. 
        
\item[\ding{118}] $\text{Flech}_{Quat}$ : Les flèches dans cette catégorie sont les éléments $1, \mathrm{i}, \mathrm{j}$ et $\mathrm{k}$, qui sont les unités imaginaires des quaternions. Chacun de ces éléments est considéré comme une flèche distincte. $\mathrm{H}=\{\alpha 1+\mathrm{i} \beta+\mathrm{j} \gamma+\mathrm{k} \delta, \alpha, \beta, \gamma, \delta \in \mathbb{R}\}$

\item[\ding{118}] « o » : Pour deux flèches a et $\mathrm{b}$, la composition $\mathrm{a} \circ \mathrm{b}$ est la multiplication de quaternions correspondante :
$$
\begin{aligned}
& \mathrm{i}^2=\mathrm{j}^2=\mathrm{k}^2=\mathrm{ijk}=-1 \\
& \mathrm{ij}=-\mathrm{ji}=\mathrm{k}, \mathrm{jk}=-\mathrm{kj}=\mathrm{i}, \mathrm{ki}=-\mathrm{ik}=\mathrm{j} \ldots
\end{aligned}
$$ 
    \end{itemize}
    \item[\textbullet] Catégorie  Cnc: 
    \begin{itemize}
        \item[\textbullet] $\text{Obj}_{enc}$ = \quad \filledbubble
        \item[\textbullet] $\text{Flech}_{enc}$\quad=\quad\{-3,  -2,  ....  1..\}
        \item[\textbullet] 2 composition \qquad $\longrightarrow$ + \qquad($\text{id}_.$  =  0)

        \qquad \qquad \qquad \qquad 
        \hspace{0.3mm} $\longrightarrow$ $\times$ \qquad($\text{id}_.$  =  1)
    \end{itemize}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.2\textwidth]{sym1.png}
\end{figure}
\end{itemize}
\subsection{Contre-exemple}

Et quoi pour bien assimiler la notion de catégorie que de trouver un contre-exemple concret.
\vspace{3mm}

$\text{Obj}_{enc}$: \qquad \qquad 
 X , Y , Z , W
\vspace{3mm}

\begin{minipage}{0\textwidth}
 $\text{Flech}_{enc}$:   
\end{minipage}
\hspace{1mm}
\begin{minipage}{0.7\textwidth}
    
    \centering
\includegraphics[width=0.5\textwidth]{Sym.png}

\end{minipage}



 

\hspace{92mm}$(g\mathrm{o}f): = \phi$

\hspace{92mm} $(h\mathrm{o}g):  = \psi$ 

Composition: on voit clairement que  $\alpha$ \ne $\beta$  \hspace{21mm}$(h\mathrm{o}\phi): = \alpha$

\hspace{92mm}$(\psi\mathrm{o}f):=\beta$



\newpage
\bibliographystyle{amsalpha}
\bibliography{bib}
\end{document}
