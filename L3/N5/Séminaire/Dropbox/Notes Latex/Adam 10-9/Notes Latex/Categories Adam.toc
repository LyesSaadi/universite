\select@language {french}
\contentsline {section}{\tocsection {}{1}{Introduction : l'alg\IeC {\`e}bre moderne}}{1}{section.1}
\contentsline {subsection}{\tocsubsection {}{1.1}{L'Alg\IeC {\`e}bre ou l'analyse des quations}}{1}{subsection.1.1}
\contentsline {subsection}{\tocsubsection {}{1.2}{L'alg\IeC {\`e}bre ou l'axiomatisation des structures alg\IeC {\'e}briques}}{1}{subsection.1.2}
\contentsline {subsection}{\tocsubsection {}{1.3}{Int\IeC {\'e}r\IeC {\^e}ts de l'axiomatisation }}{3}{subsection.1.3}
\contentsline {subsection}{\tocsubsection {}{1.4}{L'Alg\IeC {\`e}bre moderne}}{4}{subsection.1.4}
\contentsline {subsection}{\tocsubsection {}{1.5}{Bourbaki}}{4}{subsection.1.5}
\contentsline {section}{\tocsection {}{2}{L\IeC {\textquoteright }alg\IeC {\`e}bre de l\IeC {\textquoteright }alg\IeC {\`e}bre : les cat\IeC {\'e}gories}}{4}{section.2}
\contentsline {subsection}{\tocsubsection {}{2.1}{Introduction}}{4}{subsection.2.1}
\contentsline {subsection}{\tocsubsection {}{2.2}{D\IeC {\'e}finition}}{4}{subsection.2.2}
\contentsline {subsection}{\tocsubsection {}{2.3}{Exemples}}{4}{subsection.2.3}
\contentsline {subsubsection}{\tocsubsubsection {}{2.3.1}{Cat\IeC {\'e}gories concr\IeC {\`e}tes}}{5}{subsubsection.2.3.1}
\contentsline {subsubsection}{\tocsubsubsection {}{2.3.2}{Cat\IeC {\'e}gories non-concr\IeC {\`e}tes}}{5}{subsubsection.2.3.2}
\contentsline {subsection}{\tocsubsection {}{2.4}{Contre-exemple}}{6}{subsection.2.4}
