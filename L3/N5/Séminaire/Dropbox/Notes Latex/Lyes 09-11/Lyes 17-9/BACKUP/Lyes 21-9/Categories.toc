\babel@toc {french}{}\relax 
\contentsline {section}{\tocsection {}{1}{Introduction : l'algèbre moderne}}{1}{section.1}%
\contentsline {subsection}{\tocsubsection {}{1.1}{L'Algèbre ou l'analyse des équations}}{1}{subsection.1.1}%
\contentsline {subsection}{\tocsubsection {}{1.2}{L'algèbre ou l'axiomatisation des structures algébriques}}{2}{subsection.1.2}%
\contentsline {subsection}{\tocsubsection {}{1.3}{Intérêts de l'axiomatisation }}{3}{subsection.1.3}%
\contentsline {subsection}{\tocsubsection {}{1.4}{L'Algèbre moderne}}{4}{subsection.1.4}%
\contentsline {subsection}{\tocsubsection {}{1.5}{Bourbaki}}{4}{subsection.1.5}%
\contentsline {section}{\tocsection {}{2}{L'algèbre de l'algèbre : les catégories}}{4}{section.2}%
\contentsline {subsection}{\tocsubsection {}{2.2}{Définitions}}{4}{subsection.2.2}%
\contentsline {subsection}{\tocsubsection {}{2.5}{Petites catégories}}{4}{subsection.2.5}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.5.1}{Définitions}}{5}{subsubsection.2.5.1}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.5.2}{Exemples}}{5}{subsubsection.2.5.2}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.5.3}{Contre-exemples}}{5}{subsubsection.2.5.3}%
\contentsline {subsection}{\tocsubsection {}{2.6}{Catégories associées à des ensembles partiellement ordonnés}}{5}{subsection.2.6}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.6.1}{Ensembles partiellement ordonnés}}{5}{subsubsection.2.6.1}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.6.2}{Graphes}}{6}{subsubsection.2.6.2}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.6.3}{Constructions}}{6}{subsubsection.2.6.3}%
