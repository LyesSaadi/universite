\subsubsection{Caractérisation}
Tout comme on peut caractériser les bijections uniquement à partir de l'application sans avoir la donnée de sa réciproque, on peut avoir envie de caractériser les foncteurs qui font partie d'une équivalence de catégories sans avoir la donnée du foncteur qui va dans l'autre sens. C'est précisément cette caractérisation que fournit le théorème suivant. 

\begin{theo}\label{theo:EquivCat}
        Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. Un foncteur $\mathrm{F} : \mathcal{C} \to \mathcal{D}$ fait partie d'une équivalence de catégories si et seulement si les conditions suivantes sont satisfaites :
        \begin{itemize}[label=\textbullet]
            \item $F$ est essentiellement surjectif, c'est-à-dire qu'il est surjectif au niveau des classes d'isomorphismes d'objets: pour tout objet $Y$ de $\mathcal{D}$, il existe un objet $X$ de $\mathcal{C}$ tel que 
            $\mathrm{F}(X)$ soit un isomorphisme à $Y$ dans $\mathcal{D}$. 
            \item $\mathrm{F}$ est un foncteur plein, 
            \item $\mathrm{F}$ est un foncteur fidèle.
        \end{itemize}
    \end{theo}

    \begin{proof}
        Nous allons procéder par analyse-synthèse. Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories et soit 
        $\mathrm{F} : \mathcal{C} \to \mathcal{D}$ un foncteur.
\begin{description}
\item[\sc Analyse] Supposons que $F$ fasse partie d'une équivalence de catégories :  il existe un foncteur $\mathrm{G} \colon \mathcal{C} \to \mathcal{D}$ et des isomorphismes naturels  
$\eta : \mathrm{Id}_{\mathcal{C}} \stackrel{\cong}{\Rightarrow} \mathrm{G}\circ \mathrm{F}$ 
et 
$ \varepsilon: \mathrm{F}\circ \mathrm{G} \stackrel{\cong}{\Rightarrow} \mathrm{Id}_{\mathcal{D}}$.
                Pour tout objet $D$ appartenant à la catégorie $\mathcal{D}$, nous avons 
                ${\varepsilon}_{D} \colon (\mathrm{F}\circ \mathrm{G})(D) {\cong} D $. Il existe donc un objet $C = \mathrm{G}(D)$ dans $\mathcal{C}$ tel que $\mathrm{F}(D)\cong D$. Par conséquent, le foncteur $\mathrm{F}$ est essentiellement surjective. 
                
                \medskip
                
En appliquant l'isomorphisme naturel $\eta$ à ton morphisme $f \colon C \to C'$ de $\mathcal{C}$, on obtient le diagramme commutatif suivant :
                \[
                \begin{tikzcd}
                    C \arrow[r, "\eta_{C}"] \arrow[d, "f"'] & (\mathrm{G}\circ \mathrm{F})(C)~\, \arrow[d, "(\mathrm{G}\circ \mathrm{F})(f)"] \\
                    C' \arrow[r, "\eta_{C'}"'] & (\mathrm{G}\circ \mathrm{F})(C')~,
                \end{tikzcd}
                \]
où $\eta_{C}$ et $\eta_{C'}$ sont des isomorphismes de $\mathcal{C}$. 
 On a donc $f={\eta_{C'}}^{-1} \circ (\mathrm{G}\circ \mathrm{F})(f)\circ\eta_{C}$~. 
Ceci montre que si deux flèches  $f,f': C \to C'$ vérifient $\mathrm{F}(f) = \mathrm{F}(f')$, alors on obtient 
\[
  f={\eta_{C'}}^{-1} \circ (\mathrm{G}\circ \mathrm{F})(f)\circ\eta_{C}
    ={\eta_{C'}}^{-1} \circ (\mathrm{G}\circ \mathrm{F})(f')\circ\eta_{C}=f' \]
et donc que $\mathrm{F}$ est fidèle.
De la même manière, en appliquant l'isomorphisme naturel $\varepsilon$, on montre que 
le foncteur $\mathrm{F}$ est plein. 

\item[\sc Synthèse] Supposons maintenant que le foncteur $\mathrm{F}$ soit plein, fidèle et essentiellement surjectif. Notre but est de montrer que ce foncteur appartient à une équivalence de catégories. Pour cela, on commence par construire un foncteur 
$\mathrm{G} : \mathcal{D} \to \mathcal{C}$ qui associe, à tout objet $D$ de la catégorie $\mathcal{D}$, un objet $\mathrm{G}(D)\coloneqq C$ de la catégorie $\mathcal{C}$ tel que son image $\mathrm{F}(C) \stackrel{\alpha_{D}}{\cong}D$ par $\mathrm{F}$ soit isomorphe à $D$ : pour cela on utilise le fait que le foncteur $\mathrm{F}$ est essentiellement surjectif et ... l'axiome du choix. 
            Pour tout morphisme $g : D\to D'$ de la catégorie $\mathcal{D}$, on considère la composée suivante
            \[
                \begin{tikzcd}
                    \mathrm{F}\circ \mathrm{G}(D) \arrow[r, "\alpha_{D}", "\cong"'] & D ~\, \arrow[d, "g"] \\
                    \mathrm{F}\circ \mathrm{G}(D') & \arrow[l, "{\alpha_{D'}}^{-1}"', "\cong"] 
                     D'~. 
                \end{tikzcd}
            \]
            Puisque le foncteur $\mathrm{F}$ est plein et fidèle, il existe un unique morphisme $f : \mathrm{G}(D)\to \mathrm{G}(D')$ tel que $\mathrm{F}(f) ={\alpha_{D'}}^{-1} \circ g \circ \alpha_{D}$; on pose donc $\mathrm{G}(g) \coloneq f$. On laisse à la lectrice ou au lecteur le soin de vérifier que cela définit bien un foncteur $\mathrm{G}$. 
            Par définition, la famille des isomorphismes $\{\alpha_{D} : F\circ G(D)\to D\}_{D\in \mathcal{D}}$ forme un isomorphisme naturel $\alpha : \mathrm{F}\circ \mathrm{G}\stackrel{\cong}{\Rightarrow} \mathrm{Id}_{\mathcal{D}}$.
            On laisse à la lectrice ou au lecteur le soin de montrer que l'on a aussi 
            $\mathrm{Id}_{\mathcal{C}}  \stackrel{\cong}{\Rightarrow} \mathrm{G}\circ \mathrm{F}$.    
   \end{description}
\end{proof}

\subsubsection{Catégories squelétales}

    \begin{defi}(Catégorie squelétale)
Une catégorie  $\mathcal{C}$ est dite \emph{squelétale} si toutes ses classes d'équivalence d'objets à isomorphisme près n'ont qu'un seul élément.
    \end{defi}
    
\begin{exem}
La catégorie $\mathcal{Mat}$ vue précédemment est une catégorie squelétale. 
        % et $\mathcal{Fin}$: définie où ? 
\end{exem}

    \begin{theo}\label{thm:CatSq}
        Toute catégorie $\mathcal{C}$ localement petite est équivalente 
         à une catégorie squelétale $\mathcal{sqC}$, unique à isomorphisme près. 
    \end{theo}

    Avant de démontrer ce théorème, énonçons un lemme qui nous sera utile pour la démonstration.

    \begin{lemm}\label{lemm:SquelEqIso}
        Deux catégories squelétales équivalentes sont isomorphes.
    \end{lemm}

    \begin{proof}
        Soient $\mathcal{S}$ et $\mathcal{S'}$ deux catégories squelétales équivalentes : 
 il existe deux foncteurs $\mathrm{F} : \mathcal{S} \to \mathcal{S'}$ et $\mathrm{G} : \mathcal{S'} \to \mathcal{S}$ et deux isomorphismes naturels  $ \eta : \mathrm{G}\circ \mathrm{F}\stackrel{\cong}{\Rightarrow} \mathrm{Id}_{\mathcal{S}}$ et $ \epsilon: \mathrm{Id}_{\mathcal{S'}} \stackrel{\cong}{\Rightarrow} \mathrm{F}\circ \mathrm{G}$.
        Pour tout objet $S$ de la catégorie $\mathcal{S}$, le morphisme $\eta_{S} : \mathrm{G}\circ \mathrm{F}(S)\to S$ est un isomorphisme. Comme la catégorie $\mathcal{S}$ est squelétale, le seul objet de $\mathcal{S}$ isomorphe à $S$ est lui même, d'où $\mathrm{G}\circ \mathrm{F}(S) = S$. On obtient donc, avec la naturalité de la transformation $\eta$, l'égalité de foncteurs $\mathrm{G}\circ \mathrm{F} = \mathrm{Id}_{\mathcal{S}}$. 
        Par un raisonnement analogue dans la catégorie $\mathcal{S}'$, on obtient que $\mathrm{F}\circ \mathrm{G} = \mathrm{Id}_{\mathcal{S'}}$. Au final, on en conclut que les deux catégories $\mathcal{S}$ et $\mathcal{S'}$ sont isomorphes $\mathcal{S} \cong \mathcal{S'}$. 
    \end{proof}
    On peut à présent procéder à la démonstration du théorème~\ref{thm:CatSq}. 
    \begin{proof}[Démonstration du théorème~\ref{thm:CatSq}]\leavevmode
        \begin{description}
            \item[\sc Construction d'une catégorie squelétale $\mathcal{sqC}$]  Construisons la catégorie $\mathcal{sqC}$ comme suit.
            \begin{description}
            \item[\it Objets] Les objets sont les classes d'équivalence à isomorphisme près des objets de la catégorie $\mathcal{C}$.
            
            \item[\it Morphismes]  Pour chaque classe d'équivalence $[A]$, on fixe un objet $A$ qui la représente puis on définit, pour toute paire d'objets $[A]$ et $[A']$ dans $\mathcal{sqC}$: 
            \[\mathrm{Hom}_{\mathcal{sqC}}\big([A], [A']\big) \coloneqq  \mathrm{Hom}_{\mathcal{C}}(A, A')~.\]
            
            \item[\it Composition] Le composition de la catégorie squelétale $\mathcal{sqC}$ est definie à partir de celle de la catégorie $\mathcal{C}$. Soient $f \colon [A] \to [A']$ et $g \colon [A'] \to [A'']$, elles proviennent de flèches  
            $f \colon A \to A'$ et $g \colon A' \to A''$ de la catégorie $\mathcal{C}$; leur composée est donc donnée par 
	$g\circ f \colon [A] \to [A'']$.

            \item[\it  Identités] Pour toute classe $[A]$, on considère le morphisme identité 
            $\mathrm{id}_{[A]}$ représenté par le morphisme identité $\mathrm{id}_A$ de $A$ dans $\mathcal{C}$. 
            \end{description}
            Les axiomes d'associativité et d'unitarité de la composition de $\mathcal{sqC}$ sont vérifiés, ils découlent de ceux de la catégorie $\mathcal{C}$.Par construction, la catégorie $\mathcal{sqC}$ est squelétale.
            
            \item[\sc \'Equivalence de catégories]  \`A présent, construisons un foncteur 
\begin{align*}
F: \mathcal{sqC} \to \mathcal{C}\\ 
[A]\mapsto A 
\end{align*} qui associe à une classe d'équivalence l'objet de la catégorie $\mathcal{C}$ qui le représente (axiome du choix). Par définition de la catégorie squelétale $\mathcal{sC}$, le foncteur $\mathrm{F}$ est plein et fidèle: 
            \[\mathrm{Hom}_{\mathcal{sqC}}\big([A], [A']\big) = \mathrm{Hom}_{\mathcal{C}}(A, A')~.\]
Il est aussi essentiellement surjectif, en effet : pour tout objet $B$ de la catégorie $\mathcal{C}$, sa classe d'équivalence à isomorphisme près est représentée par un objet $A$, c'est-à-dire 
$\mathrm{F}([A])=A \cong B$~.             
On en conclut que le foncteur $\mathrm{F}$ fait donc partie d'une équivalence de catégories par le théorème~\ref{theo:EquivCat}. 

            \item[\sc Unicité à isomorphisme près de $\mathcal{sqC}$] Supposons que $\mathcal{C}$ soit équivalente à deux catégories squelétales $\mathcal{S}$ et $\mathcal{S'}$. \'Etant donné que la relation <<être équivalent>> est une relation d'équivalence entre catégories, on obtient par transitivité que $\mathcal{S}$ et $\mathcal{S'}$ sont équivalentes. Elles sont donc isomorphes par le lemme~\ref{lemm:SquelEqIso}. 
	\end{description}
\end{proof}
    