\subsection{Produits et coproduits}

\subsubsection{Produits classiques}

Le concept de <<produit>> est une notion fondamentale que l'on rencontre fréquemment dans différentes structures, qu'elles relèvent de l'algèbre ou de l'informatique. 

\begin{itemize}
  \item Quand on \'etudie les ensembles, on a le produit cartésien. 
  \item Quand on \'etudie les corps, on d\'efinit le produit de deux
    corps $K$ et $L$, noté $\mathbf{K\times L}$, comme un corps où les
    opérations de base (addition et multiplication) sont définies
    composante par composante. Peuvent être définis de la même façon
    les produits de groupes, d'anneaux, d'espaces vectoriels. 
  \item En OCaml, qui est un langage de programmation fonctionnelle,
    le produit $\mathbf{int*string}$ définit le type des tuples
    (tuple = regroupement
    ordonné de valeurs) composés d'un entier de
    type `$int$' et d'une chaine de caractère de type `$string$'.
  % \item Le produit de deux graphes $(G, A)$ et $(G', A')$ vérifiant $|G| = |G'|$ peut être défini comme le produit de leur matrice d'adjacence respective
\end{itemize}
Au niveau de ces <<produits>>, on retrouve souvent cette idée de combiner des objets de façon struturée, générant ainsi des entités ayant leurs propres propriétés.
Nous allons voir qu'il est possible d'appliquer cette notion de produit à une échelle plus large.

\subsubsection{Exemple motivant : produit cartésien dans les ensembles}

Soient $A$ et $B$ deux ensembles. Le \emph{produit cartésien} de $A$ et $B$,
noté $A\times B$, est défini par l'ensemble des couples ordonnés $(a, b)$ tels que $a$ appartient à $A$ et $b$ appartient à $B$. Formellement:
$$ A\times B  = \{(a,b) \mid a \in A , b \in B  \} .$$
Sont définies par la même occasion les applications <<projections>> :
\[ p_{A} : A\times B \to A, \quad p_{A}(a, b) \coloneqq a, \]
\[ p_{B} : A\times B \to B, \quad p_{B}(a, b) \coloneqq b. \]
Analysons cette définition du produit cartésien et partons de l'idée selon laquelle :
$$ \forall a \in A, \quad \forall b \in B, \quad \exists! (a, b) \in A\times B. $$ 
En termes catégoriques, cela donne : 
\[\forall f : \{*\} \to A,\  \forall g : \{*\} \to B, \ \exists! h: \{*\} \to A\times B\] tel que
$$ p_{A} \circ h = f \quad \text{et} \quad p_{B} \circ h = g $$ 
Le diagramme suivant résume la situation ci-dessus:
\[
\begin{tikzcd}
& \{ *\} \arrow[bend right, swap]{ddl}{f}\arrow[bend left]{ddr}{g}
\arrow[dashed]{d}{\exists ! h} &  \\
& A \times B \arrow{dl}{p_A} \arrow[swap]{dr}{p_B} & \\
A & & B 
\end{tikzcd}
\]

\subsubsection{D\'efinition g\'en\'erale.} On va \`a présent rendre plus abstraite la notion
de produit, en la g\'en\'eralisant \`a une cat\'egorie quelconque.

\begin{defi}[Produit dans une catégorie] Soit $\mathcal{C}$ une
  catégorie (arbitraire) et soient $A, B$ appartenant à $\mathrm{Obj}(\mathcal{C})$.
    Un objet $P$  de $\mathcal{C}$ muni de morphismes $p_{A} : P \to
    A$ et $p_{B} : P \to B$ est un \emph{produit} de $A$ et $B$ dans
    $\mathcal{C}$ si pour tout objet 
    $X \in \mathrm{Obj}(\mathcal{C})$, 
    et pour toute paire de fl\`eches $X \xrightarrow{f} A$ et $X
    \xrightarrow{g} B$, il existe une unique fl\`eche $X
    \xrightarrow{h} P$ telle que
    $p_{A} \circ h = f$ et $p_{B} \circ h = g$. En forme
    diagrammatique, on a: 
\[
\begin{tikzcd}
& X \arrow[bend right, swap]{ddl}{f}\arrow[bend left]{ddr}{g}
\arrow[dashed]{d}{\exists !h} &  \\
& A \times B \arrow{dl}{p_A} \arrow[swap]{dr}{p_B} & \\
A & & B .
\end{tikzcd}
\]
\end{defi}
  
\begin{rema}
 Les morphismes $p_A$ et $p_B$, appelés \emph{projections}, sont en effet des \emph{données} et doivent être définies pour que la notion de produit puisse avoir un sens.  
\end{rema}

\begin{rema}
Le produit $P$, s'il existe, est souvent noté
 $A\times B$, et on écrit $h = \langle f, g \rangle$. On va voir qu'il
 est l\'egitime d'utiliser l'expression <<le>> produit, car leur
 d\'efinition rend les
 produits uniques \`a isomorphisme pr\`es.
\end{rema}

\begin{prop}(Produits dans $\mathcal{Ens}$)
  Soit $A$ et $B$ deux objets de $\mathcal{Ens}$. Le produit cartésien
  $A{\times}B$ est un produit catégorique de $A$ et $B$.
\end{prop}

\begin{proof}\leavevmode
\begin{description}
\item[\sc Existence] Soit $X$ un ensemble. Si les fonctions $f: X\to A$
et $g: X\to B$ sont définies, alors la fonction $h: X\to A\times B$
tel que $\forall x\in X,\;h(x)=(f(x), g(x))$ est bien définie.

\item[\sc Unicité] Il suffit de remarquer que pour n'importe quelle fonction
$h :X\to
A\times B$, on a $h(x) = (p_A(h(x)), p_B(h(x)))$ pour tout $x \in X$. 
Donc si $h, h' : X \to A \times B$ satisfont la propri\'et\'e
de la d\'efinition, alors pour tout $x\in X$, $h(x) = (f(x), g(x)) = h'(x).$ 
\end{description}
\end{proof}

\subsubsection{Propriétés des produits}

On montre d'abord que les produits sont <<stables par isomorphisme>>. En d'autres termes, un objet isomorphe à un produit donne un autre produit. 

\begin{prop}[Stabilité à isomorphisme près]\label{prop:ProdStabIso}
 Soit $\mathcal{C}$  une catégorie arbitraire. Soient $A, B$ des objets de $\mathcal{C}$,  
 et soit $(P, p_{A}, p_{B})$ un produit de
  $A$ et $B$.
  Si $Q$ est un objet de $\mathcal{C}$ muni d'un isomorphisme $\rho :
  P\xrightarrow{\cong} Q$, alors $Q$ est un produit dont les projections $q_A$ et $q_B$ sont
  d\'efinies comme suit:
  \[
    \begin{tikzcd}[row sep=2em]
    & Q \arrow[swap]{dl}{\rho^{-1}}\arrow{dr}{\rho^{-1}} & \\
    P \arrow[swap]{d}{p_A} & & P \arrow{d}{p_B} \\
    A & & B.
  \end{tikzcd}
  \]
\end{prop}

\begin{proof}
On doit montrer que $(Q, q_A, q_B)$ vérifie la propri\'et\'e d'un
produit. Prenons deux fl\`eches $f : X \to A$ et $g : X \to B$, on
doit montrer qu'il existe une unique fl\`eche $h : X \to Q$ telle que
$q_A \circ h = f$ et $q_B \circ h = g$. 

Par définition du produit $P$, on sait que pour tout objet  $X$ de $\mathcal{C}$, il existe un unique morphisme
$k : X\to  P$ tel que $p_{A} \circ k = f$ et $p_{B} \circ k = g$. On
d\'efinit $h = \rho \circ k$, et on a bien: 
  $$ f = p_{A} \circ k  = p_{A} \circ (\rho^{-1}\circ h) = (p_{A}
  \circ \rho^{-1}) \circ h =  q_{A}\circ h.$$
  Par un raisonnement similaire, on a $g =  p_{B} \circ
  (\rho^{-1}\circ h) = q_{B}\circ h$. 
Il nous reste \`a montrer l'unicit\'e de $h$. Supposons qu'il existe
un autre morphisme $h' : X \to Q$ tel que $f= q_{A}\circ h'$ et $g =
q_{B}\circ h'.$
On peut alors construire deux flèches $\rho^{-1}\circ h$ et $\rho^{-1}\circ h':X\to P$. On a: 
  $$P_{A}\circ(\rho^{-1}\circ h) = P^\prime_{A}\circ h = P^\prime_{A}\circ h' = f $$ 
  $$P_{B}\circ(\rho^{-1}\circ h') = P^\prime_{B}\circ h' = P^\prime_{B}\circ h = g $$
  mais, par définition du produit $P$, la flèche $X\xrightarrow{k} P$
  vérifiant $P_{A}\circ k= f\; et\; P_{B}\circ k = g$ est unique. On
  en conclut donc que $\rho^{-1}\circ h = \rho^{-1}\circ h'$ et donc
  que $h' = h$ puisque $\rho^{-1}$ est un isomorphisme.
L'argument est r\'esum\'e par le diagramme suivant.
  \[
\begin{tikzcd}
    % Nodes
  & X \arrow[d, dashed, "h"'] \arrow[dd,dashed, bend left=50, "k"] \arrow[dddl,bend
  right, "f"'] \arrow[dddr,  bend left, "g"]  & \\
  & Q  \arrow[d, red, bend
  right, "\rho^{-1}"'] & \\
  & P \arrow[u, red, bend right, "\rho"']\arrow[rd, "p_B"'] \arrow[ld, "p_A"] & \\
 A &  &  B 
\end{tikzcd}
\]
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "Categories"
%%% End:
