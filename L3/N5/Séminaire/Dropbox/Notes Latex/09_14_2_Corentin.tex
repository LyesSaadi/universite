\subsubsection{Union disjointe de catégories}
Maintenant que nous avons vu comment définir la notion de catégorie, nous pouvons chercher comment construire de nouvelles catégories à partir de catégories existantes.


\begin{defprop}[Union disjointe de catégories]
    Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. On appelle \emph{union disjointe de $\mathcal{C}$ et $\mathcal{D}$} la catégorie $\mathcal{C} \sqcup \mathcal{D}$ formée des données suivantes. 
    \begin{itemize}[label=\textbullet]
        \item La collection $\mathrm{Obj}_{\mathcal{C} \sqcup \mathcal{D}}$ de ses objets est formée des objets $\mathrm{Obj}_{\mathcal{C}}$ de $\mathcal{C}$ et des objets $\mathrm{Obj}_{\mathcal{D}}$ de $\mathcal{D}$ de manière disjointe. 
        \item $\mathrm{Hom}_{\mathcal{C} \sqcup \mathcal{D}}(E,F) \coloneqq
            \left\{\begin{array}{ll}
                \mathrm{Hom}_\mathcal{C}(E,F)~, & \text{si } E \text{ et } F \text{ sont dans } \mathrm{Obj}_{\mathcal{C}}~,\\
                \mathrm{Hom}_\mathcal{D}(E,F)~, & \text{si } E \text{ et } F \text{ sont dans } \mathrm{Obj}_\mathcal{D}~,\\
                \emptyset~, &  \text{ sinon}.
            \end{array}\right.$
        \item Les compositions et les identités de $\mathcal{C} \sqcup \mathcal{D}$ sont celles de $\mathcal{C}$ et de $\mathcal{D}$~.
    \end{itemize}
\end{defprop}    

\begin{proof}\leavevmode
    \begin{itemize}[label=\textbullet]
        \item La composition est bien associative par l'associativité des compositions de $\mathcal C$ et $\mathcal D$ respectivement. 
        \item La composition est bien unitaire par l'unitarité des identités de $\mathcal C$ et $\mathcal D$ respectivement. 
    \end{itemize}
\end{proof}

\begin{exem} Dans l'exemple suivant, on omet de représenter les flèches identités pour plus de clarté. 
    \begin{center}
    \begin{tikzpicture}
        \node (C) at (1,2.5) {$\mathcal C$};
        \node (1) at (1,2) {$1$};
        \node (2) at (0.5,1) {$2$};
        \node (3) at (1.5,1) {$3$};
        \node (4) at (1,0) {$4$};

        \draw[->] (1) to (2);
        \draw[->] (2) to (3);
        \draw[->] (2) to (4);
        \draw[->] (1) to (3);
        \draw[->, bend right=100] (1) to (4);
        
        \node (D) at (4,2.5) {$\mathcal D$};
        \node (a) at (4,2) {$a$};
        \node (b) at (3.5,1) {$b$};
        \node (c) at (4.5,1) {$c$};
        
        \draw[->] (a) to (b);
        \draw[->] (b) to (c);
        \draw[->] (c) to (a);
    \end{tikzpicture}
    \end{center}
    \begin{center}
    \begin{tikzpicture}
        \node (C) at (2,2.5) {$\mathcal{C} \sqcup \mathcal{D}$};
        \node (1) at (1,2) {$1$};
        \node (2) at (0.5,1) {$2$};
        \node (3) at (1.5,1) {$3$};
        \node (4) at (1,0) {$4$};

        \draw[->] (1) to (2);
        \draw[->] (2) to (3);
        \draw[->] (2) to (4);
        \draw[->] (1) to (3);
        \draw[->, bend right=100] (1) to (4);
        
        \node (a) at (3,2) {$a$};
        \node (b) at (2.5,1) {$b$};
        \node (c) at (3.5,1) {$c$};
        
        \draw[->] (a) to (b);
        \draw[->] (b) to (c);
        \draw[->] (c) to (a);
    \end{tikzpicture}
    \end{center}
\end{exem}

\begin{prop}
   La construction de l'union disjointe est \emph{commutative}, c'est-à-dire que 
  les deux catégories  $\mathcal{C} \sqcup \mathcal{D}$ et $\mathcal{C} \sqcup \mathcal{D}$ sont <<les mêmes>>. 
   \end{prop}
   
\begin{proo}
Tout objet est dans $\mathcal{C} \sqcup \mathcal{D}$ si et seulement si il est dans $\mathcal{D} \sqcup \mathcal{C}$. Les flèches, compositions et  identités des deux catégories sont identiques. 
\end{proo}

\begin{rema}La lectrice ou le lecteur attentif aura remarqué notre gène (mathématique) : nous n'avons pas encore défini de notion d'<<équivalence>> ou d'<<isomorphisme>> de catégorie ... Cela en saurait tarder. 
\end{rema}

\subsubsection{Joint de catégories}

\begin{defprop}[Joint de catégories]
    Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. On appelle \emph{joint entre $\mathcal{C}$ et $\mathcal{D}$} la catégorie $\mathcal{C} \rhd \mathcal{D}$ formée des données suivantes. 
    \begin{itemize}[label=\textbullet]
	\item $\mathrm{Obj}_{\mathcal{C} \sqcup \mathcal{D}}\coloneqq \mathrm{Obj}_{\mathcal{C} \rhd  \mathcal{D}}$~.
	\item $\mathrm{Hom}_{\mathcal{C} \rhd  \mathcal{D}}(E,F) \coloneqq
            \left\{\begin{array}{ll}
                \{E \rightarrow F\}~, &\text{ si } E \text{ est dans } \mathrm{Obj}{\mathcal C} \text{ et } F \text{ est dans } \mathrm{Obj}_{\mathcal D}~,\\
                \mathrm{Hom}_\mathcal{C}(E,F)~, & \text{si } E \text{ et } F \text{ sont dans } \mathrm{Obj}_\mathcal{C}~,\\                
                \mathrm{Hom}_\mathcal{D}(E,F)~, & \text{si } E \text{ et } F \text{ sont dans } \mathrm{Obj}_\mathcal{D}~,\\
                \emptyset~, &  \text{ si } E \text{ est dans } \mathrm{Obj}{\mathcal D} \text{ et } F \text{ est dans } \mathrm{Obj}_{\mathcal C}~. 
            \end{array}\right.$
        \item Les compositions de la catégorie joint $C \rhd D$ sont données par les compositions des catégories $\mathcal C$ et de $\mathcal D$ lorsque les trois objets appartiennent à une de ces deux catégories. Sinon, lorsque deux objets appartiennent à une catégorie et le troisième objet à l'autre catégorie, la composition est l'unique flèche entre le premier objet et le dernier. 
             \item Les identités de la catégorie joint $\mathcal C \rhd \mathcal D$ sont celles des catégories $\mathcal C$ et celles de $\mathcal D$~. 
    \end{itemize}
\end{defprop}

\begin{exem}
Dans l'exemple suivant, on omet de représenter les flèches identités et les compositions pour plus de clarté. 
    \begin{center}
    \begin{tikzpicture}
        \node (C) at (1,2.5) {$\mathcal C$};
        \node (1) at (1,2) {$1$};
        \node (2) at (0.5,1) {$2$};
        \node (3) at (1.5,1) {$3$};
        \node (4) at (1,0) {$4$};

        \draw[->] (1) to (2);
        \draw[->] (2) to (3);
        \draw[->] (2) to (4);
        
        \node (D) at (4,2.5) {$\mathcal D$};
        \node (a) at (4,2) {$a$};
        \node (b) at (4,1) {$b$};
        
        \draw[->] (a) to (b);
    \end{tikzpicture}
    \end{center}
    \begin{center}
    \begin{tikzpicture}
        \node (C) at (2,2.5) {$\mathcal C \rhd \mathcal D$};
        \node (1) at (1,2) {$1$};
        \node (2) at (0.5,1) {$2$};
        \node (3) at (1.5,1) {$3$};
        \node (4) at (1,0) {$4$};

        \draw[->] (1) to (2);
        \draw[->] (2) to (3);
        \draw[->] (2) to (4);
        
        \node (a) at (2,0) {$a$};
        \node (b) at (3,0) {$b$};
        
        \draw[->] (a) to (b);
        \draw[->] (3) to (a);
        \draw[->] (4) to (a);
    \end{tikzpicture}
    \begin{tikzpicture}
        \node (C) at (2,2.5) {$\mathcal D \rhd \mathcal C$};
        \node (1) at (1,2) {$1$};
        \node (2) at (0.5,1) {$2$};
        \node (3) at (1.5,1) {$3$};
        \node (4) at (1,0) {$4$};

        \draw[->] (1) to (2);
        \draw[->] (2) to (3);
        \draw[->] (2) to (4);
        
        \node (a) at (3,2) {$a$};
        \node (b) at (2.5,1) {$b$};
        
        \draw[->] (a) to (b);
        \draw[->] (b) to (1);
    \end{tikzpicture}
    \end{center}
\end{exem}

\begin{proof}\leavevmode
    \begin{itemize}[label=\textbullet]
        \item Pour tout triplet de flèches composables $f : E \to F$, $g : F \to G$ et $h : G \to H$, si les quatre objets $E,F,G,H$ appartiennent à la catégorie $\mathcal C$ ou $\mathcal D$ respectivement, alors l'associativité de la composition des morphismes de la catégorie $\mathcal C$ ou $\mathcal D$ respectivement donne 
        \[ h\circ (g \circ f) = (h \circ g) \circ f~. \]
        Si $E$, $E$ et $F$, ou $E$, $F$ et $G$ sont des objets de la catégorie $\mathcal{C}$, alors les deux compositions $h\circ (g \circ f)$ et $(h \circ g) \circ f$ sont égales car elles coincident chacune avec l'unique flèche $E \to H$. 
        
        \item Soit $f : E \to F$ un morphisme de la catégorie joint $\mathcal C \rhd \mathcal D$. Si les deux objets $E$ et $F$ appartiennent à la même catégorie $\mathcal C$ ou $\mathcal D$, l'unitarité respective des catégories $\mathcal C$ ou $\mathcal D$ donne $\mathrm{id}_F \circ f = f = f \circ  \mathrm{id}_E$~. Si $E$ est un object de $\mathcal C$ et si $F$ est un objet de la catégorie $\mathcal D$, comme il n'existe qu'un seul morphisme entre $E$ et $F$, alors il est égal aux trois morphismes  $\mathrm{id}_F \circ f = f = f \circ  \mathrm{id}_E$. 
    \end{itemize}
\end{proof}

\begin{rema}
   La  joint entre les catégories n'est \emph{pas} une construction commutative comme le montre l'exemple ci-dessus. 
\end{rema}

\begin{prop}
Pour tout $n \in \mathbb{N}^*$, on considère la catégorie $\mathcal{C}_{[n]}$ associée à l'ensemble totalement ordonné $[n]\coloneqq (\{1, \ldots, n\} ,\leq)$~. 
La catégorie joint de $\mathcal{C}_{[m]}$ avec $\mathcal{C}_{[n]}$ est la catégorie 
\[
\mathcal{C}_{[n]} \rhd \mathcal{C}_{[m]}=\mathcal{C}_{[n+m]}~. 
\]
\end{prop}

\begin{proof}
Encore une fois, cette démonstration est informelle car nous n'avons pas encore de notion de comparaison entre catégories. \'Etudions néanmoins la catégorie joint $\mathcal{C}_{[n]} \rhd \mathcal{C}_{[m]}$~. 
En omettant les morphismes identités, les morphismes <<élémentaires>>, c'est-à-dire ceux à partir desquels on peut obtenir tous les autres par composition sont les suivants : 
\[\mathcal{C}_{[n]} : 1 \rightarrow 2 \rightarrow \cdots \rightarrow n \qquad \text{et} \qquad \mathcal{C}_{[m]} : 1 \rightarrow 2 \rightarrow \cdots \rightarrow m~.\]
Avec la même convention, les morphismes <<élémentaires>> de la catégorie joint $\mathcal{C}_{[n]}\rhd\mathcal{C}_{[m]}$
sont les suivants :
\[ \mathcal{C}_{[n]}\rhd\mathcal{C}_{[m]} \ : \ 1_{\mathcal C} \rightarrow 2_{\mathcal C} \rightarrow \cdots \rightarrow n_{\mathcal C} \ \textcolor{teal}{\rightarrow}\  1_{\mathcal D} \rightarrow 2_{\mathcal D} \rightarrow \cdots \rightarrow m_{\mathcal D}~.\]

La composition des morphismes de $\mathcal{C}_{[n]}\rhd\mathcal{C}_{[m]}$ et le fait que chaque élément de $\mathrm{Obj}_{\mathcal{C}_{[n]}}$ a un morphisme vers chaque élément de $\mathrm{Obj}_{\mathcal{C}_{[m]}}$ nous permettent de compléter <<l'ordre>> entre $[n]$ et $[m]$ : en notant, 
$1_{\mathcal D} = n+1$, $2_{\mathcal D} = n+2$, \ldots, $m_{\mathcal D} = n+m$, 
on retrouve bien la catégorie $\mathcal{C}_{[n+m]}$~.
\end{proof}

\subsubsection{Produit de catégories}

\begin{defprop}[Produit de catégories]
    Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. On appelle \emph{produit de $\mathcal{C}$ et $\mathcal{D}$} la catégorie $\mathcal{C} \times \mathcal{D}$ formée des données suivantes. 
    
    \begin{itemize}[label=\textbullet]
        \item La collection $\mathrm{Obj}_{\mathcal{C} \times \mathcal{D}}$ de ses objets est formée 
        des paires $(E,F)$ d'objets $E$ de $\mathrm{Obj}_{\mathcal C}$ et $F$ de $\mathrm{Obj}_{\mathcal D}$.
        \item Les morphismes de $(E,F)$ vers $(E',F')$ sont les paires $(f,g)$ de morphismes $f : E \to E'$ de $\mathcal C$ et $g : F \to F'$ de $\mathcal D$ respectivement. 
        
        \item La composition de deux paires $(f : E \to E', g : F \to F')$ et $(f' : E' \to E'', g : F' \to F'')$ est définie par la paire de composées :  
        \[(f', g')\circ (f,g)\coloneqq (f'\circ f, g' \circ g)~.\]
        \item  Les identités sont définies par les paires d'identités : 
       \[\mathrm{id}_{(E,F)}\coloneqq (\mathrm{id}_E, \mathrm{id}_F)~.\] 
    \end{itemize}    
\end{defprop}

\begin{exem}
Dans l'exemple suivant, on omet de représenter les flèches identités pour plus de clarté. 

    \begin{center}
    \begin{tikzpicture}
        \node (C) at (1,2.5) {$\mathcal C$};
        \node (1) at (1,2) {$1$};
        \node (2) at (0.5,1) {$2$};
        \node (3) at (1.5,1) {$3$};

        \draw[->] (1) to (2);
        \draw[->] (2) to (3);
        \draw[->] (3) to (1);
        
        \node (D) at (3,2.5) {$\mathcal D$};
        \node (a) at (3.5,1.5) {$a$};
        \node (b) at (2.5,1.5) {$b$};
        
        \draw[->] (a) to (b);
    \end{tikzpicture}
    \end{center}
    \medskip
    \begin{center}
    \begin{tikzpicture}[math3d]
        \node (CxD) at (0, 2,2.8) {$\mathcal C \times \mathcal D$};
        \node (1a) at (0,3.5,2) {$(1,a)$};
        \node (1b) at (3,1.5,2) {$(1,b)$};
        \node (2a) at (0,2,0) {$(2,a)$};
        \node (2b) at (3,0,0) {$(2,b)$};
        \node (3a) at (0,5,0) {$(3,a)$};
        \node (3b) at (3,3,0) {$(3,b)$};

        \draw[->] (1b) to (2b);
        \draw[->] (2b) to (3b);
        \draw[->] (3a) to (1a);
        \draw[->] (3a) to (3b);
        \draw[->] (1a) to (2a);
        \draw[->] (2a) to (2b);
        \draw[->] (2a) to (3a);
        \draw[->] (2a) to (3b);
        \draw[->] (1a) to (1b);        
        \draw[->] (1a) to (2b);
                
\fill[white] (0.66,1.56,0) circle (1.5pt);
\fill[white] (0,2.4,0.54) circle (1.5pt); 
\fill[white] (1.7,1.54,0.87) circle (1.5pt); 
\fill[white] (1.3,2.03,1.16) circle (1.5pt); 
       
        \draw[->] (3b) to (1b);
        \draw[->] (3a) to (1b);    
    \end{tikzpicture}
    \end{center}
\end{exem}

\begin{proof}
La démonstration est automatique en posant bien les choses. On la laisse donc aux soins du lecteur ou de la lectrice.
\end{proof}

\subsubsection{Catégorie opposée}

Lorsqu'on a une catégorie, on peut avoir envie, tout d'un coup, de changer l'orientation de toutes les flèches. Il se trouve que c'est possible et que cela donne bien une catégorie. 

\begin{defi}[Catégorie opposée]
    Soit $\mathcal C$ une catégorie. On note ${\mathcal C}^{\rm op}$ la \emph{catégorie opposée} formée des données suivantes. 
    \begin{itemize}[label=\textbullet]
        \item $\mathrm{Obj}_{{\mathcal C}^{\rm op}} \coloneq \mathrm{Obj}_{\mathcal C}$~.
        \item Pour tout morphisme $f : X \to Y$ de la catégorie $\mathcal C$, on considère un morphisme dit \emph{opposé} $f^{\rm op} : Y \to X$ dans $\mathcal{C}^{\rm op}$.
        \item La composition de deux morphismes $f^{\rm op} : Y \to X$ et $g^{\rm op} : Z \to Y$ est définie par le morphisme opposé du composé dans la catégorie $\mathcal C$~: 
        \[f^{\rm op}\circ_{\mathcal{C}^{\rm op}} g^{\rm op}\coloneqq (g\circ_{\mathcal{C}} f)^{\rm op}~.\]
        \item Les identités de la catégorie opposée ${\mathcal C}^{\rm op}$ sont les (opposées des) identités de la catégorie $\mathcal C$~. 
    \end{itemize}
\end{defi}

\begin{exem} La catégorie opposée de la catégorie $\mathcal{C}_{[n]} = 1 \rightarrow 2 \rightarrow 3 \rightarrow \cdots \rightarrow n$ est  la catégorie 
\[\mathcal{C}_{[n]}^{\rm op} = n \rightarrow \cdots \rightarrow 3 \rightarrow 2 \rightarrow 1~.\]
\end{exem}

\subsubsection{Sous-catégories}

\begin{defi}[Sous-catégorie] Soit $\mathcal C$ une catégorie. Une catégorie $\mathcal S$ est une \emph{sous-catégorie} de $\mathcal C$ (noté $\mathcal S \subset \mathcal C$) si 
    \begin{itemize}[label=\textbullet]
        \item chaque élément de $\mathrm{Obj}_{\mathcal S}$ est aussi dans $\mathrm{Obj}_{\mathcal{C}}$~,
        \item chaque morphisme de $\mathrm{Hom}_{\mathcal S}(X,Y)$ est également dans $\mathrm{Hom}_{\mathcal C}(X,Y)$~, 
        \item la composition des morphismes de la catégorie $\mathcal S$ est donnée par la composition des morphismes de la catégorie $\mathcal C$, 
        \item les identités de $\mathcal S$ sont celles de $\mathcal C$, pour les éléments de $\mathrm{Obj}_{\mathcal S}$~.
    \end{itemize}
\end{defi}

\begin{exem}
    Soit $\mathcal C$ la catégorie faite des objets  
    $\mathrm{Obj}_{\mathcal C}\coloneqq \{a,b,c,d,e\}$  et des morphismes suivants (en ajoutant les identités) : 
    
    \begin{center}
    \begin{tikzpicture}
        \node (a) at (1,3) {$a$};
        \node (b) at (1,2) {$b$};
        \node (c) at (0,1) {$c$};
        \node (d) at (2,1) {$d$};
        \node (e) at (1,0) {$e$};
        
        \draw[->] (a) to (b);
        \draw[->] (b) to (c);
        \draw[->] (b) to (d);
        \draw[->] (c) to (e);
    \end{tikzpicture}
    \end{center}
    
    On considère la sous-catégorie $\mathcal S$ faite des objets  
    $\mathrm{Obj}_{\mathcal S}\coloneqq \{a,b,d\}$  et des morphismes suivants (en ajoutant les identités) : 
    \begin{center}
    \begin{tikzpicture}
        \node (a) at (0,1) {$a$};
        \node (b) at (1,1) {$b$};
        \node (d) at (2,1) {$d$};
        
        \draw[->] (a) to (b);
        \draw[->] (b) to (d);
    \end{tikzpicture}
    \end{center}
\end{exem}

\begin{prop}
Toute sous-catégorie $\mathcal S$ d'une catégorie $\mathcal C$ vérifie ${\mathcal{S}}^{\rm op} \subset {\mathcal{C}}^{\rm op}$~. 
\end{prop}
    
    \begin{proof}\leavevmode
        \begin{itemize}[label=\textbullet]
            \item Les éléments de $\mathrm{Obj}_{\mathcal{C}^{\rm op}}$ sont les mêmes que $\mathrm{Obj}_{\mathcal{C}}$ et les éléments de $\mathrm{Obj}_{\mathcal{S}^{\rm op}}$ sont les mêmes que $\mathrm{Obj}_{\mathcal{S}}$. Ces derniers étant dans $\mathrm{Obj}_{\mathcal{C}}$, les objets de $\mathrm{Obj}_{\mathcal{S}^{\rm op}}$ sont aussi dans $\mathrm{Obj}_{\mathcal{C}}$ et par conséquent dans $\mathrm{Obj}_{\mathcal{C}^{\rm op}}$~.
            \item Comme les morphismes de $\mathcal S$ sont dans $\mathcal C$, en passant à l'opposé on a que les morphismes opposés de ceux de $\mathcal S$ sont aussi des morphismes opposés de ceux de $\mathcal C$, donc les morphismes de $\mathrm{Obj}_{\mathcal{S}^{\rm op}}$ sont des morphismes de $\mathrm{Obj}_{\mathcal{C}^{\rm op}}$~. 
            \item De la même manière, on voit que la composition des morphismes de $\mathrm{Obj}_{\mathcal{S}^{\rm op}}$ est induite par la composition des morphismes de $\mathrm{Obj}_{\mathcal{C}^{\rm op}}$~. 
            \item Encore de la même manière, les identités de $\mathrm{Obj}_{\mathcal{S}^{\rm op}}$ sont celles de $\mathrm{Obj}_{\mathcal{C}^{\rm op}}$~. 
        \end{itemize}
    \end{proof}

\subsubsection{Intersection de sous-catégories}

\begin{defprop}[Intersection de sous-catégories]
    Soit $\mathcal C$ une catégorie et soient $\mathcal S,\mathcal S'$ deux sous-catégories 
    $\mathcal S \subset \mathcal C$, $\mathcal S' \subset \mathcal C$ de $\mathcal C$~.
    On appelle \emph{intersection} de $\mathcal S$ et $\mathcal S'$ (notée $\mathcal S\cap \mathcal S'$) est la sous-catégorie de $C$ telle que :
    \begin{itemize}[label=\textbullet]
        \item les objets de $\mathcal S \cap \mathcal S'$ sont les objets de $\mathcal C$ étant à la fois dans $\mathcal S$ et dans $\mathcal S'$~,
        \item Les morphismes de $X$ vers $Y$ dans $\mathcal S \cap \mathcal S'$ sont les morphismes de $X$ vers $Y$ de $\mathcal C$ à la fois présents dans $\mathcal S$ et dans $\mathcal S'$~,
        \item les compositions et identités sont celles de $\mathcal  C$ pour les éléments de $\mathrm{Obj}_{\mathcal  S \cap \mathcal S'}$~.
    \end{itemize}
\end{defprop}

    \begin{proof}
        La catégorie $\mathcal S \cap \mathcal S'$ est bien une sous-catégorie de $\mathcal  C$ : la composée de deux morphismes de $\mathcal S$ (respectivement de $\mathcal S'$) est un morphisme de $\mathcal S$ (respectivement de $\mathcal S'$). 
    \end{proof}
