(TeX-add-style-hook
 "Categories"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("amsart" "twoside" "11pt")))
   (TeX-run-style-hooks
    "latex2e"
    "package"
    "09_07_1_Bruno"
    "09_07_2_Adam"
    "09_14_1_Lyes"
    "09_14_2_Corentin"
    "09_21_1_Oumaima"
    "09_21_2_Alaeddine"
    "10_05_1_Nassira"
    "09_28_1_Mamadou"
    "09_28_2_Mactar"
    "10_05_2_Adya"
    "10_19_1_Jeno"
    "10_12_Moustapha"
    "10_19_2_ChungAnh_update"
    "11_9_Mamadou"
    "11_9_Lyes"
    "11_16_1_Adam"
    "11_16_Oumaima"
    "12_7_Moustapha"
    "exercice_version_sans_correction"
    "amsart"
    "amsart11"))
 :latex)

