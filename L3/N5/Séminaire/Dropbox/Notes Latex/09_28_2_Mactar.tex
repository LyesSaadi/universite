\begin{prop}\label{prop:ProdIso}
Soient $A$ , $B$, $P$ et $Q$ des objets de $\mathcal{C}$ tels que :
\begin{itemize}[label=\textbullet]
    \item $P$ est un produit de $A$ et $B$, muni de projections \( p_A: P \rightarrow A \) et
    \( p_B: P \rightarrow B \).
    \item $Q$ est un produit de $A$ et $B$, muni de projections \( q_A: Q \rightarrow A \) et
    \( q_B: Q \rightarrow B \).
\end{itemize}
Alors il existe un isomorphisme entre $P$ et $Q$.
\end{prop}

\begin{proof}
  On sait que $P$ est un produit de $A$ et $B$, on peut donc
  instancier la propri\'et\'e du produit en prenant $X = Q$, $f = q_A$ et $g =
  q_B$, pour obtenir un unique morphisme $h : Q \to P$ tel que $p_B
  \circ h = q_B$ et $p_A \circ h = q_A$.   

  De la m\^eme mani\`ere, puisque $Q$ est un produit de $A$ et $B$, on
  obtient un unique morphisme $k : P \to Q$ tel que $q_B
  \circ k = p_B$ et $q_A \circ k = p_A$. On obtient les deux diagrammes
  commutatifs suivants:
  \[
    \begin{tikzcd}
      & P \arrow[swap]{dl}{p_A} \arrow{dr}{p_B} & \\
      A &Q \arrow[ swap]{u}{h} \arrow{l}{q_A} \arrow[swap]{r}{q_B} & B 
    \end{tikzcd}
    \qquad
    \begin{tikzcd}
      & P \arrow[swap]{dl}{p_A} \arrow{dr}{p_B} \arrow[swap]{d}{k}& \\
      A & \arrow{l}{q_A} \arrow[swap]{r}{q_B}  Q & B 
    \end{tikzcd}
  \]

 Pour montrer que $P$ et $Q$ sont isomorphes, on v\'erifie que \( k \circ h = \text{id}_Q \) et \( h \circ k =
\text{id}_P \). En effet, puisque $P$ est un produit, il existe un
unique morphisme $P \to P$ faisant commuter le diagramme suivant
\[
    \begin{tikzcd}
      & P \arrow[swap]{dl}{p_A} \arrow{dr}{p_B} \arrow[swap, dashed]{dd}{}& \\
      A &  & B.\\
      & \arrow{ul}{p_A} \arrow[swap]{ur}{P_B}  P &
    \end{tikzcd}
\]
Or on a les deux diagrammes commutatifs 
\[
  \begin{tikzcd}
      & P \arrow[swap]{dl}{p_A} \arrow{dr}{p_B} \arrow[swap]{dd}{\mathrm{id}_P}& \\
      A &    & B \\ 
       & P  \arrow{ul}{p_A} \arrow[swap]{ur}{P_B} & 
    \end{tikzcd}
    \qquad
    \begin{tikzcd}
      & P \arrow[swap]{dl}{p_A} \arrow{dr}{p_B} \arrow{d}{k}& \\
      A & Q\arrow{l}{q_A} \arrow[swap]{r}{q_B} \arrow{d}{h} & B \\
      & \arrow{ul}{p_A} \arrow[swap]{ur}{P_B}  P &
    \end{tikzcd}
  \]
et donc $h \circ k = \mathrm{id}_P$. De la même manière on montre que
\( k \circ h = \text{id}_Q \), et ainsi que $P$ et $Q$ sont isomorphes.
\end{proof}

 Pour deux objets $A$ et $B$ d'une cat\'egorie $\mathcal{C}$ qui admettent un produit, on a 
 \'ecrire \(A \times B \cong  B \times A\). En termes plus pr\'ecis:

\begin{prop}\label{prop:ComProd}
Si $A, B$ sont des objets d'une catégorie $\mathcal{C}$  qui admettent un produit $(A \times B,
 p_A, p_B)$, alors $(A \times B, p_B,
 p_A)$ est un produit de $B$ et $A$. 
\end{prop}

\begin{proof}   
La définition du produit est facilement vérifiée par le triplet $(A \times B, p_B,
 p_A)$ : il suffit d'inverser l'ordre des morphismes $f$ et $g$. 
\end{proof}

\subsubsection{Lien avec l'objet terminal}
Pour terminer cette section, on d\'efinit la notion d'\emph{objet terminal},
qui g\'en\'eralise le singleton dans les ensembles. 

\begin{defi}[Objet terminal]
    Dans une \emph{catégorie $\mathcal{C}$}, un objet \( \mathbf{1} \)
    est \emph{terminal} s'il existe un unique morphisme \( X \rightarrow
    \mathbf{1} \), 
    pour tout objet  $X$  de $\mathcal{C}$.
\end{defi}

Il est facile de v\'erifier que dans la cat\'egorie $\mathcal{Ens}$ des ensembles,
l'objet $\{ *\}$ (ainsi que n'importe quel autre ensemble singleton)
est terminal. Dans les ensembles, on a une bijection $A \times \{ *\} \cong A$, ce qui se généralise de la manière suivante. 

\begin{prop}\label{prop:ProdTerm}
Soit $\mathcal{C}$ une cat\'egorie poss\'edant un objet terminal
$1$. Pour tout objet $A$ de $\mathcal{C}$, le produit de $A$ avec $1$ existe et est donné par 
 \(A \times 1 \cong A\).
\end{prop}

\begin{proof}
  Puisque les produits sont uniques \'a isomorphisme pr\`es, il suffit
  de montrer que $A$ est un produit de $A$ et $1$. Les projections
  sont donn\'ees par l'identit\'e $\mathrm{id_A} : A \to A$ et
  l'unique morphisme $* : A \to 1$ vers l'objet terminal.

  On v\'erifie la
  propri\'et\'e du produit. Soit $X$ un objet de $\mathcal{C}$
  muni de morphismes \( g : X \to 1 \) et \( f : X \to A\).
  On veut montrer qu'il existe un unique morphisme \( k : X
  \rightarrow A \) tel que ce diagramme commute :
  \[
    \begin{tikzcd}
      & X \arrow{d}{k} \arrow[swap, bend right]{ddl}{f} \arrow[bend left]{ddr}{g} & \\
      & A \arrow[swap]{dl}{\mathrm{id}_A} \arrow{dr}{*} & \\
      A & & 1 
    \end{tikzcd}
  \]
  L'\'equation $\mathrm{id}_A \circ k = f$ donn\'ee par le diagrame
  nous force \`a poser $k = f$. Il reste \`a montrer que la
  partie droite du diagramme commute, mais on a forc\'ement $g = *
  \circ f$, car il s'agit de deux morphismes vers un objet terminal.
\end{proof}

\subsubsection{Coproduit d'objets dans une cat\'egorie}

Dans la continuité de notre exploration de la théorie des catégories,
après avoir examiné en détail la notion de produit de catégorie, nous
nous tournons désormais vers un concept complémentaire fondamental :
\emph{le coproduit}. Le coproduit est un concept dual au produit : un
coproduit est un produit dans la cat\'egorie oppos\'ee; en particulier
les projections deviennent des injections.

\begin{defi}[Coproduit]
   Dans une cat\'egorie $\mathcal{C}$, un \emph{coproduit} de deux
   objets $A$ et $B$ est un objet $K$ muni d'\emph{injections}
   $i_A : A \to K$ et $i_B : B \to K$ satisfaisant la
   propri\'et\'e suivante: pour tout
   objet $X$ et pour tous morphismes $f : A \to X$ et $g : B \to X$, il existe
   un unique $h : K \to X$ tel que le diagramme ci-dessous commute : 
\[
   \begin{tikzcd}
     & X & \\
     & K \arrow{u}{h} & \\
     A \arrow[swap]{ur}{i_A} \arrow[bend left]{uur}{f}  & &
     \arrow{ul}{i_B} \arrow[swap, bend right]{uul}{g} B.
   \end{tikzcd}
 \]
\end{defi}

\begin{prop}
Le coproduit de deux objets \(A\) et \(B\) dans la cat\'egorie des
ensembles est leur union disjointe, not\'ee $A \sqcup B$. Ses éléments sont des paires \((1, a)\) pour chaque élément \(a\) de \(A\) et des paires \((2, b)\) pour chaque élément \(b\) de \(B\):
    \[A \sqcup  B = \{(1, a) \mid a \in A\} \cup \{(2, b) \mid b \in B\}. \]
  \end{prop}    
\begin{rema} Si l'ensemble \(A\) a \(m\) éléments et l'ensemble \(B\) a \(n\)
    éléments, alors leur coproduit a \(m + n\) éléments.  Par exemple,
    le coproduit de \(\{\mathrm{chien}, \mathrm{chat}, \mathrm{souris}\}\) et  \(\{\mathrm{chien}, \mathrm{loup},
    \mathrm{ours}\}\) est l'ensemble \(\{(1, \mathrm{chien}), (1,  \mathrm{chat}), (1, \mathrm{souris}),
    (2, \mathrm{chien}), (2, \mathrm{loup}), (2, \mathrm{ours})\}\).
  \end{rema}
\begin{proof}
      Soit \(X\) \(\in \mathrm{Obj}_{\mathcal{Ens}}\) et soient  
        \( g: B \rightarrow X \) et \( f : A \rightarrow X \) des morphismes. 
        On définit un morphisme \(h : A \sqcup B  \rightarrow X \) comme suit:
        \[
            h(z) = 
            \left\{
            \begin{array}{ll}
                f(a) & \text{si } z = (1, a) \\
                g(b) & \text{si } z = (2, b).
            \end{array}
            \right.
        \]
Il reste \`a v\'erifier que le diagramme suivant commute
\[
  \begin{tikzcd}
     & X & \\
     & A \sqcup B \arrow{u}{h} & \\
     A \arrow[swap]{ur}{i_A} \arrow[bend left]{uur}{f}  & &
     \arrow{ul}{i_B} \arrow[swap, bend right]{uul}{g} B
   \end{tikzcd}
 \]
 et que $h$ est l'unique morphisme qui le fait commuter : on laisse cela au soin de la lectrice ou du lecteur. 
    \end{proof}
 \begin{exem}[Coproduit d'anneaux]

\begin{prop}
Soient $R$ et $S$  deux anneaux. Leur coproduit dans la catégorie
 des anneaux est la somme directe $R$ \(\oplus \) $S$, dont les
 opérations sont définies composante par composante.
\end{prop} 
 
        \begin{proof}
            Soit $Z$ = $R$ \(\oplus \) $S$ la somme directe de $R$ et $S$ munie des injections \( i : R \rightarrow Z \) et \(j : S \rightarrow Z \) définies par l'inclusion de $R$ et $S$ dans $Z$. 
            Pour tout objet $X$ et tous morphismes \(f : R \rightarrow X \) et \(g : S \rightarrow X \), il existe un morphisme \(h : Z \rightarrow X \) défini comme suit  $h(r,0) = f(r)$ pour r \( \in R \)  , et $h(0,s) = g(s)$ pour s \(\in S \).

        Supposons qu'il existe un autre morphisme \(h' : A \oplus B \rightarrow X \) tel que $h'(r,0) = f(r)$ et $h'(0,s) = g(s)$ . Par la propriété de la somme directe, les valeurs de $h$ sont entièrement déterminées par ces assignations. Ainsi, $h'=h$ , montrant l'unicité du morphisme.
        \end{proof}
   \end{exem}
   
\subsubsection{Propriétés des coproduits}
   
Le coproduit étant la notion duale au produit, on retrouve le même type de propriétés que ce dernier. 
   
\begin{lemm}\label{lem:ProdCoprod}
Soient $A$ et $B$ deux objets d'une catégorie $\mathcal{C}$. Un triplet $(Q, i_A, i_B)$ est 
un coproduit de $A$ et de $B$ dans la catégorie $\mathcal{C}$ si et seulement si 
le triplet $\left(Q, {i_A}^{\mathrm{op}}, {i_B}^{\mathrm{op}}\right)$ est produit de $A$ et de $B$ dans la catégorie opposée $\mathcal{C}^{\mathrm op}$. 
\end{lemm}   

\begin{proof}
La démonstration est automatique avec les définitions respectives de produit et de coproduit.
\end{proof}   
   
\begin{prop}[Stabilité à isomorphisme près]
 Soit $\mathcal{C}$  une catégorie arbitraire. Soient $A, B$ des objets de $\mathcal{C}$,  
 et soit $(Q, i_{A}, i_{B})$ un coproduit de  $A$ et $B$.
  Si $K$ est un objet de $\mathcal{C}$ muni d'un isomorphisme $\rho :
  Q\xrightarrow{\cong} K$, alors $K$ est un coproduit dont les injections $j_A$ et $j_B$ sont
  d\'efinies comme suit:
  \[
    \begin{tikzcd}[row sep=2em]
    & K  & \\
    Q  \arrow[swap]{ur}{\rho} & & Q \arrow{ul}{\rho} \\
    A \arrow[swap]{u}{i_A}& & B. \arrow{u}{i_B}
  \end{tikzcd}
  \]
\end{prop}

\begin{proof}
C'est un corollaire de la proposition~\ref{prop:ProdStabIso} en passant à la catégorie duale par le lemme~\ref{lem:ProdCoprod}. 
\end{proof}   

\begin{prop}
Deux coproduits d'une paire d'objets d'une catégorie sont isomorphes. 
\end{prop}

\begin{proof}
La démonstration s'obtient en passant à la catégorie opposée grâce au lemme~\ref{lem:ProdCoprod} et en appliquant la proposition~\ref{prop:ProdIso}. 
\end{proof}

\begin{prop}
Si $A, B$ sont des objets d'une catégorie $\mathcal{C}$  qui admettent un coproduit $(A \sqcup B,
 i_A, i_B)$, alors $(A \sqcup B, i_B, i_A)$ est un coproduit de $B$ et $A$. 
\end{prop}

\begin{proof}   
De la même manière, la démonstration s'obtient en passant à la catégorie opposée grâce au lemme~\ref{lem:ProdCoprod} et en appliquant la proposition~\ref{prop:ComProd}. 
\end{proof}

\subsubsection{Lien avec l'objet initial}
Pour terminer cette section, on d\'efinit la notion d'\emph{objet initial},
qui g\'en\'eralise le singleton dans les ensembles. 

\begin{defi}[Objet initial]
    Dans une \emph{catégorie $\mathcal{C}$}, un objet \( \varnothing \)
    est \emph{initial} s'il existe un unique morphisme 
    \( \varnothing \to X\), 
    pour tout objet $X$ de $\mathcal{C}$.
\end{defi}

\begin{prop}  
Soit $\mathcal{C}$ une cat\'egorie poss\'edant un objet initial
$\varnothing$. Pour tout objet $A$ de $\mathcal{C}$, 
 le coproduit de $A$ avec $\varnothing$ existe et est donné par 
\(A \sqcup \varnothing \cong A\).
\end{prop}

\begin{proof}
La démonstration s'obtient en passant à la catégorie opposée grâce au lemme~\ref{lem:ProdCoprod} et en appliquant la proposition~\ref{prop:ProdTerm}. 
\end{proof}