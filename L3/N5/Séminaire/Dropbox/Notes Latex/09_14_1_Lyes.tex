\subsubsection{Catégories petites et localement petites}

On peut aussi donner une autre définition des catégories en regroupant chaque sous-collection de 
flèches en fixant la source et le but. On remarque aussi que les flèches sont souvent appelées \emph{morphismes} (voire \emph{homomorphismes}).
Soit $f$ une flèche de source $X$ et de but $Y$, on dira que 
$f$ appartient à la collection des morphismes de $X$ dans $Y$, qu'on notera au choix 
\[\operatorname{Flech}_\mathcal{C}(X,Y)=\operatorname{Hom}_\mathcal{C}(X, Y) = \operatorname{Mor}_\mathcal{C}(X, Y) = \mathcal{C}(X, Y).\]

Ceci donne la définition équivalente suivante de catégorie, où il n'y a plus besoin d'applications <<$\mathrm{source}$>> et 
<<$\mathrm{but}$>> !

\begin{defi}[Catégorie (bis)]
Une \emph{catégorie} $\mathcal{C}$ est composée des éléments suivants :
\begin{itemize}[label=\textbullet]
	\item une collection d'\emph{objets}, notée $\mathrm{Obj}_\mathcal{C}$ : ${X}, {Y}, {Z},  \ldots$, 
	\item des collections de \emph{flèches} (ou \emph{morphismes}) pour toute paire d'objets $X$ et $Y$, notées $\mathrm{Hom}_\mathcal{C}(X, Y)$ : $f, g, h, \ldots$,
	\item une \emph{composition} :
        pour toute paire de flèches $f$ de $\operatorname{Hom}_\mathcal{C}(X, Y)$
        et $g$ de $\mathrm{Hom}_\mathcal{C}(Y, Z)$, on a une flèche $g \circ f$
        dans $\mathrm{Hom}_\mathcal{C}(X, Z)$,
\item des \emph{identités} : pour tout objet $X$ de $\mathcal{C}$,  il existe une flèche 
$ \mathrm{id}_X$ dans $\mathrm{Hom}_\mathcal{C}(X, X)$.
\end{itemize}
La composition est \emph{associative} 
\[h\circ (g\circ f) = (h\circ g)\circ f\]
et \emph{unitaire} 
\[f\circ \mathrm{id}_X=f \quad \text{et} \quad \mathrm{id}_Y \circ f=f.\]
\end{defi}

Dans la définition de la notion de catégorie, on utilise des collections d'objets et de morphismes, et non
des ensembles. Cela nous donne plus de flexibilité, et nous évite certaines
restrictions vis-à-vis des ensembles. Néanmoins, il peut arriver que certaines catégories soient constituées d'ensembles. 

\begin{defi}[Petite catégorie]
Une catégorie $\mathcal{C}$ est dite \emph{petite} si ses objets $\mathrm{Obj}_\mathcal{C}$ et ses 
flèches $\mathrm{Flech}_\mathcal{C}$ forment un ensemble.
\end{defi}

\begin{exem}
La catégorie $\mathbb{1}$ composée d'un seul objet $\mathrm{Obj}_{\mathbb{1}}\coloneq \{\bullet\}$ 
et d'une seule flèche $\mathrm{Flech}_{\mathbb{1}}\coloneq \{\to\}$ munie de la composition et de l'identité évidentes est une petite catégorie.
\[
\begin{tikzcd}
    \bullet 
    \ar[loop, ->, in=130,out=40,looseness=10]
\end{tikzcd}
\]
\end{exem}

\begin{contrex}
La catégorie $\mathcal{Ens}$ des ensembles  n'est pas une petite catégorie. En
effet, l'ensemble des ensembles n'existe pas (cf. paradoxe de Russel).
La catégorie des espaces vectoriels et des corps ne forment pas non plus de
petites catégories, parce qu'il n'existe pas d'ensemble de tous les espaces
vectoriels, ni d'ensemble de tous les corps, on doit donc se limiter aux
collections.
\end{contrex}

Néanmoins, limiter les objets aux ensembles n'est pas toujours ce que l'on
recherche et peut donc se révéler trop restrictif, comme le montre les exemples ci-dessus. 
 Nous proposons donc plutôt de limiter nos collections de morphismes aux ensembles.

\begin{defi}[Catégorie localement petite]
Une catégorie est dite \emph{localement petite} si, pour toute paire d'objets $X,Y$, les morphismes $\operatorname{Hom}_\mathcal{C}(X, Y)$ de $X$ vers $Y$  forment un ensemble.
\end{defi}

\begin{exem}
La catégorie des ensembles $\mathcal{Ens}$  est localement petite : pour tous ensembles $X,Y$, les applications ensemblistes de $X$ vers $Y$ forment un ensemble  $\operatorname{Hom}_{\mathcal{E}ns}(X, Y)$. 
Il en va de même pour la catégorie $\mathcal{Vect}$ des espaces vectoriels et la catégorie 
$\mathcal{Cor}$ des corps qui sont des petites catégories. 
\end{exem}

\begin{rema} On a peut voir que : 
petite catégorie  $\implies$ catégorie localement petite. Par contre si on ne n'exige uniquement que les objets forment un ensemble, alors, il se peut que la catégorie ne soit pas localement petite comme le montre le contre-exemple suivant : 
\begin{itemize}[label=\textbullet]
    \item $\mathrm{Obj} = \{ \cdot \}$
    \item $\mathrm{Hom}_{\mathcal C}(\cdot, \cdot) = \text{la collection des ensembles}$
    \item $\circ =$ l'union disjointe des ensembles : Pour tous $A$, $B$ des
        ensembles, $A \sqcup B$ est un ensemble, donc fait partie de
        $\mathrm{Hom}_{\mathcal C}(\cdot, \cdot)$
    \item $\mathrm{id}_\cdot = \emptyset$
\end{itemize}

Cette catégorie vérifie les propriétés :
\begin{itemize}
    \item $(A \sqcup B) \sqcup C = A \sqcup (B \sqcup C)$
    \item $A \sqcup \mathrm{id}_\cdot = A \sqcup \emptyset = A$ et
        $\mathrm{id}_\cdot \sqcup B = \emptyset \sqcup B = B$
\end{itemize}

Ici, nos objets forment un ensemble, l'ensemble à un seul élément. Pourtant, les morphismes ne forment pas un ensemble, ce n'est donc pas une catégorie localement petite.
\end{rema}

\subsection{Constructions de catégories}

\subsubsection{Catégories associées à des ensembles partiellement ordonnés}

\begin{defi}[Ensemble partiellement ordonné]
Un \emph{ensemble partiellement ordonné} $\pi=(E, \leq)$ est un ensemble $E$ muni 
d'une relation d'ordre $\le$, c'est-à-dire une relation binaire vérifiant :
\begin{itemize}[label=\textbullet]
    \item \textsc{réflexivité} : $\forall x \in E,\ x \leq x$,
    \item \textsc{transitivité} : $\forall x, y, z \in E,\ x \leq y \wedge y \leq z \Rightarrow x \leq z$,
    \item \textsc{antisymétrie} : $\forall x, y \in E,\ x \leq y \wedge y \leq x \Rightarrow x = y$.
\end{itemize}
\end{defi}


\begin{exem}[Ensemble des parties]
Pour tout ensemble $X$, l'ensemble $E \coloneq \mathcal{P}(X)=\{A\subset X\}$ de ses parties est un ensemble partiellement ordonné  où la relation d'ordre est donnée par l'inclusion 
$\leq\, \coloneq\, \subset$.
\end{exem}

On peut représenter un tel ensemble avec un diagramme, le diagramme de Hasse,
qui met en évidence l'ordre établi dans l'ensemble.


\begin{center}
\begin{tikzpicture}
    \node (X) at (1,3) {X};
    \node (12) at (0,2) {$\{1, 2\}$};
    \node (13) at (1,2) {$\{1, 3\}$};
    \node (23) at (2,2) {$\{2, 3\}$};
    \node (1) at (0,1) {$\{1\}$};
    \node (2) at (1,1) {$\{2\}$};
    \node (3) at (2,1) {$\{3\}$};
    \node (e) at (1,0) {$\emptyset$};

    \draw[->] (e) to (1);
    \draw[->] (e) to (2);
    \draw[->] (e) to (3);
    \draw[->] (1) to (12);
    \draw[->] (1) to (13);
    \draw[->] (2) to (12);
    \draw[->] (2) to (23);
    \draw[->] (3) to (23);
    \draw[->] (3) to (13);
    \draw[->] (12) to (X);
    \draw[->] (23) to (X);
    \draw[->] (13) to (X);
\end{tikzpicture}
\end{center}

On peut associer canoniquement une catégorie à tout ensemble partiellement ordonné.

\begin{defprop}[Catégorie associée à un ensemble partiellement ordonné]
\label{def:cat_epo}
  Soit $\pi = (E, \leq)$ un ensemble partiellement ordonné. Les données suivantes 
    forment une catégorie, notée $\mathcal{C}_\pi$ :   
       \begin{itemize}[label=\textbullet]
        \item $\mathrm{Obj}_{\mathcal{C}_\pi} \coloneqq E~.$
        \item $
            \operatorname{Hom}_{\mathcal{C}_\pi}(e, e')\coloneqq
            \left\{\begin{array}{ll}
            	e \rightarrow e'~, & \text{si } e \leq e'~,\\
                \emptyset~,&  \text{sinon}~.
            \end{array}\right.
        $
        \item La composition est donnée par la transitivité : 
            $\forall e, e', e'' \in E$, tels que $e \rightarrow e'$
            et $e' \rightarrow e''$, on définit leur composée par l'unique flèche $e \rightarrow e''$, qui existe car on a $e \leq e'$ et $e' \leq e''$,
            et par transitivité  $e \leq e''$~.
        \item Les identités sont données par la réflexivité : 
            $\forall e \in E$,  $\mathrm{id}_e : e \rightarrow e$, car $e \leq e$~.
    \end{itemize}
\end{defprop}

\begin{proof}\leavevmode 
    \begin{itemize}[label=\textbullet]
    	\item {\sc Associativité } :
            soient $e, e', e'', e''' \in E$ tels que $e \leq e' \leq e'' \leq e'''$; les deux compositions suivantes sont égales à l'unique flèche de $e$ vers $e'''$: 
	\[(e \rightarrow e' \rightarrow e'') \rightarrow e''' = e \rightarrow e''' =  e \rightarrow (e' \rightarrow e'' \rightarrow e''')~.\]
        \item {\sc Unitaire } :
        soient $e, e' \in E$ tels que $e \leq e'$; les deux compositions suivantes sont égales à l'unique flèche de $e$ vers $e'$: 
	\[(e \xrightarrow{\mathrm{id}_e} e) \rightarrow e' = e \rightarrow e' =  (e \rightarrow e') \xrightarrow{\mathrm{id}_{e'}} e'~.\]
    \end{itemize}
\end{proof}

\subsubsection{Catégorie associée à un graphe}

Les graphes sont des outils centraux en mathématiques et en informatique. 
On peut déjà remarquer une certaine proximité entre les graphes et les catégories : on représente souvent des
catégories au moyen de graphes. On peut aussi créer des graphes à partir d'ensembles partiellement ordonnés via la notion de diagramme de Hasse. 

\begin{defi}[Graphes]\leavevmode
\begin{itemize}[label=\textbullet]
    \item Un \emph{graphe} $G=(S,A)$ est un ensemble $S$ (fini et non vide) de \emph{sommets} 
    et un ensemble $A$ de paires de sommets appelées \emph{arêtes}.
    \item Un graphe $G=(S,A)$ est \emph{dirigé} lorsque toutes ses arêtes sont des paires \emph{ordonnées} de sommets, c'est-à-dire $A\subset S\times S$. 
    \item Un graphe $G$ est dit \emph{complet} lorsqu'il est constitué de toutes les arêtes possibles. 
\end{itemize}
\end{defi}

On ne considérera ici que des graphes dirigés. On peut maintenant se demander quels types de graphes dirigés donnent naissance à des catégories. 

\begin{defi}[Graphe transitifs, graphes réflexifs]\leavevmode
\begin{itemize}[label=\textbullet]
	\item Un graphe dirigé $G=(S,A)$ est \emph{transitif} si  
     $(x, y)\in A$ et $(y, z) \in A$ implique 
    $(x, z) \in A$.
    \item Un graphe dirigé $G=(S,A)$ est \emph{réflexif} si 
    pour tout $x \in S$, on a $(x, x) \in A$.
\end{itemize}
\end{defi}

\begin{defprop}[Catégorie associée à un graphe transitif et réflexif]
Pour tout graphe dirigé $G=(S,A)$ transitif et réflexif, les données suivantes forment une catégorie notée $\mathcal{C}_G$ :
    \begin{itemize}[label=\textbullet]
        \item $\operatorname{Obj}_{\mathcal{C}_G} \coloneqq S$~. 
        \item $\operatorname{Hom}_{\mathcal{C}_G}(s, s') \coloneqq
            \left\{\begin{array}{ll}
            	s \rightarrow s'~, & \text{si } (s, s') \in A~,\\
                \emptyset~,&  \text{sinon}~.
                \end{array}\right.$
        \item La composition est donnée par la transitivité du graphe : 
            soient $s \rightarrow s'$ et
            $s' \rightarrow s''$, on définit leur composée par l'unique flèche $s \rightarrow s''$, qui existe car on a $(s, s') \in A$ et $(s', s'') \in A$, 
            puis  $(s, s'') \in A$  par transitivité  du graphe. 
                \item Les identités sont données par la réflexivité du graphe : 
            $\forall s \in S$, $\mathrm{id}_s : s \rightarrow s$, car $(s, s) \in A$~.
    \end{itemize}
\end{defprop}

\begin{proof}\leavevmode
    \begin{itemize}[label=\textbullet]
    	\item {\sc Associativité } :
            soient $s, s', s'', s''' \in S$ tels que $(s, s') \in A$, $(s', s'') \in A$ et $(s'', s''') \in A$; les deux compositions suivantes sont égales à l'unique flèche de $s$ vers $s'''$: 
	\[(s \rightarrow s' \rightarrow s'') \rightarrow s''' = s \rightarrow s''' =  s \rightarrow (s' \rightarrow s'' \rightarrow s''')~.\]
        \item {\sc Unitaire } :
        soient $s, s' \in S$ tels que $(s, s') \in A$; les deux compositions suivantes sont égales à l'unique flèche de $s$ vers $s'$: 
	\[(s \xrightarrow{\mathrm{id}_s} s) \rightarrow s' = s \rightarrow s' =  (s \rightarrow s') \xrightarrow{\mathrm{id}_{s'}} s'~.\]
    \end{itemize}
\end{proof}

\begin{prop}Il existe une bijection entre les ensembles partiellement ordonnés finis et les graphes dirigés transitifs et réflexifs :
\[
        \text{Ensembles partiellement ordonnés finis}
            \Longleftrightarrow
        \text{Graphes dirigés transitifs et réflexifs}~.
\]
\end{prop}

\begin{proo}
    \begin{itemize}[label=\textbullet]
        \item $\text{Ensemble partiellement ordonné fini} \mapsto  \text{Graphe dirigé transitif et réflexif}$~.         
        Montrons que nous pouvons construire un graphe dirigé transitif et réflexif pour tout ensemble partiellement ordonné fini :
        soit $(E, \leq)$ un ensemble partiellement ordonnée fini, on considère le graphe dirigé $G \coloneqq (E, A)$ où  et $A = \{(s, s') \in E^2, e \leq e'\}$~. 
Un tel graphe est transitif, car $\forall (s, s'), (s', s'') \in A$, on a $(s, s'') \in A$ : si $s \leq s'$ et $s' \leq s''$, alors $s \leq s''$ par transitivité. Et, ce graphe est réflexif, car $\forall s \in E, (s, s) \in A$ :  $s \leq s$ par réflexivité.
        \item $\text{Graphe dirigé transitif et réflexif} \mapsto \text{Ensemble partiellement ordonné fini}$~.              
        Montrons que nous pouvons construire un ensemble partiellement ordonné pour tout graphe dirigé transitif et  réflexif. 
        Soit $G = (S, A)$ un graphe dirigé transitif et réflexif, on considère l'ensemble $(S, \leq)$ des sommets muni de la relation définie par $e \leq e'$ si $(e, e') \in A$. Cette relation est réflexive, c'est-à-dire $\forall e \in S$, $e \leq e$, car $(e, e) \in A$ par réflexivité du graphe. Cette relation est aussi transitive, c'est-à-dire $\forall e, e', e'' \in E$ tels que $e \leq e'$ et $e' \leq e''$, on a $e \leq e''$ : en effet, $(e, e'), (e', e'') \in A$ implique $(e, e'') \in A$ par transitivité du graphe. Enfin, cette relation $\leq$ est antisymétrique, car le graphe est simple, donc, s'il existe $e, e' \in S$  tels que $e \leq e'$, cela signifie que $(e, e') \in A$, et le seul cas où $(e', e) \in A$ (c'est-à-dire $e' \leq e$) est celui où $e = e'$.
    \end{itemize}
\end{proo}

\begin{rema}
On vient de voir qu'il y a équivalence entre les ensembles partiellement ordonnés finis et les graphes dirigés transitifs et réflexifs. Cette équivalence est compatible avec les constructions respectives de catégories : 
\[\begin{tikzcd}
\text{Ensembles partiellement ordonnés finis}  \arrow[rr, Leftrightarrow] \arrow[rd] && \text{Graphes dirigés transitifs et réflexifs} \arrow[ld]\\
 &        \text{Catégories}&
\end{tikzcd}\]

    Nous avons donc réussi à englober les ensembles partiellement ordonnées
    et les graphes dirigés transitifs et réflexifs dans les catégories. Après tout,
    nous avons schématiquement représenté plusieurs de nos catégories par
    des graphes et des ensembles partiellement ordonnés.
\end{rema}
