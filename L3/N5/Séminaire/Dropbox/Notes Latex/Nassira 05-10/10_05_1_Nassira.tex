\subsection{Relation monomorphismes-épimorphismes-isomorphismes}

\Bruno{Section à changer de place : à mettre dans la section 2.5 \emph{à la fin}}

\begin{prop}
Tout isomorphisme d'une catégorie est à la fois un monomorphisme et un épimorphisme.
\end{prop}

\begin{proo}
C'est un corollaire direct des propositions \ref{prop:monofort} et \ref{prop:epifort} : un isomorphisme est un monomorphisme fort et un épimorphisme fort. 
\end{proo}

\begin{rema}
La réciproque est en général fausse, voici un contre-exemple. 
On considère l'application <<identité>> $Id \colon (\mathbb{R}, \mathrm{P}(\mathbb{R})) \to (\mathbb{R}, \mathrm{T})$ qui envoie un nombre réel sur lui-même mais où on considère la topologie discrète $\mathrm{P}(\mathbb{R})$ à la source et  de la topologie <<usuelle>> $\mathrm{T}$ au but. 

Pour rappel, la catégorie  $\mathcal{Top}$ des espaces topologiques est composée des données suivantes : 
\begin{itemize}[label=\textbullet]
  \item les objets $\mathrm{Obj}_{\mathcal{Top}}$ sont les espaces topologiques,
  \item les flèches $\mathrm{Flech}_{\mathcal{Top}}$ sont les applications continues, c'est-à-dire les applications ensemblistes $ f : (X, \mathrm{S}) \rightarrow (Y, \mathrm{T})$ qui vérifient : 
$\forall O\in \mathrm{T} \ \text{ouvert de}\  Y, f^{-1}(O)\in \mathrm{S}\  \text{est un ouvert de}\  X$~.
\end{itemize}

Par définition, l'application <<identité>> $Id$ est continue; comme elle est injective et surjective, c'est un monomorphisme et un épimorphisme. Si elle admettait un morphisme inverse, cela serait une application continue
$g \colon (\mathbb{R}, \mathrm{T}) \to (\mathbb{R}, \mathrm{P}(\mathbb{R}))$. 
\[
\begin{tikzcd}
(\mathbb{R}, \mathrm{P}(\mathbb{R})) \arrow[r, shift left=0.75ex, "Id"] \arrow[r, shift right=0.75ex, "g", swap, leftarrow] &
(\mathbb{R}, \mathrm{T})
\end{tikzcd}
\]
Mais le fait d'être l'inverse de l'application <<identité>> impliquerait que $g(x)=x$ pour tout $x\in \mathbb{R}$. Or, cette application là n'est pas continue car, par exemple, $g^{-1}(]0,1]) = ]0,1]$. 
Or, on a bien que $]0,1]$ est un ouvert pour la topologie discrète de $\mathbb{R}$, mais ce n'est pas un ouvert pour la topologie usuelle de $\mathbb{R}$.
\end{rema}

\begin{rema}
Dans la catégorie $\mathcal{Top}$ des espaces topologiques, les isomorphismes sont les \emph{homéomorphismes}, c'est-à-dire les applications continues $f : X \to Y$ qui vérifient :
\[\exists g : Y  \rightarrow X\ \text{continue}~,\  
f\circ g=\mathrm{id}_Y \ \text{et}\ g\circ f=\mathrm{id}_X~.\]
\end{rema}
           

\begin{prop}
Dans une catégorie concrète, tout morphisme surjectif est un épimorphisme et tout morphisme injectif est un monomorphisme. 
\end{prop}

\begin{proo}
On rappelle que nous n'avons pas (encore) de définition formelle de <<catégorie concrète>>. Regardons tout de même ce qui se passe lorsque les morphismes d'une catégorie $\mathcal{C}$ sont des applications ensemblistes avec des propriétés supplémentaires. 

Soit $f : X \to Y \in \mathrm{Flech}_\mathcal{C}$ un morphisme dont son application sous-jacente est surjective.
Soient $k_1, k_2 : Y \rightarrow Z$ deux morphismes de $\mathcal C$ tels que $k_1\circ f=k_2\circ f$.
On cherche à montrer que $k_1=k_2$, c'est-à-dire $k_1(y)=k_2(y)$, pour tout $y \in Y$.
Soit $y \in Y$, il existe  $x \in X$ tel que $f(x)=y$ car $f$ est surjective. 
Or $k_1(y)=k_1(f(x))=k_2(f(x))=k_2(y)$, donc $k_1=k_2$.
On a bien que $f$ est un épimorphisme.

Soit $f : X \to Y \in \mathrm{Flech}_\mathcal{C}$ un morphisme dont son application sous-jacente est injective.
Soient $k_1, k_2 : Z \rightarrow X$ deux morphismes de $\mathcal C$ tels que $f \circ k_1\circ =f \circ k_2$.
On cherche à montrer que $k_1=k_2$, c'est-à-dire $k_1(z)=k_2(z)$, pour tout $z \in Z$.
Pour tout $z \in Z$, on a $f\circ k_1(z)=f\circ k_2(z)$, c'est-à-dire   $f(k_1(z)) = f(k_2(z))$
Par injectivité de $f$, on obtient $k_1(z)=k_2(z)$, pour tout $z\in Z$. 
On a donc $k_1 = k2$ et $f$ est un monomorphisme.
\end{proo}

\begin{contrex} Attention la réciproque est fausse en général; nous donnons ici un exemple d'épimorphisme qui n'est pas surjectif.
On se place dans la catégorie $\mathcal{Top}$ des espaces topologiques. Soit $f : \mathbb{Q} \to \mathbb{R}$ l'application continue qui injecte $\mathbb{Q}$ dans $\mathbb{R}$ (munis respectivement de leurs topologies usuelles). 
Soient $k_1, k_2  : \mathbb{R} \rightarrow Z$ deux applications telles que $k_1\circ f=k_2\circ f$.
Comme $f(\mathbb{Q})=\mathbb{Q}$ et que $\mathbb{Q}$ est dense dans $\mathbb{R}$, 
les deux  applications continues $k_1$ et $k_2$ qui sont égales sur $\mathbb{Q}$ le  sont également sur $\mathbb{R}$ tout entier. On a bien que le morphisme $f$ un épimorphisme. 
Prenons maintenant $y=\sqrt{3}$ par exemple, comme $y$ n'a aucun antécédent par $f$, l'application $f$ n'est pas surjective.
\end{contrex}



\subsection{Caractérisation des monomorphismes, épimorphismes et isomorphismes dans les catégories concrètes}


\begin{prop}
Soit $\mathcal{C}$ une catégorie localement petite, les propositions suivantes sont équivalentes. 
\begin{enumerate}
    \item[i)] Un morphisme $f : X \rightarrow Y$ est un monomorphisme.
    \item[ii)] Pour tout objet $C$ de $\mathcal{C}$, l'application <<poussé-en-avant>>
    $f_*: \mathcal{C}(C,X) \rightarrow \mathcal{C}(C,Y)$ définie par 
\[(g: C \rightarrow X) \stackrel{f_*}\longmapsto (f\circ g: C \rightarrow Y)\]
    est injective. 
\end{enumerate}
\end{prop}

\begin{proo}
\begin{description}
\item[$i) \implies ii)$] Supposons que $f : X\rightarrow Y$ est un monomorphisme. 
Soit $C$ un objet de $\mathcal{C}$ et soient $g1, g2 : C\rightarrow X$ deux applications.
Considérons l'application <<poussé-en-avant>> $f_*$ associée à $f$.
Montrons qu'elle est injective.
Supposons que $f_*(g1)=f_*(g2)$.
Ce qui est équivalent à dire que $f\circ g1 =f\circ g2 \implies g1=g2$, car $f$ est un monomorphisme.
Donc, $f_*$ est injective.

\item[$ii) \implies i)$] Soit $C$ un objet de $\mathcal{C}$.
On suppose que l'application <<poussé-en-avant>> $f_*$ associée à un morphisme $f : X\rightarrow Y$ est injective. 
    Montrons que $f$ est un monomorphisme. 
    Soient $g1, g2 : C\rightarrow X$ deux applications.
    Supposons que $f(g1)=f(g2)$.
    Par définition de $f_*$ : $f_*(g1)=f\circ g1$ et $f_*(g2)=f\circ g2$ .
    Et par injectivité de $f_*$, $g1=g2$ .
    On bien $f$ un monomorphisme.
\end{description}
\end{proo}


\begin{prop}
Soit $\mathcal{C}$ une catégorie localement petite, les propositions suivantes sont équivalentes. 
\begin{itemize}
    \item[i)] Un morphisme $f : X \rightarrow Y$ est un épimorphisme.
     \item[ii)] Pour tout objet $C$ de $\mathcal{C}$, l'application <<tiré-en-arrière>>
    $f^*: \mathcal{C}(Y,C) \rightarrow \mathcal{C}(X,C)$ définie par 
\[(g: Y \rightarrow C) \stackrel{f^*}\longmapsto (g\circ f : X \rightarrow C)\]
    est injective. 

\end{itemize}
\end{prop}

\begin{proo}
\begin{description}
\item[$i) \implies ii)$] Supposons que $f : X\rightarrow Y$ est un épimorphisme. 
Soit $C$ un objet de $\mathcal{C}$ et soient $g1, g2 : Y\rightarrow C$ deux applications.
Considérons l'application <<tiré-en-arrière>>
    $f^*$ associée à $f$.
Montrons qu'elle est injective.
Supposons $f^*(g1)=f^*(g2)$.
Ce qui est équivalent à dire que $g1\circ f =g2\circ f \implies g1=g2$, car $f$ est un épimorphisme.
On a bien trouvé $f^*$ injective.

\item[$ii) \implies i)$] Soit $C$ un objet de $\mathcal{C}$.
On suppose que l'application <<tiré-en-arrière>> $f^*$ associée à un morphisme $f : X\rightarrow Y$ est injective. 
    Montrons que $f$ est un épimorphisme. 
    Soient $g1, g2 : Y\rightarrow C$ deux applications.
    Supposons que $g1(f)=g2(f)$.
    Ce qui est équivalent à dire que $f^*(g2)=f^*(g2)$ .
    Par injectivité de $f^*$, $g1=g2$ .
    On bien $f$ un épimorphisme.
\end{description}
\end{proo}

\begin{prop}
Soit $\mathcal{C}$ une catégorie localement petite, les propositions suivantes sont équivalentes. 
\begin{itemize}
    \item[i)] Un morphisme $f : X \rightarrow Y$ est un isomorphisme.
    
    \item[ii)] Pour tout objet $C$ de $\mathcal{C}$, l'application <<poussé-en-avant>>
    $f_*: \mathcal{C}(C,X) \rightarrow \mathcal{C}(C,Y)$ définie par 
\[(g: C \rightarrow X) \stackrel{f_*}\longmapsto (f\circ g: C \rightarrow Y)\]
    est bijective. 
\end{itemize}
\end{prop}

\begin{proo}
\begin{description}
\item[$i) \implies ii)$]
Soit $f: X \rightarrow Y$ un isomorphisme, c'est -à-dire $\exists g : Y \rightarrow X$ tel que $g\circ f=\mathrm{id}_X$ et $f \circ g=\mathrm{id}_Y$.
Soit $C$ un objet de $\mathcal{C}$, montrons que l'application <<poussé-en-avant>> $f_*: \mathcal{C}(C,X) \rightarrow \mathcal{C}(C,Y)$ est bijective. Pour cela, on affirme que l'application <<poussé-en-avant>> $g_*$ associée au morphisme $g$ est l'inverse de $f_*$~. En effet, pour tout $h : C\rightarrow X$, on a 
$\left(g_*\circ f_*\right)(h)=g\circ f \circ h = h$ et, pour tout $j : C\rightarrow Y$,  
on a $\left(f_*\circ g_*\right)(j)=f\circ g \circ j=j$. Donc $f_*$ est bijective.

\item[$ii) \implies i)$] Supposons que l'application <<poussé-en-avant>> $f_*$ associée à un morphisme 
$f : X \rightarrow Y$ soit bijective et montrons que $f$ est un isomorphisme.
On cherche un morphisme $g : Y \rightarrow X$ de $\mathcal C$ tel que $f\circ g=\mathrm{id}_Y$ et $g\circ f=\mathrm{id}_X$~.
Considérons $C = Y$, comme l'application  $f_* : \mathcal{C}(Y,X) \rightarrow \mathcal{C}(Y,Y)$ est bijective, elle est en particulier surjective et donc il existe un morphisme $g : Y\rightarrow X$ tel que $f_*(g)= \mathrm{id}_Y$, c'est-à-dire tel que $f\circ g=\mathrm{id}_Y$.
Il reste à montrer que $g\circ f=\mathrm{id}_X$. Pour cela, on commence par remarquer que 
$f_*(g\circ f)=f\circ (g\circ f)=(f\circ g)\circ f=\mathrm{id}_Y\circ f=f$~.
En outre, on a $f_*(\mathrm{id}_X)=f\circ \mathrm{id}_X =f$.
Et comme l'application  $f_* : \mathcal{C}(X,X) \rightarrow \mathcal{C}(X,Y)$ est bijective, elle est en particulier injective, ce qui implique $g\circ f=\mathrm{id}_X$.
En conclusion, le morphisme $f$ est bien un isomorphisme.
\end{description}
\end{proo}
