\subsection{Les cat\'egories en informatique}

Les cat\'egories sont utilis\'ees en informatique pour les m\^emes
raisons qu'en math\'ematiques : on veut prendre du recul sur les
structures que l'on utilise (en programmation, en logique, en
th\'eorie des automates, \emph{etc.}) et comprendre ce qui les relie,
de mani\'ere abstraite et axiomatique. Parfois, l'influence des
cat\'egories se retrouve directement dans les outils informatiques :
en programmation fonctionnelle on parle explicitement de foncteurs et
de monades ! Dans cette section, on voit seulement quelques exemples de
base. 

\begin{exem}[Une catégorie de types et de programmes en OCaml]
  En OCaml (par exemple), on peut voir un programme comme un morphisme
  allant d'un type <<entr\'ee>> \`a un type <<sortie>>. On aurait envie de
  construire une cat\'egorie, mais c'est difficile \`a formaliser
  math\'ematiquement. En effet, pour composer deux programmes, on va
  utiliser une op\'eration telle que
\begin{verbatim}
(* Composition *)
let compose (f : 'a -> 'b) (g : 'b -> 'c) (x : 'a) : 'c = g (f x)
\end{verbatim}
  et l'identit\'e pourrait \^etre donn\'ee par
\begin{verbatim}
(* Élément neutre *)
let neutral_element (x : 'a) : 'a = x
\end{verbatim}
Probl\`eme : si on compose \texttt{g : int -> bool} avec l'identit\'e, 
on obtient un programme qui envoie \texttt{x : 'int} sur 
\begin{verbatim}
neutral_element (g x). 
\end{verbatim}
Ce n'est pas le m\^eme programme que $g$, bien qu'ils se comportent de
la m\^eme fa\c con sur toutes les entr\'ees. 
\end{exem}
Pour \'eviter le probl\`eme de l'exemple pr\'ec\'edent, on pourrait
consid\'erer les programmes modulo une relation d'\'equivalence :
identifie deux programmes qui se comportent de la m\^eme fa\c con sur
toutes les entr\'ees. Mais souvent, il est plus naturel et
int\'er\'essant de chercher des mod\`eles math\'ematiques 
repr\'esentant les programmes de mani\'ere plus abstraite. Par
exemple, pour un langage tr\'es simple qui ne permet que d'appeler
\texttt{print} et des fonctions ensemblistes (disons, \texttt{+} et \texttt{*}
sur les entiers, \texttt{\&} et \texttt{||} sur les bool\'eens), on peut
consid\'erer la cat\'egorie suivante.
\begin{exem}
  \label{ex:print}
Soit $\mathrm{string}$ l'ensemble des cha\^\i nes de caract\`eres. La catégorie $\mathcal{Ens_{\text{print}}}$ est définie comme suit :
\begin{itemize}
    \item Objets : les m\^emes objets que $\mathcal{Ens}$,
      c'est-\`a-dire tous les ensembles.
    \item Flèches (Morphismes) :
      $\mathrm{Hom}_{\mathcal{Ens}_{\mathrm{print}}}(A, B) :=
      \mathrm{Hom}_{\mathcal{Ens}}(A, B \times
      \mathrm{string})$.
    \item Composition : Pour chaque paire de flèches $f: A \to B$ et
      $g: B \to C$, on a des applications $f : A \to B \times
      \mathrm{string}$ et $g : B \to C \times \mathrm{string}$, et
      leur composition est donn\'ee par le morphisme suivant: 
      \[
        \begin{tikzcd}[column sep=3.5em]
          A \arrow[r, "f"] & B \times \mathrm{string} \arrow[r, "g \times \mathrm{id}_{\mathrm{string}}"] & C \times \mathrm{string} \times \mathrm{string} \arrow[r, "\mathrm{id}_C \times \mathrm{concat}"] & C \times \mathrm{string}
        \end{tikzcd}
      \]
    \item Identité : $\mathrm{id}_A: A \to A \times \mathrm{string}$,
      définie par $\mathrm{id}_A(a) = (a, \epsilon)$, o\`u $\epsilon$
      est la cha\^\i ne de caract\`eres vide. 
\end{itemize}
(On a utilis\'e la fonction $\mathrm{concat}: \mathrm{string} \times
\mathrm{string} \to \mathrm{string}$ qui prend deux chaînes de
caractères $s$ et $r$ en entrée et les concaténant pour former la
chaîne $st$. La composition des flèches dans cette catégorie est
associative car la concat\'enation des chaines est associative. De plus, l'identité agit comme l'élément unitaire pour la composition, c'est-à-dire que la composition d'une flèche avec l'identité ne change pas la flèche elle-même.
\end{exem}

Enfin, on observe qu'on peut construire une cat\'egorie \`a partir
d'un automate.
\begin{exem}
Soit $A = (Q, \Sigma, \delta, q_0, F)$ un automate fini déterministe où :
\begin{align*}
Q & : \text{ensemble d'états,} \\
\Sigma & : \text{ensemble de symboles d'alphabet,} \\
\delta & : Q \times \Sigma \to Q \text{ est la fonction de transition,} \\
q_0 & : \text{état initial,} \\
F & : \text{ensemble d'états finaux.}
\end{align*}
On note $\delta^*$ l'extension de $\delta$ qui permet des transitions multiples:
c'est une fonction $Q \times \Sigma^* \to Q$. 
\end{exem}

\begin{defi}[Catégorie induite un automate]
La \emph{catégorie induite par l'automate} $A$ est définie par les données suivantes. 

\begin{itemize}[label=\textbullet]
  \item \textsc{Objets :} les objets de la catégorie sont les états de l'automate $Q$.

  \item \textsc{Flèches :} les flèches de la catégorie
    représentent les transitions définies par la fonction de
    transition $\delta$. Un morphisme de $q$ à $q'$ est un mot $s \in
    \Sigma^*$ tel que $\delta^*(q, s) = q'$. 

  \item \textsc{Composition :} la composition des flèches est définie
    par la concaténation des étiquettes des transitions. Si $f$ est
    une flèche de l'état $q$ à $q'$ avec l'étiquette $s \in \Sigma^*$, et $g$ est
    une flèche de l'état $q'$ à $q''$ avec l'étiquette $t \in
    \Sigma^*$, alors la
    composition $g \circ f$ est la flèche de l'état $q$ à $q''$ ayant 
    l'étiquette $st$. 
    
  \item \textsc{Identité :} Pour chaque état $q$ de l'automate, il
    existe une flèche identité $\text{id}_q : q \to q$ avec
    l'étiquette $\epsilon$.
\end{itemize}
\end{defi}

\subsection{Propriétés des morphismes}

\subsubsection{Isomorphismes}

La notion d'isomorphisme est fondamentale en théorie des
catégories. Elle représente une équivalence spéciale entre deux objets
d'une catégorie. Pour que deux objets, $A$ et $B$, soient considérés
comme isomorphes, il doit exister deux morphismes, $f$ de $A$ vers $B$
et $g$ de $B$ vers $A$, tels que les deux compositions $g
\circ f$ et $f \circ g$ correspondent aux morphismes identit\'e sur
$A$ et $B$, respectivement. Intuitivement, si on va de $A$ à $B$ en suivant $f$, puis de $B$ à $A$ en suivant
$g$, on se retrouve au m\^eme point dans $A$, et inversement quand on
part de $B$. Dans ce cas, $f$ et $g$ sont des morphismes inverses l'un de l'autre.

\begin{defi}[Isomorphisme]
Dans une catégorie $\mathcal{C}$, deux objets $A$ et $B$ sont dits \emph{isomorphes} s'il existe deux morphismes $f : A \to B$ et $g : B \to A$ tels que les compositions $f \circ g$ et $g \circ f$ soient respectivement égales aux morphismes d'identité des objets $A$ et $B$, c'est-à-dire :
\[
f \circ g = \text{id}_A \qquad \text{et} \qquad g \circ f = \text{id}_B~.
\] 
Dans ce cas, on appelle $f$ et $g$ des \emph{isomorphismes}.
\end{defi}

Les \'equations ci-dessus sont repr\'esent\'ees par les
\emph{diagrammes commutatifs} suivants:
\[
\begin{tikzcd}
A \arrow[r, "f"] \arrow[rd, "\mathrm{id}_A"'] & B \arrow[d, "g"] \\
& A
\end{tikzcd}
\qquad
\begin{tikzcd}
B \arrow[r, "g"] \arrow[rd, "\mathrm{id}_B"'] & A \arrow[d, "f"] \\
& B
\end{tikzcd}
\]

\subsubsection{Monomorphismes et applications injectives}

Dans ce qui suit, on introduit la notion de monomorphisme, qui est une
propri\'et\'e de certains morphismes dans une cat\'egorie. L'objectif
est d'exprimer de mani\`ere abstraite ce qui caract\'erise les
applications injectives dans la cat\'egorie des ensembles. On rappelle
la d\'efinition.

\begin{defi}[Application injective]
Soient $A$ et $B$ des ensembles. Une application \(f : A \to B\) est
\emph{injective} si la propriété suivante est satisfaite : \\
\begin{center}
$\forall x, y \in A, \quad f(x) = f(y) \Rightarrow x = y$.
\end{center}
\end{defi}

On cherche maintenant a g\'en\'eraliser cette d\'efinition. On
remarque que, si $A$ est un objet de la cat\'egorie des ensembles,
alors les \'el\'ements de $A$ correspondent pr\'ecis\'ement aux
morphismes $1 \to A$, o\`u $1$ est un ensemble singleton fix\'e. On
peut donc r\'e-exprimer l'injectivit\'e d'une application de la
mani\'ere suivante.

\begin{prop}
Une fl\'eche $f : A \to B$ dans la cat\'egorie $\mathcal{Ens}$ est une
application injective si et seulement si, \'etant donn\'e deux
fl\`eches $x : 1 \to A$ et $y : 1 \to A$, on a
\[
f\circ x= f \circ y \quad \implies \quad x = y. 
  \] 
\end{prop}
\begin{proof}
  Il suffit d'observer que la fl\`eche $f \circ x : 1 \to B$
  correspond bien \`a l'\'element $f(x)$. 
\end{proof}

\'Etant donn\'e une cat\'egorie quelconque, on n'a pas forc\'ement
acc\`es \`a un ensemble singleton qui joue le r\^ole de l'objet
$1$ dans la proposition pr\'ec\'edente. On va voir qu'il suffit de remplacer $1$ par un quantificateur
sur tous les objets $X$ de la cat\'egorie.

\begin{defi}[Monomorphisme]
Un \emph{monomorphisme} (ou simplement un \emph{mono}) est un morphisme $f$ tel que pour tous les objets \(X\)  et tous les morphismes \(g_1\) et \(g_2 : X \to A\), si \(f \circ g_1 = f \circ g_2\), alors \(g_1 = g_2\).
En d'autres termes, si deux morphismes  \(g_1\) et \(g_2\) de \(X\) vers \(A\) donnent le même résultat lorsqu'ils sont composés avec \(f\), alors \(g_1\) et \(g_2\) sont égaux.
\end{defi}

Nous allons maintenant prouver que dans la catégorie des ensembles $\mathcal{Ens}$ , les monomorphismes sont précisément les applications injectives.
\begin{prop}
Dans la catégorie $\mathcal{Ens}$,  toute fl\`eche \(f : A \to B\) vérifie : 
\begin{center}
$f$ est une application injective $\iff$ $f$ est un monomorphisme.
\end{center}
\end{prop}

\begin{proof}\leavevmode

($\Leftarrow$) Supposons que $f : A \to B$ soit un monomorphisme.
Nous voulons montrer que $f$ est injective. Soient $x$ et $y$ deux
éléments distincts de l'ensemble $A$ tels que $f(x) = f(y)$.
Dans la catégorie des ensembles, on appelle $g$ et $h$ les morphismes de
$\{1\}$ vers $A$ d\'efinis par $g(1) = x$ et $h(1) = y$. 
Puisque $f(x) = f(y)$, on a $f \circ g(1) = f \circ h(1)$, et donc $f
\circ g = f \circ h$. Puisque $f$ est un mono, on obtient que $g = h$
et donc que $x = y$.

($\Rightarrow$) Réciproquement, supposons que $f: A \to B$ soit une
fonction injective. Nous devons montrer que $f$ est un
monomorphisme. Soit $X$ un objet quelconque et $h, k: X \to A$ deux
morphismes tels que $f \circ h = f \circ k$. Pour tout
$x \in X$, on a $f ( h(x)) = f (k (x))$ et donc, puisque $f$ est
injective, $h(x) = k(x)$. On en conclut que $h = k$.

Ainsi, dans la catégorie des ensembles $\mathcal{Ens}$, les monomorphismes sont précisément les applications injectives.
\end{proof}

Dans certaines catégories, il existe une notion plus forte que celle des monomorphismes, appelée monomorphisme fort.
\begin{defi}[Monomorphisme fort]
 Un morphisme $f : A \to B$ est un \emph{monomorphisme fort} s'il existe un
 morphisme $g : B \to A$ tel que $g \circ f = \mathrm{id}_A$:
\begin{center}
\begin{tikzcd}
A \arrow[r, "f"] \arrow[rd, "id_A"'] &\  B\  \arrow[d, "g"] \\
& \ A \ . 
\end{tikzcd}
\end{center}
\end{defi}

\begin{prop}\label{prop:monofort}
Tout monomorphisme fort est un monomorphisme. 
\end{prop}
\begin{proof}
Supposons que $f : A \to B$ soit un monomorphisme fort, et considérons
deux morphismes $h$ et $k : X \to A$ tels que $f \circ h = f \circ
k$. On a : 
\begin{align*}
&  f \circ h = f \circ k  \\
& \quad \Rightarrow   g \circ f \circ h = g \circ f \circ k \\
&  \quad \Rightarrow \text{id} \circ h = \text{id} \circ k, 
\end{align*}
et donc $f$ est bien un monomorphisme.
\end{proof}

\begin{defprop}[Sous-catégorie des monomorphismes]\label{defprop:SousCatMono}
Soit $\mathcal{C}$ une catégorie donnée. On peut définir une
sous-catégorie $\mathcal{C}_\text{mono}$ dont les objets sont
les m\^emes que ceux de $\mathcal{C}$, mais dont les morphismes sont
seulement les monomorphismes de $\mathcal{C}$. Les id\'entit\'es et la
composition sont les m\^emes que dans $\mathcal{C}$.
\end{defprop}
\begin{proof}
L'identité d'un objet dans n'importe quelle catégorie est toujours un
monomorphisme. En effet, pour tout objet $X$ et pour tous morphismes
$h, k : X \to A$, si $\mathrm{id}_A \circ h = \mathrm{id}_A \circ k$, alors $h = k$. Par conséquent, l'identité est un morphisme dans la catégorie $\mathcal{C}_\text{mono}$.
    
 Il nous reste \`a montrer que la composition de deux morphismes
 dans $\mathcal{C}_\text{mono}$ est bien d\'efinie, c'est-\`a-dire que
 si $f : A \to B$ et $g : B \to C$ sont deux monomorphismes (dans $\mathcal{C}$), alors $g
 \circ f$ est aussi un monomorphisme.
Considérons deux morphismes $h : X \to A$ et $k : X \to A$ tels que
$(g \circ f) \circ h = (g \circ f) \circ k$. En utilisant
l'associativit\'e de la composition, nous obtenons $g \circ (f \circ
h) = g \circ (f \circ k)$. Puisque $g$ est un monomorphisme, nous
pouvons conclure que $f \circ h = f \circ k$. Et puisque $f$ est également un monomorphisme, cela implique que $h = k$.
\end{proof}

Pour terminer, on observe la propri\'et\'e suivante: 

\begin{prop}
Si $\pi = (E, \leq)$ un ensemble partiellement ordonn\'e, alors \emph{tous} les morphismes de
la catégorie associée 
$\mathcal{C}_\pi$ sont des monomorphismes.
\end{prop}

\begin{proof}
On rappelle (\emph{cf.}~D\'efinition~\ref{def:cat_epo}) qu'il existe
au plus un morphisme entre deux objets de $\mathcal{C}_\pi$. Donc,
pour tout morphisme $f : x \to y$, la condition \`a v\'erifier pour
montrer que $f$ est un monomorphisme est triviale.
\end{proof}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "Categories"
%%% End:
