\section{Exercices}

Voici une liste d'exercices, avec certaines corrections, qui permettent de bien appréhender les différentes notions vues dans le corps du texte.

\begin{exer}
    Montrer que, dans toute catégorie, si un morphisme $f : A \rightarrow B$ a un inverse $g : B \rightarrow A$, alors cette inverse est unique.
\end{exer}

\begin{correc}
    Soient $\mathcal{C}$ une catégorie, $A$ et $B$ deux objets de $\mathcal{C}$ et $f : A \rightarrow B$ un morphisme inversible dans $\mathcal{C}$, c'est-à-dire qu'il existe 
 un morphisme $g : B \rightarrow A$ de $\mathcal{C}$ tel que $g \circ f = \mathrm{id}_A$ et $f \circ g = \mathrm{id}_B$.
    Supposons qu'il existe un morphisme $g' : B \rightarrow A$ de $\mathcal{C}$ vérifiant aussi $g' \circ f = \mathrm{id}_A$ et $f \circ g' = \mathrm{id}_B$. On a alors que $g \circ f = g' \circ f = \mathrm{id}_A$ implique 
   \[g= g \circ (f \circ g)= (g \circ f) \circ g = (g' \circ f) \circ g= g' \circ (f \circ g)=g'~.\]
\end{correc}

\begin{exer}\leavevmode
\begin{enumerate}
\item     Définir deux catégories telles que la notion de la différentielle (du cours d'analyse) soit un foncteur entre elles.
\item     Faire la même question en rempla\c cant la différentielle par la matrice jacobienne. 
\end{enumerate}
\end{exer}

\begin{correc}\leavevmode
\begin{enumerate}
\item 
On considère d'abord la catégorie $\mathcal{Diff}$ dont les objets sont les paires $(V,a)$ formées d'un espace 
vectoriel normé $V$ et d'un élément $a\in V$ et dont les morphismes, c'est-à-dire les éléments de 
$\mathrm{Hom}_{\mathcal{Diff}}\big((V,a), (W,b)\big)$, sont les applications $f \colon V \to W$ différentiables en $a$ et telles que $f(a)=b$. 
On considère ensuite la catégorie $\mathcal{Cont}$ dont les objets sont les espaces vectoriels normés $V$ et 
dont les morphismes 
$\mathrm{Hom}_{\mathcal{Cont}}(V,W)$, sont les applications linéaires continues $\varphi \colon V \to W$.  
On laisse au soin de la lectrice ou du lecteur de vérifier les axiomes des catégories  $\mathcal{Diff}$ et $\mathcal{Cont}$, ce qui est assez automatique. 

Le foncteur <<différentielle>> $\mathrm{D} \colon \mathcal{Diff} \to \mathcal{Cont}$ envoie une paire $(V,a)$  d'un espace vectoriel normé $V$ et d'un élément $a\in V$ sur son espace vectoriel normé $V$ sous-jacent et une application différentiable $f \colon (V,a) \to (W,b)$ sur sa différentielle 
$\mathrm{D}_a f \colon V \to W$ en $a$. Le fait que la différentielle de l'identité est égale à l'identité et la propriété 
de chaînes 
\[\mathrm{D}_a (g \circ f)=\mathrm{D}_{f(a)} g  \circ \mathrm{D}_a f\]
impliquent qu'il s'agit bien d'un foncteur. 

\item Dans le cas du jacobien, on considère la sous-catégorie $\mathcal{Diff}_{\mathcal{fini}}$ des 
espaces vectoriels normés \emph{de dimension finie} munis d'un élément et  
la catégorie $\mathcal{Mat}$ des matrices.
Le foncteur <<matrice jacobienne>> $\mathrm{J} \colon \mathcal{Diff}_{\mathcal{fini}} \to \mathcal{Mat}$ envoie une paire $(V,a)$  sur l'objet $\dim V$ de $\mathcal{Mat}$ et une application différentiable $f \colon (V,a) \to (W,b)$ sur sa 
matrice jacobienne
$\mathrm{J}_a f \in \mathrm{Mat}_{\dim W, \dim V}$ en $a$. Le fait que la matrice jacobienne de l'application identité est égale à la matrice identité et la propriété 
de chaînes 
\[\mathrm{J}_a (g \circ f)=\mathrm{J}_{f(a)} g  \times \mathrm{J}_a f\]
impliquent qu'il s'agit bien d'un foncteur. 
\end{enumerate}    
\end{correc}

\begin{exer}\label{exer:FoncIso}
    Est-ce qu'un foncteur préserve les monomorphismes, les épimorphismes,  les isomorphismes ?
\end{exer}

\begin{correc}\leavevmode
    \begin{description}
        \item[\it Monomorphismes] Un foncteur ne preserve pas les monomorphismes en général, en voici un contre-exemple. 
        On considère la catégorie $\mathcal{C}$ formée de deux objets $2$ et $3$, d'un seul morphisme $\alpha \colon 2 \to 3$ et des deux morphismes identités. 
                On considère la catégorie $\mathcal{D}$ formée de trois objets $A$, $B$ et $C$, 
                de deux morphismes $f \colon A \to B$ et $g \colon A \to B$, d'un morphisme $h \colon B \to C$, d'un morphisme $k \colon A \to C$, vérifiant $k=h \circ f=h \circ g$, et des morphismes identités. De ces définitions, on voit que $\alpha$ est un monomorphisme de la catégorie $\mathcal{C}$ et que 
                $h \colon B \to C$ n'est pas un monomorphisme de la catégorie $\mathcal{D}$. Il suffit enfin de voir que le foncteur $\mathrm{F} \colon \mathcal{C} \to \mathcal{D}$ qui envoie $2$ sur $B$, $3$ sur $C$ et $\alpha$ sur $h$ est bien défini. 
		
        \item[\it \'Epimorphismes] Un foncteur ne préserve pas les épimorphismes en général, en voici un contre-exemple. On procède de la même manière qu'au-dessus mais avec la catégorie $\mathcal{C}$ formée de deux objets $1$ et $2$, d'un seul morphisme $\alpha \colon 1 \to 2$ et des deux morphismes identités et avec la catégorie 
        $\mathcal{D}$ formée de trois objets $A$, $B$ et $C$, 
                de deux morphismes $f \colon B \to C$ et $g \colon B \to C$, d'un morphisme $h \colon A \to B$, d'un morphisme $k \colon A \to C$, vérifiant $k=f \circ h=g \circ h$, et des morphismes identités. On voit alors que le morphisme $h$ n'est pas un épimorphisme et  le foncteur $\mathrm{F} \colon \mathcal{C} \to \mathcal{D}$ qui envoie $1$ sur $A$, $2$ sur $B$ et $\alpha$ sur $h$ est bien défini. 
        
        \item[\it Isomorphismes] Un foncteur préserve les isomorphismes, en voici une démonstration.  
        Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories et $\mathrm{F} \colon \mathcal{C}\to \mathcal{D}$ un foncteur entre elles. Soit  $f : A \rightarrow B$ un isomorphisme de $\mathcal{C}$; il existe donc un morphisme $g : B \rightarrow A$ tel que $f \circ g = \mathrm{id}_B$ et $g \circ f = \mathrm{id}_A$ dans $\mathcal{C}$. Alors on a aussi 
        \[\mathrm{F}(g) \circ {\mathrm{F}(f)} = \mathrm{F}(g \circ f) = \mathrm{F}(\mathrm{id}_A) = \mathrm{id}_{\mathrm{F}(A)}\] et 
        \[\mathrm{F}(f) \circ \mathrm{F}(g) = \mathrm{F}(f \circ g) = \mathrm{F}(\mathrm{id}_B) = \mathrm{id}_{F(B)}~.\]
        Ainsi $\mathrm{F}(f)$ est lui aussi un isomorphisme. 
    \end{description}
\end{correc}

\begin{exer}
    Décrire les foncteurs entre catégories associées à des ensembles partiellement ordonnés.
\end{exer}

\begin{correc}
    Soient $X$ et $Y$ deux ensembles munis de deux ordres partiels  $\pi_X := (X, \leq)$ et $\pi_Y := (Y, \preceq)$. La donnée d'un foncteur $\mathrm{F} : \mathcal{C}_{\pi_X} \to \mathcal{C}_{\pi_Y}$ entre les catégories associées est équivalente à la donnée d'une application ensembliste $f : X \to Y$ croissante : $x \leq y \implies \mathrm{F}(x) \preceq\mathrm{F}(y)$. 
\end{correc}

\begin{exer}
    Montrer que l'assignement qui envoie un espace topologique sur l'ensemble de ses ouverts ordonnés par inclusion définit un foncteur $\mathcal{Top^{op}} \rightarrow \mathcal{EPO}$, où $\mathcal{EPO}$, est la catégorie dont les objets sont les ensembles partiellement ordonnés, et les morphismes sont les applications monotones. L'image d'une application continue $f : X \to Y$ est l'application $O\subset Y \mapsto f^{-1}(O)$. 
\end{exer}

\begin{exer}
		Soit ${\mathcal{C}}$ une catégorie localement petite et soit $A$ un objet de  ${\mathcal{C}}$. On considère  le foncteur contravariant $\mathrm{Hom}_{\mathcal{C}}(-,A)$ de la catégorie $\mathcal{C}$ dans celle des ensembles $\mathcal{Ens}$ qui associe à tout objet  $X$ de $\mathcal{C}$ l'ensemble des flèches $\mathrm{Hom}_{\mathcal{C}}(X,A)$ et à tout morphisme $f : X \to Y$ de $\mathcal{C}$ l'application ensembliste
\[f^{*} \colon \mathrm{Hom}_{\mathcal{C}}(Y,A) \to \mathrm{Hom}_{\mathcal{C}}(X,A)\ , \quad g \mapsto g\circ{f}~.\]
\begin{enumerate}
\item Vérifier que l'association $A \mapsto \mathrm{Hom}_{\mathcal{C}}(-,A)$ définit un foncteur $\mathcal{C} \to [\mathcal{C}^{op}, \mathcal{Ens}]$.
\item Soit $f \colon A \to B$ un morphisme de la catégorie $\mathcal{C}$. Vérifier que les applications ensemblistes 
    <<poussées-en-avant>> 
$f_* \colon \mathrm{Hom}_{\mathcal{C}}(X,A) \to \mathrm{Hom}_{\mathcal{C}}(X,B)$ 
définissent une transformation naturelle.
\end{enumerate}
	\end{exer}
	
\begin{exer}
Décrire la catégorie squelétale de la catégorie $\mathcal{Ens_{finis}}$ des ensembles finis.
\end{exer}

\begin{exer}
    Montrer que l'espace vectoriel engendré par $\mathbb{N}$ n'est pas isomorphe à son espace vectoriel dual.
\end{exer}

\begin{exer}
    Est-ce que les foncteurs suivants sont représentables ?  Si oui, par quel objet ?
    \begin{enumerate}
        \item Le foncteur <<Oubli>> des anneaux vers les ensembles qui envoie un anneau sur son ensemble sous-jacent. 
        \item Le foncteur <<Unités>> des anneaux vers les ensembles qui envoie un anneau sur son ensemble des éléments inversibles. 
        \item Le foncteur <<Objets>> des petites catégories vers les ensembles qui envoie une petite catégorie  sur son ensemble d'objets.
        \item Le foncteur <<Flèches>> des petites catégories vers les ensembles qui envoie une petite catégorie sur son ensemble (global) de flèches. 
        \item Le foncteur <<Ensemble des ouverts>> de la catégorie opposée des espaces topologiques vers la catégorie des ensembles qui envoie un espace topologique sur son ensemble d'ouverts. 
        \item Le foncteur <<Parties>> de la catégorie opposée des ensemble vers celle des ensembles qui envoie un ensemble sur l'ensemble de tous ses sous-ensembles (parties). 
    \end{enumerate}
\end{exer}