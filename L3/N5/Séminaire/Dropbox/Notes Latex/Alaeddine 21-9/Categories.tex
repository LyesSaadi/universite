% LaTeX : Séminaire sur les Catégories (DL3 2023)
% Auteur-trices : Hugo Paquet, Bruno Vallette, Adam, Lyes, Corentin, 
% Encodage : UTF 8

\documentclass[twoside, 11pt]{amsart}

\include{package}

%%%%%%%%%%%%%%%%%%%%   Titre et auteur %%%%%%%%%%%%%%%%%%%%

\title{Les cat\'egories}

\dedicatory{``Mal nommer les choses contribue au malheur du monde''' \\  \hfill \textit{Albert Camus}}

\author{Bruno Vallette}
\address{Laboratoire Analyse, G\'eom\'etrie et Applications, Universit\'e Paris 13, Sorbonne Paris Cit\'e, CNRS, UMR 7539, 93430 Villetaneuse, France.}
\email{vallette@math.univ-paris13.fr}

\date{\today}

\begin{document}

\maketitle

\begin{abstract} Notes du séminaire sur les catégories de la classe de Double Licence 3 Mathématiques-Informatique de l'Université Sorbonne Paris Nord 2023-2024.
 \end{abstract}

\setcounter{tocdepth}{2}

\tableofcontents

\section{Introduction : l'algèbre moderne}

\subsection{L'Algèbre ou l'analyse des équations}
Le mot français  <<algèbre>> provient du mot arabe <<al-jabr>> qui apparait
dans le titre du livre  <<Abrégé du calcul par la restauration et la
comparaison>> publié par en 825 par Al-Khwarizmi, mathématicien perse. 
Cet ouvrage est le premier à étudier systématiquement la résolution des
équations du premier et du second degré. Il est intéressant de  noter qu'il se
compose de deux parties : la première contient la théorie abstraite des
équations algébriques où l'accent est mis sur le type d'opérations utilisée, la
seconde contient les diverses applications en vue à l'époque comme les calculs
d'héritage, d'arpentage ou de commerce. Le mot arabe <<al-jabr>>, qui signifie
<<la réduction>> au sens de <<la réduction d'une fracture>>, est la terminologie
choisie par Al-Khwarizmi pour une des opérations sur les équations, celle  qui
consiste à réduire une équation en ajoutant des termes, soit deux termes de même
nature d'une même côté d'une équation comme 
$$x^2+3x^2= 5x+3 \ \Longleftrightarrow \ 4x^2= 5x+3 \ ,$$
soit deux termes identiques de part et d'autres de l'équation comme 
$$3x^2+3=-5x \ \Longleftrightarrow \ 3x^2+5x+3=0 \ .$$
(Il est amusant de noter qu'en espagnol le terme dérivé <<algebrista>> signifie
à la fois un algébriste ou rebouteux, celui qui réduit les fractures.)\\

Disons rapidement que jusqu'au XIX\ieme, les mathématiciens font des calculs,
parfois du même type sur des objets différents. Même s'ils savent utiliser des
variables à la place de nombres, s'ils se servent d'une notation pour le $0$ ou
qu'ils peuvent considérer des nombres imaginaires, peu de place est alors
accordée à  la théorie de ses calculs. 
La première moitié du XIX\ieme \ siècle voit une renaissance de l'Algèbre par
l'introduction de nouveaux concepts, méthodes et objets pour la résolution des
équations algébriques. D'ailleurs, Serret en 1866 écrit en introduction de son
\emph{Cours d'algèbre supérieure} (sic) que <<L'Algèbre [est], à proprement
parlé, l'Analyse des équations>>. \\


\subsection{L'algèbre ou l'axiomatisation des structures algébriques}
C'est donc à partir de la seconde moitié du XIX\ieme \ siècle que naît la forme
actuelle de l'Algèbre qui consiste à axiomatiser les propriétés des opérations
apparaissant dans le traitement des équations et à étudier les structures
algébriques qui en résultent plus qu'à étudier les manières de résoudre les
dites équations.  
Voici ce qu'écrit Bourbaki dans son <<\'Elements d'histoire des mathématiques>>
: <<Nous arrivons ainsi à l'époque moderne, où la méthode axiomatique et la
notion de structure (sentie d'abord, définie à date récente seulement),
permettent de séparer des concepts qui jusque-là avaient été inextricablement
mêlés, de formuler ce qui était vague ou inconscient, de démontrer avec la
généralité qui leur est propre les théorèmes qui n'étaient connus que dans des
cas particuliers.>>
Trois grandes familles d'équations y ont  alors joué un rôle crucial. 

\begin{description}
\item[\sc Les équations linéaires] Elles sont du type 
$$\left\{\begin{array}{lll}
2x+y-z&=& 1\\
3x+2y+z&=& 4 \\
x+3y+z&=& 2\ .
\end{array}\right. $$
La théorie des espaces dans lesquels en elles s'expriment à donné 
naissance à la notion d'{\it espaces vectoriels}, dont l'axiomatisation a été
donnée principalement par Peano en 1888. 

\item[\sc Les équations diophantiennes] Elles sont type 
$$2^{x}-1=y \quad \text{ou}\quad  x^{5}+y^{5}=z^5 \ ,$$ 
avec pour solutions des nombres entiers. Leur étude abstraite a donné naissance
aux notions algébriques d'{\it anneaux}, d'{\it idéaux} et de {\it corps}, via
celle de nombre algébrique gr‰ce principalement à l'école allemande des
Dirichlet, Kummer, Kronecker, Dedekind, Hilbert, après bien sur les travaux de
Gauss. 

\item[\sc Les équations polynomiales] Elles sont du type 
$$8x^3-3x^2+x+7=0 \ .$$
L'étude de leurs solutions a donné naissance à la notion de {\it groupe}. Galois
est en assurément le principal instigateur mais ses travaux fulgurants mais
succincts ne  sont  publiés et  diffusés par Liouville et Serret que bien des
années après sa mort en 1832. L'émergence conceptuelle de cette notion doit
beaucoup au <<Traité des substitutions et des équations algébriques>> de Camille
Jordan (1870). 
\end{description}

Que se passe-t-il à chaque fois ? On reconnait dans différents exemples des
opérations, méthodes et résultats similaires. Il s'agit alors d'en extraire une
substantifique moelle : on fait ressortir les  propriétés communes essentielles
que l'on érige  en axiomes pour définir une nouvelle notion conceptuelle.
Prenons l'exemple de l'algèbre linéaire, c'est-à-dire des espaces vectoriels.
Comment travaille-t-on avec des objets apparemment si différents que sont 
\begin{itemize}[label=$\diamond$]
\item la droite, le plan, l'espace ambiant,
\item les matrices (tableaux de nombres), 
\item $\mathbb{R}^n$,
\item les polynômes, 
\item les applications ensemblistes réelles (à valeurs dans $\mathbb{R}$), 
\item les applications continues réelles,  
\item les applications $C^{\infty}$ réelles, 
\item les applications mesurable réelles, 
\item les ensembles de solution de systèmes d'équations linéaires homogènes, 
\item les ensembles de solution de systèmes d'équations différentielles homogènes, 
\item les suites numériques satisfaisant un relation de récurrence linéaire, 
\item les extensions de corps, 
\item etc. ?
\end{itemize}

On se rend compte que tous les calculs utilisent deux opérations et que ces
dernières vérifient toujours le même type de relations. 
La première opération est une  opération binaire consistant à {\it sommer} les
éléments : 
$$ E\times E \xrightarrow{+} E\ .$$ 
Cette addition vérifie à chaque fois les mêmes propriétés : associativité,
commutativité, présence d'un neutre ($0$). 
Cette structure est enrichie par la présence d'une action du corps de base : on
sait multiplier ces éléments par des nombres : 
$$ \mathbb{R}\times E \xrightarrow{\cdot} E\ .$$ 
Dans ce cas aussi, tous les exemples susmentionnés vérifient la même liste de
relations : associatitivé, distributivité, action du neutre, et action sur le
$0$. Facile alors de donner la définition abstraite et générale d'un espace
vectoriel. 

\subsection{Intérêts de l'axiomatisation }

On l'a tous bien senti, le cerveau commence à chauffer. Il y a en effet un prix
à payer pour arriver à concevoir cette axiomatisation, c'est celui de travailler
de plus en plus abstraitement. Se pose du coup avec acuité la question de
l'intérêt d'une telle démarche ; essayons d'en dégager quels bénéfices. 

\begin{enumerate}
\item Cette conceptualisation offre une prise de hauteur remarquable. Cela
permet de mettre sur un même pied 
différents objets qui sont au fond de même nature et cela donne des moyens de
les comparer efficacement à l'aide d'une bonne notion de morphisme.

\noindent 
{\sc Exemple : } En algèbre linéaire, on dispose d'une notion d'{\it application
linéaire} entre espaces vectoriels qui permet de les comparer facilement
(injectivité-noyau, surjectivité, dimension, etc.)


\item \'Etablir une telle  théorie générale permet de démontrer d'un seul coup
un  résultat qui sera valable automatiquement dans tous les exemples de la
théorie. Cela permet une simplification conceptuelle des énoncés. 

\noindent 
{\sc Exemple : } L'existence de bases et leur cardinal qui définit la notion de
dimension. 

\item L'approche abstraite permet de s'affranchir des contraintes imposées à
l'esprit par tel ou tel domaine. 

\noindent 
{\sc Exemple : } La géométrie peut nous faire penser que la dimension finie est
une hypothèse indispensable, il n'en est souvent rien. 

\item Extraire un type de structure algébrique permet de mettre au jour ce type
de structure sur de nouveaux objets et ainsi d'y appliquer les méthodes d'autres
domaines. 

\noindent 
{\sc Exemple : } Utiliser les méthodes vectoriels puissantes de dimension dans
le domaine des extensions de corps. 


\item Cette axiomatisation, une fois bien digérée, permet de voir dans quelle
direction poursuivre les recherches. 
On peut dire que cette sédimentation des idées amènent irrémédiablement à une
renaissance quelques (dizaines ?) années plus tard. 

\noindent 
{\sc Exemple : } La notion d'espace vectoriel ouvre ensuite les portes à celles
d'algèbres (associatives, commutatives, de Lie), d'espace tangent d'une variété,
d'espace vectoriel topologique, etc. 


\item Cette démarche met au jour un language universel dont d'autres matières
peuvent d'emparer avec intérêt. 

\noindent 
{\sc Exemple : } Les méthodes et le language de l'algèbre linéaire ont été
accaparé par de nombreux champs de la connaissance comme la mécanique, les
sciences naturelles ou les sciences sociales, par exemple. En économie, la
modélisation de l'état de l'économie à plusieurs facteurs comme celle d'un pays
à l'aide de vecteurs de $\mathbb{R}^n$ a permis à Leontief d'obtenir le <<prix
Nobel>> d'économie en 1973. En effet, si on considère que l'évolution d'une
telle économie évolue suivant des règles constantes et linéaires, on est ramené
à itérer un endomorphisme, dont la réduction permettra de faire efficacement de
la prospective. 

%\item Changement d'échelle et .... 
%
%on recommence. 
\end{enumerate} 
 
\subsection{L'Algèbre moderne}
Sous l'impulsion de l'école allemande des Dedekind, Hilbert, Steinitz, Artin,
Noether une unification conceptuelle des notions susmentionnées est entreprise
entre 1900 et 1930. Son point culminant est le livre de Van der Waerden, publié
en 1930 et dont le titre est bien sur <<Algèbre moderne>>, en français. 

\subsection{Bourbaki}
C'est en 1935 que naquit le groupe Bourbaki dont l'ambition n'est rien de moins
que d'offrir une 
présentation cohérente et exhaustive des mathématiques de son époque. Pour se
faire, il faut une bonne méthode. Il commence donc par un premier volume de
fondation avec la théorie des ensembles, puis continue avec l'Algèbre, etc.  Le
style est aussi détonnant pour l'époque\!; Bourbaki écrit ainsi en exergue de
chaque de ses traités : <<
Le mode d'exposition suivi est axiomatique et procède le plus souvent du général
au particulier>>, un peu  comme chez Al-Khwarizmi ! Bourbaki choisit donc de
décrire les mathématiques à travers les diverses structures qui les composent.
Cela fera dire à Emil Artin : <<Notre époque assiste à la création d'un ouvrage
monumental : un exposé de la totalité des mathématiques d'aujourd'hui. De plus,
cet exposé est fait de telle manière que les liens entre les diverses branches
des mathématiques deviennent clairement visibles>>. Car évidement, le fait de
faire ressortir les différentes structures présentes permet de faire des liens
entre les différents domaines. 

\section{Catégories}

\marginpar{\footnotesize{\qquad \qquad \ \Bruno{Adam [10/9]}}}

Imaginez-vous dans un vaste musée, où les chefs-d'œuvre de l'art mathématique prennent vie sous la lumière douce de la théorie et l'abstraction. Au cœur de cette galerie se trouve une salle mystérieuse, un sanctuaire intellectuel où les mathématiciens et mathématiciennes se réunissent pour explorer l'un des concepts les plus puissants et englobants qui soient : les catégories. Les catégories sont bien plus que de simples ensembles d'objets et de flèches que nous allons découvrir par la suite. Elles sont un concept central en mathématiques qui permet de structurer et d'organiser les objets mathématiques ainsi que les relations entre eux afin d’avoir une vue globale de ces dernières et de les étudier uniformément. Cette partie va nous aider à pénétrer dans ce monde abstrait et à découvrir ces fameuses catégories en s’appuyant sur des exemples pertinents et en soulignant sur les remarques, questions faites et réflexions qui en découlent.

\subsection{Définition}

\begin{defi}[Catégorie]
Une \emph{catégorie} $\mathcal{C}$ est composée des éléments suivants :
\begin{itemize}[label=\textbullet]
\item une collection d'\emph{objets}, notée $\mathrm{Obj}(\mathcal{C})$ : ${X}, {Y}, {Z},  \ldots$, 
\item une collection de \emph{flèches}, notée $\mathrm{Flech}(\mathcal{C})$ : 
$\nearrow, \swarrow, \uparrow, \downarrow, \rightarrow, \ldots $, 
\item deux applications  «$\mathrm{source}$» $: \mathrm{Flech}(\mathcal{C}) \rightarrow \mathrm{Obj}(\mathcal{C})$ et 
«$\mathrm{but}$» $: \mathrm{Flech}(\mathcal{C}) \rightarrow \mathrm{Obj}(\mathcal{C})$, 
\item une \emph{composition} : pour toute paire de flèches
$$
f \colon X \rightarrow {Y} \quad \text{et} \quad {g} \colon {Y} \rightarrow {Z} \quad 
\text{telles que}\quad  \mathrm{source}(g)=\mathrm{but}(f),$$
il existe une flèche 
$$g \circ f : X \to Z \quad 
\text{telle que}\quad  \mathrm{source}(g\circ f)=\mathrm{source}(f) \ \text{et}\ \mathrm{but}(g\circ f)=\mathrm{but}(g),$$
\item des \emph{identités} : pour tout objet $X$ de $\mathcal{C}$,  il existe une flèche 
$$ \mathrm{id}_X : X \to X 
\quad 
\text{telle que}\quad  \mathrm{source}(\mathrm{id}_X)=\mathrm{but}(\mathrm{id}_X)=X.$$
\end{itemize}
La composition est \emph{associative} 
\[h\circ (g\circ f) = (h\circ g)\circ f\]
et \emph{unitaire} 
\[f\circ \mathrm{id}_X=f \quad \text{et} \quad \mathrm{id}_Y \circ f=f.\]
\end{defi}


\subsection{Exemples}
\subsubsection{Catégories <<concrètes>>}

\begin{exem}[La catégorie des ensembles] 
La \emph{catégorie $\mathcal{Ens}$ des ensembles} est formée des données suivantes : 
    \begin{itemize}[label=\textbullet]
\item $\textrm{Obj}_{\mathcal{Ens}}$ $=\mathbb{R} , \mathbb{Q}$...  une collection des ensembles \Bruno{@Adam : à reprendre et finir. Présentation à uniformiser avec la définition (générale) donnée ci-dessus }\textcolor{green}{\checkmark}
        
\item $\text{Flech}_{ens}$ $=x \stackrel{f }\longmapsto=$ y: application /entre ensembles / injective/ surjective
\item Source $=f\longmapsto x$ surjective...
\item But $=f \longmapsto y$
\item "O" = composition normale des application
\end{itemize}
\end{exem}

\begin{rema}
Il n'existe pas d'ensemble de tous les ensembles (paradoxe de Russell), c'est pourquoi on a utilisé la notion de collection dans la définition de catégories et non celle d'ensemble. Ceci nous a permis d'inclure l'exemple de la catégorie des ensembles. 
\end{rema}

\begin{exem}[Catégorie des espaces vectoriels]
La \emph{catégorie $\mathcal{Vect}$ des espaces vectoriels} sur un corps $\mathbb{K}$ fixé est formée des données suivantes : 
\begin{itemize}[label=\textbullet]
\item 	$\text{Obj}_{Vect}$ : Les objets dans cette catégorie sont les espaces vectoriels sur un champ donné (par exemple, epace V muni d'un). \Bruno{Tu peux (dois ?) fixer le corps $\mathbb{K}$ au début.} \Bruno{Présentation à uniformiser avec la définition (générale) donnée ci-dessus}
        
\item 	$\text{Flech}_{Vect}$ : Les flèches dans cette catégorie sont les transformations linéaires entre les espaces vectoriels. Chaque flèche relie deux espaces vectoriels et représente une transformation linéaire d'un espace vectoriel dans un autre.

\item 	Applications "Source" et "But" : Les applications "Source" et "But" associent à chaque flèche sa source et son but dans la catégorie des espaces vectoriels. La source d'une flèche est l'espace vectoriel de départ, et le but est l'espace vectoriel d'arrivée.

Source(f) : E1 (l'espace vectoriel de départ de la flèche f)

But(f) : E2 (l'espace vectoriel d'arrivée de la flèche f)
\item  La composition est une opération définie sur les flèches de la catégorie. Pour composer deux flèches f : E1 → E2 et g : E2 → E3, il doit être vérifié que l'espace vectoriel E2 est à la fois la source de f et le but de g (But(f) = Source(g)).
    \end{itemize}
\end{exem}

\begin{exem}[Catégories des corps]
La \emph{catégorie $\mathcal{Cor}$ des corps} est formée des données suivantes : 
    \begin{itemize}[label=\textbullet]	
\item
        $\mathrm{Obj}_{\mathcal{Cor}}$ : les ensembles munis d'opérations d'addition et de multiplication qui forment un corps ($\mathbb{Z}$/p$\mathbb{Z}, \mathbb{Q}$ …) \Bruno{@Adam : à reprendre pour finir}
\item		$\text{Flech}_{Cor}$  : les homomorphismes de corps, inclusion ensembliste, application ensembliste

\item		Source / But : corps de départ et corps d’arrivé.

\item		« o » : composition des flèches

    \end{itemize}
\end{exem}    

De manière analogue, on pourrait introduire les catégories des anneaux, des groupes, etc. On parle ici de catégories <<concrètes>> car les flèches sont toutes des applications ensemblistes (vérifiant parfois des propriétés). 

\subsubsection{Catégories <<non-concrètes>>}
Toutes les catégories ne sont pas forcément faites de flèches qui sont des applications ensemblistes; en voici quelques exemples. 

\begin{exem}[Catégories des quaternions]
La catégorie $\mathcal{Quat}$ des quaternions est formée des données suivantes : 

\begin{itemize}[label=\textbullet]	
\item	$\mathrm{Obj}_\mathcal{Quat}$ : « . » nous manipulons des quaternions sous la forme $\alpha 1$+$\beta \mathrm{i}$+$\gamma \mathrm{j}$+ $\delta \mathrm{k}$, où , $\alpha,\beta,\gamma,$ \text{et}  $\delta$
           sont des nombres réels. 
        
\item	 $\text{Flech}_{Quat}$ : Les flèches dans cette catégorie sont les éléments $1, \mathrm{i}, \mathrm{j}$ et $\mathrm{k}$, qui sont les unités imaginaires des quaternions. Chacun de ces éléments est considéré comme une flèche distincte. $\mathrm{H}=\{\alpha 1+\mathrm{i} \beta+\mathrm{j} \gamma+\mathrm{k} \delta, \alpha, \beta, \gamma, \delta \in \mathbb{R}\}$

\item	 « o » : Pour deux flèches a et $\mathrm{b}$, la composition $\mathrm{a} \circ \mathrm{b}$ est la multiplication de quaternions correspondante :
\begin{align*}
& \mathrm{i}^2=\mathrm{j}^2=\mathrm{k}^2=\mathrm{ijk}=-1 \\
& \mathrm{ij}=-\mathrm{ji}=\mathrm{k}, \mathrm{jk}=-\mathrm{kj}=\mathrm{i}, \mathrm{ki}=-\mathrm{ik}=\mathrm{j} \ldots
\end{align*}
    \end{itemize}
\end{exem}

\begin{exem}[La catégorie $\mathcal{Cnc}$]
La catégorie $\mathcal{Cnc}$  est formée des données suivantes : 
    \begin{itemize}[label=\textbullet]
        \item $\mathrm{Obj}_{\mathcal{Cnc}}$ = \quad \textbullet \Bruno{@Adam : à finir}
        \item $\mathrm{Flech}_{\mathcal{Cnc}}$\quad=\quad\{-3,  -2,  ....  1..\}
        \item 2 composition \qquad $\longrightarrow$ + \qquad($\text{id}_.$  =  0)

        \qquad \qquad \qquad \qquad 
        \hspace{0.3mm} $\longrightarrow$ $\times$ \qquad($\text{id}_.$  =  1)
    \end{itemize}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.2\textwidth]{sym1.png}
\end{figure}
\end{exem}

\subsubsection{Contre-exemple}
Quoi de mieux pour bien assimiler la notion de catégorie que de trouver un contre-exemple concret.
On considère les données suivantes  : 
\begin{itemize}[label=\textbullet]
\item $\text{Obj}_{enc}$: \qquad \qquad  X , Y , Z , W
\item

\begin{minipage}{0\textwidth}
 $\text{Flech}_{enc}$:   
\end{minipage}
\hspace{1mm}
\begin{minipage}{0.7\textwidth}
    
    \centering
\includegraphics[width=0.5\textwidth]{Sym.png}
\end{minipage}


\hspace{92mm}$(g\circ f): = \phi$

\hspace{92mm} $(h\mathrm{o}g):  = \psi$ 

Composition: on voit clairement que  $\alpha \ne \beta$  \hspace{21mm}$(h\mathrm{o}\phi): = \alpha$

\hspace{92mm}$(\psi\mathrm{o}f):=\beta$
\end{itemize}
\Bruno{ce serait bien d'utiliser Tikz pour les dessins}

\marginpar{\footnotesize{\Bruno{Lyes [17/9]}}}

\subsection{Catégories petites et localement petites}

On peut aussi donner une autre définition des catégories en regroupant chaque sous-collection de 
flèches en fixant la source et le but. On remarque aussi que les flèches sont souvent appelées \emph{morphismes} ou \emph{homomorphismes}.
Soit $f$ une flèche de source $X$ et de but $Y$, on dira que 
$f$ appartient à la collection des morphismes de $X$ dans $Y$, qu'on notera au choix 
\[\operatorname{Flech}_\mathcal{C}(X,Y)=\operatorname{Hom}_\mathcal{C}(X, Y) = \operatorname{Mor}_\mathcal{C}(X, Y) = \mathcal{C}(X, Y).\]

Ceci donne la définition équivalente suivante de catégorie, où il n'y a plus besoin d'applications <<$\mathrm{source}$>> et 
<<$\mathrm{but}$>> !

\begin{defi}[Catégorie (bis)]
Une \emph{catégorie} $\mathcal{C}$ est composée des éléments suivants :
\begin{itemize}[label=\textbullet]
	\item une collection d'\emph{objets}, notée $\mathrm{Obj}(\mathcal{C})$ : ${X}, {Y}, {Z},  \ldots$, 
	\item des collections de \emph{flèches} (ou \emph{morphismes}) pour toute paire d'objets $X$ et $Y$, notées $\mathrm{Hom}_\mathcal{C}(X, Y)$ : $f, g, h, \ldots$,
	\item une \emph{composition} :
        pour toute paire de flèches $f$ de $\operatorname{Hom}_\mathcal{C}(X, Y)$
        et $g$ de $\mathrm{Hom}_\mathcal{C}(Y, Z)$, on a une flèche $g \circ f$
        dans $\mathrm{Hom}_\mathcal{C}(X, Z)$,
\item des \emph{identités} : pour tout objet $X$ de $\mathcal{C}$,  il existe une flèche 
$ \mathrm{id}_X$ dans $\mathrm{Hom}_\mathcal{C}(X, X)$.
\end{itemize}
La composition est \emph{associative} 
\[h\circ (g\circ f) = (h\circ g)\circ f\]
et \emph{unitaire} 
\[f\circ \mathrm{id}_X=f \quad \text{et} \quad \mathrm{id}_Y \circ f=f.\]
\end{defi}

Dans la définition de la notion de catégorie, on utilise des collections d'objets et de morphismes, et non
des ensembles. Cela nous donne plus de flexibilité, et nous évite certaines
restrictions vis-à-vis des ensembles. Néanmoins, il peut arriver que certaines catégories soient constituées d'ensembles. 

\begin{defi}[Petite catégorie]
Une catégorie $\mathcal{C}$ est dite \emph{petite} si ses objets $\mathrm{Obj}(\mathcal{C})$ forment un ensemble.
\end{defi}

\begin{exem}
La catégorie des ensembles finis $\mathcal{Ens}_{fini}$ est définie par : \Bruno{à reprendre}
\begin{itemize}[label=\textbullet]
	\item $\mathrm{Obj} = \{ \text{ensembles finis} \}$
	\item $\mathrm{Hom} =$ \begin{itemize}
                                     \item[\textbullet] applications ensemblistes
                                     \item[\textbullet] injections
                                     \item[\textbullet] surjections
                                 \end{itemize}
\end{itemize}
Cette catégorie est petite car les ensembles finis forment un ensemble. 
\end{exem}

\begin{exem}
La catégorie composée d'un seul objet est une petite catégorie \Bruno{à reprendre : ne pas mélanger "définition" et "propriété" dans la même phrase} :
\begin{itemize}[label=\textbullet]
    \item $\mathrm{Obj} = \{ \cdot \}$
    \item $\mathrm{Hom} = \{ id_\cdot : \cdot \rightarrow \cdot \}$
\end{itemize}

\textcolor{red}{(insérer ici un graphe d'un point qui revient vers lui-même)}
\end{exem}

En effet, l'ensemble des ensembles finis est défini. De la même façon, la
catégorie des corps finis, des espaces vectoriels de dimension finie et toute
catégorie dont les objets sont finis sont des petites catégories.

\Bruno{ajouter une phrase de commentaire avant ou après disant que la propriété d'être petite est souvent trop restrictive et porte sur les objets.}

\begin{defi}[Catégorie localement petite]
Une catégorie est dite \emph{localement petite} si, pour toute paire d'objets $X,Y$ les morphismes $\operatorname{Hom}_\mathcal{C}(X, Y)$ de $X$ vers $Y$  forment un ensemble.
\end{defi}

La catégorie des ensembles $\mathcal{E}ns$ n'est pas une petite catégorie. En
effet, l'ensemble des ensembles n'existe pas (cf. Paradoxe de Russel).
\textcolor{red}{(pas sûr d'avoir eu une bonne lecture du tableau sur ce qui suit)}
\Bruno{oui, c'est bien}
Néanmoins, les $\operatorname{Hom}_{\mathcal{E}ns}(X, Y)$ forment des ensembles.
C'est donc une catégorie localement petite.

La catégorie des espaces vectoriels et des corps ne forment pas non plus de
petites catégories, parce qu'il n'existent pas d'ensembles de tous les espaces
vectoriels, ni d'ensemble de tous les corps, on doit donc se limiter aux
collections.

\begin{prop}
Si une catégorie est petite, alors, elle est aussi localement petite. 

\textcolor{red}{(à prouver ? mais difficile sans une bonne compréhension de la différence entre
ensemble et collection)}
\end{prop}

\Bruno{cette proposition est fausse ! Mais c'est très bien de t'être posé la question. (Du coup, je suis d'accord la terminologie choisie peut prêter à confusion : <<petit>> n'implique pas <<localement petit>>. On peut le sentir car la première condition porte sur les objets et la seconde sur les morphismes ... En voici un contre-exemple, qu'il faut taper. :) On considère la catégorie à un objet et dont la collection des flèches est les ensembles. On la munit de la composition donnée par l'union (disjointe) et l'identité est l'ensemble vide !)}

\subsection{Constructions de catégories}

\subsubsection{Catégories associées à des ensembles partiellement ordonnés}

\begin{defi}[Ensemble partiellement ordonné]
Un \emph{ensemble partiellement ordonné} $\pi=(E, \leq)$ est un ensemble $E$ muni 
d'une relation d'ordre $\le$, c'est-à-dire une relation binaire vérifiant :
\begin{itemize}[label=\textbullet]
    \item \textsc{réflexivité} : $\forall x \in E,\ x \leq x$,
    \item \textsc{transitivité} : $\forall x, y, z \in E,\ x \leq y \wedge y \leq z \Leftrightarrow x \leq z$,
    \item \textsc{antisymétrie} : $\forall x, y \in E,\ x \leq y \wedge y \leq x \Leftrightarrow x = y$.
\end{itemize}
\end{defi}


\begin{exem}[Ensemble des parties]
Pour tout ensemble $X$, l'ensemble $E \coloneq \mathcal{P}(X)=\{A\subset X\}$ de ses parties est un ensemble partiellement ordonné  où la relation d'ordre est donnée par l'inclusion 
$\leq\, \coloneq\, \subset$.
\end{exem}

On peut représenter un tel ensemble avec un diagramme, le diagramme de Hasse,
qui met en évidence l'ordre établi dans l'ensemble.

\textcolor{red}{(insérer ici un diagramme de Hasse) dans le cas $X = \{1, 2, 3\}$.}

On peut associer canoniquement une catégorie à tout ensemble partiellement ordonné.

\begin{defprop}[Catégorie associée à un ensemble partiellement ordonné]
    Soit $\pi = (E, \leq)$ un ensemble partiellement ordonné. Les données suivantes 
    forment une catégorie, notée $\mathcal{C}_\pi$ :   
       \begin{itemize}[label=\textbullet]
        \item $\operatorname{Obj} = E$
        \item $
            \operatorname{Hom}(e, e') =
            \begin{cases}
            	e \rightarrow e'\ \text{si } e \leq e'\\
                \emptyset\ \text{sinon}
            \end{cases}
        $
        \item \Bruno{La composée est donnée par ...}
        \item \Bruno{les identités sont ...}
    \end{itemize}
\end{defprop}

\begin{proof}
\Bruno{La composition est associative car (transitivité), elle est unitaire car (...)}
\end{proof}

\subsubsection{Catégorie associée à un graphe}

Les graphes sont des outils centraux en mathématiques et en informatique. 
On peut déjà remarquer une certaine proximité entre les graphes et les catégories : on représente souvent des
catégories au moyen de graphes. On peut aussi créer des graphes à partir d'ensembles partiellement ordonnés via la notion de diagramme de Hasse. 

\begin{defi}[Graphes]\leavevmode
\begin{itemize}[label=\textbullet]
    \item Un \emph{graphe} $G=(S,A)$ est un ensemble $S$ (fini et non vide) de \emph{sommets} 
    et un ensemble $A$ de paires de sommets appelées \emph{arêtes}.
    \item Un graphe $G$ est dit simple s'il n'y a au plus qu'une seule arête
        reliant deux sommets. \Bruno{à virer : on va faire plus simple : par défaut nos graphes sont "simples" (sinon on les appelerait des "multigraphes").}
    \item Un graphe $G=(S,A)$ est \emph{dirigé} lorsque toutes ses arêtes sont des paires \emph{ordonnées} de sommets, c'est-à-dire $A\subset S\times S$. 
    \item Un graphe $G$ est dit \emph{complet} lorsqu'il est constitué de toutes les arêtes possibles. 
\end{itemize}
\end{defi}

On ne considérera ici que des graphes dirigés. On peut maintenant se demander quels types de graphes dirigés donnent naissance à des catégories. 

\begin{defi}[Graphe transitifs, graphes réflexifs]\leavevmode
\begin{itemize}[label=\textbullet]
	\item Un graphe dirigé $G=(S,A)$ est \emph{transitif} si  
     $(x, y)\in A$ et $(y, z) \in A$ implique 
    $(x, z) \in A$. \Bruno{une définition n'est pas un équivalence, elle n'est pas symétrique. on ne dit donc pas "si et seulement si", mais seulement "si".}
\item     Un graphe dirigé $G=(S,A)$ est \emph{réflexif} si 
    pour tout $x \in S$, on a $(x, x) \in A$.
\end{itemize}
\end{defi}

\begin{defprop}[Catégorie associée à un graphe transitif et réflexif]
Pour tout graphe dirigé transitif et réflexif $G=(S,A)$, les données suivantes forment une catégorie donnée $\mathcal{C}_G$ :
    \begin{itemize}
        \item $\operatorname{Obj} = E$
        \item $
            \operatorname{Hom}(e, e') =
            \begin{cases}
            	e \rightarrow e'\ \text{si } e \leq e'\\
                \emptyset\ \text{sinon}
            \end{cases}
        $
        \item \Bruno{La composée est donnée par ...}
        \item \Bruno{les identités sont ...}
    \end{itemize}
\end{defprop}

\begin{proof}
\Bruno{La composition est associative car (transitivité), elle est unitaire car (...)}
\end{proof}

\begin{rema}
    On a sait qu'à partir d'un ensemble partiellement ordonné on
    peut construire un graphe transitif et réflexif via le diagramme de Hasse. 
    Et à partir d'un
    graphe transitif et réflexif,
    on peut construire une catégorie.

    \[
        \text{Ensembles partiellement ordonnés}
            \rightsquigarrow
        \text{Graphes transitifs réflexifs}
            \rightsquigarrow
        \text{Catégories}
    \]
\end{rema}
\Bruno{et alors ? Commenter ? la seconde construction est-elle plus générale ou équivalente à la première ? \`A finir.}

\marginpar{\footnotesize{\Bruno{Corentin [17/9]}}}

Maintenant que nous avons vu comment définir la notion de catégorie, nous pouvons chercher comment construire de nouvelles catégories à partir de catégories existantes.

\subsubsection{Union disjointe de catégories}

\begin{defprop}[Union disjointe de catégories]

    Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. On appelle \emph{union disjointe de $\mathcal{C}$ et $\mathcal{D}$} la catégorie $\mathcal{C} \sqcup \mathcal{D}$ formée des données suivantes :
    \begin{itemize}[label=\textbullet]
        \item $\mathrm{Obj}(C \sqcup D) :=$ La collection formée des éléments de $\mathrm{Obj}(C) \text{ et } \operatorname{Obj}(D)$ telle que les éléments de $\operatorname{Obj}(C) \text{ et } \operatorname{Obj}(D)$ sont disjoints.
        \item $\operatorname{Hom}_{C \sqcup D}(e,f) :=
            \begin{cases}
                \operatorname{Hom}_C(e,f)\text{ si } e \text{ et } f \text{ sont dans } \operatorname{Obj}(C)\\
                \operatorname{Hom}_D(e,f)\text{ si } e \text{ et } f \text{ sont dans } \operatorname{Obj}(D)\\
                \emptyset \text{ sinon}
            \end{cases}$
        \item Les compositions et les identités de $C \sqcup D$ sont celles de $C$ et de $D$.
    \end{itemize}
    \textcolor{red}{(insérer un exemple classique)}\Bruno{je ne crois pas j'en connaisse mais tu peux en chercher. En tout cas, il serait bon d'illustrer avec un exemple, voire un dessin comme en cours.}
\end{defprop}

\begin{proof}
\Bruno{c'est assez évident, mais dire 2 mots.}
\end{proof}

\begin{rema}
   \textbf{Attention :} Si on appelle $e$ un élément de $\operatorname{Obj}(C)$ et $e$ un élément de $\operatorname{Obj}(D)$, ils seront considérés différents dans $\operatorname{Obj}(C \sqcup D)$. On pourra les noter $e_C$ et $e_D$ dans $\operatorname{Obj}(C \sqcup D)$.\Bruno{je retirerais cette remarque ... je vois ce que tu veux dire, mais ... bof ... on fait l'union disjointe des ensembles d'objets de $\mathcal{C}$ et $\mathcal{D}$ .... c'est clair. enfin, c'est clair pour moi car quand je pense union, je ne pense pas union dans un gros ensemble donné. Si tu veux laisser ok, mais je changerais la forme=rédaction.}
\end{rema}
\begin{prop}
   La construction de l'union disjointe est \emph{commutative}, c'est-à-dire que l'on a $C \sqcup D = D \sqcup C$.
\end{prop}

\Bruno{vraie question : c'est quoi une égalité entre catégories ? :-P}

\Bruno{proposition < théorème si tu vois ce que je veux dire} 

\begin{proof}
\Bruno{toute proposition/théorème nécessite une démonstration, même courte.}
\end{proof}

\subsubsection{Joint de catégories}

\begin{defprop}[Joint de catégories]

    Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. On appelle \emph{joint entre $\mathcal{C}$ et $\mathcal{D}$} la catégorie $\mathcal{C} \rhd \mathcal{D}$ formée des données suivantes :
    \begin{itemize}[label=\textbullet]
        \item $\mathrm{Obj}(C \rhd D) := \mathrm{Obj}(C \sqcup D)$ 
        \item $\mathrm{Hom}_{C \rhd D}(e,f) :=
            \begin{cases}
                \operatorname{Hom}_{C \rhd D}(e,f) = \{e \rightarrow e'\} \text{ si } e \text{ est dans } \operatorname{Obj}(C) \text{ et } f \text{ est dans } \operatorname{Obj}(D)\\
                \text{sinon } \operatorname{Hom}_{C \rhd D}(e,f) = \operatorname{Hom}_{C \sqcup D}(e,f)
            \end{cases}$
        \item Les identités de $C \rhd D$ sont celles de $C$ et celles de $D$
        \item Les compositions de $C \rhd D$ sont les compositions de $C$ et de $D$ et s'il existe des flèches $e \rightarrow f$ et $f \rightarrow g$ alors il existe une flèche $e \rightarrow g$.
    \end{itemize}
\end{defprop}

\begin{proof}
\Bruno{à faire}
\end{proof}

\begin{rema}
   La  joint entre les catégories n'est \emph{plus} une construction commutative.
\end{rema}

\begin{prop}
Soit $\mathcal{C}at_{[n]} :=$ la catégorie formée à partir de $(\textlbrackdbl n\textrbrackdbl ,\leq)_{n \in \mathbb{N}^*}$
On pose $\mathcal{C} = \mathcal{C}at_n$ et $\mathcal{D} = \mathcal{C}at_m$, $(n,m)\in {\mathbb{N}^{*}}^2$.
\Bruno{On a 
\[
\mathcal{C}at_{[n]} \rhd \mathcal{C}at_{[m]}=\mathcal{C}at_{[n+m+1]}
\]
}

\textcolor{red}{(Finir l'exemple)}
\end{prop}

\begin{proof}
\Bruno{à finir}
\end{proof}

\subsubsection{Produit de catégories}

\begin{defprop}[Produit de catégories]

    Soient $\mathcal{C}$ et $\mathcal{D}$ deux catégories. On appelle \emph{produit de $\mathcal{C}$ et $\mathcal{D}$} la catégorie $\mathcal{C} \times \mathcal{D}$ formée des données suivantes :
    \begin{itemize}[label=\textbullet]
        \item $\operatorname{Obj}(C \times D) :=$ La collection formée des paires $(e,f)$ telles que $e$ est dans $\operatorname{Obj}(C)$ et $f$ est dans $\operatorname{Obj}(D)$.
        \item $(e,f) \rightarrow (e',f')$ est dans $\operatorname{Hom}_{C \times D}((e,f),(e',f'))$ si et seulement si $e \rightarrow e'$ est dans $\operatorname{Hom}_C(e,e')$ et $f \rightarrow f'$ est dans $\operatorname{Hom}_D(f,f')$.
    \end{itemize}
    \textcolor{red}{(insérer un exemple)}
\end{defprop}

\begin{proof}
\Bruno{a faire}
\end{proof}

\textcolor{red}{(Rajouter Sous-catégorie et Catégorie opposée)}

\end{document}
