
\documentclass[twoside, 11pt]{amsart}

\usepackage{tikz}
\usetikzlibrary{positioning, arrows.meta}
\include{package}

\begin{document}
\section{Propriétés des morphismes}
\subsection{Epimorphisme dans une catégorie}
\begin{defi}
    (Surjectivité dans les ensembles): Soit \(f : \mathbb{R} \to \mathbb{R}\) une fonction. On dit que \(f\) est surjective si et seulement si pour tout \(y \in \mathbb{R}\), il existe \(x \in \mathbb{R}\) tel que \(f(x) = y\), c'est-à-dire :
        \[\forall y \in \mathbb{R}, \exists x \in \mathbb{R} : f(x) =
          y\]
        \Hugo{Pourquoi se restreindre \`a l'ensemble $\mathbb{R}$ ?}
      \end{defi}
      
\begin{defi}
Dans la catégorie opposée $\mathcal{C}^\text{op}$ : Un morphisme $f: A \to B$ est un épimorphisme si, pour tout objet $Z$ de $\mathcal{C}^\text{op}$ et pour tout couple de morphismes $g, h: Z \to A$, si $f^\text{op} \circ g = f^\text{op} \circ h$ dans $\mathcal{C}^\text{op}$, alors $g = h$ dans $\mathcal{C}^\text{op}$.
\end{defi}
\Hugo{La d\'efinition d'\'epimorphisme qui suit est la bonne. Mais
  pour la d\'efinition pr\'ec\'edente, pourrais-tu clarifier et/ou
  rajouter des explications ? Pour r\'esumer l'id\'ee : on a envie de
  voir ce qu'il se passe si on 
  ``retourne toutes les fl\`eches'' dans la d\'efinition d'un mono,
  donc on regarde ce que veut dire etre un mono dans
  $\mathcal{C}^\mathrm{op}$. Et on va tomber sur la d\'efinition d'\'epi comme suit.} 
\begin{defi}
    Un morphisme \( f : A \to B \) dans une catégorie est appelé un \emph{épimorphisme} si, pour tout objet \( Z \) et pour tout couple de morphismes \( h, k : B \to Z \) tels que \( h \circ f = k \circ f \), alors \( h = k \).
\end{defi}

\begin{prop}
Dans $\mathcal{Ens}$, pour tout morphisme $f : A \to B$ :
\[ f \text{ est un épimorphisme} \iff f \text{ est surjective} \]
\end{prop}

\begin{proof}
Prouvons l'implication dans les deux sens.

\textbf{($\Rightarrow$) Si $f$ est un épimorphisme :} Supposons que $f$ est un épimorphisme.

Supposons par l'absurde qu'il existe un élément $b \in B$ pour lequel il n'existe pas d'élément $a \in A$ tel que $f(a) = b$.

Nous pouvons maintenant construire une flèche $h : B \to \{0, 1\}$ pour un certain objet $\{0, 1\}$ comme suit :
\begin{center}
\begin{tikzpicture}
  \node (A) at (0,0) {$A$};
  \node (B) at (3,0) {$B$};
  \node (C) at (6,0) {$\{0, 1\}$};
  \draw[->, >=Stealth] (A) -- node[above] {$f$} (B);
  \draw[->, >=Stealth] (B) to[bend left] node[above] {$h$} (C);
  \draw[->, >=Stealth] (B) to[bend right] node[below] {$1_{\text{Im}(A)}$} (C);
\end{tikzpicture}
\end{center}

Définissons la fonction $h(y)$ comme suit :
\[ h(y) = \begin{cases} 
      1 & \text{si } y = b \\
      1_{\text{Im}(A)}(y) & \text{sinon}
   \end{cases}
\]

Maintenant, considérons $h \circ f$ et $1_{\text{Im}(A)} \circ f$ :

\[ (h \circ f)(a) = h(f(a)) = h(b) = 1 \text{ si } f(a) = b \]
\[ (h \circ f)(a) = h(f(a)) = 1_{\text{Im}(A)}(f(a)) \text{ si } f(a) \neq b \]

\[ (1_{\text{Im}(A)} \circ f)(a) = 1_{\text{Im}(A)}(f(a)) \]

Donc, pour tout $a \in A$, nous avons $(h \circ f)(a) = (1_{\text{Im}(A)} \circ f)(a)$. Cela signifie que $h \circ f = 1_{\text{Im}(A)} \circ f$ pour tous les $a \in A$.

Cependant, nous avons défini $h(y)$ de manière à ce qu'elle diffère de $1_{\text{Im}(A)}(y)$ si $y = b$. Cela implique que $h \neq 1_{\text{Im}(A)}$. Par conséquent, $h \circ f \neq 1_{\text{Im}(A)} \circ f$, ce qui contredit la propriété d'épimorphisme de $f$.

Ainsi, notre hypothèse selon laquelle il existe un élément $b \in B$ pour lequel il n'existe pas d'élément $a \in A$ tel que $f(a) = b$ est incorrecte. Par conséquent, $f$ est surjective.

\textbf{($\Leftarrow$) Si $f$ est surjective :} Supposons maintenant que $f$ soit surjective. Pour montrer que $f$ est un épimorphisme, prenons deux morphismes $h, k : B \to Z$ pour un objet $Z$. Supposons que $h \circ f = k \circ f$. Nous devons montrer que $h = k$. Soit $y \in B$. 

Puisque $f$ est surjective, $\exists x \in A$ tel que $f(x) = y$. Montrons que $h(y) = k(y)$.

Ainsi, pour tout $y \in B$, nous avons montré que $h(y) = k(y)$. Cela implique que $h = k$, ce qui prouve que $f$ est un épimorphisme.
\end{proof}

\begin{exem}
Considérons l'application de projection $\pi_1 : \mathbb{R}^2 \to \mathbb{R}$ définie comme suit :
\[
\pi_1(x, y) = x
\]

Montrons que c'est un épimorphisme. \Hugo{Dans quelle cat\'egorie ?
  Ensembles ? Espaces vectoriels ? } Soit $f, g : \mathbb{R} \to V$ deux applications linéaires vers un espace vectoriel $V$ telles que $f \circ \pi_1 = g \circ \pi_1$. Alors, pour tout $x \in \mathbb{R}$, nous avons :
\[
f(x) = f(\pi_1(x, 0)) = (f \circ \pi_1)(x, 0) = (g \circ \pi_1)(x, 0) = g(\pi_1(x, 0)) = g(x)
\]

Ainsi, $f = g$, ce qui montre que $\pi_1$ est un épimorphisme dans la catégorie des espaces vectoriels.
\end{exem}

\begin{defi}
    Soit $\mathcal{C}$ une catégorie. Une \emph{sous-catégorie des épimorphismes} de $\mathcal{C}$, notée $\mathcal{C}_{\text{epi}}$, est une sous-catégorie de $\mathcal{C}$ qui a les mêmes objets que $\mathcal{C}$, mais dont les flèches (morphismes) sont restreintes aux épimorphismes de $\mathcal{C}$.

En d'autres termes, dans $\mathcal{C}_{\text{epi}}$, pour chaque paire d'objets $A$ et $B$ dans $\mathcal{C}$, si $f : A \to B$ est une flèche (morphisme) dans $\mathcal{C}$ et que $f$ est un épimorphisme dans $\mathcal{C}$, alors $f$ est également une flèche dans $\mathcal{C}_{\text{epi}}$.
\end{defi}
\Hugo{Je parlerais plutot de ``la'' sous-categorie des
  epimorphismes. Comment on sait que c'est une cat\'egorie ? Il faut
  s'assurer que la composition et les identit\'es sont bien d\'efinies.}


\begin{defi}
    Soit $\mathcal{C}$ une catégorie. Un \emph{épimorphisme fort}  dans $\mathcal{C}$ est une flèche $f : A \to B$ telle que, pour tout objet $X$ de $\mathcal{C}$ et pour toute paire de flèches $g, h : B \to X$ dans $\mathcal{C}$, si $g \circ f = h \circ f$, alors $g = h$.

En d'autres termes, un épimorphisme fort est une flèche telle que si
deux flèches $g$ et $h$ ont la même composition avec elle, alors elles
sont égales.
\Hugo{\c{C}a c'est la d\'efinition d'un \'epimorphisme, la
  m\^eme que tu as \'ecrit plus haut ! Pour les \'epis forts, tu peux
  aller t'inspirer de la d\'efinition des monos forts ou regarder dans les
  photos du tableau. }
\end{defi}


\section{Catégorie en informatique}
\subsection{Exemple de catégorie en informatique}
\begin{exem}
    \textbf{Construction d'une catégorie de types et de programmes en OCaml :}

\begin{itemize}
    \item \textbf{Objets :} Les types de données en OCaml.
    
    \item \textbf{Morphismes :} Les fonctions entre les types.
    
    \item \textbf{Composition :} La composition de fonctions standard en OCaml.
    
    \item \textbf{Identité :} Problème majeur ! En OCaml, il est difficile de définir une identité générique pour tous les types, car le système de types d'OCaml est statiquement vérifié et ne permet pas de manipulation arbitraire des types.
    \end{itemize}
\Hugo{Ce n'est pas tout \`a fait la raison du probl\`eme. En fait on
  peut sans probl\`eme d\'efinir un programme identit\'e pour tous les
  types. Le probl\'eme c'est que pour composer
   deux programmes on doit faire un renommage de variables
  pour brancher la sortie du premier et l'entr\'ee du deuxi\'eme. Et
  du coup, en composant avec le programme identit\'e, on va rajouter
  une ligne de renommage de variable, donc ce n'est pas un
  \'el\'ement neutre au sens strict.}
\end{exem}
\begin{exem}
Le catégorie $\mathcal{Ens_(print)}$ est definie comme suit :
\begin{itemize}
    \item Objets : Ensemble tas \Hugo{??}.
    \item Flèches (Morphismes) :
      $\text{Hom}_{\mathcal{Ens}_{\text{print}}}(A, B) :=
      \text{Hom}_{\mathcal{Ens}}(A, B \times
      \mathrm{string})$. \Hugo{Pour les probl\`emes d'espacement, je
        te conseille d'utiliser la commande $\backslash$\texttt{mathrm} plutot
        que $\backslash$\texttt{text}. Il y a s\^urement d'autres
      solutions possibles...}
    \item Composition : Pour chaque paire de flèches $f: A \to B$ et
      $g: B \to C$, la composition $g \circ f: A \to C$ est définie
      comme suit : $g \circ f = \text{Hom}_{\mathcal{Ens}}(A, B \times
      \text{string}) \times \text{Hom}_{\mathcal{Ens}}(B, C \times
      \text{string}) \to \text{Hom}_{\mathcal{Ens}}(A, C \times
      \text{string})$.
      \[
        \begin{tikzcd}
    A \arrow[r, "f"] & B \times \text{string} \arrow[r, "g \times id_str"] & C \times (\text{string} \times \text{string}) \arrow[r, "Id_C \times \text{concat}"] & C \times \text{string}
        \end{tikzcd}
      \]
    \item Source : La source d'une flèche $f: A \to B$ est l'ensemble $A$.
    \item But : Le but d'une flèche $f: A \to B$ est l'ensemble $B
      \times \text{string}$. \Hugo{Non, le but c'est juste $B$ ! C'est
        le ``type de sortie''.}
    \item Identité : $\text{id}_A: A \to A \times \text{string}$, définie par $\text{id}_A(a) = (a, \epsilon)$.
\end{itemize}

Avec concat definie comme suit:      
$\text{concat:} \text{string} \times\text{string} \to \text{string} $
\Hugo{D\'efinition?}

\Hugo{Ce serait super de rajouter une preuve que tout \c{c}a forme
  bien une cat\'egorie.}

\end{exem}
\begin{exem}
Soit $A = (Q, \Sigma, \delta, q_0, F)$ un automate fini déterministe où :
\begin{align*}
Q & : \text{ensemble d'états} \\
\Sigma & : \text{ensemble de symboles d'alphabet} \\
\delta & : Q \times \Sigma \to Q \text{ est la fonction de transition} \\
q_0 & : \text{état initial} \\
F & : \text{ensemble d'états finaux}
\end{align*}

La \emph{catégorie induite par l'automate} $A$ est définie comme suit :

\begin{itemize}
  \item \textbf{Objets :} Les objets de la catégorie sont les \emph{états} de l'automate $Q$.

  \item \textbf{Flèches (Morphismes) :} Les flèches de la catégorie représentent les \emph{transitions} définies par la fonction de transition $\delta$. Un morphisme (flèche) de $q$ à $q'$ existe si et seulement si $\delta(q, a) = q'$ pour un certain symbole $a \in \Sigma$. Les flèches sont étiquetées par les symboles $a$ correspondants.

  \item \textbf{Composition :} La composition des flèches est définie par la concaténation des étiquettes des transitions. Si $f$ est une flèche de l'état $q$ à $q'$ avec l'étiquette $a$, et $g$ est une flèche de l'état $q'$ à $q''$ avec l'étiquette $b$, alors la composition $g \circ f$ est une flèche de l'état $q$ à $q''$ avec l'étiquette $ba$.

  \item \textbf{Source et But :} Pour chaque flèche $f : q \to q'$, $q$ est la \emph{source} (origine) de la flèche, et $q'$ est le \emph{but} (destination) de la flèche.

  \item \textbf{Identité :} Pour chaque état $q$ de l'automate, il existe une flèche identité $\text{id}_q : q \to q$ avec l'étiquette vide (sans consommation de symbole).
\end{itemize}
\end{exem}
\Hugo{Attention, il y a une ambiguit\'e dans ta d\'efinition : les
  fl\'eches sont-elles donn\'ees par des symboles ou des mots ? De quoi
  est-ce qu'on a besoin pour la composition ? }

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
