
\documentclass[twoside, 11pt]{amsart}

\usepackage{tikz}
\usetikzlibrary{positioning, arrows.meta}
\include{package}

\begin{document}
\section{Propriétés des morphismes}
\subsection{Epimorphisme dans une catégorie}
\begin{defi}
    (Surjectivité dans les ensembles): Soit \(f : V \to V\) une fonction. On dit que \(f\) est surjective si et seulement si pour tout \(y \in \mathbb{R}\), il existe \(x \in V\) tel que \(f(x) = y\), c'est-à-dire :
        \[\forall y \in V, \exists x \in V : f(x) =
          y\]
      \end{defi}
      
\begin{defi}
\textbf{Épimorphisme dans la catégorie opposée (Cop) :}Une flèche \(f : A \to B\) est un épimorphisme (epi) dans une catégorie \(\mathcal{C}\) si et seulement si elle est un monomorphisme (mono) dans la catégorie opposée \(\mathcal{C}^{op}\).
Cette définition permet de comprendre ce qu'implique le fait d'être un épimorphisme lorsque l'on inverse les flèches dans la catégorie.

\end{defi}

\begin{defi}
    Un morphisme \( f : A \to B \) dans une catégorie est appelé un \emph{épimorphisme} si, pour tout objet \( Z \) et pour tout couple de morphismes \( h, k : B \to Z \) tels que \( h \circ f = k \circ f \), alors \( h = k \).
\end{defi}

\begin{prop}
Dans $\mathcal{Ens}$, pour tout morphisme $f : A \to B$ :
\[ f \text{ est un épimorphisme} \iff f \text{ est surjective} \]
\end{prop}

\begin{proof}
Prouvons l'implication dans les deux sens.

\textbf{($\Rightarrow$) Si $f$ est un épimorphisme :} Supposons que $f$ est un épimorphisme.

Supposons par l'absurde qu'il existe un élément $b \in B$ pour lequel il n'existe pas d'élément $a \in A$ tel que $f(a) = b$.

Nous pouvons maintenant construire une flèche $h : B \to \{0, 1\}$ pour un certain objet $\{0, 1\}$ comme suit :
\begin{center}
\begin{tikzpicture}
  \node (A) at (0,0) {$A$};
  \node (B) at (3,0) {$B$};
  \node (C) at (6,0) {$\{0, 1\}$};
  \draw[->, >=Stealth] (A) -- node[above] {$f$} (B);
  \draw[->, >=Stealth] (B) to[bend left] node[above] {$h$} (C);
  \draw[->, >=Stealth] (B) to[bend right] node[below] {$1_{\text{Im}(A)}$} (C);
\end{tikzpicture}
\end{center}

Définissons la fonction $h(y)$ comme suit :
\[ h(y) = \begin{cases} 
      1 & \text{si } y = b \\
      1_{\text{Im}(A)}(y) & \text{sinon}
   \end{cases}
\]

Maintenant, considérons $h \circ f$ et $1_{\text{Im}(A)} \circ f$ :

\[ (h \circ f)(a) = h(f(a)) = h(b) = 1 \text{ si } f(a) = b \]
\[ (h \circ f)(a) = h(f(a)) = 1_{\text{Im}(A)}(f(a)) \text{ si } f(a) \neq b \]

\[ (1_{\text{Im}(A)} \circ f)(a) = 1_{\text{Im}(A)}(f(a)) \]

Donc, pour tout $a \in A$, nous avons $(h \circ f)(a) = (1_{\text{Im}(A)} \circ f)(a)$. Cela signifie que $h \circ f = 1_{\text{Im}(A)} \circ f$ pour tous les $a \in A$.

Cependant, nous avons défini $h(y)$ de manière à ce qu'elle diffère de $1_{\text{Im}(A)}(y)$ si $y = b$. Cela implique que $h \neq 1_{\text{Im}(A)}$. Par conséquent, $h \circ f \neq 1_{\text{Im}(A)} \circ f$, ce qui contredit la propriété d'épimorphisme de $f$.

Ainsi, notre hypothèse selon laquelle il existe un élément $b \in B$ pour lequel il n'existe pas d'élément $a \in A$ tel que $f(a) = b$ est incorrecte. Par conséquent, $f$ est surjective.

\textbf{($\Leftarrow$) Si $f$ est surjective :} Supposons maintenant que $f$ soit surjective. Pour montrer que $f$ est un épimorphisme, prenons deux morphismes $h, k : B \to Z$ pour un objet $Z$. Supposons que $h \circ f = k \circ f$. Nous devons montrer que $h = k$. Soit $y \in B$. 

Puisque $f$ est surjective, $\exists x \in A$ tel que $f(x) = y$. Montrons que $h(y) = k(y)$.

Ainsi, pour tout $y \in B$, nous avons montré que $h(y) = k(y)$. Cela implique que $h = k$, ce qui prouve que $f$ est un épimorphisme.
\end{proof}

\begin{exem}
Considérons l'application de projection $\pi_1 : \mathbb{R}^2 \to \mathbb{R}$ dans la catégorie des espaces vectoriels définie comme suit :
\[
\pi_1(x, y) = x 
\]

Montrons que c'est un épimorphisme. Soit $f, g : \mathbb{R} \to V$ deux applications linéaires vers un espace vectoriel $V$ telles que $f \circ \pi_1 = g \circ \pi_1$. Alors, pour tout $x \in \mathbb{R}$, nous avons :
\[
f(x) = f(\pi_1(x, 0)) = (f \circ \pi_1)(x, 0) = (g \circ \pi_1)(x, 0) = g(\pi_1(x, 0)) = g(x)
\]

Ainsi, $f = g$, ce qui montre que $\pi_1$ est un épimorphisme dans la catégorie des espaces vectoriels.
\end{exem}

\begin{defi}
Soit $\mathcal{C}$ une catégorie. Une sous-catégorie des épimorphismes de $\mathcal{C}$, notée $\mathcal{C}_{\text{epi}}$, est une sous-catégorie de $\mathcal{C}$ telle que :

\begin{enumerate}
    \item Les objets de $\mathcal{C}_{\text{epi}}$ sont les mêmes que ceux de $\mathcal{C}$.
    
    \item Pour chaque paire d'objets $A$ et $B$ de $\mathcal{C}$, si $f : A \to B$ est une flèche (morphisme) dans $\mathcal{C}$ et que $f$ est un épimorphisme dans $\mathcal{C}$, alors $f$ est également une flèche dans $\mathcal{C}_{\text{epi}}$.
    
    \item La composition des flèches dans $\mathcal{C}_{\text{epi}}$ est la même que celle de $\mathcal{C}$.
    
    \item Les flèches identité dans $\mathcal{C}_{\text{epi}}$ sont les mêmes que celles de $\mathcal{C}$.
\end{enumerate}
\end{defi}

\begin{defi}
    Soit $\mathcal{C}$ une catégorie. Un \emph{épimorphisme fort}  dans $\mathcal{C}$ est une flèche $f : A \to B$ telle que, il existe un $g: B \to A$ avec $g \circ f = id_A$.
\end{defi}
\begin{center}
\begin{tikzcd}
A \arrow[r, "\exists g"] \arrow[rd, "id_A"'] & B \arrow[d, "f"] \\
& A
\end{tikzcd}
\end{center}


\section{Catégorie en informatique}
\subsection{Exemple de catégorie en informatique}
\begin{exem}
    \textbf{Construction d'une catégorie de types et de programmes en OCaml :}

En OCaml, lorsqu'on compose deux programmes, il peut être nécessaire de renommer les variables pour faire correspondre les sorties du premier programme aux entrées du second. Ce renommage de variables peut rendre difficile la définition d'un élément neutre strict pour la composition.

Par exemple, considérons deux programmes simples :
\begin{align*}
f &: \text{int} \to \text{float} \\
g &: \text{float} \to \text{bool}
\end{align*}

La composition $g \circ f$ doit prendre un entier et produire un booléen. Cependant, si $f$ renomme la variable entière en une variable flottante, cela peut créer un conflit avec $g$ qui s'attend à une entrée flottante.

Une solution consiste à définir un élément neutre pour la composition qui effectue un renommage trivial des variables, mais ce n'est pas un élément neutre strict.

Voici un exemple OCaml illustrant le problème :

\begin{verbatim}
(* Types *)
type 'a my_type = 'a

(* Programmes *)
let f (x : int) : float = float_of_int x
let g (x : float) : bool = x > 0.0

(* Composition *)
let compose (f : 'a -> 'b) (g : 'b -> 'c) (x : 'a) : 'c = g (f x)

(* Élément neutre (avec renommage trivial) *)
let neutral_element (x : 'a) : 'a = x
\end{verbatim}
\end{exem}
\begin{exem}
La catégorie $\mathcal{Ens_{\text{print}}}$ est définie comme suit :
\begin{itemize}
    \item Objets : Ensembles de chaînes de caractères, des nombres entiers naturels, etc.
    \item Flèches (Morphismes) :
      $\mathrm{Hom}_{\mathcal{Ens_{\text{print}}}}(A, B) :=
      \mathrm{Hom}_{\mathcal{Ens}}(A, B \times
      \mathrm{string})$.
    \item Composition : Pour chaque paire de flèches $f: A \to B$ et
      $g: B \to C$, la composition $g \circ f: A \to C$ est définie
      comme suit : $g \circ f = \text{Hom}_{\mathcal{Ens}}(A, B \times
      \text{string}) \times \text{Hom}_{\mathcal{Ens}}(B, C \times
      \text{string}) \to \text{Hom}_{\mathcal{Ens}}(A, C \times
      \text{string})$.
      \[
        \begin{tikzcd}
          A \arrow[r, "f"] & B \times \text{string} \arrow[r, "g \times id_{\text{string}}"] & C \times (\text{string} \times \text{string}) \arrow[r, "Id_C \times \text{concat}"] & C \times \text{string}
        \end{tikzcd}
      \]
    \item Source : La source d'une flèche $f: A \to B$ est l'ensemble $A$.
    \item But : Le but d'une flèche $f: A \to B$ est l'ensemble $B$. 
    \item Identité : $\text{id}_A: A \to A \times \text{string}$, définie par $\text{id}_A(a) = (a, \epsilon)$.
\end{itemize}

Avec $\text{concat}$ définie comme suit:      
$\text{concat}: \text{string} \times \text{string} \to \text{string}$, prenant deux chaînes de caractères $a$ et $b$ en entrée et les concaténant pour former une chaîne $ab$. La composition des flèches dans cette catégorie est associative, ce qui signifie que l'ordre dans lequel vous composez les flèches n'a pas d'importance. De plus, l'identité agit comme l'élément unitaire pour la composition, c'est-à-dire que la composition d'une flèche avec l'identité ne change pas la flèche elle-même.
\end{exem}
\begin{exem}
Soit $A = (Q, \Sigma, \delta, q_0, F)$ un automate fini déterministe où :
\begin{align*}
Q & : \text{ensemble d'états} \\
\Sigma & : \text{ensemble de symboles d'alphabet} \\
\delta & : Q \times \Sigma \to Q \text{ est la fonction de transition} \\
q_0 & : \text{état initial} \\
F & : \text{ensemble d'états finaux}
\end{align*}

La \emph{catégorie induite par l'automate} $A$ est définie comme suit :

\begin{enumerate}
  \item \textbf{Objets :} Les objets de la catégorie sont les états de l'automate $Q$.

  \item \textbf{Flèches (Morphismes) :} Les flèches de la catégorie représentent les transitions définies par la fonction de transition $\delta$. Un morphisme (flèche) de $q$ à $q'$ existe si et seulement si $\delta(q, a) = q'$ pour un certain symbole $a \in \Sigma$. Les flèches sont étiquetées par les symboles dans $\Sigma$.

  \item \textbf{Composition :} La composition des flèches est définie par la concaténation des étiquettes des transitions. Si $f$ est une flèche de l'état $q$ à $q'$ avec l'étiquette $a$, et $g$ est une flèche de l'état $q'$ à $q''$ avec l'étiquette $b$, alors la composition $g \circ f$ est une flèche de l'état $q$ à $q''$ avec l'étiquette $ba$. Pour composer, vous avez besoin de deux flèches consécutives dans l'automate et vous concaténez leurs étiquettes pour obtenir l'étiquette de la nouvelle flèche. La composition maintient la source et le but de la première flèche et le but de la seconde flèche.

  \item \textbf{Source et But :} Pour chaque flèche $f : q \to q'$, $q$ est la source (origine) de la flèche, et $q'$ est le but (destination) de la flèche.

  \item \textbf{Identité :} Pour chaque état $q$ de l'automate, il existe une flèche identité $\text{id}_q : q \to q$ avec l'étiquette vide (sans consommation de symbole).
\end{enumerate}
\end{exem}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
