#include "graph_mat-1.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ORDRE 10
#define PROBA 0.2

int main()
{
  // Sinon, bah, c'est pas aléatoire.
  srand(time(NULL));

  char fichier[20];
  graph_mat * g[ORDRE];
  graph_mat * h[ORDRE];
  graph_mat * c[ORDRE];
  g[0] = gm_init(ORDRE);
  h[0] = gm_init(ORDRE);
  c[0] = gm_init(ORDRE);
  c[1] = g[1] = gm_random(ORDRE, PROBA);
  h[1] = gm_sum(g[0], g[1]);

  graph_mat * id = gm_init(ORDRE);
  for (unsigned i = 0; i < ORDRE; i++) {
    gm_add_edge(id, i, i);
  }
  c[1] = gm_sum(c[1], id);
  gm_free(id);

  printf("Matrice d'adjacence du graphe initial :\n");
  gm_disp(g[1]);
  gm_write_dot(g[1], "chemins-01.dot");
  
  for (int k=2; k<ORDRE; ++k)
    {
      g[k] = gm_prod(g[k - 1], g[1]);
      h[k] = gm_sum(h[k - 1], g[k]);
      c[k] = gm_prod(c[k - 1], c[1]);


      printf("Matrice d'adjacence des chemins de longueurs %d :\n", k);
      gm_disp(g[k]);
      gm_disp(h[k]);
      gm_disp(c[k]);
      sprintf(fichier, "chemins-%02d.dot", k);
      gm_write_dot(g[k], fichier);
    }

  printf("La distance entre sommets :\n");
  unsigned diametre = 0;
  unsigned d[ORDRE*ORDRE];
  for (unsigned i = 0; i < ORDRE; i++) {
    for (unsigned j = 0; j < ORDRE; j++) {
      d[i * ORDRE + j] = 0;
      for (unsigned k = 0; k < ORDRE; k++) {
        if (gm_mult_edge(g[k], i, j) != 0) {
          d[i * ORDRE + j] = k;
          if (k > diametre)
            diametre = k;
          break;
        }
      }
      printf("%3d", d[i * ORDRE + j]);
      if (j != ORDRE - 1)
      	printf(" ");
      else
      	printf("\n");
    }
  }
  printf("Le diametre du graphe est : %u\n", diametre);

  /* libération de la mémoire */
  for (int k=0; k<ORDRE; ++k)
    {
      gm_free(g[k]);
      gm_free(h[k]);
      gm_free(c[k]);
    }
  return 0;
}
