#include "graph_mat-1.h"
#include <stdlib.h>
#include <stdio.h>

struct graph_mat {
	unsigned n;
	unsigned m;
	unsigned *adj;
	/* int is_or; in future version*/
	/* double *values; in future version*/
};

graph_mat *gm_init(unsigned n)
{
	graph_mat* g = (graph_mat*) malloc(sizeof(graph_mat));
	if (g == NULL)
		return NULL;

	g->n = n;
	g->m = 0;
	g->adj = (unsigned*) calloc(n * n, sizeof(unsigned));
	if (g->adj == NULL) {
		free(g);
		return NULL;
	}

	return g;
}

void gm_free(graph_mat *g)
{
	if (g == NULL)
		return;
	free(g->adj);
	free(g);
}

unsigned gm_n(const graph_mat *g)
{
	return g->n;
}

unsigned gm_m(const graph_mat *g)
{
	return g->m;
}

unsigned gm_mult_edge(const graph_mat *g, unsigned v, unsigned w)
{
	return g->adj[v * g->n + w];
}

void gm_add_edge(graph_mat *g, unsigned v, unsigned w)
{
	g->m++;
	g->adj[v * g->n + w] = g->adj[w * g->n + v] = gm_mult_edge(g, v, w) + 1;
}

void gm_rm_edge(graph_mat *g, unsigned v, unsigned w)
{
	g->m = g->m == 0 ? 0 : g->m - 1;
	unsigned a = gm_mult_edge(g, v, w);
	g->adj[v * g->n + w] = g->adj[w * g->n + v] = a == 0 ? 0 : a - 1;
}

// Fonction en O(n). On pourrait avoir du O(1) en stockant les degrés.
unsigned gm_degree(const graph_mat *g, unsigned v)
{
	unsigned d = 0;
	for (unsigned i = 0; i < g->n; i++) {
		d += gm_mult_edge(g, v, i);
	}
	d += gm_mult_edge(g, v, v);
	return d;
}

graph_mat *gm_sum(graph_mat * g1, graph_mat * g2)
{
	graph_mat * g = gm_init(gm_n(g1));
	if (gm_n(g1) != gm_n(g2)) abort();
	if (g == NULL) return NULL;

	g->m = g1->m + g2->m;

	for (unsigned i = 0; i < g->n * g->n; i++) {
		g->adj[i] = g1->adj[i] + g2->adj[i];
	}
  
	return g;
}

graph_mat *gm_prod(graph_mat * g1, graph_mat * g2)
{
	graph_mat * g = gm_init(gm_n(g1));
	if (gm_n(g1) != gm_n(g2)) abort();

	for (unsigned i = 0; i < g->n; i++) {
		for (unsigned j = 0; j < g->n; j++) {
			for (unsigned v = 0; v < g->n; v++) {
				g->adj[i * g->n + j] += g1->adj[i * g->n + v] * g2->adj[v * g->n + j];
			}
		}
	}

	for (unsigned i = 0; i < g->n; i++) {
		g->m += gm_degree(g, i);
	}
	g->m /= 2;
  
	return g;
}

void gm_disp(const graph_mat *g)
{
	unsigned v, w;
	printf("n = %d, m = %d\n", gm_n(g), gm_m(g));
	for (v = 0; v < gm_n(g); ++v)
		for (w = 0; w < gm_n(g); ++w) {
			printf("%3d", gm_mult_edge(g, v, w));
			if (w != gm_n(g) - 1)
				printf(" ");
			else
				printf("\n");
		}
}

int gm_write_dot(const graph_mat *g, const char *filename)
{
	/* début solution */
	FILE *f;
	unsigned v, w, k;

	f = fopen(filename, "w");
	if (f == NULL) {
		perror("fopen in gm_write_dot");
		return -1;
	}

	fprintf(f, "graph {\n");
	for (v = 0; v < gm_n(g); ++v)
		fprintf(f, "\t%d;\n", v);

	fprintf(f, "\n");

	for (v = 0; v < gm_n(g); ++v)
		for (w = v; w < gm_n(g); ++w)
			for (k = 0; k < gm_mult_edge(g, v, w); ++k)
				fprintf(f, "\t%d -- %d;\n", v, w);
	fprintf(f, "}\n");
	fclose(f);
	/* fin solution */
	return 0;
}

graph_mat *gm_random(unsigned n, double p)
{
	unsigned v, w;
	graph_mat *g = gm_init(n);
	
	if (g == NULL) {
		perror("gm_init in gm_random");
		return NULL;
	}
	for (v = 0; v < gm_n(g); ++v)
		for (w = v + 1; w < gm_n(g); ++w) {
			double u = (double) rand() / RAND_MAX;
			if (u < p)
				gm_add_edge(g, v, w);
		}
	return g;
}
