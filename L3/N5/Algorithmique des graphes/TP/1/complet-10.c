#include <stdio.h>
#include <stdlib.h>
#include <error.h>

int main() {
    FILE* f = fopen("complet-10.dot", "w");

    if (f == NULL) {
        perror("open");
        exit(1);
    }

    fprintf(f, "graph {");

    for (int i = 1; i <= 10; i++)
        fprintf(f, "%d;", i);

    for (int i = 1; i < 10; i++)
        for (int j = i + 1; j <= 10; j++)
            fprintf(f, "%d -- %d;", i, j);

    fprintf(f, "}");
    fclose(f);

    return 0;
}
