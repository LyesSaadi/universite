#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define ORDRE_MAX 50
int mat[ORDRE_MAX][ORDRE_MAX];

void afficher_mat(int n);
void graphe_complet(int n);
int ecrire_dot(int n, const char *nom_fichier);
void graphe_stable(int n);
void graphe_cycle(int n);
void graphe_biparti_complet(int m, int p);
void graphe_alea(int n, double p);
int lire_dot(const char *nom_fichier);
int a_triangle(int n);

int main()
{
	// srand(time(NULL));
	// graphe_alea(20, 0.7);
	// afficher_mat(20);
	// ecrire_dot(20, "matrice_adj.dot");
	lire_dot("matrice_adj.dot");
	afficher_mat(20);

	return 0;
}

void afficher_mat(int n)
{
	int i, j;
	for (i = 0; i < n; ++i)
		for (j = 0; j < n; ++j) {
			printf("%d", mat[i][j]);
			if (j == n - 1)
				printf("\n");
			else
				printf("\t");
		}
	printf("\n");
}

void graphe_complet(int n)
{
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			if (i == j) {
				mat[i][j] = 0;
			} else {
				mat[i][j] = 1; 
			}
		}
	}
}

void graphe_stable(int n)
{
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			mat[i][j] = 0;
		}
	}
}


int ecrire_dot(int n, const char *nom_fichier)
{
    FILE* f = fopen(nom_fichier, "w");

    if (f == NULL) {
        perror("open");
        exit(1);
    }

    fprintf(f, "graph {\n");

	int i,j;
	for (i = 0; i < n; i++) {
		fprintf(f, "\t%d;\n", i);
	}

    fprintf(f, "\n");
	for (i = 0; i < n; i++)
		for (j = i; j < n; j++)
			if (mat[i][j])
				fprintf(f, "\t%d -- %d;\n", i, j);

    fprintf(f, "}\n");
    fclose(f);

    return 0;
}

void graphe_cycle(int n)
{
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			if (j - i == 1 || i - j == 1) {
				mat[i][j] = 1;
			} else {
				mat[i][j] = 0; 
			}
		}
	}
	mat[0][n - 1] = 1;
	mat[n - 1][0] = 1;
}

void graphe_biparti_complet(int m, int p)
{
	graphe_stable(m + p);
	int i, j;
	for (i = 0; i < m; i++) {
		for (j = 0; j < p; j++) {
			mat[i][j + m] = 1;
			mat[j + m][i] = 1;
		}
	}
}

void graphe_alea(int n, double p)
{
	int i, j;
	graphe_stable(n);
	for (i = 0; i < n; i++) {
		for (j = i + 1; j < n; j++) {
			if (((double) rand() / RAND_MAX) <= p) {
				mat[i][j] = 1;
				mat[j][i] = 1;
			} else {
				mat[i][j] = 0; 
				mat[j][i] = 0; 
			}
		}
	}
}

void multi_alea(int n, double p, int a_boucles)
{
	int i, j;
	graphe_stable(n);
	for (i = 0; i < n; i++) {
		for (j = i + 1; j < n; j++) {
			if (!a_boucles && i == j) continue;
			mat[i][j] = 0;
			mat[j][i] = 0;
			while (((double) rand() / RAND_MAX) <= p) {
				mat[i][j] += 1;
				if (i != j)
					mat[j][i] += 1;
			}
		}
	}
}

int lire_dot(const char *nom_fichier)
{
	FILE* f = fopen(nom_fichier, "r");
	size_t len = 0;
	char *str = NULL;

	if (f == NULL)
		return -1;

	getline(&str, &len, f);

	int i, j, n = 0;
	while (getline(&str, &len, f) > 1) {
		sscanf(str, "\t%d;\n", &i);
		printf("%d\n", i);
		if (i > n) n = i;
	}

	if (n >= ORDRE_MAX)
		return -2;

	graphe_stable(n);

	while (getline(&str, &len, f) > 1) {
		printf("%s", str);
		sscanf(str, "\t%d -- %d;\n", &i, &j);
		printf("%d %d\n", i, j);
		mat[i][j] = 1;
		mat[j][i] = 1;
	}

	free(str);

	fclose(f);

	return 0;
}

// O(n³)
int a_triangle(int n) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (mat[i][j] && i != j) {
				for (int k = 0; k < n; k++) {
					if (mat[j][k] && mat[k][i]] && i != k && j != k) {
						return 1;
					}
				}
			}
		}
	}
	return 0;
}
