/**
 * \file graphe-4.c
 * \brief Représentation des graphes par liste de successeurs
 * \version 1
 * \date lundi 4 novembre 2019
 * \authors Pierre Rousselin, Antoine Rozenknop, Sophie Toulouse
 * Rev 2022 : F. Roupin
*/
#include "graphe-4.h"

#include <stdio.h>
#include <stdlib.h>

/* __________________________________ Accesseurs sur les maillons */
msuc *msuc_suivant(msuc *m)
{
	return m->suivant;
}

int msuc_sommet(msuc *m)
{
	return m->sommet;
}

double msuc_valeur(msuc *m)
{
	return m->valeur;
}


/* __________________________________ Initialisation / Destruction */

int graphe_stable(graphe* g, int n, int est_or)
{
	if (n < 0)
		return -2;
	g->n = n;
	g->m = 0;
	g->tab_sucs = (msuc**) malloc(n * sizeof(msuc*));
	if (g->tab_sucs == NULL)
		return -1;
	g->est_or = est_or;
    return 0;
}

void graphe_detruire(graphe *g)
{
	msuc *m, *temp;
	for (int i = 0; i < g->n; i++) {
		m = g->tab_sucs[i];
		while (m != NULL) {
			temp = m;
			m = msuc_suivant(m);
			free(temp);
		}
	}
	free(g->tab_sucs);
}
/* __________________________________ Ajout / Suppression d'arêtes */

/* Attention :
 * Si le graphe est orienté, utiliser les fonctions _arc
 * Si le graphe est non orienté, utiliser les fonction _arete
 */

int graphe_ajouter_arc(graphe *g, int v, int w, double val)
{
	if (v >= g->n || v < 0 || w >= g->n || w < 0)
		return -2;
	msuc* m = (msuc*) malloc(sizeof(msuc));
	if (m == NULL)
		return -1;
	m->sommet = w;
	m->valeur = val;
	m->suivant = g->tab_sucs[v];
	g->tab_sucs[v] = m;
	g->m++;
	return 0;
}

int graphe_ajouter_arete(graphe* g, int v, int w, double val)
{
	if (v >= g->n || v < 0 || w >= g->n || w < 0)
		return -2;
	msuc* m = (msuc*) malloc(sizeof(msuc));
	if (m == NULL)
		return -1;
	m->sommet = w;
	m->valeur = val;
	m->suivant = g->tab_sucs[v];
	g->tab_sucs[v] = m;
	if (v != w) {
		msuc* n = (msuc*) malloc(sizeof(msuc));
		if (n == NULL)
			return -1;
		n->sommet = v;
		n->valeur = val;
		n->suivant = g->tab_sucs[w];
		g->tab_sucs[w] = n;
	}
	g->m++;
	return 0;
}

int graphe_supprimer_arc(graphe *g, int v, int w, double val)
{
	msuc* m = graphe_get_prem_msuc(g, v);
	if (msuc_sommet(m) == w && msuc_valeur(m) == val) {
		g->tab_sucs[v] = msuc_suivant(m);
		free(m);
		g->m--;
		return 0;
	}
	msuc* prec = m;
	m = msuc_suivant(m);
	while (m != NULL) {
		if (msuc_sommet(m) == w && msuc_valeur(m) == val) {
			prec->suivant = msuc_suivant(m);
			free(m);
			g->m--;
			return 0;
		}
		prec = m;
		m = msuc_suivant(m);
	}
	return -1;
}


int graphe_supprimer_arete(graphe* g, int v, int w, double val)
{
	if (v == w)
		return graphe_supprimer_arc(g, v, w, val);
	if (graphe_supprimer_arc(g, v, w, val) == 0) {
		g->m++; // Pour rééquilibrer, sinon, on a un double --.
 		return graphe_supprimer_arc(g, w, v, val);
	}
	return -1;
}

/* ______________________________________ Accesseurs en lecture */
int graphe_est_or(graphe *g)
{
	return g->est_or;
}

int graphe_get_n(graphe* g)
{
	return g->n;
}

int graphe_get_m(graphe* g)
{
	return g->m;
}

msuc *graphe_get_prem_msuc(graphe *g, int v)
{
    return g->tab_sucs[v];
}

/* NE PAS ACCÉDER DIRECTEMENT AUX CHAMPS EN-DESSOUS DE CETTE LIGNE */

int graphe_get_multiplicite_arc(graphe* g, int v, int w)
{
	int i = 0;
	msuc* m = graphe_get_prem_msuc(g, v);
	while (m != NULL) {
		if (msuc_sommet(m) == w)
			i++;
		m = msuc_suivant(m);
	}
	return i;
}

int graphe_get_multiplicite_arete(graphe* g, int v, int w)
{
	// Ne devrait pas être utilisé dans ce cas, mais bon, les tests l'utilisent :
	if (graphe_est_or(g) && v != w)
		return graphe_get_multiplicite_arc(g, v, w) + graphe_get_multiplicite_arc(g, w, v);
	return graphe_get_multiplicite_arc(g, v, w);
}

int graphe_get_degre_sortant(graphe* g, int v)
{
	int d = 0;
	msuc* m = graphe_get_prem_msuc(g, v);
	while (m != NULL) {
		d++;
		if (!graphe_est_or(g) && msuc_sommet(m) == v)
			d++;
		m = msuc_suivant(m);
	}
	return d;
}

int graphe_get_degre_entrant(graphe* g, int w)
{
	if (!graphe_est_or(g))
		return graphe_get_degre_sortant(g, w);
	int d = 0;
	for (int v = 0; v < graphe_get_n(g); v++)
		d += graphe_get_multiplicite_arc(g, v, w);
    return d;
}

int graphe_get_degre(graphe *g, int v)
{
	if (graphe_est_or(g))
		return graphe_get_degre_entrant(g, v) + graphe_get_degre_sortant(g, v);
	else
		return graphe_get_degre_sortant(g, v);
}

/* ______________________________________ Entrées / Sorties */

void graphe_afficher(graphe* g)
{
	int v, w;
	msuc *m;
	int n = graphe_get_n(g);
	printf("graphe %s d'ordre %d à %d aretes :\n",
		graphe_est_or(g) ? "orienté" : "non orienté",
		graphe_get_n(g), graphe_get_m(g));
	for (v = 0; v < n; ++v) {
		puts("successeurs de v : ");
		for (m = graphe_get_prem_msuc(g, v); m; m = msuc_suivant(m)) {
			w = msuc_sommet(m);
			printf("%2d, ", w);
		}
		puts("");
	}
}

int graphe_ecrire_dot(graphe *g, char *nom_fichier, int ecrire_valeurs)
{
	int u;
	int n = graphe_get_n(g);
	FILE *f = fopen(nom_fichier, "w");
	int est_or = graphe_est_or(g);
	msuc *m;
	if (!f) { perror("fopen"); return -1; }

	if (est_or)
		fputs("digraph {\n", f);
	else
		fputs("graph {\n", f);

	for (u = 0; u < n; ++u)
		fprintf(f, "\t%d;\n", u);
	fputs("\n", f);

	for (u = 0; u < n; ++u)
		for (m = graphe_get_prem_msuc(g, u); m; m = msuc_suivant(m)) {
			int v = msuc_sommet(m);
			double val = msuc_valeur(m);
			if (!est_or && v < u) /* arête déjà rencontrée */
				continue;
			fprintf(f, "\t%d -%c %d ",
					u, est_or ? '>' : '-', v);
			if (ecrire_valeurs)
				fprintf(f, " [label = %.2f]", val);
			fprintf(f, ";\n");
		}
	fputs("}\n", f);

	fclose(f);
	return 0;
}

/* ______________________________________ Autres fonctions d'initialisation */
int graphe_complet(graphe* g, int n) {
	int stat =-1;
	int v, w;

	stat =graphe_stable(g, n, 0);
	if(! stat) {
		for (v =0 ; v <graphe_get_n(g) -1 && !stat ; ++v)
			for (w =v +1 ; w <graphe_get_n(g)  && !stat ; ++w)
				stat =graphe_ajouter_arete(g, v, w, 1.);
			
		if(stat)
			graphe_detruire(g);
	}

    /* on retourne le statut de réussite de la fonction */
	return stat;
}

int graphe_aleatoire(graphe* g, int n, double p, int est_or)
{
	int v, w;
	if (n < 0)
		return -2;
	if (p < 0 || p > 1)
		return -3;
	if (graphe_stable(g, n, est_or) < 0)
		return -1;
	if (!est_or) {
		for (v = 0; v < graphe_get_n(g); ++v)
			for (w = v; w < graphe_get_n(g); ++w)
				if ((double) rand()/RAND_MAX < p)
					if (graphe_ajouter_arete(g, v, w, 0.) < 0) {
						graphe_detruire(g);
						return -1;
					}
	} else {
		for (v = 0; v < graphe_get_n(g); ++v)
			for (w = 0; w < graphe_get_n(g); ++w)
				if ((double) rand()/RAND_MAX < p)
					if ( graphe_ajouter_arc(g, v, w, 0.) < 0) {
						graphe_detruire(g);
						return -1;
					}
	}
	return 0;
}

int graphe_aleatoire_multi(graphe* g, int n, double p, int est_or)
{
	int v, w;
	if (n < 0)
		return -2;
	if (p < 0 || p >= 1)
		return -3;
	if (graphe_stable(g, n, est_or) < 0)
		return -1;
	if (!est_or) {
		for (v = 0; v < graphe_get_n(g); ++v)
			for (w = v; w < graphe_get_n(g); ++w)
				while ((double) rand()/RAND_MAX < p)
					if (graphe_ajouter_arete(g, v, w, 0.) < 0) {
						graphe_detruire(g);
						return -1;
					}
	} else {
		for (v = 0; v < graphe_get_n(g); ++v)
			for (w = 0; w < graphe_get_n(g); ++w)
				while ((double) rand()/RAND_MAX < p)
					if (graphe_ajouter_arc(g, v, w, 0.) < 0) {
						graphe_detruire(g);
						return -1;
					}
	}
	return 0;
}
