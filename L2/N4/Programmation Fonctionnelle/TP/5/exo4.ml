let rec pliage_droit f l a = match l with
    [] -> a
    | x::ll -> f x (pliage_droit f ll a);;

let applatir l = pliage_droit (@) l [];;

let map_bis f l =
    let m f' e l' =
        (f' e)::l'
    in pliage_droit (m f) l [];;
