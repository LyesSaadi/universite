let rec filtre p l = match l with
    [] -> []
    | x::ll -> if (p x) then x::(filtre p ll) else (filtre p ll);;

let mult_3 l = let mult x = (x mod 3) == 0 in filtre mult l;;

let liste_impaires l =
    let long_imp l' =
        let rec long l'' = match l'' with
            [] -> 0
            | e::ll -> (long ll) + 1
        in ((long l') mod 2) == 1
    in filtre long_imp l;;
