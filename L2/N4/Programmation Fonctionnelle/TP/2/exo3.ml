let rec decouper_bis l = match l with
    [] ->
        [],[]
    | e::ll ->
        let
            (a, b) = decouper_bis ll
        in
            (e::b,a);;

let rec fusion l1 l2 = match l1,l2 with
    [],_ -> l2
    | _,[] -> l1
    | e1::ll1,e2::ll2 ->
        if e1 < e2 then
            e1::(fusion ll1 l2)
        else
            e2::(fusion l1 ll2);;

let rec tri_fusion l = match l with
    [] -> []
    | e::[] -> e::[]
    | _ ->
        let
            (a, b) = decouper_bis l
        in
            (fusion (tri_fusion a) (tri_fusion b));;
