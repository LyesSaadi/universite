---
title: Projet de programmation fonctionnelle
subtitle: L2 Double Licence
author: Corentin Leroy-Bury (12102536) — Lyes Saadi (12107246)
date: "16 Mai 2023"
header-includes: |
    \usepackage{fancyhdr}
    \usepackage{lastpage}
    \pagestyle{fancy}
    \fancyfoot[CO,CE]{}
    \fancyfoot[LO,RE]{Corentin Leroy-Bury (12102536) \& Lyes Saadi (12107246)}
    \fancyfoot[LE,RO]{\thepage/\pageref{LastPage}}
geometry: "right=2cm,left=2cm,top=4cm,bottom=4cm"
output: pdf_document
...

# Présentation

## Unification et Anti-Unification

L'Unification de termes a des applications multiples en informatique, que ce
soit en typage et en inférence de type dans des langages fonctionnels tel Ocaml
ou Rust. En effet, ça nous permet de connaître les contraintes sur les
différents termes de premier ordre, et donc de pouvoir déterminer la validité
de nos types, et éventuellement d'inférer ces types.

L'Anti-Unification, de son côté, est l'opération inverse, le terme général
englobant nos deux termes.

![Unification et anti-unification](/var/home/lyes/Documents/Université/L2/N4/Programmation Fonctionnelle/Projet/unif.png){height=5cm}

\pagebreak

En cela, nous pouvons représenter l'Unification et l'Anti-Unification sous forme
de treillis.

![Treillis](/var/home/lyes/Documents/Université/L2/N4/Programmation Fonctionnelle/Projet/treillis.png){witdth=3cm height=3cm}


## Solution Algorithmique

La solution algorithmique est celle présentée en cours.

### Unification

Soit $e \in \epsilon = \{ t_i = t'_i / i = [1, n]\ \text{et}\ t_i, t'_i \in M(F,V) \}$

Tant qu'il existe une équation $e$ de la forme :

1. $t = t$ : on supprime $e$.

1. $T = X$ et $T \not \in V$ : remplacer $e$ par $X = t$.

1. $F(t_1, ..., t_n) = G(t'_1, ..., t'_n)$ : Si $F \not \equiv G$,
    alors Echec. Sinon, remplacer $e$ par $(t_1 = t'_1, ..., t_n = t'_n)$.

1. $X = t$ et $X$ a plus d'une occurence dans $\epsilon$ : Si $X$ appartient
   dans $t$, alors Echec. Sinon, garder $e$ et remplacer toutes les autres
   occurences de $X$ par $t$.

### Anti-unification

- Si $t \equiv F(t_1, ..., t_n)$ et $t' \equiv F(t'_1, ..., t'_n)$ :
    Alors, return $F(anti\_unif(t_1, t'_1), ..., anti\_unif(t_n, t'_n))$.

- Sinon Si il existe dans `LISTE` un couple $((t, t'), i)$ : Alors, return
  $Z_i$.

- Sinon, Alors :
    + Générer une nouvelle variable $Z_j$.
    + Ajouter à `LISTE` le couble $((t, t'), j)$.
    + Return $Z_j$.


# Implémentation

## Choix d'implémentation

### Types

On a défini $M(F, V)$, l'ensemble des termes. On va donc représenter ce $M(F,V)$
dans une sorte d'union d'un type F qui est un couple constitué d'un caractère
(nom du symbole de fonction) et d'une liste d'arguments dont la taille nous
donnera l'arité, et dont les composants appartiennent eux-même à $M(F,V)$ ainsi
qu'un type V qui n'est composé que d'un entier, qui définiera notre variable. Le
choix du caractère pour F est pour se rapproche le plus possible de nos symboles
de fonctions réels qu'on nomme généralement f, g, h, etc... Le choix de l'entier
pour V se fait pour l'anti-unification qui nous demande de créer à l'occasion
une nouvelle variable, ce qui est plus simple avec un entier qu'avec un
caractère par exemple.

### Erreurs

L'unification étant un processus qui peut échouer, ils nous faut pouvoir
représenter les Erreurs. On a donc créer une exception `FailedToUnifyException`
et nos fonction `unif` et `anti_unif` renvoient un `Result`, type de la
librairie standard qui renvoie soit `Ok`, soit `Error`. Pour pouvoir imbriquer
les appels à ces fonctions, `unif'` et `anti_unif'` renvoient le résultat
directement, au risque de soulever l'exception.

### Algorithme

L'algorithme implémenté est basé sur l'algorithme général du cours avec certains
raccourcis permis par la façon dont le code est structuré.

Par exemple, pour l'unification, on va essayer de substituer ce qu'on peut
substituer immédiatement.

#### Unification

Regardons le cœur de l'unification :

```ocaml
(* Unifie deux termes, et renvoie un Result avec le résultat. *)
let rec unif t1 t2 =
    (* Met les termes dans une forme intermédiaire et définit les subsitutions à réaliser.
        - Combine les fonctions.
        - Unifie les variables égales.
        - Unifie les variables avec les termes de premier ordre. *)
    let rec unif' t1 t2 subs = match t1,t2 with
        (* Cas 3 : On unifie les symboles de fonctions. *)
        F(n, a),F(m, b) ->
            if n = m && (arite a) == (arite b) then
                let rec combiner a b = match a,b with
                    (* Impossible d'éviter l'utilisation du get_ok ici *)
                    e::aa,f::bb -> (Result.get_ok (unif e f))::(combiner aa bb)
                    | _ -> []
                in
                Ok(F(n, combiner a b),subs)
            else
                Error(FailedToUnifyException("Impossible d'unifier les fonctions"))
```

On va déjà essayer d'unifier les deux termes dans les cas où on a des symboles
de fonction. On vérifie déjà que les deux fonctions ont le même nom et la même
arité, sinon, on renvoie l'erreur signalant un échec.

Ensuite, on va appliquer l'unification sur chacun des arguments deux à deux et
récupérer le résultat qui seront nos nouveaux arguments.

En soit, on ne rajoute aucune autre opération de substitution, en effet, dans le
Cas 3 de l'algorithme, on nous demande de supprimer $e$.

```ocaml
        (* Si les V(i) sont égaux, on retourne V(i) (Cas 1, en quelque sorte).
           Sinon, on substitue tous les V(i) par V(i'). *)
        | V(i),V(i') -> if i = i' then Ok(V(i),subs) else Ok(V(i'),(V(i),V(i'))::subs)
``` 

Ici on traite le cas où on doit unifier deux variables. Si ces variables sont
les même, on est dans le Cas 1, et on ne substitue rien (vu qu'on nous demande
de supprimer $e$). Sinon, si ces variables sont différente, on garde la
variable `V(i)`, et on rajoute une substitution des `V(i)` par `V(i')`.

```ocaml
        (* Cas 4 : On substitue tous les V(i). *)
        | V(i),t ->
            (* On vérifie que V(i) n'appartient pas à t. *)
            if (verif t i) then
                Error(FailedToUnifyException(
                    "Impossible d'unifier la variable avec le terme du premier ordre"
                ))
            else
                Ok(t,(V(i),t)::subs)
```

Si on a une expression du type `Variable = Terme`, on garde `t`, et on substitue
tous les `V(i)` par `t`. On vérifie aussi que $V(i) \not \in t$, si c'est
le cas, on renvoie une erreur.

```ocaml
        (* Cas 2, en quelque sorte... *)
        | t,V(i) -> (unif' (V(i)) t subs)
    in
```

Dans le cas final où `Terme = Variable`, conformément au cas 2, on inverse
l'égalité et on refusionne ça.

```ocaml
    (* On vérifie qu'on a pas une répétition de substitutions in-unifiables entre elles.
        - Exemple, on ne veut pas unifier X = f(a, b) et X = a. *)
    let rec verif_repetition subs = match subs with
        [] -> false
        | e::subs' -> let (x, t) = e in
            (* On traverse la liste pour vérifier à chaque fois qu'un X ne se répète pas. *)
            let rec traverse s =
                match s with
                    [] -> false
                    | e'::s' -> let (x', t') = e' in
                        (if (x = x') then
                            Result.is_error (unif t t')
                        else
                            false) || (traverse s')
            in (traverse subs') || (verif_repetition subs')
    in
```

Ensuite, avant d'appliquer nos substitutions, on veut déjà vérifier que nous
n'avons pas des substitutions en double. Si on a par exemple `(X, f(a))` et `(X,
f(b))`, on doit faire échouer la substitution.

```ocaml
    (* On applique les subsitutions. *)
    match (unif' t1 t2 []) with
        Ok(tm, subs) ->
            if (verif_repetition subs) then
                Error(FailedToUnifyException("Repetition in substitution"))
            else
                let rec f t s =
                    match s with
                        [] -> t
                        | e::s' -> let (t', t'') = e in
                            f (substitution t t' t'') s'
                in Ok(f tm subs)
        | Error(e) -> Error(e);;
```

Enfin, ici on regroupe tout notre travail, en prenant le résultat de nos
substitutions partielles, et en y appliquant le reste des substitutions.

#### Anti-unification

L'anti-unification de son côté est plus court, mais plus dense. On rencontre
d'emblée un problème lorsque l'on essaye de créer une liste des variables qui
regroupent deux autres variables. On ne peut muter une variable en OCaml. Pour
ça, on va donc devoir créer une fonction intermédiare qui va toujours renvoyer
notre liste mise à jour. On garde aussi un entier pour connaitre le prochain
entier à utiliser pour la variable.

```ocaml
(* Anti-unifie un terme. *)
let rec anti_unif t1 t2 =
    (* On défini une fonction intermédiaire qui garde en mémoire l'indice de la variable à
       laquelle on est arrivé et des couples de termes associés aux variables. *)
    let rec anti_unif' t1 t2 i vars =
        (* Si on a égalité entre les termes, pas besoin d'agir. *)
        if (t1 = t2) then
            Ok(t1, i, vars)
```

On vérifie déjà que les termes ne sont pas égaux. S'ils sont égaux, on passe,
on a rien à faire.

```ocaml
        else
            match t1,t2 with
            (* Si on a deux symboles de fonctions, on vérifie que ces dernières sont
               compatibles et on anti-unifie tout ses arguments. *)
            F(n, a),F(m, b) ->
                if n = m && (arite a) = (arite b) then
                    let rec traverse_anti_unif a b i vars = match a,b with
                        e::aa,e'::bb ->
                            (* Impossible d'éviter l'utilisation du get_ok ici *)
                            let (r, j, v) = Result.get_ok (anti_unif' e e' i vars) in
                                let (l, k, w) = (traverse_anti_unif aa bb j v) in (r::l, k, w)
                        | _ -> ([], i, vars)
                    in
                    let (r, j, v) = traverse_anti_unif a b i vars in Ok(F(n, r), j, v)
                else
                    Error(FailedToUnifyException("Impossible d'anti-unifier"))
```

Si on anti-unifie une fonction, alors, on anti-unifie chacun de ses paramètres.
On vérifie bien sûr que les deux fonctions soient égales en nom et en arité.

```ocaml
            (* Sinon, on va remplacer nos termes par des variables. *)
            | _,_ -> match (recup_num (t1,t2) vars) with
                (* Si on a déjà remplacer ces termes, on récupère la variable. *)
                Some(j) -> Ok(V(j), i, vars)
                (* Sinon, on redéfini une nouvelle variable qu'on rajoute dans notre liste. *)
                | None -> Ok(V(i), i + 1, ((t1,t2),i)::vars)
    in
    match (anti_unif' t1 t2 (max (num_var t1 0) (num_var t2 0)) []) with
        Ok(r, _, _) -> Ok(r)
        | Error(e) -> Error(e);;
```

Maintenant, si on a n'importe quel autre combinaison de termes, on va essayer de
les anti-unifier derrière une variable. Pour celà, on va vérifier avec
`recup_num` si cette dernière n'existe pas déjà dans nos variables dans le
tableau `vars`. Si c'est le cas, on remplace par la variable correspondante,
sinon, on crée notre nouvelle variable.

## Problèmes rencontrés

### Gestion d'erreurs

Pour gérer les erreurs en OCaml, on a étudier plusieurs options en consultant la
documentation de OCaml : `failswith` ou `raise` par exemple. Mais, on a décidé
de renvoyer à la place un `Result` qui représente soit un résultat valide (`Ok`)
ou une erreur (`Error`).

Mais, ce choix ne peux être respecté dans deux cas dans le code sans introduire
des changements majeurs qui pourraient impacter négativement la lisibilité du code :
```ocaml
                    e::aa,f::bb -> (Result.get_ok (unif e f))::(combiner aa bb)
```
et
```ocaml
                            let (r, j, v) = Result.get_ok (anti_unif' e e' i vars) in
```

### Anti-unif demandant d'utiliser des entiers pour les variables

Initialement, on avait choisi des caractères pour identifier les variables.
Mais, utiliser des entiers simplifie enormément la création de nouvelles
variables dans anti-unif, on a qu'à incrémenter une valeurs :

```ocaml
                (* Sinon, on redéfini une nouvelle variable qu'on rajoute dans notre liste. *)
                | None -> Ok(V(i), i + 1, ((t1,t2),i)::vars)
```

### Éviter les conflits avec des variables existantes

Si on commençais la numérotations de nos variables par 1, on risque de créer des
conflits avec des variables existantes dans les termes originaux qui seraient
gardé par le code. On a donc décidé de prendre la valeur la plus élevée parmis
nos variables comme début de numérotation.

```ocaml
(* Cherche la variable de numéro le plus élevé pour éviter les conflits. *)
let rec num_var' t m = match t with
    V(i) -> max i m
    | F(_, a) ->
        let rec traverse a m = match a with
            [] -> m
            | e::aa -> max (num_var' e m) (traverse aa m)
        in traverse a m;;

(* On incrémente pour éviter le conflit avec V(i) *)
let rec num_var t m = (num_var' t m) + 1;;
```

### Garder les valeurs de `i` et `vars` dans anti-unif

L'algorithme de anti-unif nous demande de stocker nos variables dans `LISTE`.
Or, nous ne pouvons pas muter une variable globalement et changer son état. On
doit donc à chaque fois retourner notre `LISTE` mise à joue.

# Code

```ocaml
(* Notre type m(F, V) qui définit un terme du premier ordre comme étant :
    soit une fonction avec son nom et une liste d'arguments
    soit une variable  *)
type m = F of char * m list | V of int;;

(* Exception levée en cas d'impossibilité d'unification. *)
exception FailedToUnifyException of string;;

(* Détermine l'arité d'un symbole de fonction en fonction de son nombre d'arguments. *)
let rec arite a = match a with
    [] -> 0
    | e::aa -> 1 + (arite aa);;

(* Vérifie l'existence d'une Variable c dans un Terme t.
    - Renvoie faux s'il ne trouve pas d'occurence de c.
    - Renvoie vrai s'il trouve une occurence de c. *)
let rec verif t i = 
    (* Traverse les arguments d'une fonction pour vérifier chaque terme. *)
    let rec traverse_verif a i = match a with
        [] -> false
        | e::aa -> (verif e i) || (traverse_verif aa i)
    in
    match t with
        F(_, a) -> traverse_verif a i
        | V(i') -> i' = i;;

(* Subtitue dans t toutes les occurences de t1 par t2. *)
let rec substitution t t1 t2 =
    (* Traverse les arguments d'une fonction pour substituer chaque terme. *)
    let rec traverse_substitution a t1 t2 = match a with
        [] -> []
        | e::aa -> (substitution e t1 t2)::(traverse_substitution aa t1 t2)
    in
    if t = t1 then
        t2
    else
        match t with
            F(n, a) -> F(n, traverse_substitution a t1 t2)
            | m -> m;;

(* Unifie deux termes, et renvoie un Result avec le résultat. *)
let rec unif t1 t2 =
    (* Met les termes dans une forme intermédiaire et définit les subsitutions à réaliser.
        - Combine les fonctions.
        - Unifie les variables égales.
        - Unifie les variables avec les termes de premier ordre. *)
    let rec unif' t1 t2 subs = match t1,t2 with
        (* Cas 3 : On unifie les symboles de fonctions. *)
        F(n, a),F(m, b) ->
            if n = m && (arite a) == (arite b) then
                let rec combiner a b = match a,b with
                    (* Impossible d'éviter l'utilisation du get_ok ici *)
                    e::aa,f::bb -> (Result.get_ok (unif e f))::(combiner aa bb)
                    | _ -> []
                in
                Ok(F(n, combiner a b),subs)
            else
                Error(FailedToUnifyException("Impossible d'unifier les fonctions"))
        (* Si les V(i) sont égaux, on retourne V(i) (Cas 1, en quelque sorte).
           Sinon, on substitue tous les V(i) par V(i'). *)
        | V(i),V(i') -> if i = i' then Ok(V(i),subs) else Ok(V(i'),(V(i),V(i'))::subs)
        (* Cas 4 : On substitue tous les V(i). *)
        | V(i),t ->
            (* On vérifie que V(i) n'appartient pas à t. *)
            if (verif t i) then
                Error(FailedToUnifyException(
                    "Impossible d'unifier la variable avec le terme du premier ordre"
                ))
            else
                Ok(t,(V(i),t)::subs)
        (* Cas 2, en quelque sorte... *)
        | t,V(i) -> (unif' (V(i)) t subs)
    in
    (* On vérifie qu'on a pas une répétition de substitutions in-unifiables entre elles.
        - Exemple, on ne veut pas unifier X = f(a, b) et X = a. *)
    let rec verif_repetition subs = match subs with
        [] -> false
        | e::subs' -> let (x, t) = e in
            (* On traverse la liste pour vérifier à chaque fois qu'un X ne se répète pas. *)
            let rec traverse s =
                match s with
                    [] -> false
                    | e'::s' -> let (x', t') = e' in
                        (if (x = x') then
                            Result.is_error (unif t t')
                        else
                            false) || (traverse s')
            in (traverse subs') || (verif_repetition subs')
    in
    (* On applique les subsitutions. *)
    match (unif' t1 t2 []) with
        Ok(tm, subs) ->
            if (verif_repetition subs) then
                Error(FailedToUnifyException("Repetition in substitution"))
            else
                let rec f t s =
                    match s with
                        [] -> t
                        | e::s' -> let (t', t'') = e in
                            f (substitution t t' t'') s'
                in Ok(f tm subs)
        | Error(e) -> Error(e);;

(* Unif qui crash à la place de revoyer un Result. *)
let unif' t1 t2 = Result.get_ok (unif t1 t2);;

(* Détermine le maximum entre deux nombres. *)
let max i j = if i < j then j else i;;

(* Cherche la variable de numéro le plus élevé pour éviter les conflits. *)
let rec num_var' t m = match t with
    V(i) -> max i m
    | F(_, a) ->
        let rec traverse a m = match a with
            [] -> m
            | e::aa -> max (num_var' e m) (traverse aa m)
        in traverse a m;;

(* On incrémente pour éviter le conflit avec V(i) *)
let rec num_var t m = (num_var' t m) + 1;;

(* Récupère le numéro lié à l'unif des variables. *)
let rec recup_num t vars = match vars with
    [] -> None
    | (tt, i)::v -> if t = tt then Some(i) else recup_num t v;;

(* Anti-unifie un terme. *)
let rec anti_unif t1 t2 =
    (* On défini une fonction intermédiaire qui garde en mémoire l'indice de la variable à
       laquelle on est arrivé et des couples de termes associés aux variables. *)
    let rec anti_unif' t1 t2 i vars =
        (* Si on a égalité entre les termes, pas besoin d'agir. *)
        if (t1 = t2) then
            Ok(t1, i, vars)
        else
            match t1,t2 with
            (* Si on a deux symboles de fonctions, on vérifie que ces dernières sont
               compatibles et on anti-unifie tout ses arguments. *)
            F(n, a),F(m, b) ->
                if n = m && (arite a) = (arite b) then
                    let rec traverse_anti_unif a b i vars = match a,b with
                        e::aa,e'::bb ->
                            (* Impossible d'éviter l'utilisation du get_ok ici *)
                            let (r, j, v) = Result.get_ok (anti_unif' e e' i vars) in
                                let (l, k, w) = (traverse_anti_unif aa bb j v) in (r::l, k, w)
                        | _ -> ([], i, vars)
                    in
                    let (r, j, v) = traverse_anti_unif a b i vars in Ok(F(n, r), j, v)
                else
                    Error(FailedToUnifyException("Impossible d'anti-unifier"))
            (* Sinon, on va remplacer nos termes par des variables. *)
            | _,_ -> match (recup_num (t1,t2) vars) with
                (* Si on a déjà remplacer ces termes, on récupère la variable. *)
                Some(j) -> Ok(V(j), i, vars)
                (* Sinon, on redéfini une nouvelle variable qu'on rajoute dans notre liste. *)
                | None -> Ok(V(i), i + 1, ((t1,t2),i)::vars)
    in
    match (anti_unif' t1 t2 (max (num_var t1 0) (num_var t2 0)) []) with
        Ok(r, _, _) -> Ok(r)
        | Error(e) -> Error(e);;

(* Anti_unif qui crash à la place de revoyer un Result. *)
let anti_unif' t1 t2 = Result.get_ok (anti_unif t1 t2);;
```

# Essais

```ocaml
# #use "projet.ml";;
type m = F of char * m list | V of int
exception FailedToUnifyException of string
val arite : 'a list -> int = <fun>
val verif : m -> int -> bool = <fun>
val substitution : m -> m -> m -> m = <fun>
val unif : m -> m -> (m, exn) result = <fun>
val unif' : m -> m -> m = <fun>
val max : 'a -> 'a -> 'a = <fun>
val num_var' : m -> int -> int = <fun>
val num_var : m -> int -> int = <fun>
val recup_num : 'a -> ('a * 'b) list -> 'b option = <fun>
val anti_unif : m -> m -> (m, exn) result = <fun>
val anti_unif' : m -> m -> m = <fun>
# unif (V(1)) (V(2));;
- : (m, exn) result = Ok (V 2)
# unif (F('a', V(1)::[])) (V(2));;
- : (m, exn) result = Ok (F ('a', [V 1]))
# unif (F('a', V(1)::[])) (F('b', V(2)::[]));;
- : (m, exn) result =
Error (FailedToUnifyException "Impossible d'unifier les fonctions")
# unif (F('a', V(1)::[])) (F('a', F('b', [])::[]));;
- : (m, exn) result = Ok (F ('a', [F ('b', [])]))
# anti_unif (F('a', V(1)::[])) (V(2));;
- : (m, exn) result = Ok (V 3)
# anti_unif (F('a', V(1)::[])) (F('a', V(2)::[]));;
- : (m, exn) result = Ok (F ('a', [V 3]))
# anti_unif (F('a', F('a', [])::[])) (F('a', V(1)::[]));;
- : (m, exn) result = Ok (F ('a', [V 2]))
# anti_unif (F('a', F('a', [])::V(3)::[])) (F('a', V(1)::V(4)::[]));;
- : (m, exn) result = Ok (F ('a', [V 5; V 6]))
# anti_unif (F('a', V(3)::F('a', [])::V(3)::[])) (F('a', V(4)::V(1)::V(4)::[]));;
- : (m, exn) result = Ok (F ('a', [V 5; V 6; V 5]))
# anti_unif (F('a', V(3)::F('a', [])::V(3)::V(3)::[])) (F('a', V(4)::V(1)::V(4)::V(3)::[]));;
- : (m, exn) result = Ok (F ('a', [V 5; V 6; V 5; V 3]))
# (unif' (anti_unif' (F('a', (V(1))::[])) (F('a', (F('b', []))::[])))
         (unif' (V(1)) (F('a', (F('c', []))::[]))));;
- : m = F ('a', [F ('c', [])])
```
