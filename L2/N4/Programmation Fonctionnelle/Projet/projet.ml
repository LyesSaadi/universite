(* Notre type m(F, V) qui définit un terme du premier ordre comme étant :
    soit une fonction F avec son nom et une liste d'arguments
    soit une variable  *)
type m = F of char * m list | V of int;;

(* Exception levée en cas d'impossibilité d'unification. *)
exception FailedToUnifyException of string;;

(* Détermine l'arité d'un symbole de fonction en fonction de son nombre d'arguments. *)
let rec arite a = match a with
    [] -> 0
    | e::aa -> 1 + (arite aa);;

(* Vérifie l'existence d'une Variable c dans un Terme t.
    - Renvoie faux s'il ne trouve pas d'occurence de c.
    - Renvoie vrai s'il trouve une occurence de c. *)
let rec verif t i = 
    (* Traverse les arguments d'une fonction pour vérifier chaque terme. *)
    let rec traverse_verif a i = match a with
        [] -> false
        | e::aa -> (verif e i) || (traverse_verif aa i)
    in
    match t with
        F(_, a) -> traverse_verif a i
        | V(i') -> i' = i;;

(* Subtitue dans t toutes les occurences de t1 par t2. *)
let rec substitution t t1 t2 =
    (* Traverse les arguments d'une fonction pour substituer chaque terme. *)
    let rec traverse_substitution a t1 t2 = match a with
        [] -> []
        | e::aa -> (substitution e t1 t2)::(traverse_substitution aa t1 t2)
    in
    if t = t1 then
        t2
    else
        match t with
            F(n, a) -> F(n, traverse_substitution a t1 t2)
            | m -> m;;

(* Unifie deux termes, et renvoie un Result avec le résultat. *)
let rec unif t1 t2 =
    (* Mets les termes dans une forme intermédiaire et définie les subsitutions à réaliser.
        - Combine les fonctions.
        - Unifie les variables égales.
        - Unifie les variables avec les termes de premier ordre. *)
    let rec unif' t1 t2 subs = match t1,t2 with
        (* Cas 3 : On unifie les symboles de fonctions. *)
        F(n, a),F(m, b) ->
            if n = m && (arite a) == (arite b) then
                let rec combiner a b = match a,b with
                    (* Impossible d'éviter l'utilisation du get_ok ici *)
                    e::aa,f::bb -> (Result.get_ok (unif e f))::(combiner aa bb)
                    | _ -> []
                in
                Ok(F(n, combiner a b),subs)
            else
                Error(FailedToUnifyException("Impossible d'unifier les fonctions"))
        (* Si les V(i) sont égaux, on retourne V(i) (Cas 1, en quelque sorte).
           Sinon, on substitue tous les V(i) par V(i'). *)
        | V(i),V(i') -> if i = i' then Ok(V(i),subs) else Ok(V(i),(V(i),V(i'))::subs)
        (* Cas 4 : On substitue tous les V(i). *)
        | V(i),t ->
            (* On vérifie que V(i) n'appartient pas à t. *)
            if (verif t i) then
                Error(FailedToUnifyException(
                    "Impossible d'unifier la variable avec le terme du premier ordre"
                ))
            else
                Ok(t,(V(i),t)::subs)
        (* Cas 2, en quelque sorte... *)
        | t,V(i) -> (unif' (V(i)) t subs)
    in
    (* On vérifie qu'on a pas une répétition de substitutions in-unifiables entre elles.
        - Exemple, on ne veut pas unifier X = f(a, b) et X = a. *)
    let rec verif_repetition subs = match subs with
        [] -> false
        | e::subs' -> let (x, t) = e in
            (* On traverse la liste pour vérifier à chaque fois qu'un X ne se répète pas. *)
            let rec traverse s =
                match s with
                    [] -> false
                    | e'::s' -> let (x', t') = e' in
                        (if (x = x') then
                            Result.is_error (unif t t')
                        else
                            false) || (traverse s')
            in (traverse subs') || (verif_repetition subs')
    in
    (* On applique les subsitutions. *)
    match (unif' t1 t2 []) with
        Ok(tm, subs) ->
            if (verif_repetition subs) then
                Error(FailedToUnifyException("Repetition in substitution"))
            else
                let rec f t s =
                    match s with
                        [] -> t
                        | e::s' -> let (t', t'') = e in
                            f (substitution t t' t'') s'
                in Ok(f tm subs)
        | Error(e) -> Error(e);;

(* Unif qui crash à la place de revoyer un Result. *)
let unif' t1 t2 = Result.get_ok (unif t1 t2);;

(* Détermine le maximum entre deux nombres. *)
let max i j = if i < j then j else i;;

(* Cherche la variable de numéro le plus élevé pour éviter les conflits. *)
let rec num_var' t m = match t with
    V(i) -> max i m
    | F(_, a) ->
        let rec traverse a m = match a with
            [] -> m
            | e::aa -> max (num_var' e m) (traverse aa m)
        in traverse a m;;

(* On incrémente pour éviter le conflit avec V(i) *)
let rec num_var t m = (num_var' t m) + 1;;

(* Récupère le numéro lié à l'unif des variables. *)
let rec recup_num t vars = match vars with
    [] -> None
    | (tt, i)::v -> if t = tt then Some(i) else recup_num t v;;

(* Anti-unifie un terme. *)
let rec anti_unif t1 t2 =
    (* On défini une fonction intermédiaire qui garde en mémoire l'indice de la variable à
       laquelle on est arrivé et des couples de termes associés aux variables. *)
    let rec anti_unif' t1 t2 i vars =
        (* Si on a égalité entre les termes, pas besoin d'agir. *)
        if (t1 = t2) then
            Ok(t1, i, vars)
        else
            match t1,t2 with
            (* Si on a deux symboles de fonctions, on vérifie que ces dernières sont
               compatibles et on anti-unifie tout ses arguments. *)
            F(n, a),F(m, b) ->
                if n = m && (arite a) = (arite b) then
                    let rec traverse_anti_unif a b i vars = match a,b with
                        e::aa,e'::bb ->
                            (* Impossible d'éviter l'utilisation du get_ok ici *)
                            let (r, j, v) = Result.get_ok (anti_unif' e e' i vars) in
                                let (l, k, w) = (traverse_anti_unif aa bb j v) in (r::l, k, w)
                        | _ -> ([], i, vars)
                    in
                    let (r, j, v) = traverse_anti_unif a b i vars in Ok(F(n, r), j, v)
                else
                    Error(FailedToUnifyException("Impossible d'anti-unifier"))
            (* Sinon, on va remplacer nos termes par des variables. *)
            | _,_ -> match (recup_num (t1,t2) vars) with
                (* Si on a déjà remplacer ces termes, on récupère la variable. *)
                Some(j) -> Ok(V(j), i, vars)
                (* Sinon, on redéfini une nouvelle variable qu'on rajoute dans notre liste. *)
                | None -> Ok(V(i), i + 1, ((t1,t2),i)::vars)
    in
    match (anti_unif' t1 t2 (max (num_var t1 0) (num_var t2 0)) []) with
        Ok(r, _, _) -> Ok(r)
        | Error(e) -> Error(e);;

(* Anti_unif qui crash à la place de revoyer un Result. *)
let anti_unif' t1 t2 = Result.get_ok (anti_unif t1 t2);;
