#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#define N 1000

void *bosser(void *);
int incremente_moi = 0;

int main()
{
	sem_t sem_inc;
	if (sem_init(&sem_inc, 0, 1)) perror("sem_init");
	pthread_t th[N];
	int i;
	for (i = 0; i < N; ++i)
		pthread_create(&th[i], NULL, bosser, &sem_inc);
	for (i = 0; i < N; ++i)
		pthread_join(th[i], NULL);

	printf("incremente_moi = %d\n", incremente_moi);
	sem_destroy(&sem_inc);
	return 0;
}

void *bosser(void *arg)
{
	sem_t *sem_inc = (sem_t*) arg;
	int i;
	for (i = 0; i < N; ++i) {
		if (sem_wait(sem_inc)) perror("sem_wait");
		++incremente_moi;
		if (sem_post(sem_inc)) perror("sem_post");
	}
	return NULL;
}
