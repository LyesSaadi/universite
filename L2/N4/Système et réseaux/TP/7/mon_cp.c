#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFSZ 256

int main(int argc, char** argv) {
	if (argc != 3) {
		fprintf(stderr, "Wrong number of arguments\n");
		exit(2);
	}

	int input = open(argv[1], O_RDONLY);
	int output = open(argv[2], O_CREAT | O_WRONLY, 0666);

	ssize_t n;
	char buf[BUFSZ];

	n = read(input, buf, BUFSZ);
	while (n != 0) {
		write(output, buf, n);
		n = read(input, buf, BUFSZ);
	}
	close(input);
	close(output);

	return 0;
}
