#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFSZ 256

int main(int argc, char** argv) {
	size_t s = argc == 1 ? 1 : (argc - 1);
	int fds[s];

	if (argc == 1) {
		fds[0] = 1;
	} else {
		for (int i = 1; i < argc; i++) {
			fds[i - 1] = open(argv[i], O_RDONLY);
		}
	}

	ssize_t n;
	char buf[BUFSZ];

	for (size_t i = 0; i < s; i++) {
		n = read(fds[i], buf, BUFSZ);
		while (n != 0) {
			write(0, buf, n);
			n = read(fds[i], buf, BUFSZ);
		}
		close(fds[i]);
	}

	return 0;
}
