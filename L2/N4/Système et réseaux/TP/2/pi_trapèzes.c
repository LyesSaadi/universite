#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef N
#define N 10000000L
#endif

double calculer_pi();
double f(double x);

int main(int argc, char *argv[])
{
	struct timespec tic, toc;
	double duree, pi;

	clock_gettime(CLOCK_REALTIME, &tic);

	pi = calculer_pi(N);

	clock_gettime(CLOCK_REALTIME, &toc);
	duree = (toc.tv_sec - tic.tv_sec);
	duree += (toc.tv_nsec - tic.tv_nsec) / 1000000000.0;

	printf("pi = %.10f\n", pi);
	printf("durée : %g\n", duree);
	return 0;
}

double f(double x)
{
	return 4. / (1. + x * x);
}

double calculer_pi(long n)
{
	double x = 0., res = 0.;
	double pas = 1. / n;
	for (; x < 1; x += pas)
		res += (f(x) + f(x + pas)) / 2.;
	return res / n;
}
