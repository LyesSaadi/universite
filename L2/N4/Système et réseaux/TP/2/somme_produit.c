#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define N_DFT 8
#define test_err(test, msg, no) if ((test)) perror(msg), exit(no)

/* Retourne la somme 1 + ... + n */
void *somme(void *arg);

/* Retourne le produit 1 * ... * n */
void *produit(void *arg);

int main(int argc, char *argv[])
{
	int n = argc < 2 ? N_DFT : atoi(argv[1]);

	pthread_t ts, tp;
	int *s, *p;

	test_err(pthread_create(&ts, NULL, somme, &n), "thread error", 1);
	test_err(pthread_create(&tp, NULL, produit, &n), "thread error", 1);

	test_err(pthread_join(ts, (void **) &s), "thread error", 1);
	test_err(pthread_join(tp, (void **) &p), "thread error", 1);

	printf("n : %d, somme : %d, produit : %d\n", n, *s, *p);

	free(s);
	free(p);

	return 0;
}

void *somme(void *arg)
{
	int *n = (int *) arg, *res = (int *) malloc(sizeof(int));
	test_err(res == NULL, "malloc 1", 2);
	*res = 0;
	for (int i = 1; i <= *n; ++i)
		*res = (*res) + i;
	return res;
}

void *produit(void *arg)
{
	int *n = (int *) arg, *res = (int *) malloc(sizeof(int));
	test_err(res == NULL, "malloc 2", 2);
	*res = 1;
	for (int i = 1; i <= *n; ++i)
		*res = (*res) * i;
	return res;
}
