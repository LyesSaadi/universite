#include <unistd.h>
#include <stdio.h>
#include <wait.h>

int main(int argc, char** argv)
{
	switch (fork()) {
	case -1:
		perror("fork");
		return 1;
	case 0:
		execlp("ls", "ls", argv[1], NULL);
		perror("exec: ");
		return 1;
	}
	wait(NULL);
	switch (fork()) {
	case -1:
		perror("fork");
		return 1;
	case 0:
		execlp("du", "du", "-sh", argv[1], NULL);
		perror("exec: ");
		return 1;
	}
	wait(NULL);
	switch (fork()) {
	case -1:
		perror("fork");
		return 1;
	case 0:
		execlp("df", "df", argv[1], NULL);
		perror("exec: ");
		return 1;
	}
	wait(NULL);
	printf("Commande terminée.\n");
	return 0;
}
