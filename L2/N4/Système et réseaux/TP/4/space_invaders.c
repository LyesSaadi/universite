#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#define HAUTEUR 40
#define LARGEUR 80
#define NUM_ITER_DFT 30

int (ecran1)[HAUTEUR][LARGEUR];
int (ecran2)[HAUTEUR][LARGEUR];
int (*ecran_a_afficher)[HAUTEUR][LARGEUR];
int (*ecran_a_modifier)[HAUTEUR][LARGEUR];

pthread_t th_aff;
pthread_t th_buf;
sem_t sem_aff;
sem_t sem_buf;

void effacer_lignes(int num_lignes);
void* buffer(void* arg);
void* affichage(void* arg);

int main(int argc, char *argv[])
{
	int num_iter = argc > 1 ? atoi(argv[1]) : NUM_ITER_DFT;

	sem_init(&sem_aff, 0, 1);
	sem_init(&sem_buf, 0, 0);

	ecran_a_afficher = &ecran1;
	ecran_a_modifier = &ecran2;

	pthread_create(&th_buf, NULL, buffer, &num_iter);
	pthread_create(&th_aff, NULL, affichage, &num_iter);

	pthread_join(th_buf, NULL);
	pthread_join(th_aff, NULL);

	puts("YOU LOSE.\n");
	return 0;
}

void* buffer(void* n) {
	int i, j, num_iter = *((int*) n);
	int (*ecran_tmp)[HAUTEUR][LARGEUR];

	for (int k = 0; k < num_iter; k++) {
		/* calcul de l'affichage suivant */
		for (i = HAUTEUR - 1; i > 0; --i)
			for (j = 0; j < LARGEUR; ++j)
				(*ecran_a_modifier)[i][j] = (*ecran_a_afficher)[i - 1][j];

		for (j = 0; j < LARGEUR; ++j)
			(*ecran_a_modifier)[0][j] = 0;
		(*ecran_a_modifier)[0][rand() % LARGEUR] = 1;

		sem_wait(&sem_aff);
		ecran_tmp = ecran_a_modifier;
		ecran_a_modifier = ecran_a_afficher;
		ecran_a_afficher = ecran_tmp;
		sem_post(&sem_buf);
	}

	sem_post(&sem_buf);

	return NULL;
}

void* affichage(void* n) {
	int i, j, num_iter = *((int*) n);

	for (int k = 0; k < num_iter; k++) {
		/* affichage de l'écran modifié */
		effacer_lignes(HAUTEUR);

		usleep(500000);

		sem_wait(&sem_buf);
		for (i = 0; i < HAUTEUR; ++i) {
			for (j = 0; j < LARGEUR; ++j) {
				if ((*ecran_a_afficher)[i][j] == 0)
					putchar(' ');
				else
					putchar('*');
			}
			putchar('\n');
		}
		sem_post(&sem_aff);
	}

	return NULL;
}

void effacer_lignes(int num_lignes)
{
	printf("\033[2K");
	for(; num_lignes > 0; --num_lignes) {
		printf("\033[F");
		printf("\033[2K");
	}
}
