#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

struct data {
	int i;
	pthread_mutex_t verrou_i;
	pthread_mutex_t verrou_affichage;
	char **texte;
	int *durees;
};

void *depardieu(void *arg);
void *belmondo(void *arg);

int main()
{
	char *texte[] = {
		"je te tiens", "tu me tiens", "par la barbichette",
		"le premier", "de nous deux", "qui rira", "aura une",
		"ta", "pette", NULL, NULL };
	int durees[] = { 2, 2, 7, 1, 3, 2, 2, 1, 1, 0 };

	struct data donnees = { .i = 0, .texte = texte, .durees = durees };
	
	pthread_t acteur1, acteur2;
	
	pthread_mutex_init(&donnees.verrou_i, NULL);
	pthread_mutex_init(&donnees.verrou_affichage, NULL);
	
	if (pthread_create(&acteur1, NULL, depardieu, &donnees) != 0) {
		perror("pthread");
		return 1;
	}
	if (pthread_create(&acteur2, NULL, belmondo, &donnees) != 0) {
		perror("pthread");
		return 1;
	}
	pthread_join(acteur1, NULL);
	pthread_join(acteur2, NULL);
	pthread_mutex_destroy(&donnees.verrou_affichage);
	pthread_mutex_destroy(&donnees.verrou_i);

	return 0;
}

void *depardieu(void *arg)
{
	char *tirade = "";
	struct data *data = arg;
	char a_lire[100];
	int i, delai;
	while (tirade != NULL) {
		pthread_mutex_lock(&data->verrou_i);
		i = data->i;
		/* incrémentation du compteur de lignes traitées */
		data->i = data->i + 1 ;
		pthread_mutex_unlock(&data->verrou_i);

		/* accès au texte d'origine */
		tirade = data->texte[i];
		if (tirade == NULL) {
			break;
		}
		delai = data->durees[i] * .5e6 / strlen(tirade);

		/* préparation du texte à lire */
		for (i = 0; tirade[i] != 0; ++i) {
			a_lire[i] = tirade[i] + (tirade[i]==' ' ? 0 : 'A' - 'a');
			usleep(delai);
		}
		a_lire[i] = 0;
		
		pthread_mutex_lock(&data->verrou_affichage);
		/* lecture du texte */
		for (i = 0; a_lire[i] != 0; ++i) {
			putchar(a_lire[i]); fflush(stdout);
			usleep(delai);
		}
		putchar('\n');
		pthread_mutex_unlock(&data->verrou_affichage);
	}
	return NULL;
}

void *belmondo(void *arg)
{
	char *tirade = "";
	struct data *data = arg;
	char a_lire[100];
	int i, delai;
	
	while (tirade != NULL) {
		pthread_mutex_lock(&data->verrou_i);
		i = data->i;
		/* incrémentation du compteur de lignes traitées */
		data->i = data->i + 1 ;
		pthread_mutex_unlock(&data->verrou_i);

		/* accès au texte d'origine */
		tirade = data->texte[i];
		if (tirade == NULL) {
			break;
		}
		delai = data->durees[i] * .5e6 / strlen(tirade);
				
		/* préparation du texte à lire */
		for (i = 0; tirade[i] != 0; ++i) {
			a_lire[2*i] = a_lire[2*i+1] = tirade[i];
			usleep(delai);
		}
		a_lire[2*i] = 0;

		pthread_mutex_lock(&data->verrou_affichage);
		/* lecture du texte */
		for (i = 0; a_lire[i] != 0; ++i) {
			putchar(a_lire[i]); fflush(stdout);
			usleep(delai);
		}
		putchar('\n');
		pthread_mutex_unlock(&data->verrou_affichage);
	}	
	return NULL;
}
