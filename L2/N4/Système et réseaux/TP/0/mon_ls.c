#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>

extern char** environ;

int main(int argc, char** argv) {
	char* path = NULL;

	if (argc == 1) {
		int i = 0;
		while (environ[i] != NULL) {
			if (environ[i][0] == 'P' &&
				environ[i][1] == 'W' &&
				environ[i][2] == 'D' &&
				environ[i][3] == '=') {
				path = &environ[i][4];
				break;
			}
			i++;
		}

		if (path == NULL) {
			printf("No PWD in path");
			exit(2);
		}
	} else {
		path = argv[1];
	}

	int i = 1;

	while (path != NULL) {
		DIR* d = opendir(path);

		if (d == NULL) {
			perror("error opening the dir");
			exit(2);
		}

		struct dirent* f = readdir(d);

		if (argc > 2)
			printf("%s:\n", path);

		while (f != NULL) {
			printf("%s\n", f->d_name);
			f = readdir(d);
		}

		path = argv[++i];

		if (argc > 2 && path != NULL)
			printf("\n");

		closedir(d);
	}
}
