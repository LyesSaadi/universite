#include <stdlib.h>
#include <stdio.h>

#define MULTIPLIER 2

int main() {
	int t = 4, i = 0, c; char a;

	char* line = (char*) malloc(sizeof(char) * 4);
	
	while ((c = getchar()) != EOF) {
		if (c == '\n') {
			while (i != 0) {
				putchar(line[--i]);
			}
			putchar('\n');
		} else {
			a = (char) c;

			line[i++] = a;

			if (i == t) {
				t *= MULTIPLIER;
				line = (char *) realloc(line, sizeof(char) * t);
			}
		}
	}

	free(line);

	printf("%d\n", t);
}
