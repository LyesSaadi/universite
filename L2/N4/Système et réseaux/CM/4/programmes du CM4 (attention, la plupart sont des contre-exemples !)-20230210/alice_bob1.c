#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
void *alice(void *);
void *bob(void *);
void faire_des_choses();
char res;
int main()
{
	pthread_t a, b;
	srand(time(NULL));
	pthread_create(&a, NULL, alice, NULL);
	pthread_create(&b, NULL, bob, NULL);

	pthread_join(a, NULL);
	pthread_join(b, NULL);
	return 0;
}
void faire_des_choses() { usleep(rand() % 10 * 50000); }
void *alice(void *arg) { faire_des_choses(); res = 'A'; }
void *bob(void *arg) { faire_des_choses(); printf("%c\n", res); }
