#include <stdio.h>
#include <pthread.h>

#define N 100
void *print_i(void *addr_i);
int main()
{
	pthread_t th[N];
	int i;
	for (i = 0; i < N; ++i)
		pthread_create(th + i, NULL, print_i, &i);

	for (i = 0; i < N; ++i)
		pthread_join(th[i], NULL);
	return 0;
}

void *print_i(void *addr_i)
{
	int i = *((int *) addr_i);
	printf("i = %d\n", i);
	return NULL;
}
