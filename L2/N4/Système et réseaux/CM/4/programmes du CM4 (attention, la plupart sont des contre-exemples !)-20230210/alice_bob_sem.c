#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <time.h>
void *alice(void *);
void *bob(void *);
void faire_des_choses();
char res;
int main()
{
	pthread_t a, b;
	sem_t s;
	sem_init(&s, 0, 0); /* initialement la ressource n'est pas présente */
	srand(time(NULL));
	pthread_create(&a, NULL, alice, &s);
	pthread_create(&b, NULL, bob, &s);

	pthread_join(b, NULL);
	pthread_join(a, NULL);

	sem_destroy(&s);
	return 0;
}
void faire_des_choses() { usleep(rand() % 10 * 50000); }
void *alice(void *sem)
{
	sem_t *s = sem;
	faire_des_choses();
	res = 'A';
	sem_post(s);
	faire_des_choses();
	printf("Alice : j'ai fait pas mal de choses !\n");
	return NULL;
}
void *bob(void *sem)
{
	sem_t *s = sem;
	faire_des_choses();
	sem_wait(s);
	printf("Bob : Alice a fini et a écrit : %c\n", res);
	return NULL;
}
