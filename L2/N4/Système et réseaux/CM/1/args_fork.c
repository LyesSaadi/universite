#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int i;
	if (fork() == -1) {
		perror("fork");
		return 1;
	}
	printf("%d: Nom du programme (argv[0]) : %s\n", getpid(), argv[0]);
	for (i = 1; i <= argc; ++i)
		printf("%d: argv[%d] = %s\n", getpid(), i, argv[i]);
	return 0;
}
