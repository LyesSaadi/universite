#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	int a = 3, b = 5;
	pid_t p;

	switch ((p = fork())) {
	case -1:
		perror("fork");
		return 1;
	case 0:
		printf("%d: je suis l'enfant de %d\n", getpid(), getppid());
		b = 42;
		break;
	default:
		printf("%d: mon enfant est %d\n", getpid(), p);
		a = -2;
	}
	printf("%d: a = %d, b = %d\n", getpid(), a, b);

	return 0;
}
