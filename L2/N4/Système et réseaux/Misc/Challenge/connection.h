#ifndef __CONNECTION_H
#define __CONNECTION_H

#include "args.h"

int establish_connection(args_t* args);

#endif
