#include <stdlib.h>
#include <stdio.h>

#include "args.h"
#include "connection.h"
#include "ui.h"

int main(int argc, char** argv) {
	args_t* args = parse_args(argc, argv);

	if (args->dest == NULL)
		wait_for_connection(args);

	int sock = establish_connection(args);

	setup_ui(sock);

	destroy_args(args);

	return 0;
}
