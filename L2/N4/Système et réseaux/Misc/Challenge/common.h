#ifndef __COMMON_H
#define __COMMON_H

// Color codes
#define ERR "\e[1m\e[31m"
#define LOG "\e[38;5;007m"
#define CLR "\e[0m"

// Debug info
#define ERROR(str) ERR "[ERROR]: " str CLR

// Debug macros
#define PRTERR(str, ...) fprintf(stderr, ERROR(str), ##__VA_ARGS__)
#define ERRNO(str) PRTERR(str ": %s\n", strerror(errno))

#endif
