#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>

void boulot(int n);
void *th_A(void*);
void *th_B(void*);

sem_t aff_a;

int main() {
	pthread_t t1, t2;
	sem_init(&aff_a, 0, 0);
	pthread_create(&t1, NULL, th_A, NULL);
	pthread_create(&t2, NULL, th_B, NULL);
	pthread_join(t1, NULL);
	pthread_join(t2, NULL);
	sem_destroy(&aff_a);
	return 0;
}

void boulot(int n) {
	for (int i = 0; i < n; i++)
		;
}

void *th_A(void*) {
	boulot(1000);
	printf("A\n");
	sem_post(&aff_a);
	return NULL;
}

void *th_B(void*) {
	boulot(1000);
	sem_wait(&aff_a);
	printf("B\n");
	return NULL;
}
