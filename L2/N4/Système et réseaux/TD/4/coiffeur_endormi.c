#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>

#define NUM_CLIENTS 10
#define NUM_CHAISES 6
#define COEFF_ATTENTE 500000
int num_chaises_vides = NUM_CHAISES;
/* Choses à ajouter ici */
void *client(void *arg);
void *coiffeur(void *arg);
void coiffer();
void bavarder_avec_le_coiffeur();
sem_t il_y_a_des_clients;
sem_t coiffeur_dispo;
pthread_mutex_t maj_chaise;

int main()
{
	int i
	pthread_t clt[NUM_CLIENTS], coiff;
	/* Préparatifs : initialisations... */
	/* Choses à faire ici */

	sem_init(&il_y_a_des_clients, 0, 0);
	sem_init(&coiffeur_dispo, 0, 1);
	pthread_mutex_init(&maj_chaise, NULL);
	pthread_create(&coiff, NULL, coiffeur, NULL);
	for (i = 0; i < NUM_CLIENTS; ++i)
		pthread_create(&clt[i], NULL, client, NULL);

	/* De toute façon les boucles sont infinies, ... */
	for (i = 0; i < NUM_CLIENTS; ++i)
		pthread_join(clt[i], NULL);
	pthread_join(coiff, NULL);
	sem_destroy(&il_y_a_des_clients);
	sem_destroy(&coiffeur_dispo);
	pthread_mutex_destroy(&maj_chaise);
	return 0;
}
void *coiffeur(void *arg)
{
	/* Fonction à modifier */
	for (;;) {
		sem_wait(&il_y_a_des_clients);
		pthread_mutex_lock(&maj_chaise);
		++num_chaises_vides;
		pthread_mutex_unlock(&maj_chaise);
		coiffer();
		sem_post(&coiffeur_dispo);
	}
}
		
void *client(void *arg)
{
	/* Fonction à modifier */
	for (;;) {
		pthread_mutex_lock(&maj_chaise);
		if (num_chaises_vides == 0) {
			pthread_mutex_unlock(&maj_chaise);
			continue;
		}
		--num_chaises_vides;
		pthread_mutex_unlock(&maj_chaise);
		sem_post(&il_y_a_des_clients);
		sem_wait(&coiffeur_dispo);
		//bavarder_avec_le_coiffeur(); <- Complique les choses
	}
}
void coiffer() {
	usleep((double) rand() / RAND_MAX * COEFF_ATTENTE);
}
void bavarder_avec_le_coiffeur() {
	usleep((double) rand() / RAND_MAX * COEFF_ATTENTE);
}
