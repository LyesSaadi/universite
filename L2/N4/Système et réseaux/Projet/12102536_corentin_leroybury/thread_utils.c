/* Corentin LEROY-BURY 12102536
Je déclare qu'il s'agit de mon propre travail. */

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <arpa/inet.h>
#include "comptine_utils.h"
#include "thread_utils.h"
#include "queue.h"

void init_args(arguments *args) {
	pthread_mutex_init(args->mutex_queue,NULL);
	pthread_mutex_init(args->mutex_log,NULL);
	pthread_mutex_init(args->mutex_catalogue,NULL);
	sem_init(args->sem_client,0,0);
	sem_init(args->clt_retrieved,0,1);
	sem_init(args->clt_available,0,0);
	sem_init(args->sem_catalogue,0,1);
}

void destroy_args(arguments *args) {
	pthread_mutex_destroy(args->mutex_queue);
	pthread_mutex_destroy(args->mutex_log);
	pthread_mutex_destroy(args->mutex_catalogue);
	sem_destroy(args->sem_client);
	sem_destroy(args->clt_retrieved);
	sem_destroy(args->clt_available);
	sem_destroy(args->sem_catalogue);
	free_queue(args->client_queue);
	liberer_catalogue(args->c);
}

void write_log(struct in_addr ip, pthread_mutex_t *mutex_log, char *request, int nc, char *path_name) {
	time_t now;
	time(&now);
	char *string_now = ctime(&now);
	string_now[strlen(string_now) - 1] = '\0';
	char ip_client[INET_ADDRSTRLEN];

	pthread_mutex_lock(mutex_log);
	int fd_log = open("log", O_WRONLY | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR);
	if(nc >= 0) 				dprintf(fd_log,"[%s] %s -> %s n°%d.\n",string_now,inet_ntop(AF_INET,&ip,ip_client,INET_ADDRSTRLEN),request,nc);
	else if(path_name != NULL) 	dprintf(fd_log,"[%s] %s -> %s : \"%s\".\n",string_now,inet_ntop(AF_INET,&ip,ip_client,INET_ADDRSTRLEN),request,path_name);
	else 						dprintf(fd_log,"[%s] %s -> %s\n",string_now,inet_ntop(AF_INET,&ip,ip_client,INET_ADDRSTRLEN),request);
	close(fd_log);
	pthread_mutex_unlock(mutex_log);
}

/* Ces 2 fonctions sont basées sur le problème de l'écrivain et des lecteurs */
void client_start_reading_catalogue(arguments *args) {
	pthread_mutex_lock(args->mutex_catalogue);
	if(args->nb_using_catalogue == 0)
		sem_wait(args->sem_catalogue);
	args->nb_using_catalogue++;
	pthread_mutex_unlock(args->mutex_catalogue);
}

void client_stop_reading_catalogue(arguments *args) {
	pthread_mutex_lock(args->mutex_catalogue);
	args->nb_using_catalogue--;
	if(args->nb_using_catalogue == 0)
		sem_post(args->sem_catalogue);
	pthread_mutex_unlock(args->mutex_catalogue);
}
