/* Corentin LEROY-BURY 12102536
Je déclare qu'il s'agit de mon propre travail. */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <arpa/inet.h> /* in_addr */
#include "queue.h"


queue *init_queue(const uint16_t max_size) {
	queue* result = malloc(sizeof(queue));
	if(result == NULL) {
		perror("malloc");
		return NULL;
	}
	result->client_sock = malloc(max_size * sizeof(int));
	result->client_ip = malloc(max_size * sizeof(struct in_addr));
	if(result->client_sock == NULL && result->client_ip == NULL) {
		perror("malloc");
		return NULL;
	} 
	result->max_size = max_size;
	result->size = 0;
	return result;
}

void free_queue(queue *q) {
	if(q != NULL) {
		free(q->client_sock);
		free(q->client_ip);
	} free(q);
}

int enqueue(queue* q, int sock, struct in_addr ip)  {
	if(q->size >= q->max_size) return 0;
	q->client_sock[q->size] = sock;
	q->client_ip[(q->size)++] = ip;
	return 1;
}

int dequeue(queue* q, int *sock_addr, struct in_addr *ip_addr) {
	if(q->size == 0) return 0;
	*sock_addr = q->client_sock[0];
	*ip_addr = q->client_ip[0];
	for(int i = 0; i < q->size; i++) {
		q->client_sock[i] = q->client_sock[i+1];
		q->client_ip[i] = q->client_ip[i+1];
	}
	q->size--;
	return 1;
}

void display_queue(queue *q) {
	for(int i = 0; i < q->size; i++)
		printf("N°%d -> Socket %d ; IPv4 %s\n",i,q->client_sock[i],inet_ntoa(q->client_ip[i]));
}