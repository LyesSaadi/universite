/* Corentin LEROY-BURY 12102536
Je déclare qu'il s'agit de mon propre travail. */

/* fichiers de la bibliothèque standard */
#include <stdio.h>
#include <stdlib.h>
#include <time.h> /* Pour l'heure et la date dans les logs */
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
/* bibliothèque standard unix */
#include <unistd.h> /* close, read, write */
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <dirent.h>
#include <errno.h>
/* spécifique à internet */
#include <arpa/inet.h> /* inet_pton */
/* spécifique aux comptines */
#include "comptine_utils.h"
/* Multi-threading (Ajout-personnel) */
#include <pthread.h>
#include <semaphore.h>
#include "queue.h"
#include "thread_utils.h"

#define PORT_WCP 4321
#define NB_CLIENT 128
#define NB_THREAD 8

void usage(char *nom_prog) {
	fprintf(stderr, "Usage: %s repertoire_comptines\n"
			"serveur pour WCP (Wikicomptine Protocol)\n"
			"Exemple: %s comptines/\n", nom_prog, nom_prog);
}
/** Retourne en cas de succès le descripteur de fichier d'une socket d'écoute
 *  attachée au port port et à toutes les adresses locales. */
int creer_configurer_sock_ecoute(uint16_t port);

/** Écrit dans le fichier de desripteur fd la liste des comptines présents dans
 *  le catalogue c comme spécifié par le protocole WCP, c'est-à-dire sous la
 *  forme de plusieurs lignes terminées par '\n' :
 *  chaque ligne commence par le numéro de la comptine (son indice dans le
 *  catalogue) commençant à 0, écrit en décimal, sur 6 caractères
 *  suivi d'un espace
 *  puis du titre de la comptine
 *  une ligne vide termine le message */
void envoyer_liste(int fd, struct catalogue *c);

/** Lit dans fd un entier sur 2 octets écrit en network byte order
 *  retourne : cet entier en boutisme machine. */
uint16_t recevoir_num_comptine(int fd);

/** Écrit dans fd la comptine numéro ic du catalogue c dont le fichier est situé
* dans le répertoire dirname comme spécifié par le protocole WCP, c'est-à-dire :
* chaque ligne du fichier de comptine est écrite avec son '\n' final, y
* compris son titre, deux lignes vides terminent le message */
void envoyer_comptine(int fd, struct catalogue *c, uint16_t ic, char *dir_name);

/* Ajout personnel */

/** Lit dans fd un nom de fichier et un titre de comptine, vérifie si elle est inexistante 
	dans le catalogue, puis lit chacune de ses lignes, pour être écrite dans un nouveau fichier */
int recevoir_comptine(int fd, struct catalogue *c, char *dir_name, char* base_name);

/** Fonctions de thread, la première écoute les demandes de connexion des clients, la seconde 
	discute avec le client à l'aide du protocole WCP */
void *listener_routine(void *args);
void *worker_routine(void *args);

/* Fin ajout personnel */

int main(int argc, char *argv[]) {
	if (argc != 2) {
		usage(argv[0]);
		return 1;
	}
	/* On vérifie si le nom de répertoire donné en argument a bien son '/' final */
	int i = strlen(argv[1]);
	char dir_name[i+2];
	strcpy(dir_name,argv[1]);
	if(dir_name[i - 1] != '/') {
		dir_name[i] = '/';
		dir_name[i+1] = '\0';
	}

	/** Initialisation des données relatives au parallélisme
	 *  Les mutex pour ne pas écrire dans la queue ou les logs en même temps
	 *  sem_client compte le nombre de clients en attente
	 *  clt_retrieved et clt_available pour indiquer aux thread de récupérer un client */
	pthread_mutex_t mutex_queue, mutex_log, mutex_catalogue;
	sem_t sem_client, clt_retrieved, clt_available, sem_catalogue;

	/* Socket et IP du client à récupérer */
	int client_sock;
	struct in_addr client_ip;
	queue *client_queue = init_queue(NB_CLIENT);
	struct catalogue *c;
	if((c = creer_catalogue(dir_name)) == NULL)
		exit(5);

	/* Argument générique envoyé à tous les threads */
	arguments args = { .client_sock = &client_sock, .client_ip = &client_ip, .sem_catalogue = &sem_catalogue,
					.sem_client = &sem_client, .clt_retrieved = &clt_retrieved, .clt_available = &clt_available,
					.mutex_queue = &mutex_queue, .mutex_log = &mutex_log, .mutex_catalogue = &mutex_catalogue,
					.c = c, .client_queue = client_queue, .dir_name = dir_name, .nb_using_catalogue = 0 };
	
	init_args(&args);
	
	/** Initialisation des threads
	 *  Un thread th_listener qui récupère les demandes de connexion et les envoie au main
	 *  NB_THREAD threads de travail qui répondent aux clients simultanément */
	pthread_t th_listener, th_worker[NB_THREAD];
	if(pthread_create(&th_listener,NULL,listener_routine,&args) != 0) {
		perror("pthread_create");
		exit(7);
	}

	for(int i = 0; i < NB_THREAD; i++)
		if(pthread_create(&th_worker[i],NULL,worker_routine,&args) != 0) {
			perror("pthread_create");
			exit(7);
		}

	/* Boucle principale qui sert de relais entre le socket d'écoute et les threads de travail */
	for(;;) {
		/* On attend qu'il y ait un client dans la file */
		sem_wait(&sem_client);
		/* Tant que le client d'avant n'a pas été récupéré on attend */
		sem_wait(&clt_retrieved);
		/* Le client en première position de la file est envoyé aux threads */
		pthread_mutex_lock(&mutex_queue);
		dequeue(client_queue,&client_sock,&client_ip);
		pthread_mutex_unlock(&mutex_queue);
		/* Un client est disponible pour un thread */
		sem_post(&clt_available);
	}

	/* Fermeture du serveur */
	pthread_join(th_listener,NULL);
	for(int i = 0; i < NB_THREAD; i++)
		pthread_join(th_worker[i],NULL);

	destroy_args(&args);

	return 0;
}

int creer_configurer_sock_ecoute(uint16_t port) {
	int sock_listen;
	if((sock_listen = socket(AF_INET,SOCK_STREAM,0)) < 0) {
		perror("socket");
		exit(1);
	}

	struct sockaddr_in sa = { .sin_family = AF_INET,
							.sin_port = htons(PORT_WCP),
							.sin_addr.s_addr = htonl(INADDR_ANY) };
	int opt = 1;
	setsockopt(sock_listen, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));

	if(bind(sock_listen, (struct sockaddr *) &sa, sizeof(struct sockaddr_in)) < 0) {
		perror("bind");
		exit(2);
	}

	if(listen(sock_listen,NB_CLIENT) < 0) {
		perror("listen");
		exit(3);
	} return sock_listen;
}

void envoyer_liste(int fd, struct catalogue *c) {
	for(int i = 0; i < c->nb; i++)
		dprintf(fd,"%6d %s",i,c->tab[i]->titre);
	char end = '\n';
	write(fd,&end,sizeof(char));
}

uint16_t recevoir_num_comptine(int fd) {
	uint16_t num;
	exact_read(fd,&num,sizeof(uint16_t));
	return ntohs(num);
}

void envoyer_comptine(int fd, struct catalogue *c, uint16_t ic, char *dir_name) {
	/* En cas d'erreur */
	if(ic >= c->nb) {
		dprintf(fd,"Numéro de comptine hors du catalogue !\n\n\n");
		return;
	}
	
	char file_path[256] = "";
	strcat(file_path,dir_name);
	strcat(file_path,c->tab[ic]->nom_fichier);

	int fd_comptine;
	if((fd_comptine = open(file_path, O_RDONLY)) < 0) {
		perror("open");
		exit(6);
	}

	char buf[256];
	int i;
	/* On lit et envoie toutes les lignes du fichier comptine ouvert
	Lorsque l'on a atteint la fin du fichier, on envoie 2 lignes vides. */
	for(;;) {
		while((i = read_until_nl(fd_comptine,buf)) > 0) {
			buf[i+1] = '\0';
			dprintf(fd,"%s",buf);
		}
		if(( i = read_until_nl(fd_comptine,buf)) == 0) {
			dprintf(fd,"\n\n");
			close(fd_comptine);
			return;
		} buf[i+1] = '\0';
		dprintf(fd,"\n%s",buf);
	}
}

/* Ajout personnel */

int recevoir_comptine(int fd, struct catalogue *c, char *dir_name, char* base_name) {
	char buf[256];
	uint16_t response;

	/* On lit d'abord le nom de fichier de la comptine puis son titre */
	int i = read_until_nl(fd,base_name);
	base_name[i] = '\0';
	i = read_until_nl(fd,buf);
	buf[i+1] ='\0';
	
	/* Si le nom de fichier ou le titre correspond à une comptine déjà existante,
	on annule l'ajout dans le catalogue (On évite les doublons, même si le nom
	de fichier est différent !) */
	for(i = 0; i < c->nb; i++) {
		if(!strcmp(c->tab[i]->nom_fichier,base_name)) {
			printf("%s\n",c->tab[i]->nom_fichier);
			response = htons(1);
			write(fd,&response,sizeof(uint16_t));
			return 0;
		}
		if(!strcmp(c->tab[i]->titre,buf)) {
			printf("%s\n",c->tab[i]->titre);
			response = htons(2);
			write(fd,&response,sizeof(uint16_t));
			return 0;
		}
	}
	response = htons(0);
	write(fd,&response,sizeof(uint16_t));

	/* Si tout va bien, on crée un nouveau fichier de chemin path_name pour accueillir la comptine */
	char path_name[256] = "";
	strcat(path_name,dir_name);
	strcat(path_name,base_name);
	int fd_comptine = open(path_name, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);

	/* Lecture de la comptine (buf contient déjà le titre avant la boucle) */
	dprintf(fd_comptine,"%s",buf);
	for(;;) {
		if((i = read_until_nl(fd,buf)) == 0) {
			if((i = read_until_nl(fd,buf)) == 0) {
				close(fd_comptine);
				return 1;
			} dprintf(fd_comptine,"\n");
		}
		buf[i+1] = '\0';
		dprintf(fd_comptine,"%s",buf);
	} 
	close(fd_comptine);
	return 0;
}

void *listener_routine(void *args) {
	arguments *arg = (arguments *) args;
	int sock_listen = creer_configurer_sock_ecoute(PORT_WCP);
	int fd_client, status;

	struct sockaddr_in sa_clt;
	socklen_t sl = sizeof(struct sockaddr_in);

	for(;;) {
		if((fd_client = accept(sock_listen, (struct sockaddr *) &sa_clt, &sl)) < 0) {
			perror("accept");
			exit(4);
		}
		write_log(sa_clt.sin_addr,arg->mutex_log,"Demande de connexion",-1,NULL);

		/* On essaie d'ajouter le client dans la file */
		pthread_mutex_lock(arg->mutex_queue);
		status = enqueue(arg->client_queue,fd_client,sa_clt.sin_addr);
		pthread_mutex_unlock(arg->mutex_queue);

		/* Client accepté */
		if(status) {
			sem_post(arg->sem_client);
			write_log(sa_clt.sin_addr,arg->mutex_log,"Connexion acceptée",-1,NULL);
		}
		/* Client refusé */
		else {
			close(fd_client);
			write_log(sa_clt.sin_addr,arg->mutex_log,"Connexion refusée (file pleine)",-1,NULL);
		}
	} 
	close(sock_listen);
	return NULL;
}

void *worker_routine(void* args) {
	int client_fd;
	uint16_t choice, nc;
	struct in_addr client_ip;
	arguments *arg = (arguments *) args;

	for(;;) {
		/* Récupération d'un client */
		sem_wait(arg->clt_available);
		client_fd = *(arg->client_sock);
		client_ip = *(arg->client_ip);
		sem_post(arg->clt_retrieved);

		/* On sert le client actuel, on lui envoie juste un octet pour qu'il sache que c'est son tour */
		char response;
		write(client_fd,&response,1);

		envoyer_liste(client_fd,arg->c);

		do{
			if(exact_read(client_fd,&choice,sizeof(uint16_t)) == 0)
				break;
			choice = ntohs(choice); 
			switch(choice) {
				/* Afficher la liste des comptines */
				case 1:
					write_log(client_ip,arg->mutex_log,"Demande d'envoi de la liste des comptines",-1,NULL);
					client_start_reading_catalogue(arg);
					envoyer_liste(client_fd,arg->c);
					client_stop_reading_catalogue(arg);
					break;
				/* Envoyer une comptine */
				case 2:
					nc = recevoir_num_comptine(client_fd);
					write_log(client_ip,arg->mutex_log,"Demande d'envoi de la comptine",nc,NULL);
					client_start_reading_catalogue(arg);
					envoyer_comptine(client_fd,arg->c,nc,arg->dir_name);
					client_stop_reading_catalogue(arg);
					break;
				/* Recevoir une comptine */
				case 3:
					sem_wait(arg->sem_catalogue);
					char base_name[256];
					if(recevoir_comptine(client_fd,arg->c,arg->dir_name,base_name)) {
						write_log(client_ip,arg->mutex_log,"Demande d'ajout au catalogue acceptée",-1,base_name);
						liberer_catalogue(arg->c);
						if((arg->c = creer_catalogue(arg->dir_name)) == NULL)
							exit(5);
					} else write_log(client_ip,arg->mutex_log,"Demande d'ajout au catalogue refusée",-1,base_name);
					sem_post(arg->sem_catalogue);
					break;
				/* Fin de connexion */
				default:
					choice = 0;
			}
		} while(choice != 0);
		/* Fin du service rendu au client */
		close(client_fd);
		write_log(client_ip,arg->mutex_log,"Fin de connexion",-1,NULL);
	} return NULL;
}