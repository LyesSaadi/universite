/* Corentin LEROY-BURY 12102536
Je déclare qu'il s'agit de mon propre travail. */

/* fichiers de la bibliothèque standard */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
/* bibliothèque standard unix */
#include <unistd.h> /* close, read, write */
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
/* spécifique à internet */
#include <arpa/inet.h> /* inet_pton */
/* spécifique aux comptines */
#include "comptine_utils.h"

#define PORT_WCP 4321

void usage(char *nom_prog) {
	fprintf(stderr, "Usage: %s addr_ipv4\n"
			"client pour WCP (Wikicomptine Protocol)\n"
			"Exemple: %s 208.97.177.124\n", nom_prog, nom_prog);
}

void aide() {
	printf("\nListe des commandes (attention à la casse !) :\n\n"
			"$quitter\n\tInterrompt la connexion avec le serveur et ferme le client.\n"
			"$liste\n\tDemande au serveur la liste des comptines disponibles dans le catalogue\n"
			"$demander\n\tDemande au serveur une comptine du catalogue\n"
			"$envoyer\n\tEnvoie une comptine choisie au catalogue du serveur\n"
			"$aide\n\tAffiche l\'aide\n\n");
}

/** Retourne (en cas de succès) le descripteur de fichier d'une socket
 *  TCP/IPv4 connectée au processus écoutant sur port sur la machine d'adresse
 *  addr_ipv4 */
int creer_connecter_sock(char *addr_ipv4, uint16_t port);

/** Lit la liste numérotée des comptines dans le descripteur fd et les affiche
 *  sur le terminal.
 *  retourne : le nombre de comptines disponibles */
uint16_t recevoir_liste_comptines(int fd);

/** Demande à l'utilisateur un nombre entre 0 (compris) et nc (non compris)
 *  et retourne la valeur saisie. */
uint16_t saisir_num_comptine(uint16_t nb_comptines);

/** Écrit l'entier ic dans le fichier de descripteur fd en network byte order */
void envoyer_num_comptine(int fd, uint16_t nc);

/** Affiche la comptine arrivant dans fd sur le terminal */
void afficher_comptine(int fd);

/* Ajout personnel */

/** Demande à l'utilisateur quelle comptine il veut téléverser dans le catalogue 
	Renvoi un nombre négatif si la comptine n'existe pas ou qu'elle n'est pas au
	bon format, renvoi le descripteur fd de la comptine en cas de réussite.
	La comptine doit avoir comme chemin_relatif path_name, base_name se remplit
	tout seul dans la fonction. */
int demande_envoi_comptine(char *base_name);

/** Envoie une comptine de nom de fichier base_name de descripteur fd_comptine dans fd */
void envoyer_comptine(int fd, int fd_comptine, char *base_name);

/** Récupère une ligne sur l'entrée standart et l'interprète avec le langage de requête 
	du client. Renvoi 5 en cas de commande non reconnue ou erronée. */
uint16_t stdin_request(char *request_buf);

/* Fin Ajout personnel */

int main(int argc, char *argv[]) {
	if (argc != 2) {
		usage(argv[0]);
		return 1;
	}

	int fd_comptine, sock = creer_connecter_sock(argv[1],PORT_WCP);
	char response;
	printf("En attente de connexion... \n");
	if(read(sock,&response,1) == 0)
		exit(4);
	printf("Connexion établie ! Voici le catalogue actuel du serveur : \n");

	uint16_t choice = 5, nc = recevoir_liste_comptines(sock);
	char base_name[256], request_buf[256];

	do {
		/* On attend que le client rentre une commande */
		if((choice = stdin_request(request_buf)) == 5) continue;
		switch(choice) {
			/* Afficher la liste des comptines */
			case 1: 
				choice = htons(choice);
				write(sock,&choice,sizeof(uint16_t));
				nc = recevoir_liste_comptines(sock);
				break;
			/* Demander une comptine */
			case 2:
				choice = htons(choice);
				write(sock,&choice,sizeof(uint16_t));
				envoyer_num_comptine(sock,saisir_num_comptine(nc));
				printf("\n");
				afficher_comptine(sock);
				break;
			/* Envoyer une comptine */
			case 3:
				if((fd_comptine = demande_envoi_comptine(base_name)) >= 0) {
					choice = htons(choice);
					write(sock,&choice,sizeof(uint16_t));
					envoyer_comptine(sock,fd_comptine,base_name);
				}
				break;
			/* Aide */
			case 4:
				aide();
				break;
			/* Quitter */
			default:
			    choice = htons(choice);
				write(sock,&choice,sizeof(uint16_t));
				choice = 0;
		}
	} while(choice != 0);

	close(sock);
	return 0;
}

int creer_connecter_sock(char *addr_ipv4, uint16_t port) {
	/* Ouverture du socket */ 
	int sock;
	if((sock = socket(AF_INET,SOCK_STREAM,0)) < 0) {
		perror("socket");
		exit(1);
	}

	struct sockaddr_in sa = { .sin_family = AF_INET,
							.sin_port = htons(PORT_WCP) };
	/* Verification de l'adresse IP */ 
	if((inet_pton(AF_INET,addr_ipv4, &sa.sin_addr)) != 1) {
		perror("inet_pton");
		exit(2);
	}

	/* Connexion avec le serveur */
	if(connect(sock, (struct sockaddr * ) &sa, sizeof(struct sockaddr_in)) < 0) {
		perror("connect");
		exit(3);
	} return sock;
}

uint16_t recevoir_liste_comptines(int fd) {
	char buf[256];
	int i = 0, j;
	while((j = read_until_nl(fd,buf)) > 0) {
		buf[j+1] = '\0';
		printf("%s",buf);
		i++;
	} return i;
}

uint16_t saisir_num_comptine(uint16_t nb_comptines) {
	printf("Entrez un numéro de comptine dans l'intervalle [0, %" PRIu16 "] : ",nb_comptines-1);
	uint16_t result;
	scanf("%" SCNu16,&result);
	/* pour vider le buffer du \n */
	getchar();
	return result;
}

void envoyer_num_comptine(int fd, uint16_t nc) {
	uint16_t num = htons(nc);
	write(fd,&num,sizeof(uint16_t));
}

void afficher_comptine(int fd) {
	char buf[256];
	for(int i;;) {
		if((i = read_until_nl(fd,buf)) == 0) {
			if((i = read_until_nl(fd,buf)) == 0) return;
			else printf("\n");
		}
		buf[i+1] = '\0';
		printf("%s",buf);
	}
}

/* Ajout personnel */

int demande_envoi_comptine(char *base_name) {
	char path_name[256];
	printf("Entrez le chemin de la comptine que vous voulez envoyer au serveur : ");
	scanf("%s",path_name);
	/* pour vider le buffer du \n */
	getchar();
	int fd;

	int last_slash = -1;
	for(int i = 0; *(path_name + i) != '\0'; i++)
		if(*(path_name + i) == '/') last_slash = i;
	
	strcpy(base_name,(path_name + last_slash + 1 ));

	if(!est_nom_fichier_comptine(base_name)) {
		printf("Format de fichier non valide. (requis : .cpt )\n");
		return -1;
	}

	if((fd = open(path_name, O_RDONLY)) < 0) {
		perror("open");
		return fd;
	}

	return fd;
}

void envoyer_comptine(int fd, int fd_comptine, char *base_name) {
	/* Envoi du nom_fichier et du titre de la comptine */
	char buf[256] = "";
	dprintf(fd,"%s\n",base_name);
	read_until_nl(fd_comptine,buf);
	dprintf(fd,"%s",buf);
	/* lecture réponse du serveur si comptine acceptée : 
	0 oui, 1 non car fichier existant, 2 non car titre existant */
	uint16_t response;
	exact_read(fd,&response,sizeof(uint16_t));
	response = ntohs(response);
	if(response == 1) {
		printf("Fichier %s déjà existant dans le catalogue.\n",base_name);
		close(fd_comptine);
		return;
	} if(response == 2) {
		printf("Une comptine avec un titre identique existe déjà dans le catalogue.\n");
		close(fd_comptine);
		return;
	}

	int i;
	/* On lit et envoie toutes les lignes du fichier comptine ouvert
	Lorsque l'on a atteint la fin du fichier, on envoie 2 lignes vides */
	for(;;) {
		while((i = read_until_nl(fd_comptine,buf)) > 0) {
			buf[i+1] = '\0';
			dprintf(fd,"%s",buf);
		}
		if((i = read_until_nl(fd_comptine,buf)) == 0) {
			dprintf(fd,"\n\n");
			close(fd_comptine);
			return;
		} buf[i+1] = '\0';
		dprintf(fd,"\n%s",buf);
	}
}

uint16_t stdin_request(char *request_buf) {
		printf("$");
		fgets(request_buf, 255, stdin);
		if(request_buf[0] == '\n' || request_buf[0] == ' ')	return 5;
		if(!strcmp(request_buf,"quitter\n")) 				return 0;
		if(!strcmp(request_buf,"liste\n")) 					return 1;
		if(!strcmp(request_buf,"demander\n")) 				return 2;
		if(!strcmp(request_buf,"envoyer\n"))				return 3;
		if(!strcmp(request_buf,"aide\n")) 					return 4;
		printf("Commande non reconnue. Tapez \"aide\" pour avoir une liste des commandes.\n");
		return 5;
}
