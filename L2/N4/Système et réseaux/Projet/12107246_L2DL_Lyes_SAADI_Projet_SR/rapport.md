---
title: Projet de Système et Réseau
subtitle: L2 Double Licence
author: Lyes Saadi (12107246)
header-includes: |
    \usepackage{fancyhdr}
    \usepackage{lastpage}
    \pagestyle{fancy}
    \fancyfoot[CO,CE]{}
    \fancyfoot[LO,RE]{Lyes Saadi (12107246)}
    \fancyfoot[LE,RO]{\thepage/\pageref{LastPage}}
output: pdf_document
...

# Rapport de projet

## Ajouts au protocole WCP

### Mini-langage

Pour intéragir avec l'utilisateur, la commande client accepte des commandes sous
la forme d'un mini-langage.

```
$ ./wcp_clt 127.0.0.1
INFO	 wcp_clt.c:73	 > connected to 127.0.0.1

     0 Dans sa maison, un grand cerf
     1 Petit escargot
     2 G comme Gaston
     3 Mon petit lapin
     4 Les petits poissons dans l'eau
     5 La famille tortue
INFO	 wcp_clt.c:79	 > type help for list of commands
> help
List of commands:
	quit	close the program
	help	display this help
	list	list all comptines
	get		ask for a comptine
	send	send a new comptine to the server
> list
     0 Dans sa maison, un grand cerf
     1 Petit escargot
     2 G comme Gaston
     3 Mon petit lapin
     4 Les petits poissons dans l'eau
     5 La famille tortue
> get
Quelle comptine voulez-vous ? (Entrer un entier entre 0 et 5) : 4
Les petits poissons dans l'eau

Les petits poissons dans l'eau
Nagent, nagent, nagent, nagent
Les petits poissons dans l'eau
Nagent aussi bien que les gros
> quit
```

On peut donc afficher la liste des comptines, demander une comptine, et
téléverser une comptine dans le sens qu'on veut.

Or, le protocole WCP original impose que l'interaction client serveur se passe
ainsi :

1. Le client se connecte au serveur

2. Le serveur accepte la connection du client

3. Le serveur envoie la liste des comptines au client

4. Le client demande à l'utilisateur la comptine qu'il veut

5. Le client envoie au serveur le choix de l'utilisateur

6. Le serveur envoie la comptine au client et ferme la connexion

7. Le client l'affiche à l'utilisateur et ferme la connexion

Afin de pouvoir faire ces opérations dans l'ordre qu'on veut, et autant de fois
que l'on veut, on altère ainsi le protocole de cette façon :

1. Le client se connecte au serveur

2. Le serveur accepte la connection du client

3. Le serveur envoie la liste des comptines au client

4. Le client attends une instruction de l'utilisateur. Si l'utilisateur choisi :

    - `quit` : Le client ferme sa connexion, et on continue à l'étape (5.)

    - `help` : Le client affiche une page d'aide, et on revient à l'étape (4.)

    - `list` :

        a. Le client envoie le caractère `l`

        b. Le serveur reçoit le caractère `l` et comprend que le client veut
        la liste, il renvoie donc la liste des comptines

        c. Le client reçoit et affiche la liste des comptines

        d. On revient à l'étape (4.)

    - `get` :
        
        a. Le client envoie le caractère `g`, puis demande à l'utilisateur la
        comptine qu'il veut

        b. Le serveur reçoit `g`, reçoit le numéro de la comptine, et renvoie la
        comptine demandée

        c. Le client reçoit et affiche la comptine

        d. On revient à l'étape (4.)

    - `send` :

        a. Le client envoie le caractère `s`, puis demande à l'utilisateur le
        fichier à téléverser, puis téléverse son contenu

        b. Le serveur reçoit `s`, reçoit le nom du fichier à téléverser,
        télécharge le contenu du fichier, et puis met à jour son catalogue,
        qu'il renvoie au client

        c. Le client affiche le nouveau catalogue à l'utilisateur

        d. On revient à l'étape (4.)

5. Le serveur voit que la connexion a été coupé, il ferme sa connexion à son
   tour

### Téléversement

Le téléversement permet de rajouter une nouvelle comptine au catalogue.

Le client va envoyer au serveur une ligne avec le nom du fichier, puis, le
contenu du fichier, et enfin deux lignes vides pour marquer la fin du
fichier.

Par exemple, le client pourrait envoyer ceci au serveur :
```
bonjour.cpt
bonjour

bonjour, comment allez-vous ?
 // ligne vide
 // ligne vide
```

Le serveur va enregistrer le contenu du fichier dans
`dirname/nom_du_fichier.cpt`. Mais, s'il exsite déjà un fichier avec le même
nom, ce dernier sera contenu à la place dans `dirname/nom_du_fichier-1.cpt`, et
si même ce dernier existe, on passera à `-2.cpt`, `-3.cpt`... etc...

## Fonctionnalités mises en œuvre

### Multi-threading

Pour permettre au serveur d'accepteur plusieurs requêtes en parallèle, un
système de multi-threading a été implémenté. Dès qu'une nouvelle connexion est
reçue, on crée un nouveau thread auquel qui s'occupera de traiter la requête. On
dispatch aussi immédiatement le thread pour ne plus avoir à s'en occuper, car on
ne veut pas garder d'œuil sur tous nos threads étant donné qu'on ne limite pas
le nombre de thread qu'on a.

Le thread va appeler la fonction `void* worker(void*)` à laquelle on passera
notre socket, l'adresse IP de notre correspondant et le dossier contenant les
comptines. Le worker va s'occuper de répondre à notre client et enfin de quitter
en libérant ses ressources.

### Logging

Un système complexe de logging à travers des macros imbriquées a été élaboré.
J'ai été inspiré par la librairie officielle
[`log`](https://crates.io/crates/log) de Rust pour le fonctionnement, et par
la librairie [`pretty-env-logger`](https://github.com/seanmonstar/pretty-env-logger)
pour l'affichage.

On va en effet définir plusieurs niveau de logging :

1. `DEBUG` : pour les informations sur le déroulement du programme, ce sont les
   plus fréquentes. Elles servent surtout à savoir où on s'est arrêté en cas de
   crash et pour pouvoir retracer le fil des évennements.

2. `INFO` : pour les informations sur les actions du programme, elle sont
   fréquentes. Elles servent à informer l'utilisateur ou le développeur des
   actions entreprises par le programme.

3. `ERROR` : pour les erreurs rencontrés par le programme. Ces erreurs ne sont
   pas bloquantes, et le programme peut se remettre en marche après avoir
   rencontré l'une de ces erreurs. Elles sont généralement dû à une mauvaise
   manipulation d'un utilisateur.

4. `FATAL` : pour les erreurs fatales rencontrés par le programme. Ce sont des
   erreurs pour lesquelles le programme ne peut pas se remettre. Rencontrer une
   de ces erreurs signifie la fin du programme.

Ces erreurs ont une hierarchie (celle présentée dans cette liste ordonnée) et
leur affichage suit cette hierarchie. Si on affiche un niveau de log, on affiche
aussi tous les niveaux plus important dans la hierarchie.

Le niveau de logging qu'on souhaite choisir est contrôlé par une variable dans
`wcp_clt.c` et `wcp_srv.c`, ce qui rend la configuration très simple, et ouvre
les portes à une configuration dynamique du logging (par un argument de ligne de
commande ou une variable variable d'environnement par exemple). On a aussi un
niveau `QUIET` pour ignorer tout logging.

On peut aussi imprimer nos logs dans un fichier de log, qui peut aussi être
configuré par une simple variable. On fera attention à ne pas imprimer dans ce
fichier des couleurs pour éviter l'apparition d'artefact destiné au terminal
dans un fichier texte. Si le fichier est défini à -1, le programme sera assez
intelligent pour simplement ignorer le logging dans un fichier. Par défaut,
`wcp_srv.c` définira comme fichier de log le fichier
`log_AAAA-MM-JJ_hh:mm:ss.txt`.

On a aussi deux mutex à initialiser pour éviter des conflits sur l'accès au
terminal et au fichier de log avec le parallélisme. Si ces mutex ne sont pas
initialisé, le programme ignorera simplement l'affichage des logs pour la
destination pour laquelle le mutex n'est pas initialisé.

## Problèmes rencontrés

### Stringification de `__LINE__`

Pour le logging, je voulais pouvoir connaître la ligne où une alerte s'était
déclenchée. On a de la chance, C a la macro parfaite pour ça : `__LINE__`. Mais...
`__LINE__` est un entier, et dans le contexte des macros que j'avais faites, il
aurait été ardu de faire passer ça par le formatage du \*printf.

J'ai donc voulu récupérer `__LINE__` en forme de primitive chaîne de caractère pour
pouvoir la concaténer au reste des primitives chaînes de caractères. Mais, C ne
donne pas de tel macro.

Heureusement, me rappelant de la stringification dans les macros en Rust, j'ai
cherché à voir s'il n'existait pas un outil similaire en C. En cherchant un peu
sur le net, bingo, j'ai trouvé la ligne de code parfaite pour le travail qu'on
cherche à accomplir :

```c
#define _STR(x) #x     // 2. On va stringifier le résultat.
#define STR(x) _STR(x) // 1. On va étendre notre macro grâce à un premier appel.
```

Cette macro va abuser en quelque sorte du méchanisme des macros en C. En effet,
il va d'abord forcer C à évaluer `__LINE__` en passant par une macro
intermédiaire (`STR(X)`), pour ensuite stringifier l'entier obtenu, qu'on pourra
concaténer !

### Mise à jour du catalogue avec le multi-threading

Avec le parallélisme, on risque d'avoir une situation de ce type se produire
avec un catalogue partagé entre les threads.

1. Alice : Se connecte au serveur et récupère la liste des comptines

2. Bob : Se connecte au serveur et téléverse une nouvelle comptine

3. Alice : Choisis ça comptine, et envoie l'indice correspondant au serveur

Sauf que depuis, les indices ont changé sans que Alice ne soit au courant à
cause du téléversement d'une nouvelle comptine.

Pour remédier à ça, j'ai décidé de faire en sorte qu'un catalogue soit local à
un thread, pour éviter qu'un thread change les indices sans que notre thread
ne soit au courant. Même si une comptine est dès lors ajouté par un autre
thread, les indices vers une comptine sur notre thread n'ont pas changé tant que
le catalogue n'a pas été mis à jour.

## Ressources externes

(vous pouvez cliquer sur les titres pour ouvrir un lien vers le site web
correspondant)

### Documentation
- Les manpages des appels systèmes et des fonctions C
- Pour l'élaborations des macros de test et de logging :
	+ [GCC - Variadic Macros](https://gcc.gnu.org/onlinedocs/cpp/Variadic-Macros.html)
	+ [GCC - Function-like Macros](https://gcc.gnu.org/onlinedocs/cpp/Function-like-Macros.html)
	+ [GCC - Standard Predefined Macros](https://gcc.gnu.org/onlinedocs/cpp/Standard-Predefined-Macros.html)
	+ [GCC - Stringification](https://gcc.gnu.org/onlinedocs/gcc-3.4.3/cpp/Stringification.html)
- [Everything you never wanted to know about ANSI escape codes](https://notes.burke.libbey.me/ansi-escape-codes/) : Pour améliorer le rendu terminal
- [Variables locales, globales et statiques en C](https://www.codequoi.com/variables-locales-globales-statiques-en-c/) : Sur les variables extern

### Forums d'aide
- [StackOverflow - C Macro - how to get an integer value into a string literal](https://stackoverflow.com/questions/40591312/c-macro-how-to-get-an-integer-value-into-a-string-literal)
- [StackOverflow - Building a date string in c](https://stackoverflow.com/questions/10917491/building-a-date-string-in-c)
- [What's the best way to check if a file exists in C?](https://stackoverflow.com/questions/230062/whats-the-best-way-to-check-if-a-file-exists-in-c)

## Aide reçue

Un peu d'aide reçue de Corentin Leroy-Bury par nos conversations. Et surtout,
merci à lui de m'avoir putain de faire remarqué que c'était TCP et pas UDP parce
que je ne sais pas lire.
