/* Lyes Saadi 12107246
 * Je déclare qu'il s'agit de mon propre travail. */

/* fichiers de la bibliothèque standard */
#include <asm-generic/socket.h>
#include <bits/types/struct_tm.h>
#include <bits/types/time_t.h>
#include <time.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
/* bibliothèque standard unix */
#include <unistd.h> /* close, read, write */
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
/* spécifique à internet */
#include <arpa/inet.h> /* inet_pton */
/* spécifique aux comptines */
#include "common.h"
#include "comptine_utils.h"
#include "config.h"

#define PORT_WCP 4321

enum log_level loglvl = DEBUG;
int logfile = -1;
pthread_mutex_t* loglock = NULL;
pthread_mutex_t* stderrlock = NULL;

void usage(char *nom_prog)
{
	fprintf(stderr, "Usage: %s repertoire_comptines\n"
			"serveur pour WCP (Wikicomptine Protocol)\n"
			"Exemple: %s comptines\n", nom_prog, nom_prog);
}
/** Retourne en cas de succès le descripteur de fichier d'une socket d'écoute
 *  attachée au port port et à toutes les adresses locales. */
int creer_configurer_sock_ecoute(uint16_t port);

/** Écrit dans le fichier de desripteur fd la liste des comptines présents dans
 *  le catalogue c comme spécifié par le protocole WCP, c'est-à-dire sous la
 *  forme de plusieurs lignes terminées par '\n' :
 *  chaque ligne commence par le numéro de la comptine (son indice dans le
 *  catalogue) commençant à 0, écrit en décimal, sur 6 caractères
 *  suivi d'un espace
 *  puis du titre de la comptine
 *  une ligne vide termine le message */
void envoyer_liste(int fd, struct catalogue *c);

/** Lit dans fd un entier sur 2 octets écrit en network byte order
 *  retourne : cet entier en boutisme machine. */
uint16_t recevoir_num_comptine(int fd);

/** Écrit dans fd la comptine numéro ic du catalogue c comme spécifié par le
 *  protocole WCP, c'est-à-dire :
 *  chaque ligne du fichier de comptine est écrite avec son '\n' final, y
 *  compris son titre
 *  deux lignes vides terminent le message */
void envoyer_comptine(int fd, const char *dirname, struct catalogue *c, uint16_t ic);

void recevoir_comptine(int fd, const char *dirname);

void log_init();

void *worker(void*);

struct worker_args {
	int sock;
	char addr[INET_ADDRSTRLEN];
	char* dirname;
};

int main(int argc, char *argv[])
{
	log_init();

	if (argc != 2) {
		usage(argv[0]);
		return 1;
	}

	PRTDBG("creating listening socket");
	int sock = creer_configurer_sock_ecoute(PORT_WCP);
	PRTINF("listening to incoming connections...");


	while (1) {
		struct sockaddr_in sa_clt;
		socklen_t sl = sizeof(sa_clt);

		PRTDBG("waiting for connection...");
		int clt_sock = accept(sock, (struct sockaddr*) &sa_clt, &sl);

		if (clt_sock == -1) {
			ERRNO("unable to accept connection");
			continue;
		}

		struct worker_args* args = (struct worker_args*) malloc(sizeof(struct worker_args));
		args->sock = clt_sock;
		args->dirname = argv[1];

		inet_ntop(AF_INET, &sa_clt.sin_addr, args->addr, sl);

		pthread_t th;
		pthread_create(&th, NULL, worker, args);
		pthread_detach(th);
	}

	close(sock);

	pthread_mutex_destroy(loglock);
	pthread_mutex_destroy(stderrlock);
	free(loglock);
	free(stderrlock);

	if (logfile != -1)
		close(logfile);

	return 0;
}

void log_init() {
	char buf[BUFSIZE];
	char timebuf[BUFSIZE - 4];
	time_t now = time(NULL);
	struct tm *t = localtime(&now);
	strftime(timebuf, BUFSIZE - 4, "%Y-%m-%d_%H:%M:%S", t);

	strcpy(buf, "log_");
	strcat(buf, timebuf);
	strcat(buf, ".txt");

	logfile = open(buf, O_WRONLY | O_CREAT, 0666);

	loglock = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t));
	stderrlock = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t));

	pthread_mutex_init(loglock, NULL);
	pthread_mutex_init(stderrlock, NULL);
}

void* worker(void* args) {
	struct worker_args* clt = (struct worker_args*) args;

	PRTINF("established connection with client %s", clt->addr);

	struct catalogue* cat;

	PRTDBG("creating catalog for %s", clt->addr);
	cat = creer_catalogue(clt->dirname);

	if (cat == NULL) {
		PRTFTL("unable to create catalog for %s", clt->addr);
		exit(4);
	}

	PRTDBG("catalog created for %s", clt->addr);

	PRTDBG("sending list to client %s", clt->addr);
	envoyer_liste(clt->sock, cat);
	PRTINF("sent list to client %s", clt->addr);

	char command;

	while (read(clt->sock, &command, 1)) {
		if (command == 'l') {
			PRTDBG("sending list to client %s", clt->addr);
			envoyer_liste(clt->sock, cat);
			PRTINF("sent list to client %s", clt->addr);
		} else if (command == 'g') {
			PRTDBG("waiting for choice of client %s", clt->addr);
			uint16_t nc = recevoir_num_comptine(clt->sock);
			PRTINF("client %s chose comptine #%" PRIu16, clt->addr, nc);

			PRTDBG("sending comptine #%" PRIu16 " to %s", nc, clt->addr);
			envoyer_comptine(clt->sock, clt->dirname, cat, nc);
			PRTINF("comptine #%" PRIu16 " sent to %s", nc, clt->addr);
		} else if (command == 's') {
			PRTDBG("receiving new comptine from %s", clt->addr);
			liberer_catalogue(cat);
			recevoir_comptine(clt->sock, clt->dirname);
			PRTDBG("received new comptine from %s", clt->addr);

			PRTDBG("recreating catalog for %s", clt->addr);
			cat = creer_catalogue(clt->dirname);

			if (cat == NULL) {
				PRTFTL("unable to recreate catalog for %s", clt->addr);
				exit(5);
			}

			PRTDBG("catalog created for %s", clt->addr);

			PRTDBG("sending list to client %s", clt->addr);
			envoyer_liste(clt->sock, cat);
			PRTINF("sent list to client %s", clt->addr);
		} else {
			PRTERR("bad request from client %s", clt->addr);
		}
	}

	liberer_catalogue(cat);
	close(clt->sock);
	PRTINF("closed connection with client %s", clt->addr);

	free(args);

	return NULL;
}

int creer_configurer_sock_ecoute(uint16_t port)
{
	struct sockaddr_in sa = {
		.sin_family = AF_INET,
		.sin_port = htons(port),
		.sin_addr.s_addr = htonl(INADDR_ANY)
	};

	int sock = socket(AF_INET, SOCK_STREAM, 0);

	if (sock == -1) {
		ERRNOFTL("unable to create a socket");
		exit(1);
	}

	int opt = 1;
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));

	if (bind(sock, (const struct sockaddr*) &sa, sizeof(sa)) == -1) {
		ERRNOFTL("unable to bind the socket to the port");
		exit(2);
	}

	if (listen(sock, 128) == -1) {
		ERRNOFTL("unable to listen to incoming connections");
		exit(3);
	}

	return sock;
}

void envoyer_liste(int fd, struct catalogue *c)
{
	for (int i = 0; i < c->nb && i <= UINT16_MAX; i++) {
		dprintf(fd, "%6d %s", i, c->tab[i]->titre);
	}
	dprintf(fd, "\n");
}

uint16_t recevoir_num_comptine(int fd)
{
	uint16_t nc;
	read(fd, &nc, sizeof(uint16_t));
	nc = ntohs(nc);

	return nc;
}

void envoyer_comptine(int fd, const char *dirname, struct catalogue *c, uint16_t ic)
{
	char filename[strlen(dirname) + strlen(c->tab[ic]->nom_fichier) + 2];
	strcpy(filename, dirname);
	strcat(filename, "/");
	strcat(filename, c->tab[ic]->nom_fichier);

	int file = open(filename, O_RDONLY);

	int size;
	char buf[256];
	while ((size = read(file, buf, 256)) > 0) {
		write(fd, buf, size);
	}
	write(fd, "\n\n", 2);

	close(file);
}

void recevoir_comptine(int fd, const char *dirname) {
	char buf[BUFSIZE];

	int size = read_until_nl(fd, buf);
	buf[size] = '\0';

	PRTDBG("receiving comptine `%s`", buf);

	char filename[strlen(dirname) + strlen(buf) + 7 + 2];
	strcpy(filename, dirname);
	strcat(filename, "/");
	strcat(filename, buf);

	buf[size - 4] = '\0';

	int i = 1;
	
	struct stat pfff;
	while (stat(filename, &pfff) == 0) {
		PRTDBG("`%s` exists", filename);
		char name[size + 7 + 4];
		sprintf(name, "%s-%d.cpt", buf, i++);
		strcpy(filename, dirname);
		strcat(filename, "/");
		strcat(filename, name);
		PRTDBG("trying `%s`", filename);
	}

	int file = open(filename, O_WRONLY | O_CREAT, 0666);
	PRTDBG("created file `%s`", filename);

	size = 0;
	int ligne_vide = 0;

	PRTDBG("writing file `%s`", filename);
	while (1) {
		size = read_until_nl(fd, buf);
		if (size) {
			if (ligne_vide)
				dprintf(file, "\n");
			ligne_vide = 0;

			buf[size] = '\n';
			buf[size + 1] = '\0';
			dprintf(file, "%s", buf);
		} else {
			if (ligne_vide)
				return;
			ligne_vide = 1;
		}
	}
	PRTDBG("`%s` wrote", filename);

	close(file);
}
