/* Lyes Saadi 12107246
 * Je déclare qu'il s'agit de mon propre travail. */

/* fichiers de la bibliothèque standard */
#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
/* bibliothèque standard unix */
#include <strings.h>
#include <unistd.h> /* close, read, write */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
/* spécifique à internet */
#include <arpa/inet.h> /* inet_pton */
/* spécifique aux comptines */
#include "common.h"
#include "comptine_utils.h"
#include "config.h"

#define PORT_WCP 4321

enum log_level loglvl = INFO;
int logfile = -1;
pthread_mutex_t* loglock = NULL;
pthread_mutex_t* stderrlock = NULL;

void usage(char *nom_prog)
{
	fprintf(stderr, "Usage: %s addr_ipv4\n"
			"client pour WCP (Wikicomptine Protocol)\n"
			"Exemple: %s 208.97.177.124\n", nom_prog, nom_prog);
}

/** Retourne (en cas de succès) le descripteur de fichier d'une socket
 *  TCP/IPv4 connectée au processus écoutant sur port sur la machine d'adresse
 *  addr_ipv4 */
int creer_connecter_sock(char *addr_ipv4, uint16_t port);

/** Lit la liste numérotée des comptines dans le descripteur fd et les affiche
 *  sur le terminal.
 *  retourne : le nombre de comptines disponibles */
uint16_t recevoir_liste_comptines(int fd);

/** Demande à l'utilisateur un nombre entre 0 (compris) et nc (non compris)
 *  et retourne la valeur saisie. */
uint16_t saisir_num_comptine(uint16_t nb_comptines);

/** Écrit l'entier ic dans le fichier de descripteur fd en network byte order */
void envoyer_num_comptine(int fd, uint16_t nc);

/** Affiche la comptine arrivant dans fd sur le terminal */
void afficher_comptine(int fd);

void envoyer_comptine(int fd, char* filename);

int main(int argc, char *argv[])
{
	stderrlock = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(stderrlock, NULL);

	if (argc != 2) {
		usage(argv[0]);
		return 1;
	}

	PRTDBG("connecting to %s\n", argv[1]);
	int sock = creer_connecter_sock(argv[1], PORT_WCP);
	PRTINF("connected to %s\n", argv[1]);

	PRTDBG("asking for list of comptines");
	uint16_t nb_comptines = recevoir_liste_comptines(sock);
	PRTDBG("list of comptines received");

	_EPRTINF("type help for list of commands");

	char command[BUFSIZE];

	while (1) {
		printf(LOG "> " CLR DBG);
		scanf("%s", command);
		printf(CLR);

		if (strncasecmp("quit", command, 4) == 0) {
			break;
		} else if (strncasecmp("help", command, 4) == 0) {
			printf("List of commands:\n");
			printf("\t" DBG "quit" CLR "\tclose the program\n");
			printf("\t" DBG "help" CLR "\tdisplay this help\n");
			printf("\t" DBG "list" CLR "\tlist all comptines\n");
			printf("\t" DBG "get" CLR "\t\task for a comptine\n");
			printf("\t" DBG "send" CLR "\tsend a new comptine to the server\n");
		} else if (strncasecmp("list", command, 4) == 0) {
			write(sock, "l", 1);

			PRTDBG("asking for list of comptines");
			nb_comptines = recevoir_liste_comptines(sock);
			PRTDBG("list of comptines received");
		} else if (strncasecmp("get", command, 4) == 0) {
			write(sock, "g", 1);

			PRTDBG("asking for a comptine");
			uint16_t nc = saisir_num_comptine(nb_comptines);
			PRTDBG("asking for comptine #%" PRIu16, nc);

			envoyer_num_comptine(sock, nc);

			afficher_comptine(sock);
			PRTDBG("received comptine #%" PRIu16, nc);
		} else if (strncasecmp("send", command, 4) == 0) {
			char filename[BUFSIZE];
			printf("Le nom du fichier de la comptine à envoyer : ");
			scanf("%s", filename);

			if (!est_nom_fichier_comptine(filename)) {
				PRTERR("file `%s` not a comptine", filename);
				continue;
			}

			struct stat pfff;
			if (stat(filename, &pfff) != 0) {
				PRTERR("file `%s` not found", filename);
				continue;
			}

			write(sock, "s", 1);

			PRTDBG("sending the filename");
			int size = strlen(filename);
			filename[size] = '\n';
			write(sock, filename, size + 1);

			filename[size] = '\0';
			PRTDBG("sending a comptine");
			envoyer_comptine(sock, filename);

			nb_comptines = recevoir_liste_comptines(sock);
			PRTDBG("new list of comptines received");
		} else {
			PRTERR("unknown command `%s`, please type help for a list of commands", command);
		}
	}

	close(sock);

	if (logfile != -1)
		close(logfile);

	pthread_mutex_destroy(stderrlock);
	free(stderrlock);

	return 0;
}

int creer_connecter_sock(char *addr_ipv4, uint16_t port)
{
	struct sockaddr_in sa = {
		.sin_family = AF_INET,
		.sin_port = htons(port),
	};

	if (inet_pton(AF_INET, addr_ipv4, &sa.sin_addr) != 1) {
		PRTFTL("unable to parse the IPv4 address");
		exit(1);
	}

	int sock = socket(AF_INET, SOCK_STREAM, 0);

	if (sock == -1) {
		ERRNOFTL("unable to create a socket");
		exit(2);
	}

	if (connect(sock, (struct sockaddr *) &sa, sizeof(sa)) == -1) {
		ERRNOFTL("unable to connect to the server");
		exit(3);
	}

	return sock;
}

uint16_t recevoir_liste_comptines(int fd)
{
	char buf[BUFSIZE];
	int size = 0;
	uint16_t i = 0;

	while ((size = read_until_nl(fd, buf)) != 0) {
		buf[size + 1] = '\0';
		printf("%s", buf);
		i++;
	}

	return i;
}

uint16_t saisir_num_comptine(uint16_t nb_comptines)
{
	uint16_t nc = UINT16_MAX;

	printf("Quelle comptine voulez-vous ? (Entrer un entier entre 0 et %d) : ", nb_comptines - 1);
	scanf("%" SCNu16, &nc);

	while (nc >= nb_comptines) {
		_EPRTERR("Entrez un nombre entre 0 et %d", nb_comptines - 1);
		printf("Quelle comptine voulez-vous ? (Entrer un entier entre 0 et %d) : ", nb_comptines - 1);
		scanf("%" SCNu16, &nc);
	}

	return nc;
}

void envoyer_num_comptine(int fd, uint16_t nc)
{
	nc = htons(nc);
	write(fd, &nc, sizeof(uint16_t));
}

void afficher_comptine(int fd)
{
	char buf[BUFSIZE];
	int size = 0;
	int ligne_vide = 0;

	while (1) {
		size = read_until_nl(fd, buf);
		if (size) {
			if (ligne_vide)
				printf("\n");
			ligne_vide = 0;

			buf[size] = '\n';
			buf[size + 1] = '\0';
			printf("%s", buf);
		} else {
			if (ligne_vide)
				return;
			ligne_vide = 1;
		}
	}
}

void envoyer_comptine(int fd, char *filename) {
	int file = open(filename, O_RDONLY);

	if (file == -1) {
		ERRNO("unable to open file");
		return;
	}

	char buf[256];
	int size;

	while ((size = read(file, buf, 256)) > 0) {
		PRTDBG("hi");
		write(fd, buf, size);
		buf[size+1] = '\0'; 
		PRTDBG("%s %d", buf, size);
	}

	write(fd, "\n\n", 2);

	close(file);
}
