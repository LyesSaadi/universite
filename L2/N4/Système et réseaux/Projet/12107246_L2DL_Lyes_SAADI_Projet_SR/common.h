/* Lyes Saadi 12107246
 * Je déclare qu'il s'agit de mon propre travail. */

#ifndef __COMMON_H
#define __COMMON_H

#include <errno.h>
#include <pthread.h>

// Stringification
#define _STR(x) #x     // 2. On va stringifier le résultat.
#define STR(x) _STR(x) // 1. On va étendre notre macro grâce à un premier appel.

// Primitive color codes
#define BOLD "\e[1m"
#define CLR "\e[0m"

// Debug color codes
#define DBG "\e[34m"
#define INF "\e[32m"
#define WRN "\e[33m"
#define ERR "\e[31m"
#define FTL BOLD ERR
#define LOG "\e[37m"
#define WHT "\e[97m"

// Debug info
#define _DBG(type, name, file_color, str_color, str) type name CLR "\t " file_color __FILE__ ":" STR(__LINE__) CLR str_color "\t > " str "\n" CLR
#define DEBUG(str) _DBG(DBG, "DEBUG", LOG, LOG, str)
#define INFO(str) _DBG(INF, "INFO", LOG, WHT, str)
#define ERROR(str) _DBG(ERR, "ERROR", WHT, WHT, str)
#define FATAL(str) _DBG(FTL, "FATAL", FTL, FTL, str)
#define _FILE(lvl, str) _DBG("", lvl, "", "", str)

// Debug macros
#define _LOCK(macro) if (stderrlock != NULL) {pthread_mutex_lock(stderrlock);macro;pthread_mutex_unlock(stderrlock);}
#define _EPRTDBG(str, ...) _LOCK(fprintf(stderr, DEBUG(str), ##__VA_ARGS__))
#define _EPRTINF(str, ...) _LOCK(fprintf(stderr, INFO(str), ##__VA_ARGS__))
#define _EPRTERR(str, ...) _LOCK(fprintf(stderr, ERROR(str), ##__VA_ARGS__))
#define _EPRTFTL(str, ...) _LOCK(fprintf(stderr, FATAL(str), ##__VA_ARGS__))
#define _EERRNO(str) _EPRTERR(str ": %s", strerror(errno))
#define _EERRNOFTL(str) _EPRTFTL(str ": %s", strerror(errno))

// Logging macros
#define _CHECKFILE(macro) if (logfile != -1 && loglock != NULL) {pthread_mutex_lock(loglock);macro;pthread_mutex_unlock(loglock);}
#define _LPRTDBG(str, ...) _CHECKFILE(dprintf(logfile, _FILE("DEBUG", str), ##__VA_ARGS__))
#define _LPRTINF(str, ...) _CHECKFILE(dprintf(logfile, _FILE("INFO", str), ##__VA_ARGS__))
#define _LPRTERR(str, ...) _CHECKFILE(dprintf(logfile, _FILE("ERROR", str), ##__VA_ARGS__))
#define _LPRTFTL(str, ...) _CHECKFILE(dprintf(logfile, _FILE("FATAL", str), ##__VA_ARGS__))
#define _LERRNO(str) _LPRTERR(str ": %s", strerror(errno))
#define _LERRNOFTL(str) _LPRTFTL(str ": %s", strerror(errno))

// Log levels
enum log_level {
	QUIET = 0,
	FATAL = 1,
	ERROR = 2,
	INFO = 3,
	DEBUG = 4,
};

#define PRTDBG(str, ...) if (loglvl >= DEBUG) {_EPRTDBG(str, ##__VA_ARGS__);_LPRTDBG(str, ##__VA_ARGS__);}
#define PRTINF(str, ...) if (loglvl >= INFO) {_EPRTINF(str, ##__VA_ARGS__);_LPRTINF(str, ##__VA_ARGS__);}
#define PRTERR(str, ...) if (loglvl >= ERROR) {_EPRTERR(str, ##__VA_ARGS__);_LPRTERR(str, ##__VA_ARGS__);}
#define PRTFTL(str, ...) if (loglvl >= FATAL) {_EPRTFTL(str, ##__VA_ARGS__);_LPRTFTL(str, ##__VA_ARGS__);}
#define ERRNO(str) if (loglvl >= ERROR) {_EERRNO(str);_LERRNO(str);}
#define ERRNOFTL(str) if (loglvl >= FATAL) {_EERRNOFTL(str);_LERRNOFTL(str);}

// Test macros
#define BGNTEST(n) printf("TEST " n " : ")
#define TEST(t) if (t) printf("."); else printf(ERR "F" CLR)
#define ENDTEST printf("\n\n")

#endif
