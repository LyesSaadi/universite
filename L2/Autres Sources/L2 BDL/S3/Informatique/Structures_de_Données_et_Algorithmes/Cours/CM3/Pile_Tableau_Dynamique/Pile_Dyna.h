#ifndef _PILE_DYNA_
#define _PILE_DYNA_

#include<stdio.h>
#include<stdlib.h>


//Déclaration des Structures

//"Dé-Référencement" du type int
typedef int E;


//Déclaration du type pile qui a
typedef struct Pile{
	unsigned int max;
	unsigned int cur;
	E * tab;
}Pile;

//Déclaration des Fonctions

//____________________________________________________

Pile * creer_Pile();

//____________________________________________________

void free (Pile * p);

//____________________________________________________

void push(Pile * p, E);

//____________________________________________________

E pop(Pile * p);


//____________________________________________________

int est_Vide(Pile * p);

//____________________________________________________

void afficher_Pile(Pile * p);

//____________________________________________________

void exo1(Pile * P1, Pile * P2, Pile * P3);




#endif
