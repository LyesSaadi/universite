#ifndef _LISTE_H_
#define _LISTE_H_

#include <stdlib.h>

#include <stdio.h>

#include"contact.h"

typedef struct liste liste;
struct liste
{

	contact *premier;
	int nb_contact;

} ;


/* à mettre dans un autre fichier 
typedef struct liste2 liste2;
struct liste2
{
	contact *premier;
	contact *dernier;
	int nb_contact;


}
*/

void inserer_contact(liste *l, contact *c);

void supprimer_contact (liste *l);

void afficher_un_contact(liste *l, contact *c);

void afficher_tous_les_contacts(liste *l, contact *c);

void chercher_un_contact(liste *l, contact *c);

void trier_les_contacts(liste *l, contact *c);
 
 











#endif
