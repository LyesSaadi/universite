/*
	tas.h
*/

typedef struct {
	int max;
	int n;
	int* tab;
} tas;

//____________________________________

tas* init_tas(int m);

//____________________________________

void affiche_tas_Tableau(tas* T);

//____________________________________

void affiche_tas_Arbre(tas* T);

//____________________________________

int est_Tas_Max(tas* T, int i);

//____________________________________

void affiche_Est_Tas_Max(tas* T);

//____________________________________

int tas_maximum(tas* T);

//____________________________________

void sommet(tas* T);

//____________________________________

void tas_insertion(int k, tas* T);

//____________________________________

int extraction(tas* T);

//____________________________________

void detruire_tas(tas* T);

//____________________________________

int max(int x, int y);

//____________________________________

int min(int x, int y);

//____________________________________

int puissance(int x, int y);

//____________________________________

void swap(int* x, int* y);

//____________________________________

int appartient(int* tab, int tabSize, int x);

//____________________________________
