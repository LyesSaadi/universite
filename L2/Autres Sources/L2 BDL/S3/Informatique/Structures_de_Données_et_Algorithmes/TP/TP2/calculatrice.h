//_______________________________________
typedef struct maillon_pile
{
	int val;
	maillon_pile * maillon_suivant;

}maillon_pile;



//_______________________________________
typedef struct pile_entier
{
	maillon_pile * premier;
//	maillon_pile * dernier;
	int taille;

}pile_entier;

//_______________________________________

pile_entier * creer_pile();

//_______________________________________

void detruire_pile(pile_entier * p);

//_______________________________________

int est_vide(pile_entier * p);
//_______________________________________

maillon_pile * creer_maillon_pile (int e);

//_______________________________________

void detruire_maillon_pile(maillon_pile * maillon);

//_______________________________________

void push(pile_entier * p, int e);

//_______________________________________

maillon_pile * pop(pile_entier * p);


//_______________________________________

maillon_pile * trouver_sommet(pile_entier * p);

//_______________________________________




