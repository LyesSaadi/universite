
/**
 * Write a description of class ClassB here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ClassB extends ClassA {
    public int x;

    /**
     * Constructor for objects of class ClassB
     */
    public ClassB(String monNom, int b) {
        super(monNom, 2*b);
        x = b;
    }

    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public int methodRedefinie(int y) {
        System.out.println("ClassB:methodRedefinie(" + y + ")[x=" + x + "]");
        return methode(x + y);
    }
    public String toString() {
        return super.toString() + "ClassB[x=" + x + "]";
    }
}
