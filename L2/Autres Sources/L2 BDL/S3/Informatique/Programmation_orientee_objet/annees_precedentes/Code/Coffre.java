
import java.util.ArrayList;
import java.util.LinkedList;

import examjanvier2016bis.ObjetZork;

/**
 * <p>
 * Un {@code Coffre} représente une collection non ordonnée et de taille
 * illimitée d'{@code ObjetZork} non {@code null}. Chaque {@code Coffre} possède
 * une serrure et ne peut être ouvert ou fermé qu'en utilisant un objet
 * {@code Cle} adapté spécifiquement à ce {@code Coffre}. Lorsqu'un
 * {@code Coffre} est fermé il se comporte comme s'il était vide, à l'exception
 * des méthodes {@code getPoids}, {@code equals}, {@code clone},
 * {@code hashCode} et {@code toString}. De plus, aucun objet ne peut être
 * ajouté ou retiré à un {@code Coffre} fermé. Un {@code Coffre} peut contenir
 * plusieurs occurences d'un même objet.
 * </p>
 * 
 * @invariant getDescription() != null;
 * @invariant !contient(null);
 * @invariant !estOuvert() ==> (getNbObjets() == 0);
 * @invariant getPoidsAVide() > 0;
 * @invariant getPoids() >= getPoidsAVide();
 * 
 * @author Marc Champesme
 * @since Novembre 2017
 *
 */
public class Coffre {
	private ArrayList<ObjetZork> contenu;
	private String description;
	private int poidsAVide;
	private boolean ouvert;

	/**
	 * Initialise un nouveau {@code Coffre} vide ouvert ayant pour nom et poids
	 * les valeurs spécifiées par les arguments.
	 * 
	 * @param description
	 *            la description de ce {@code Coffre}
	 * @param poidsVide
	 *            le poids à vide de ce {@code Coffre}
	 * 
	 * @requires description != null;
	 * @requires poidsVide > 0;
	 * @ensures estOuvert();
	 * @ensures getNbObjets() == 0;
	 * @ensures getDescription().equals(description);
	 * @ensures getPoidsAvide() == poidsVide;
	 * 
	 */
	public Coffre(String description, int poidsVide) {
		this.description = description;
		this.poidsAVide = poidsVide;
		contenu = new ArrayList<ObjetZork>();
		this.ouvert = true;
	}

	/**
	 * Initialise un nouveau {@code Coffre} ouvert ayant pour nom, clé et poids
	 * à vide les valeurs spécifiées par les arguments. Les {@code nbObjets}
	 * premiers éléments du tableau spécifié sont ajoutés au contenu de ce
	 * {@code Coffre} .
	 * 
	 * @param description
	 *            La description de ce {@code Coffre}
	 * @param poidsVide
	 *            Le poids à vide de ce {@code Coffre}
	 * @param desObjets
	 *            Tableau contenant les {@code nbObjets} à placer dans ce
	 *            {@code Coffre}
	 * @param nbObjets
	 *            Nombre d'objets à placer dans ce {@code Coffre}
	 * 
	 * @requires description != null;
	 * @requires poidsVide > 0;
	 * @requires desObjets != null;
	 * @requires desObjets.length >= nbObjets;
	 * @requires (\forall int i; i >= 0 && i < nbObjets; desObjets[i] != null);
	 * @ensures estOuvert();
	 * @ensures (\forall int i; i >= 0 && i < nbObjets; contient(desObjets[i]));
	 * @ensures getNbObjets() == nbObjets;
	 * @ensures getDescription().equals(description);
	 * @ensures getPoidsAvide() == poidsVide;
	 * @ensures getPoids() == (poidsVide + (\sum int i; i >= 0 && i < nbObjets;
	 *          desObjets[i].getPoids()));
	 * 
	 */
	public Coffre(String description, int poidsVide, ObjetZork[] desObjets,
			int nbObjets) {
		this(description, poidsVide); // Appel du deuxième constructeur de cette classe
		for (int i = 0; i < nbObjets; i++) {
			contenu.add(desObjets[i]);
		}
	}

	/**
	 * Renvoie l'état actuel de ce {@code Coffre}: {@code true} s'il est ouvert
	 * et {@code false} s'il est fermé.
	 * 
	 * @return {@code true} si ce {@code Coffre} est ouvert; {@code false} sinon
	 * 
	 * @pure
	 */
	public boolean estOuvert() {
		return ouvert;
	}

	/**
	 * Tente d'ouvrir ce {@code Coffre} avec la clé spécifiée. Si la clé
	 * spécifiée est adaptée à ce {@code Coffre}, il est garanti que ce
	 * {@code Coffre} est dans l'état ouvert après appel de cette méthode. Si la
	 * clé spécifiée n'est pas adaptée à ce {@code Coffre}, ce {@code Coffre}
	 * reste dans le même état (ouvert ou fermé) que précédemment.
	 * 
	 * @requires uneCle != null;
	 * @ensures uneCle.peutOuvrir(this) ==> estOuvert();
	 * @ensures \old(estOuvert()) ==> !\result;
	 * @ensures !uneCle.peutOuvrir(this) ==> !\result;
	 * @ensures (\old(!estOuvert()) && uneCle.peutOuvrir(this)) <==> \result;
	 * 
	 * @param uneCle
	 *            clé utilisée pour tenter d'ouvrir ce {@code Coffre}.
	 * @return {@code true} si la clé spécifiée a permis de faire changer l'état
	 *         de ce {@code Coffre} et {@code false} sinon.
	 */
	public boolean ouvrirAvec(Cle uneCle) {
		if (uneCle.peutOuvrir(this) && !estOuvert()) {
			ouvert = true;
			return true;
		}
		return false;
	}

	/**
	 * Tente de fermer ce {@code Coffre} avec la clé spécifiée. Si la clé
	 * spécifiée est adaptée à ce {@code Coffre}, il est garanti que ce
	 * {@code Coffre} est dans l'état fermé après appel de cette méthode. Si la
	 * clé spécifiée n'est pas adaptée à ce {@code Coffre}, ce {@code Coffre}
	 * reste dans le même état (ouvert ou fermé) que précédemment.
	 * 
	 * @requires cle != null;
	 * @ensures uneCle.peutOuvrir(this) ==> !estOuvert();
	 * @ensures \old(!estOuvert()) ==> !\result;
	 * @ensures !uneCle.peutOuvrir(this) ==> !\result;
	 * @ensures (\old(estOuvert()) && uneCle.peutOuvrir(this)) <==> \result;
	 * 
	 * @param uneCle
	 *            clé utilisée pour tenter de fermer ce {@code Coffre}.
	 * @return true si la clé spécifiée a permis de faire changer l'état de ce
	 *         {@code Coffre} et false sinon.
	 */
	public boolean fermerAvec(Cle uneCle) {
		if (uneCle.peutOuvrir(this) && estOuvert()) {
			ouvert = false;
			return true;
		}
		return false;

	}

	/**
	 * Renvoie le poids à vide de ce {@code Coffre}.
	 * 
	 * @return le poids à vide de ce {@code Coffre}
	 * 
	 * @pure
	 */
	public int getPoidsAVide() {
		return poidsAVide;
	}

	/**
	 * Renvoie la description de ce {@code Coffre}.
	 * 
	 * @return la description de ce {@code Coffre}.
	 * 
	 * @pure
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Renvoie le poids total de ce {@code Coffre}, c'est-à-dire la somme du
	 * poids à vide de ce {@code Coffre} et des poids de tous les objets qu'il
	 * contient. Renvoie le même résultat, que ce {@code Coffre} soit ouvert ou
	 * fermé.
	 * 
	 * @return le poids total de ce {@code Coffre}
	 * 
	 * @pure
	 */
	public int getPoids() {
		int poids = poidsAVide;
		for (ObjetZork oz : this.contenu) {
			poids += oz.getPoids();
		}
		return poids;
	}

	/**
	 * Renvoie le nombre d'{@code ObjetZork} présents dans ce {@code Coffre} si
	 * ce {@code Coffre} est ouvert ou 0 si ce {@code Coffre} est fermé.
	 * 
	 * @return le nombre d'{@code ObjetZork} présents dans ce {@code Coffre}
	 * 
	 * @ensures !estOUvert() ==> (\result == 0);
	 * @ensures (estOuvert() && getPoids() > getPoidsAVide()) ==> (\result > 0);
	 * 
	 * @pure
	 */
	public int getNbObjets() {
		if (estOuvert()) {
			return contenu.size();
		}
		return 0;
	}

	/**
	 * Ajoute l'objet spécifié aux objets présents dans ce {@code Coffre} s'il
	 * est ouvert. Si l'objet est déja présent dans ce {@code Coffre} un
	 * exemplaire supplémentaire de cet objet y est ajouté. L'argument doit etre
	 * non {@code null}. Si ce {@code Coffre} est fermé il n'est pas modifié.
	 * 
	 * @requires oz != null;
	 * @ensures estOuvert() <==> \result;
	 * @ensures estOuvert() ==> contient(oz);
	 * @ensures estOuvert() <==> (getNbObjets() == (\old(getNbObjets()) + 1));
	 * 
	 * @param oz
	 *            L'objet à ajouter dans ce {@code Coffre}.
	 * 
	 * @return {@code true} si l'objet a été effectivement ajouté ;
	 *         {@code false} sinon.
	 */
	public boolean ajouter(ObjetZork oz) {
		if (estOuvert()) {
			return contenu.add(oz);
		}
		return false;
	}

	/**
	 * Retire l'objet spécifié de ce {@code Coffre} s'il
	 * est ouvert et si l'objet était présent dans ce {@code Coffre}. Si l'objet
	 * était présent en plusieurs exemplaires dans ce {@code Coffre} un seul
	 * exemplaire de cet objet en est retiré. Si ce {@code Coffre} est fermé il
	 * n'est pas modifié.
	 * 
	 * @ensures \result <==> (estOuvert() && \old(contient(oz)));
	 * @ensures \result <==> (getNbObjets() == (\old(getNbObjets()) - 1));
	 * @ensures !estOUvert() ==> (getPoids() == \old(getPoids()));
	 * 
	 * @param oz
	 *            L'objet à retirer de ce {@code Coffre}.
	 * 
	 * @return {@code true} si l'objet a été effectivement retiré ;
	 *         {@code false} sinon.
	 */
	public boolean retirer(ObjetZork oz) {
		if (estOuvert()) {
			return contenu.remove(oz);
		}
		return false;
	}

	/**
	 * Si ce {@code Coffre} est ouvert, renvoie une nouvelle instance de tableau
	 * contenant tous les objets présents dans ce {@code Coffre}. Le tableau
	 * renvoyé contient, pour chaque objet, autant d'exemplaires de cet objet
	 * que ce {@code Coffre} en contient. La taille du tableau renvoyé est
	 * identique au nombre d'objets présents dans ce {@code Coffre} tel que
	 * renvoyé par la méthode {@code getNbObjets()}. L'ordre dans lequel les
	 * objets sont placé dans le tableau n'est pas significatif. Si ce
	 * {@code Coffre} est fermé, renvoie un tableau de taille 0.
	 * 
	 * @ensures \result != null;
	 * @ensures \result.length == getNbObjets();
	 * @ensures (\forall int i; i >= 0 && i < \resul.length;
	 *          contient(\result[i]))
	 * 
	 * @return Un tableau contenant tous les objets présents dans ce coffre si
	 *         ce {@code Coffre} est ouvert ; ou un tableau de taille 0 sinon.
	 * 
	 * @pure
	 */
	public ObjetZork[] getTabObjets() {
		if (estOuvert()) {
			return contenu.toArray(new ObjetZork[0]);
		}
		return new ObjetZork[0];
	}

	/**
	 * Renvoie {@code true} si ce {@code Coffre} est ouvert et contient au moins
	 * un exemplaire de l'objet spécifié. La présence d'un objet est testé en
	 * utilisant la méthode {@code equals} de la classe {@code ObjetZork}.
	 * 
	 * @ensures !estOuvert() ==> !\result;
	 * 
	 * @param oz
	 *            Objet dont on cherche à savoir s'il est présent dans ce
	 *            {@code Coffre}
	 * @return {@code true} si ce {@code Coffre} est ouvert et contient au moins
	 *         un exemplaire de l'objet spécifié ; {@code false} sinon
	 * 
	 * @pure
	 */
	public boolean contient(ObjetZork oz) {
		if (estOuvert()) {
			return this.contenu.contains(oz);
		}
		return false;
	}

	/**
	 * Compare ce {@code Coffre} avec l'objet spécifié. Le résultat est
	 * {@code true} si l'argument est un {@code Coffre} de même description, de
	 * même poids à vide, contenant les mêmes objets et qui est dans le même
	 * état ouvert/fermé.
	 * 
	 * @ensures !(o instanceof Coffre) ==> !\result;
	 * @ensures \result ==> getDescription().equals(((Coffre)
	 *          o).getDescription());
	 * @ensures \result ==> (getPoidsAVide() == ((Coffre) o).getPoidsAVide());
	 * @ensures \result ==> (getPoids() == ((Coffre) o).getPoids());
	 * @ensures \result ==> (estOuvert() == ((Coffre) o).estOuvert());
	 * 
	 * @param o
	 *            L'objet à comparer avec ce {@code Coffre}.
	 * @return {@code true} si les deux objets ont les mêmes caractéristiques ;
	 *         {@code false} sinon.
	 * @pure
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Coffre)) {
			return false;
		}

		Coffre autreCoffre = (Coffre) o;
		if (this.estOuvert() != autreCoffre.estOuvert()) {
			return false;
		}

		if (!description.equals(autreCoffre.getDescription())) {
			return false;
		}

		if (this.getPoidsAVide() != autreCoffre.getPoidsAVide()) {
			return false;
		}

		if (this.contenu.size() != autreCoffre.contenu.size()) {
			return false;
		}
		LinkedList<ObjetZork> l = new LinkedList<ObjetZork>(this.contenu);
		for (ObjetZork oz : autreCoffre.contenu) {
			if (!l.remove(oz)) {
				return false;
			}
		}
		return l.isEmpty();
	}

	/**
	 * Renvoie un code de hashage pour ce {@code Coffre}.
	 * 
	 * @return un code de hashage pour ce {@code Coffre}.
	 * @pure
	 */
	@Override
	public int hashCode() {
		int code = this.getDescription().hashCode();
		if (estOuvert()) {
			code++;
		}
		for (ObjetZork oz : this.contenu) {
			code += oz.hashCode();
		}
		return code + this.poidsAVide;
	}

	/**
	 * Renvoie une représentation de ce {@code Coffre} sous forme de chaîne de
	 * caractères.
	 * 
	 * @return une représentation de ce {@code Coffre} sous forme de chaîne de
	 *         caractères.
	 * @pure
	 */
	@Override
	public String toString() {
		return "Coffre:" + this.getDescription() + ";" + this.poidsAVide + ";"
				+ this.contenu + ";" + (estOuvert() ? "ouvert" : "fermé");
	}

}
