
/**
 * Write a description of class ClassA here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ClassA {
    public int x;
    private String nom;

    /**
     * Constructor for objects of class ClassA
     */
    public ClassA(String monNom, int a) {
        nom = monNom;
        x = a;
    }

    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public int methode(int y) {
        System.out.println("ClassA:methode(" + y + ")[x=" + x + "]");
        return x + y;
    }
    
    public int methodRedefinie(int y) {
        System.out.println("ClassA:methodRedefinie(" + y + ")[x=" + x + "]");
        return methode(x + y);
    }
    
    public String toString() {
        return "ClassA[nom=" + nom + ";x=" + x + "]";
    }
}
