
/**
 *  Monomes non modifiables à coefficients entiers, c'est-à-dire, les
 *  polynomes élémentaires de la forme coeff.x^(degré) où coeff et degré sont
 *  des entiers et degré est un entier positif ou nul.
 *
 * @invariant (\forall int exp; exp >= 0 && exp < getDegre();
 *			getCoefficient(exp) == 0);
 * @invariant getExposant() == getDegre();
 * @invariant getCoefficient() == getCoefficient(getExposant());
 *
 * @author     Marc Champesme
 * @version    17 mars 2012
 * @since      31 janvier 2006
 */
public class Monome extends Polynome {
        /*@
	  @ invariant (\forall int exp; exp >= 0 && exp < getDegre();
	  @		getCoefficient(exp) == 0);
	  @ invariant getExposant() == getDegre();
	  @ invariant getCoefficient() == getCoefficient(getExposant());
        @*/
        private int coefficient;

        /**
         *  Initialise un <code>Monome</code> nul, c'est-à-dire un
         * <code>Monome</code> de degré et de coefficient 0.
         *
         * @ensures estZero();
         */
        //@ ensures estZero();
        public Monome() {
        	super(0);
        }

        /**
         * Initialise un <code>Monome</code> à partir du coefficient et du degré
         * spécifié. Le coefficient doit être non nul, et le degré spécifié doit
         * être positif.
         *
         * @param  coefficient  L'entier non nul correspondant au coefficient du monome.
         * @param  exposant     L'entier positif correspondant au degré du monome.
         *
         * @requires coefficient != 0;
         * @requires exposant >= 0;
         * @ensures !estZero();
         * @ensures getDegre() == exposant;
         * @ensures getCoefficient() == coefficient;
         */
        /*@
	  @ requires coefficient != 0;
	  @ requires exposant >= 0;
	  @ requires exposant < (Integer.MAX_VALUE / 2);
	  @ ensures !estZero();
	  @ ensures getDegre() == exposant;
	  @ ensures getCoefficient() == coefficient;
        @*/
        public Monome(int coefficient, int exposant) {
                super(exposant);
                this.coefficient = coefficient;
        }

        public boolean estZero() {
                return coefficient == 0;
        }

       public int getCoefficient(int exp) {
                if (exp == getDegre()) {
                        return coefficient;
                }
                return 0;
        }

        /**
         *  Renvoie le coefficient de ce <code>Monome</code>.
         *
         * @return Le coefficient de ce <code>Monome</code>.
         *
         */
        public int getCoefficient() {
                return coefficient;
        }

        /**
         *  Renvoie l'exposant de ce <code>Monome</code>.
         *
         * @return    L'exposant de ce <code>Monome</code>.
         *
         */
        public int getExposant() {
                return getDegre();
        }

        public double evaluer(double x) {
                return coefficient * Math.pow(x, getDegre());
        }

        public Polynome additionner(Polynome p) {
                if (estZero()) {
                        return p;
                }
                if (p.estZero()) {
                        return this;
                }
                PolynomeCreux thisPoly = new PolynomeCreux(this);
                return thisPoly.additionner(p);
        }

        public boolean equals(Object o) {
                if (! (o instanceof Polynome)) {
                        return false;
                }

                if (this == o) {
                        return true;
                }

                Polynome p = (Polynome) o;
                if (getDegre() != p.getDegre()) {
                        return false;
                }
                if (! (p instanceof Monome)) {
                        for (int exp = 0; exp < getDegre(); exp++) {
                                if (p.getCoefficient(exp) != 0) {
                                        return false;
                                }
                        }
                }
                return getCoefficient() == p.getCoefficient(p.getDegre());
        }
}

