
/**
 *  Monomes non modifiables à coefficients entiers, c'est-à-dire, les
 *  polynomes élémentaires de la forme coeff.x^(degré) où coeff et degré sont
 *  des entiers et degré est un entier positif ou nul.
 *
 * @invariant getDegre() >= 0;
 * @invariant estZero() <==> (getCoefficient() == 0);
 * @invariant (getCoefficient() == 0) ==> (getDegre() == 0);
 * @invariant getExposant() == getDegre();
 * @invariant getCoefficient() == getCoefficient(getExposant());
 *
 * @author     Marc Champesme
 * @version    17 mars 2012
 * @since      31 janvier 2006
 */
public class Monome {
	/*@
	  @ invariant getDegre() >= 0;
	  @ invariant estZero() <==> (getCoefficient() == 0);
	  @ invariant (getCoefficient() == 0) ==> (getDegre() == 0);
	  @ invariant getExposant() == getDegre();
	  @ invariant getCoefficient() == getCoefficient(getExposant());
	  @*/
	private int coefficient;
	private int exposant;


	/**
	 *  Initialise un <code>Monome</code> nul, c'est-à-dire un 
	 * <code>Monome</code> de degré et de coefficient 0.
	 *
	 * @ensures estZero();
	 */
	//@ ensures estZero();
	public Monome() { }


	/**
	 * Initialise un <code>Monome</code> à partir du coefficient et du degré
	 * spécifié. Le coefficient doit être non nul, et le degré spécifié doit 
	 * être positif.
	 *
	 * @param  coefficient  L'entier non nul correspondant au coefficient du monome.
	 * @param  exposant     L'entier positif correspondant au degré du monome.
	 *
	 * @requires coefficient != 0;
	 * @requires exposant >= 0;
	 * @ensures !estZero();
	 * @ensures getDegre() == exposant;
	 * @ensures getCoefficient() == coefficient;
	 */
	/*@
	  @ requires coefficient != 0;
	  @ requires exposant >= 0;
	  @ requires exposant < (Integer.MAX_VALUE / 2);
	  @ ensures !estZero();
	  @ ensures getDegre() == exposant;
	  @ ensures getCoefficient() == coefficient;
	  @*/
	public Monome(int coefficient, int exposant) {
		this.coefficient = coefficient;
		this.exposant = exposant;
	}


	/**
	 * Renvoie <code>true</code> si ce <code>Monome</code> est le <code>Monome</code> 
	 * nul (i.e. monome de coefficient 0).
	 *
	 * @return <code>true</code> si ce <code>Monome</code> est le <code>Monome</code> nul 
	 * (i.e. monome de coefficient 0) ; <code>false</code> sinon.
	 * @pure
	 */
	//@ pure
	public boolean estZero() {
		return coefficient == 0;
	}


	/**
	 *  Renvoie le coefficient du terme de ce <code>Monome</code> (considéré 
	 * ici comme un polynome) dont l'exposant est spécifié.
	 *
	 * @param  exp  L'exposant du terme dont on cherche le coefficient.
	 * @return      Le coefficient du terme de ce polynome dont l'exposant est
	 *      spécifié.
	 *
	 * @requires exp >= 0;
	 * @ensures exp == getExposant() ==> \result == getCoefficient();
	 * @ensures exp != getExposant() ==> \result == 0;
	 * @pure
	 */
	/*@
	  @ requires exp >= 0;
	  @ ensures exp == getExposant() ==> \result == getCoefficient();
	  @ ensures exp != getExposant() ==> \result == 0;
	  @ pure
	  @*/
	public int getCoefficient(int exp) {
		if (exp == exposant) {
			return coefficient;
		}
		return 0;
	}


	/**
	 *  Renvoie le coefficient de ce <code>Monome</code>.
	 *
	 * @return Le coefficient de ce <code>Monome</code>.
	 *
	 * @pure
	 */
	//@ pure
	public int getCoefficient() {
		return coefficient;
	}


	/**
	 *  Renvoie le degré de ce <code>Monome</code>.
	 *
	 * @return    Le degré de ce <code>Monome</code>.
	 *
	 * @pure
	 */
	//@ pure
	public int getDegre() {
		return exposant;
	}


	/**
	 *  Renvoie l'exposant de ce <code>Monome</code>.
	 *
	 * @return    L'exposant de ce <code>Monome</code>.
	 *
	 * @pure
	 */
	//@ pure
	public int getExposant() {
		return exposant;
	}


	/**
	 * Renvoie la valeur de ce <code>Monome</code> pour la valeur spécifiée 
	 * du paramètre.
	 *
	 * @param  x  Valeur du parametre pour laquelle est evalué le <code>Monome</code>.
	 * @return    Valeur du <code>Monome</code> en <code>x</code>.
	 *
	 * @ensures getDegre() == 0 ==> \result == (double) getCoefficient();
	 * @ensures getDegre() > 0 ==> ((x == 0.0) ==> (\result == 0.0));
	 * @ensures getDegre() > 0 ==> (* \result est le produit du coefficient de ce monome
	 *			par x élévé a la puissance getDegre() *);
	 * @pure
	 */
	/*@
	  @ ensures estZero() ==> (\result == 0);
	  @ ensures (x == 0) ==> (\result == getCoefficient(0));
	  @ ensures (x == 1) ==> (\result == (\sum int exp; exp >= 0 && exp <= getDegre();
	  @						getCoefficient(exp)));
	  @ ensures getDegre() == 0 ==> \result == (double) getCoefficient();
	  @ ensures getDegre() > 0 ==> ((x == 0.0) ==> (\result == 0.0));
	  @ ensures getDegre() > 0 ==> (* \result est le produit du coefficient de ce monome
	  @			par x élévé a la puissance getDegre() *);
	  @ pure
	  @*/
	public double evaluer(double x) {
		return coefficient * Math.pow(x, exposant);
	}


	/**
	 *  Renvoie une nouvelle instance de <code>PolynomeCreux</code> dont la valeur correspond à
	 *  l'addition du <code>Monome</code> spécifié à ce <code>Monome</code>.
	 *
	 * @param  m  Le <code>Monome</code> à additionner à ce <code>Monome</code>.
	 * @return    <code>PolynomeCreux</code> somme de ce <code>Monome</code> avec le <code>Monome</code> spécifié.
	 *
	 * @requires m != null;
	 * @ensures \result != null;
	 * @ensures (getDegre() != m.getDegre())
	 *	==> (\result.getDegre() == Math.max(getDegre(), m.getDegre()));
	 * @ensures ((getDegre() == m.getDegre()) 
	 *		&& ((getCoefficient() + m.getCoefficient()) != 0))
	 *			==> \result.getDegre() == getDegre();
	 * @ensures ((getDegre() == m.getDegre()) 
	 *		&& ((getCoefficient() + m.getCoefficient()) == 0))
	 *			==> \result.estZero();
	 * @ensures (getDegre() == m.getDegre())
	 *	==> (\result.getCoefficient(getDegre()) 
	 *		== (getCoefficient() + m.getCoefficient()));
	 * @ensures (getDegre() != m.getDegre()) 
	 *	==> (\result.getCoefficient(getDegre()) == getCoefficient());
	 * @ensures (getDegre() != m.getDegre()) 
	 *	==> (\result.getCoefficient(m.getDegre()) == m.getCoefficient());
	 * @pure
	 */
	/*@
	  @ requires m != null;
	  @ requires ((getDegre() == m.getDegre()) && (this.getCoefficient() < 0) && (m.getCoefficient() < 0))
	  @		==> (this.getCoefficient() > (Integer.MIN_VALUE - m.getCoefficient()));
	  @ requires ((getDegre() == m.getDegre()) && (this.getCoefficient() > 0) && (m.getCoefficient() > 0))
	  @		==> (this.getCoefficient() < (Integer.MAX_VALUE - m.getCoefficient()));
	  @ ensures \result != null;
	  @ ensures (getDegre() != m.getDegre())
	  @	==> (\result.getDegre() == Math.max(getDegre(), m.getDegre()));
	  @ ensures ((getDegre() == m.getDegre()) && ((getCoefficient() + m.getCoefficient()) != 0))
	  @			==> (\result.getDegre() == getDegre());
	  @ ensures ((getDegre() == m.getDegre()) && ((getCoefficient() + m.getCoefficient()) == 0))
	  @			==> \result.estZero();
	  @ ensures (getDegre() == m.getDegre()) 
	  @	==> (\result.getCoefficient(getDegre())  == (getCoefficient() + m.getCoefficient()));
	  @ ensures (getDegre() != m.getDegre()) 
	  @	==> (\result.getCoefficient(getDegre()) == getCoefficient());
	  @ ensures (getDegre() != m.getDegre())
	  @	==> (\result.getCoefficient(m.getDegre()) == m.getCoefficient());
	  @ pure
	  @*/
	public PolynomeCreux additionner(Monome m) {
		PolynomeCreux thisPoly,otherPoly;
		
		if (this.estZero()) {
			thisPoly = new PolynomeCreux();
		} else {
			thisPoly = new PolynomeCreux(this);
		}
		if (m.estZero()) {
			otherPoly = new PolynomeCreux();
		} else {
			otherPoly = new PolynomeCreux(m);
		}
		return thisPoly.additionner(otherPoly);
	}


	/**
	 *  Renvoie <code>true</code> si et seulement si l'objet spécifié est un <code>Monome</code> de
	 *  m&ecirc;me coefficient et même degré que ce <code>Monome</code>.
	 *
	 * @param  o  L'objet &agrave; comparer avec ce <code>Monome</code>.
	 * @return    <code>true</code> si et seulement si l'objet spécifié est un <code>Monome</code> de
	 *      m&ecirc;me coefficient et même degré que ce <code>Monome</code> ; <code>false</code> sinon.
	 * @ensures !(o instanceof Monome) ==> !\result;
	 * @ensures (o instanceof Monome) 
	 *	==> (\result
	 *		<==> ((getDegre() == ((Monome) o).getDegre())
	 *			&& (((Monome) o).getCoefficient() == this.getCoefficient())));
	 * @pure
	 */
	/*@ also
	  @ 	ensures !(o instanceof Monome) ==> !\result;
	  @	ensures \result ==> (this.hashCode() == o.hashCode());
	  @ also
	  @	requires (o instanceof Monome);
	  @	ensures \result
	  @		<==> (getDegre() == ((Monome) o).getDegre()
	  @			&& ((Monome) o).getCoefficient() == this.getCoefficient());
	  @ pure
	  @*/
	public boolean equals(Object o) {
		if (!(o instanceof Monome)) {
			return false;
		}

		if (this == o) {
			return true;
		}

		Monome autreMonome = (Monome) o;
		if (getDegre() != autreMonome.getDegre()) {
			return false;
		}

		return getCoefficient() == autreMonome.getCoefficient();
	}


	/**
	 *  Renvoie le code de hashage de ce <code>Monome</code>.
	 *
	 * @return    Le code de hashage de ce <code>Monome</code>.
	 * @pure
	 */
	//@ pure
	public int hashCode() {
		return (int) Math.pow(31, exposant) + exposant * getCoefficient();
	}


	/**
	 *  Renvoie une brève représentation textuelle de ce <code>Monome</code> de la forme
	 *  "Monome: 56.X^7". Si ce <code>Monome</code> est zero (i.e. <code>this.estZero()</code> est
	 *  true) la chaine "Monome: zero" est retournée.
	 *
	 * @return    Une brève représentation textuelle de ce <code>Monome</code>.
	 * @pure
	 */
	//@ pure
	public String toString() {
		String str = "Monome: ";
		if (getCoefficient() == 0) {
			return str + "zero";
		}
		return str + getCoefficient()
			 + ".X^" + getDegre();
	}

}

