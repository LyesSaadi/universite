
/**
 * Un Sortilege (ou sort) est enti�rement caract�ris� par son nom (une cha�ne de
 * caract�res non null) et sa puissance (un entier quelconque). Les Sortilege sont
 * habituellement utilis�s par les Magicien.
 * 
 * @invariant getNom() != null;
 * 
 * @author Marc Champesme 
 * @version 10 avril 2007
 * @since 6 mars 2007
 */
public class Sortilege {
    private String nom;
    private int puissance;

    /**
     * Initialise un Sortil�ge � partir du nom et de la puissance sp�cifi�s.
     * 
     * @pre nom != null;
     * @post getNom() == nom;
     * @post getPuissance() == puissance;
     * 
     * @param nom Le nom de ce Sortil�ge
     * @param puissance La puissance de ce Sortil�ge
     * @throws NullPointerException si le param�tre nom est null
     */
    public Sortilege(String nom, int puissance) {
        if (nom == null) {
            throw new NullPointerException("Le param�tre nom doit �tre non null");
        }
        this.nom = nom;
        this.puissance = puissance;
    }

    /**
     * Renvoie le nom de ce Sortilege.
     * 
     * @post \\result != null;
     * 
     * @return Le nom de ce Sortilege.
     */
    public String getNom() {
        return nom;
    }
    
    /**
     * Renvoie la puissance de Sortilege.
     * 
     * @return La puissance de Sortilege.
     */
    public int getPuissance() {
        return puissance;
    }
    
    /**
     * Compare en terme d'�galit� ce Sortilege avec l'objet sp�cifi�. Renvoie true
     * si et seulement si, l'objet sp�cifi� est un Sortil�ge de m�me nom et de m�me
     * puissance.
     * 
     * @post !(o instanceof Sortilege) ==> !\\result;
     * @post (o instanceof Sortilege) ==> 
     *          (\\result <==> getNom().equals(((Sortilege) o).getNom())
     *                       && getPuissance() == ((Sortilege) o).getPuissance());
     * 
     * @return true si et seulement si, l'objet sp�cifi� est un Sortil�ge de 
     * m�me nom et de m�me puissance ; false sinon.
     */
    public boolean equals(Object o) {
        if (!(o instanceof Sortilege)) {
            return false;
        }
        Sortilege sort = (Sortilege) o;
        return getNom().equals(sort.getNom())
                && getPuissance() == sort.getPuissance();
    }
    
    /**
     * Renvoie un code de hashage pour ce Sortilege.
     * 
     * @return un code de hashage pour ce Sortilege.
     */
    public int hashCode() {
        int code;
        code = nom.hashCode() * 31 + getPuissance();
        
        return code;
    }
    
    /**
     * Renvoie une repr�sentation textuelle rudimentaire de ce Sortilege.
     * 
     * @return Une repr�sentation textuelle de ce Sortilege.
     */
    public String toString() {
        return "Sortilege:" + nom + "/" + getPuissance();
    }
}
