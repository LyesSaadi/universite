import java.util.ArrayList;

/**
 * Un Magicien est un personnage caract�ris� par son nom et une r�serve de sorts.
 * Les sorts d'un Magicien sont repr�sent�s par des 
 * instances de la classe Sortilege. Un Magicien peut accumuler un nombre 
 * illimit� de sorts qu'il peut ensuite "invoquer", il lui est aussi possible
 * de poss�der plusieurs exemplaires d'un m�me sort (au sens de equals). Une fois 
 * un sort invoqu� par un Magicien, un exemplaire de ce sort disparait de sa 
 * r�serve de sorts. L'ordre dans lequel un Magicien invoque ses sorts n'est 
 * pas sp�cifi�, en cons�quence l'utilisateur de cette classe commettrait une 
 * erreur en faisant reposer le bon fonctionnement de son programme sur un 
 * ordre particulier.
 * 
 * @invariant getNom() != null;
 * @invariant getNbSorts() >= 0;
 * @invariant reserveEstVide() <==> (getNbSorts() == 0);
 * 
 * @author Marc Champesme 
 * @version 11 mars 2007
 * @since 6 mars 2007
 */
public class Magicien implements Cloneable {
    private String nom;
    private ArrayList mesSorts;
    
    /**
     * Initialise un Magicien dont le nom est la cha�ne de caract�res
     * sp�cifi�e et ne poss�dant encore aucun sort.
     * 
     * @pre nom != null;
     * @post getNom().equals(nom);
     * @post reserveEstVide();
     * 
     * @param nom le nom du magicien.
     * @throws NullPointerException si le param�tre nom est null
     */
    public Magicien(String nom) {
        if (nom == null) {
            throw new NullPointerException("Le param�tre nom doit �tre non null");
        }
        this.nom = nom;
        mesSorts = new ArrayList();
    }

    /**
     * Initialise un Magicien dont le nom est la cha�ne de caract�res
     * sp�cifi�e et poss�dant le sortil�ge sp�cifi�.
     * 
     * @pre nom != null;
     * @pre unSort != null;
     * @post getNom().equals(nom);
     * @post !reserveEstVide();
     * @post possede(unSort);
     * @post possedeCombienDe(unSort) == 1;
     * @post getNbSorts() == 1;
     * @post getPuissance() == unSort.getPuissance();
     * 
     * @param nom le nom du magicien.
     * @param unSort le sortil�ge du nouveau magicien
     * @throws NullPointerException si l'un des param�tres est null
     */
    public Magicien(String nom, Sortilege unSort) {
        if (nom == null) {
            throw new NullPointerException("Le param�tre nom doit �tre non null");
        }
        if (unSort == null) {
            throw new NullPointerException("Le param�tre Sortilege doit �tre non null");
        }
        this.nom = nom;
        mesSorts = new ArrayList();
        mesSorts.add(unSort);
    }
    
    /**
     * Renvoie le nom de ce magicien.
     * 
     * @post \\result != null;
     * 
     * @return le nom de ce magicien.
     */
    public String getNom() {
        return nom;
    }
    
    /**
     * Renvoie <code>true</code> si la r�serve de sorts de ce Magicien
     * est vide.
     * 
     * @post \\result <==> (getNbSorts() == 0);
     * 
     * @return <code>true</code> si ce Magicien ne poss�de aucun sort ; 
     * <code>false</code> sinon.
     */
    public boolean reserveEstVide() {
        return getNbSorts() == 0;
    }
    
    /**
     * Renvoie le nombre de sorts restant � ce magicien.
     * 
     * @post \\result >= 0;
     * 
     * @return le nombre de sorts restant � ce magicien.
     */
    public int getNbSorts() {
        return mesSorts.size();
    }
    
    /**
     * Invoque un des sorts d�tenus par ce magicien et renvoie la puissance
     * de ce sort. En cons�quence de cette invocation, un sort equals au sort
     * invoqu� est supprim� de l'ensemble des sorts d�tenus par ce
     * magicien. L'ordre dans lequel les sorts sont invoqu�s n'est pas sp�cifi�,
     * il est par contre garanti que chaque sort ajout� sera invoqu� avant que la
     * r�serve de ce magicien ne devienne vide.
     * Si ce magicien ne d�tient plus aucun sort: le nombre de sorts 
     * reste � 0 et la valeur renvoy�e est 0.
     * 
     * @post \\old(reserveEstVide()) ==> (getNbSorts() == \\old(getNbSorts()))
     *                                  && (\\result == 0);
     * @post !\\old(reserveEstVide()) 
     *                    ==> (getNbSorts() == \\old(getNbSorts()) - 1);
     * @post !\\old(reserveEstVide()) 
     *                    ==> (getPuissance() == \\old(getPuissance()) - \\result);
     * 
     * @return la puissance du sort invoqu� ou bien 0 si ce magicien ne
     * poss�de plus de sorts.
     */
    public int invoquer() {
        if (mesSorts.isEmpty()) {
            return 0;
        }
        Sortilege sort = (Sortilege) mesSorts.remove(mesSorts.size() - 1);
        return sort.getPuissance();
    }
    
    
    
    /**
     * Renvoie true si ce magicien poss�de un sort equals au sort 
     * sp�cifi�. 
     * 
     * @post (sort == null) ==> !\\result;
     * @post \\result <==> (possedeCombienDe(sort) > 0);
     * 
     * @param sort le sort � chercher dans la r�serve de ce magicien
     * @return true si ce magicien poss�de le sort sp�cifi� ;  false 
     * sinon
     */
    public boolean possede(Sortilege sort) {
        return mesSorts.contains(sort);
    }
    
    /**
     * Renvoie le nombre de sorts d�tenus par ce Magicien et equals 
     * au sort sp�cifi�.
     * 
     * @post \\result <= getNbSorts();
     * @post \\result >= 0;
     * @post (sort == null) ==> (\\result == 0);
     * @post (\\result > 0) <==> possede(sort);
     * 
     * @param sort le sort � chercher dans la r�serve de ce magicien
     * @return le nombre d'occurence du sort sp�cifi� dans la r�serve 
     * de ce Magicien.
     */
    public int possedeCombienDe(Sortilege sort) {
        if (sort == null) {
            return 0;
        }
        
        int nb = 0;

        for (Object o : mesSorts) {
            if (sort.equals(o)) {
                nb++;
            }
        }
        return nb;
    }
    
    /**
     * Ajoute le sort sp�cifi� � la r�serve de sorts de ce 
     * magicien. Le sort sp�cifi� doit �tre non <code>null</code>.
     * 
     * @pre sort != null;
     * @post !reserveEstVide();
     * @post possede(unSort);
     * @post possedeCombienDe(unSort) == (\\old(possedeCombienDe(unSort)) + 1);
     * @post getNbSorts() == (\\old(getNbSorts()) + 1);
     * @post getPuissance() == (\\old(getPuissance()) + unSort.getPuissance();     
     * 
     * @param sort le sort � ajouter � la r�serve de ce magicien.
     * @throws NullPointerException si le sort sp�cifi� est <code>null</code>.
     */
    public void ajouter(Sortilege unSort) {
        if (unSort == null) {
            throw new NullPointerException("Le param�tre Sortilege doit �tre non null");
        }
        mesSorts.add(unSort);
    }
    
    /**
     * Renvoie la somme des puissances des sorts d�tenus par
     * ce Magicien. Les sorts pouvant avoir une puissance positive, n�gative
     * ou nulle, il est possible que la valeur renvoy�e soit nulle alors
     * que le Magicien poss�de encore plusieurs sorts de puissance 
     * non nulle.
     * 
     * @return la puissance totale conf�r�e � ce Magicien par les sorts
     * qu'il d�tient.
     */
    public int getPuissance() {
        int puissance = 0;
        for (Object o : mesSorts) {
            Sortilege sort = (Sortilege) o;
            puissance += sort.getPuissance();
        }
        return puissance;
    }
    
    /**
     * Compare ce Magicien avec l'objet sp�cifi� et renvoie 
     * <code>true</code> si et seulement si cet objet est un
     * Magicien de m�me nom poss�dant les m�mes sorts (au 
     * sens de equals) et poss�dant chaque sort en autant d'exemplaires
     * que ce Magicien. L'ordre dans lequel seraient invoqu�s les
     * sorts des deux Magicien n'est pas pris en compte dans la
     * comparaison. Il est donc possible que deux Magicien equals
     * invoquent leurs sorts dans des ordres diff�rents.
     * 
     * @post !(obj instanceof Magicien) ==> !\\result;
     * @post (obj instanceof Magicien) ==>
     *            \\result ==> getNom().equals(((Magicien) obj).getNom());
     * @post (obj instanceof Magicien) ==>
     *            \\result ==> (getNbSorts() == ((Magicien) obj).getNbSorts());
     * @post (obj instanceof Magicien) ==>
     *            \\result ==> (getPuissance() == ((Magicien) obj).getPuissance());
     * @post (obj instanceof Magicien) ==>
     *            \\result ==> (hashCode() == ((Magicien) obj).hashCode());
     *              
     * @param obj l'objet � comparer � ce Magicien en terme d'�galit�
     * @return <code>true</code> si l'objet sp�cifi� est un Magicien
     * de m�me nom poss�dant les m�mes sorts.
     */
    public boolean equals(Object obj) {
        if (!(obj instanceof Magicien)) {
            return false;
        }
        
        Magicien autreMagicien = (Magicien) obj;
        if (!this.getNom().equals(autreMagicien.getNom())) {
            return false;
        }
        
        if (this.getNbSorts() != autreMagicien.getNbSorts()) {
            return false;
        }
        
        for (Object o : mesSorts) {
            Sortilege sort = (Sortilege) o;
            if (possedeCombienDe(sort) != autreMagicien.possedeCombienDe(sort)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Renvoie un nouveau Magicien equals � ce Magicien.
     * 
     * @post \\result != null;
     * @post \\result != this;
     * @post this.equals(\\result);
     * 
     * @return un clone de ce Magicien
     */
    public Magicien clone() {
        Magicien m = null;
        try {
            m = (Magicien) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
        m.mesSorts = (ArrayList) mesSorts.clone();
        return m;
    }
    
    /**
     * Renvoie un code de hashage pour ce Magicien.
     * 
     * @return un code de hashage pour ce Magicien.
     */
    public int hashCode() {
        int code = nom.hashCode();
        
        code = code * 31;
        for (Object o : mesSorts) {
            code += o.hashCode();
        }
        
        return code;
    }
    
    /**
     * Renvoie une repr�sentation concise sous forme de cha�ne de caract�res
     * de ce Magicien. 
     * 
     * @post \\result != null;
     * 
     * @return une repr�sentation de ce Magicien sous forme de cha�ne de 
     * caract�res.
     */
    public String toString() {
        return "Magicien:" + nom + mesSorts;
    }
}
