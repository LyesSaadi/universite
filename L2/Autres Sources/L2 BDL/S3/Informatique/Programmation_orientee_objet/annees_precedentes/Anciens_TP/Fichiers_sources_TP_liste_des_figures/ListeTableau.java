import java.util.Iterator;

/**
 *  <p>
 *
 *  Version restreinte et francis�e de la classe <code>java.util.ArrayList</code>
 *  (classe tr�s proche de la classe <code>java.util.Vector</code>). Comme la
 *  classe <code>java.util.ArrayList</code> (qui impl�mente l'interface <code>java.util.List</code>
 *  ), cette classe utilise un tableau pour impl�menter une structure de liste
 *  permettant de m�moriser des objets de classe arbitraire, dans ce contexte,
 *  la valeur sp�ciale <code>null</code> est consid�r� comme un �l�ment comme
 *  les autres pouvant appartenir &agrave; une liste au m&ecirc;me titre qu'un
 *  objet quelconque. En plus des op�rations basiques sur les listes, cette
 *  classe fournit des m�thodes pour manipuler la taille du tableau utilis� pour
 *  m�moriser la liste. Comme il est habituel pour les listes, une instance de
 *  <code>ListeTableau</code> peut contenir plusieurs exemplaires (i.e.
 *  plusieurs �l�ments identiques au sens de "equals") d'un m&ecirc;me �l�ment.
 *  </p> <p>
 *
 *  Chaque instance de <code>ListeTableau</code> poss�de une <i> capacit�</i> .
 *  La capacit� est la taille du tableau utilis� pour m�moriser les �l�ments de
 *  la liste. Elle est toujours au moins aussi grande que la taille de la liste
 *  (i.e. le nombre d'�l�ments de la liste). Au fur et &agrave; mesure que des
 *  �l�ments sont ajout�s &agrave; une <code>ListeTableau</code>, sa capacit�
 *  augmente automatiquement. </p> <p>
 *
 *  Lorsqu'une application peut pr�voir &agrave; l'avance qu'elle va proc�der
 *  &agrave; l'ajout d'un grand nombre d'�l�ments &agrave; une instance de
 *  <code>ListeTableau</code>, il lui est possible d'augmenter la capacit�
 *  &agrave; l'aide de la m�thode <code>assurerCapaciteMinimum</code> . Une
 *  telle pr�caution peut permettre de remplacer une succession de r�allocations
 *  de faible ampleur par un nombre plus r�duit de r�allocations plus
 *  importantes. </p>
 *
 * @author     Marc Champesme
 * @version    18 d�cembre 2004
 * @since      1 septembre 2004
 * @see        java.util.ArrayList
 * @see        java.util.AbstractList
 * @see        java.util.List
 */
 

public class ListeTableau<E> implements Liste<E> {
	/*@
	  @ invariant capacite() >= taille();
	  @*/
	private Object[] memo;
	private int nbElements;
	private final static int incrementCapacite = 5;

	/**
	 *  Initialise une liste vide avec une capacite initiale fix�e &agrave; dix.
	 */
	/*@
	  @ ensures capacite() == 10;
	  @ ensures estVide();
	  @*/
	public ListeTableau() {
		this(10);
	}

	//public Iterator<E> iterator() {
//		return new TableauIterator<E>(memo, taille());
//	}
	/**
	 *  Initialise une liste vide avec une capacite initiale fix�e &agrave; la
	 *  valeur sp�cifi�e.
	 *
	 * @param  capaciteInitiale  la capacite initiale de la liste
	 */
	/*@
	  @ requires capaciteInitiale >= 0;
	  @ ensures capacite() == capaciteInitiale;
	  @ ensures estVide();
	  @*/
	public ListeTableau(int capaciteInitiale) {
		memo = new Object[capaciteInitiale];
	}

	public E get(int index) {
		return (E) memo[index];
	}


	public void ajouter(int index, E element) {
		if (taille() == capacite()-1) {
			Object[] newMemo = new Object[taille() + incrementCapacite];
			for (int i = 0; i < index; i++) {
				newMemo[i] = memo[i];
			}
			newMemo[index] = element;
			for (int i = index + 1; i <= taille(); i++) {
				newMemo[i] = memo[i - 1];
			}
			memo = newMemo;
		} else {
			for (int i = taille(); i > index; i--) {
				memo[i] = memo[i - 1];
			}
			memo[index] = element;
		}
	}



	public E set(int index, E element) {
		Object oldElem = memo[index];
		memo[index] = element;
		return (E) oldElem;
	}


	public E retirer(int index) {
		E oldElem = (E) memo[index];

		for (int i = index; i < taille() - 1; i++) {
			memo[i] = memo[i + 1];
		}
		nbElements--;
		return oldElem;
	}


	public void vider() {
		nbElements = 0;

	}



	public int taille() {
		return nbElements;
	}


	/**
	 *  Renvoie la capacit� actuelle de cette liste.
	 *
	 * @return    la capacit� actuelle de cette liste
	 */
	/*@
	  @ ensures \result >= taille();
	  @ pure
	  @*/
	public int capacite() {
		return memo.length;
	}


	/**
	 *  Augmente, si n�cessaire, la capacit� de cette instance de <code>ListeTableau</code>
	 *  pour assurer qu'elle puisse contenir au moins le nombre d'elements sp�cifi�
	 *  par le param�tre.
	 *
	 * @param  capaciteMini  la capacit� minimum souhait�e
	 */
	/*@
	  @ ensures capacite() >= capaciteMini;
	  @ ensures this.taille() == \old(this.taille());
	  @ ensures (\forall int i; i >= 0 && i < taille();
	  @				this.get(i) == \old(this.get(i)));
	  @*/
	public void assurerCapaciteMinimum(int capaciteMini) {
		if (capacite() < capaciteMini) {
			Object[] newMemo = new Object[capaciteMini];
			for (int i = 0; i < taille(); i++) {
				newMemo[i] = memo[i];
			}
			memo = newMemo;
		}
	}



	/**
	 *  Ajuste la capacit� de cette instance de <code>ListeTableau</code> au nombre
	 *  d'�l�ments actuellement pr�sents dans la liste. Une application peut
	 *  utiliser cette op�ration pour minimiser l'espace m�moire utilis� par cette
	 *  instance de <code>ListeTableau</code>.
	 */
	/*@
	  @ ensures capacite() == taille();
	  @ ensures taille() == \old(taille());
	  @ ensures (\forall int i; i >= 0 && i < taille();
	  @				get(i) == \old(get(i)));
	  @*/
	public void ajusterALaTaille() {
		if (taille() < capacite()) {
			Object[] newMemo = new Object[taille()];
			for (int i = 0; i < taille(); i++) {
				newMemo[i] = memo[i];
			}
			memo = newMemo;
		}
	}
	
		public boolean estVide(){
		return taille() == 0; 
		}
		
	/**
	 *  Renvoie une copie de surface de cette instance de <code>ListeTableau</code>
	 *  (Les �l�ments eux-m&ecirc;mes ne sont pas clon�s).
	 *
	 * @return    un clone de cette instance de <code>ListeTableau</code> .
	 */
	/*@ also
	  @ ensures \result != this;
	  @ ensures \result.equals(this);
	  @ ensures ((ListeTableau) \result).taille() == this.taille();
	  @ ensures (\forall int i; i >= 0 && i < taille();
	  @				((ListeTableau) \result).get(i) == this.get(i));
	  @ pure
	  @*/
	//public Object clone() {
	//	ListeTableau leClone;

		/*
		 * L'appel a super.clone() plutot qu'un new ListeTableau()
		 * permet d'assurer que l'instance obtenue sera bien de la
		 * meme classe que this: c'est-a-dire ListeTableau pour un appel
		 * sur une instance de cette classe et ListeTableauEtendue si cette
		 * m�thode est appel�e (par h�ritage) sur une instance de
		 * ListeTableauEtendue. Par opposition, le recours a un
		 * new ListeTableau() n�cessiterait que clone soit de nouveau red�finie
		 * dans toutes les sous-classe pour pouvoir respecter son contrat
		 * (i.e. assertion ensures \typeof(\result) == \typeof(this)).
		 */
		//try {
			//leClone = (ListeTableau) super.clone();
		//} catch (CloneNotSupportedException e) {
			// Impossible car cette classe implemente
			// clone() et Object aussi
//			throw new InternalError(e.toString());
	//	}
		//leClone.memo = new Object[taille()];
		//for (int i = 0; i < taille(); i++) {
//			leClone.memo[i] = memo[i];
	//	}
		// Inutile de copier la valeur de nbElements car super.clone()
		// l'a d�ja fait
		// leClone.nbElements = taille();
//		return leClone;
	//}
		
		public void ajouter(E element){
		
		if (taille() == capacite()) {
			Object[] newMemo = new Object[taille() + incrementCapacite];
			for (int i = 0; i < taille(); i++) {
				newMemo[i] = memo[i];
			}
			newMemo[nbElements] = element;
			memo = newMemo;
			nbElements++;
		} else
		 
		{
		memo[nbElements] = element;
		nbElements++;
		}
		
	}
		
		public void ajouterTout(Liste<E> l){
		
		for(int i=0; i<l.taille(); i++){
			 E element = l.get(i); 
			 ajouter(element); 
			}
		}
		
		public boolean contient (E element){
		
		for(int i=0; i<taille(); i++){
		E elt = (E) memo[i];   
       if(elt.equals(element))
		    return true;
	    }
		return false;
    }
	 
  			
		public boolean contientTout(Liste<E> l){
		
		boolean contientAll = true; 		
		
		for(int i=0; i<l.taille() && contientAll; i++){
			E elt = l.get(i); 
			contientAll = contient(elt);
			}  
				
			return contientAll; 
		}

}

