import java.awt.Point;
/**
 *Modélise un Carre
 *
 * @author Valentina Dragos
 * @version 19 Mars 2007
 */
 /**
  * Un carré est un rectangle dont la hauteur et la largeur sont 
  * identiques et sont égales au côté du carré.
  */
  
public class Carre extends Rectangle {


		public Carre() {
		
		}
		
		public Carre(Point coinSupGauche, int cote) {
		
		super(coinSupGauche, cote, cote);
 		
		}
		
		public boolean equals(Object obj) {
		if (!(obj instanceof Carre)) {
			return false;
		}
		return true; 
		}	
	/**
	 * Remplace le côté de ce carré par la valeur
	 * spécifiée. Si la valeur spécifiée est inférieure ou égale
	 * à zéro, utilise l'opposé de la valeur spécifiée.
	 *
	 * @param  newCote nouvelle valeur du côté
	 */
	 
	
			
}