import java.awt.Point;
/**
 *Modélise un Carre
 *
 * @author Valentina Dragos
 * @version 19 Mars 2007
 */
 /**
  * Un carré est un rectangle dont la hauteur et la largeur sont 
  * identiques et sont égales au côté du carré.
  */
  
public class Polygone extends Figure {

		private Point coinSupGauche;
		private int nbCotes; 
		private int[] cote;
		
		
		public Polygone() {
		
		}
	
		public Polygone(Point coinSupGauche, int nbCotes) {
			this.coinSupGauche = coinSupGauche;
			this.nbCotes = nbCotes; 
		
		}
		
	/**
	 * ajouter le cote i du polygine
	 *@param cote : la longueur du coté
	 *@param i : le coté i  	  
	 */
	 
	public void setCote(int cote, int i) {
	
		this.cote[i] = cote;  	
		
	}
	
	/**
	 * retourne le nombre de sommets du polygone
	 *
	 */
	 
	public int getNbSommets() {
	
	return nbCotes;  
	
	}
	
		
		/** 
	 * Calcule le périmètre du polygone 
	 */
	 
	public int getPerimetre() {
		
		int perim =0; 
		for(int i=0; i<nbCotes; i++)
			perim += cote[i];
			
	    return perim; 			
		
		}

		
	public boolean equals(Object obj) {
		if (!(obj instanceof Polygone)) {
			return false;
		}
		
		Polygone polygone = (Polygone) obj;
		return this.coinSupGauche.equals(polygone.coinSupGauche)
			&& (this.getNbSommets() == polygone.getNbSommets()) 
			&& (this.getPerimetre() == polygone.getPerimetre());  
			
	}
		
	/** 
	 * Affiche le type  
	 */
		
	public String getType() {
		return "polygone";
	}	
	
}