import java.util.Scanner;

/** Un objet dans le jeu d'aventure Zork
 * Cette classe appartient au logiciel Zork.
 * Un ObjetZork est defini par son nom qui est une chaine de caractere de type String
 * Un poids qui est un entier toujours positif de type int (Integer)
 * Et une transportabilite qui est du type booleen et qui represente si l'objet peut etre transporte ou non.
 *
 * N.B. Toute instance de la classe sur laquelle vous appelez une methode ne doit pas etre null, naturellement, au risque d'engendrer une NullPointerException
 * Cela est garanti par nos constructeurs
 *
 * @invariant getNom()!=null;
 * @invariant getPoids() >= 0;
 * @author	Alexis Chandara
 * @author	Nicolas Floquet
 */

public class ObjetZork
{
	/*@
	  @invariant getNom() != null;
	  @invariant getPoids() >= 0;
	  @*/

	private String nom;
	private int poids;
	private boolean transp;
	
	
/*_______________________________________________________________________*/
//
//				CONSTRUCTEURS
//
/*_______________________________________________________________________*/

	/** Initialise un nouvel ObjetZork par defaut
	 * L'objet cree a pour nom la chaine de caractere nom
	 * Son poids est 0
	 * Sa transportabilite est false 
	 */
	/*@
	  @ensures getNom() != null;
	  @ensures getPoids() >= 0;
	  @*/

	public ObjetZork ()
	{
		this.nom = "nom";
		this.poids = 0;
		this.transp = false;
	}

//_________________________________________________________________________

	/** Initialise un nouvel ObjetZork en prenant en argument une chaine de caractere
	 * Le nom du nouvel objet sera la chaine de caractere entree en argument si cette derniere n'etait pas null
	 * Sinon, on initialise le nom de l'objet a la chaine "nom non null"
	 * Son poids est 0
	 * Sa transportabilite est false 
	 * @param nom Chaine de caractere qui, si elle est non null sera le nom du nouvel objet
	 */
	/*@
	  @ensures getNom() != null;
	  @ensures getPoids() >= 0;
	  @*/
	public ObjetZork (String nom)
	{
		if(nom != null)
		{
			if(nom.length() != 0)
			{
				this.nom = nom;
			}
			else
			{
				this.nom = "nom d'objet non null";
			}
		}
		else
		{
			this.nom = "nom d'objet non null";
		}
		this.poids = 0;
		this.transp = false;
		}

//_________________________________________________________________________
	
	/** Initialise un nouvel ObjetZork en prenant en argument une chaine de caractere, un entier, et un boolen
	 * Le nom du nouvel objet sera la chaine de caractere entree en argument si cette derniere n'etait pas null
	 * Sinon, on initialise le nom de l'objet a la chaine "nom non null"
	 * Le poids du nouvel objet sera l'entier entre en argument, sauf si l'argument est negatif, dans ce cas la, le poids sera 0
	 * Sa transportabilite est initialisee a la valeur du booleen entre en argument
	 * @param nom Chaine de caractere qui, si elle est non null sera le nom du nouvel objet
	 * @param poids entier, qui si il est positif ou nul sera le poids de l'objet, si l'entier est negatif, le poids sera 0
	 * @param transp booleen qui represente la transportabilite du nouvel objet
	 */
	/*@
	  @ensures this.getNom() != null;
	  @ensures this.getPoids() >= 0;
	  @*/

	public ObjetZork (String nom, int poids, boolean transp)
	{
		if(nom != null)
		{
			if(nom.length() != 0)
			{
				this.nom = nom;
			}
			else
			{
				this.nom = "nom d'objet non null";
			}
		}
		else
		{
			this.nom = "nom d'objet non null";
		}
		if(poids >= 0)
		{
			this.poids = poids;
		}
		else
		{
			this.poids = 0;
		}
		this.transp = transp;
	}

/*_______________________________________________________________________*/
//
//				ACCESSEURS
//
/*_______________________________________________________________________*/

	/** Renvoie la chaine de caracteres qui est le nom de l'ObjetZork sur lequel on appelle la methode
	 * Cette methode est pure et ne prend aucun parametre
	 *@return une chaine de caractere qui est le nom de l'ObjetZork de type String
	 */
	
	/*@
	  @ensures this.getNom() == this.nom;
	  @pure;
	  @*/
	
		public String getNom()
		{
			return this.nom;
		}

//_________________________________________________________________________

	/** Renvoie un entier qui est le poids de l'ObjetZork sur lequel on appelle la methode
	 * Cette methode est pure et ne prend aucun parametre
	 *@return un entier qui est le poids de l'ObjetZork de type int
	 */
	
	/*@
	  @ensures this.getPoids() == this.poids;
	  @pure;
	  @*/
	
		public int getPoids()
		{
			return this.poids;
		}

//_________________________________________________________________________


	/** Renvoie un booleen qui est la transportabilite de l'ObjetZork sur lequel on appelle la methode
	 * Cette methode est pure et ne prend aucun parametre
	 *@return un booleen qui est la transportabilite de l'ObjetZork de type boolean
	 */
	
	/*@
	  @ensures this.getTransp() == this.transp;
	  @pure;
	  @*/
		public boolean getTransp()
		{
			return this.transp;
		}

/*_______________________________________________________________________*/
//
//				AUTRES METHODES
//
/*_______________________________________________________________________*/


	/** Renvoie un booleen qui est la valeur de verite de x.equals(o) ou x est l'objet sur lequel on appelle la methode et o l'ObjetZork auquel on compare x
	 * Cette methode est pure
	 * Si la methode est appelee sur un ObjetZork null, elle renverra une exception
	 * Verifie d'abord que o contient une instance de la classe ObjetZork
	 * Si ce n'est pas le cas, on renvoit false
	 * Autrement, on initialise un nouvel ObjetZork aux valeurs de o
	 * Ensuite on compare tous les attributs des 2 ObjetZork, si au moins un attribut differe, alors on renvoie false
	 * Si tous les attributs sont egaux, on renvoie true
	 *@Throws NullPointerException si on l'appelle sur un ObjetZork null
	 *@param o un objet que l'on va comparer a l'objet sur lequel on a appele la methode
	 *@return un booleen qui est true si les objets sont egaux et false sinon, de type boolean
	 */
	
	/*@
	  @(\forall ObjetZork x; x != null; x.poids != null; x.nom != null);
	  @(\forall ObjetZork y; y != null; y.poids != null; y.nom != null);
	  @ensures x.equals(null) == false;
	  @ensures x.equals(x) == true;
	  @ensures x.getPoids() != y.getPoids() ==> x.equals(y) == false;
	  @ensures (!(x.getNom().equals(y.getNom()))) ==> x.(equals(y) == false);
	  @ensures x.getTransp() != y.getTransp() ==> x.equals(y) == false;
	  @pure;
	  @*/
		public boolean equals (Object o)
		{
			if(!(o instanceof ObjetZork))
			{
				return false;
			}
			
			ObjetZork objet = (ObjetZork) o;
		
			return (getPoids() == objet.getPoids() && getTransp() == objet.getTransp() && getNom().equals(objet.getNom())); 
		} 

//_________________________________________________________________________

	/** Renvoie sous forme de chaine de caracteres une description courte d'un ObjetZork
	 * Cette methode est pure
	 *@return une chaine de caractere decrivant l'ObjetZork de type String
	 */
	/*@
	  @pure;
	  @*/
		public String toString()
		{
			if(this.getTransp())
			{
				return "\n" + this.getNom() + " de " + this.getPoids() + " kg, transportable\n";
			}
			else
			{
				return "\n" + this.getNom() + " de " + this.getPoids() + " kg, non transportable\n";
			}
		}

//_________________________________________________________________________
		
	/** Renvoie le hashCode de l'ObjetZork, un entier qui depend des valeurs de ses attributs
	 * Cette methode est pure
	 * L'entier est decide selon les attributs de l'objet, si il est transportable, c'est la somme du hashCode de son nom multiplie par 31 et de son poids
	 *  S'il n'est pas transportable, c'est la soustraction du hashCode de son nom multiplie par -31 par son poids, le tout modulo 128
	 *@return un entier, qui est le hashCode de l'ObjetZork, de type int
	 */
	/*@
	  @requires this.getNom() != null;
	  @ensures (\for all ObjetZork x, y; (x.equals(y)) ==> (x.hashCode() == y.hashCode());
	  @pure;
	  @*/

	public int hashCode()
	{
		if(this.getTransp())
		{
			return (31*this.getNom().hashCode() + this.getPoids())%128;
		}
		else
		{
			return (-31*this.getNom().hashCode() - this.getPoids())%128;
		}
	}

/*_______________________________________________________________________*/
//
//				TEST DE LA CLASSE
//
/*_______________________________________________________________________*/


	public static void main (String [] args)
	{
		String n;
		int p;
		ObjetZork ob;

		System.out.println("Entrez un nom : ");
		Scanner c = new Scanner(System.in);
		n = c.nextLine();

		System.out.println("Entrez un poids : ");
		Scanner in = new Scanner(System.in);
		p = in.nextInt();

		ob = new ObjetZork(n, p, true);
		
		ObjetZork obMeme = new ObjetZork(n, p, true);
		ObjetZork ob2 = new ObjetZork(n+1, p, true);
		ObjetZork ob3 = new ObjetZork(n, p+1, true);
		ObjetZork ob4 = new ObjetZork(n, p, false);
		ObjetZork ob5 = new ObjetZork(n+1, p+1, true);
		ObjetZork ob6 = new ObjetZork(n, p+1, false);
		ObjetZork ob7 = new ObjetZork(n+1, p, false);
		ObjetZork ob8 = new ObjetZork(n+1, p+1, false);

		
		System.out.print("\nLe nom de l'objet ob est : " + ob.getNom() + ", son poids est de : " + ob.getPoids());
		if(ob.getTransp())
			System.out.println(" et il est transportable");
		else
			System.out.println(" et il n'est pas transportable");
		System.out.println("Le hashCode de l'objet ob est " + ob.hashCode());
		System.out.println("Le hashCode de l'objet ob4 est " + ob4.hashCode());
		System.out.println("Le hashCode de l'objet obMeme est " + obMeme.hashCode());
		
		System.out.println("ob.equals(ob) est " + ob.equals(ob));
		System.out.println("ob.equals(obMeme) est " + ob.equals(obMeme));
		System.out.println("ob.equals(ob2) est " + ob.equals(ob2));
		System.out.println("ob.equals(ob3) est " + ob.equals(ob3));
		System.out.println("ob.equals(ob4) est " + ob.equals(ob4));
		System.out.println("ob.equals(ob5) est " + ob.equals(ob5));
		System.out.println("ob.equals(ob6) est " + ob.equals(ob6));
		System.out.println("ob.equals(ob7) est " + ob.equals(ob7));
		System.out.println("ob.equals(ob8) est " + ob.equals(ob8));
				
		
		/*
		//obNull est cree en preuve de concept pour tester nos methodes
		//ObjetZork obNull = null;
		Donnerons toute une exception NullPointerException quand obNull est null: 
			System.out.println("obNull.equals(ob) est " + obNull.equals(ob));
			obNull.toString();
			obNull.getNom();
			obNull.getPoids();
			obNull.getTransp();
			obNull.hashCode();
		*/
		
		
	}	

}
