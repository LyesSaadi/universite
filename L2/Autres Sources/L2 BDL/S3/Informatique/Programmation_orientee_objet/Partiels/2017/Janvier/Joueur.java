import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.AbstractCollection;
import java.util.Collection;

public class Joueur extends AbstractCollection<ObjetZork> {
  private String nom;
  private int score;
  private ArrayList<ObjetZork> lesObjets;
  private Set<Joueur> lesJoueurs;

  public Joueur(String nom)
  {
    if(nom == null)
      throw new NullPointerException();
    this.nom = nom;
    this.score = 0;
    this.lesObjets = new ArrayList<ObjetZork>();
    this.lesJoueurs = new HashSet<Joueur>();
  }

  public Joueur(String nom, Collection <? extends ObjetZork> c)
  {
    if(nom == null)
      throw new NullPointerException();
    if(c.contains(null))
      throw new NullPointerException();
    this.nom = nom;
    this.score = 0;
    this.lesObjets = new ArrayList<ObjetZork>(c);
    this.lesJoueurs = new HashSet<Joueur>();
  }

  public int size()
  {
    return lesObjets.size();
  }

  public Iterator<ObjetZork> iterator()
  {
    return lesObjets.iterator();
  }

}
