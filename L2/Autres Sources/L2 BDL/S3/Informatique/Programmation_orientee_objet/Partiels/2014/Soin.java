/**
 * Les soins sont utilisés notamment par les classes Medecin et Joueur: les médecins peuvent
 * transporter des soins et les prodiguer aux joueurs, un Joueur peut recevoir des soins pour
 * augmenter son capital de points de vie.
 *
 * Un soin est entièrement caractérisé par son nom (une chaîne de caractères non null) et
 * sa valeur soignante (un entier positif ou nul). Une fois créées, les instances de 
 * cette classe ne peuvent pas être modifiées (i.e. classe non modifiable ou "immutable"):
 * les caractéristiques nom et valeur sont fixées lors de la création de l’instance et ne
 * peuvent pas être modifiées par la suite. Deux soins seront considérés comme égaux 
 * s’ils possèdent le même nom et la même valeur.
 *
 * @invariant getNom() != null;
 * @invariant getValeur() >= 0;
 *
 * @author Marc Champesme
 * @since 6 mars 2013
 * @version 6 mars 2013
 */
public class Soin {
	private String nom;
	private int valeur;
	
    /**
     * Initialise une nouvelle instance avec les nom et valeur spécifiés.
     *
     * @param nom le nom de ce Soin
     * @param valeur la valeur soignate de ce Soin
     *
     * @requires nom != null;
     * @requires valeur >= 0;
     * @ensures getNom().equals(nom);
     * @ensures getValeur() == valeur;
     */
	public Soin(String nom, int valeur) {
		this.nom = nom;
		this.valeur = valeur;
	}

    /**
     * Renvoie le nom de ce Soin.
     *
     * @return le nom de ce Soin.
     *
     * @pure
     */
	public String getNom() {
		return this.nom;
	}
	
    /**
     * Renvoie la valeur soignante de ce Soin. Cette valeur est un entier positif ou nul.
     *
     * @return la valeur soignante de ce Soin.
     *
     * @pure
     */
	public int getValeur() {
		return this.valeur;
	}
	
    /**
     * Compare ce Soin avec l'objet spécifié et renvoie true si et seulement si
     * cet objet est un Soin possédant le même nom et la même valeur que ce Soin.
     *
     * @param obj l'objet à comparer avec ce Soin.
     *
     * @ensures !(obj instanceof Soin) ==> !\result;
     * @ensures (obj instanceof Soin) 
     *          ==> (\result <==> (this.getNom().equals((Soin) obj)
     *                              && this.getValeur() == ((Soin) obj).getValeur()));
     * @ensures \result ==> (this.hashCode() == obj.hashCode());
     *
     * @pure
     */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Soin)) {
			return false;
		}
		
		Soin unSoin = (Soin) obj; 
		if (!this.nom.equals(unSoin.nom)) {
			return false;
		}
		
		return this.valeur == unSoin.valeur;
	}
	
    /**
     * Renvoie un code de hashage pour ce Soin.
     *
     * @return un code de hashage pour ce Soin.
     *
     * @pure
     */
	@Override
	public int hashCode() {
		return this.nom.hashCode() + this.valeur;
	}
	
    /**
     * Renvoie une représentation concise sous forme de chaîne de caractères
     * de ce Soin.
     *
     * @return une représentation de ce Soin sous forme de chaîne de caractères.
     *
     * @ensures \result != null;
     *
     * @pure
     */
	@Override
	public String toString() {
		return "Aliment:" + this.nom + ":" + this.valeur;
	}
}
