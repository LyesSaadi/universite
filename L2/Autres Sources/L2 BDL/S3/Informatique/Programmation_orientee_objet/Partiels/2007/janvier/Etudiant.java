import java.util.Date;

/**
 * <p>
 * Représentation d'un étudiant caractérisé par son nom, son prénom, sa date de
 * naissance et l'ensemble des cours auxquels il est inscrit.
 * </p>
 * 
 * <p>
 * Pour satisfaire le contrat de equals (tel qu'il est hérité de MathEnsemble<E>),
 * le test d'égalité par equals NE DOIT PAS prendre en compte les
 * caractéristiques spécifiques définies dans cette classe Etudiant (i.e. nom,
 * prenom, et date de naissance), par conséquent, l'implémentation fournie par
 * la super-classe TabMathEnsemble<E> est tout à fait correcte et ne nécessite
 * par une redéfinition. En conséquence, la redéfinition des méthodes hashCode
 * et toString n'est pas nécessaire.
 * </p>
 * 
 * <p>
 * Pour la méthode clone: les deux attributs nom et prenom étant immutable (car
 * instance de la classe String qui est immutable), le clonage effectué par la
 * méthode clone héritée est satisfaisant. Le cas de l'attribut dateNaissance
 * qui correspond à une instance d'une classe modifiable/mutable doit être
 * examiné de plus prêt: y a t'il un risque à ce que deux instances d'Etudiant
 * partagent une même instance de Date ? La réponse à cette question dépend
 * entièrement de l'implémentation de cette classe et c'est cette réponse qui
 * détermine si la date doit être clonée (ce qui implique alors de redéfinir la
 * méthode clone) ou bien si la même instance de la classe Date peut-être
 * partagée entre plusieurs instances d'Etudiant (la redéfinition de clone est
 * alors inutile):
 * <ul>
 * <li>Dans le cas de l'implémentation fournie ici, l'implémentation garantie
 * que la date de naissance ne pourra pas être modifiée (i.e. clonage de la date
 * à l'initialisation et dans l'accesseur getDateNaissance()). Il est donc
 * inutile de redéfinir la méthode clone.</li>
 * <li>Pour toute autre implémentation n'offrant pas cette garantie, le clonage
 * de l'attribut dateNaissance est INDISPENSABLE et la méthode clone doit donc
 * être impérativement redéfinie.</li>
 * </ul>
 * </p>
 * 
 * @invariant getNom() != null;
 * @invariant getPrenom() != null;
 * @invariant getDateNaissance() != null;
 * 
 * @author Marc Champesme
 * @since 16 mai 2007
 * @version 16 mai 2007
 * 
 */
public class Etudiant extends TabMathEnsemble<Cours> {
	private String nom;

	private String prenom;

	private Date dateNaissance;

	/**
	 * Initialise un Etudiant dont le nom, le prénom et la date de naissance
	 * sont les éléments spécifiés en paramètre. L'Etudiant ainsi initialisé
	 * n'est inscrit à aucun cours.
	 * 
	 * @requires nom != null;
	 * @requires prenom != null;
	 * @requires dateNaissance != null;
	 * @ensures getNom().equals(nom);
	 * @ensures getPrenom().equals(prenom);
	 * @ensures getDateNaissance().equals(dateNaissance);
	 * @ensures estVide();
	 * 
	 * @param nom
	 *            Le nom de l'étudiant.
	 * @param prenom
	 *            Le prenom de l'étudiant.
	 * @param dateNaissance
	 *            La date de naissance de l'étudiant
	 * 
	 * @throws NullPointerException
	 *             si l'un des paramètres est null.
	 */
	public Etudiant(String nom, String prenom, Date dateNaissance) {
		if ((nom == null) || (prenom == null) || (dateNaissance == null)) {
			throw new NullPointerException(
					"Le nom, le prénom et la date de naissance"
							+ "de l'Etudiant doivent être non null.");
		}
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = (Date) dateNaissance.clone();
	}

	/**
	 * Initialise un Etudiant dont le nom, le prénom et la date de naissance
	 * sont les éléments spécifiés en paramètre. L'Etudiant ainsi initialisé est
	 * inscrit aux cours contenus dans l'ensemble spécifié.
	 * 
	 * @requires nom != null;
	 * @requires prenom != null;
	 * @requires dateNaissance != null;
	 * @requires ens != null;
	 * @ensures getNom().equals(nom);
	 * @ensures getPrenom().equals(prenom);
	 * @ensures getDateNaissance().equals(dateNaissance);
	 * @ensures this.equals(ens);
	 * 
	 * @param nom
	 *            Le nom de l'étudiant.
	 * @param prenom
	 *            Le prenom de l'étudiant.
	 * @param dateNaissance
	 *            La date de naissance de l'étudiant
	 * @param ens
	 *            L'ensemble des cours auxquels cet étudiant doit être inscrit.
	 * 
	 * @throws NullPointerException
	 *             si l'un des paramètres est null.
	 */
	public Etudiant(String nom, String prenom, Date dateNaissance,
			MathEnsemble<? extends Cours> ens) {
		super(ens);
		if ((nom == null) || (prenom == null) || (dateNaissance == null)) {
			throw new NullPointerException(
					"Le nom, le prénom et la date de naissance"
							+ "de l'Etudiant doivent être non null.");
		}
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = (Date) dateNaissance.clone();
	}

	/**
	 * Renvoie le nom de cet Etudiant.
	 * 
	 * @ensures \result != null;
	 * 
	 * @return Le nom de cet Etudiant.
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Renvoie le prénom de cet Etudiant.
	 * 
	 * @ensures \result != null;
	 * 
	 * @return Le prénom de cet Etudiant.
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Renvoie la date de naissance de cet Etudiant.
	 * 
	 * @ensures \result != null;
	 * 
	 * @return La date de naissance de cet Etudiant.
	 */
	public Date getDateNaissance() {
		return (Date) dateNaissance.clone();
	}

}
