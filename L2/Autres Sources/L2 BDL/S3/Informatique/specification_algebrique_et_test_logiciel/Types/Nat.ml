(*
#use "Nat.ml" ;;
*)
#use "Bool.ml" ;;
type nat = Zero | Suc of nat ;;

let zero = Zero ;;
let one = Suc Zero ;;
let two = Suc (Suc Zero) ;;
let three = Suc (Suc (Suc Zero)) ;;


let rec add a b =
  match b with
  | Zero -> a
  | Suc i -> add (Suc a) i ;;

add one two ;;

let rec mult a b =
  match b with
  | Zero -> Zero
  | Suc i -> add a (mult a i) ;;

mult two three ;;

let rec conversion num =
  match num with
  | 0 -> Zero
  | _ -> if(num < 0)
    then
      failwith "Natural numbers are greater than or equal to 0"
    else
      Suc (conversion (num-1)) ;;

conversion 5 ;;

let rec printRec nat start =
  match nat with
  | Zero -> start
  | Suc i -> printRec i start+1 ;;

let print nat = printRec nat 0 ;;

print (mult two three) ;;

let rec inf a b =
  match a, b with
  | a, Zero -> FALSE
  | Zero, Suc i -> TRUE
  | Suc i, Suc j -> inf i j ;;

inf (conversion 5) (conversion 6) ;;
inf (conversion 5) (conversion 5) ;;
inf (conversion 7) (conversion 6) ;;

let rec sup a b =
  match a, b with
  | Zero, a -> FALSE
  | Suc i, Zero -> TRUE
  | Suc i, Suc j -> sup i j ;;

sup (conversion 5) (conversion 6) ;;
sup (conversion 5) (conversion 5) ;;
sup (conversion 7) (conversion 6) ;;

let rec prd a =
  match a with
  | Zero -> failwith "Undefined operation"
  | Suc i -> i ;;

print (prd (conversion 5)) ;;



let rec minus a b =
  if((inf a b) = TRUE)
  then
    failwith "Undefined operation"
  else
    match a, b with
    | a, Zero -> a
    | a, Suc j ->
      if((inf a (Suc j)) = TRUE)
      then
        failwith "Undefined operation"
      else
        prd (minus a j) ;;


minus (conversion 5) (conversion 5) ;;
minus (conversion 7) (conversion 6) ;;
minus (conversion 100) (conversion 100) ;;


(*
#use "Nat.ml" ;;
*)
