#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char *argv[]) {
  pid_t pidf1, pidf2;

  if ((pidf1 = fork()) == 0) {
    printf("Je suis fils1 : %d\n", getpid());
    exit(0);
  }
  if ((pidf2 = fork()) == 0) {
    printf("Je suis fils2 : %d\n", getpid());
    exit(0);
  }
  printf("Je suis le père. Mes fils sont %d et %d\n", pidf1, pidf2);
  exit(0);
}
