;;;  Licence 2 INFO - Univ Paris 13
;;;  Stefano Guerrini
;;;  AA 2015-16
;;; 
;;;  TD 3 - exercice 3
;;;  29/09/2015

	.ORIG x3000

	LD R0, chmin
	JSR minmaj
	ST R0, res
	
	LD R0, chmaj
	JSR minmaj
	ST R0, res

	LD R0, ch1
	JSR minmaj
	ST R0, res

	LD R0, ch2
	JSR minmaj
	ST R0, res

	LD R0, ch3
	JSR minmaj
	ST R0, res

	LD R0, ch4
	JSR minmaj
	ST R0, res

	HALT
res:	.BLKW 1
chmin:	.FILL x70
chmaj:	.FILL x52
ch1:	.FILL x60
ch2:	.FILL x7B
ch3:	.FILL x40
ch4:	.FILL x5B

a_min:	.FILL x61		; 'a'
z_min:	.FILL x7A		; 'z'
A_maj:	.FILL x41		; 'A'
Z_maj:	.FILL x5A		; 'Z'

minmaj:
	;; compaire ch avec 'z'
	LD R1, z_min
	NOT R1, R1
	ADD R1, R1, 1
	ADD R1, R0, R1
	BRP finminmaj		; char non alphabetique
	;; compaire ch avec 'a'
	LD R1, a_min
	NOT R1, R1
	ADD R1, R1, 1
	ADD R1, R0, R1
	BRN nonmin
	;; ch est un caratère minuscule
	;; R0 contient la position du caractère dans l'alphabet
	LD R0, A_maj
	ADD R0, R1, R0		; R0 contient ch converti en majuscule
	BR finminmaj
nonmin:
	;; compaire ch avec 'Z'
	LD R1, Z_maj
	NOT R1, R1
	ADD R1, R1, 1
	ADD R1, R0, R1
	BRP finminmaj		; char non alphabetique
	;; compaire ch avec 'A'
	LD R1, A_maj
	NOT R1, R1
	ADD R1, R1, 1
	ADD R1, R0, R1
	BRN finminmaj
	;; ch est un caratère majuscule
	;; R0 contient la position du caractère dans l'alphabet
	LD R0, a_min
	ADD R0, R1, R0		; R0 contient ch converti en minuscule
finminmaj:	RET

	.END
