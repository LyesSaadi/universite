#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
  pid_t pid = fork();
  
  if (pid == 0) {
    printf(">> C'est le fils qui parle.\n");
    printf(">>\tMon pid est %ld\n", (long int) getpid());
    printf(">>\tLe pid de mon père est %ld\n", (long int) getppid());
  } else {
    pid_t pidfils;
    wait(&pidfils);
    printf("> C'est le père qui parle.\n");
    printf(">\tLe pid de mon fils est %ld\n", (long int) pid);
    printf(">\tMon pid est %ld\n", (long int) getpid());
    printf(">\tLe pid du grand-père de mon fils, donc mon père est %ld\n", (long int) getppid());
  } 
}
