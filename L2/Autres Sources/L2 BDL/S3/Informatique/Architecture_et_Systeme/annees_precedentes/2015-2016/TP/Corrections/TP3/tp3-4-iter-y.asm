	.ORIG x3000
	
	;; on met les paramètres dans R1 et R2
	;; ce sont les adresses de deux chaînes d'exemple
	LEA R1, s1
	LEA R2, s2
	
	;; on saute à la sous-routine
	JSR strcmp

	HALT

	;; routine recursive pour la comparaison de deux chaines
	;; @param R1 R2 : les adresses de deux chaînes de caractères
	;; @result R0 : ??
	;; la routine modifie les registres R3, R4
strcmp:	LDR R3, R1, 0	; R3 = *R1 le caractère en tète à la première chaîne
	LDR R4, R2, 0	; R4 = *R2 le caractère en tète à la deuxième chaîne
	;; on compare les deux caractères
	NOT R4, R4
	ADD R4, R4, 1		; R4 = -R4
	ADD R0, R3, R4		; R0 = R3 - R4
	BRZ ch_egaux		; si zero, les deux caractères sont égaux
	;; sinon les chaîne diffèrent et
	;; on a trouvé le premier caractère qui diffère 
	;; R0 contient une valeur >0 si le caractère dans la première
	;; chaîne as une valeur plus grande, ou une valeur <0 s'il a une
	;; valeur plus petite
	RET
	;; on vérifie si le caractère (le même dans les deux chaînes)
	;; est le fin de chaîne (caractère 0)
ch_egaux: ADD R3, R3, 0
	BRNP next
	;; on a trouvé le fin de chaîne
	;; R0 contient la valeur 0
	RET    			; return
	;; il faut comparer les caractères suivants des chaînes
next:	ADD R1, R1, 1
	ADD R2, R2, 1
	;; appel récursive sur les restes des deux chaînes
	BR strcmp

	;; données : deux chaînes pour tester la routine
s1:	.STRINGZ "toto"
s2:	.STRINGZ "toso"

	.END
