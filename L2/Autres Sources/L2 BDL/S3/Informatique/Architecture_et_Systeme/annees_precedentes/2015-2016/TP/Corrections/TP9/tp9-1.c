#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char *argv[]) {
  int fd;
  
  if (argc < 2) {
    printf("Usage : tp4-1 path\n");
    return 1;
  }

  if ((fd = open(argv[1], O_CREAT | O_TRUNC | O_WRONLY, 0600)) == -1) {
    char buf[512];
    sscanf(buf, "je ne pas ouvrire le fichier %s", argv[1]);
    perror(buf);
    return 1;
  }

  dup2(fd, 1);

  if (execlp("cat", "cat", NULL) == -1)
    perror("erreur exec");
}
