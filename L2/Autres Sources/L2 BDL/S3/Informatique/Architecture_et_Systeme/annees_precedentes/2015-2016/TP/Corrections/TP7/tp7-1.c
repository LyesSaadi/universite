/* execmenu.c : menu avec exec */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int menu (void) {
  int choix;

  printf("1 - éditer avec vi\n");
  printf("2 - éditer avec emacs\n");
  printf("3 - affciher le fichier\n");
  printf("4 - détruire le fichier\n");
  printf("Entrer votre choix : ");

  scanf("%d", &choix) ;

  return choix;
}

int main (int argc, char* argv[]) {
  char* fichier;
  if (argc != 2){
    printf("Usage : %s <fichier-`a-traiter>\n", argv[0]);
    exit(1);
  }
  fichier = argv[1];

  switch(menu()) {
  case 1 :
    execlp("vi", "vi", fichier, NULL);
    break ;
  case 2 :
    execlp("emacs", "emacs", fichier, NULL);
    break;
  case 3 :
    execlp("cat", "cat", fichier, NULL);
    break;
  case 4 :
    execlp("rm", "rm", "-i", fichier, NULL);
    break;
  }
  
  printf("Fin du programme\n");

  return 0;
}
