#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

/*
 * Exercice 2 TD 8
 * Question 1 : 
 * compiler normalement, donc sans les parties de code optionnel 
 */

void sig_handler(int sig) {
  printf("Signal recu: %d %s\n", sig, sys_signame[sig]);
}

int main(int argc, char *argv[]){
  int sig;

  printf("PID: %d\n", getpid());

#if (defined IGNORESIG)
  /* Question 2 : on ignore les signaux */
  for(sig = 1; sig < NSIG ; sig++){
    signal(sig, SIG_IGN);
  }
#elif (defined PRINTSIG)
  /* Question 3 : on écoute les signaux qu'on reçoit et
   *              on affiche le code et le nom du signal reçu 
   */
  for(sig = 1; sig < NSIG ; sig++){
    signal(sig, sig_handler);
  }
#endif
  
  while(1){
     sleep(5);
  }  /* Attendre des signaux   */
  return 0;
}
