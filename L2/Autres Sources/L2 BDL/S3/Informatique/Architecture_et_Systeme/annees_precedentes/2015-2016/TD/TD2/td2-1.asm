;;;  Archi & Sys
;;;  Licence 2 INFO - Univ Paris 13
;;;  Stefano Guerrini
;;;  AA 2015-16
;;; 
;;;  TD 2 - exercice 1
;;;  22/09/2015
	
	.ORIG x3000
	;;  ** point 1 **
	AND R0, R0, 0		; initialisation à 0 de R0, R0 := 0

	;;  ** point 2 **
	;; exemple : R1 := -15 constante de 5 bit
	AND R1, R1, 0		; on met à 0 le registre R1
	ADD R1, R1, -15		; R1 := -15
	;; attention au passage de -15 de 5 bit en complément à 2
	;; à 16 bit, extension du signe. on ajoute des 1 à gauche
	;; exemple : R2 := 12 constante de 5 bit
	;;  on utilise le fait que R0 == 0 (suite au point 1)
	ADD R2, R0, 7		; R2 := 7 car R0 == 0
	;; la constante de 16 doit être mémoriser dans une location
	;; à laquell on associe une étiquette
	;; par exemple, voir point suivant, R1 := NUM1
	
	;;  ** point 3 **
	LD R1, NUM1		; R1 := NUM1 constante à 16 bit
	LD R2, NUM2		; R2 := NUM2
	ADD R0, R1, R2		; R0 := R1 + R2
	ST R0, SUM		; SUM := R0 donc SUM = R1 + R2

	;;  ** point 4 **
	;; exemple : on vérifie si R1 <> 0 et on saute s'il est le cas
	ADD R1, R1, 0		; R1 := R1 + 0
	;; la valeur de R1 n'a pas été modifiée, mais attention aux bit N,Z,P
	;; après cette opération il correspondent à la valeur dans R1
	BRNP LABEL		; saut pour non-zéro
	;;  BRZ LABEL           ; pour sauter si zéro
	ADD R1, R1, 0		; juste pour faire quelque chose

	;;  ** point 5 et 6 **
	;; calcul de l'opposé de R0
LABEL:	NOT R0, R2		; négation bit a bit
	ADD R0, R0, 1		; ajoute 1 pour compléter le complément
	;; maintenant on a R0 = -R2
	ADD R0, R1, R0		; on complete le calcul de R0 := R1 - R2
	ST R0, DIFF		; DIFF := R1 - R2
	;; je met -R0 dans OPDIFF
	;; pour voir la valeur positive si le résultat était négatif
	NOT R0, R0
	ADD R0, R0, 1
	ST R0, OPDIFF
	HALT
NUM1:	.FILL x0004
NUM2:	.FILL x0005
SUM:	.BLKW 1
DIFF:	.BLKW 1
OPDIFF:	.BLKW 1
	.END
