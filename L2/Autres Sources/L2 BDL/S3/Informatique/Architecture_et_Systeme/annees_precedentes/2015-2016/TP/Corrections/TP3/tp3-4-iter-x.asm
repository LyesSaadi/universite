	;; tp3-3 6/10/2015

	.ORIG x3000
	LEA R6, pile
	
	LEA R1, s1
	LEA R2, s2
	ADD R6, R6, 2
	STR R3, R6, 0
	STR R4, R6, 1
	JSR strcmp
	LDR R3, R6, 0
	LDR R4, R6, 1
	ADD R6, R6, -2
	HALT


	;; routine pour la comparaison de deux chaines
	;; R1 et R2 sont les adresses des deux chaines
	;; à la fin on met le resultat dans R0
strcmp:	LDR R3, R1, 0
	LDR R4, R2, 0
	NOT R4, R4
	ADD R4, R4, 1
	ADD R0, R3, R4
	BRZ zero
	BRP pos
	LD R0, cmun
	RET
pos:	LD R0, cun
	RET
zero:	ADD R3, R3, 0
	BRNP next
	RET
next:	ADD R1, R1, 1
	ADD R2, R2, 1
	BR strcmp

cmun:	.FILL -1
cun:	.FILL 1
s1:	.STRINGZ "toto"
s2:	.STRINGZ "toso"
	.BLKW 128
pile:	
	.END
