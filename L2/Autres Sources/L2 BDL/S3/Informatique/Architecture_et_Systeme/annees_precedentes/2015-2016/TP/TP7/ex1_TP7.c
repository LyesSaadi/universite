#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

int main(int argc, char* argv[])
{
  int fd, choix;
  char nomFichier[100];

  if(argc == 1)
  {
    printf("Vous n'avez pas entré assez d'arguments\n");
    return EXIT_FAILURE;
  }
	strcpy(nomFichier,argv[1]);
	printf("Ouvrons le fichier de nom %s\n", nomFichier);
  if((fd=open(nomFichier, O_CREAT | O_RDWR | O_APPEND, 0600))==-1)
  {
    printf("Erreur à l'ouverture du fichier entré en paramètre\n");
    return EXIT_FAILURE;
  }
  do {
    printf("Que souhaitez vous faire ?\n");
    printf("> 0) Quitter le programme\n");
    printf("> 1) Éditer le fichier\n> 2) Afficher le fichier\n> 3) Supprimer le fichier\n");
    printf("Entrez le nombre correspondant à votre choix\n");
    scanf("%d", &choix);
  } while(choix < 0 || choix > 3);

  switch (choix) {
    case 0 :
      printf("Vous avez choisi de quitter le programme.\nAu revoir!\n");
      close(fd);
      return EXIT_SUCCESS;
      //break; pas nécessaire après return
    case 1 :
      printf("Vous avez choisi d'éditer le fichier\n");
      execlp("vi", "vi", nomFichier, (char*) NULL);

    case 2 :
      printf("Vous avez choisi d'afficher le fichier\n");
      execlp("cat", "cat", nomFichier, (char*) NULL);
    case 3 :
      printf("Vous avez choisi de supprimer le fichier\n");
      execlp("rm", "rm", nomFichier, (char*) NULL);

  }

  return EXIT_SUCCESS;
}
