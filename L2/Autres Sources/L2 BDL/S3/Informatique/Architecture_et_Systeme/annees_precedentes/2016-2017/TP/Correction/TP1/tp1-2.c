/*
 * Stefano Guerrini
 * AA 2015-16
 *  
 * TP 1 - exercice 2
 * 15/09/2015
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * On code la conversion décimale-binaire d'une valeur positive
 * @param k : nombre décimal positif à convertir en binaire
 * @param n : longueur du vecteur pour le résultat
 * @param bits : vecteur pour mémoriser la séquence de bits de
 *            la représentation binaire de k (au maximum n bits)
 * @result : 0 si la longueur de la représentation binaire de k <= n
 *            sinon le nombre correspondant au bits qui ne rentrent pas
 *            dans le vecteur. Pratiquement, est égal à la division
 *            euclidienne k/2^n
 */
int dec2bin(int k, unsigned n, unsigned char bits[]) {
  int i;
  for (i = 0; i < n && k != 0; i++, k /= 2)
    bits[i] = k % 2;
  return k;
}

int main(int argc, char *argv[]) {
  int n, k, h, i;
  unsigned char *bits;

  /* lecture des données */
  printf("Valeur k qu'on veut convertir en binaire :\n");
  scanf("%d", &k);
  printf("Nombre n de bits du résultat (2^n > k) :\n");
  do { 
    scanf("%d", &n);
    if (n < 0)
      printf("Entrer une valeur positive :\n");
  } while (n < 0);

  /* allocation du vecteur */
  bits = calloc(n, sizeof(int));
  /* conversion */
  h = dec2bin(k, n, bits);
  /* on vérifie que le nombre k n'était pas trop large */
  if (h != 0) {
    printf("Le nombre %d est trop large pour être représenté sur %d bit.\n",
	   k, n);
    printf("Il dépasse de (%d * 2^%d)\n", h, n);
    printf("Les %d bits plus faibles de la représentation binaire de %d sont : \n", n, k);
  } else {
    printf("La représentation binaire de %d sur %d bit est : \n", k, n);
  }

  /* on affiche les bit obtenus */
  for (i = n-1; i >= 0; i--)
    putchar('0'+bits[i]);
  putchar('\n');
}

