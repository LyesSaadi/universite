;;;  Archi & Sys
;;;  Licence 2 INFO - Univ Paris 13
;;;  Stefano Guerrini
;;;  AA 2015-16
;;; 
;;;  TD 2 - exercice 2, point 2
;;;  22/09/2015
	
	.ORIG x3000
	;; on initialise les registres
	;; par exemple : on prend les valeurs des locations NUM1, NUM2	
	LD R1, NUM1
	LD R2, NUM2
	;; on test R2 > R1, pour ça on test R2 - R1 > 0
	NOT R0, R1
	ADD R0, R0, 1	      	; R0 := -R1
	ADD R0, R2, R0		; R0 := R2 - R1
	BRP ELSE		; on saute si R0 > 0, et si donc R2 > R1
	;; branche then du test: R1 > R2, donc R1 est max
	ADD R0, R1, 0		; R0 := R1
	BRNZP ENDITE		; fin de la branche then du test
	                        ; on saute la branche else
	;; branche else: R2 <= R1, donc R2 est max
ELSE:	ADD R0, R2, 0		; R2 := R0
	;; on met resultat dans la location MAX
ENDITE:	ST R0, MAX		; MAX := R0
	HALT
NUM1:	.FILL x0004
NUM2:	.FILL x0005
MAX:	.BLKW 1
	.END
