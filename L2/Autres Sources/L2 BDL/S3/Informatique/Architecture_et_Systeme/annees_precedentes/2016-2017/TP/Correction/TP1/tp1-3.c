/*
 * Stefano Guerrini
 * AA 2015-16
 *  
 * TD 1 - exercice 2
 * 08/09/2015
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * Procédure pour la conversion décimale-binaire d'une valeur positive
 * @param k : nombre décimal positif à convertir en binaire
 * @param n : longueur du vecteur pour le résultat
 * @param bits : vecteur pour mémoriser la séquence de bits de
 *            la représentation binaire de k (au maximum n bits)
 * @result : 0 si la longueur de la représentation binaire de k <= n
 *            sinon le nombre correspondant au bits qui ne rentrent pas
 *            dans le vecteur. Pratiquement, est égal à la division
 *            euclidienne k/2^n
 */
int dec2bin(int k, unsigned n, unsigned char bits[]) {
  int i;
  for (i = 0; i < n && k != 0; i++, k /= 2)
    bits[i] = k % 2;
  return k;
}

/*
 * Procédure pour le calcul de l'opposite en complément à 2
 * @param n : longueur du vecteur avec la séquence de bits d'input
 * @param bits : vecteur avec la séquence de bits d'input
 * @result : rien
 */
void comp2(unsigned n, unsigned char bits[]) {
  int i;
  /* les bits jusqu'au premier 1 inclu ne sont pas modifiés */
  for (i = 0; i < n && bits[i] == 0; i++);
  /* les bits suivant sont complémentés :
   *   un bit 1 devient 0, et un bit 0 devient 1 */
  for (i++; i < n; i++)
    bits[i] = (bits[i]+1) % 2;
}

/*
 * Procédure pour l'affichage des bits dans un vecteur
 * @param n : longueur du vecteur avec la séquence de bits d'input
 * @param bits : vecteur avec la séquence de bits d'input
 * @result : rien
 */
void print_bits(unsigned n, unsigned char bits[]) {
  for (int i = n-1; i >= 0; i--)
    putchar('0'+bits[i]);
}

int main(int argc, char *argv[]) {
  int n, k, mk, h;
  unsigned char *bits;

  /* lecture des données */
  printf("Valeur k qu'on veut convertir en binaire complément à 2 :\n");
  scanf("%d", &k);
  /* on prend la valeur absolue de du nombre à convertir */ 
  if (k < 0)
    mk = -k;
  else
    mk = k;
  /* on demande la longueur du vecteur pour la séquence de bit
   * dans la notation en complément à 2, la valeur absolue doit
   * être convertie sur n-1 bit (donc mk < 2^(n-1))
   */
  printf("Nombre n de bits du résultat (2^(n-1) > %d) :\n", mk);
  do {
    scanf("%d", &n);
    if (n < 0)
      printf("Entrer une valeur positive :\n");
  } while (n < 0);

  /* allocation du vecteur */  
  bits = calloc(n, sizeof(int));
  /* conversion de la valeur absolue sur n-1 bit
   * vu qu'on a utilisé calloc, le bit plus fort dans le vecteur
   * (en position n-1) était est reste égal à 0
   */
  h = dec2bin(mk, n-1, bits);
  /* on vérifie que la valeur absolue n'était pas trop large */
  if (h != 0) {
    printf("Le nombre %d est trop large pour être représenté sur %d bit.\n",
	   k, n);
    printf("Son modulo dépasse de (%d * 2^%d)\n", h, n-1);
    printf("Les %d bits plus faibles de la représentation binaire de %d sont : \n", n-1, k);
  } else {
    printf("La représentation binaire complément à 2 de %d sur %d bit est : \n", k, n);
  }

  /* si la valeur k était négative on complément */ 
  if (k != mk)
    comp2(n, bits);

  /* on affiche le résultat */
  print_bits(n, bits);
  putchar('\n');
}
