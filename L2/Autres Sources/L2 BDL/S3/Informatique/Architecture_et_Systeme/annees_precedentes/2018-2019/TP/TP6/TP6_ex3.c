#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>


int CPT=7;
void sighand(int sig);
pid_t fils;

int main()
{
  signal(15, sighand);
  if((fils=fork())==0)
  {
    printf("%d\n", getpid());
    while(1)
    {
      if(CPT>0)
        printf("La commande `kill -15 %d` a été faite %d fois, encore %d fois\n", getpid(), 7-CPT, CPT);
      else if(CPT==0)
        printf("La commande `kill -15 %d` a été faite %d fois, au revoir \n", getpid(), 7-CPT);
      sleep(1);
    }
  }
  else
  {
    while(1)
    {
      if(CPT<=0)
        kill(fils, 9);
      kill(fils,15);
      sleep(1);
    }
  }
  wait(NULL) ;
  return EXIT_SUCCESS;
}

void sighand(int sig)
{
  if(sig==15 && CPT>=0)
  {
    CPT--;
  }
  else if(CPT<0)
  {
    kill(fils, 9);
    kill(getpid(), 9);
  }
}
