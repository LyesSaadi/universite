#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main()
{
	int i=0,x=0;
	pid_t pidfils = fork();
	printf("%d\n", pidfils);
	if(pidfils == 0)
	{
		x=1;
	}
	else
	{
		printf("Ici x=%d\n", x);
		//Quand on ajoute sleep, le processus du fils sera généralement éxécuté avant celui du perd
		//sleep(5);
		//De même pour cette boucle
/*		for(i=0;i<10000;i++)
		{
			i++;
			i--	;
		}
*/
	}
	printf("Là x=%d\n", x+1);
	exit(0);
}
