#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

void handler(int sig);

int main()
{
  for(int sig=0; sig<NSIG; sig++)
  {
    if(signal(sig, SIG_IGN)==SIG_ERR)
      printf("Le signal %d est non déroutable\n", sig);
    else
      printf("Le signal %d est déroutable\n", sig);
  }
  printf("pid : %d\n", getpid());
  while(1)
  {
    sleep(5);
  }
  return EXIT_SUCCESS;
}

void handler(int sig)
{
  printf("Reçu : %d\n", sig);
}
