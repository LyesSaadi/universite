.ORIG x3000
		LD R1, ch
		LEA R2, str

loop:		LDR R0, R2, 0
		BRz notFound
		NOT R0, R0
		ADD R0, R0, 1
		ADD R0, R1, R0
		BRz found
		ADD R2, R2, 1
		BR loop

notFound:	AND R0, R0, 0
		ADD R0, R0, -1
		BR end

found:		LEA R0, str
		NOT R0, R0
		ADD R0, R0, 1
		ADD R0, R0, R2
		BR end

end:		HALT

		ch : .FILL 111
		str : .STRINGZ "Hell World"
.END
