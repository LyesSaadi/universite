#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include<sys/wait.h>

#define BUFFER_SIZE 256

int main ()
{
  char fileName[BUFFER_SIZE];
  char buffer[BUFFER_SIZE];
  pid_t pid;
  int fd, nByte;
  printf("Entrez le nom du fichier que vous souhaitez créer\n> ");
  scanf("%s", fileName);
  if((pid=fork())==-1)
  {
    perror("fork");
    exit(1);
  }
  if (pid==0)
  {
    /*Fils*/
    if((fd=open(fileName, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR))==-1)
    {
      perror("fils open");
      exit(1);
    }
    dup2(fd, 1);
    execl("/bin/ls", "ls", "-l", (char*) NULL);
    perror("execl");
    exit(1);
  }
  /*Père*/
  wait(NULL);
  if((fd=open(fileName, O_RDONLY))==-1)
  {
    perror("père open");
    exit (1);
  }
  while((nByte = read(fd, buffer, BUFFER_SIZE))!=0)
    write(1, buffer, nByte);
  close(fd);
  return EXIT_SUCCESS;
}
