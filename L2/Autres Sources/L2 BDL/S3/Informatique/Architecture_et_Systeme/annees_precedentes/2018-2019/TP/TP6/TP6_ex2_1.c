#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int main()
{
  while(1)
  {
    printf("pid : %d\n", getpid());
    sleep(2);
  }
  return EXIT_SUCCESS;
}
