#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

/*
	@Author FLOQUET Nicolas		13/10/1997	11706348
	@Author CHANDARA Alexis		04/03/1999	11702812
*/

int main()
{
  pid_t F1, F2, premier, deuxieme;
  if((F1=fork())==-1)
  {
    printf("Erreur Fork 1\n");
    exit(1);
  }
  if(F1==0)
  { //Fils 1
    printf("Je suis le processus fils 1, avec %d. Mon père est le processus %d\n", getpid(), getppid());
    exit(0);
  }

  if((F2=fork())==-1)
  {
    printf("Erreur Fork 2\n");
    exit(1);
  }
  if(F2==0)
  { //Fils 2
    printf("Je suis le processus fils 2, avec %d. Mon père est le processus %d\n", getpid(), getppid());
    exit(0);
  }

  //Père
  premier = wait(NULL);
  deuxieme = wait(NULL);

  printf("Je suis le processus père de %d.\n", getpid());
  if(F1==premier)
  {
    printf("Premier fils à terminer (fils 1) : %d.\n", F1);
    printf("Deuxième fils à terminer  (fils 2) : %d.\n", F2);
  }
  else if(F1==deuxieme)
  {
    printf("Premier fils à terminer (fils 2) : %d.\n", F2);
    printf("Deuxième fils à terminer  (fils 1) : %d.\n", F1);
  }

  return EXIT_SUCCESS;
}
