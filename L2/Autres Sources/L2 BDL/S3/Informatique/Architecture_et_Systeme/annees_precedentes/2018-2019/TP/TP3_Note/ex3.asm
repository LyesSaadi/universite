; Floquet Nicolas 13/10/97 11706348
; Chandara Alexis 04/03/99 11702812


; Exercice 3 :

; Ce programme inverse un tableau donné.
; @param tab1 un tableau. 
; @return un tableau inversé à partir d'un tableau donné.



.ORIG x3000

	LD R0, taille			; R0 contient la taille du tableau.
	LEA R1, tab1			; R1 contient l'adresse du premier tableau.
	LEA R2, tab2			; R2 contient l'adresse du second tableau.
	ADD R1, R1, R0			; R1 contient l'adresse de fin du premier tableau.
	LDR R3, R1, 0			; R3 contient la valeur du dernier élément du tableau.
	ST R3, tab2			; Dans tab2, on place le dernier élément du premier tableau.
	
InverserElements:	
	ADD R0, R0, -1			; On décrémente la taille, car il nous reste n-1 éléments à ajouter.
	BRnz tableauFini		; Si la taille est négative, il y a eu un bug (si vous entrez une mauvaise valeur), si la taille est nulle, c'est qu'on a fini de copier tout le tableau donc on a fini d'inverser le tableau.
	ADD R1, R1, -1			; R1 contient maintenant l'adresse de l'élément précédent de tab1 (que l'on veut copier).
	ADD R2, R2, 1			; R2 contient l'adresse de l'élément suivant de tab2 (vers lequel on veut copier la valeur de R1).
	LDR R3, R1, 0			; R3 contient la valeur de l'élément à l'adresse R1.
	STR R3, R2, 0			; La valeur R3, va être stockée dans l'adresse R2 (le prochain élément à rajouter dans le tab2).
	
	BR InverserElements
		
tableauFini: 				; On a bien inversé le premier tableau dans le second.
		HALT				; Fin du programme.
	



taille	: .FILL 7			; Le tableau contient 5 éléments.
tab1 : .FILL x3050			; Adresse du tableau 1.
	.FILL 5					; Valeur du premier element du tableau.
	.FILL 4
	.FILL 1
	.FILL 1
	.FILL 3
	.FILL 2
	.FILL 1

tab2 : .BLKW 7				; On réserve assez de mémoire pour inverser notre premier tableau
	


.END
