#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

/*
	@Author FLOQUET Nicolas		13/10/1997	11706348
	@Author CHANDARA Alexis		04/03/1999	11702812
*/

void sig_handler(int sig);

int CPT = 0;

int main()
{
  printf("Mon PID est %d\n", getpid());
  signal(2, sig_handler);
  while(1)
  {
    while(CPT<=5)
    {
      sleep(10);
      printf("\nSIGINT reçu : %d fois\n", CPT);
      if(CPT==5)
        signal(2, SIG_DFL);
    }
  }

  return EXIT_SUCCESS;
}

void sig_handler(int sig)
{
  CPT++;
}
