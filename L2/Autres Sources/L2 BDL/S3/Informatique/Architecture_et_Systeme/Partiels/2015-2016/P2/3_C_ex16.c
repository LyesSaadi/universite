#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
  pid_t pid = fork();
  printf("hello + %d\n", getpid());
  if(pid == 0)
    printf("hello + %d\n", getpid());
  else
  {
    fork();
    printf("hello + %d\n", getpid());
  }
  return 0;
}

//  printf("hello + %d\n", getpid());
