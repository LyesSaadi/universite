#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
  pid_t fils1, fils2, petitfils;
  if((fils1 = fork()) != 0)
  { //Père
    if((fils2 = fork()) != 0)
    { //Père
      printf("Père : mon PID est %d et ceux de mes fils sont %d et %d\n", getpid(), fils1, fils2);
    }
    else
    { //Fils 2
      if((petitfils = fork()) != 0)
      { //Fils 2
        printf("Fils : mon PID est %d, celui de mon père est %d et celui de mon fils est %d\n", getpid(), getppid(), petitfils);
      }
      else
      { //Petit fils 2
        printf("Petit fils : mon PID est %d, celui de mon père est %d\n", getpid(), getppid());
      }
    }
  }
  else
  { //Fils1
    if((petitfils = fork()) != 0)
    { //Fils 1
      printf("Fils : mon PID est %d, celui de mon père est %d et celui de mon fils est %d\n", getpid(), getppid(), petitfils);
    }
    else
    { //Petit fils 1
      printf("Petit fils : mon PID est %d, celui de mon père est %d\n", getpid(), getppid());
    }
  }
  return 0;
}
