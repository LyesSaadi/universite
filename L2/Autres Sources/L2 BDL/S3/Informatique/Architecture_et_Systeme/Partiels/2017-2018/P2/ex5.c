#include<stdio.h>
#include<stdlib.h>
#include <unistd.h>

int main()
{
  int F1, F2, x;
  if((F1 = fork()) != 0)
  {
    //Père
    if((F2 = fork()) != 0)
    {
      //Père
      sleep(2);
      printf("Entrez un entier\n");
      scanf("%d", &x);
      sleep(2);
      printf("x * 2 = %d \t PID : %d\t PPID : %d\n", x*2, getpid(), getppid());
    }
    else
    {
      //Fils 2
      sleep(1);
      printf("Entrez un entier\n");
      scanf("%d", &x);
      sleep(1);
      printf("x * 2 = %d \t PID : %d\t PPID : %d\n", x*2, getpid(), getppid());
    }
  }
  else
  {
    //Fils 1
    printf("Entrez un entier\n");
    scanf("%d", &x);
    printf("x * 2 = %d \t PID : %d\t PPID : %d\n", x*2, getpid(), getppid());
  }

  exit(0);
}
