; Programme qui prend 2 entiers en paramètre, x et y et qui fait :
;	if (x > y)
;		x:= x-y
;	else
;		x:=y-x
;@param x un entier
;@param y un autre entier

.ORIG x3000
	LD R0, x		; R0 = x
	LD R1, y		; R1 = y
	NOT R1, R1		; R1 = non y
	ADD R1, R1, 1		; R1 = -y
	ADD R0, R0, R1		; R0 = x - y
	
	;Si R0 est négatif ou nul, alors x <= y, sinon x > y
	BRnz	xInfouEgaly	;
	; Si x > y alors on veut que x devienne x - y, hors R0 = x - y
	ST R0, x		; x := x - y
	BR end
	
xInfouEgaly:			; On veut x := y - x
	NOT R1, R1		; On refait le complément à deux de R1
	ADD R1, R1, 1		; R1 = y
	ADD R0, R0, R1		; R0 = x
	NOT R0, R0		; Complément à deux de x
	ADD R0, R0, 1		; R0 = -x
	ADD R0, R1, R0		; R0 = y - x
	ST R0, x		; x := y -x
	BR end
	
end:	HALT

x : .FILL 3
y : .FILL 8


.END
