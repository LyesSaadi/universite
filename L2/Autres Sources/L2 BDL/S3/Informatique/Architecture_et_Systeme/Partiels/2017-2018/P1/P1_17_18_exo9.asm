; Programme qui compte le nombre d'occurences d'un caractère dans une chaîne de caractères
; Le caractère est stocké dans R0, l'adresse de la chaîne dans R1, et le résultat dans R3
; Prend en paramètre une chaîne de caractères et un caractère
; @param STR, une chaîne de caractères
; @param char, un caractère
; @return un entier dans R3 qui est le nombre d'occurences du caractère char dans la chaîne STR

.ORIG x3000
		LD R0, char				; Le caractère est stocké dans R0
		LEA R1, STR				; L'adresse de la chaîne est stockée dans R1
		AND R3, R3, 0				; Le compteur du résultat est initialisé à 0
parcours:	LDR R2, R1, 0				; La valeur à l'adresse stockée dans R1 (le premier char de la chaîne) est stockée dans R2
		BRz	chaineFinie			; Si le caractère vaut 0, alors on a atteint le caractère de fin de chaîne
		ADD R1, R1, 1				; Le prochain caractère que l'on va chercher sera celui après R1
		NOT R2, R2
		ADD R2, R2, 1				; Complément à 2 de la valeur du caractère
		ADD R2, R2, R0				; On ajoute la valeur de char au complément à deux du caractère courant de la chaîne, si le résultat vaut 0, alors les caractères sont les même
		BRnp parcours				; Si les caractères n'étaient pas les même, on a rien à faire, donc on continue à parcourir la chaîne
		ADD R3, R3, 1				; S'ils étaient égaux, alors on incrémente R3
		BR parcours				; On continue à parcourir la chaîne
		
		
		

chaineFinie : NOP					; Nombre d'occurences dans R3

STR : .STRINGZ "ttt"					; La chaîne qu'on va analyser
char: .FILL 116						; Le caractère dont on veut connaître le nombre d'occurences dans la chaîne STR

.END
