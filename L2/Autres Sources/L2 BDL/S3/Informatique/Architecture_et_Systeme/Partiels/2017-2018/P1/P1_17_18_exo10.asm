; Ce programme calcule le max de 2 entiers dans une pile passée en paramètres
; @return Le plus grand des 2 entiers dans le registre R0

.ORIG x3000
	AND R0, R0, 0			; Résultat initialisé à 0
	JSR max
	BR end
	
	; Sous routine qui prend en paramètre l'adresse de la pile est passée dans R1
	; Calcule le max entre 2 valeurs entières stockées dans une pile
	; @return, le max entre les 2 entiers de la pile
	
	max:
	LEA R6, finPile			; L'adresse de la pile est passée dans R6
	LDR R2, R6, -1			; R2 vaut la valeur du premier élement de la pile
	LDR R3, R6, -2			; R3 vaut la valeur du premier élement de la pile
	
	NOT R3, R3
	ADD R3, R3, 1			; R3 = - 2ème élément de la pile
	ADD R2, R2, R3			;
	BRn secondSupPremier
	LDR R0, R6, -1
	RET
	
	secondSupPremier:
		LDR R0, R6, -2
		RET

end:	HALT




taille : .FILL 2
	.FILL 5
	.FILL 18
finPile :.FILL x3050
.END
