(* Ceci est un éditeur pour OCaml
   Entrez votre programme ici, et envoyez-le au toplevel en utilisant le
   bouton "Évaluer le code" ci-dessous. *)

 (*Définir une fonction inserer qui insère à la bonne place un élément donné dans
      une liste croissante donnée*)
let rec inserer elem liste = match liste with 
  |[] -> [elem]               
  |tete::queue -> if elem <= tete
      then elem::liste 
      else
        tete ::(inserer elem queue);;

(*En déduire une fonction tri_insertion qui utilise 
la fonction précédente et qui
trie selon l’ordre croissant une liste donnée 
    en utilisant la méthode du tri par
insertion*)
let rec tri_insertion liste = match liste with 
  | [] -> []
  |tete::queue ->
      inserer tete (tri_insertion queue)
;;
(*Définir une fonction decouper qui prend en
  entrée un élément donné e et une
liste donnée l et qui 
renvoie en sortie le couple formé de la liste des éléments de l
qui sont plus petits que e et de la liste
des éléments de l qui sont plus grands que e.*)
let couple_de_liste = ([], [])
  
  
 
let rec decouper_pas_efficace elem liste  = match liste with 
  | [] -> ([], [])
  | tete::queue -> if tete <= elem 
      then (tete:: (fst(decouper_pas_efficace elem queue)),
            tete::(snd(decouper_pas_efficace elem queue)))
      else (fst(decouper_pas_efficace elem queue),
            tete::(snd(decouper_pas_efficace elem queue)))
;;
                  
                  
            
            
            
let rec decouper elem liste = match liste with 
  | [] -> ([], [])
  |tete::queue -> 
      let decoupage_queue = decouper elem queue in 
      if tete <= elem 
      then (tete::(fst decoupage_queue), snd decoupage_queue)
      else (fst decoupage_queue, tete::(snd decoupage_queue));;
                                  
let rec concatenation liste1 liste2 = match liste1 with 
  |[] -> liste2
  |tete1::queue1 -> tete1 :: (concatenation queue1 liste2)
                             
                             
let rec tri_rapide liste = match liste with 
  |[] -> []
  |tete::queue ->
      let decoupage = decouper tete queue in 
      let moitie_gauche_triee = tri_rapide (fst decoupage) in 
      let moitie_droite_triee = tri_rapide (snd decoupage) in 
      concatenation moitie_gauche_triee (tete moitie_droite_triee)
        
        

let rec decouper_bis liste = match liste with 
  |[] -> ([],[])
  |tete::queue -> 
      let decoupage_queue = decouper_bis queue in 
      (tete::(snd decoupage_queue), fst decoupage_queue)
      
let rec fusion liste1 liste2 =
  match (liste1, liste2) with 
  |([], []) -> []
  |(tete1::queue1, [])-> liste1
  |([], tete2::queue2)-> liste2
  |(tete1::queue1, tete2::queue2)->
      if tete1 <= tete2 
      then 
        tete1::(fusion queue1 liste2)
      else
        tete2::(fusion liste1 queue2)
;;
      








