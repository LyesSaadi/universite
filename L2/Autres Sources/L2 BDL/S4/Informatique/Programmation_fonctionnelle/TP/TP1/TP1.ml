(*
	#use "TP1.ml" ;;
	dans l'interpréteur
*)

(*Exercice 1*)
(*1*)

let rec fact n =
if(n<=2)
then
	n
else
	n * fact (n-1);;

fact (-5);;
fact 5;;
fact 10;;
fact 4;;

(*2*)
let rec fact_acc n acc = 
	if(n<=0)
	then
		acc
	else
		fact_acc (n-1) (n*acc);;
	
fact_acc (-5) 1;;
fact_acc 5 1;;
fact_acc 10 1;;
fact_acc 4 1;;



(*3*)

let rec append l1 l2 = 
	match (l1, l2) with
	|([], _) -> l2
	|(_, []) -> l1
	|(t1::q1, l2) -> t1 :: append q1 l2;;
	
let azerty = ['a';'z';'e';'r';'t';'y';'u';'i';'o';'p'];;
let qsdf = ['q';'s';'d';'f';'g';'h';'j';'k';'l';'m'];;

azerty;;
append azerty qsdf;;

(*4*)

let rec rev l = 
	match l with
	|[] -> l
	|t1::q1 -> append (rev q1) (t1 :: []);;

rev azerty;;

(*5*)

let rec rev_acc l1 accl = 
	match l1 with
	|[] -> accl
	|t1::q1 -> rev_acc q1 (append (t1::[]) accl);;

rev_acc azerty [];;


(*________________________________________*)


(*Exercice 2*)

let listInt = [1;2;3;4;5;6;7;8;9;10;11;12];;
let listDizaine = [10;20;30;40;50;60;70;80;90];;
let listPair = [2;4;6;8;10];;
let smallList = [1;2;3;4;5];;



(*1*)

(*i*)

let rec inter l1 l2 = 
	match(l1, l2) with
	|([], l2) -> []
	|(l1, []) -> []
	|(t1::q1, t2::q2) -> 
	if(t1=t2)
	then
		t1 :: inter q1 q2
	else
		if(t1 < t2)
		then
			inter q1 l2
		else
			inter l1 q2;;

inter smallList listInt;;
inter listDizaine listInt;;
inter listPair smallList;;
inter listPair listInt;;
inter listInt listInt;;
inter [1;4;7] [2;4;8];;

(*ii*)

let rec union l1 l2 = 
	match(l1, l2) with
	|([], l2) -> l2
	|(l1, []) -> l1
	|(t1::q1, t2::q2) -> 
	if(t1=t2)
	then
		t1 :: union q1 q2
	else
		if(t1 < t2)
		then
			t1 :: union q1 l2
		else
			t2 :: union l1 q2;;


union smallList listInt;;
union listDizaine listInt;;
union listPair smallList;;
union listPair listInt;;
union listInt listInt;;
union [1;4;7] [2;4;8];;



(*2*)

(*i*)

let rec appartient e1 l1 = 
	match l1 with
	|[] -> false
	|e::q1 -> (e=e1) || appartient e1 q1;;

appartient 10 listDizaine;;
appartient 15 listDizaine;;
appartient 20 listDizaine;;


(*ii*)

let rec inter_2 l1 l2 = 
	match(l1, l2) with
	|([], l2) -> []
	|(l1, []) -> []
	|(t1::q1, t2::q2) -> 
	if(appartient t1 l2)
	then
		if(appartient t1 q1)
		then	
				inter_2 q1 l2
		else
				t1 :: inter_2 q1 l2
	else
		inter_2 q1 l2;;

inter_2 smallList listInt;;
inter_2 listDizaine listInt;;
inter_2 listPair smallList;;
inter_2 listPair listInt;;
inter_2 listInt listInt;;
inter_2 [1;4;7] [2;4;8];;


(*iii*)

let rec union_2 l1 l2 = 
	match(l1, l2) with
	|([], l2) -> l2
	|(l1, []) -> l1
	|(t1::q1, t2::q2) -> 
	if(appartient t1 q1)
	then	
			union_2 q1 l2
	else
		if(not (appartient t1 l2))
		then
			t1 :: union_2 q1 l2
		else
			union_2 q1 l2;;

union_2 smallList listInt;;
union_2 listDizaine listInt;;
union_2 listPair smallList;;
union_2 listPair listInt;;
union_2 listInt listInt;;
union_2 [1;4;7] [2;4;8];;

