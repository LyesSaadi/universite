/*HAMES OUEZNA 11610524*/
/*HENOUNE ASMA  11606588*/
/*TP 10*/

#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct rec_t {
    char name[20];
    char surname[20];
    int age;
} rec_t;

int rec_count(int fd) {
  return (lseek(fd, 0, SEEK_END) / sizeof(rec_t));
}

int rec_read(int fd, int n, rec_t *pr) {
  lseek(fd, n*sizeof(rec_t), SEEK_SET);
  if ((read(fd, pr, sizeof(rec_t)) == -1))
  {  return 0;}
  else { return 1;}
}

int rec_write(int fd, int n, rec_t *pr) {
  lseek(fd, n*sizeof(rec_t), SEEK_SET);
  if ((write(fd, pr, sizeof(rec_t)) == -1)){
  	return 0;
  } else {
  	return 1;}
}

int main(int argc, char * argv[]) {
  int fd, go = 1;
  char fname[256];
  unsigned a;
  
  do {
    printf("Fichier des enregistrements [256 chars max] : ");
    scanf("%s", fname);

    if ((fd = open(fname, O_RDWR | O_CREAT, 0660)) == -1)
      printf("Impossible d'ouvrir le fichier %s\n", fname);
  } while (fd == -1);

  while(go) {
    printf(">>> Choisir une axion\n    [0: exit | 1: read | 2: write | 3: count ] : ");
    scanf("%u", &a);
    switch(a) {
    case 0: {
      go = 0;
      printf("Au revoir!.\n\n");
      break;
    }
    case 1: {
      int n, nrec = rec_count(fd);
      rec_t rec;
      if (nrec == 0) {
	printf("Le fichier est vide.\n\n");
	break;
      }
      printf("Num d'enregistrement à lire [0..%d] : ", nrec-1);
      scanf("%d", &n);
      if (n < 0 || n >= nrec) {
	printf("Num d'enregistrement non valid.\n\n");
	break;
      }
      rec_read(fd, n, &rec);
      printf("%s %s %d\n\n", rec.name, rec.surname, rec.age);
      break;
    }
    case 2: {
      int n, nrec = rec_count(fd);
      rec_t rec;
      printf("Num d'enregistrement à écrire [0..%d] : ", nrec);
      scanf("%d", &n);
      if (n < 0 || n > nrec) {
	printf("Num d'enregistrement non valid.\n\n");
	break;
      }
      printf("Prénom : ");
      scanf("%s", rec.name);
      printf("Nom : ");
      scanf("%s", rec.surname);
      printf("Age : ");
      scanf("%d", &rec.age);
      rec_write(fd, n, &rec);
      printf("%s %s %d\n\n", rec.name, rec.surname, rec.age);
      break;
    }
    case 3: {
      int nrec = rec_count(fd);
      printf("Le fichier contient %d enregistrements.\n\n", nrec);
      break;
    }
    default:
      printf("%d n'est une action connue.\n", a);
    }
  }
  
  return EXIT_SUCCESS;
}

