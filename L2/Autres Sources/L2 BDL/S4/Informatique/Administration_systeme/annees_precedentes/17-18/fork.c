#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
int main(){

	pid_t pid =fork();
	printf("je suis :%d\n",pid);
	if(pid < 0){
		perror("Fork failled");
	}
	
	if(pid == 0){
		printf("Je suis le fils et mon pid est: %d\n",(int) getpid());
		sleep(5);
		printf("le fils endormis ..\n");
		exit(0);
	}
	
	
		printf("Je suis le parent et mon pid est: %d\n",(int) getppid());
		wait(NULL);
	return 0;
	
}
