#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

stat* tabStat = (stat*) malloc(sizeof(stat));

void* routineT1(void *);

void* routineT2(void *);

int main(int argc, char* argv[])
{
  pthread_t t1[argc], t2[argc];
  int i;
  for(i = 1; i<argc; i++)
  {
    if((stat(argv[i], tabStat)) == -1) {perror("stat"); exit(EXIT_FAILURE); }
    if(pthread_create(t1[i], NULL, routineT1, (void*) tabStat)) {perror("pthread_create");}
  }
  return  EXIT_SUCCESS;
}


void* routineT1(void* arg)
{
  printf("size = %d\n", (* tabStat)arg->st_size);
  pthread_exit(0);
}
