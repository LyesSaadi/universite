#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "message.h"

void erreur(char *message_erreur) 
{
	printf(message_erreur) ;
	exit(1) ;
}

// Client


#define BUF_SIZE 256
#define MSG_SIZE 16

int main(int argc, char *argv[])
{
	int sock, ret, taille;
	unsigned int len;
	struct sockaddr_in serveur_adr;
	struct sockaddr_in moi;
	char input[MSG_SIZE + 1];
	char buffer [BUF_SIZE + 1];
	char buffer2 [BUF_SIZE + 1];
	message messageTmp;
	
	if (argc  != 2) 
	{
		fprintf(stderr,"usage : %s <@IP_Serveur>\n", argv[0]);
		exit(0);
	}
	
	sock = socket(AF_INET, SOCK_STREAM, 0) ;
	
	if (sock < 0) erreur("Erreur de creation de la socket") ;
	
	serveur_adr.sin_family = AF_INET ;
	serveur_adr.sin_port = htons(5000) ;
	serveur_adr.sin_addr.s_addr = inet_addr(argv[1]) ;
	bzero(&(serveur_adr.sin_zero),8) ;
	len = sizeof(moi) ;
	if(connect(sock, (struct sockaddr_in *) &moi, sizeof(serveur_adr)) < 0)
	{
		erreur("bind client");
	}
	
	getsockname(sock, (struct sockaddr *)&moi, (socklen_t*) &len);

	do{
			if((ret = read(sock, buffer, BUF_SIZE) <= 0) erreur("erreur read 1");
		
			buffer[ret] = '\0';
			printf("%s", buffer);
			scanf("%s", input);
		
			if((write(sock, input, MSG_SIZE) == -1)
				erreur("Erreur sendto input");
		
		
			if(strcmp(input, "AJOUT") == 0)
			{
				printf("\nVous avez choisi ajout\n");
				ret = read(sock, buffer, BUF_SIZE); 
				if(ret <= 0)
					erreur("Erreur recvfrom AJOUT");
			
				buffer[ret] = '\0';
				printf("%s", buffer);
				scanf("%s", buffer2);
				if((write(sock, buffer2, strlen(buffer2) + 1)) == -1)
					erreur("Erreur sendto message ajout");
			}
			if(strcmp(input, "LISTER") == 0)
			{
				if(read(sock, &taille, sizeof(int)) <= 0)
					erreur("Erreur recvfrom taille");
				for(int i = 0; i < taille; i++)
				{
					read(sock, &messageTmp, sizeof(message));
					printf("|\tMessage %d \t| \n\t%s\t\n\n", messageTmp.id, messageTmp.texte);
				}
			}
		} while ((strcmp(input, "q") != 0) && strcmp(input, "Q") != 0);
		close(sock);
		exit(0) ;

}











