#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "message.h"

void erreur(char *message_erreur) 
{
	printf(message_erreur) ;
	exit(1) ;
}

// Client

int main(int argc, char *argv[])
{
	int sock, ret, identifiant, taille;
	unsigned int len;
	struct sockaddr_in serveur_adr ;
	char input[10];
	char buffer [201];
	char buffer2 [201];
	message messageTmp;
	
	if (argc  != 2) 
	{
		fprintf(stderr,"usage : %s <@IP_Serveur>\n", argv[0]) ;
		exit(0) ;
	}
	
	sock = socket(AF_INET, SOCK_DGRAM, 0) ;
	
	if (sock < 0) erreur("Erreur de creation de la socket") ;
	
	serveur_adr.sin_family = AF_INET ;
	serveur_adr.sin_port = htons(5000) ;
	serveur_adr.sin_addr.s_addr = inet_addr(argv[1]) ;
	bzero(&(serveur_adr.sin_zero),8) ;
	len = sizeof(struct sockaddr_in) ;
	
	if((sendto(sock, "Bonjour", 10, 0, (struct sockaddr *)&serveur_adr,sizeof(serveur_adr))) == -1) 
		erreur("Erreur sendto connexion");
		

	do{
		if((ret = recvfrom(sock, buffer, 200, 0, (struct sockaddr *)&serveur_adr, &len)) <= 0) erreur("erreur recvfrom 1");
		
		buffer[ret] = '\0';
		printf("%s", buffer);
		scanf("%s", input);
		
		if((sendto(sock, input, 10, 0, (struct sockaddr *)&serveur_adr,sizeof(serveur_adr))) == -1)
			erreur("Erreur sendto input");
		
		if(strncmp(input, "SUPPRIMER", strlen("SUPPRIMER")) == 0)
		{
			ret = recvfrom(sock, buffer, 200, 0, (struct sockaddr *)&serveur_adr, &len);
			if(ret  <= 0)
				erreur("Erreur recvfrom SUPPRIMER");
			
			buffer[ret] = '\0';
			printf("%s", buffer);
			scanf("%d", &identifiant);
			
			if((sendto(sock, &identifiant, sizeof(int), 0, (struct sockaddr *)&serveur_adr,sizeof(serveur_adr))) == -1)
				erreur("Erreur sendto identifiant");
		}
		if(strncmp(input, "AJOUT", strlen("AJOUT")) == 0)
		{
			printf("\nVous avez choisi ajout\n");
			ret = recvfrom(sock, buffer, 200, 0, (struct sockaddr *)&serveur_adr, &len); 
			if(ret <= 0)
				erreur("Erreur recvfrom AJOUT");
			
			buffer[ret] = '\0';
			printf("%s", buffer);
			scanf("%s", buffer2);
			if((sendto(sock, buffer2, strlen(buffer2) + 1, 0, (struct sockaddr *)&serveur_adr,sizeof(serveur_adr))) == -1)
				erreur("Erreur sendto message ajout");
		}
		if(strncmp(input, "LISTER", strlen("LISTER")) == 0)
		{
			if(recvfrom(sock, &taille, sizeof(int), 0, (struct sockaddr *)&serveur_adr, &len) <= 0)
				erreur("Erreur recvfrom taille");
			for(int i = 0; i < taille; i++)
			{
				recvfrom(sock, &messageTmp, sizeof(message), 0, (struct sockaddr *)&serveur_adr, &len);
				printf("|\tMessage %d \t| \n\t%s\t\n\n", messageTmp.id, messageTmp.texte);
			}
		}
	
	}while ((strcmp(input, "q") != 0) && strcmp(input, "Q") != 0);
	exit(0) ;

}











