#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<unistd.h>
#include<pthread.h>

#define N 5

pthread_t pthread_id[N][N];

void generation_aleatoire(int matrice[N][N]);

void imprime(int matrice[N][N]);

void* somme(void* arg);

int main()
{
  int MatA[N][N];
  int MatB[N][N];
  int MatC[N][N];   //Matrice Résultat
  int i, j, **arg;
  srand(time(NULL));
  generation_aleatoire(MatA);
  generation_aleatoire(MatB);
  imprime(MatA);
  printf("\n\t+\n");
  imprime(MatB);
  printf("\n\t=\n");
  for(i = 0; i < N; i++)
  {
    for(j = 0; j < N; j++)
    {
      arg = (int**) malloc(sizeof(int*) * 3);
      arg[0] = &(MatC[i][j]);
      arg[1] = &(MatA[i][j]);
      arg[2] = &(MatB[i][j]);
      if(pthread_create(&pthread_id[i][j], NULL, somme, (void*) arg) == -1)
      {
        printf("Erreur à la création du thread (%d, %d)\n", i, j);
        exit(1);
      }
    }
  }
  sleep(1);
  imprime(MatC);
  exit(0);
}

//____________________________________________________

void generation_aleatoire(int matrice[N][N])
{
  int i, j;
  for(i = 0; i < N; i++)
  {
    for(j = 0; j < N; j++)
      matrice[i][j] = rand()%100;
  }
}

//____________________________________________________

void imprime(int matrice[N][N])
{
  int i = 0, j = 0;
  for(i = 0; i < N-1; i++)
  {
    j=0;
    printf("[ %3d | ", matrice[i][j]);
    for(j = 1; j < N-1; j++)
    {
      printf("%3d | ", matrice[i][j]);
    }
    printf(" %3d ]\n", matrice[i][j]);
  }
}

//____________________________________________________

void* somme(void* arg)
{
  *((int **)arg)[0] = *((int **)arg)[1] + *((int **)arg)[2];
  return arg;
}
