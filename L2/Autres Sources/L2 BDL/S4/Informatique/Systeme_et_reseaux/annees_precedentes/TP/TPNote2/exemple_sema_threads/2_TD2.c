#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<unistd.h>
#include<string.h>
#include<pthread.h>
#include<dirent.h>
#include<sys/types.h>
#include<sys/syscall.h>
#include<linux/unistd.h>
#include <errno.h>


void* mon_ls(void* dir);

int main(int argc, char* argv[])
{
  int i;
  char *s;
  pthread_t *pthread_id;
  pthread_id = (pthread_t *) malloc (argc * sizeof(pthread_id));
  for(i = 1; i < argc; i++)
  {
    s = (char *) malloc (strlen(argv[i]) * sizeof(char) + 1);
    strcpy(s, argv[i]);
    if(pthread_create( &pthread_id[i], NULL, mon_ls, (void *) s) == -1)
      fprintf(stderr, " Erreur de creation du pthread numero %d", i);
  }
  sleep(1);
  return EXIT_SUCCESS;

}

void* mon_ls(void* dir)
{
  DIR *dirp;
  struct dirent *dp;

  printf("Répertoire: %s\n", (char *) dir);

  dirp = opendir((char *) dir);
  if (dirp == NULL)
  {
    perror("Erreur ouverture répertoire");
    exit (1);
  }
  do
  {
    errno = 0;
    if ((dp = readdir(dirp)) != NULL)
    {
      printf("Dans repertoire %s => %s %d\n", (char *)dir, dp->d_name, dp->d_type);
    }
  } while (dp != NULL);


  if (errno != 0)
    perror("error reading directory");
  closedir(dir);
  return dir;
}
