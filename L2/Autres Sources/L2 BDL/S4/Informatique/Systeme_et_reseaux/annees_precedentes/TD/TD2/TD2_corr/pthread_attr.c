#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/syscall.h>
#include<sys/types.h>
#include<linux/unistd.h>
#include<bits/posix1_lim.h>

pthread_t pthread_id[3];

int gettid() {
	return syscall(SYS_gettid);
}

void getparam(const pthread_attr_t *tattr)
{
	struct sched_param param;
	int detachstate, stacksize, policy;
	pthread_attr_getdetachstate(tattr, &detachstate);
	pthread_attr_getstacksize(tattr, &stacksize);
	pthread_attr_getschedpolicy(tattr, &policy);
	pthread_attr_getschedparam(tattr, &param);

	printf("pthread %d:\n \t etat de detachement %d,\n \t taille de pile %d,\n \t politique d'ordonnancement %d,\n \t priorite %d\n\n",
		(int) gettid(), detachstate, stacksize, policy, param.sched_priority);
}

void setparam(pthread_attr_t *tattr)
{
	struct sched_param param;
	int detachstate, stacksize, policy;
	pthread_attr_setdetachstate(tattr, PTHREAD_CREATE_DETACHED);
	pthread_attr_setstacksize(tattr, PTHREAD_STACK_MIN + 0x4000);
	pthread_attr_setschedpolicy(tattr, SCHED_FIFO);
	param.sched_priority = 10;
	pthread_attr_setschedparam(tattr, &param);
}

void* f(void* i)
{
	printf("pthread numero %d avec pid %d, tid %d, pthread_t %lu\n", 
		*((int *)i), (int) getpid(), (int) gettid(), (unsigned long int) pthread_self());
}

int main()
{
	pthread_attr_t tattr;
	int i,ret;
	for(i = 0; i < 3; i++)
	{
		ret = pthread_attr_init(&tattr);
		if (i==0) setparam(&tattr);
		getparam(&tattr);
		if( pthread_create( &pthread_id[i], &tattr, f, &i) == -1)
		{
			fprintf(stderr, " Erreur de creation du pthread numero %d", i);
		}
	}
	printf("pthread initial du processus avec pid %d, tid %d, pthread_t %lu\n", 
		(int) getpid(), (int) gettid(), (unsigned long int) pthread_self());
	sleep(1);
	return EXIT_SUCCESS;
}
