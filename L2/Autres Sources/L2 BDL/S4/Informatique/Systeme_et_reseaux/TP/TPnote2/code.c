#include <string.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct Annonce {
	char *adresse;
	char *texte;
	struct Annonce *suivant;
} Annonce_t;

Annonce_t *liste_annonces;
int ajout(char *adresse, char *texte);
int suppression(Annonce_t *annonce);
char *lister();
size_t longueur();

int ajout(char *adresse, char *texte) {
	Annonce_t *annonce;
	if ((annonce = (Annonce_t *) malloc (sizeof(Annonce_t))) == NULL) return -1;
	annonce->adresse = adresse;
	annonce->texte = texte;
	annonce->suivant = liste_annonces;
	liste_annonces = annonce;
	return 0;
}

int suppression(Annonce_t *annonce) {
	if (annonce == NULL) return -1;
	if (liste_annonces == NULL) return -2;
	Annonce_t *tmp = liste_annonces;
	Annonce_t *tmp_pred;
	if (strcmp(tmp->adresse,annonce->adresse) == 0 && strcmp(tmp->texte,annonce->texte) == 0) {
		liste_annonces = liste_annonces->suivant;
		return 0;
	}
	tmp_pred = tmp;
	tmp = tmp->suivant;
	while (tmp != NULL) {
		if (strcmp(tmp->adresse,annonce->adresse) == 0 && strcmp(tmp->texte,annonce->texte) == 0) {
			tmp_pred->suivant = tmp->suivant;
			return 0;
		}
		tmp_pred = tmp;
		tmp = tmp->suivant;
	}
	return -2;
}

char *lister() {
	char *s;
	Annonce_t *tmp = liste_annonces;
	if ((s = (char *) malloc (longueur() * sizeof(char) + 1)) == NULL) return NULL;
	while (tmp != NULL) {
		strcat(s,tmp->adresse);
		strcat(s,"\t");
		strcat(s,tmp->texte);
		strcat(s,"\n");
		tmp = tmp->suivant;
	}
	return s;
}

size_t longueur() {
	size_t l = 0;
	Annonce_t *tmp = liste_annonces;
	while (tmp != NULL) {
		l += strlen(tmp->adresse) + 1 + strlen(tmp->texte) + 1;
		tmp = tmp->suivant;
	}
	return l;
}


