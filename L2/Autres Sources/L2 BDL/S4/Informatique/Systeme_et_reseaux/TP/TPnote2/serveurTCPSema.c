/* serveur_TCP.c (serveur TCP) */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define BUF_SIZE 256

#define NB_CLIENTS_MAX 1024

typedef struct Annonce {
	char *adresse;
	char *texte;
	struct Annonce *suivant;
} Annonce_t;

Annonce_t *liste_annonces;


pthread_t pthread_id[NB_CLIENTS_MAX];

typedef struct
{
	sem_t libre;
	int num_libre;
} ControleAccesClient;

ControleAccesClient controle;

void initialisecontrole (ControleAccesClient* controle)
{
	sem_init (&controle->libre, 0, 2);
	controle->num_libre = 1;
}

int ajout(char *adresse, char *texte);
int suppression(Annonce_t *annonce);
char *lister();
size_t longueur();
void* attenteConnexion(void* arg);
void* gestionConnexion(void* arg);



int sock = 20000; /* socket de communication */

int main(int argc, char** argv) {
	struct sockaddr_in serveur; /* SAP du serveur */
	int i_eme_client = 0;
	initialisecontrole(&controle);

	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		fprintf(stderr,"%s: socket %s\n", argv[0],strerror(errno));
		exit(1);
	}
	serveur.sin_family = AF_INET ;
	serveur.sin_port = htons(20000) ;
	serveur.sin_addr.s_addr = htonl(INADDR_ANY) ;
	bzero(&(serveur.sin_zero),8) ;

	if (bind(sock, (struct sockaddr *) &serveur,sizeof(serveur)) < 0) {
		fprintf(stderr,"%s: bind %s\n", argv[0],strerror(errno));
		exit(1);
	}
	if (listen(sock, 5) != 0) {
		fprintf(stderr,"%s: listen %s\n", argv[0],strerror(errno));
		exit(1);
	}
	printf("Serveur démarré, mon adresse est : %s\n", inet_ntoa(serveur.sin_addr));

	if(pthread_create(&pthread_id[i_eme_client], NULL, attenteConnexion, NULL))
	{
		printf("erreur pthread_create attenteConnexion");
		exit(1);
	}
	pthread_exit(0);
}

void* attenteConnexion(void* arg)
{
	int i_eme_client = 0;
	unsigned int len = sizeof(struct sockaddr_in);
	while(1)
	{
		struct sockaddr_in client; /* SAP du client */
		int sock_pipe = accept(sock, (struct sockaddr *) &client, &len); /* socket de dialogue */
		if(pthread_create(&pthread_id[i_eme_client], NULL, gestionConnexion, (void*) sock_pipe))
		{
			printf("erreur pthread_create gestionConnexion");
			exit(1);
		}
	}
}


void* gestionConnexion(void* arg)
{
	char msg[BUF_SIZE] = "Veuillez entrez un des choix suivants :\n- AJOUT <adresse_mail> <texte>\n- LISTER\n- SUPPRIMER <adresse_mail> <texte>\n";
	char buffer[BUF_SIZE];		// commande et messages
	char buffer2[BUF_SIZE];		// mail
	char buffer3[BUF_SIZE];		// texte
	int tailleCommandeEtAddresse, retVal, tailleALire, tailleAEcrire, i, charLu;
	int sock_pipe = (int) arg;
	while(1)
	{
		bzero(buffer, BUF_SIZE);
		bzero(buffer2, BUF_SIZE);
		bzero(buffer3, BUF_SIZE);
		tailleAEcrire = strlen(msg);
    if((write(sock_pipe, &tailleAEcrire, sizeof(int))) < 0)
    {
    	printf("erreur Write Taille 1\n");
    	exit(1);
    }
    if((write(sock_pipe, msg, tailleAEcrire)) < 0)
    {
    	printf("erreur Write\n");
    	exit(1);
    }

    if((charLu = read(sock_pipe, &tailleALire, sizeof(int))) < 0)
    {
    	printf("erreur Read Taille 1\n");
    	exit(1);
    }
    if((charLu = read(sock_pipe, buffer, tailleALire)) < 0)
    {
    	printf("erreur Read\n");
    	exit(1);
    }
		buffer[charLu] = 0;

    if(strncmp(buffer, "AJOUT ", strlen("AJOUT ")) == 0)
    {
			bzero(buffer2, BUF_SIZE);
			bzero(buffer3, BUF_SIZE);
    	for(i = 0; buffer[i + strlen("AJOUT ")] != ' '; i++)
    		buffer2[i] = buffer[i + strlen("AJOUT ")];

    	buffer2[i] = '\0';
    	tailleCommandeEtAddresse = (++i) + strlen("AJOUT ");
			char* adr = (char*) malloc(sizeof(char) * (strlen(buffer2) + 1));
			strcat(adr, buffer2);

    	for(i=0; buffer[i + tailleCommandeEtAddresse] != '\0' && (i + tailleCommandeEtAddresse) < BUF_SIZE; i++)
    		buffer3[i] = buffer[i + tailleCommandeEtAddresse];

    	buffer3[i + tailleCommandeEtAddresse] = '\0';
			char* txt = (char*) malloc(sizeof(char) * (strlen(buffer3) + 1));
			strcat(txt, buffer3);

			sem_wait(&controle.libre);
			controle.num_libre--;
    	if(ajout(adr, txt))
    	{
    		printf("erreur ajout");
    		exit(1);
    	}
			sem_post(&controle.libre);
			controle.num_libre++;

			tailleAEcrire = strlen(" ");
    	if((write(sock_pipe, &tailleAEcrire, sizeof(int))) < 0)
    	{
    		printf("erreur réponse_ajout");
    		exit(1);
    	}
			if((write(sock_pipe, " ", tailleAEcrire)) < 0)
    	{
    		printf("erreur réponse_ajout");
    		exit(1);
    	}
    }
    if(strncmp(buffer, "SUPPRIMER ", strlen("SUPPRIMER ")) == 0)
    {
    	for(i = 0; buffer[i + strlen("SUPPRIMER ")] != ' '; i++)
    		buffer2[i] = buffer[i + strlen("SUPPRIMER ")];

    	buffer2[i] = '\0';
    	tailleCommandeEtAddresse = (++i) + strlen("SUPPRIMER ");

    	for(i=0; buffer[i + tailleCommandeEtAddresse] != '\0' && (i + tailleCommandeEtAddresse) < BUF_SIZE; i++)
    		buffer3[i] = buffer[i + tailleCommandeEtAddresse];

    	buffer3[i + tailleCommandeEtAddresse] = '\0';
			Annonce_t* annonceASupprimer = NULL;
    	Annonce_t* annonceTmp = liste_annonces;
			if(annonceTmp != NULL)
			{
				if (strcmp(annonceTmp->adresse, buffer2) == 0 && strcmp(annonceTmp->texte, buffer3) == 0)
				{
					annonceASupprimer = annonceTmp;
				}
				else
				{
					annonceTmp = annonceTmp->suivant;
					while (annonceTmp != NULL)
					{
						if (strcmp(annonceTmp->adresse, buffer2) == 0 && strcmp(annonceTmp->texte, buffer3) == 0)
						{
							annonceASupprimer = annonceTmp;
							break ;
						}
						annonceTmp = annonceTmp->suivant;
					}
				}
			}
			sem_wait(&controle.libre);
			controle.num_libre--;
			retVal = suppression(annonceASupprimer);
			sem_post(&controle.libre);
			controle.num_libre++;
    	if(retVal < 0)
    	{
				tailleAEcrire = strlen("erreur");
	    	if((write(sock_pipe, &tailleAEcrire, sizeof(int))) < 0)
	    	{
	    		printf("erreur réponse_supprimer");
	    		exit(1);
	    	}
		  	if((write(sock_pipe, "erreur", tailleAEcrire)) < 0)
		  	{
		  		printf("erreur réponse_supprimer");
		  		exit(1);
		  	}
    	}
    	else
    	{
				tailleAEcrire = strlen("OK");
	    	if((write(sock_pipe, &tailleAEcrire, sizeof(int))) < 0)
	    	{
	    		printf("erreur réponse_supprimer");
	    		exit(1);
	    	}
		  	if((write(sock_pipe, "OK", tailleAEcrire)) < 0)
		  	{
		  		printf("erreur réponse_supprimer");
		  		exit(1);
		  	}
    	}
    }
    if(strncmp(buffer, "LISTER", strlen("LISTER")) == 0)
    {
			sem_wait(&controle.libre);
			controle.num_libre--;
			char * liste = lister();
			if(!longueur())
				bzero(liste, strlen(liste) + 1);
			sem_post(&controle.libre);
			controle.num_libre++;
			tailleAEcrire = strlen(liste);
    	if((write(sock_pipe, &tailleAEcrire, sizeof(int))) < 0)
    	{
    		printf("erreur réponse_lister");
    		exit(1);
    	}
    	if((write(sock_pipe, liste, tailleAEcrire)) < 0)
    	{
    		printf("erreur lister");
    		exit(1);
    	}
			free(liste);
    }
		fflush(stdout) ;
	}
}

int ajout(char *adresse, char *texte)
{
	Annonce_t *annonce;
	if ((annonce = (Annonce_t *) malloc (sizeof(Annonce_t))) == NULL) return -1;
	annonce->adresse = (char*) malloc (sizeof(char) * (strlen(adresse) + 1));
	annonce->texte = (char*) malloc (sizeof(char) * (strlen(texte) + 1));
	strcat(annonce->adresse, adresse);
	strcat(annonce->texte, texte);
	annonce->suivant = liste_annonces;
	liste_annonces = annonce;
	return 0;
}

int suppression(Annonce_t *annonce)
{
	if (annonce == NULL) return -1;
	if (liste_annonces == NULL) return -2;
	Annonce_t *tmp = liste_annonces;
	Annonce_t *tmp_pred;
	if (strcmp(tmp->adresse,annonce->adresse) == 0 && strcmp(tmp->texte,annonce->texte) == 0)
	{
		free(tmp->adresse);
		free(tmp->texte);
		liste_annonces = liste_annonces->suivant;
		return 0;
	}
	tmp_pred = tmp;
	tmp = tmp->suivant;
	while (tmp != NULL)
	{
		if (strcmp(tmp->adresse,annonce->adresse) == 0 && strcmp(tmp->texte,annonce->texte) == 0)
		{
			free(tmp->adresse);
			free(tmp->texte);
			tmp_pred->suivant = tmp->suivant;
			return 0;
		}
		tmp_pred = tmp;
		tmp = tmp->suivant;
	}
	return -2;
}

char *lister()
{
	char *s;
	Annonce_t *tmp = liste_annonces;
	if ((s = (char *) malloc (longueur() * sizeof(char) + 1)) == NULL) return NULL;
	bzero(s, longueur() + 1);
	while (tmp != NULL)
	{
		strcat(s,tmp->adresse);
		strcat(s,"\t");
		strcat(s,tmp->texte);
		strcat(s,"\n");
		tmp = tmp->suivant;
	}
	return s;
}

size_t longueur()
{
	size_t l = 0;
	Annonce_t *tmp = liste_annonces;
	while (tmp != NULL) {
		l += strlen(tmp->adresse) + 1 + strlen(tmp->texte) + 1;
		tmp = tmp->suivant;
	}
	return l;
}
