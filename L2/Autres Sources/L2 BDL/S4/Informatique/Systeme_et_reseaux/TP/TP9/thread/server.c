#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <pthread.h>

const int MAX_LINE = 2048;
const int PORT = 33333;
const int BACKLOG = 10;
const int LISTENQ = 10;
const int MAX_CONNECT = 10;

void affichage_client(struct sockaddr_in socket);
void *traitement_client(void *socket);
void traitement_requete(int socketID, char* requete);

int main()
{

	int serverSocketID , connfd;
	socklen_t clilen;
	pthread_t thread;

	struct sockaddr_in servaddr , cliaddr;


  serverSocketID = socket(AF_INET , SOCK_STREAM , 0);
	if(serverSocketID == -1)
	{
		perror("socket error.\n");
		exit(1);
	}

	bzero(&servaddr , sizeof(servaddr)); /// initailiser servadr avec des 0.

  servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(PORT);

	if(bind(serverSocketID , (struct sockaddr *)&servaddr , sizeof(servaddr)) < 0)
	{
		perror("bind error.\n");
		exit(1);
	}

	if(listen(serverSocketID , LISTENQ) < 0)
	{
		perror("listen error.\n");
		exit(1);
	}

	clilen = sizeof(cliaddr);
	if((connfd = accept(serverSocketID , (struct sockaddr *)&cliaddr , &clilen)) < 0)
	{
		perror("accept error.\n");
		exit(1);
	}

  affichage_client(cliaddr);
	/// Creation de thread.
	if(pthread_create(&thread , NULL , traitement_client, &connfd) == -1)
	{
		perror("pthread create error.\n");
		exit(1);
	}

 pthread_exit(NULL);

 return 0;
}

void affichage_client(struct sockaddr_in socket)
{
  	printf("Le serveur: une nouvelle connexion de  %s port %d \n",
            inet_ntoa(socket.sin_addr), ntohs(socket.sin_port));
}

void *traitement_client(void* socket)
{

	int socketID = *(int *)socket;
	char buf[MAX_LINE];
	memset(buf , 0 , MAX_LINE);
	int n;
	if((n = recv(socketID , buf , MAX_LINE , 0)) == -1)
	{
			perror("recv error.\n");
			exit(1);
	}
	buf[n] = '\0';

  printf("\n La requete est : %s\n", buf);

  traitement_requete(socketID, buf);
  close(socketID);

}

void traitement_requete(int socketID, char* requete)
{
  int n = strlen(requete);
  char msg[MAX_LINE];
  sprintf(msg, "Le nombre de caracteres de la requete est: %d.", n);
  n = send(socketID , msg , strlen(msg) , 0);
  if(n < 0)
  {
    perror("send error.\n");
		exit(1);
  }
}
