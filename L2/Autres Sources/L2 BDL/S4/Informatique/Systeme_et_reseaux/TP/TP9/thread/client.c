#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <pthread.h>

const int MAX_LINE = 2048;
const int PORT = 33333;
const int BACKLOG = 10;
const int LISTENQ = 10;
const int MAX_CONNECT = 10;

void communication(int socketID);

int main(int argc , char **argv)
{

    int sockfd;
    struct sockaddr_in servaddr;

    if(argc != 2)
    {
        perror("usage:programme <IPaddress>");
        exit(1);
    }


    if((sockfd = socket(AF_INET , SOCK_STREAM , 0)) == -1)
    {
        perror("socket error");
        exit(1);
    }

    bzero(&servaddr , sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    if(inet_pton(AF_INET , argv[1] , &servaddr.sin_addr) < 0)
    {
        printf("inet_pton error for %s\n",argv[1]);
        exit(1);
    }

    if( connect(sockfd , (struct sockaddr *)&servaddr , sizeof(servaddr)) < 0)
    {
        perror("connect error");
        exit(1);
    }

    communication(sockfd);
    return 0;
}

void communication(int socketID)
{
  char msg[MAX_LINE];
  int n;

	memset(msg , 0 , MAX_LINE);
	fgets(msg , MAX_LINE , stdin);
  if(send(socketID , msg , strlen(msg) , 0) == -1)
		{
			perror("send error.\n");
			exit(1);
		}
  n = recv(socketID , msg , MAX_LINE , 0);
  if(n == -1)
  {
  			perror("recv error.\n");
  			exit(1);
  }

  printf("\n%s\n", msg);

  close(socketID);
}
