
Exo 1 : 
*******
15 processus créés

racine0
i=0
PID:7472; PPID:2961
fork
	->	fils1
		i=1
		PID:7473; PPID:7472
		fork
			->	fils11
				i=2
				PID:7475; PPID:7473
				fork
					->	fils111
						i=3
						exit
				fork
					->	fils 112
						i=3
						exit
				exit
		fork
			-> fils12
				i=2
				PID:7476; PPID:1754
				fork
					->	fils121
						i=3
						exit
				fork
					->	fils 122
						i=3
						exit
				exit
		exit
fork
	->	fils2
		i=1
		PID:7474; PPID:7472
		fork
			->	fils21
				i=2
				PID:7478; PPID:1754
				fork
					->	fils211
						i=3
						exit
				fork
					->	fils212
						i=3
						exit
				exit
		fork
			-> fils22
				i=2
				PID:7477; PPID:1754
				fork
					->	fils221
						i=3
						exit
				fork
					->	fils222
						i=3
						exit
				exit
		exit
exit
 

Exo2:
*****
ATTENTION : pour le TP, il faut peut-être rajouter les 2 lignes suivantes (versions différentes de gcc/sys)
#include<sys/types.h>
#include<sys/wait.h>

**Q1: 8 fois "sortie 1", 0 fois "sortie 2", 7 fois "sortie 3"
15 processus créés

racine:
n=2, p=3
i=0
fork
	->	fils1:
		pid:8072 et ppid:8071 et 3
		n=2, p=2
		i=0
		fork
			->	fils11:
				pid:8074 et ppid:8072 et 2
				n=2, p=1
				i=0
				fork
					->	fils111:
						pid:8076 et ppid:8074 et 1
						n=2, p=0
						"Sortie 1"
				i=1
				fork
					->	fils112:
						pid:8077 et ppid:8074 et 1
						n=2, p=0
						"Sortie 1"
				i=2
				"Sortie 3"
		i=1
		fork
			->	fils12:
				pid:8075 et ppid:8072 et 2
				n=2, p=1
				i=0
				fork
					->	fils121:
						pid:8078 et ppid:8075 et 1
						n=2, p=0
						"Sortie 1"
				i=1
				fork
					->	fils122:
						pid:8081 et ppid:8075 et 1
						n=2, p=0
						"Sortie 1"
				i=2
				"Sortie 3"
		i=2
		"Sortie 3"
i=1
fork
	->	fils2:
		pid:8073 et ppid:8071 et 3
		n=2, p=2
		i=0
		fork
			->	fils21:
				pid:8079 et ppid:8073 et 2
				n=2, p=1
				i=0
				fork
					->	fils211:
						pid:8082 et ppid:8079 et 1
						n=2, p=0
						"Sortie 1"
				i=1
				fork
					->	fils212:
						pid:8084 et ppid:8079 et 1
						n=2, p=0
						"Sortie 1"
				i=2
				"Sortie 3"
		i=1
		fork
			->	fils22:
				pid:8080 et ppid:8073 et 2
				n=2, p=1
				i=0
				fork
					->	fils221:
						pid:8083 et ppid:8080 et 1
						n=2, p=0
						"Sortie 1"
				i=1
				fork
					->	fils222:
						pid:8085 et ppid:8080 et 1
						n=2, p=0
						"Sortie 1"
				i=2
				"Sortie 3"
		i=2
		"Sortie 3"
i=2
"Sortie 3"

**Q2: proc2 n p ??
Soit Q(n,p) le nombre de processus créés sans compter la racine.
Q(n,0) = 0
Q(n,p+1) = n*(1+Q(n,p)) = somme_i n^i (où i varie entre 1 et p)

Ainsi on a Q(2,3) = 2 + 2^2 + 2^3 = 14
 
