#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>

#define A sleep(6)

void *fonction(void *);

int main(int argc, char *argv[])
{
	int i;
	int n=atoi(argv[1]);
	int p=atoi(argv[2]);
	pthread_t fils;
	if (p == 0) {
		printf("sortie1\n");
		pthread_exit(0);
	}
	char **temp;
	temp = (char **) malloc (3 * sizeof (char *));
	for (i=0;i<n;i++) {
		printf("tid:%u et pid:%d et ppid:%d et p=%d\n", (unsigned int) (pthread_self()), getpid(), getppid(), p);
		temp[0] = (char *) malloc (sizeof (char)); 
		temp[1] = (char *) malloc (4*sizeof(char) + sizeof (char)); // limite des entiers à 4 caracteres : 9999 
		temp[2] = (char *) malloc (4*sizeof(char) + sizeof (char)); 
		sprintf(temp[0],"");
		sprintf(temp[1],"%d",n);
		sprintf(temp[2],"%d",p-1);
		if (pthread_create(&fils,NULL,fonction,(void *)temp)) {
			perror("pthread_create");
		}
	}
	printf("Sortie3\n");
	pthread_exit(0);
}

void *fonction( void *argv) {
	main(3,(char **)argv);
	printf("Sortie2\n");
	pthread_exit(0);
}
