#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>

/*
  Routine qui calcule et renvoie la taille de tous les fichiers (répertoires et autres fichiers) dans un répertoire
  @param voidDirName le nom du répertoire.
  @return la taille du répertoire sous forme d'un pointeur (void *)
*/
void* taille(void* voidDirName);

int main(int argc, char* argv[])
{
  pthread_t pthread_id;
  struct stat* tabStat = malloc(sizeof(struct stat));
  int sizeArg;
  if(argc != 2)
  { //Si on ne nous donne pas d'argument ou si on en a trop
    printf("Donnez moi le nom d'un fichier en argument\n");
    exit(1);
  }
  if((stat(argv[1], tabStat)) == -1) {perror("stat"); exit(EXIT_FAILURE); }   //Erreur si le fichier n'existe pas par exemple
  sizeArg = tabStat->st_size; // La taille du fichier passé en argument s'il existe

  if ((tabStat->st_mode & S_IFMT) == S_IFDIR)     //On teste si le fichier est un répertoire
  {
    if(pthread_create(&pthread_id, NULL, taille, (void *) argv[1]))
    { //Lancement du thread pour trouver la taille du répertoire
      printf("Erreur pthread_create\n");
      exit(1);
    }
    void* tmp = 0;
    if(pthread_join(pthread_id, &tmp) != 0)    //Récupération de la valeur de retour de la routine
    {
      printf("Erreur pthread_join\n");
      exit(1);
    }
    tmp += sizeArg;                           // On ajoute la taille du fichier lui-même à celle de son contenu
    long int tailleFinale = (long int) tmp;   // On met tout ça dans un (long) int pour afficher le résultat sans warning
    printf("%s est un répertoire, sa taille (son contenu compris) est de  %ld\n", argv[1], tailleFinale);
  }
  else
  { // On affiche juste la taille du fichier dans le cas où ce n'est pas un répertoire
    printf("%s n'est pas un répertoire, sa taille est de %d\n", argv[1], sizeArg);
  }
  free(tabStat);
  return  EXIT_SUCCESS;
}

void* taille(void* voidDirName)
{
  void* tmp = 0, *res = 0;      //2 variables void* car tmp est écrasé par l'appel de pthread_join donc il faut le sauvegarder
  char* dirName = (char *) malloc(sizeof(char) * (1 + strlen((char*) voidDirName)));    // Caste du nom de type void* en char*
  strcpy(dirName, voidDirName);
  struct stat* tabStatTaille = malloc(sizeof(struct stat));
  DIR* leDIR;
  if((leDIR = opendir(dirName)) == NULL)
  { //Ne devrez JAMAIS arriver, vu qu'on appelle pas la fonction taille sur autre chôse qu'un répertoire normalement
    printf("Erreur opendir %s\n", dirName);
    exit(1);
  }
  struct dirent * sd;
  while((sd = readdir(leDIR)) != NULL)
  { // Lecture de tous les fichiers du répertoire
    if(((strcmp(sd->d_name, ".")) != 0) && (strcmp(sd->d_name, "..")))
    { // On ignore les fichiers dont le nom commence par . ou ..
      char* chemin = (char*) malloc(sizeof(char) * (strlen(dirName) + strlen(sd->d_name) + 2));
      // Le chemin relatif du nouveau fichier par rapport à l'ancien répertoire
      strcpy(chemin, dirName);
      strcat(chemin, "/");
      strcat(chemin, sd->d_name);
      if(sd->d_type == 4)
      { //Si le fichier est un répertoire alors on appelle récursivement la fonction taille pour trouver la taille de son contenu
        pthread_t pthread_idTaille;
        if(pthread_create(&pthread_idTaille, NULL, taille, (void *) chemin)) { printf("Erreur pthread_create\n"); exit(1); }
        if(pthread_join(pthread_idTaille, &tmp) != 0) { printf("Erreur pthread_join\n"); exit(1); }
        // Valeur de retour de la routine dans tmp
        res += (long int) tmp;
      }
      // Répertoire ou non, on ajoute la taille du fichier même
      if((stat(chemin, tabStatTaille)) == -1) {perror("stat"); exit(EXIT_FAILURE); }
      res += tabStatTaille->st_size;
      free(chemin);
    }
  }
  free(dirName);
  free(tabStatTaille);
  closedir(leDIR);
  pthread_exit(res);
}
