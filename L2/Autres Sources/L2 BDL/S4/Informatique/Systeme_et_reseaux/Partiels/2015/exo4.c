#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<wait.h>
#include<sys/types.h>

void wait_simple(int sig);

int main()
{
	signal(SIGCHLD, wait_simple);
	int fils;
	if((fils=fork())==-1) {perror("fork"); exit (1);}
	if(fils!=0)
	{
		pause();
	}
	printf("[%d] fini\n", getpid());
	return 0;
}


void wait_simple(int sig)
{
	if(sig == SIGCHLD)
	{
		//Réveillé
	}
}
