#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<wait.h>



int main(void)
{
  pid_t pid;
  int status;
  if((pid=fork()) == -1) { exit(EXIT_FAILURE);}
  char n;
  if(pid != 0)
  {
    n++;
    wait(&status);
    char n;
    if(WIFEXITED(status)) n+=WEXITSTATUS(status);
  }
  else
    n--;
  printf("[%d] : valeur de n est %d\n", getpid(), -n);
  exit(n);
}
