#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
int main(int argc, char *argv[]) {
	int fd[2];
/* create pipe */
	if (pipe(fd) == -1) {
		perror("pipe");
		exit(EXIT_FAILURE);
	}
	pid_t pid1, pid2;
/* create 1st son */
	if ((pid1 = fork()) == -1) {
		perror("1st fork");
		exit(EXIT_FAILURE);
	}
	if (pid1 == 0) {
/** 1st son
* redirect stdout to pipe input fd[1]
* run the command
* ps -aj
**/
/* close pipe output */
		if (close(fd[0]) == -1) {
			perror("1st son close fd[0]");
			exit(EXIT_FAILURE);
		}
/* redirect stdout to pipe input */
		if (dup2(fd[1], 1) == -1) {
			perror("1st son dup2");
			exit(EXIT_FAILURE);
		}
/* exec command: ps -aj */
		execl("/bin/ps", "ps", "-aj", NULL);
/* exec failed */
		perror("1st son exec");
		exit(EXIT_SUCCESS);
	}
/* create 2nd< son */
	if ((pid2 = fork()) == -1) {
		perror("1st fork");
		exit(EXIT_FAILURE);
	}
	if (pid2 == 0) {
/** 2nd son
* redirect stdin to pipe ouput fd[2]
* run the command
* tr "[:upper:][:lower:]" "[:lower:][:upper:]"
**/
/* close pipe input */
		if (close(fd[1]) == -1) {
			perror("2nd son close fd[1]");
			exit(EXIT_FAILURE);
		}
/* redirect stdin to pipe output */
		if (dup2(fd[0], 0) == -1) {
			perror("2nd son dup2");
			exit(EXIT_FAILURE);
		}
/* exec command: tr "[:upper:][:lower:]" "[:lower:][:upper:]" */
		execl("/usr/bin/tr", "tr", "[:upper:][:lower:]", "[:lower:][:upper:]", NULL);
/* exec failed */
		perror("2nd son exec");
		exit(EXIT_SUCCESS);
	}
/** parent **/
/* close pipe input et output */
/* close pipe output */
	if (close(fd[0]) == -1) {
		perror("parent close fd[0]");
		exit(EXIT_FAILURE);
	}
/* close pipe input */
	if (close(fd[1]) == -1) {
		perror("parent close fd[1]");
		exit(EXIT_FAILURE);
	}
/* wait for sons */
	wait(NULL);
	wait(NULL);
/* affichage du père */
	printf("\n%6d : ps -aj | tr \"[:upper:][:lower:]\" \"[:lower:][:upper:]\"\n",
		↪→getpid());
	printf("%6d : ps -aj\n", pid1);
	printf("%6d : tr \"[:upper:][:lower:]\" \"[:lower:][:upper:]\"\n\n", pid2);
	exit(EXIT_SUCCESS);
}

//exo 2

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#define V_MAXSIZE 32 /* max size data vector */
int main(int argc, char *argv[]) {
int ps_fd[2]; /* parent->son pipe */
int sp_fd[2]; /* son->parent pipe */
/* create pipes */
	if (pipe(ps_fd) == -1) {
		perror("parent->son pipe");
		exit(EXIT_FAILURE);
	}
	if (pipe(sp_fd) == -1) {
		perror("son->parent pipe");
		exit(EXIT_FAILURE);
	}
	pid_t pid;
/* create son */
	if ((pid = fork()) == -1) {
		perror("fork");
		exit(EXIT_FAILURE);
	}
	if (pid == 0) {
/** son
* close unused pipes sides
* read data from parent->son pipes
* compute values
* write values on son->parent pipes
**/
/**
* local variabales
**/
int n, v[V_MAXSIZE]; /* for read values */
int min, max; /* for results */
		float mean;
/**
* close unused pipes sides
**/
/* close parent->son pipe input */
		if (close(ps_fd[1]) == -1) {
			perror("son close ps_fd[1]");
			exit(EXIT_FAILURE);
		}
/* close son->parent pipe output */
		if (close(sp_fd[0]) == -1) {
			perror("son close sp_fd[0]");
			exit(EXIT_FAILURE);
		}
/**
* read data sent by parent
**/
/* read number of elements */
		if (read(ps_fd[0], &n, sizeof(int)) != sizeof(int)) {
			perror("son read n");
			exit(EXIT_FAILURE);
		}
/* read vector of elements */
		if (read(ps_fd[0], v, sizeof(int)*n) != sizeof(int)*n) {
			perror("son read v");
			exit(EXIT_FAILURE);
		}
/**
* compute max, min, mean
* we assume n > 0
**/
/* initialise */
		mean = max = min = v[0];
/* max-min compute the max-min of the part of v scanned
* mean accumulate the sum of the elements,
* to compute its value at the we shall divide it by n
*/
		for (int i = 1; i < n; i++) {
			mean += v[i];
			if (v[i] > max)
max = v[i]; /* new max */
				else if(v[i] < min)
min = v[i]; /* new min */
			}
		mean /= n;
/*
* write results to the parent
*/
		if (write(sp_fd[1], &min, sizeof(int)) != sizeof(int)) {
			perror("son write min");
			exit(EXIT_FAILURE);
		}
		if (write(sp_fd[1], &max, sizeof(int)) != sizeof(int)) {
			perror("son write max");
			exit(EXIT_FAILURE);
		}
		if (write(sp_fd[1], &mean, sizeof(float)) != sizeof(float)) {
			perror("son write mean");
			exit(EXIT_FAILURE);
		}
		exit(EXIT_SUCCESS);
	}
/**
* parent
* read data
* send data to son
* read results
* print results
**/
/**
* local variabales
**/
int n, v[V_MAXSIZE]; /* for read values */
int min, max; /* for results */
	float mean;
/**
* close unused pipes sides
**/
/* close parent->son pipe output */
	if (close(ps_fd[0]) == -1) {
		perror("son close ps_fd[0]");
		exit(EXIT_FAILURE);
	}
/* close son->parent pipe input */
	if (close(sp_fd[1]) == -1) {
		perror("son close sp_fd[1]");
		exit(EXIT_FAILURE);
	}
/**
* ask data to user
**/
	do {
		printf("number of elements n [0 < n <= %d]: ", V_MAXSIZE);
		scanf("%d", &n);
if (n <= 0 || n > V_MAXSIZE) /* invalid value */
		printf("n = %d is not a valid value. Valid value: 0 < n <= %d\n", n, V_MAXSIZE
			↪→);
	} while (n <= 0);
	for (int i = 0; i < n; i++) {
		printf("v[%d] : ", i);
		scanf("%d", v+i);
	}
/**
* send data to son
**/
	if (write(ps_fd[1], &n, sizeof(int)) != sizeof(int)) {
		perror("parent write n");
		exit(EXIT_FAILURE);
	}
	if (write(ps_fd[1], v, sizeof(int)*n) != sizeof(int)*n) {
		perror("parent write v");
		exit(EXIT_FAILURE);
	}
/**
* read results sent by son
**/
	if (read(sp_fd[0], &min, sizeof(int)) != sizeof(int)) {
		perror("parent read min");
		exit(EXIT_FAILURE);
	}
	if (read(sp_fd[0], &max, sizeof(int)) != sizeof(int)) {
		perror("parent read max");
		exit(EXIT_FAILURE);
	}
	if (read(sp_fd[0], &mean, sizeof(float)) != sizeof(float)) {
		perror("parent read mean");
		exit(EXIT_FAILURE);
	}
/**
* print results
**/
	printf("\nmin: %5d\nmax: %5d\nmean: %5.2f\n\n", min, max, mean);
	exit(EXIT_SUCCESS);
}