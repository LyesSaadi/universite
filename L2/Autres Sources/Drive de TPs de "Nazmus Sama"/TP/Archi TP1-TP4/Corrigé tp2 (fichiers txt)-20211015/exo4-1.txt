.ORIG x3000

	;; lecture d'un caractère dans R0 avec IN
	IN
	;; affiche le caractère
	OUT

	;; lecture d'un caractère dans R0 avec GETC
	GETC
	;; affiche le caractère
	OUT

	HALT
		
	.END
