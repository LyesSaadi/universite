#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "binary_tree_char.h"
#include "bst.h"

void BST_sort (element_t *, int, int);
void random_array(element_t *, int);
int keyboard_array(element_t *, int);
void print_array(element_t *, int, int);
void usage_sort (char *);

/* MAIN FUNCTION TO BE WRITTEN */

/** Prints the values of an array in increasing order */
/* by using a BST */
void BST_sort (element_t * tab, int l, int r) {
/* LEFT AS AN EXERCISE */
}

/** Initializes an array of n values (pseudo-random generation of values) */
void random_array(element_t * tab, int n) {
  int j;
  srand(time(NULL));
  printf("Random generation of the array\n");
  for (j = 0; j < n; ++j) {
    tab[j] = 'A' + rand()%26;
  }
}

/** Initializes an array of at most n values (keyboard capture) */
/* Returns the number of values */
int keyboard_array(element_t * tab, int n) {
 /* LEFT AS AN EXERCISE */
 }

/** Prints the values of an array between indices l and r */
void print_array(element_t * tab, int l, int r) {
  int i;
  for (i = l; i <= r; ++i) {
    printf("%3c", tab[i]);
  }
  printf("\n");
}

void usage_sort (char * s) {
  printf("%s array_size [0|1] \n", s);
  printf("1st argument: size of array\n");
  printf("2nd argument: 1 for random generation, 0 for keyboard capture \n");
}
