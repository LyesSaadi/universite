#ifndef STACK_H
#define STACK_H

typedef int element_t;

typedef struct stack_s {
	element_t value;
	struct stack_s* stack;
} stack_t;

stack_t* stack_push(element_t value, stack_t* stack);
stack_t* stack_pop(stack_t* stack);
element_t stack_peek(stack_t* stack);
int stack_height(stack_t* stack);
void stack_delete(stack_t** stack);

#endif
