#ifndef TREE_H
#define TREE_H

typedef char element_t;
typedef struct binary_tree_s {
	element_t label;
	struct binary_tree_s * left;
	struct binary_tree_s * right;
} binary_tree_t;

binary_tree_t * empty_tree();
binary_tree_t * cons_binary_tree(element_t, binary_tree_t *, binary_tree_t *);
void delete_binary_tree(binary_tree_t **);

binary_tree_t * left(binary_tree_t *);
binary_tree_t * right(binary_tree_t *);

element_t get_binary_tree_root(const binary_tree_t *);
int is_empty_binary_tree(const binary_tree_t *);

void display_element(element_t);
void display_tree(binary_tree_t *);

int binary_tree_size(binary_tree_t *);
int binary_tree_leaves(binary_tree_t *);
int binary_tree_height(binary_tree_t *);

void prof_pref_rec(binary_tree_t *);
void prof_pref_iter(binary_tree_t *);
void prof_inf_rec(binary_tree_t *);
void prof_inf_iter(binary_tree_t *);
void prof_suf_rec(binary_tree_t *);
void prof_suf_iter(binary_tree_t *);

void affichage_largeur(binary_tree_t *);
void lar(binary_tree_t *, int l);

#endif
