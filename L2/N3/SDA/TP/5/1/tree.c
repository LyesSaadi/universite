#include "tree.h"
#include <stdlib.h>
#include <stdio.h>

#define max(a, b) ((a) < (b) ? (b) : (a))

binary_tree_t * empty_tree() {
	return NULL;
}

binary_tree_t * cons_binary_tree(element_t label, binary_tree_t * left, binary_tree_t * right) {
	binary_tree_t * bt = (binary_tree_t *) malloc(sizeof(binary_tree_t));
	bt->label = label;
	bt->left = left;
	bt->right = right;
	return bt;
}

void delete_binary_tree(binary_tree_t ** bt) {
	if (bt == NULL)
		return;
	if (*bt == NULL)
		return;
	
	delete_binary_tree(&((*bt)->left));
	delete_binary_tree(&((*bt)->right));

	free(*bt);
	*bt = NULL;
}

binary_tree_t * left(binary_tree_t * bt) {
	return bt->left;
}

binary_tree_t * right(binary_tree_t * bt) {
	return bt->right;
}

element_t get_binary_tree_root(const binary_tree_t * bt) {
	return bt->label;
}

int is_empty_binary_tree(const binary_tree_t * bt) {
	return bt == NULL;
}

int binary_tree_size(binary_tree_t * pt) {
	if (is_empty_binary_tree(pt))
		return 0;
	return 1 + binary_tree_size(left(pt)) + binary_tree_size(right(pt));
}

int binary_tree_leaves(binary_tree_t * pt) {
	if (is_empty_binary_tree(pt))
		return 0;
	if (is_empty_binary_tree(left(pt)) && is_empty_binary_tree(right(pt)))
		return 1;
	return binary_tree_leaves(left(pt)) + binary_tree_leaves(right(pt));
}

int binary_tree_height(binary_tree_t * pt) {
	if (is_empty_binary_tree(pt))
		return 0; // ou -1, n'influence pas, et permet de discriminer les arbres vides.
	if (is_empty_binary_tree(left(pt)) && is_empty_binary_tree(right(pt)))
		return 0;
	return 1 + max(binary_tree_height(left(pt)), binary_tree_height(right(pt)));
}

void display_element(element_t e) {
	printf("%c", e);
}

void display_tree(binary_tree_t * bt) {
	printf("=== PROFONDEUR ===\n");
	printf("== PREFIXE ==\n");
	printf("= REC =\n");
	prof_pref_rec(bt);
	printf("= ITER =\n");
	prof_pref_iter(bt);
	printf("== INFIXE ==\n");
	printf("= REC =\n");
	prof_inf_rec(bt);
	printf("= ITER =\n");
	prof_inf_iter(bt);
	printf("== SUFFIXE ==\n");
	printf("= REC =\n");
	prof_suf_rec(bt);
	printf("= ITER =\n");
	prof_suf_iter(bt);
	printf("=== LARGEUR ===\n");
	affichage_largeur(bt);
}

void prof_pref_rec(binary_tree_t * bt) {
	if (bt == NULL)
		return;
	display_element(get_binary_tree_root(bt));
	prof_pref_rec(left(bt));
	prof_pref_rec(right(bt));
}


void prof_pref_iter(binary_tree_t * bt) {
}

void prof_inf_rec(binary_tree_t * bt) {
	if (bt == NULL)
		return;
	prof_inf_rec(left(bt));
	display_element(get_binary_tree_root(bt));
	prof_inf_rec(right(bt));
}

void prof_inf_iter(binary_tree_t * bt) {
}

void prof_suf_rec(binary_tree_t * bt) {
	if (bt == NULL)
		return;
	prof_suf_rec(left(bt));
	prof_suf_rec(right(bt));
	display_element(get_binary_tree_root(bt));
}

void prof_suf_iter(binary_tree_t * bt) {
}

void affichage_largeur(binary_tree_t * bt) {
	int i = 0;

	while (i <= binary_tree_height(bt)) {
		lar(bt, i);
		i++;
	}
}

void lar(binary_tree_t * bt, int l) {
	if (bt == NULL) {
		return;
	}

	if (l == 0) {
		display_element(get_binary_tree_root(bt));
		return;
	} else {
		lar(left(bt), l - 1);
		lar(right(bt), l - 1);
		return;
	}
}
