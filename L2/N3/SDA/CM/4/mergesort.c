#include <stdio.h>
#include <stdlib.h>
#define min(a, b) ((a) < (b) ? a : b)
#define max(a, b) ((a) > (b) ? a : b)

typedef int element_t;
void merge(element_t* t, int b1, int b2) ;
void merge_sort(element_t* t, int d, int f);


void merge_sort(element_t* t, int d, int f) {
	int n = (f - d) + 1;
	/*
	printf("%d\n", n);
	int size = 1, i;
	while (size < n) {
		for (i = 0; i <= n - 2*size; i += 2*size) {
			merge(t + i, size, size);
			// merge(t + i, min(i, n - 1), t + min(i + size, n - 1), ? );
			// `?` serait  `min(i + 2 * size, n - 1) - i - size);` à mon avis
			// Correction :	
			// derniers blocs : max(0, (n % 2*size) - size) dernier bloc.
			// min(size, n % 2*size) taille de l'avant dernier bloc.
		}
		// On s'occupe des deux cas bizarre à la fin.
		merge(t + i, min(size, n % (2*size)), max(0, (n % (2*size)) - size));
		size *= 2;
	}
	*/
	
	if (d > 2) {
		merge_sort(t, d, d + n/2);
		merge_sort(t, d + n/2 + 1, f);
		merge(t, n / 2 + 1, f - d - n / 2);
	}
}

void merge(element_t* t, int b1, int b2) {
	element_t aux[b1 + b2];
	int i, j, k;

	printf("b1 = %d, b2 = %d\n", b1, b2);
	if (b2 < 0)
		return;

	for (i = 0; i < b1; ++i)
		aux[i] = t[i];
	for (k = b1, j = b1 + b2 - 1; j > b1 - 1; --j, k++)
		aux[k] = t[j];
	//i = 0; j = b1 + b2 - 1; k = 0;
	//while (i < j) {
	for (k = 0, i = 0, j = b1 + b2 - 1; k < b1 + b2; ++k) {
		if (aux[i] < aux[j]) {
			t[k] = aux[i++];
		} else {
			t[k] = aux[j--];
		}
	}
}

int main(int argc, char** argv) {
	int* t = (int*) malloc((argc - 1) * sizeof(int));

	if (NULL == t) {
		perror("malloc error");
		exit(2);
	}

	for (int i = 0; i < argc - 1; i++) {
		t[i] = atoi(argv[i + 1]);
	}

	merge_sort(t, 0, argc - 2);

	for (int i = 0; i < argc - 1; i++) {
		printf("t[%d] = %d\n", i, t[i]);
	}

	return 0;
}
