#include <stdlib.h>
#include <stdio.h>

void quicksort(int* t, int n) {

}

int main(int argc, char** argv) {
	int* t = (int*) malloc((argc - 1) * sizeof(int));

	if (NULL == t) {
		perror("malloc error");
		exit(2);
	}

	for (int i = 0; i < argc - 1; i++) {
		t[i] = atoi(argv[i + 1]);
	}

	quicksort(t, argc - 1);

	for (int i = 0; i < argc - 1; i++) {
		printf("t[%d] = %d\n", i, t[i]);
	}

	return 0;
}
