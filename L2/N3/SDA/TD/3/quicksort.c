#include <stdio.h>
#include <stdlib.h>
#define min(a, b) ((a) < (b) ? a : b)

typedef int element_t;

void swap(element_t* a, element_t* b) {
	element_t temp = *b;
	*b = *a;
	*a = temp;
}

// We take the last element e as a pivot here. And put it in its right position.
int partition(element_t* t, int b, int e) {
	int i = b, j = e; // e - 1 était l'erreur
	while (i < j) {
		while (t[i] < t[e]) {
			++i;
		}
		while ((t[j] >= t[e]) && (i < j)) {
			--j;
		}
		swap(t + i, t + j);
		printf("   swap(t + %d, t + %d)\n", j, e);
		if (!(i < j)) {
			i++; j--;
		}
	}

	if ((i == j) && (t[j] > t[e])) {
		swap(t + j, t + e);
		printf("  swap(t + %d, t + %d)\n", j, e);
		return j;
	}

	swap(t + (j + 1), t + e);
	printf("  swap(t + %d, t + %d)\n", j + 1, e);
	return j + 1;
}

void quicksort(int* t, int n) {
	if (n > 1) {
		printf("partition(t, 0, %d - 1)\n", n);
		int p = partition(t, 0, n - 1);
		printf(" quicksort(t, %d)\n", p);
		quicksort(t, p);
		printf(" quicksort(t + %d + 1, %d - (%d + 1))\n", p, n, p);
		quicksort(t + p + 1, n - (p + 1));
	}
}


int main(int argc, char** argv) {
	int* t = (int*) malloc((argc - 1) * sizeof(int));

	if (NULL == t) {
		perror("malloc error");
		exit(2);
	}

	for (int i = 0; i < argc - 1; i++) {
		t[i] = atoi(argv[i + 1]);
	}

	quicksort(t, argc - 1);

	for (int i = 0; i < argc - 1; i++) {
		printf("t[%d] = %d\n", i, t[i]);
	}

	return 0;
}
