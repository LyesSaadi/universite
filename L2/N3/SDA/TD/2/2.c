#include <assert.h>
#include <math.h>

typedef struct node_s {
	float value;
	struct node_s* next;
} node_t;

typedef struct list_s {
	node_t* first;
	int size;
} list_t;

int main() {
	return 0;
}

node_t* min(node_t* n, node_t* m) {
	assert(m != NULL);

	if (n == NULL)
		return m;
	return min(n->next, (m->value > n->value) ? n : m);
}

void going_through(node_t* prior) {

	going_through(prior->next);
}

void selection_linked_list(list_t* l) {
	node_t* m;
	node_t* n = l->first;

	while (n != NULL) {
		m = min(n->next, n);



		n = n->next;
	}
}
