/*
 * Lyes SAADI 12107246
 * Ceci est mon propre travail.
 */

#include <assert.h>
#include <stdlib.h>

typedef int element_t;

typedef struct node_s * node_t;
struct node_s {
	element_t value;
	node_t next;
};

typedef struct list_s list_t;
struct list_s {
	node_t first; // Le prof m'a dit que ce n'était qu'une seul * ?
	node_t last;
};

list_t empty_list();
void delete_list(list_t*);
element_t list_first(list_t);
void list_push_back(element_t, list_t*);
void list_push_front(element_t, list_t*);
int is_empty(list_t);

node_t list_node_remove_first(list_t*);
void list_node_push_back(list_t*, node_t); // Une seule * ici selon de prof aussi.
void list_node_push_front(list_t*, node_t); // Pareil.

void list_split(list_t*, list_t*, list_t*);
void list_merge(list_t*, list_t*, list_t*);
list_t list_merge_sort(list_t*);
list_t list_iter_merge_sort(list_t*);

list_t empty_list() {
	return { NULL, NULL };
}

void delete_list(list_t* list) {
	// Théoriquement, si list->first est null, alors list->last est null, mais, juste au cas où, on free les deux.

	if (list->first != NULL) {
		free(list->first);
		list->first = NULL;
	}

	if (list->last != NULL) {
		free(list->last);
		list->last = NULL;
	}
}

element_t list_first(list_t list) {
	return list.first->value;
}

void list_push_back(element_t element, list_t* list) {
	node_t n = (node_t) malloc(sizeof(struct node_s));
	n->value = element;
	n->next = NULL;

	if (is_empty(*list)) {
		list->first = n;
		list->last = n;
	} else {
		list->last->next = n;
		list->last = n;
	}
}

void list_push_front(element_t element, list_t* list) {
	node_t n = (node_t) malloc(sizeof(struct node_s));
	n->value = element;
	n->next = NULL;

	if (is_empty(*list)) {
		list->first = n;
		list->last = n;
	} else {
		n->next = list->first;
		list->first = n;
	}
}

int is_empty(list_t list) {
	// Il existe un cas théorique où list.first ou list.first est NULL, mais pas les deux. Ce cas est invalide. 
	assert((list.first == NULL && list.last == NULL) && (list.first != NULL || list.last != NULL));

	return (list.first == NULL && list.last == NULL);
}

node_t list_node_remove_first(list_t* list) {
	node_t f = list->first;	
	list->first = f->next;

	return f;
}

void list_node_push_back(list_t* list, node_t node) {
	if (is_empty(*list)) {
		list->first = node;
		list->last = node;
	} else {
		list->last->next = node;
		list->last = node;
	}
}

void list_node_push_front(list_t* list, node_t node) {
	if (is_empty(*list)) {
		list->first = node;
		list->last = node;
	} else {
		node->next = list->first;
		list->first = node;
	}
}

void list_split(list_t* l, list_t* l1, list_t* l2) {
	assert(!is_empty(*l));

	int taille = 1;
	node_t n = l->first;

	while (n->next != NULL) {
		++taille;
		n = n->next;	
	}

	n = l->first;

	for (int i = 0; i < (taille - 1) / 2; ++i) {
		n = n->next;	
	}

	l1->first = l->first;
	l1->last = n;
	l2->first = n->next;
	l2->last = l->last;
	l1->last->next = NULL;
	l->first = NULL;
	l->last = NULL;
}

void list_merge(list_t* l, list_t* l1, list_t* l2) {
	if (is_empty(*l1)) {
		l->first = l2->first;
		l->last = l2->last;
	} else {
		l->first = l1->first;
		l1->last->next = l2->first;
		l->last = l2->last;
	}

	l1->first = NULL;
	l2->first = NULL;
	l1->last = NULL;
	l2->last = NULL;
}

list_t list_merge_sort(list_t* l) {
	if(is_empty(*l)) {
		return *l;
	}

	int taille = 1;
	node_t n = l->first;

	while (n->next != NULL) {
		++taille;
		n = n->next;	
	}

	if (taille == 1) {
		return *l;
	} if (taille == 2) {
		list_t* l1 = (list_t*) malloc(sizeof(list_t));
		list_t* l2 = (list_t*) malloc(sizeof(list_t));

		list_split(l, l1, l2);
		
		if (l1->first->value > l2->last->value) {
			list_t* temp = l1;
			l1 = l2;
			l2 = temp;
		}

		list_merge(l, l1, l2);

		free(l1);
		free(l2);

		return *l;
	} else {
		list_t* l1 = (list_t*) malloc(sizeof(list_t));
		list_t* l2 = (list_t*) malloc(sizeof(list_t));

		list_split(l, l1, l2);
		list_merge_sort(l1);
		list_merge_sort(l2);
		list_merge(l, l1, l2);

		free(l1);
		free(l2);

		return *l;
	}
}

// Pas assez de temps
list_t list_iter_merge_sort(list_t* l) {}
