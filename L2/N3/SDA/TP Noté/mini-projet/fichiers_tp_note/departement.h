#ifndef __DEPARTEMENT__
#define __DEPARTEMENT__

#include "academie.h"

typedef struct departement_s  departement_t;

/*-----------------------------------------------------------------------------*
 * role            : donner acces au code du departement
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const char * departement_get_code(const departement_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces au nom du departement
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const char * departement_get_nom(const departement_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces a l'academie du departement
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const academie_t * departement_get_academie(const departement_t * p);

#endif
