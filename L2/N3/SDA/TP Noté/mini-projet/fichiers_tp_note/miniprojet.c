#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "academie.h"
#include "commune.h"
#include "departement.h"
#include "etablissement.h"
#include "information_geo.h"
#include "region.h"


void afficher_commune(const commune_t * pcom);
void afficher_departement(const departement_t * pdep);
void afficher_academie(const academie_t * pacad);
void afficher_region(const region_t * pregion);


int main() {
	FILE *              fichier_annuaire = NULL;
	etablissement_t **  donnees_annuaire = NULL;
	int                 taille_annuaire  = 0;
	information_geo_t * information_geo  = NULL;
	int                 i;

	/* chargement et affichage des informations a caractere geographique */
	information_geo = load_information_geo();
	puts("----- informations a caractere geographique -----");
	show_information_geo(information_geo);
	puts("-------------------------------------------------");

	/* allocation dynamique d'un tableau permettant de stocker un etablissement */
	donnees_annuaire = (etablissement_t **)malloc(sizeof(etablissement_t*));
	if (NULL == donnees_annuaire) {
		perror("malloc fails.");
		exit(-1);
	}
	
	/* ouverture d'un flux sur le fichier annuaire */
	fichier_annuaire = open_data();
	
	/* lecture de l'annuaire */
	while (! feof(fichier_annuaire)) {
		donnees_annuaire[taille_annuaire] = load_etablissement(fichier_annuaire, information_geo);
		
		if (NULL != donnees_annuaire[taille_annuaire]) {
			++taille_annuaire;
			/* augmenter dynamiquement la taille du tableau permettant de stocker les etablissements */
			donnees_annuaire = (etablissement_t **)realloc(donnees_annuaire, (taille_annuaire + 1) * sizeof(etablissement_t*));
			if (NULL == donnees_annuaire) {
				perror("realloc fails.");
				exit(-1);
			}
		}
	}
	
	/* ajustement dynamique de la taille du tableau permettant de stocker les etablissements */
	donnees_annuaire = (etablissement_t **)realloc(donnees_annuaire, taille_annuaire * sizeof(etablissement_t*));
	if (NULL == donnees_annuaire) {
		perror("realloc fails.");
		exit(-1);
	}
	
	/* fermeture du flux sur le fichier annuaire */
	fclose(fichier_annuaire);
	fichier_annuaire = NULL;
	
	/* affichage de l'annuaire */
	puts("----- annuaire des etablissements -----");
	for(i = 0; i < taille_annuaire; ++i) {

		printf("(%s) %s - %s - %s - (%f,%f) - ",
			   etablissement_get_uai(donnees_annuaire[i]),
			   etablissement_get_nom(donnees_annuaire[i]),
			   (etablissement_get_statut(donnees_annuaire[i]) == STATUT_PUBLIC) ? "PU" : ((etablissement_get_statut(donnees_annuaire[i]) == STATUT_PRIVATE) ? "PR" : "???"),
			   (etablissement_get_niveau(donnees_annuaire[i]) == NIVEAU_COLLEGE) ? "Collège" : ((etablissement_get_niveau(donnees_annuaire[i]) == NIVEAU_LYCEE) ? "Lycée" : "???"),
			   etablissement_get_longitude(donnees_annuaire[i]),
			   etablissement_get_latitude(donnees_annuaire[i]));
		
		afficher_commune(etablissement_get_commune(donnees_annuaire[i]));
		putchar('\n');
	}
	puts("---------------------------------------");

	/* liberation de la memoire allouee dynamiquement */
	for(i = 0; i < taille_annuaire; ++i)
		delete_etablissement(donnees_annuaire + i);
	delete_information_geo(&information_geo);
	
	return 0;
	
}

void afficher_commune(const commune_t * pcom) {
	assert(NULL != pcom);
	printf("(%s) %s - ", commune_get_code(pcom), commune_get_nom(pcom));
	afficher_departement(commune_get_departement(pcom));
}

void afficher_departement(const departement_t * pdep) {
	assert(NULL != pdep);
	printf("(%s) %s - ", departement_get_code(pdep), departement_get_nom(pdep));
	afficher_academie(departement_get_academie(pdep));
}

void afficher_academie(const academie_t * pacad) {
	assert(NULL != pacad);
	printf("(%d) %s - ", academie_get_code(pacad), academie_get_nom(pacad));
	afficher_region(academie_get_region(pacad));
}

void afficher_region(const region_t * pregion) {
	assert(NULL != pregion);
	printf("(%d) %s", region_get_code(pregion), region_get_nom(pregion));
}
