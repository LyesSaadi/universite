#ifndef __COMMUNE__
#define __COMMUNE__

#include "departement.h"

typedef struct commune_s  commune_t;

/*-----------------------------------------------------------------------------*
 * role            : donner acces au code de la commune
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const char * commune_get_code(const commune_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces au nom de la commune
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const char * commune_get_nom(const commune_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces au departement de la commune
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const departement_t * commune_get_departement(const commune_t * p);

#endif
