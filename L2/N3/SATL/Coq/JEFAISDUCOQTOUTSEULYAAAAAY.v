Require Extraction.

Inductive Nat : Type :=
  zero | suc : Nat -> Nat.

Fixpoint plus (i j : Nat) :=
  match j with
    zero => i
  | suc j' => suc(plus i j')
  end.

Notation "i + j" := (plus i j).

Compute (suc (suc (suc zero)) + suc (suc (suc zero))).

Theorem plus0r : forall (i: Nat), i = i + zero.
Proof.
  intros.
  destruct i.
  +
    simpl.
    reflexivity.
  +
    simpl.
    reflexivity.
Qed.

Theorem plus0l : forall (i: Nat), i = zero + i.
Proof.
  intros.
  induction i.
  +
    simpl.
    reflexivity.
  +
    simpl.
    rewrite <-IHi.
    reflexivity.
Qed.

Theorem plus1r : forall (i: Nat), i + (suc zero) = (suc i).
Proof.
  intros.
  destruct i.
  +
    simpl.
    reflexivity.
  +
    simpl.
    reflexivity.
Qed.

Theorem plus1l : forall (i: Nat), (suc zero) + i = (suc i).
Proof.
  intros.
  induction i.
  +
    simpl.
    reflexivity.
  +
    simpl.
    rewrite IHi.
    reflexivity.
Qed.

Theorem sucl : forall (i: Nat) (j: Nat), (suc i) + j = (suc (i + j)).
Proof.
  intros.
  induction j.
  +
    simpl.
    reflexivity.
  +
    simpl.
    rewrite IHj.
    reflexivity.
Qed.

Theorem comm : forall (i: Nat) (j: Nat), i + j = j + i.
Proof.
  intros.
  induction j.
  +
    simpl.
    apply plus0l.
  +
    simpl.
    rewrite IHj.
    rewrite sucl.
    reflexivity.
Qed.

Theorem assoc : forall (i: Nat) (j: Nat) (k: Nat), (i + j) + k = i + (j + k).
Proof.
  intros.
  induction j.
  +
    simpl.
    rewrite <-plus0l.
    reflexivity.
  +
    simpl.
    rewrite sucl.
    rewrite IHj.
    rewrite sucl.
    simpl.
    reflexivity.
Qed.