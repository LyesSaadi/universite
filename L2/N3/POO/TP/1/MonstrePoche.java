public class MonstrePoche {
    private String nom;
    private int atq;
    private int vie;
    private int niv;

    public MonstrePoche(String n, int a, int v, int lvl) {
        nom = n;
        atq = a;
        vie = v;
        niv = lvl;
    }
    public void afficher() {
        System.out.printf("Monstre %s, niv. %d, atq. %d, vie %d\n",
                nom, niv, atq, vie);
    }
    public void levelUp() {
        ++niv;
    }
    public void prendreCoup(int pv) {
        vie -= pv;
    }
    public static void main(String[] args) {
        MonstrePoche mp = new MonstrePoche("pique-atchoum", 20, 50, 1);
        mp.afficher();
        mp.levelUp();
        mp.afficher();
        mp.prendreCoup(15);
        mp.afficher();
    }
}
