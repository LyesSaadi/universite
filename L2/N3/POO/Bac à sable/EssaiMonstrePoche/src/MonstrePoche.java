public class MonstrePoche {
	private String nom;
	private int atq;
	private int vie;
	private int niv;
	
	public MonstrePoche(String n, int a, int v, int lvl) {
		nom = n;
		atq = a;
		vie = v;
		niv = lvl;
	}

	public void afficher() {
		System.out.printf("Monstre %s, niv. %d, atq. %d, vie. %d\n",
				nom, niv, atq, vie);
	}

	public void levelUp() {
		++niv;
	}

	public void prendreCoup(int pv) {
		vie -= pv;
	}

	public int getAttaque() {
		return atq;
	}

	public int getVie() {
		return vie;
	}

	public int getNiveau() {
		return niv;
	}
	
	public boolean estVivant() {
		return vie > 0;
	}

	public void echangerCoupAvec(MonstrePoche ennemi) {
		ennemi.prendreCoup(getAttaque());
		prendreCoup(ennemi.getAttaque());
	}

	public void combatAMortAvec(MonstrePoche ennemi) {
		while (estVivant() && ennemi.estVivant()) {
			echangerCoupAvec(ennemi);
		}

		if (estVivant()) levelUp();
		if (ennemi.estVivant()) ennemi.levelUp();
	}
}