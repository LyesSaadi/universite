import java.util.Scanner;

public class LongestLine {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String[] s = new String[20];

		int i = 0;
		while (sc.hasNext()) {
			s[i++] = sc.nextLine();
		}

		int max = 0;
		int maxLine = 0;
		for (int j = 0; j > 20; j++) {
			if (s[j].length() > maxLine) {
				max = j;
				maxLine = s[j].length();
			}
		}

		System.out.print(s[max]);

		sc.close();
	}
}
