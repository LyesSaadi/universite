package devoirjm2.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import devoirjm2.Aliment;
import devoirjm2.Direction;
import devoirjm2.Piece;

/**
 * Test class for Piece.
 *
 * Une piece dans un jeu d'aventure.
 * <p>
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * </p>
 * <p>
 * Une "Piece" represente un des lieux dans lesquels se déroule l'action du jeu.
 * Elle est reliée a au plus quatre autres "Piece" par des sorties. Les sorties
 * sont étiquettées "nord", "est", "sud", "ouest". Pour chaque direction, la
 * "Piece" possède une référence sur la piece voisine ou null s'il n'y a pas de
 * sortie dans cette direction.
 * </p>
 * <p>
 *
 * Une piece peut aussi contenir des objets représentés par des instances de la
 * classe Aliment. Durant le déroulement du jeu des objets peuvent etre ajoutés
 * ou supprimés de la piece.
 * </p>
 */
public class TestPiece extends DataProvider {
	// State:
	private String description;
	private LinkedList<Aliment> content;
	private Piece nord;
	private Piece est;
	private Piece sud;
	private Piece ouest;


	private void saveState(Piece self) {
		// Put here the code to save the state of self:
		description = self.descriptionCourte();
		content = new LinkedList<Aliment>(self);
		nord = self.pieceSuivante(Direction.NORD);
		est = self.pieceSuivante(Direction.EST);
		sud = self.pieceSuivante(Direction.SUD);
		ouest = self.pieceSuivante(Direction.OUEST);
	}

	private void assertPurity(Piece self) {
		// Put here the code to check purity for self:
		assertEquals(description, self.descriptionCourte());
		assertTrue(self.contentEquals(content));
		assertEquals(nord, self.pieceSuivante(Direction.NORD));
		assertEquals(est, self.pieceSuivante(Direction.EST));
		assertEquals(sud, self.pieceSuivante(Direction.SUD));
		assertEquals(ouest, self.pieceSuivante(Direction.OUEST));
	}

	public void assertInvariant(Piece self) {
		// Put here the code to check the invariant:
		// @invariant descriptionCourte() != null;
		assertNotNull(self.descriptionCourte());
		// @invariant descriptionSorties() != null
		assertNotNull(self.descriptionSorties());
		// @invariant descriptionLongue() != null;
		assertNotNull(self.descriptionLongue());
		// @invariant descriptionLongue().contains(descriptionCourte());
		assertTrue(self.descriptionLongue().contains(self.descriptionCourte()));
		// @invariant descriptionLongue().contains(descriptionSorties());
		assertTrue(self.descriptionLongue().contains(self.descriptionSorties()));
	}

	/**
	 * Test method for constructor Piece
	 *
	 * Initialise une piece décrite par la chaine de caractères spécifiée.
	 * Initialement, cette piece ne possède aucune sortie. La description fournie
	 * est une courte phrase comme "la bibliothèque" ou "la salle de TP". La piece
	 * initialisée ne contient aucun objets.
	 *
	 * @throws NullPointerException si la description spécifiée est null
	 */
	@ParameterizedTest
	@MethodSource("namePieceProvider")
	public void testPiece(String description) {

		// Pré-conditions:
		// @requires description != null;
		if (description == null) {
			assertThrows(NullPointerException.class, () -> new Piece(description));
			return;
		}

		// Oldies:

		// Exécution:
		Piece result = new Piece(description);

		// Post-conditions:
		// @ensures descriptionCourte().equals(description);
		assertEquals(description, result.descriptionCourte());
		// @ensures pieceSuivante(Direction.NORD) == null;
		assertNull(result.pieceSuivante(Direction.NORD));
		// @ensures pieceSuivante(Direction.SUD) == null;
		assertNull(result.pieceSuivante(Direction.SUD));
		// @ensures pieceSuivante(Direction.EST) == null;
		assertNull(result.pieceSuivante(Direction.EST));
		// @ensures pieceSuivante(Direction.OUEST) == null;
		assertNull(result.pieceSuivante(Direction.OUEST));
		// @ensures isEmpty();
		assertTrue(result.isEmpty());

		// Invariant:
		assertInvariant(result);
	}

	/**
	 * Test method for constructor Piece
	 *
	 * Initialise une piece décrite par la chaine de caractères spécifiée et
	 * contenant les objets de la Collection spécifiée.
	 * @throws NullPointerException si la description ou la Collection spécifiée est null,
         			ou bien si la Collection spécifiée contient null
	 */
	@ParameterizedTest
	@MethodSource("nameAndCollProvider")
	public void testPiece(String description, Collection<? extends Aliment> c) {

		// Pré-conditions:
		// @requires description != null;
		// @requires c != null;
		// @requires !contains(null);
		if (description == null || c == null || c.contains(null)) {
			assertThrows(NullPointerException.class, () -> new Piece(description, c));
			return;
		}

		// Oldies:

		// Exécution:
		Piece result = new Piece(description, c);

		// Post-conditions:
		// @ensures descriptionCourte().equals(description);
		assertEquals(description, result.descriptionCourte());
		// @ensures pieceSuivante(Direction.NORD) == null;
		assertNull(result.pieceSuivante(Direction.NORD));
		// @ensures pieceSuivante(Direction.SUD) == null;
		assertNull(result.pieceSuivante(Direction.SUD));
		// @ensures pieceSuivante(Direction.EST) == null;
		assertNull(result.pieceSuivante(Direction.EST));
		// @ensures pieceSuivante(Direction.OUEST) == null;
		assertNull(result.pieceSuivante(Direction.OUEST));
		// @ensures size() == c.size();
		assertEquals(c.size(), result.size());
		// @ensures contentEquals(c);
		assertTrue(result.contentEquals(c));

		// Invariant:
		assertInvariant(result);
	}

	/**
	 * Test method for constructor Piece
	 *
	 * Initialise une piece décrite par la chaine de caractères spécifiée et
	 * contenant les nbObjets premiers objets du tableau spécifié.
	 * @throws NullPointerException si la description ou le tableau spécifié est null, ou bien si un des nbAliments premiers éléments du
        								tableau spécifié est null
	 * @throws IllegalArgumentException si l'entier spécifié est strictement négatif ou strictement supérieur à la taille du tableau
	 */
	@ParameterizedTest
	@MethodSource("nameAndTabProvider")
	public void testPiece(String description, Aliment[] tabAliments, int nbAliments) {

		// Pré-conditions:
		// @requires description != null;
		// @requires tabAliments != null;
		// @requires nbAliments >= 0;
		// @requires nbAliments <= tabAliments.length;
		// @requires (\forall int i; i >= 0 && i < nbAliments; tabAliments[i] != null);
		boolean containsNull = false;
		if (tabAliments != null && nbAliments >= 0 && nbAliments <= tabAliments.length) {
			for (int i = 0; i < nbAliments && !containsNull; i++) {
				containsNull = (tabAliments[i] == null);
			}
		}
		if (description == null || tabAliments == null || nbAliments < 0 || nbAliments > tabAliments.length || containsNull) {
			Throwable ex = assertThrows(Throwable.class, () -> new Piece(description, tabAliments, nbAliments));
			if (ex instanceof NullPointerException) {
				assertTrue(description == null || tabAliments == null || containsNull);
			} else if (ex instanceof IllegalArgumentException) {
				if (tabAliments == null) {
					assertTrue(nbAliments < 0);
				} else {
					assertTrue(nbAliments < 0 || nbAliments > tabAliments.length);
				}
			} else {
				fail("Exception inattendue" + ex);
			}
			return;
		}

		// Oldies:

		// Exécution:
		Piece result = new Piece(description, tabAliments, nbAliments);

		// Post-conditions:
		// @ensures descriptionCourte().equals(description);
		assertEquals(description, result.descriptionCourte());
		// @ensures pieceSuivante(Direction.NORD) == null;
		assertNull(result.pieceSuivante(Direction.NORD));
		// @ensures pieceSuivante(Direction.SUD) == null;
		assertNull(result.pieceSuivante(Direction.SUD));
		// @ensures pieceSuivante(Direction.EST) == null;
		assertNull(result.pieceSuivante(Direction.EST));
		// @ensures pieceSuivante(Direction.OUEST) == null;
		assertNull(result.pieceSuivante(Direction.OUEST));
		// @ensures size() == nbAliments;
		assertEquals(nbAliments, result.size());
		// @ensures (\forall int i; i >= 0 && i < nbAliments;
		//		frequency(tabAliments[i])
		//		== (\num_of int j; j >= 0 && j < nbAliments; tabAliments[j].equals(tabAliments[i])));
		for (int i = 0; i < nbAliments; i++) {
			int nbOcc = 1;
			for (int j = 0; j < nbAliments; j++) {
				if (i != j && tabAliments[j].equals(tabAliments[i])) {
					nbOcc++;
				}
			}
			assertEquals(nbOcc, result.frequency(tabAliments[i]));
		}

		// Invariant:
		assertInvariant(result);
	}

	/**
	 * Test method for method setSorties
	 *
	 * Définie les sorties de cette piece. A chaque direction correspond ou bien une
	 * piece ou bien la valeur null signifiant qu'il n'y a pas de sortie dans cette
	 * direction.
	 */
	@ParameterizedTest
	@MethodSource("fivePieceProvider")
	public void testsetSorties(Piece self, Piece nord, Piece est, Piece sud, Piece ouest) {
		assumeTrue(self != null);

		// Invariant:
		assertInvariant(self);

		// Pré-conditions:

		// Oldies:

		// Exécution:
		self.setSorties(nord, est, sud, ouest);

		// Post-conditions:
		// @ensures pieceSuivante(Direction.NORD) == nord;
		assertEquals(nord, self.pieceSuivante(Direction.NORD));
		// @ensures pieceSuivante(Direction.SUD) == sud;
		assertEquals(sud, self.pieceSuivante(Direction.SUD));
		// @ensures pieceSuivante(Direction.EST) == est;
		assertEquals(est, self.pieceSuivante(Direction.EST));
		// @ensures pieceSuivante(Direction.OUEST) == ouest;
		assertEquals(ouest, self.pieceSuivante(Direction.OUEST));

		// Invariant:
		assertInvariant(self);
	}

	/**
	 * Test method for method descriptionCourte
	 *
	 * Renvoie la description de cette piece (i.e. la description spécifiée lors de
	 * la création de cette instance).
	 */
	@ParameterizedTest
	@MethodSource("pieceProvider")
	public void testdescriptionCourte(Piece self) {
		assumeTrue(self != null);

		// Invariant:
		assertInvariant(self);

		// Pré-conditions:

		// Save state for purity check:
		saveState(self);

		// Oldies:

		// Exécution:
		String result = self.descriptionCourte();

		// Post-conditions:
		// @ensures \result != null;
		assertNotNull(result);

		// Assert purity:
		assertPurity(self);

		// Invariant:
		assertInvariant(self);
	}

	/**
	 * Test method for method descriptionLongue
	 *
	 * Renvoie une description de cette piece mentionant ses sorties et directement
	 * formatée pour affichage, de la forme:
	 *
	 * <pre>
	 *  Vous etes dans la bibliothèque.
	 *  Sorties: nord ouest
	 * </pre>
	 *
	 * Cette description contient les chaines de caractères renvoyées par les
	 * méthodes descriptionCourte et descriptionSorties pour décrire les sorties de
	 * cette piece.
	 */
	@ParameterizedTest
	@MethodSource("pieceProvider")
	public void testdescriptionLongue(Piece self) {
		assumeTrue(self != null);

		// Invariant:
		assertInvariant(self);

		// Pré-conditions:

		// Save state for purity check:
		saveState(self);

		// Oldies:

		// Exécution:
		String result = self.descriptionLongue();

		// Post-conditions:
		// @ensures \result != null;
		assertNotNull(result);
		// @ensures \result.indexOf(descriptionCourte()) >= 0;
		assertTrue(result.indexOf(self.descriptionCourte()) >= 0);
		// @ensures \result.indexOf(descriptionSorties()) >= 0;
		assertTrue(result.indexOf(self.descriptionSorties()) >= 0);

		// Assert purity:
		assertPurity(self);

		// Invariant:
		assertInvariant(self);
	}

	/**
	 * Test method for method descriptionSorties
	 *
	 * Renvoie une description des sorties de cette piece, de la forme:
	 *
	 * <pre>
	 *  Sorties: nord ouest
	 * </pre>
	 *
	 * Cette description est utilisée dans la description longue d'une piece.
	 */
	@ParameterizedTest
	@MethodSource("pieceProvider")
	public void testdescriptionSorties(Piece self) {
		assumeTrue(self != null);

		// Invariant:
		assertInvariant(self);

		// Pré-conditions:

		// Save state for purity check:
		saveState(self);

		// Oldies:

		// Exécution:
		String result = self.descriptionSorties();

		// Post-conditions:
		// @ensures \result != null;
		assertNotNull(result);
		// @ensures (pieceSuivante(Direction.NORD) != null) ==> \result.contains("NORD");
		if (self.pieceSuivante(Direction.NORD) != null) {
			assertTrue(result.contains("NORD"));
		}
		// @ensures (pieceSuivante(Direction.SUD) != null) ==> \result.contains("SUD");
		if (self.pieceSuivante(Direction.SUD) != null) {
			assertTrue(result.contains("SUD"));
		}
		// @ensures (pieceSuivante(Direction..EST) != null) ==> \result.contains("EST");
		if (self.pieceSuivante(Direction.EST) != null) {
			assertTrue(result.contains("EST"));
		}
		// @ensures (pieceSuivante(Direction.OUEST) != null) ==> \result.contains("OUEST");
		if (self.pieceSuivante(Direction.OUEST) != null) {
			assertTrue(result.contains("OUEST"));
		}

		// Assert purity:
		assertPurity(self);

		// Invariant:
		assertInvariant(self);
	}

	/**
	 * Test method for method pieceSuivante
	 *
	 * Renvoie la piece atteinte lorsque l'on se déplace à partir de cette piece
	 * dans la direction spécifiée. Si cette piece ne possède aucune sortie dans
	 * cette direction, renvoie {@code null}.
	 *
	 * @throws NullPointerException si la Direction spécifiée est null
	 */
	@ParameterizedTest
	@MethodSource("pieceAndDirectionProvider")
	public void testpieceSuivante(Piece self, Direction d) {
		assumeTrue(self != null);

		// Invariant:
		assertInvariant(self);

		// Pré-conditions:
		// @requires direction != null;
		// Ajout du 29/12/2022:
        if (d == null) {
            assertThrows(NullPointerException.class, () -> self.pieceSuivante(d));
            return;
        }
        // Fin de l'ajout du 29/12/2022

		// Save state for purity check:
		saveState(self);

		// Oldies:

		// Exécution:
		Piece result = self.pieceSuivante(d);

		// Post-conditions:

		// Assert purity:
		assertPurity(self);

		// Invariant:
		assertInvariant(self);
	}
} // End of the test class for Piece
