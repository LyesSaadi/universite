package devoirjm2;

/**
 * Les directions utilisées pour se déplacer dans le jeu Zork.
 *
 * @author Marc Champesme
 * @version 20/10/2018
 */
public enum Direction {
    NORD,
    EST,
    SUD,
    OUEST;
}
