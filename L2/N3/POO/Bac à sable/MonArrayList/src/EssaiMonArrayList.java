public class EssaiMonArrayList {
	public static void main(String[] args) {
		MonArrayList<String> a = new MonArrayList<String>();
		a.add("Bonjour");
		a.add("Bonchour");
		a.add("Bonjjjjourreee :3");
		a.add(2, "Bonjjjjourreee UwU");
		System.out.println(a);
		System.out.println(a.equals(a));

		for (String s : a) {
			System.out.println(s);
		}
	}
}