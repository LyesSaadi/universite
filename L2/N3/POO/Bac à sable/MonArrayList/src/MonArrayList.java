import java.util.Iterator;

public class MonArrayList<E> implements Cloneable, Iterable<E> {
    private int sz;
    private Object[] vals;
    private final static int CAP_DEFAUT = 20;
    private final static int CAP_MULTIPLIER = 2;
    public MonArrayList() {
        sz = 0;
        vals = new Object[CAP_DEFAUT];
    }
    public MonArrayList(int capacity) {
    	if (capacity < 0)
    		throw new IllegalArgumentException("Capacité " + capacity + " négative");
        sz = 0;
        vals = new Object[capacity];
    }
    public int size() { return sz; }
    public boolean isEmpty() { return size() == 0; }
    public void ensureCapacity(int minCapacity) {
        int oldCap = vals.length;
        if (oldCap < minCapacity) {
            Object[] old = vals;
            vals = new Object[minCapacity];
            for (int i = 0; i < sz; ++i)
                vals[i] = old[i];
        }
    }
    /**
     * Ajoute l'élément e au tableau à la fin du tableau
     * 
     * @param e L'élément à ajouter.
     * 
     * @ensures get(\old(size())) == e
     * @ensures \old(size()) + 1 == size()
     */
    public void add(E e) {
        if (size() == vals.length)
            ensureCapacity(CAP_MULTIPLIER * vals.length);
        vals[sz++] = e;
    }
    /**
     * Ajoute l'élément e à l'index {@code index} du tableau
     * 
     * @param index 
     * @param e L'élément à ajouter.
     * 
     * @ensures (\forall int i; i >= 0 && i < index; get(i).equals(\old(get(i))))
     * @ensures get(index) == e
     * @ensures (\forall int i; i > index && i <= \old(size()); get(i).equals(\old(get(i - 1))))
     * @ensures \old(size()) + 1 == size()
     * @throws IndexOutOfBoundsException si l'index est en dehors de [0; size()[
     */
    public void add(int index, E e) {
    	if (size() < index || index < 0)
    		throw new IndexOutOfBoundsException("You cannot add an element too far in the list.");
        if (sz == vals.length)
            ensureCapacity(CAP_MULTIPLIER * vals.length);
        for (int i = sz; i > index; --i)
            vals[i] = vals[i - 1];
        vals[index] = e;
        ++sz;
    }
    @SuppressWarnings("unchecked")
	public E get(int index) {
    	if (size() <= index || index < 0)
    		throw new IndexOutOfBoundsException("You cannot get an element which does not exists.");
    	return (E) vals[index];
   	}
    public E set(int index, E e) {
    	if (size() <= index || index < 0)
    		throw new IndexOutOfBoundsException("You cannot set an element which does not exists.");
        E res = get(index);
        vals[index] = e;
        return res;
    }
    public E remove(int index) {
    	if (size() <= index || index < 0)
    		throw new IndexOutOfBoundsException("You cannot remove an element which does not exists.");
        E res = get(index);
        for (int i = index; i < sz - 1; ++i)
            vals[i] = vals[i + 1];
        --sz;
        return res;
    }
    @Override
    public String toString() {
        if (isEmpty())
            return "[]";
        String res = "[";
        for (int i = 0; i < size() - 1; ++i)
            res += get(i) + ", ";
        res += get(size() - 1) + "]";
        return res;
    }
	@Override
    public boolean equals(Object o) {
        if (!(o instanceof MonArrayList))
            return false;
        MonArrayList<?> l = (MonArrayList<?>) o;
        if (l.size() != size())
            return false;
        for (int i = 0; i < size(); ++i)
            if (!(get(i).equals(l.get(i))))
                return false;
        return true;
    }
    @Override
    public int hashCode() {
        int res = 1;
        for (int i = 0; i < size(); ++i)
            res = 31 * res + get(i).hashCode();
        return res;
    }
    @SuppressWarnings("unchecked")
    @Override
    public Object clone() {
        try {
			MonArrayList<E> res = (MonArrayList<E>) super.clone();
            res.vals = vals.clone();
            return res;
        } catch (CloneNotSupportedException c) {
            /* ne devrait jamais arriver, car implements Cloneable */
            throw new InternalError();
        }
    }
	@Override
	public Iterator<E> iterator() {
		return new MonArrayListIterator<E>(this);
	}
	public class MonArrayListIterator<E> implements Iterator<E> {
		private MonArrayList<E> list;
		private int index;

		public MonArrayListIterator(MonArrayList<E> l) {
			list = l;
			index = 0;
		}

		@Override
		public boolean hasNext() {
			return index < size();
		}

		@Override
		public E next() {
			return list.get(index++);
		}
	}
}


/*
public class MonArrayList {
	private int sz;
	private int[] vals;
	private final static int CAP_DEFAULT = 20;
	private final static int CAP_MULTIPLIER = 2;

	public static void main(String[] args) {
		MonArrayList liste = new MonArrayList();

		for (int i = 0; i < 50; i++) {
			liste.add(i);
		}
		
		liste.afficher();
		
		for (int i = 0; i < 20; i++) {
			liste.add(20, i);
		}

		liste.afficher();

		for (int i = 0; i < 20; i++) {
			liste.remove(20);
		}

		liste.afficher();

		liste.remove(liste.indexOf(49));

		liste.afficher();
	}

	public MonArrayList() {
		vals = new int[CAP_DEFAULT];
	}

	public MonArrayList(int capacity) {
		vals = new int[capacity];
	}

	public void add(int x) {
		if (sz + 1 >= vals.length) {
			int[] oldVals = vals;
			vals = new int[vals.length * CAP_MULTIPLIER];
			
			int i = 0;
			for (int val : oldVals) {
				vals[i++] = val;
			}
		}
		
		vals[sz++] = x;
	}

	public void add(int index, int x) {
		while (!(index + 1 < vals.length)) {
			int[] oldVals = vals;
			vals = new int[vals.length * CAP_MULTIPLIER];
			
			int i = 0;
			for (int val : oldVals) {
				vals[i++] = val;
			}
		}

		if (index + 1 < sz) {
			for (int i = sz - 1; i >= index; i--) {
				vals[i + 1] = vals[i];
			}
		}

		vals[index] = x;
		sz++;
	}
	
	public int remove(int index) {
		int r = vals[index];

		for (int i = index; i < sz - 1; i++) {
			vals[i] = vals[i + 1];
		}

		vals[--sz] = 0;
		
		return r;
	}

	public int indexOf(int e) {
		for (int i = 0; i < sz; i++) {
			if (e == vals[i]) return i;
		}
		
		return -1;
	}

	public void afficher() {
		System.out.println("[");
		for (int i = 0; i < sz; i++) {
			System.out.println(vals[i] + ",");
		}
		System.out.println("]");
	}
}
*/