public class Joueur {
	private String nom;
	private Piece piece;
	
	public Joueur(Piece p) {
		piece = p;
		nom = "";
	}
	
	public Joueur(Piece p, String n) {
		piece = p;
		nom = n;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String n) {
		nom = n;
	}

	public Piece getPiece() {
		return piece;
	}

	public void setPiece(Piece p) {
		piece = p;
	}
}