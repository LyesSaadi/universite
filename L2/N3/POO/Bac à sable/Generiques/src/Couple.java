public class Couple<PIPI, CACA> {
	private PIPI pipi;
	private CACA caca;
	
	public Couple(PIPI pipi, CACA caca) {
		this.pipi = pipi;
		this.caca = caca;
	}

	public PIPI getPipi() {
		return pipi;
	}

	public CACA getCaca() {
		return caca;
	}

	@Override
	public String toString() {
		return "(" + pipi + ", " + caca + ")";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Couple)) {
			return false;
		}

		Couple<?, ?> couple = (Couple<?, ?>) o;
		return pipi.equals(couple.pipi) && caca.equals(couple.caca);
	}

	@Override
	public int hashCode() {
		return pipi.hashCode() + caca.hashCode();
	}
}