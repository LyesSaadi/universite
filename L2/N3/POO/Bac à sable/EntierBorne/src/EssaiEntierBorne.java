import java.util.Scanner;

public class EssaiEntierBorne {
	public static void main(String[] args) {
		System.out.print("Entrez quelque chose : ");
		Scanner sc = new Scanner(System.in);
		String s = sc.nextLine();
		sc.close();

		try {
			int i = Integer.parseInt(s);

			EntierBorne e = new EntierBorne(i);

			System.out.println(e);

			EntierBorne add = EntierBorne.addition(e, e);

			System.out.println(add);

			EntierBorne sus = EntierBorne.soustraction(e, add);

			System.out.println(sus);

			EntierBorne mul = EntierBorne.multiplication(sus, add);

			System.out.println(mul);

			EntierBorne div = EntierBorne.division(mul, e);

			System.out.println(div);
		} catch (NumberFormatException e) {
			System.out.println("Ceci n'est pas un nombre.");
		} catch (HorsBornesException e) {
			System.out.println("Un entier est hors bornes.");
		} catch (DivisionParZeroException e) {
			System.out.println("Vous divisez par zéro là.");
		}
	}
}