public class Guerrier extends Perso {
	private int atq;
	private int end;
	public static final int END_MAX = 10;

	public Guerrier(String nom, int pv, int niveau, int end, int atq) {
		super(nom, pv, niveau);
		this.end = end;
		this.atq = atq;
	}

	public Guerrier(String nom, int pv, int end, int atq) {
		super(nom, pv);
		this.end = end;
		this.atq = atq;
	}

	public int getAttaque() {
		return atq;
	}

	public int getEndurance() {
		return end;
	}

	protected void setEndurance(int end) {
		this.end = end;
	}

	public boolean attaquer(Perso p) {
		if (end == 0)
			return false;
		p.prendreDegats(atq);
		end--;
		return true;
	}
	
	@Override
	public void seReposer() {
		super.seReposer();
		if (estVivant() && end < END_MAX)
			end++;
	}

	@Override
	public void levelUp() {
		super.levelUp();
		atq += 2;
	}

	@Override
	public String toString() {
		return "Guerrier " + getNom()
		+ " de niveau " + getNiveau()
		+ ", de vie " + getVie() + "/" + PV_MAX
		+ "hp, d'endurance " + end + "/" + END_MAX
		+ " et d'attaque " + atq;
	}
}